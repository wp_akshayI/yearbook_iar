package com.app.yearbook.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.adapter.StaffDeletedPostUserListAdapter;
import com.app.yearbook.model.staff.trash.DeleteListItem;
import com.app.yearbook.model.staff.trash.GetDeleteList;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeletedFragment extends Fragment{

    // implements StaffDeletedPostUserListAdapter.ListAdapterListener
    public View view;
    private ArrayList<DeleteListItem> deleteListArrayList;
    public ProgressDialog progressDialog;
    private LoginUser loginUser;
    public AllMethods utils;
    private StaffDeletedPostUserListAdapter staffDeletedPostUserListAdapter;
    private SearchView searchView;
    private RecyclerView rvDeletePost;
    private String schoolId;
    private LinearLayout lvNoData;
    private String type;

    public DeletedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_delete, container, false);
        setHasOptionsMenu(true);
        setView();
        return view;
    }

    public void setView()
    {
        Log.d("TTT","Fragment delete");
        //SP
        loginUser = new LoginUser(getActivity());
        if (loginUser.getUserData() != null) {
            schoolId = loginUser.getUserData().getSchoolId();
            Log.d("TTT","School id: "+schoolId);
        }

        //utils
        utils = new AllMethods();

        //rv
        rvDeletePost = view.findViewById(R.id.rvDeletePost);

        //lv
        lvNoData = view.findViewById(R.id.lvNoData);

        getDeletePostUser(schoolId);

    }

    public void getDeletePostUser(String id) {
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetDeleteList> userPost = retrofitClass.deletePostUser(String.valueOf(schoolId));
        userPost.enqueue(new Callback<GetDeleteList>() {
            @Override
            public void onResponse(Call<GetDeleteList> call, Response<GetDeleteList> response) {

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    deleteListArrayList = new ArrayList<>();
                    if (response.body().getDeleteList().size() > 0) {
                        rvDeletePost.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        deleteListArrayList.addAll(response.body().getDeleteList());

                        staffDeletedPostUserListAdapter = new StaffDeletedPostUserListAdapter(deleteListArrayList, getActivity(), new StaffDeletedPostUserListAdapter.ListAdapterListener() {
                            @Override
                            public void onClickAtOKButton(int position) {
                                Log.d("TTT","position::: "+position);
                                if(position==0)
                                {
                                    lvNoData.setVisibility(View.VISIBLE);
                                    rvDeletePost.setVisibility(View.GONE);
                                }
                            }
                        });
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                        rvDeletePost.setLayoutManager(mLayoutManager);
                        rvDeletePost.setAdapter(staffDeletedPostUserListAdapter);
                    } else {
                        rvDeletePost.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10")) {
                    Toast.makeText(getActivity(),response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(getActivity());
                    loginSP.clearData();

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                }
                else {
                    rvDeletePost.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                    // utils.setAlert(getActivity(),"",response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<GetDeleteList> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                utils.setAlert(getActivity(), "", t.getMessage());
            }
        });
    }
//
//    @Override
//    public void onClickAtOKButton(int position) {
//        Log.d("TTT","OkButton: "+position);
//    }
}

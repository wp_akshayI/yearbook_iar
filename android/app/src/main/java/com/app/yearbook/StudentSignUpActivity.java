package com.app.yearbook;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.model.StaffStudent;
import com.app.yearbook.model.StudentSignUP;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentSignUpActivity extends AppCompatActivity implements View.OnClickListener{

    private ProgressDialog progressDialog;
    private EditText etFirstName,etLastName,etEmail,etPassword,etAccessCode,etStudentId,etSchoolname;
    private Button btnNext,btnSignUP;
    private AllMethods allMethods;
    private TextView tvSignIn;
    private LinearLayout lvFirstLayout,lvSecondLayout,lvSchoolName;
    private View lvMain;
    private String schoolName,schoolId;
    public static final int SUB_ACTIVITY_CREATE_USER = 10;
    private Toolbar toolbar;
    private LoginUser loginUserSP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_sign_up);

        //all control
        setView();
    }

    public void setView()
    {
        //set toolbar
        setToolbar();

        //SP
        loginUserSP=new LoginUser(StudentSignUpActivity.this);

        //all methods object
        allMethods=new AllMethods();

        //all edit text
        etFirstName=findViewById(R.id.etFirstName);
        etLastName=findViewById(R.id.etLastName);
        etEmail=findViewById(R.id.etEmail);
        etPassword=findViewById(R.id.etPassword);
        etSchoolname=findViewById(R.id.etSchoolname);
        etStudentId=findViewById(R.id.etStudentcode);
        etAccessCode=findViewById(R.id.etAccesscode);

        //btn
        btnNext=findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);

        btnSignUP=findViewById(R.id.btnSignup);
        btnSignUP.setOnClickListener(this);

        //text view
        tvSignIn=findViewById(R.id.tvSignIn);
        tvSignIn.setOnClickListener(this);

        //linear view
        lvFirstLayout=findViewById(R.id.lvFirstLayout);
        lvSecondLayout=findViewById(R.id.lvSecondLayout);
        lvSchoolName=findViewById(R.id.lvSchoolName);
        lvSchoolName.setOnClickListener(this);
        lvMain=findViewById(R.id.lvMain);

    }

    public void setToolbar()
    {
        toolbar=findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Student SignUp");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(6f);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SUB_ACTIVITY_CREATE_USER && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                schoolName = extras.getString("schoolName");
                schoolId = extras.getString("schoolId");

                Log.d("TTT","Intent data: "+schoolName+" / "+schoolId);
                etSchoolname.setText(schoolName);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public boolean checkValidationFirstLv()
    {
        boolean result=false;
        if(etFirstName.getText().toString().trim().equals(""))
        {
            result=false;
            allMethods.setAlert(StudentSignUpActivity.this,getResources().getString(R.string.app_name),"Please enter first name");
        }
        else if(etLastName .getText().toString().trim().equals(""))
        {
            result=false;
            allMethods.setAlert(StudentSignUpActivity.this,getResources().getString(R.string.app_name),"Please enter last name");
        }
        else if(etEmail.getText().toString().trim().equals(""))
         {
             allMethods.setAlert(StudentSignUpActivity.this,getResources().getString(R.string.app_name),"Please enter email address");
             result=false;
         }
        else if(!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches())
         {
             allMethods.setAlert(StudentSignUpActivity.this,getResources().getString(R.string.app_name),"Please enter valid email address");
             result=false;
         }
        else if(etPassword.getText().toString().trim().equals(""))
        {
            allMethods.setAlert(StudentSignUpActivity.this,getResources().getString(R.string.app_name),"Please enter profile password");
            result=false;
        }
        else
        {
            result=true;
        }

        return result;
    }

    public boolean checkValidationSecondLv()
    {
        boolean result=false;
        if(etSchoolname.getText().toString().trim().equals(""))
        {
            result=false;
            allMethods.setAlert(StudentSignUpActivity.this,getResources().getString(R.string.app_name),"Please enter school name");
        }
        else if(etStudentId.getText().toString().trim().equals(""))
        {
            result=false;
            allMethods.setAlert(StudentSignUpActivity.this,getResources().getString(R.string.app_name),"Please enter Student no.");
        }
        else if(etAccessCode.getText().toString().equals(""))
        {
            allMethods.setAlert(StudentSignUpActivity.this,getResources().getString(R.string.app_name),"Please enter access code");
            result=false;
        }

        else
        {
            result=true;
        }

        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Log.d("TTT","OnBack");
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(lvSecondLayout.getVisibility()==View.VISIBLE)
        {
            lvSecondLayout.setVisibility(View.GONE);
            lvFirstLayout.setVisibility(View.VISIBLE);
            Log.d("TTT","second gone");
        }
        else if(lvFirstLayout.getVisibility()==View.VISIBLE) {

            Log.d("TTT","first visible");
            finish();
            super.onBackPressed();
        }
    }

    //Student SignUp API
    public void studentSignUp()
    {
        //Progress bar
        progressDialog= ProgressDialog.show(StudentSignUpActivity.this,"","",true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar=progressDialog.findViewById(R.id.progress);
        Circle bounce=new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        String token ="";
        token = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("Token", "");
        Log.d("TTT", "token: " + token);
        //simulatetoken

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<StudentSignUP> studentSignUp = retrofitClass.studentSignUp(etFirstName.getText().toString(),etLastName.getText().toString(),etEmail.getText().toString(),etPassword.getText().toString(),Integer.parseInt(schoolId), Integer.parseInt(etStudentId.getText().toString()),etAccessCode.getText().toString(),"android",token);
        studentSignUp.enqueue(new Callback<StudentSignUP>() {
            @Override
            public void onResponse(Call<StudentSignUP> call, Response<StudentSignUP> response) {
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
                if(response.body().getResponseCode().equals("1") && response.body().getResult().equalsIgnoreCase("true"))
                {
                    StaffStudent staffUSer=new StaffStudent();
                    staffUSer.setUserFirstname(response.body().getData().getUserFirstname());
                    staffUSer.setUserLastname(response.body().getData().getUserLastname());
                    staffUSer.setUserEmail(response.body().getData().getUserEmail());
                    staffUSer.setUserPassword(response.body().getData().getUserPassword());
                    staffUSer.setSchoolId(response.body().getData().getSchoolId());
                    staffUSer.setUserStudentId(response.body().getData().getUserStudentId());
                    staffUSer.setUserAccessCode(response.body().getData().getUserAccessCode());
                    staffUSer.setUserDeviceType(response.body().getData().getUserDeviceType());
                    staffUSer.setUserDeviceToken(response.body().getData().getUserDeviceToken());
                    staffUSer.setUserType("student");

                    loginUserSP.setUserData(staffUSer);
                    Log.d("TTT","SP: "+loginUserSP.getUserData().getUserType());

                    Snackbar.make(lvMain, response.body().getResponseMsg(),Snackbar.LENGTH_SHORT).show();
                    Intent i=new Intent(StudentSignUpActivity.this,StudentHomeActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    allMethods.setAlert(StudentSignUpActivity.this,"",response.body().getResponseMsg()+"");
                }
            }

            @Override
            public void onFailure(Call<StudentSignUP> call, Throwable t) {
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StudentSignUpActivity.this,"",t.getMessage()+"");
                //Snackbar.make(lvMain, t.getMessage()+"",Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.tvSignIn)
        {
            finish();
        }
        else if(v.getId()==R.id.btnNext)
        {
            if(checkValidationFirstLv())
            {
                lvFirstLayout.setVisibility(View.GONE);
                lvSecondLayout.setVisibility(View.VISIBLE);
            }
        }
        else if(v.getId()==R.id.btnSignup)
        {
            if(checkValidationSecondLv())
            {
                studentSignUp();
            }
        }
        else if(v.getId()==R.id.lvSchoolName)
        {
            Intent i=new Intent(getApplicationContext(),SearchSchoolActivity.class);
            startActivityForResult(i, SUB_ACTIVITY_CREATE_USER);
        }
    }
}

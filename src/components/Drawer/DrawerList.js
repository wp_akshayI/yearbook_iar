import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import { ListItemIcon } from '@material-ui/core'

function DrawerList ({ id, order, title, left, right, divider, action, ...other }) {
  const classes = useStyles()

  return (
    <>
      <ListItem
        button
        className={divider === false ? null : classes.ListItemBorder}
        classes={{ root: classes.ListItem }}
        {...other}
      >
        {
          left && (
            <ListItemIcon classes={{ root: classes.icon_root }}>
              {left}
            </ListItemIcon>
          )
        }

        <ListItemText
          classes={{ primary: classes.ListItemText }}
          primary={title}
          onClick={() => action ? action() : null}
        />

        {
          right && (
            <ListItemSecondaryAction classes={{ root: classes.action_root }}>
              {right}
            </ListItemSecondaryAction>
          )
        }

      </ListItem>
    </>
  )
}

export default DrawerList

const useStyles = makeStyles(theme => ({
  ListItemAction: {
    color: '#fff',
    fontFamily: 'calibri',
    fontSize: 14,
    textTransform: 'capitalize'
  },
  ListItemText: {
    color: '#c9c9c9',
    fontFamily: 'calibri',
    fontSize: 14
  },
  ListItemBorder: {
    borderBottomWidth: 1,
    borderStyle: 'solid',
    borderColor: '#b2b2b2'
  },
  ListItem: {
    paddingTop: 10,
    paddingBottom: 12
  },
  icon_root: {
    minWidth: 'unset'
  },
  action_root: {
    right: 5
  }
}))

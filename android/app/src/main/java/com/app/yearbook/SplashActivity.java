package com.app.yearbook;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.app.yearbook.model.CheckUserStatus;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1500;
    private LoginUser loginUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        Test code React
        /*Intent i = new Intent(SplashActivity.this, MyReactActivity.class);
        startActivity(i);
        return;*/
//        Test code React end

        //set all control
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setView();
            }
        }, 500);
    }

    public void setView() {
        //SP object
        loginUser = new LoginUser(SplashActivity.this);

        if (loginUser.getUserData() != null) {
            Log.d("TTT", "setView: " + loginUser.getUserData().getUserType());

            if (loginUser.getUserData().getSchoolId() == null) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loginUser.clearData();
                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                }, SPLASH_TIME_OUT);
//            } else if (loginUser.isUserStudent()
            }/* else if (loginUser.getUserData().getUserType().equalsIgnoreCase("student")
                    && loginUser.getUserData().getIsAccessCodeVerify().equalsIgnoreCase("yes")
                    && !loginUser.getUserData().isArchived()
                    && !loginUser.getUserData().isYearbookPurchased()) {
                Intent intent = new Intent(this, UserProcessWithWeb.class);
                intent.putExtra("action", Constants.USER_WEB_PAYMENT);
                startActivity(intent);
                finish();
            }*/ else if (loginUser.getUserData().getUserType().equalsIgnoreCase("student") || loginUser.getUserData().getUserType().equalsIgnoreCase("yb_student")) {
                checkUserStatus(loginUser.getUserData().getUserId());
            } else if (loginUser.getUserData().getUserType().equalsIgnoreCase("staff")) {
                chkStaffStatus(loginUser.getUserData().getStaffId());
            } else if (loginUser.getUserData().getUserType().equalsIgnoreCase("employee")) {

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        Intent i = new Intent(getApplicationContext(), EmployeeHomeActivity.class);
                        startActivity(i);
                        finish();
                    }
                }, SPLASH_TIME_OUT);


            } else {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                }, SPLASH_TIME_OUT);
            }
        } else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }

    //49.34.134.20
    public void checkUserStatus(String userID) {

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<CheckUserStatus> status = retrofitClass.checkStatus(Integer.parseInt(userID));
        status.enqueue(new Callback<CheckUserStatus>() {
            @Override
            public void onResponse(Call<CheckUserStatus> call, Response<CheckUserStatus> response) {
                Log.d("TTT", "Splashhhh: " + response.body().getResponseCode());
                if (response.body().getResponseCode().equals("1")) {

                    if (response.body().getFlag().equals("1")) {
                        checkTerms();
                    } else {
                        Intent i = new Intent(getApplicationContext(), UserBlockActivity.class);
                        i.putExtra("Msg", "block");
                        startActivity(i);
                        finish();
                    }
                } else if (response.body().getResponseCode().equals("2")) {
                    if (response.body().getResponseMsg().equalsIgnoreCase("No user available")) {
                        loginUser.clearData();
                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                        return;
                    }
                    if (response.body().getFlag().equals("1")) {
                        checkTerms();
                    } else {
                        Intent i = new Intent(getApplicationContext(), UserBlockActivity.class);
                        i.putExtra("Msg", "block");
                        startActivity(i);
                        finish();
                    }
                } else if (response.body().getResponseCode().equals("3")) {
                    //school is deleted of student
                    Intent i = new Intent(getApplicationContext(), UserBlockActivity.class);
                    i.putExtra("Msg", "school");
                    startActivity(i);
                    finish();
                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(SplashActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckUserStatus> call, Throwable t) {
                Toast.makeText(SplashActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    public void checkTerms() {
        if (loginUser.getUserData().getUserIsAgree().equals("2")) {
            Intent i = new Intent(getApplicationContext(), TermsConditionActivity.class);
            startActivity(i);
            finish();
        } else {
            Intent i = new Intent(getApplicationContext(), StudentHomeActivity.class);
            startActivity(i);
            finish();
            /*if (!loginUser.getUserData().isYearbookPurchased()){
                Intent intent = new Intent(getApplicationContext(), UserProcessWithWeb.class);
                intent.putExtra("action", Constants.USER_WEB_PAYMENT);
                startActivity(intent);
            }else {
                Intent i = new Intent(getApplicationContext(), StudentHomeActivity.class);
                startActivity(i);
                finish();
            }*/
        }
    }

    public void chkStaffStatus(String staffID) {
        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<CheckUserStatus> status = retrofitClass.checkStaffStatus(Integer.parseInt(staffID));
        status.enqueue(new Callback<CheckUserStatus>() {
            @Override
            public void onResponse(Call<CheckUserStatus> call, Response<CheckUserStatus> response) {
                if (response.body().getResponseCode().equals("1")) {
                    if (response.body().getFlag().equals("1")) {
                        checkTermsStaff();
                    } else {
                        Intent i = new Intent(getApplicationContext(), UserBlockActivity.class);
                        i.putExtra("Msg", "block");
                        startActivity(i);
                        finish();
                    }
                } else if (response.body().getResponseCode().equals("2")) {
                    //school is deleted of staff
                    Intent i = new Intent(getApplicationContext(), UserBlockActivity.class);
                    i.putExtra("Msg", "school");
                    startActivity(i);
                    finish();
                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(SplashActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(SplashActivity.this, "" + response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CheckUserStatus> call, Throwable t) {
                Toast.makeText(SplashActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    public void checkTermsStaff() {
        Log.d("TTT", "checkTermsStaff: " + loginUser.getUserData().getStaffIsAgree());
        if (loginUser.getUserData().getStaffIsAgree().equals("2")) {
            Intent i = new Intent(getApplicationContext(), TermsConditionStaffActivity.class);
            startActivity(i);
            finish();
        } else {
            Intent i = new Intent(getApplicationContext(), StaffHomeActivity.class);
            startActivity(i);
            finish();
        }
    }

}


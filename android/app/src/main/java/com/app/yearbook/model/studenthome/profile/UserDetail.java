package com.app.yearbook.model.studenthome.profile;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class UserDetail{

	@SerializedName("user_device_token")
	private String userDeviceToken;

	@SerializedName("parent_email")
	private String parentEmail;

	@SerializedName("user_password")
	private String userPassword;

	@SerializedName("user_access_code")
	private String userAccessCode;

	@SerializedName("user_device_type")
	private String userDeviceType;

	@SerializedName("user_is_suspend")
	private String userIsSuspend;

	@SerializedName("user_lastname")
	private String userLastname;

	@SerializedName("user_address")
	private String userAddress;

	@SerializedName("user_created")
	private String userCreated;

	@SerializedName("user_grade")
	private String userGrade;

	@SerializedName("school_city")
	private String schoolCity;

	@SerializedName("school_end_date")
	private String schoolEndDate;

	@SerializedName("user_year")
	private String userYear;

	@SerializedName("school_start_date")
	private String schoolStartDate;

	@SerializedName("password")
	private String password;

	@SerializedName("user_type")
	private String userType;

	@SerializedName("school_is_deleted")
	private String schoolIsDeleted;

	@SerializedName("school_safety")
	private String schoolSafety;

	@SerializedName("school_id")
	private String schoolId;

	@SerializedName("is_access_code_verify")
	private String isAccessCodeVerify;

	@SerializedName("school_address")
	private String schoolAddress;

	@SerializedName("user_student_id")
	private String userStudentId;

	@SerializedName("school_phone")
	private String schoolPhone;

	@SerializedName("user_phone")
	private String userPhone;

	@SerializedName("user_token")
	private String userToken;

	@SerializedName("email")
	private String email;

	@SerializedName("user_status")
	private String userStatus;

	@SerializedName("parent_lastname")
	private String parentLastname;

	@SerializedName("user_email")
	private String userEmail;

	@SerializedName("user_is_agree")
	private String userIsAgree;

	@SerializedName("user_image")
	private String userImage;

	@SerializedName("school_status")
	private String schoolStatus;

	@SerializedName("user_firstname")
	private String userFirstname;

	@SerializedName("user_registered")
	private String userRegistered;

	@SerializedName("school_name")
	private String schoolName;

	@SerializedName("admin_type")
	private String adminType;

	@SerializedName("school_created")
	private String schoolCreated;

	@SerializedName("school_state")
	private String schoolState;

	@SerializedName("user_birthdate")
	private String userBirthdate;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("parent_phone")
	private String parentPhone;

	@SerializedName("parent_firstname")
	private String parentFirstname;

	@SerializedName("school_is_suspend")
	private String schoolIsSuspend;

	@SerializedName("deleted_time")
	private String deletedTime;

	@SerializedName("yearbook_price")
	private String yearbookPrice;

	@SerializedName("user_images")
	private List<String> userImages;

	public void setUserDeviceToken(String userDeviceToken){
		this.userDeviceToken = userDeviceToken;
	}

	public String getUserDeviceToken(){
		return userDeviceToken;
	}

	public void setParentEmail(String parentEmail){
		this.parentEmail = parentEmail;
	}

	public String getParentEmail(){
		return parentEmail;
	}

	public void setUserPassword(String userPassword){
		this.userPassword = userPassword;
	}

	public String getUserPassword(){
		return userPassword;
	}

	public void setUserAccessCode(String userAccessCode){
		this.userAccessCode = userAccessCode;
	}

	public String getUserAccessCode(){
		return userAccessCode;
	}

	public void setUserDeviceType(String userDeviceType){
		this.userDeviceType = userDeviceType;
	}

	public String getUserDeviceType(){
		return userDeviceType;
	}

	public void setUserIsSuspend(String userIsSuspend){
		this.userIsSuspend = userIsSuspend;
	}

	public String getUserIsSuspend(){
		return userIsSuspend;
	}

	public void setUserLastname(String userLastname){
		this.userLastname = userLastname;
	}

	public String getUserLastname(){
		return userLastname;
	}

	public void setUserAddress(String userAddress){
		this.userAddress = userAddress;
	}

	public String getUserAddress(){
		return userAddress;
	}

	public void setUserCreated(String userCreated){
		this.userCreated = userCreated;
	}

	public String getUserCreated(){
		return userCreated;
	}

	public void setUserGrade(String userGrade){
		this.userGrade = userGrade;
	}

	public String getUserGrade(){
		return userGrade;
	}

	public void setSchoolCity(String schoolCity){
		this.schoolCity = schoolCity;
	}

	public String getSchoolCity(){
		return schoolCity;
	}

	public void setSchoolEndDate(String schoolEndDate){
		this.schoolEndDate = schoolEndDate;
	}

	public String getSchoolEndDate(){
		return schoolEndDate;
	}

	public void setUserYear(String userYear){
		this.userYear = userYear;
	}

	public String getUserYear(){
		return userYear;
	}

	public void setSchoolStartDate(String schoolStartDate){
		this.schoolStartDate = schoolStartDate;
	}

	public String getSchoolStartDate(){
		return schoolStartDate;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setSchoolIsDeleted(String schoolIsDeleted){
		this.schoolIsDeleted = schoolIsDeleted;
	}

	public String getSchoolIsDeleted(){
		return schoolIsDeleted;
	}

	public void setSchoolSafety(String schoolSafety){
		this.schoolSafety = schoolSafety;
	}

	public String getSchoolSafety(){
		return schoolSafety;
	}

	public void setSchoolId(String schoolId){
		this.schoolId = schoolId;
	}

	public String getSchoolId(){
		return schoolId;
	}

	public void setIsAccessCodeVerify(String isAccessCodeVerify){
		this.isAccessCodeVerify = isAccessCodeVerify;
	}

	public String getIsAccessCodeVerify(){
		return isAccessCodeVerify;
	}

	public void setSchoolAddress(String schoolAddress){
		this.schoolAddress = schoolAddress;
	}

	public String getSchoolAddress(){
		return schoolAddress;
	}

	public void setUserStudentId(String userStudentId){
		this.userStudentId = userStudentId;
	}

	public String getUserStudentId(){
		return userStudentId;
	}

	public void setSchoolPhone(String schoolPhone){
		this.schoolPhone = schoolPhone;
	}

	public String getSchoolPhone(){
		return schoolPhone;
	}

	public void setUserPhone(String userPhone){
		this.userPhone = userPhone;
	}

	public String getUserPhone(){
		return userPhone;
	}

	public void setUserToken(String userToken){
		this.userToken = userToken;
	}

	public String getUserToken(){
		return userToken;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setUserStatus(String userStatus){
		this.userStatus = userStatus;
	}

	public String getUserStatus(){
		return userStatus;
	}

	public void setParentLastname(String parentLastname){
		this.parentLastname = parentLastname;
	}

	public String getParentLastname(){
		return parentLastname;
	}

	public void setUserEmail(String userEmail){
		this.userEmail = userEmail;
	}

	public String getUserEmail(){
		return userEmail;
	}

	public void setUserIsAgree(String userIsAgree){
		this.userIsAgree = userIsAgree;
	}

	public String getUserIsAgree(){
		return userIsAgree;
	}

	public void setUserImage(String userImage){
		this.userImage = userImage;
	}

	public String getUserImage(){
		return userImage;
	}

	public void setSchoolStatus(String schoolStatus){
		this.schoolStatus = schoolStatus;
	}

	public String getSchoolStatus(){
		return schoolStatus;
	}

	public void setUserFirstname(String userFirstname){
		this.userFirstname = userFirstname;
	}

	public String getUserFirstname(){
		return userFirstname;
	}

	public void setUserRegistered(String userRegistered){
		this.userRegistered = userRegistered;
	}

	public String getUserRegistered(){
		return userRegistered;
	}

	public void setSchoolName(String schoolName){
		this.schoolName = schoolName;
	}

	public String getSchoolName(){
		return schoolName;
	}

	public void setAdminType(String adminType){
		this.adminType = adminType;
	}

	public String getAdminType(){
		return adminType;
	}

	public void setSchoolCreated(String schoolCreated){
		this.schoolCreated = schoolCreated;
	}

	public String getSchoolCreated(){
		return schoolCreated;
	}

	public void setSchoolState(String schoolState){
		this.schoolState = schoolState;
	}

	public String getSchoolState(){
		return schoolState;
	}

	public void setUserBirthdate(String userBirthdate){
		this.userBirthdate = userBirthdate;
	}

	public String getUserBirthdate(){
		return userBirthdate;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setParentPhone(String parentPhone){
		this.parentPhone = parentPhone;
	}

	public String getParentPhone(){
		return parentPhone;
	}

	public void setParentFirstname(String parentFirstname){
		this.parentFirstname = parentFirstname;
	}

	public String getParentFirstname(){
		return parentFirstname;
	}

	public void setSchoolIsSuspend(String schoolIsSuspend){
		this.schoolIsSuspend = schoolIsSuspend;
	}

	public String getSchoolIsSuspend(){
		return schoolIsSuspend;
	}

	public void setDeletedTime(String deletedTime){
		this.deletedTime = deletedTime;
	}

	public String getDeletedTime(){
		return deletedTime;
	}

	public void setYearbookPrice(String yearbookPrice){
		this.yearbookPrice = yearbookPrice;
	}

	public String getYearbookPrice(){
		return yearbookPrice;
	}

	public void setUserImages(List<String> userImages){
		this.userImages = userImages;
	}

	public List<String> getUserImages(){
		return userImages;
	}
}
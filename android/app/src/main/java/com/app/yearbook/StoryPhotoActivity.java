package com.app.yearbook;

import android.graphics.Bitmap;
import android.os.StrictMode;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.app.yearbook.utils.ZoomableImageView;

public class StoryPhotoActivity extends AppCompatActivity {

    private String fileUrl;
    private ProgressBar progressBar;
    private ZoomableImageView touch;
    private LinearLayout lvMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

    }

    public void setView()
    {

        lvMain=findViewById(R.id.lvMain);
       // lvMain.setBackgroundColor(getResources().getColor(R.color.white));

        if(getIntent().getExtras()!=null)
        {
            fileUrl=getIntent().getStringExtra("FileUrl");

            touch = findViewById(R.id.imgPost);
            progressBar=findViewById(R.id.progress);

            Glide.with(StoryPhotoActivity.this)
                    .load(fileUrl)
                    .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.mipmap.home_post_img_placeholder)
                    .error(R.mipmap.home_post_img_placeholder)
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(touch);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Image not found",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}

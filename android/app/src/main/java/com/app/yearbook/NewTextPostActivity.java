package com.app.yearbook;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.app.yearbook.databinding.ActivityNewTextPostBinding;
import com.app.yearbook.model.allpost.TextPost;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NewTextPostActivity extends AppCompatActivity {

    ActivityNewTextPostBinding binding;
    TextPost textPost;
    PostListItem postListItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_text_post);
        setToolbar();
        if (getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false)){
            postListItem = getIntent().getParcelableExtra(Constants.INTENT_DATA);
            textPost = new TextPost(postListItem.getPostTitle(), postListItem.getPostSubTitle(), postListItem.getPostDescription(), "");
            binding.setModel(textPost);
            binding.appbar.toolbarTitle.setText(getString(R.string.edit_post));
        }else {
            textPost = new TextPost("", "", "", "");
            binding.setModel(textPost);
        }
    }

    private void setToolbar() {
        setSupportActionBar(binding.appbar.toolbar);
        binding.appbar.toolbarTitle.setText(getString(R.string.new_post));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_next:
                validate();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void validate() {
        if (TextUtils.isEmpty(textPost.getSubject())) {
            binding.edtSubject.setError("Please add subject");
        } else if (TextUtils.isEmpty(textPost.getComposedPost())) {
            binding.edtComposePost.setError("Please add message");
        } else {
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);

            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            String formattedDate = df.format(c);

            textPost.setDate(formattedDate);
            Intent intent = new Intent(this, PostPreviewActivity.class);
            intent.putExtra(Constants.INTENT_ISEDIT, getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false));
            intent.putExtra(Constants.INTENT_DATA, postListItem);
            intent.putExtra(Constants.POST_TEXT, textPost);
            intent.putExtra(Constants.POST_TYPE, Constants.POST_TEXT);
            startActivityForResult(intent, 111);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_next, menu);
        return super.onCreateOptionsMenu(menu);
    }


}

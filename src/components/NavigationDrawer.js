import React from 'react';
import { withRouter } from "react-router-dom";
import { useSelector } from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import NavigationHeader from './NavigationHeader';
import ListSubheader from '@material-ui/core/ListSubheader';
import DrawerList from './Drawer/DrawerList';
import CategoriesDrawerItem from "./Drawer/AppDrawer/CategoriesDrawerItem";

const drawerWidth = 260;

function NavigationDrawer({ history, children }) {
    const { yearbookDetails } = useSelector(({ yearbook }) => yearbook);
    const classes = useStyles();

    const _navigateEditor = () => {
        if(!yearbookDetails) return;
        const { screen_id, name } = yearbookDetails;
        return history.push({ pathname: "/app/page-editor", search: "_id=" + screen_id + "&_name=" + name })
    }

    return (
        <div className={classes.root}>
            <div>
                <NavigationHeader />
                {/* <CssBaseline /> */}
                <Drawer
                    className={classes.drawer}
                    variant="permanent"
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <div className={classes.toolbar} />
                    <ListSubheader
                        className={classes.ListSubheader}>
                        Homepage & Category Pages
                    </ListSubheader>

                    <DrawerList
                        title="Yearbook Homepage"
                        onClick={_navigateEditor}
                    />
                    <CategoriesDrawerItem />
                </Drawer>
            </div>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                {children}
            </main>
        </div>
    );
}

export default withRouter(NavigationDrawer)

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
        backgroundColor: "#3F3F3F"
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    toolbar: theme.mixins.toolbar,
    ListSubheader: {
        color: "#868686",
        fontSize: 13,
        borderBottomWidth: 1.5,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderStyle: "solid",
        borderColor: "#b2b2b2",
        display: "flex",
        alignItems: "center",
    },

}));
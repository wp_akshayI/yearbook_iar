package com.app.yearbook.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.makeramen.roundedimageview.RoundedImageView;
import com.app.yearbook.R;
import com.app.yearbook.model.studenthome.Comment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import ru.rambler.libs.swipe_layout.SwipeLayout;

public class StudentCommentListAdapter extends RecyclerView.Adapter<StudentCommentListAdapter.MyViewHolder> {
    private ArrayList<Comment> commentLists;
    private Activity ctx;
    private commentListner listner;
    private String getCommentFrom,userId,flag="";

    public interface commentListner
    {
        void onClickAtButton(int position,String flag);
    }

    @NonNull
    @Override
    public StudentCommentListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_studentcomment_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final StudentCommentListAdapter.MyViewHolder holder, final int position) {
        final Comment userList=commentLists.get(position);
        holder.tvUserComment.setText(userList.getCommentMessage()+"");
        holder.tvUserName.setText(userList.getUserFirstname()+"  "+userList.getUserLastname());
        holder.tvUserTime.setText(userList.getCommentCreated());

        Glide.with(ctx)
                .load(userList.getUserImage())
                .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.profile_placeholder)
                .error(R.mipmap.profile_placeholder)
                .into(holder.imgUserPhoto);

        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("CST6CDT"));

        Date date = null;
        try {

            date = sdf.parse(userList.getCommentCreated());
            sdf.setTimeZone(TimeZone.getDefault());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat sameDateSDF=new SimpleDateFormat("dd-MM-yyyy");
        sameDateSDF.setTimeZone(TimeZone.getTimeZone("CST6CDT"));

        sameDateSDF.setTimeZone(TimeZone.getDefault());
        String DBDate = sameDateSDF.format(date);
        String currentDate=sameDateSDF.format(new Date());
        Log.d("TTT","Date: "+DBDate+" / "+currentDate);

        if(DBDate.equalsIgnoreCase(currentDate))
        {
            //same date
            SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //dateFormat.setTimeZone(TimeZone.getTimeZone("CST"));
            Date currDate = new Date();

            //dateFormat.setTimeZone(TimeZone.getDefault());
            String dd = dateFormat.format(currDate);
            Log.d("TTT","Dateeeee: "+dd +" / "+DBDate);

            try {
                currDate=dateFormat.parse(dd);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long different = currDate.getTime() - date.getTime();
            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            Log.d("TTT","DDDD : "+elapsedDays+" / "+elapsedHours+" / "+elapsedMinutes+" / "+elapsedSeconds);

            if(elapsedHours<1)
            {
                holder.tvUserTime.setText(elapsedMinutes+"m");
            }
            else {
                holder.tvUserTime.setText(elapsedHours+"h");
            }
        }
        else
        {
            //not same
            String outputPattern = "MMM dd, yyyy -hh:mm aa";
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            String str = null;
            str = outputFormat.format(date);

            holder.tvUserTime.setText(str+"");
        }

        if(getCommentFrom.equalsIgnoreCase("staff"))
        {

            flag="staff";
            holder.swipeLayout.setSwipeEnabled(true);

        }
        else
        {
            if(userId.equalsIgnoreCase(userList.getUserId())) {
                flag="user";
                holder.swipeLayout.setSwipeEnabled(true);

            }
            else
            {
                holder.swipeLayout.setSwipeEnabled(false);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.swipeLayout.animateReset();
            }
        });

        holder.swipeLayout.setOnSwipeListener(new SwipeLayout.OnSwipeListener() {
            @Override
            public void onBeginSwipe(SwipeLayout swipeLayout, boolean moveToRight) {
            }

            @Override
            public void onSwipeClampReached(SwipeLayout swipeLayout, boolean moveToRight) {
                //removeItem(position);
                listner.onClickAtButton(Integer.parseInt(userList.getCommentId()),flag);

            }

            @Override
            public void onLeftStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {
            }

            @Override
            public void onRightStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {
            }
        });
    }

    public StudentCommentListAdapter(String userId,String getCommentFrom,ArrayList<Comment> commentLists, Activity context,commentListner listner) {
        this.commentLists = commentLists;
        this.ctx=context;
        this.listner=listner;
        this.getCommentFrom=getCommentFrom;
        this.userId=userId;
    }

    @Override
    public int getItemCount() {
        return commentLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvUserComment, tvUserName,tvUserTime;
        public RoundedImageView imgUserPhoto;
        public RelativeLayout lviewForeground,lviewBackground;
        SwipeLayout swipeLayout;

        public MyViewHolder(View view) {
            super(view);
            tvUserComment = view.findViewById(R.id.tvUserComment);
            tvUserName = view.findViewById(R.id.tvUserName);
            tvUserTime = view.findViewById(R.id.tvUserTime);
            imgUserPhoto=view.findViewById(R.id.imgUserImage);
            lviewBackground=view.findViewById(R.id.lviewBackground);
            lviewForeground=view.findViewById(R.id.lviewForeground);
            swipeLayout=view.findViewById(R.id.swipe);

        }
    }
}

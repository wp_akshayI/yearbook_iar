package com.app.yearbook.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.adapter.StaffPostListAdapter;
import com.app.yearbook.model.staff.livefeed.PostListItem;
import com.app.yearbook.model.staff.trash.GetReportedPost;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportedFragment extends Fragment implements SwipyRefreshLayout.OnRefreshListener {

    public View view;
    private ArrayList<PostListItem> getPostList;
    public ProgressDialog progressDialog;
    private LoginUser loginUser;
    public AllMethods utils;
    private RecyclerView rvReport;
    private int schoolId, userId, page;
    private LinearLayout lvNoData;
    private SwipyRefreshLayout swipeRefreshLayout;


    public ReportedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_reported, container, false);
        setHasOptionsMenu(true);
        setView();
        return view;
    }

    public void setView() {

        page = Integer.parseInt(getArguments().getString("page", ""));
        Log.d("TTT", "Fragment report: " + page);


        //SP
        loginUser = new LoginUser(getActivity());
        if (loginUser.getUserData() != null) {
            schoolId = Integer.parseInt(loginUser.getUserData().getSchoolId());
            userId = Integer.parseInt(loginUser.getUserData().getStaffId());
            Log.d("TTT", "School id: " + schoolId);
        }

        //utils
        utils = new AllMethods();

        //lv
        lvNoData = view.findViewById(R.id.lvNoData);

        //swipe layout
        swipeRefreshLayout = view.findViewById(R.id.swipyrefreshlayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        //rv
        rvReport = view.findViewById(R.id.rvDeletePost);

        getPostList = new ArrayList<>();
        getReport(schoolId, page);

    }


    public void getReport(int sid, int page) {

        Log.d("TTT", "Page: " + page);
        if (page == 1) {
            progressDialog = ProgressDialog.show(getActivity(), "", "", true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.progress_view);
            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
            Circle bounce = new Circle();
            bounce.setColor(Color.BLACK);
            progressBar.setIndeterminateDrawable(bounce);

            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
            final Call<GetReportedPost> userPost = retrofitClass.getReportedPost(sid, page);
            userPost.enqueue(new Callback<GetReportedPost>() {
                @Override
                public void onResponse(Call<GetReportedPost> call, Response<GetReportedPost> response) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    swipeRefreshLayout.setRefreshing(false);

                    if (response.body().getResponseCode().equals("1")) {

                        if (response.body().getPostList().size() > 0) {
                            swipeRefreshLayout.setVisibility(View.VISIBLE);
                            rvReport.setVisibility(View.VISIBLE);
                            lvNoData.setVisibility(View.GONE);

                            getPostList.addAll(response.body().getPostList());
                            StaffPostListAdapter postListAdapter = new StaffPostListAdapter(getPostList, getActivity(), userId, new StaffPostListAdapter.ListAdapterListener() {
                                @Override
                                public void onClickAtButton(int position) {
                                    if (position == 0) {
                                        lvNoData.setVisibility(View.VISIBLE);
                                        rvReport.setVisibility(View.GONE);
                                    }
                                }
                            });

                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            rvReport.setLayoutManager(mLayoutManager);
                            rvReport.setAdapter(postListAdapter);
                        } else {
                            swipeRefreshLayout.setVisibility(View.GONE);
                            rvReport.setVisibility(View.GONE);
                            lvNoData.setVisibility(View.VISIBLE);
                        }
                    } else if (response.body().getResponseCode().equals("10")) {
                        Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        LoginUser loginSP = new LoginUser(getActivity());
                        loginSP.clearData();

                        Intent i = new Intent(getActivity(), LoginActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        Log.d("TTT", "Reportedddd no data");
                        swipeRefreshLayout.setVisibility(View.GONE);
                        rvReport.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                        // utils.setAlert(getActivity(),"",response.body().getResponseMsg());
                    }

                }

                @Override
                public void onFailure(Call<GetReportedPost> call, Throwable t) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    utils.setAlert(getActivity(), "", t.getMessage());
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        } else {
            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
            final Call<GetReportedPost> userPost = retrofitClass.getReportedPost(sid, page);
            userPost.enqueue(new Callback<GetReportedPost>() {
                @Override
                public void onResponse(Call<GetReportedPost> call, Response<GetReportedPost> response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (response.body().getResponseCode().equals("1")) {
                        if (response.body().getPostList().size() > 0) {
                            getPostList.addAll(response.body().getPostList());
                            rvReport.getAdapter().notifyDataSetChanged();
                            Log.d("TTT", "Swipe else: " + getPostList.size());
                        }
                    } else if (response.body().getResponseCode().equals("10")) {
                        Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        LoginUser loginSP = new LoginUser(getActivity());
                        loginSP.clearData();

                        Intent i = new Intent(getActivity(), LoginActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        Log.d("TTT", "Reportedddd no data");
                        swipeRefreshLayout.setVisibility(View.GONE);
                        rvReport.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                        // utils.setAlert(getActivity(),"",response.body().getResponseMsg());
                    }

                }

                @Override
                public void onFailure(Call<GetReportedPost> call, Throwable t) {
                    utils.setAlert(getActivity(), "", t.getMessage());
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

            Log.d("TTT", "bottom swipeeee");
            swipeRefreshLayout.setRefreshing(true);
            page = page + 1;
            getReport(schoolId, page);
        }
    }
}

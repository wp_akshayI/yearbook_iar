import React from "react";
import { FormControl, InputLabel, OutlinedInput } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';

function InputText({ title, ...others }) {
    const classes = useStyles();
    return (
        <FormControl fullWidth variant="outlined">
            <InputLabel style={{ color: "#c9c9c9" }}>{title}</InputLabel>
            <OutlinedInput
                id="outlined-adornment-amount"
                inputlabelprops={{
                    shrink: true
                }}
                classes={{
                    notchedOutline: classes.notchedOutline
                }}
                labelWidth={60}
                style={{ color: "#fff" }}
                {...others}
            />
        </FormControl>
    )
}

export default InputText;

const useStyles = makeStyles(theme => ({
    notchedOutline: {
        borderColor: "#c9c9c9 !important"
    }
}));
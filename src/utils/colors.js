export const black = '#000000'
export const white = '#ffffff'
export const red = '#ff0000'

export const app_background = 'rgba(243.2, 243.2, 248.32, 1)'

export const Gray = '#3F3F3F'
export const DarkGray = '#A9A9A9'
export const LightGray = '#e9eef1'

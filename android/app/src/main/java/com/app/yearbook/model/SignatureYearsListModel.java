package com.app.yearbook.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SignatureYearsListModel {

    @SerializedName("signature_years_list")
    private List<String> signatureYearsList = null;
    @SerializedName("ResponseCode")
    private String responseCode;
    @SerializedName("ResponseMsg")
    private String responseMsg;
    @SerializedName("Result")
    private String result;
    @SerializedName("ServerTimeZone")
    private String serverTimeZone;
    @SerializedName("serverTime")
    private String serverTime;

    public List<String> getSignatureYearsList() {
        if (signatureYearsList == null)
            return new ArrayList<>();

        return signatureYearsList;
    }

    public void setSignatureYearsList(List<String> signatureYearsList) {
        this.signatureYearsList = signatureYearsList;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getServerTimeZone() {
        return serverTimeZone;
    }

    public void setServerTimeZone(String serverTimeZone) {
        this.serverTimeZone = serverTimeZone;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }
}

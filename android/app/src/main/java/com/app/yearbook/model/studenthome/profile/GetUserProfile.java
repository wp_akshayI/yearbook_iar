package com.app.yearbook.model.studenthome.profile;

import com.google.gson.annotations.SerializedName;

public class GetUserProfile{

	@SerializedName("ResponseCode")
	private String responseCode;

	@SerializedName("ServerTimeZone")
	private String serverTimeZone;

	@SerializedName("ResponseMsg")
	private String responseMsg;

	@SerializedName("serverTime")
	private String serverTime;

	@SerializedName("user_profile")
	private UserProfile userProfile;

	@SerializedName("Result")
	private String result;

	public void setResponseCode(String responseCode){
		this.responseCode = responseCode;
	}

	public String getResponseCode(){
		return responseCode;
	}

	public void setServerTimeZone(String serverTimeZone){
		this.serverTimeZone = serverTimeZone;
	}

	public String getServerTimeZone(){
		return serverTimeZone;
	}

	public void setResponseMsg(String responseMsg){
		this.responseMsg = responseMsg;
	}

	public String getResponseMsg(){
		return responseMsg;
	}

	public void setServerTime(String serverTime){
		this.serverTime = serverTime;
	}

	public String getServerTime(){
		return serverTime;
	}

	public void setUserProfile(UserProfile userProfile){
		this.userProfile = userProfile;
	}

	public UserProfile getUserProfile(){
		return userProfile;
	}

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}
}
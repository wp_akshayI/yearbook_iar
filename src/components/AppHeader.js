import React from 'react'
import { View, Text, Image } from 'react-native'
import { HeaderBackButton, Header } from 'react-navigation-stack'
import { menuIcon } from '../assets'
import { openDrawer } from '../Native'
import { colors } from '../utils'

export default function AppHeader () {
  return (
    <View style={{ alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', backgroundColor: colors.white, height: Header.HEIGHT, paddingLeft: 15 }}>
      <HeaderBackButton
        onPress={() => { openDrawer() }}
        backImage={(
          <Image
            source={menuIcon}
            style={{ height: 20, width: 20 }}
            resizeMode='contain'
          />
        )}
      />
      <Text style={{ color: colors.black, fontSize: 18, fontFamily: 'calibri' }}>Yearbook</Text>
      <View style={{ height: 35, width: 35 }} />
    </View>
  )
}

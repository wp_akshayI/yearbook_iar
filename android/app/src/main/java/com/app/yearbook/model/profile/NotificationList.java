package com.app.yearbook.model.profile;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationList implements Serializable {

    @SerializedName("sender_type")
    @Expose
    private String senderType;
    @SerializedName("receiver_type")
    @Expose
    private String receiverType;



    @SerializedName("notification_id")
    @Expose
    private String notificationId;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("sender_id")
    @Expose
    private String senderId;
    @SerializedName("receiver_id")
    @Expose
    private String receiverId;
    @SerializedName("notification_message")
    @Expose
    private String notificationMessage;
    @SerializedName("notification_type")
    @Expose
    private String notificationType;
    @SerializedName("is_accept")
    @Expose
    private String isAccept;
    @SerializedName("notification_created")
    @Expose
    private String notificationCreated;
    @SerializedName("yearbook_post_id")
    @Expose
    private String yearbookPostId;
    @SerializedName("yearbook_staff_id")
    @Expose
    private String yearbookStaffId;
    @SerializedName("yearbook_school_id")
    @Expose
    private String yearbookSchoolId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("yearbook_post_description")
    @Expose
    private String yearbookPostDescription;
    @SerializedName("yearbook_file")
    @Expose
    private String yearbookFile;
    @SerializedName("yearbook_post_thumbnail")
    @Expose
    private String yearbookPostThumbnail;
    @SerializedName("yearbook_post_file_type")
    @Expose
    private String yearbookPostFileType;
    @SerializedName("yearbook_post_file_size_type")
    @Expose
    private String yearbookPostFileSizeType;
    @SerializedName("yearbook_post_file_position")
    @Expose
    private String yearbookPostFilePosition;
    @SerializedName("yearbook_date")
    @Expose
    private String yearbookDate;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    /*@SerializedName("user_firstname")
    @Expose
    private String userFirstname;
    @SerializedName("user_lastname")
    @Expose
    private String userLastname;*/
    @SerializedName("firstname")
    @Expose
    private String userFirstname;
    @SerializedName("lastname")
    @Expose
    private String userLastname;

    @SerializedName("user_address")
    @Expose
    private String userAddress;
    @SerializedName("user_phone")
    @Expose
    private String userPhone;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("user_password")
    @Expose
    private String userPassword;
    @SerializedName("user_birthdate")
    @Expose
    private String userBirthdate;
    @SerializedName("image")
    @Expose
    private String userImage;
    @SerializedName("user_student_id")
    @Expose
    private String userStudentId;
    @SerializedName("user_grade")
    @Expose
    private String userGrade;
    @SerializedName("user_year")
    @Expose
    private String userYear;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("user_device_type")
    @Expose
    private String userDeviceType;
    @SerializedName("user_device_token")
    @Expose
    private String userDeviceToken;
    @SerializedName("user_access_code")
    @Expose
    private String userAccessCode;
    @SerializedName("user_status")
    @Expose
    private String userStatus;
    @SerializedName("is_access_code_verify")
    @Expose
    private String isAccessCodeVerify;
    @SerializedName("user_registered")
    @Expose
    private String userRegistered;
    @SerializedName("user_is_suspend")
    @Expose
    private String userIsSuspend;
    @SerializedName("user_is_agree")
    @Expose
    private String userIsAgree;
    @SerializedName("user_created")
    @Expose
    private String userCreated;
    @SerializedName("parent_firstname")
    @Expose
    private String parentFirstname;
    @SerializedName("parent_lastname")
    @Expose
    private String parentLastname;
    @SerializedName("parent_phone")
    @Expose
    private String parentPhone;
    @SerializedName("parent_email")
    @Expose
    private String parentEmail;
    @SerializedName("user_token")
    @Expose
    private String userToken;
    @SerializedName("deleted_time")
    @Expose
    private String deletedTime;

    @SerializedName("signature_message")
    @Expose
    private String signatureMessage;


    @SerializedName("data")
    @Expose
    private JsonObject signatureData;


    public JsonObject getSignatureData() {
        return signatureData;
    }

    public void setSignatureData(JsonObject signatureData) {
        this.signatureData = signatureData;
    }

    public String getSignatureMessage() {
        return signatureMessage;
    }

    public void setSignatureMessage(String signatureMessage) {
        this.signatureMessage = signatureMessage;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getIsAccept() {
        return isAccept;
    }

    public void setIsAccept(String isAccept) {
        this.isAccept = isAccept;
    }

    public String getNotificationCreated() {
        return notificationCreated;
    }

    public void setNotificationCreated(String notificationCreated) {
        this.notificationCreated = notificationCreated;
    }

    public String getYearbookPostId() {
        return yearbookPostId;
    }

    public void setYearbookPostId(String yearbookPostId) {
        this.yearbookPostId = yearbookPostId;
    }

    public String getYearbookStaffId() {
        return yearbookStaffId;
    }

    public void setYearbookStaffId(String yearbookStaffId) {
        this.yearbookStaffId = yearbookStaffId;
    }

    public String getYearbookSchoolId() {
        return yearbookSchoolId;
    }

    public void setYearbookSchoolId(String yearbookSchoolId) {
        this.yearbookSchoolId = yearbookSchoolId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getYearbookPostDescription() {
        return yearbookPostDescription;
    }

    public void setYearbookPostDescription(String yearbookPostDescription) {
        this.yearbookPostDescription = yearbookPostDescription;
    }

    public String getYearbookFile() {
        return yearbookFile;
    }

    public void setYearbookFile(String yearbookFile) {
        this.yearbookFile = yearbookFile;
    }

    public String getYearbookPostThumbnail() {
        return yearbookPostThumbnail;
    }

    public void setYearbookPostThumbnail(String yearbookPostThumbnail) {
        this.yearbookPostThumbnail = yearbookPostThumbnail;
    }

    public String getYearbookPostFileType() {
        return yearbookPostFileType;
    }

    public void setYearbookPostFileType(String yearbookPostFileType) {
        this.yearbookPostFileType = yearbookPostFileType;
    }

    public String getYearbookPostFileSizeType() {
        return yearbookPostFileSizeType;
    }

    public void setYearbookPostFileSizeType(String yearbookPostFileSizeType) {
        this.yearbookPostFileSizeType = yearbookPostFileSizeType;
    }

    public String getYearbookPostFilePosition() {
        return yearbookPostFilePosition;
    }

    public void setYearbookPostFilePosition(String yearbookPostFilePosition) {
        this.yearbookPostFilePosition = yearbookPostFilePosition;
    }

    public String getYearbookDate() {
        return yearbookDate;
    }

    public void setYearbookDate(String yearbookDate) {
        this.yearbookDate = yearbookDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserBirthdate() {
        return userBirthdate;
    }

    public void setUserBirthdate(String userBirthdate) {
        this.userBirthdate = userBirthdate;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserStudentId() {
        return userStudentId;
    }

    public void setUserStudentId(String userStudentId) {
        this.userStudentId = userStudentId;
    }

    public String getUserGrade() {
        return userGrade;
    }

    public void setUserGrade(String userGrade) {
        this.userGrade = userGrade;
    }

    public String getUserYear() {
        return userYear;
    }

    public void setUserYear(String userYear) {
        this.userYear = userYear;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserDeviceType() {
        return userDeviceType;
    }

    public void setUserDeviceType(String userDeviceType) {
        this.userDeviceType = userDeviceType;
    }

    public String getUserDeviceToken() {
        return userDeviceToken;
    }

    public void setUserDeviceToken(String userDeviceToken) {
        this.userDeviceToken = userDeviceToken;
    }

    public String getUserAccessCode() {
        return userAccessCode;
    }

    public void setUserAccessCode(String userAccessCode) {
        this.userAccessCode = userAccessCode;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getIsAccessCodeVerify() {
        return isAccessCodeVerify;
    }

    public void setIsAccessCodeVerify(String isAccessCodeVerify) {
        this.isAccessCodeVerify = isAccessCodeVerify;
    }

    public String getUserRegistered() {
        return userRegistered;
    }

    public void setUserRegistered(String userRegistered) {
        this.userRegistered = userRegistered;
    }

    public String getUserIsSuspend() {
        return userIsSuspend;
    }

    public void setUserIsSuspend(String userIsSuspend) {
        this.userIsSuspend = userIsSuspend;
    }

    public String getUserIsAgree() {
        return userIsAgree;
    }

    public void setUserIsAgree(String userIsAgree) {
        this.userIsAgree = userIsAgree;
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getParentFirstname() {
        return parentFirstname;
    }

    public void setParentFirstname(String parentFirstname) {
        this.parentFirstname = parentFirstname;
    }

    public String getParentLastname() {
        return parentLastname;
    }

    public void setParentLastname(String parentLastname) {
        this.parentLastname = parentLastname;
    }

    public String getParentPhone() {
        return parentPhone;
    }

    public void setParentPhone(String parentPhone) {
        this.parentPhone = parentPhone;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getDeletedTime() {
        return deletedTime;
    }

    public void setDeletedTime(String deletedTime) {
        this.deletedTime = deletedTime;
    }


    public String getSenderType() {
        return senderType;
    }

    public void setSenderType(String senderType) {
        this.senderType = senderType;
    }

    public String getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }
}

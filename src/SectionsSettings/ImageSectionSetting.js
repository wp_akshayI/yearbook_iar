import React from "react"
import { useDispatch } from "react-redux"
import { action } from "../utils";
import Actions from "../actions";
import DragAndDrop from "../components/DragAndDrop"

export default function ImageSectionSetting({ data, updateComponents }) {
    const dispatch = useDispatch();

    const uploadImage = async (file) => {
        try {
            dispatch(Actions.removeTampfile(1, data.component_url))
            let res = await dispatch(Actions.uploadImage(file));
            if (res.Result) {
                let value = { component_url: res.Data.image_url }
                updateComponents(value, data.component_order_by)
            } else {
                dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: "warning" });
            }
        } catch (error) {
            console.log(error)
        }
    }

    const _drop = (files) => {
        if (!files || files.length === 0) return;
        let file = files[0];
        uploadImage(file);
    }

    return (
        <div style={{padding: 10}}>
            <DragAndDrop handleDrop={_drop}>
                <div style={{ height: 100, borderColor: "#c9c9c9", borderWidth: 1, borderStyle: "dashed", borderRadius: 4, display: "flex", justifyContent: "center", alignItems: "center" }}>
                    <div style={{display: "grid"}}>
                        <span style={{ color: "#c9c9c9", fontSize: 14, textAlign: "center" }}>
                            Upload Image
                        </span>
                        <span style={{ color: "#c9c9c9", fontSize: 12, textAlign: "center", paddingTop: 5 }}>
                            Click Or Drag & Drop
                        </span>
                    </div>
                </div>
            </DragAndDrop>
        </div>
    )
}
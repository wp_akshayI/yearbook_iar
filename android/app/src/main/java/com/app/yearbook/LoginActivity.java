package com.app.yearbook;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.app.yearbook.model.StaffStudent;
import com.app.yearbook.utils.Constants;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.google.firebase.iid.FirebaseInstanceId;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etEmail, etPassword;
    private Button btnLogin;
    private TextView tvForGotPassword, tvStudentSignUp, tvSchoolSignUp, tvUserSignUp;
    private AllMethods allMethods;
    private LoginUser loginUserSP;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //all control
        setView();
    }

    public void setView() {

        //SP
        loginUserSP = new LoginUser(LoginActivity.this);

        //email
        etEmail = findViewById(R.id.etEmail);

        //password
        etPassword = findViewById(R.id.etPassword);

        //btn login
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        //textview
        tvForGotPassword = findViewById(R.id.tvForgotPassword);
        tvForGotPassword.setOnClickListener(this);

        tvStudentSignUp = findViewById(R.id.tvStudentSignUp);
        tvStudentSignUp.setOnClickListener(this);

        tvSchoolSignUp = findViewById(R.id.tvSchoolSignUp);
        tvSchoolSignUp.setOnClickListener(this);

        tvUserSignUp = findViewById(R.id.tvUserSignUp);
        tvUserSignUp.setOnClickListener(this);

        //common methods
        allMethods = new AllMethods();
        String token = FirebaseInstanceId.getInstance().getToken();
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("Token", token).apply();
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnLogin) {

            if (etEmail.getText().toString().trim().equals("")) {
                allMethods.setAlert(LoginActivity.this, getResources().getString(R.string.app_name), "Please enter email address");
            } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                allMethods.setAlert(LoginActivity.this, getResources().getString(R.string.app_name), "Please enter valid email address");
            } else if (etPassword.getText().toString().trim().equals("")) {
                allMethods.setAlert(LoginActivity.this, getResources().getString(R.string.app_name), "Please enter profile password");
            } else {
                //Progress bar
                progressDialog = ProgressDialog.show(LoginActivity.this, "", "", true);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDialog.setContentView(R.layout.progress_view);
                ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                Circle bounce = new Circle();
                bounce.setColor(Color.BLACK);
                progressBar.setIndeterminateDrawable(bounce);

                String token = "";
                token = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("Token", "");
                Log.d("TTT", "token: " + token);

                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                final Call<Login> login = retrofitClass.login(etEmail.getText().toString(), etPassword.getText().toString(), "android", token);
                final String finalToken = token;
                login.enqueue(new Callback<Login>() {
                    @Override
                    public void onResponse(Call<Login> call, Response<Login> response) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                        Log.d("TTT", response.body().getResponseCode() + " code");
                        if (response.body().getResponseCode().equals(String.valueOf(1))) {

                            if (response.body().getData() != null) {

                                Log.d("TTT", " data not null");

                                //set SP value
                                loginUserSP.setUserData(response.body().getData());
                                Log.d("TTT", "SP: " + loginUserSP.getUserData().getSchoolId());

                                if (response.body().getData().getSchoolId() == null){
                                    StaffStudent user = loginUserSP.getUserData();
                                    user.setUserEmail(etEmail.getText().toString().trim());
                                    user.setUserPassword(etPassword.getText().toString().trim());
                                    user.setUserToken(finalToken);
                                    loginUserSP.setUserData(user);
                                    Intent i = new Intent(getApplicationContext(), AccessCode.class);
                                    startActivity(i);
                                    finish();
//                                }else if (loginUserSP.isUserStudent()
                                }/*else if (response.body().getData().getUserType().equalsIgnoreCase("student")
                                        && response.body().getData().getIsAccessCodeVerify().equalsIgnoreCase("yes")
                                        && !response.body().getData().isArchived()
                                        && !response.body().getData().isYearbookPurchased()){
                                    Intent intent = new Intent(LoginActivity.this, UserProcessWithWeb.class);
                                    intent.putExtra("action", Constants.USER_WEB_PAYMENT);
                                    startActivity(intent);
                                }*/else if (response.body().getData().getUserType().equalsIgnoreCase("student") || response.body().getData().getUserType().equalsIgnoreCase("yb_student")) {
                                    Log.d("TTT", "SP Agree: " + loginUserSP.getUserData().getUserIsAgree());
                                    if (response.body().getData().isArchived())
                                        response.body().getData().setIsAccessCodeVerify("no");
                                    loginUserSP.setUserData(response.body().getData());
                                    if (response.body().getData().getUserIsAgree().equalsIgnoreCase("2")) {
                                        Intent i = new Intent(getApplicationContext(), TermsConditionActivity.class);
                                        startActivity(i);
                                        finish();
                                    } else {
                                        /*if (!response.body().getData().isYearbookPurchased()
                                                && !response.body().getData().isArchived()
                                                && !response.body().getData().isYearbookPurchased()){
                                            Intent intent = new Intent(LoginActivity.this, UserProcessWithWeb.class);
                                            intent.putExtra("action", Constants.USER_WEB_PAYMENT);
                                            startActivity(intent);
                                            finish();
                                        }else {*/
                                            Intent i = new Intent(getApplicationContext(), StudentHomeActivity.class);
                                            startActivity(i);
                                            finish();
//                                        }
                                    }

                                } else if (response.body().getData().getUserType().equalsIgnoreCase("staff")) {

                                    if (response.body().getData().getStaffIsAgree().equalsIgnoreCase("2")) {
                                        Intent i = new Intent(getApplicationContext(), TermsConditionStaffActivity.class);
                                        startActivity(i);
                                        finish();
                                    } else {
                                        Intent i = new Intent(getApplicationContext(), StaffHomeActivity.class);
                                        startActivity(i);
                                        finish();
                                    }
                                } else if (response.body().getData().getUserType().equalsIgnoreCase("employee") || response.body().getData().getUserType().equalsIgnoreCase("employee")) {
                                    Intent i = new Intent(getApplicationContext(), EmployeeHomeActivity.class);
                                    startActivity(i);
                                    finish();
                                } else {
                                    allMethods.setAlert(LoginActivity.this, getResources().getString(R.string.app_name), response.body().getResponseMsg());
                                }

                            } else {
                                allMethods.setAlert(LoginActivity.this, getResources().getString(R.string.app_name), response.body().getResponseMsg());
                            }
                        } else {
                            allMethods.setAlert(LoginActivity.this, getResources().getString(R.string.app_name), response.body().getResponseMsg());
                        }
                    }

                    @Override
                    public void onFailure(Call<Login> call, Throwable t) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        allMethods.setAlert(LoginActivity.this, getResources().getString(R.string.app_name), t.getMessage());
                    }
                });
            }
        } else if (v.getId() == R.id.tvForgotPassword) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.custom_alertbox_forgotpwd, null);
            dialogBuilder.setView(dialogView);

            final AlertDialog alert = dialogBuilder.create();
            alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            alert.setCancelable(false);

            Button btnCancel = dialogView.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                }
            });

            final EditText edtEmail = dialogView.findViewById(R.id.edtEmail);
            final TextInputLayout emailLayout = dialogView.findViewById(R.id.input_layout_email);

            Button btnOk = dialogView.findViewById(R.id.btnOk);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edtEmail.getText().toString().trim().equals("")) {
                        emailLayout.setErrorEnabled(true);
                        emailLayout.setError("Please enter email address");
                        //allMethods.setAlert(StudentSignUpActivity.this,"Yearbook","Please enter email address");
                    } else if (!Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString().trim()).matches()) {
                        emailLayout.setErrorEnabled(true);
                        emailLayout.setError("Please enter valid email address");
                    } else {
                        alert.dismiss();
                        emailLayout.setErrorEnabled(false);
                        emailLayout.setError("");
                        forgotPassword(edtEmail.getText().toString().trim());
                    }
                }
            });
            alert.show();

        } else if (v.getId() == R.id.tvStudentSignUp) {
            Intent i = new Intent(getApplicationContext(), StudentSignUpActivity.class);
            startActivity(i);

        } else if (v.getId() == R.id.tvSchoolSignUp) {
            //old flow
//            Intent i=new Intent(getApplicationContext(),StaffSignUpActivity.class);
//            startActivity(i);

            //new flow
            Intent i = new Intent(getApplicationContext(), StaffRequestActivity.class);
            startActivity(i);

        } else if (v.getId() == R.id.tvUserSignUp){
            startActivity(new Intent(LoginActivity.this, NewSignUpUser.class));
        }
    }

    public void forgotPassword(String email) {
        //Progress bar
        progressDialog = ProgressDialog.show(LoginActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<GiveReport> forgot = retrofitClass.forgotPwd(email);
        forgot.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(LoginActivity.this, getResources().getString(R.string.app_name), response.body().getResponseMsg());
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(LoginActivity.this, getResources().getString(R.string.app_name), t.getMessage());
            }
        });
    }
}

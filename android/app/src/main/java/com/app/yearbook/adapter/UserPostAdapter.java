package com.app.yearbook.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.yearbook.NotificationDetailActivity;
import com.app.yearbook.R;
import com.app.yearbook.StaffPostDetailActivity;
import com.app.yearbook.databinding.ItemUserpostNewBinding;
import com.app.yearbook.model.staff.livefeed.PostListItem;

import java.util.ArrayList;

public class UserPostAdapter extends RecyclerView.Adapter<UserPostAdapter.MyViewHolder> {
    private ArrayList<PostListItem> userLists;
    private Activity ctx;
    private String type;
    private int IntentCodeStaff=890, IntentCodeStudent=780;
    private ItemUserpostNewBinding binding;

    @NonNull
    @Override
    public UserPostAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.item_userpost, parent, false);

        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                , R.layout.item_userpost_new, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserPostAdapter.MyViewHolder holder, final int position) {

        holder.binding.setPost(userLists.get(position));

        //post img
        if (holder.binding.getPost().getPostFile() != null && !holder.binding.getPost().getPostFile().equals("")) {
            final String uri = holder.binding.getPost().getPostFile();
            String extension = uri.substring(uri.lastIndexOf("."));
            Log.d("TTT", "Url type: " + extension);

            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {
                holder.binding.imgPostVideo.setVisibility(View.GONE);
                if (holder.binding.getPost().getPostFile() != null) {
                    Glide.with(ctx)
                            .load(holder.binding.getPost().getPostFile())
                            .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(holder.binding.imgPost);
                }
            } else {
                holder.binding.imgPostVideo.setVisibility(View.VISIBLE);
                Glide.with(ctx)
                        .load(holder.binding.getPost().getPostThumbnail())
                        .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.mipmap.home_post_img_placeholder)
                        .error(R.mipmap.home_post_img_placeholder)
                        .into(holder.binding.imgPost);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(type.equalsIgnoreCase("staff"))
                    {
                        Intent i = new Intent(ctx, StaffPostDetailActivity.class);
                        i.putExtra("PostId", holder.binding.getPost().getPostId());
                        ctx.startActivityForResult(i,IntentCodeStaff);
                    }
                    else {
//                        Intent i = new Intent(ctx, StudentPostEditActivity.class);
//                        i.putExtra("PostObject", holder.binding.getPost());
//                        ctx.startActivity(i);

                        Intent i = new Intent(ctx, NotificationDetailActivity.class);
                        i.putExtra("postID", holder.binding.getPost().getPostId());
                        ctx.startActivity(i);
                    }
                }
            });
        }
    }

    public UserPostAdapter(ArrayList<PostListItem> userLists, Activity context, String type) {
        this.userLists = userLists;
        this.ctx = context;
        this.type=type;
    }

    @Override
    public int getItemCount() {
        return userLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemUserpostNewBinding binding;

        public MyViewHolder(ItemUserpostNewBinding view) {
            super(view.getRoot());
            binding=view;

        }
    }
}

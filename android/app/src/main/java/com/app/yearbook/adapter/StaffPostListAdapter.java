package com.app.yearbook.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.ybq.android.spinkit.style.Circle;
import com.volokh.danylo.hashtaghelper.HashTagHelper;
import com.app.yearbook.AddPostActivity;
import com.app.yearbook.ImageActivity;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.ReportContentActivity;
import com.app.yearbook.StudentCommentActivity;
import com.app.yearbook.databinding.ItemStaffpostListBinding;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.staff.livefeed.PostListItem;
import com.app.yearbook.model.staff.yearbook.UserTagg;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.VideoInfo;

public class StaffPostListAdapter extends RecyclerView.Adapter<StaffPostListAdapter.MyViewHolder> {

    private ArrayList<PostListItem> postLists;
    private Activity ctx;
    private int staffID;
    public ProgressDialog progressDialog;
    private ItemStaffpostListBinding binding;
    private HashTagHelper mEditTextHashTagHelper;


    private ListAdapterListener mListener;

    public interface ListAdapterListener { // create an interface
        void onClickAtButton(int position); // create callback function
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                , R.layout.item_staffpost_list, parent, false);

        return new MyViewHolder(binding);
        //return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.binding.setStaffdata(postLists.get(position));

        //description
        mEditTextHashTagHelper = HashTagHelper.Creator.create(ctx.getResources().getColor(R.color.hashtag), null);
        mEditTextHashTagHelper.handle(holder.binding.tvPostDescription);

        Log.d("TTT", "Report: " + holder.binding.getStaffdata().getTotalRepoted());

        Log.d("TTT", "StudentList adapter: " + holder.binding.getStaffdata().getPostType());
        if (holder.binding.getStaffdata().getPostType().equals("2")) {
            holder.binding.lvAd.setVisibility(View.VISIBLE);
            holder.binding.lvPost.setVisibility(View.GONE);

            if (holder.binding.getStaffdata().getPostFile() != null) {
                Glide.with(ctx)
                        .load(holder.binding.getStaffdata().getPostFile())
                        .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.mipmap.home_post_img_placeholder)
                        .error(R.mipmap.home_post_img_placeholder)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                holder.binding.imgStudentAd.setImageBitmap(resource);
                            }
                        });
            }

            final String path = holder.binding.getStaffdata().getPostFile();
            //image touch
            holder.binding.imgStudentAd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, ImageActivity.class);
                    i.putExtra("caption", holder.binding.getStaffdata().getPostDescription());
                    i.putExtra("FileUrl", path);
                    ctx.startActivity(i);
                }
            });

        } else if (holder.binding.getStaffdata().getPostType().equals("1")) {

            holder.binding.lvAd.setVisibility(View.GONE);
            holder.binding.lvPost.setVisibility(View.VISIBLE);

            //user img
            if (holder.binding.getStaffdata().getStaffImage() != null) {
                Glide.with(ctx)
                        .load(holder.binding.getStaffdata().getStaffImage())
                        .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.mipmap.post_place_holder)
                        .error(R.mipmap.post_place_holder)
                        .into(holder.binding.imgUserImage);

                holder.binding.imgUserImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (holder.binding.getStaffdata().getStaffImage() != null && !holder.binding.getStaffdata().getStaffImage().equals("")) {
                            Intent i = new Intent(ctx, ImageActivity.class);
                            i.putExtra("FileUrl", holder.binding.getStaffdata().getStaffImage());
                            ctx.startActivity(i);
                        }
                    }
                });
            }

            //post img
            if (holder.binding.getStaffdata().getPostFile() != null || !holder.binding.getStaffdata().getPostFile().equals("") ) {
                final String uri = holder.binding.getStaffdata().getPostFile();
                if (uri.isEmpty()) {
                    return;
                }
                String extension = uri.substring(uri.lastIndexOf("."));
                Log.d("TTT", "Url type: " + extension);

                if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {
                    holder.binding.imgUserPost.setVisibility(View.VISIBLE);
                    holder.binding.videoUserPost.setVisibility(View.GONE);
                    if (holder.binding.getStaffdata().getPostFile() != null) {
                        Glide.with(ctx)
                                .load(holder.binding.getStaffdata().getPostFile())
                                .asBitmap()
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                        holder.binding.imgUserPost.setImageBitmap(resource);
                                    }
                                });
                    }
                } else {

                    holder.binding.imgUserPost.setVisibility(View.GONE);
                    holder.binding.videoUserPost.setVisibility(View.VISIBLE);
                    //holder.binding.videoUserPost.reset();
                    //holder.binding.videoUserPost.setSource(Uri.parse(holder.binding.getStaffdata().getPostFile()));

                    Log.d("TTT", "Video urllllll: " + holder.binding.getStaffdata().getPostFile());

                    if (holder.binding.videoUserPost.getCoverView() != null) {
                        Glide.with(ctx)
                                .load(holder.binding.getStaffdata().getPostThumbnail())
                                .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(holder.binding.videoUserPost.getCoverView());
                        //holder.binding.videoUserPost.getCoverView().setImageDrawable(ctx.getDrawable(R.mipmap.ic_launcher));
                    }

                    holder.binding.videoUserPost.getVideoInfo().setPortraitWhenFullScreen(false);
                    holder.binding.videoUserPost.setVideoPath(holder.binding.getStaffdata().getPostFile());
                    holder.binding.videoUserPost.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player

                    holder.binding.imgFullScreen.setVisibility(View.GONE);
//                    holder.binding.imgFullScreen.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent i = new Intent(ctx, FullscreenActivity.class);
//                            i.putExtra("FileUrl", holder.binding.getStaffdata().getPostFile());
//                            ctx.startActivity(i);
//                        }
//                    });

                }

                //image touch
                holder.binding.imgUserPost.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ctx, ImageActivity.class);
                        i.putExtra("caption", holder.binding.getStaffdata().getPostDescription());
                        i.putExtra("FileUrl", uri);
                        ctx.startActivity(i);
                    }
                });
            }

            //comment
            holder.binding.tvComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx.getApplicationContext(), StudentCommentActivity.class);
                    i.putExtra("PostId", holder.binding.getStaffdata().getPostId());
                    i.putExtra("comment", "staff");
                    ctx.startActivity(i);
                }
            });


            if (staffID == Integer.parseInt(holder.binding.getStaffdata().getStaffId())) {
                holder.binding.imgOption.setVisibility(View.VISIBLE);
                holder.binding.imgReport.setVisibility(View.GONE);
            } else {
                holder.binding.imgOption.setVisibility(View.GONE);
                holder.binding.imgReport.setVisibility(View.VISIBLE);
            }
            //user delete
            holder.binding.imgOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (staffID == Integer.parseInt(holder.binding.getStaffdata().getStaffId())) {
                        //same user
                        showDialogForSameStudent(holder.binding.getStaffdata(), holder, holder.binding.imgOption);
                    }


//                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ctx);
//                    LayoutInflater inflater = ctx.getLayoutInflater();
//                    final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
//                    dialogBuilder.setView(dialogView);
//                    final AlertDialog alert = dialogBuilder.create();
//                    alert.setCancelable(false);
//                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
//
//                    TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
//                    tvMessage.setText("Are you sure you want to remove the user?");
//
//                    TextView tvYes = dialogView.findViewById(R.id.tvYes);
//                    TextView tvNo = dialogView.findViewById(R.id.tvNo);
//
//                    tvYes.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
//                            alert.dismiss();
//                            final String id = holder.binding.getStaffdata().getStaffId();
//
//                            progressDialog = ProgressDialog.show(ctx, "", "", true);
//                            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                            progressDialog.setContentView(R.layout.progress_view);
//                            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
//                            Circle bounce = new Circle();
//                            bounce.setColor(Color.BLACK);
//                            progressBar.setIndeterminateDrawable(bounce);
//
//                            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
//                            final Call<GiveReport> userPost = retrofitClass.staffDeleteUser(Integer.parseInt(id));
//                            userPost.enqueue(new Callback<GiveReport>() {
//                                @Override
//                                public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
//                                    if (progressDialog.isShowing()) {
//                                        progressDialog.dismiss();
//                                    }
//
//                                    if (response.body().getResponseCode().equals("1")) {
//                                        Log.d("TTT", "Total user: " + postLists.size());
//                                        for (int x = postLists.size() - 1; x >= 0; x--) {
//                                            Log.d("TTT", "loop: " + x + " / " + postLists.get(x).getStaffId() + " / " + id);
//                                            if (postLists.get(x).getStaffId().equals(String.valueOf(id))) {
//                                                Log.d("TTT", "position: " + x);
//                                                postLists.remove(x);
//                                                notifyItemRemoved(x);
//                                            }
//                                        }
//
//                                        mListener.onClickAtButton(postLists.size());
//                                        Toast.makeText(ctx, "Successfully delete the user.", Toast.LENGTH_SHORT).show();
//                                    } else if (response.body().getResponseCode().equals("10")) {
//                                        Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
//                                        LoginUser loginSP = new LoginUser(ctx);
//                                        loginSP.clearData();
//
//                                        Intent i = new Intent(ctx, LoginActivity.class);
//                                        ctx.startActivity(i);
//                                        ctx.finish();
//                                    } else {
//                                        Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
//                                    }
//
//                                }
//
//                                @Override
//                                public void onFailure(Call<GiveReport> call, Throwable t) {
//                                    if (progressDialog.isShowing()) {
//                                        progressDialog.dismiss();
//                                    }
//                                    Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
//                                }
//                            });
//                        }
//                    });
//
//                    tvNo.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alert.dismiss();
//                        }
//                    });
//                    alert.show();
                }
            });

            //view content
            holder.binding.tvReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d("TTT", "Post id: " + holder.binding.getStaffdata().getPostId());
                    if (!holder.binding.tvReport.getText().toString().trim().equalsIgnoreCase("Not reported yet")) {
                        Intent i = new Intent(ctx, ReportContentActivity.class);
                        i.putExtra("postId", holder.binding.getStaffdata().getPostId());
                        ctx.startActivity(i);
                    }
                }
            });

            //report delete
            holder.binding.imgReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ctx);
                    LayoutInflater inflater = ctx.getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
                    dialogBuilder.setView(dialogView);
                    final AlertDialog alert = dialogBuilder.create();
                    alert.setCancelable(false);
                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                    TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
                    tvMessage.setText("Are you sure you want to remove the post?");

                    TextView tvYes = dialogView.findViewById(R.id.tvYes);
                    TextView tvNo = dialogView.findViewById(R.id.tvNo);

                    tvYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            alert.dismiss();
                            final String id = holder.binding.getStaffdata().getPostId();

                            progressDialog = ProgressDialog.show(ctx, "", "", true);
                            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            progressDialog.setContentView(R.layout.progress_view);
                            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                            Circle bounce = new Circle();
                            bounce.setColor(Color.BLACK);
                            progressBar.setIndeterminateDrawable(bounce);

                            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                            final Call<GiveReport> userPost = retrofitClass.staffDeletePost(Integer.parseInt(id), staffID);
                            userPost.enqueue(new Callback<GiveReport>() {
                                @Override
                                public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }

                                    if (response.body().getResponseCode().equals("1")) {
                                        for (int i = 0; i < postLists.size(); i++) {
                                            if (postLists.get(i).getPostId().equals(String.valueOf(id))) {
                                                Log.d("TTT", "Remove " + id + " / " + i);
                                                postLists.remove(i);
                                                notifyItemRemoved(i);
                                            }
                                        }

                                        mListener.onClickAtButton(postLists.size());
                                        Toast.makeText(ctx, "Successfully delete the post.", Toast.LENGTH_SHORT).show();
                                    } else if (response.body().getResponseCode().equals("10")) {
                                        Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                        LoginUser loginSP = new LoginUser(ctx);
                                        loginSP.clearData();

                                        Intent i = new Intent(ctx, LoginActivity.class);
                                        ctx.startActivity(i);
                                        ctx.finish();
                                    } else {
                                        Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<GiveReport> call, Throwable t) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                    Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

                    tvNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });
                    alert.show();
                }

            });
        }
    }

    public void showDialogForSameStudent(final PostListItem postList, final MyViewHolder holder, ImageView imgOption) {

        PopupMenu popup = new PopupMenu(ctx, imgOption);
        popup.getMenuInflater().inflate(R.menu.same_staff_opt, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Edit")) {
                    Log.d("TTT", "Postttt dialog: " + postList.getPostId() + " / " + postList.getUserTagg().size());
                    Intent i = new Intent(ctx, AddPostActivity.class);
                    i.putExtra("Edit", "Edit");
                    i.putExtra("Thumb", postList.getPostThumbnail());
                    i.putExtra("PostId", postList.getPostId());
                    i.putExtra("Description", postList.getPostDescription());
                    i.putExtra("Title", postList.getPostTitle());
                    i.putExtra("postUrl", postList.getPostFile());
                    i.putExtra("tagUserId", (ArrayList<UserTagg>) postList.getUserTagg());
                    ctx.startActivity(i);
                } else if (item.getTitle().equals("Delete")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                    builder.setMessage("Are you sure you want to delete the post?")
                            .setCancelable(false)
                            .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    progressDialog = ProgressDialog.show(ctx, "", "", true);
                                    progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    progressDialog.setContentView(R.layout.progress_view);
                                    ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                                    Circle bounce = new Circle();
                                    bounce.setColor(Color.BLACK);
                                    progressBar.setIndeterminateDrawable(bounce);

                                    RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                    final Call<GiveReport> deletePost = retrofitClass.deletePost(Integer.parseInt(postList.getPostId()));
                                    deletePost.enqueue(new Callback<GiveReport>() {
                                        @Override
                                        public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                            if (progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }

                                            if (response.body().getResponseCode().equals("1")) {
                                                Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                                for (int i = 0; i < postLists.size(); i++) {
                                                    if (postLists.get(i).getPostId().equals(String.valueOf(postList.getPostId()))) {
                                                        Log.d("TTT", "Removeeee " + postList.getPostId() + " / " + i);
                                                        postLists.remove(i);
                                                        notifyItemRemoved(i);
                                                    }
                                                }
                                            } else if (response.body().getResponseCode().equals("10")) {
                                                Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                                LoginUser loginSP = new LoginUser(ctx);
                                                loginSP.clearData();

                                                Intent i = new Intent(ctx, LoginActivity.class);
                                                ctx.startActivity(i);
                                                ctx.finish();
                                            } else {
                                                Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<GiveReport> call, Throwable t) {
                                            if (progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }
                                            Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            })
                            .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    }
                            );

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    alert.setTitle("");
                    alert.show();

                }
                return true;
            }
        });
        popup.show();//showing popup menu
    }


    public StaffPostListAdapter(ArrayList<PostListItem> postLists, Activity context, int userID, ListAdapterListener mListener) {
        this.staffID = userID;
        this.postLists = postLists;
        this.ctx = context;
        this.mListener = mListener;
    }

    @Override
    public int getItemCount() {
        return postLists.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ItemStaffpostListBinding binding;

        public MyViewHolder(ItemStaffpostListBinding view) {
            super(view.getRoot());
            binding = view;
        }
    }


    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent event) {
            Log.d("TAG", "onDown: ");

            // don't return false here or else none of the other
            // gestures will work
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            Log.i("TAG", "onSingleTapConfirmed: ");
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            Log.i("TAG", "onLongPress: ");
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Log.i("TAG", "onDoubleTap: ");
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {
            Log.i("TAG", "onScroll: ");
            return true;
        }

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) {
            Log.d("TAG", "onFling: ");
            return true;
        }
    }

}

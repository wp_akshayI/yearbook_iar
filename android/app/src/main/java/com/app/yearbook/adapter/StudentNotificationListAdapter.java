package com.app.yearbook.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.app.yearbook.ImageActivity;
import com.app.yearbook.R;
import com.app.yearbook.databinding.ItemStudentnotificationListBinding;
import com.app.yearbook.model.profile.NotificationList;
import com.app.yearbook.utils.OnRecyclerClick;

import java.util.ArrayList;

public class StudentNotificationListAdapter extends RecyclerView.Adapter<StudentNotificationListAdapter.MyViewHolder> {
    private ArrayList<NotificationList> notificationLists;
    private Activity ctx;
    private OnRecyclerClick onRecyclerClick;
    private static final String TAG = "StudentNotificationList";
    ItemStudentnotificationListBinding binding;

    @NonNull
    @Override
    public StudentNotificationListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.item_studentnotification_list, parent, false);

        binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.item_studentnotification_list,parent,false);
        return new MyViewHolder(binding.getRoot());
//        return binding.getRoot();
    }

    @Override
    public void onBindViewHolder(@NonNull final StudentNotificationListAdapter.MyViewHolder holder, final int position) {
       NotificationList notificationList = notificationLists.get(position);
        holder.binding.setModel(notificationList);

//        String name = holder.binding.getModel().getUserFirstname() + " " + holder.binding.getModel().getUserLastname();
//        String msg = holder.binding.getModel().getNotificationMessage();

//        holder.binding.tvUserName.setText(name);
//        holder.binding.tvUserStatus.setText(msg);

//        Spannable word = new SpannableString(name);
//        word.setSpan(new ForegroundColorSpan(Color.BLACK), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//        Spannable wordTwo = new SpannableString(msg);
//
//        Spannable signature = new SpannableString("New Signature from");
//        signature.setSpan(new ForegroundColorSpan(ctx.getResources().getColor(R.color.deselectTab)), 0, signature.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//        if (userList.getNotificationType().equalsIgnoreCase("signature")) {
//            if (userList.getIsAccept().equalsIgnoreCase("0")) {
//                holder.tvUserName.setText(wordTwo + "");
//            } else if (userList.getIsAccept().equalsIgnoreCase("1")) {
//                holder.tvUserStatus.setVisibility(View.VISIBLE);
//                holder.tvUserName.setText(signature + " ");
//                holder.tvUserName.append(word);
//                holder.tvUserStatus.setText("You accepted signature");
//            } else if (userList.getIsAccept().equalsIgnoreCase("2")) {
//                holder.tvUserStatus.setVisibility(View.VISIBLE);
//                holder.tvUserName.setText(signature + " ");
//                holder.tvUserName.append(word);
//                holder.tvUserStatus.setText("You denied signature");
//            }
//        } else if (userList.getNotificationType().equalsIgnoreCase("yearbook")) {
//            holder.tvUserName.setText(msg);
//        } else {
//            holder.tvUserName.setText(word + " ");
//            wordTwo.setSpan(new ForegroundColorSpan(ctx.getResources().getColor(R.color.deselectTab)), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//            holder.tvUserName.append(wordTwo);
//
//        }
//
//
//        Log.d(TAG, "onBindViewHolder: word: " + word + " / " + wordTwo);
//        //holder.tvUserName.setText(userList.getUserFirstname() + "  " + userList.getUserLastname());
//
//        holder.tvUserTime.setText(userList.getNotificationCreated());

        if (binding.getModel().getUserImage() != null) {
//            Glide.with(ctx)
//                    .load(binding.getModel().getUserImage())
//                    .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                    .placeholder(R.mipmap.profile_placeholder)
//                    .error(R.mipmap.profile_placeholder)
//                    .into(holder.binding.imgUserImage);

            /*holder.binding.imgUserImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, ImageActivity.class);
                    i.putExtra("FileUrl", holder.binding.getModel().getUserImage());
                    ctx.startActivity(i);
                }
            });*/
        }

        //Display date time
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        sdf.setTimeZone(TimeZone.getTimeZone("CST6CDT"));
//
//        Date date = null;
//        try {
//
//            date = sdf.parse(userList.getNotificationCreated());
//            sdf.setTimeZone(TimeZone.getDefault());
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        SimpleDateFormat sameDateSDF = new SimpleDateFormat("dd-MM-yyyy");
//        sameDateSDF.setTimeZone(TimeZone.getTimeZone("CST6CDT"));
//
//        sameDateSDF.setTimeZone(TimeZone.getDefault());
//        String DBDate = sameDateSDF.format(date);
//        String currentDate = sameDateSDF.format(new Date());
//        Log.d("TTT", "Date: " + DBDate + " / " + currentDate);
//
//        if (DBDate.equalsIgnoreCase(currentDate)) {
//            //same date
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            //dateFormat.setTimeZone(TimeZone.getTimeZone("CST"));
//            Date currDate = new Date();
//
//            //dateFormat.setTimeZone(TimeZone.getDefault());
//            String dd = dateFormat.format(currDate);
//            Log.d("TTT", "Dateeeee: " + dd + " / " + DBDate);
//
//            try {
//                currDate = dateFormat.parse(dd);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//            long different = currDate.getTime() - date.getTime();
//            long secondsInMilli = 1000;
//            long minutesInMilli = secondsInMilli * 60;
//            long hoursInMilli = minutesInMilli * 60;
//            long daysInMilli = hoursInMilli * 24;
//
//            long elapsedDays = different / daysInMilli;
//            different = different % daysInMilli;
//
//            long elapsedHours = different / hoursInMilli;
//            different = different % hoursInMilli;
//
//            long elapsedMinutes = different / minutesInMilli;
//            different = different % minutesInMilli;
//
//            long elapsedSeconds = different / secondsInMilli;
//
//            Log.d("TTT", "DDDD : " + elapsedDays + " / " + elapsedHours + " / " + elapsedMinutes + " / " + elapsedSeconds);
//
//            if (elapsedHours < 1) {
//                holder.tvUserTime.setText(elapsedMinutes + "m");
//            } else {
//                holder.tvUserTime.setText(elapsedHours + "h");
//            }
//        } else {
//            //not same
//            String outputPattern = "MMM dd, yyyy -hh:mm aa";
//            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
//
//            String str = null;
//            str = outputFormat.format(date);
//
//            holder.tvUserTime.setText(str + "");
//        }

        //&& userList.getIsAccept().equalsIgnoreCase("0")
       // Log.d("TTT", "Notification type: " + userList.getNotificationType());
//        holder.itemView.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (userList.getNotificationType().equalsIgnoreCase("signature")) {
//
//                    if (userList.getIsAccept().equalsIgnoreCase("1")) {
//                        Intent i = new Intent(ctx, StudentSignatureNotificationActivity.class);
//                        i.putExtra("NotificationData", userList);
//                        i.putExtra("position", position);
//                        ctx.startActivity(i);
//                    }
//                } else if (userList.getNotificationType().equalsIgnoreCase("like") || userList.getNotificationType().equalsIgnoreCase("comment")) {
//                    Intent i = new Intent(ctx, NotificationDetailActivity.class);
//                    i.putExtra("postID", userList.getPostId());
//                    ctx.startActivity(i);
//                } else if (userList.getNotificationType().equalsIgnoreCase("yearbook_like") || userList.getNotificationType().equalsIgnoreCase("yearbook_comment")) {
//                    Intent intent = new Intent(ctx, StudentYearbookViewPostActivity.class);
//                    intent.putExtra("PostId", userList.getPostId());
//                    ctx.startActivity(intent);
//                }
//            }
//        });

//        if (userList.getNotificationType().equalsIgnoreCase("signature_accept")) {
//            holder.lvSignature.setVisibility(View.VISIBLE);
//           // holder.btnAddSignature.setVisibility(View.VISIBLE);
//            holder.btnAccept.setVisibility(View.GONE);
//            holder.btnDeny.setVisibility(View.GONE);
//        } else {
//            //holder.btnAddSignature.setVisibility(View.GONE);
//            if (userList.getNotificationType().equalsIgnoreCase("signature") && userList.getIsAccept().equalsIgnoreCase("0")) {
//                holder.lvSignature.setVisibility(View.VISIBLE);
//            } else {
//                holder.lvSignature.setVisibility(View.GONE);
//            }
//        }

    }

    public StudentNotificationListAdapter(ArrayList<NotificationList> notificationLists, Activity context, OnRecyclerClick onRecyclerClick) {
        this.notificationLists = notificationLists;
        this.ctx = context;
        this.onRecyclerClick = onRecyclerClick;
    }

    @Override
    public int getItemCount() {
        return notificationLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements OnClickListener {

        ItemStudentnotificationListBinding binding;

//        public TextView tvUserName, tvUserTime, tvUserStatus, btnAccept, btnDeny;
//        public RoundedImageView imgUserPhoto;
//        public LinearLayout lvSignature;

        public MyViewHolder(View view) {
            super(view);
            binding=DataBindingUtil.bind(view);

            binding.btnDeny.setOnClickListener(this);
            binding.btnAccept.setOnClickListener(this);
            binding.btnAddSignature.setOnClickListener(this);
            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRecyclerClick.onClick(getAdapterPosition(), v, 4);
                }
            });

        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.btnAccept) {
                onRecyclerClick.onClick(getAdapterPosition(), v, 1);
            } else if (v.getId() == R.id.btnDeny) {
                onRecyclerClick.onClick(getAdapterPosition(), v, 2);
            }
            else if (v.getId() == R.id.btnAddSignature) {
                onRecyclerClick.onClick(getAdapterPosition(), v, 3);
            }
        }
    }

}

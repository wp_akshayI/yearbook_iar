'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ActivityIndicator,
    Image,
    Platform,
    ImageBackground
} from "react-native";
import Video from "react-native-video";
import Slider from '@react-native-community/slider';
import { play_ic, pause_ic, img_section_gradient } from "../../assets";
import { secondsToTime } from '../../utils/funtions';

export default class VideoSection extends Component {

    state = {
        duration: 0.0,
        currentTime: 0.0,
        paused: true,
        isLoading: false,
        controls: Platform.OS === "android",
        isError: false
    };

    video: Video;

    onLoad = (data) => {
        this.setState({ duration: data.duration, isLoading: false });
    };

    onProgress = (data) => {
        this.setState({ currentTime: data.currentTime });
    };

    onEnd = () => {
        this.setState({ paused: true })
        this.video.seek(0)
    };

    onAudioBecomingNoisy = () => {
        this.setState({ paused: true })
    };

    onAudioFocusChanged = (event: { hasAudioFocus: boolean }) => {
        this.setState({ paused: !event.hasAudioFocus })
    };

    onError = (error) => {
        console.log(error);
        this.setState({ isLoading: false, paused: true, isError: true })
    }

    _togglePuase = () => {
        const { paused } = this.state;
        this.setState({ paused: !paused });
    }

    _sliding = (data) => {

        this.video.seek(data, 0);
        this.setState({ currentTime: data, isLoading: true });
    }

    onSeek = (data) => {
        this.setState({ isLoading: false });
        // console.log(data)
    }

    renderPlayPaused = () => {
        const { paused } = this.state
        return (
            <TouchableOpacity
                onPress={this._togglePuase}
                activeOpacity={1}
                style={{ alignSelf: "center" }}
            >
                <Image
                    source={paused ? play_ic : pause_ic}
                    style={{ height: 50, width: 50 }}
                />
            </TouchableOpacity>
        )
    }

    renderLoader = () => {
        if (this.state.isLoading)
            return (
                <ActivityIndicator
                    color="#fff"
                    size={35}
                    style={{ position: "absolute", alignSelf: "center" }}
                />
            )
        return null;
    }

    renderControls = () => {
        if (!this.state.controls || Platform.OS === "ios" || this.state.isError) return null;
        return (
            <>
                <ImageBackground
                    style={{
                        width: "100%",
                        height: "100%",
                        position: "absolute", 
                        justifyContent: "center"
                    }}
                    source={img_section_gradient}>
                    {this.renderPlayPaused()}
                    <View style={styles.controls}>
                        <Text style={styles.timer}>
                            {secondsToTime(this.state.currentTime)}
                        </Text>
                        <Slider
                            style={{ flex: 1 }}
                            minimumValue={0}
                            maximumValue={this.state.duration}
                            value={this.state.currentTime}
                            minimumTrackTintColor="#FFFFFF"
                            maximumTrackTintColor="#d7d7d7"
                            onValueChange={this._sliding}
                            onSlidingStart={() => this.setState({ paused: true })}
                            onSlidingComplete={() => this.setState({ paused: false })}
                        />
                        <Text style={styles.timer}>
                            {secondsToTime(this.state.duration)}
                        </Text>
                    </View>
                </ImageBackground>
            </>
        )
    }

    render() {
        const { controls, isError } = this.state
        const { component_thumb_url, component_url } = this.props.data;
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.setState({ controls: !controls })}
                    style={styles.container}
                >
                    <Video
                        ref={(ref: Video) => { this.video = ref }}
                        source={{ uri: component_url }}
                        style={{ aspectRatio: 16 / 9 }}
                        controls={Platform.OS === "ios"}
                        paused={this.state.paused}
                        playInBackground={false}
                        resizeMode="contain"
                        onVideoBuffer={() => console.log("isBuffering")}
                        onBuffer={() => this.setState({ isLoading: true })}
                        onLoadStart={() => this.setState({ isLoading: true })}
                        onLoad={this.onLoad}
                        onError={this.onError}
                        onProgress={this.onProgress}
                        onSeek={this.onSeek}
                        onEnd={this.onEnd}
                        onAudioBecomingNoisy={this.onAudioBecomingNoisy}
                        onAudioFocusChanged={this.onAudioFocusChanged}
                        poster={component_thumb_url}
                        posterResizeMode="contain"
                    />
                    {
                        isError && (
                            <View style={{ position: "absolute", alignSelf: "center" }}>
                                <Text style={{ textAlign: "center", color: "#ffcccc", fontSize: 14 }}>
                                    Playback error!
                                </Text>
                            </View>
                        )
                    }
                    {this.renderLoader()}
                    {this.renderControls()}
                </TouchableOpacity>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: 'black',
    },
    timer: {
        color: "#fff"
    },
    controls: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: 'transparent',
        borderRadius: 5,
        position: 'absolute',
        bottom: 10,
        left: 15,
        right: 15,
    }
});
package com.app.yearbook.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.ybq.android.spinkit.style.Circle;
import com.volokh.danylo.hashtaghelper.HashTagHelper;
import com.app.yearbook.AddPostActivity;
import com.app.yearbook.GetAllStudentLikeActivity;
import com.app.yearbook.ImageActivity;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.StudentCommentActivity;
import com.app.yearbook.databinding.ItemUserpostListBinding;
import com.app.yearbook.model.studenthome.AddBookMark;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.studenthome.LikeUnlike;
import com.app.yearbook.model.studenthome.OnOffNotification;
import com.app.yearbook.model.studenthome.PostList;
import com.app.yearbook.model.studenthome.home.PostListItem;
import com.app.yearbook.model.staff.yearbook.UserTagg;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.DefaultPlayerListener;
import tcking.github.com.giraffeplayer2.GiraffePlayer;
import tcking.github.com.giraffeplayer2.PlayerListener;
import tcking.github.com.giraffeplayer2.VideoInfo;

public class StudentPostListAdapter extends RecyclerView.Adapter<StudentPostListAdapter.MyViewHolder> {
    private ArrayList<PostListItem> postLists;
    private Activity ctx;
    private int userID;
    public ProgressDialog progressDialog;
    private HashTagHelper mEditTextHashTagHelper;
    private ItemUserpostListBinding binding;
    private MyViewHolder myholder;
    public ArrayList<UserTagg> tagUserListArray;
    private GestureDetector mDetector;
    public static Handler handler;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                , R.layout.item_userpost_list, parent, false);
        GiraffePlayer.debug = true;//show java logs
        GiraffePlayer.nativeDebug = false;//not show native logs

        return new MyViewHolder(binding);
    }

    public StudentPostListAdapter(ArrayList<PostListItem> postLists, Activity context, int userID) {
        this.userID = userID;
        this.postLists = postLists;
        this.ctx = context;
        // initHandler();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    private PlayerListener playerListener = new DefaultPlayerListener() {//example of using playerListener
        @Override
        public void onPreparing(GiraffePlayer giraffePlayer) {
            //  Toast.makeText(ctx, "start playing:" + giraffePlayer.getVideoInfo().getUri(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCompletion(GiraffePlayer giraffePlayer) {
            // Toast.makeText(ctx, "play completion:" + giraffePlayer.getVideoInfo().getUri(), Toast.LENGTH_SHORT).show();
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        myholder = holder;
        holder.binding.setData(postLists.get(position));
        holder.binding.executePendingBindings();

        Log.d("TTT", "POSTID: " + holder.binding.getData().getPostId());

        Log.d("TTT", "StudentList adapter: " + holder.binding.getData().getPostType());
        if (holder.binding.getData().getPostType().equals("2")) {
            holder.binding.lvAd.setVisibility(View.VISIBLE);
            holder.binding.lvPost.setVisibility(View.GONE);

            if (holder.binding.getData().getPostFile() != null) {
                Glide.with(ctx)
                        .load(holder.binding.getData().getPostFile())
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.mipmap.home_post_img_placeholder)
                        .error(R.mipmap.home_post_img_placeholder)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                holder.binding.imgStudentAd.setImageBitmap(resource);
                            }
                        });
            }

            final String path = holder.binding.getData().getPostFile();
            //image touch
            holder.binding.imgStudentAd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, ImageActivity.class);
                    i.putExtra("caption", holder.binding.getData().getPostDescription());
                    i.putExtra("FileUrl", path);
                    ctx.startActivity(i);
                }
            });

        } else if (holder.binding.getData().getPostType().equals("1")) {

            holder.binding.lvAd.setVisibility(View.GONE);
            holder.binding.lvPost.setVisibility(View.VISIBLE);

            //user img
            if (holder.binding.getData().getStaffImage() != null) {
                Glide.with(ctx)
                        .load(holder.binding.getData().getStaffImage())
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.mipmap.post_place_holder)
                        .error(R.mipmap.post_place_holder)
                        .into(holder.binding.imgUserImage);

                binding.imgUserImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ctx, ImageActivity.class);
                        i.putExtra("FileUrl", holder.binding.getData().getStaffImage());
                        ctx.startActivity(i);
                    }
                });
            }


            //post img
            if (holder.binding.getData().getPostFile() != null && !holder.binding.getData().getPostFile().equals("")) {
                final String uri = holder.binding.getData().getPostFile();

                String extension = uri.substring(uri.lastIndexOf("."));
                Log.d("TTT", "Url type: " + extension);

                if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {
                    holder.binding.imgUserPost.setVisibility(View.VISIBLE);
                    holder.binding.videoUserPost.setVisibility(View.GONE);
                    if (holder.binding.getData().getPostFile() != null) {
                        Glide.with(ctx)
                                .load(holder.binding.getData().getPostFile())
                                .asBitmap()
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                        holder.binding.imgUserPost.setImageBitmap(resource);
                                    }
                                });
                    }
                } else if (extension.equalsIgnoreCase(".mp4")) {
                    holder.binding.imgUserPost.setVisibility(View.GONE);
                    holder.binding.videoUserPost.setVisibility(View.VISIBLE);

//                    try {
//                        holder.binding.videoUserPost.setSource(Uri.parse(holder.binding.getData().getPostFile()));
//                    }
//                    catch (Exception e)
//                    {
//                        e.printStackTrace();
//                    }

                    Log.d("TTT", "Video url: " + holder.binding.getData().getPostFile());

                    holder.binding.videoUserPost.getVideoInfo().setBgColor(ctx.getResources().getColor(R.color.black));
                    if (holder.binding.videoUserPost.getCoverView() != null) {
                        holder.binding.videoUserPost.getCoverView().setBackgroundColor(ctx.getResources().getColor(R.color.black));
                        Glide.with(ctx)
                                .load(holder.binding.getData().getPostThumbnail())
                                .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(holder.binding.videoUserPost.getCoverView());
                    }

                    if (holder.binding.videoUserPost != null) {
                        holder.binding.videoUserPost.getVideoInfo().setPortraitWhenFullScreen(false);
                        holder.binding.videoUserPost.setVideoPath(holder.binding.getData().getPostFile());
                        holder.binding.videoUserPost.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player
                    }

                    holder.binding.imgFullScreen.setVisibility(View.GONE);
                }


                //tvDate 2019-03-04 12:47:21

                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                SimpleDateFormat output ;

                try {
                    String date=holder.binding.getData().getPostDate();
                    if(date.endsWith("1") && !date.endsWith("11"))
                    {
                        output = new SimpleDateFormat("MMMM d'st', yyyy");
                    }
                    else if(date.endsWith("2") && !date.endsWith("12"))
                    {
                        output = new SimpleDateFormat("MMMM d'nd', yyyy");
                    }
                    else if(date.endsWith("3") && !date.endsWith("13"))
                    {
                        output = new SimpleDateFormat("MMMM d'rd', yyyy");
                    }
                    else
                    {
                        output = new SimpleDateFormat("MMMM d'th', yyyy");
                    }

                    Date oneWayTripDate = input.parse(date);                 // parse input
                    holder.binding.tvDate.setText(output.format(oneWayTripDate));    // format output
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                //image touch
                holder.binding.imgUserPost.setOnTouchListener(new View.OnTouchListener() {

                    private GestureDetector gestureDetector = new GestureDetector(ctx, new GestureDetector.SimpleOnGestureListener() {
                        @Override
                        public boolean onDoubleTap(MotionEvent e) {
                            Log.d("TEST", "onDoubleTap");

                            AllMethods.heart(holder.binding.imgDoubleTap, holder.binding.imgLikeDislike);

                            if (holder.binding.getData().getPostLike().equalsIgnoreCase("false")) {

                                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                Call<LikeUnlike> userPost = retrofitClass.addDoubleTapLike(userID, Integer.parseInt(holder.binding.getData().getPostId()));
                                userPost.enqueue(new Callback<LikeUnlike>() {
                                    @Override
                                    public void onResponse(Call<LikeUnlike> call, Response<LikeUnlike> response) {

                                        if (response.body().getResponseCode().equals("1")) {
                                            if (response.body().getPostDetail().get(0).getPostLike().equalsIgnoreCase("true")) {
                                                holder.binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
                                            } else if (response.body().getPostDetail().get(0).getPostLike().equalsIgnoreCase("false")) {
                                                holder.binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
                                            }

                                            holder.binding.getData().setTotalLike(response.body().getPostDetail().get(0).getTotalLike());
                                            holder.binding.getData().setPostLike(response.body().getPostDetail().get(0).getPostLike());

                                        } else if (response.body().getResponseCode().equals("10")) {
                                            Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                            LoginUser loginSP = new LoginUser(ctx);
                                            loginSP.clearData();

                                            Intent i = new Intent(ctx, LoginActivity.class);
                                            ctx.startActivity(i);
                                            ctx.finish();
                                        } else {
                                            Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<LikeUnlike> call, Throwable t) {
                                        Log.d("TTT", "Failure: " + t.getMessage());
                                        Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                            return super.onDoubleTap(e);
                        }

                        @Override
                        public boolean onSingleTapConfirmed(MotionEvent e) {
                            Log.d("TEST", "onSingleTapConfirmed");
                            Intent i = new Intent(ctx, ImageActivity.class);
                            i.putExtra("caption", holder.binding.getData().getPostDescription());
                            i.putExtra("FileUrl", uri);
                            ctx.startActivity(i);
                            return super.onSingleTapConfirmed(e);
                        }
                    });

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        gestureDetector.onTouchEvent(event);
                        return true;
                    }
                });

//                holder.binding.imgUserPost.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent i = new Intent(ctx, ImageActivity.class);
//                        i.putExtra("FileUrl", uri);
//                        ctx.startActivity(i);
//                    }
//                });
            }

            //description
            mEditTextHashTagHelper = HashTagHelper.Creator.create(ctx.getResources().getColor(R.color.hashtag), null);
            mEditTextHashTagHelper.handle(holder.binding.tvPostDescription);

            //tag user
            if (binding.getData().getUserTagg() != null) {
                if (binding.getData().getUserTagg().size() > 0) {
                    Log.d("TTT", "Taggg User: " + binding.getData().getUserTagg().size());
                    tagUserListArray = new ArrayList<>();
                    binding.lvTaggPpl.setVisibility(View.VISIBLE);
                    tagUserListArray.addAll(binding.getData().getUserTagg());
                    TaggStudentUserListAdapter userListAdapter = new TaggStudentUserListAdapter(tagUserListArray, ctx, "student");
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.HORIZONTAL, false);
                    holder.binding.rvTaggUserList.setLayoutManager(mLayoutManager);
                    holder.binding.rvTaggUserList.setAdapter(userListAdapter);
                } else {
                    binding.lvTaggPpl.setVisibility(View.GONE);
                }
            }

            //like-unlike
            Log.d("TTT", "Like dislike val: " + holder.binding.getData().getPostLike());
            if (holder.binding.getData().getPostLike().equalsIgnoreCase("true")) {
                holder.binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
            } else {
                holder.binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
            }

            holder.binding.imgLikeDislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder.binding.getData().getPostLike().equalsIgnoreCase("true")) {
                        YoYo.with(Techniques.RubberBand)
                                .duration(500)
                                .playOn(holder.binding.imgLikeDislike);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                holder.binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
                            }
                        }, 200);

                    } else if (holder.binding.getData().getPostLike().equalsIgnoreCase("false")) {

                        YoYo.with(Techniques.RubberBand)
                                .duration(500)
                                .playOn(holder.binding.imgLikeDislike);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                holder.binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
                            }
                        }, 200);
                    }

                    Log.d("TTT", "LikeDisLike: " + userID + " / " + holder.binding.getData().getPostId());
                    RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                    Call<LikeUnlike> userPost = retrofitClass.postLikeDislike(userID, Integer.parseInt(holder.binding.getData().getPostId()),0);
                    userPost.enqueue(new Callback<LikeUnlike>() {
                        @Override
                        public void onResponse(Call<LikeUnlike> call, Response<LikeUnlike> response) {

                            if (response.body().getResponseCode().equals("1")) {
                                if (response.body().getPostDetail().get(0).getPostLike().equalsIgnoreCase("true")) {
                                    holder.binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
                                } else if (response.body().getPostDetail().get(0).getPostLike().equalsIgnoreCase("false")) {
                                    holder.binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
                                }

                                holder.binding.getData().setTotalLike(response.body().getPostDetail().get(0).getTotalLike());
                                holder.binding.getData().setPostLike(response.body().getPostDetail().get(0).getPostLike());

                            } else if (response.body().getResponseCode().equals("10")) {
                                Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                LoginUser loginSP = new LoginUser(ctx);
                                loginSP.clearData();

                                Intent i = new Intent(ctx, LoginActivity.class);
                                ctx.startActivity(i);
                                ctx.finish();
                            } else {
                                Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<LikeUnlike> call, Throwable t) {
                            Log.d("TTT", "Failure: " + t.getMessage());
                            Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            });

            //bookmark
            Log.d("TTT", "Bookmark: " + holder.binding.getData().getPostBookmark());
            if (holder.binding.getData().getPostBookmark().equalsIgnoreCase("true")) {
                holder.binding.imgBookmark.setImageResource(R.mipmap.bookmark_selected_ic);
            } else {
                holder.binding.imgBookmark.setImageResource(R.mipmap.bookmark_ic);
            }
            holder.binding.imgBookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("TTT", "bookmark: " + userID + " / " + holder.binding.getData().getPostId());
                    RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                    Call<AddBookMark> bookmark = retrofitClass.addBookmark(userID, Integer.parseInt(holder.binding.getData().getPostId()));
                    bookmark.enqueue(new Callback<AddBookMark>() {
                        @Override
                        public void onResponse(Call<AddBookMark> call, Response<AddBookMark> response) {
                            if (response.body().getResponseCode().equals("1")) {
                                if (response.body().getPostDetail().get(0).getPostBookmark().equalsIgnoreCase("true")) {
                                    holder.binding.imgBookmark.setImageResource(R.mipmap.bookmark_selected_ic);
                                } else if (response.body().getPostDetail().get(0).getPostBookmark().equalsIgnoreCase("false")) {
                                    holder.binding.imgBookmark.setImageResource(R.mipmap.bookmark_ic);
                                }

                                holder.binding.getData().setPostBookmark(response.body().getPostDetail().get(0).getPostBookmark());

                            } else if (response.body().getResponseCode().equals("10")) {
                                Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                LoginUser loginSP = new LoginUser(ctx);
                                loginSP.clearData();

                                Intent i = new Intent(ctx, LoginActivity.class);
                                ctx.startActivity(i);
                                ctx.finish();
                            } else {
                                Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<AddBookMark> call, Throwable t) {
                            Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });

            holder.binding.tvTotalLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d("TTT", "post id adapter: " + holder.binding.getData().getPostId());
                    Intent i = new Intent(ctx, GetAllStudentLikeActivity.class);
                    i.putExtra("postId", holder.binding.getData().getPostId());
                    i.putExtra("Type", "student");
                    ctx.startActivity(i);
                }
            });

            //comment
//            Typeface typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/avenirltstd_medium.otf");
//            holder.binding.tvComment.setTypeface(typeface);
//            holder.binding.tvComment.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent i = new Intent(ctx.getApplicationContext(), StudentCommentActivity.class);
//                    i.putExtra("comment", "user");
//                    i.putExtra("PostId", holder.binding.getData().getPostId());
//                    ctx.startActivityForResult(i, 123);
//                }
//            });

            holder.binding.imgComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx.getApplicationContext(), StudentCommentActivity.class);
                    i.putExtra("PostId", holder.binding.getData().getPostId());
                    i.putExtra("comment", "user");
                    ctx.startActivityForResult(i, 123);
                }
            });

            //option
            holder.binding.imgOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (userID == Integer.parseInt(holder.binding.getData().getUserId())) {
//                        //same user
//                        Log.d("TTT", "tag user size: " + holder.binding.getData().getUserTagg().size() + " / " + holder.binding.getData().getPostPush());
//                        showDialogForSameStudent(holder.binding.getData(), holder, holder.binding.imgOption);
//                    }
//                    else
//                    {
//                        //diff student
//                        String notification = holder.binding.getData().getPostPush();
//
//                        Log.d("TTT", "Menuuu: " + notification);
//                        //showDialogForDiffStudent(notification, userID, Integer.parseInt(holder.binding.getData().getPostId()), holder.binding.imgOption);
//
//                        PopupMenu popup = new PopupMenu(ctx, holder.binding.imgOption);
//                        popup.getMenuInflater().inflate(R.menu.different_student_opt, popup.getMenu());
//                        //Menu notificationMenu = null;
//
//                        //notification
//                        if (notification.equalsIgnoreCase("true")) {
//                            popup.getMenu().findItem(R.id.notification).setTitle("Turn off notification");
//                        } else {
//                            popup.getMenu().findItem(R.id.notification).setTitle("Turn on notification");
//                        }
//
//                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            public boolean onMenuItemClick(MenuItem item) {
//                                if (item.getTitle().equals("Report content")) {
//                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ctx);
//                                    LayoutInflater inflater = ctx.getLayoutInflater();
//                                    final View dialogView = inflater.inflate(R.layout.custom_alertbox_report, null);
//                                    dialogBuilder.setView(dialogView);
//                                    final AlertDialog alert = dialogBuilder.create();
//                                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
//
//                                    final Button btnSend = dialogView.findViewById(R.id.btnSend);
//                                    final EditText etMessage = dialogView.findViewById(R.id.etMessage);
//
//                                    etMessage.addTextChangedListener(new TextWatcher() {
//                                        @Override
//                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                                        }
//
//                                        @Override
//                                        public void onTextChanged(CharSequence s, int start, int before, int count) {
//                                            if (s.length() > 0) {
//                                                btnSend.setEnabled(true);
//                                                btnSend.setBackgroundColor(ctx.getResources().getColor(R.color.btnGray));
//                                                btnSend.setTextColor(ctx.getResources().getColor(R.color.white));
//                                            } else {
//                                                btnSend.setEnabled(false);
//                                                btnSend.setBackgroundColor(ctx.getResources().getColor(R.color.edtBG));
//                                                btnSend.setTextColor(ctx.getResources().getColor(R.color.deselectTab));
//                                            }
//                                        }
//
//                                        @Override
//                                        public void afterTextChanged(Editable s) {
//
//                                        }
//                                    });
//                                    btnSend.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//
//                                            progressDialog = ProgressDialog.show(ctx, "", "", true);
//                                            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                                            progressDialog.setContentView(R.layout.progress_view);
//                                            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
//                                            Circle bounce = new Circle();
//                                            bounce.setColor(Color.BLACK);
//                                            progressBar.setIndeterminateDrawable(bounce);
//
//                                            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
//                                            final Call<GiveReport> report = retrofitClass.reported(userID, Integer.parseInt(holder.binding.getData().getPostId()), etMessage.getText().toString());
//                                            report.enqueue(new Callback<GiveReport>() {
//                                                @Override
//                                                public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
//                                                    alert.dismiss();
//                                                    if (progressDialog.isShowing()) {
//                                                        progressDialog.dismiss();
//                                                    }
//                                                    if (response.body().getResponseCode().equals("1")) {
//                                                        Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
//                                                    } else if (response.body().getResponseCode().equals("10")) {
//                                                        Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
//                                                        LoginUser loginSP = new LoginUser(ctx);
//                                                        loginSP.clearData();
//
//                                                        Intent i = new Intent(ctx, LoginActivity.class);
//                                                        ctx.startActivity(i);
//                                                        ctx.finish();
//                                                    } else {
//                                                        Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
//                                                    }
//
//                                                }
//
//                                                @Override
//                                                public void onFailure(Call<GiveReport> call, Throwable t) {
//                                                    alert.dismiss();
//                                                    if (progressDialog.isShowing()) {
//                                                        progressDialog.dismiss();
//                                                    }
//                                                    Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
//                                                }
//                                            });
//                                        }
//                                    });
//
//                                    alert.show();
//
//                                } else {
//                                    progressDialog = ProgressDialog.show(ctx, "", "", true);
//                                    progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                                    progressDialog.setContentView(R.layout.progress_view);
//                                    ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
//                                    Circle bounce = new Circle();
//                                    bounce.setColor(Color.BLACK);
//                                    progressBar.setIndeterminateDrawable(bounce);
//
//                                    RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
//                                    Call<OnOffNotification> notification = retrofitClass.notification(userID, Integer.parseInt(holder.binding.getData().getPostId()));
//                                    notification.enqueue(new Callback<OnOffNotification>() {
//                                        @Override
//                                        public void onResponse(Call<OnOffNotification> call, Response<OnOffNotification> response) {
//                                            if (progressDialog.isShowing()) {
//                                                progressDialog.dismiss();
//                                            }
//                                            if (response.body().getResponseCode().equals("1")) {
//                                                for (int i = 0; i < postLists.size(); i++) {
//                                                    if (postLists.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
//                                                        PostList newList = response.body().getPostDetail().get(0);
//                                                        holder.binding.getData().setPostPush(newList.getPostPush());
//                                                        //postLists.set(i, newList);
//                                                        //notifyDataSetChanged();
//                                                    }
//                                                }
//                                                Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
//
//                                            } else if (response.body().getResponseCode().equals("10")) {
//                                                Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
//                                                LoginUser loginSP = new LoginUser(ctx);
//                                                loginSP.clearData();
//
//                                                Intent i = new Intent(ctx, LoginActivity.class);
//                                                ctx.startActivity(i);
//                                                ctx.finish();
//                                            } else {
//
//                                                Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
//                                            }
//                                        }
//
//                                        @Override
//                                        public void onFailure(Call<OnOffNotification> call, Throwable t) {
//                                            if (progressDialog.isShowing()) {
//                                                progressDialog.dismiss();
//                                            }
//                                            Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
//                                        }
//                                    });
//
//                                }
//                                return true;
//                            }
//                        });
//                        popup.show();
//                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return postLists.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ItemUserpostListBinding binding;

        public MyViewHolder(ItemUserpostListBinding view) {
            super(view.getRoot());
            binding = view;

            if (binding.videoUserPost != null) {
                binding.videoUserPost.setPlayerListener(playerListener);
            }
        }
    }

    public void showDialogForSameStudent(final PostList postList, final MyViewHolder holder, ImageView imgOption) {

        PopupMenu popup = new PopupMenu(ctx, imgOption);
        popup.getMenuInflater().inflate(R.menu.same_student_opt, popup.getMenu());

        MenuItem itemNotification = popup.getMenu().findItem(R.id.notification);
        itemNotification.setVisible(true);

        if (postList.getPostNotification().equalsIgnoreCase("true")) {
            popup.getMenu().findItem(R.id.notification).setTitle("Turn off notification");
        } else {
            popup.getMenu().findItem(R.id.notification).setTitle("Turn on notification");
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                if (item.getTitle().equals("Edit")) {
                    Log.d("TTT", "Postttt dialog: " + postList.getPostId() + " / " + postList.getUserTagg().size());
                    Intent i = new Intent(ctx, AddPostActivity.class);
                    i.putExtra("Edit", "Edit");
                    i.putExtra("Thumb", postList.getPostThumbnail());
                    i.putExtra("PostId", postList.getPostId());
                    i.putExtra("Description", postList.getPostDescription());
                    i.putExtra("postUrl", postList.getPostFile());
                    i.putExtra("tagUserId", (ArrayList<UserTagg>) postList.getUserTagg());
                    ctx.startActivity(i);
                } else if (item.getTitle().equals("Delete")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                    builder.setMessage("Are you sure you want to delete the post?")
                            .setCancelable(false)
                            .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    progressDialog = ProgressDialog.show(ctx, "", "", true);
                                    progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    progressDialog.setContentView(R.layout.progress_view);
                                    ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                                    Circle bounce = new Circle();
                                    bounce.setColor(Color.BLACK);
                                    progressBar.setIndeterminateDrawable(bounce);

                                    RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                    final Call<GiveReport> deletePost = retrofitClass.deletePost(Integer.parseInt(postList.getPostId()));
                                    deletePost.enqueue(new Callback<GiveReport>() {
                                        @Override
                                        public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                            if (progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }

                                            if (response.body().getResponseCode().equals("1")) {
                                                Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                                for (int i = 0; i < postLists.size(); i++) {
                                                    if (postLists.get(i).getPostId().equals(String.valueOf(postList.getPostId()))) {
                                                        Log.d("TTT", "Removeeee " + postList.getPostId() + " / " + i);
                                                        postLists.remove(i);
                                                        notifyItemRemoved(i);
                                                    }
                                                }
                                            } else if (response.body().getResponseCode().equals("10")) {
                                                Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                                LoginUser loginSP = new LoginUser(ctx);
                                                loginSP.clearData();

                                                Intent i = new Intent(ctx, LoginActivity.class);
                                                ctx.startActivity(i);
                                                ctx.finish();
                                            } else {
                                                Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<GiveReport> call, Throwable t) {
                                            if (progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }
                                            Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            })
                            .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    }
                            );

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    alert.setTitle("");
                    alert.show();

                } else {
                    progressDialog = ProgressDialog.show(ctx, "", "", true);
                    progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    progressDialog.setContentView(R.layout.progress_view);
                    ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                    Circle bounce = new Circle();
                    bounce.setColor(Color.BLACK);
                    progressBar.setIndeterminateDrawable(bounce);

                    RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                    Call<OnOffNotification> notification = retrofitClass.studentOwnPostNotification(userID, Integer.parseInt(postList.getPostId()));
                    notification.enqueue(new Callback<OnOffNotification>() {
                        @Override
                        public void onResponse(Call<OnOffNotification> call, Response<OnOffNotification> response) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            if (response.body().getResponseCode().equals("1")) {
                                for (int i = 0; i < postLists.size(); i++) {
                                    if (postLists.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                        PostList newList = response.body().getPostDetail().get(0);
                                        Log.d("TTT", "own notification" + newList.getPostNotification());
                                        holder.binding.getData().setPostNotification(newList.getPostNotification());
                                    }
                                }
                                Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();

                            } else if (response.body().getResponseCode().equals("10")) {
                                Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                LoginUser loginSP = new LoginUser(ctx);
                                loginSP.clearData();

                                Intent i = new Intent(ctx, LoginActivity.class);
                                ctx.startActivity(i);
                                ctx.finish();
                            } else {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                                Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<OnOffNotification> call, Throwable t) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                return true;
            }
        });
        popup.show();//showing popup menu
    }

}

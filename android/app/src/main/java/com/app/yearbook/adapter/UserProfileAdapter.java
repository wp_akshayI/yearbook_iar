package com.app.yearbook.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.yearbook.R;
import com.app.yearbook.model.studenthome.profile.StudentProfile;
import com.app.yearbook.utils.OnRecyclerClick;

import java.util.ArrayList;

public class UserProfileAdapter extends RecyclerView.Adapter<UserProfileAdapter.MyViewHolderGallery> {

    private Context context;
    private ArrayList<StudentProfile> imgList;
    private LayoutInflater inflater;
    private OnRecyclerClick onRecyclerClick;
    private static final String TAG = "UserImageAdapter";

    public UserProfileAdapter(Context context, ArrayList<StudentProfile> imgList, OnRecyclerClick onRecyclerClick) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.imgList = imgList;
        this.onRecyclerClick = onRecyclerClick;
    }

    @Override
    public UserProfileAdapter.MyViewHolderGallery onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = inflater.inflate(R.layout.student_profile_rowlayout, parent, false);
        MyViewHolderGallery holder = new MyViewHolderGallery(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(MyViewHolderGallery holder, int position) {
        // holder.imgIcon.setImageURI(imgList.get(position).toString());

        Log.d(TAG, "onBindViewHolder: " + imgList.get(position).getPath());
        Glide.with(context)
                .load(imgList.get(position).getPath())
                .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.home_post_img_placeholder)
                .error(R.mipmap.home_post_img_placeholder)
                .into(holder.imgIcon);

        if(imgList.get(position).isChecked())
        {
            holder.lvCheck.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.lvCheck.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return imgList.size();
    }

    class MyViewHolderGallery extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgIcon, ivOption;
        LinearLayout lvCheck;

        public MyViewHolderGallery(View itemView) {
            super(itemView);
            imgIcon = itemView.findViewById(R.id.image);
            ivOption = itemView.findViewById(R.id.ivOption);
            lvCheck=itemView.findViewById(R.id.lvCheck);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerClick.onClick(getAdapterPosition(), v, 0);
        }
    }


}



package com.app.yearbook.model.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TagsList {

    @SerializedName("post_description")
    @Expose
    private String postDescription;
    @SerializedName("total")
    @Expose
    private String total;

    public String getPostDescription() {
        return postDescription;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}

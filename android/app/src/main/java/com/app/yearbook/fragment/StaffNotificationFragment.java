package com.app.yearbook.fragment;


import androidx.databinding.DataBindingUtil;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.yearbook.NewSignatureActivity;
import com.app.yearbook.R;
import com.app.yearbook.SignatureDetailActivity;
import com.app.yearbook.adapter.StudentNotificationListAdapter;
import com.app.yearbook.databinding.FragmentStaffNotificationBinding;
import com.app.yearbook.model.NotificationAcceptDeny;
import com.app.yearbook.model.profile.NotificationList;
import com.app.yearbook.model.profile.SignatureList;
import com.app.yearbook.model.profile.getNotification;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.OnRecyclerClick;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class StaffNotificationFragment extends Fragment {


    FragmentStaffNotificationBinding binding;

    public StaffNotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_staff_notification, container, false);

        return binding.getRoot();
       // return inflater.inflate(R.layout.fragment_staff_notification, container, false);
    }



    /*Ashish*/

    RetrofitClass retrofitClass;
    private static final String TAG = "StudentNotificationFrag";
    ArrayList<NotificationList> notificationLists;
    public static int NOTIFICATION = 101;
    boolean isProgress = false;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        binding.swipyrefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                binding.swipyrefreshlayout.setRefreshing(true);
                isProgress = true;
                notificationCall();
            }
        });

        notificationCall();
    }

    private void notificationCall() {
        if (isProgress) {
            binding.progressBar.setVisibility(View.GONE);
            isProgress = false;
        }
        else
        {
            binding.progressBar.setVisibility(View.VISIBLE);
        }

        retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<getNotification> staffPostResponseCall = retrofitClass.getNotification(Integer.parseInt(LoginUser.getUserData().getStaffId()), LoginUser.getUserTypeKey());
        staffPostResponseCall.enqueue(new Callback<getNotification>() {
            @Override
            public void onResponse(Call<getNotification> call, Response<getNotification> response) {
                getNotification staffPostResponse = response.body();
                if (staffPostResponse != null) {

                    if (staffPostResponse.getResult().equalsIgnoreCase("true")) {
                        if (staffPostResponse.getNotificationList().size() > 0) {
                            notificationLists = new ArrayList<>();
                            notificationLists.addAll(staffPostResponse.getNotificationList());

                            if (notificationLists != null && notificationLists.size() > 0) {
                                binding.txtNoDta.setVisibility(View.GONE);

                                StudentNotificationListAdapter mAdapter = new StudentNotificationListAdapter(notificationLists, getActivity(), new OnRecyclerClick() {
                                    @Override
                                    public void onClick(int pos, View v, int type) {
                                        if (type == 1) {
                                            //accept
                                            callAPI(getActivity(), notificationLists.get(pos).getNotificationId(), "1", pos);
                                        } else if (type == 2) {
                                            //deny
                                            callAPI(getActivity(), notificationLists.get(pos).getNotificationId(), "2", pos);
                                        } else if (type == 3) {
                                            //deny
                                            Intent intent = new Intent(getActivity(), NewSignatureActivity.class);
                                            intent.putExtra("notification", notificationLists.get(pos));
                                            startActivityForResult(intent, NOTIFICATION);
                                        } else if (type == 4){
                                            if (notificationLists.get(pos).getNotificationType().contentEquals("signature")) {
                                                Intent i = new Intent(getActivity(), SignatureDetailActivity.class);
                                                i.putExtra("Signature", new Gson().fromJson(notificationLists.get(pos).getSignatureData().toString(), SignatureList.class));
                                                startActivity(i);
                                            }
                                        }
                                    }
                                });
                                binding.rcNotifications.setAdapter(mAdapter);

                            } else {
                                binding.txtNoDta.setVisibility(View.VISIBLE);
                            }
                        } else {
                            binding.txtNoDta.setVisibility(View.VISIBLE);
                        }


                    } else {
                        binding.txtNoDta.setVisibility(View.VISIBLE);
                    }


                    binding.swipyrefreshlayout.setRefreshing(false);
                    binding.progressBar.setVisibility(View.GONE);
                }
                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<getNotification> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
                binding.swipyrefreshlayout.setRefreshing(false);
                binding.progressBar.setVisibility(View.GONE);
                binding.txtNoDta.setVisibility(View.VISIBLE);
            }
        });
    }

    public void callAPI(final Context context, String id, final String flag, final int pos) {
        //Progress bar
//        progressDialog = ProgressDialog.show(NotificationActivity.this, "", "", true);
//        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        progressDialog.setContentView(R.layout.progress_view);
//        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
//        Circle bounce = new Circle();
//        bounce.setColor(Color.BLACK);
//        progressBar.setIndeterminateDrawable(bounce);


        binding.progressBar.setVisibility(View.VISIBLE);
        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<NotificationAcceptDeny> userPost = retrofitClass.notificationStatus(Integer.parseInt(id), Integer.parseInt(flag));
        userPost.enqueue(new Callback<NotificationAcceptDeny>() {
            @Override
            public void onResponse(Call<NotificationAcceptDeny> call, Response<NotificationAcceptDeny> response) {
                binding.progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getResponseCode().equalsIgnoreCase("1") || response.body().getResponseCode().equalsIgnoreCase("2")) {
                        notificationLists.remove(pos);
                        binding.rcNotifications.getAdapter().notifyItemRemoved(pos);

                        if (notificationLists.size() == 0) {
                            binding.txtNoDta.setVisibility(View.VISIBLE);
                        } else {
                            binding.txtNoDta.setVisibility(View.GONE);
                        }
                    }
                    Toast.makeText(context, "" + response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NotificationAcceptDeny> call, Throwable t) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(context, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NOTIFICATION) {
            if (resultCode == getActivity().RESULT_OK) {
                notificationCall();
            }
        }
    }
}

package com.app.yearbook.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.app.yearbook.NotificationDetailActivity;
import com.app.yearbook.R;
import com.app.yearbook.SignatureDetailActivity;
import com.app.yearbook.StaffHomeActivity;
import com.app.yearbook.StudentHomeActivity;
import com.app.yearbook.StudentYearbookViewPostActivity;
import com.app.yearbook.TermsConditionStaffActivity;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String CHANNEL_NAME = "FCM";
    private static final String CHANNEL_DESC = "Firebase Cloud Messaging";
    private Intent intent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        if (remoteMessage.getData().size() > 0) {

            String user_msg = "", postId = "", yearbook_postId = "", push_type = "";

            if (remoteMessage.getData().containsKey("message")) {
                user_msg = remoteMessage.getData().get("message");
            }

            if (remoteMessage.getData().containsKey("post_id")) {
                postId = remoteMessage.getData().get("post_id");
            }
            if (remoteMessage.getData().containsKey("yearbook_post_id")) {
                yearbook_postId = remoteMessage.getData().get("yearbook_post_id");
            }
            if (remoteMessage.getData().containsKey("push_type")) {
                push_type = remoteMessage.getData().get("push_type");
            }

            String notification_id = "";
            if (remoteMessage.getData().containsKey("notification_id")) {
                notification_id = remoteMessage.getData().get("notification_id");
            }

            Log.d("TTT", "Message : " + user_msg);
            Log.d("TTT", "Message data payload: " + remoteMessage.getData());

            sendMyNotification(user_msg, postId, yearbook_postId, push_type, notification_id);
        }

        //{yearbook_post_id=0, push_type=like, post_id=30, badge=14, message=yogesh savaliya likes your post!}

    }

    private void sendMyNotification(String message, String postId, String yearbook_postId, String push_type, String nID) {

        Random r = new Random();
        int randomNo = r.nextInt(80 - 1 + 1) + 1;

        Log.d("TTT", "Post id: " + postId + " / yearbook id: " + yearbook_postId);
        //On click of notification it redirect to this Activity

        NotificationCompat.Builder notificationBuilder;

        setIntentBasedPush(push_type, nID);

        if (intent != null) {

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.notification_ic)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setAutoCancel(true)
                    .setSound(soundUri)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setChannelId(getString(R.string.default_notification_channel_id))
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Log.d("TTT", "Build.VERSION_CODES.O");

                NotificationChannel channel = new NotificationChannel(getString(R.string.default_notification_channel_id), CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription(CHANNEL_DESC);
                channel.setShowBadge(true);
                channel.canShowBadge();
                channel.enableLights(true);
                channel.setLightColor(Color.RED);
                channel.enableVibration(true);
                channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});

                assert notificationManager != null;
                notificationManager.createNotificationChannel(channel);
            }


            assert notificationManager != null;
            notificationManager.notify(randomNo, notificationBuilder.build());
        }
    }

    private void setIntentBasedPush(String push_type, String nID) {
        LoginUser loginUser = new LoginUser(this);

        if (loginUser.getUserData() != null) {
            if (push_type.equalsIgnoreCase("signature")) {
                intent = new Intent(this, SignatureDetailActivity.class);
                intent.putExtra("isPush", true);
                intent.putExtra("signId", nID);
            }else if (loginUser.getUserData().getUserType().equalsIgnoreCase("student")
                    || loginUser.getUserData().getUserType().equalsIgnoreCase("yb_student")) {

                /*if (push_type.equalsIgnoreCase("add_signature")) {
                    intent = new Intent(this, StudentHomeActivity.class);
                } else if (push_type.equalsIgnoreCase("signature_accept")) {
                    intent = new Intent(this, StudentHomeActivity.class);
                }*/
                if (push_type.equalsIgnoreCase("add_signature") ||
                        push_type.equalsIgnoreCase("signature_accept")) {
                    intent = new Intent(this, StudentHomeActivity.class);
                    intent.putExtra("isRedirect", true);
                    intent.putExtra("redirectOn","notification");
                }else{
                    intent = new Intent(this, StudentHomeActivity.class);
                }
            } else if (loginUser.getUserData().getUserType().equalsIgnoreCase("staff")) {

                if (push_type.equalsIgnoreCase("add_signature") ||
                        push_type.equalsIgnoreCase("signature_accept")) {
                    intent = new Intent(this, StaffHomeActivity.class);
                    intent.putExtra("isRedirect", true);
                    intent.putExtra("redirectOn","notification");
                }else{
                    intent = new Intent(this, StaffHomeActivity.class);
                }

                /*if (push_type.equalsIgnoreCase("add_signature")) {
                    intent = new Intent(this, StaffHomeActivity.class);
                    intent.putExtra("isRedirect", true);
                    intent.putExtra("redirectOn","notification");
                } else if (push_type.equalsIgnoreCase("signature_accept")) {
                    intent = new Intent(this, StaffHomeActivity.class);
                    intent.putExtra("isRedirect", true);
                    intent.putExtra("redirectOn","notification");
                }*/


                /*if (loginUser.getUserData().getStaffIsAgree().equals("2")) {
                    intent = new Intent(getApplicationContext(), TermsConditionStaffActivity.class);
                } else {
                    intent = new Intent(getApplicationContext(), StaffHomeActivity.class);
                }*/
            }else
                intent = null;
        } else {
            intent = null;
        }

        /* else if (push_type.equalsIgnoreCase("yearbook")) {
            intent = new Intent(this, StudentHomeActivity.class);

        } else {
            if (yearbook_postId.equals("0")) {
                intent = new Intent(this, NotificationDetailActivity.class);
                intent.putExtra("postID", postId);
            } else if (postId.equals("0")) {
                intent = new Intent(this, StudentYearbookViewPostActivity.class);
                intent.putExtra("PostId", yearbook_postId);
            }

        }*/
    }
}

import React from 'react'
import { useDispatch } from 'react-redux'
import { action } from '../utils'
import Actions from '../actions'
import DragAndDrop from 'components/DragAndDrop'

export default function VideoSectionSetting ({ data, updateComponents }) {
  const dispatch = useDispatch()

  const _drop = (files) => {
    if (!files || files.length === 0) return
    const file = files[0]
    // let uri = URL.createObjectURL(file);
    var fileReader = new FileReader()
    fileReader.onload = () => {
      let duration
      var blob = new Blob([fileReader.result], { type: file.type })
      var url = URL.createObjectURL(blob)
      var video = document.createElement('video')
      var timeupdate = () => {
        if (snapImage()) {
          video.removeEventListener('timeupdate', timeupdate)
          video.pause()
        }
      }
      video.addEventListener('loadeddata', () => {
        duration = video.duration / 60
        if (duration > 1) { dispatch({ type: action.open_notification, message: 'Maximum 1 minute video duration allow!' }) }
        if (snapImage()) {
          video.removeEventListener('timeupdate', timeupdate)
        }
      })
      var snapImage = () => {
        var canvas = document.createElement('canvas')
        canvas.width = video.videoWidth
        canvas.height = video.videoHeight
        canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height)
        var image = canvas.toDataURL()
        var success = image.length > 100000
        if (success) {
          if (!duration || duration > 1) return
          uploadVideo(file, image)
          URL.revokeObjectURL(url)
        }
        return success
      }
      video.addEventListener('timeupdate', timeupdate)
      video.preload = 'metadata'
      video.src = url
      video.muted = true
      video.playsInline = true
      video.play()
    }
    fileReader.readAsArrayBuffer(file)
  }

  const uploadVideo = async (file, image) => {
    try {
      const res = await dispatch(Actions.uploadVideo(file, image))
      if (res.Result === false) return
      const value = { component_url: res.Data.video_url, component_thumb_url: res.Data.thumb_url, video_duration: res.Data.video_duration }
      updateComponents(value, data.component_order_by)
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div style={{ padding: 10 }}>
      <DragAndDrop handleDrop={_drop}>
        <div style={{ height: 100, borderColor: '#c9c9c9', borderWidth: 1, borderStyle: 'dashed', borderRadius: 4, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <div style={{ display: 'grid' }}>
            <span style={{ color: '#c9c9c9', fontSize: 14, textAlign: 'center' }}>
                            Upload Video
            </span>
            <span style={{ color: '#c9c9c9', fontSize: 12, textAlign: 'center', paddingTop: 5 }}>
                            Click Or Drag & Drop
            </span>
          </div>
        </div>
      </DragAndDrop>
    </div>
  )
}

package com.app.yearbook.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.app.yearbook.EmployeeHomeActivity;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.StaffHomeActivity;
import com.app.yearbook.StudentHomeActivity;
import com.app.yearbook.TermsConditionActivity;
import com.app.yearbook.TermsConditionStaffActivity;
import com.app.yearbook.UserProcessWithWeb;
import com.app.yearbook.databinding.FgAccessCodeBinding;
import com.app.yearbook.model.Login;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.Constants;
import com.github.ybq.android.spinkit.style.Circle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccessCodeFragment extends Fragment {

    private static final String TAG = "AccessCodeFragment";
    public View view;
    public ProgressDialog progressDialog;
    public int schoolId, userId, page = 1;

    private AllMethods allMethods;
    private LoginUser loginUserSP;

    RetrofitClass retrofitClass;
    Context ctx;
    FgAccessCodeBinding binding;
    boolean isUserAlreadyExists = false;

    public AccessCodeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fg_access_code, container, false);
        ctx = getActivity();
        loginUserSP = new LoginUser(getActivity());
        allMethods = new AllMethods();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrofitClass = APIClient.getClient().create(RetrofitClass.class);

//        binding.edtAccessCode.setText("638!JQL");
//        binding.edtReAccessCode.setText("638!JQL");

        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.edtAccessCode.getText().toString().trim().length() <= 0) {
                    Toast.makeText(ctx, "Please enter access code", Toast.LENGTH_SHORT).show();
                    return;
                } else if (binding.edtReAccessCode.getText().toString().trim().length() <= 0) {
                    Toast.makeText(ctx, "Please re-enter access code", Toast.LENGTH_SHORT).show();
                    return;
                } else if (!binding.edtAccessCode.getText().toString().contentEquals(binding.edtReAccessCode.getText().toString())) {
                    Toast.makeText(ctx, "Access code mismatch", Toast.LENGTH_SHORT).show();
                    return;
                }
                try {
                    callApi();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void callApi() {

        //Required to check for staff
        // if staff need to pay

        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        String email = loginUserSP.getUserData().getUserEmail();
        String pass = loginUserSP.getUserData().getUserPassword();
        String token = loginUserSP.getUserData().getUserToken();
        String type = "student";
        String userid = "";
        try {
            userid = loginUserSP.getUserData().getUserId();
            type = loginUserSP.getUserTypeKey()==2?"staff":"student";
        }catch (Exception e){}
        try {
            userid = loginUserSP.getUserId()+"";
        }catch (Exception e){}

        Call<Login> accesscode = retrofitClass.verifyAccessCode(type, userid, binding.edtAccessCode.getText().toString().trim());
        accesscode.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                try {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        callApi1();
                    } else {
                        allMethods.setAlert(getActivity(), getResources().getString(R.string.app_name), response.body().getResponseMsg());
                    }
                }catch (Exception e){
                    allMethods.setAlert(getActivity(), getResources().getString(R.string.app_name), response.body().getResponseMsg());
                }

            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }

    private void callApi1() {
//        Call<Login> accesscode = retrofitClass.applyAccessCode(LoginUser.getUserData().getStaffId(), LoginUser.getUserData().getStaffId());
        String email = loginUserSP.getUserData().getUserEmail();
        String pass = loginUserSP.getUserData().getUserPassword();
        String token = "";
        try {
            token = loginUserSP.getUserData().getUserToken();
        }catch (Exception e){}
        try {
            email = loginUserSP.getUserTypeKey() == 1 ? loginUserSP.getUserData().getUserEmail() : loginUserSP.getUserData().getStaffEmail();
            pass = loginUserSP.getUserTypeKey() == 1 ? loginUserSP.getUserData().getUserPassword() : loginUserSP.getUserData().getStaffPassword();
            token = loginUserSP.getLoginUserToken();
        }catch (Exception e) {}

        Call<Login> accesscode = retrofitClass.login(email, pass, "android", token);
        accesscode.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (response.body().getResponseCode().equals(String.valueOf(1))) {
                    if (response.body().getData() != null) {
                        loginUserSP.setUserData(response.body().getData());
                        if (response.body().getData().getUserType().equalsIgnoreCase("student")) {
                            /*Intent intent = new Intent(getActivity(), UserProcessWithWeb.class);
                            intent.putExtra("action", Constants.USER_WEB_PAYMENT);
                            startActivity(intent);
                            getActivity().finish();*/
                            if (response.body().getData().getUserIsAgree().equalsIgnoreCase("2")) {
                                Intent i = new Intent(getActivity(), TermsConditionActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                        Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                getActivity().finish();
                            } else {
                                Intent i = new Intent(getActivity(), StudentHomeActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                        Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                getActivity().finish();
                            }
                        } else if (response.body().getData().getUserType().equalsIgnoreCase("yb_student")) {
                            if (response.body().getData().getUserIsAgree().equalsIgnoreCase("2")) {
                                Intent i = new Intent(getActivity(), TermsConditionActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                        Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                getActivity().finish();
                            } else {
                                Intent i = new Intent(getActivity(), StudentHomeActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                        Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                getActivity().finish();
                            }
                        } else if (response.body().getData().getUserType().equalsIgnoreCase("staff")) {
                            if (response.body().getData().getStaffIsAgree().equalsIgnoreCase("2")) {
                                Intent i = new Intent(getActivity(), TermsConditionStaffActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                        Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                getActivity().finish();
                            } else {
                                Intent i = new Intent(getActivity(), StaffHomeActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                        Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                getActivity().finish();
                            }
                        } else if (response.body().getData().getUserType().equalsIgnoreCase("employee") || response.body().getData().getUserType().equalsIgnoreCase("employee")) {
                            Intent i = new Intent(getActivity(), EmployeeHomeActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                    Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            allMethods.setAlert(getActivity(), getResources().getString(R.string.app_name), response.body().getResponseMsg());
                        }

                    } else {
                        allMethods.setAlert(getActivity(), getResources().getString(R.string.app_name), response.body().getResponseMsg());
                    }
                } else {
                    allMethods.setAlert(getActivity(), getResources().getString(R.string.app_name), response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }
}

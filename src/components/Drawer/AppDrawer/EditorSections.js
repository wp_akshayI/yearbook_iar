import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import DrawerList from '../DrawerList'
import AddIcon from '@material-ui/icons/Add'
import DragHandle from '@material-ui/icons/DragHandle'
import ComponentsList from './ComponentsList'
import { global, colors } from '../../../utils'
import DrawerBacknav from '../../DrawerBacknav'
import RemoveIcon from '@material-ui/icons/Clear'
import IconButton from '@material-ui/core/IconButton'
import List from 'react-smooth-draggable-list'
import EditIcon from '@material-ui/icons/Edit'

export default function EditorSections ({ obj }) {
  const classes = useStyles()

  const [state, setState] = useState({
    order: null,
    items: null
  })

  useEffect(() => {
    _sections()
  }, [])

  const _sections = () => {
    let data = obj.state.selected && obj.state.selected.filter(item => item.component_type !== 9)
    const order = []
    data = data.map((item, index) => {
      order.push(index)
      return item
      // return { ...item, component_order_by: index }
    })
    // data.map(item => order.push(item.component_order_by))
    setState({ items: data, order })
  }

  const _removeSection = (order) => {
    // console.log('Drawer ', order)
    obj._removeSection(order.component_order_by)
    setTimeout(() => _sections(), 1)
  }

  const _reorder = (newOrder) => {
    // const { items } = state
    // const newItems = []
    // newOrder.map((order, i) => {
    //   const itemIndex = items.findIndex(i1 => i1.component_order_by === order)
    //   newItems.push({ ...items[itemIndex] })
    // })
    // const data = []
    // newItems.map((item, index) => data.push({ ...item, new_component_order_by: index }))
    // obj._reorderSection(data)
    // setState({ ...state, items: newItems, order: newOrder })
    setState((preState) => {
      const newItems = []
      newOrder.map((order, i) => {
        const itemIndex = preState.items.findIndex(i1 => i1.component_order_by === order)
        newItems.push(preState.items[itemIndex])
      })
      // const data = []
      // preState.items.map((item, index) => data.push({ ...item, new_component_order_by: index }))

      obj._reorderSection(newItems)
      return {
        ...preState,
        order: newOrder
      }
    })
  }

  const rednerItem = (item, index) => {
    const component = global.components.filter(i => i.component_type === item.component_type)
    return (
      <List.Item key={String(index)}>
        <DrawerList
          title={(index + 1) + '. ' + component[0].name}
          id={item.component_id}
          order={item.component_order_by}
          key={item.component_order_by}
          classes={{ root: classes.root }}
          action={() => {
            // console.log(item)
            obj.editComponentSettings(item.component_order_by)
          }}
          left={
            <IconButton onClick={() => null}>
              <DragHandle style={{ color: colors.white, fontSize: 18 }} />
            </IconButton>
          }
          right={
            <IconButton onClick={() => _removeSection(item)}>
              <RemoveIcon style={{ color: colors.white, fontSize: 18 }} />
            </IconButton>
          }
        />
      </List.Item>
    )
  }
  const ybSplashscreen = obj.state.ybSplashscreen
  return (
    <>
      <DrawerBacknav title='List of Sections' />
      <div>
        {
          ybSplashscreen && (
            <DrawerList
              title='Image Title Section'
              id={ybSplashscreen.component_id}
              order={ybSplashscreen.component_order_by}
              key={ybSplashscreen.component_order_by}
              // action={() => obj.editComponentSettings(ybSplashscreen.component_order_by)}
              right={
                <IconButton onClick={() => obj.editYBSection()}>
                  <EditIcon style={{ color: colors.white, fontSize: 18 }} />
                </IconButton>
              }
            />
          )
        }
        {
          state.items && state.items.length > 0 && (
            <List
              rowHeight={50}
              order={state.order}
              onReOrder={_reorder}
            >
              {
                state.items.map(rednerItem)
              }
            </List>
          )
        }
        <DrawerList
          right={
            <IconButton
              onClick={() => obj.setState({ drawer: <ComponentsList obj={obj} />, isBack: obj.state.drawer })}
            >
              <AddIcon style={{ color: colors.white, fontSize: 18 }} />
            </IconButton>
          }
          title='Add new Section'
          action={() => obj.setState({ drawer: <ComponentsList obj={obj} />, isBack: obj.state.drawer })}
        />
      </div>
    </>
  )
}

const useStyles = makeStyles(theme => ({
  root: {
    paddingLeft: 0,
    paddingRight: 0
  },
  row: {
    display: 'flex',
    alignItems: 'center'
  }
}))

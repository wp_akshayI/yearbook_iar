package com.app.yearbook.restclient;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.BaseUrl;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static Retrofit retrofit = null;
    private static String Token="",Id="",UserType="";
    public static Retrofit getClient() {
        try {
            if (LoginUser.getUserData() != null) {
                UserType = LoginUser.getUserData().getUserType();

                if (UserType.equalsIgnoreCase("student") || UserType.equalsIgnoreCase("yb_student")) {
                    Id = LoginUser.getUserData().getUserId();
                    Token = LoginUser.getUserData().getUserToken();
                } else if (UserType.equalsIgnoreCase("employee")) {
                    Id = LoginUser.getUserData().getEmployeeId();
                    Token = LoginUser.getUserData().getEmployeeToken();
                } else {
                    Id = LoginUser.getUserData().getStaffId();
                    Token = LoginUser.getUserData().getStaffToken();
                }

                Log.d("TTT", "Client Id: " + Token + " / " + Id + " / " + UserType);
            } else {
                Log.d("TTT", "Client SP null");
            }
        }catch (Exception e){
            Id = "";
            Token = "";
        }

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        .addInterceptor(interceptor)
        //OkHttpClient client = new OkHttpClient.Builder().build();

//        1 Id
//        2 Token
//        3 Flag(user/staff)

        OkHttpClient.Builder client = new OkHttpClient.Builder().connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS).addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder()
                                .header("Content-Type", "application/x-www-form-urlencoded")
//                                .header("Accept-Charset","utf-8")
                                .header("Id", Id)
                                .header("Token", Token==null?"":Token)
                                .header("Flag", UserType==null?"":UserType)
                                .header("Cookie", "cookieKey" + "=" + "cookieValue")
                                .build();

                        return chain.proceed(request);
                    }
                })
                .addNetworkInterceptor(interceptor);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.addNetworkInterceptor(loggingInterceptor);

//        CookieManager cookieManager = new CookieManager();
//        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
//        client.cookieJar(new JavaNetCookieJar(cookieManager));



//        client.cookieJar(JavaNetCookieJar(new CookieManager().setCookiePolicy(CookiePolicy.ACCEPT_ALL)));

        BaseUrl baseUrl=new BaseUrl();
        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl.getUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();

        return retrofit;
    }

    static final class CookieInterceptor implements Interceptor {
        private volatile String cookie;

        public void setSessionCookie(String cookie) {
            this.cookie = cookie;
        }

        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            if (this.cookie != null) {
                request = request.newBuilder()
                        .header("Cookie", this.cookie)
                        .build();
            }
            return chain.proceed(request);
        }
    }
}

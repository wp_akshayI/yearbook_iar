package com.app.yearbook.fragment;


import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.yearbook.ImageActivity;
import com.app.yearbook.NewImageVideoActivity;
import com.app.yearbook.NewPollActivity;
import com.app.yearbook.NewTextPostActivity;
import com.app.yearbook.R;
import com.app.yearbook.adapter.AdapterPostList;
import com.app.yearbook.databinding.FragmentStaffBookmarkBinding;
import com.app.yearbook.interfaces.OnMenuOptionClick;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.studenthome.LikeUnlike;
import com.app.yearbook.model.postmodels.BookmarkPostResponse;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.Constants;
import com.app.yearbook.utils.OptionMenuController;
//import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
//import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentBookmarkFragment extends Fragment {

    private static final String TAG = "StaffBookmarkFragment";
    FragmentStaffBookmarkBinding binding;
    RetrofitClass retrofitClass;
    List<PostListItem> PostListArray;
    boolean isProgress = false;


    public StudentBookmarkFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_staff_bookmark, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getAllBookmark();

        binding.swipyrefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                binding.swipyrefreshlayout.setRefreshing(true);
                isProgress = true;
                getAllBookmark();
                Log.d(TAG, "onRefresh: ");
            }
        });
    }

    private void getAllBookmark() {
        if (isProgress) {
            binding.progressBar.setVisibility(View.GONE);
            isProgress = false;
        }
        retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<BookmarkPostResponse> staffPostResponseCall = retrofitClass.getBookmark(Integer.parseInt(LoginUser.getUserData().getUserId()));
        staffPostResponseCall.enqueue(new Callback<BookmarkPostResponse>() {
            @Override
            public void onResponse(Call<BookmarkPostResponse> call, Response<BookmarkPostResponse> response) {

                BookmarkPostResponse staffPostResponse = response.body();
                if (staffPostResponse != null) {
                    if (PostListArray != null) {
                        PostListArray.clear();
                    }
                    PostListArray = staffPostResponse.getPostList();

                    if (PostListArray != null && PostListArray.size() > 0) {
                        for (PostListItem postListItem : PostListArray) {
                            postListItem.setPostBookmark("True");
                        }
                        AdapterPostList adapterPostList = new AdapterPostList(PostListArray, new OnRecyclerClick() {
                            @Override
                            public void onClick(int pos, int type) {
                                switch (type) {
                                    case Constants.TYPE_BOOKMARK:
                                        if (!PostListArray.get(pos).getPostBookmark()) {
                                            PostListArray.get(pos).setPostBookmark("True");
                                        } else {
                                            PostListArray.get(pos).setPostBookmark("False");
                                        }
                                        addPostBookmark(PostListArray.get(pos));
                                        PostListArray.remove(pos);
                                        binding.rcBookmarkFeeds.getAdapter().notifyItemRemoved(pos);
                                        if (PostListArray.size() == 0) {
                                            binding.txtNoDta.setVisibility(View.VISIBLE);
                                        }
                                        break;
                                    case Constants.TYPE_VIEW_IMAGE:
                                        Intent i = new Intent(getActivity(), ImageActivity.class);
                                        i.putExtra("caption", PostListArray.get(pos).getPostDescription());
                                        i.putExtra("FileUrl", PostListArray.get(pos).getPostFile());
                                        startActivity(i);
                                        break;
                                    case Constants.TYPE_LIKE:
                                        if (!PostListArray.get(pos).getPostLike()) {
                                            PostListArray.get(pos).setPostLike("True");
                                            String likes = String.valueOf(Integer.parseInt(PostListArray.get(pos).getTotalLike()) + 1);
                                            PostListArray.get(pos).setTotalLike(likes);
                                        } else {
                                            PostListArray.get(pos).setPostLike("False");
                                            String likes = String.valueOf(Integer.parseInt(PostListArray.get(pos).getTotalLike()) - 1);
                                            PostListArray.get(pos).setTotalLike(likes);
                                        }
                                        addPostLikeDislike(PostListArray.get(pos));
                                        break;
                                }
                            }

                            @Override
                            public void onVote(int pos, String pollOptionId) {
                                addPollVote(pos, pollOptionId);
                            }
                        });
                        binding.rcBookmarkFeeds.setAdapter(adapterPostList);
                        OptionMenuController controller = new OptionMenuController(getActivity(), adapterPostList, (ArrayList<PostListItem>) PostListArray);
                        controller.setOptionMenu(new OnMenuOptionClick() {
                            @Override
                            public void onClick(int pos, int type, View view) {
                                if (PostListArray.get(pos).getPostType().equalsIgnoreCase(Constants.POST_TEXT_INT)) {
                                    startActivityForResult(new Intent(getActivity(), NewTextPostActivity.class)
                                            .putExtra(Constants.INTENT_ISEDIT, true)
                                            .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), Constants.NEW_POST);
                                } else if (PostListArray.get(pos).getPostType().equalsIgnoreCase(Constants.POST_IMAGE_VIDEO_INT)) {
                                    startActivityForResult(new Intent(getActivity(), NewImageVideoActivity.class)
                                            .putExtra(Constants.POST_TYPE, (PostListArray.get(pos).getPostFileType().equalsIgnoreCase("1")) ? Constants.POST_IMAGE : Constants.POST_VIDEO)
                                            .putExtra(Constants.INTENT_ISEDIT, true)
                                            .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), Constants.NEW_POST);
                                } else {
                                    startActivityForResult(new Intent(getActivity(), NewPollActivity.class)
                                            .putExtra(Constants.INTENT_ISEDIT, true)
                                            .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), Constants.NEW_POST);
                                }
                            }
                        });
                    } else {
                        binding.txtNoDta.setVisibility(View.VISIBLE);
                    }

                    binding.swipyrefreshlayout.setRefreshing(false);
                    binding.progressBar.setVisibility(View.GONE);
                }
                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<BookmarkPostResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
                binding.swipyrefreshlayout.setRefreshing(false);
                binding.progressBar.setVisibility(View.GONE);
                binding.txtNoDta.setVisibility(View.VISIBLE);
            }
        });
    }

    private void addPollVote(final int pos, String pollOptionId) {
        Call<Login> addBookMark = retrofitClass.getUserVotePoll(pollOptionId, LoginUser.getUserId()+"", LoginUser.getUserTypeKey());
        addBookMark.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Log.d(TAG, "onResponse: ");
                binding.rcBookmarkFeeds.getAdapter().notifyItemChanged(pos);
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }

    private void addPostLikeDislike(PostListItem postListItem) {
        Call<LikeUnlike> addBookMark = retrofitClass.postLikeDislike(Integer.parseInt(LoginUser.getUserData().getUserId()), Integer.parseInt(postListItem.getPostId()), LoginUser.getUserTypeKey());
        addBookMark.enqueue(new Callback<LikeUnlike>() {
            @Override
            public void onResponse(Call<LikeUnlike> call, Response<LikeUnlike> response) {
                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<LikeUnlike> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }

    private void addPostBookmark(PostListItem postListItem) {
        Call<Login> addBookMark = retrofitClass.addBookmarkUnbookmarkStudent(LoginUser.getUserData().getUserId(), postListItem.getPostId());
        addBookMark.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }

//    @Override
//    public void onRefresh(SwipyRefreshLayoutDirection direction) {
//        if (direction == SwipyRefreshLayoutDirection.TOP) {
//            binding.swipyrefreshlayout.setRefreshing(true);
//            isProgress=true;
//            //getAllBookmark();
//            Log.d("TTT", "SwipyRefreshLayoutDirection" + direction);
//        }
//    }
}

import React from "react"
import { useDispatch } from "react-redux"
import { colors, action } from "../utils";
import { makeStyles } from "@material-ui/core/styles";
import InputText from "../components/InputText";
import Actions from "../actions";
import DragAndDrop from "../components/DragAndDrop";


export default function YBImageTitleSettings({ data, updateComponents, obj }) {
    const dispatch = useDispatch();

    const _title = (e) => {
        let value = { component_title: e.target.value }
        updateComponents(value)
    }

    const _description = (e) => {
        let value = { component_description: e.target.value }
        updateComponents(value)
    }

    const _drop = (files) => {
        if (!files || files.length === 0) return;
        let file = files[0];
        uploadImage(file);
    }

    const uploadImage = async (file) => {
        try {
            let res = await dispatch(Actions.uploadImage(file));
            if (res.Result) {
                updateComponents({ component_url: res.Data.image_url })
            } else {
                dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: "warning" });
            }
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <div style={{ padding: 10, marginTop: 5 }}>
            <InputText
                title="Subtitle"
                defaultValue={data.component_title}
                onChange={_title}
            />
            <div style={{ height: 10 }} />
            <InputText
                title="Body Text"
                defaultValue={data.component_description}
                onChange={_description}
                multiline
                rows="4"
                labelWidth={75}
            />
            <div style={{ height: 10 }} />
            <DragAndDrop handleDrop={_drop}>
                <div style={{ height: 100, borderColor: "#c9c9c9", borderWidth: 1, borderStyle: "dashed", borderRadius: 4, display: "flex", justifyContent: "center", alignItems: "center" }}>
                    <div style={{ display: "grid" }}>
                        <span style={{ color: "#c9c9c9", fontSize: 14, textAlign: "center" }}>
                            Upload Image
                        </span>
                        <span style={{ color: "#c9c9c9", fontSize: 12, textAlign: "center", paddingTop: 5 }}>
                            Click Or Drag & Drop
                        </span>
                    </div>
                </div>
            </DragAndDrop>
        </div>
    )
}
import React from "react";
import { useSelector } from "react-redux"
import { withRouter } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import logo from "../assets/logo.png"
import moment from "moment";

function NavigationHeader({ location, history }) {
  const classes = useStyles();
  const { yearbookDetails } = useSelector(({ yearbook }) => yearbook);
  let year = "";
  if(yearbookDetails)
  year = moment(yearbookDetails.yearbook_created).format("YYYY") + " - " + (Number(moment(yearbookDetails.yearbook_created).format("YYYY")) + 1)

  const _navigate = (pathname) => {
    return history.push({ pathname })
  }

  return (
    <div >
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <div className={classes.sectionDesktop}>

            <Button
              classes={{ label: classes.buttonLable }}
              onClick={() => _navigate("/")}
              color="inherit">
              Home
            </Button>

            {/* <Button classes={{ label: classes.buttonLable }} color="inherit">Jump to page</Button> */}

          </div>

          <div className={classes.grow} />

          <Typography className={classes.title} variant="subtitle2" noWrap>
            Editing <span className={classes.hightlight}>
              {yearbookDetails && yearbookDetails.yearbook_name}  
            </span> Yearbook {year}  
          </Typography>


          <div className={classes.grow} />

          <div className={classes.sectionDesktop} style={{ width: 35 }}>
            <img src={logo} style={{ height: 25, objectFit: "contain" }}/>
          </div>

        </Toolbar>
      </AppBar>
    </div>
  );
}

export default withRouter(NavigationHeader)

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  title: {
    display: 'none',
    color: "#b3b3b3",
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  hightlight: {
    color: "#FFF",
  },
  search: {
    position: 'relative',
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
    borderBottomWidth: 1,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderStyle: "solid"
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 1),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 300,
    },
  },
  sectionDesktop: {
    // display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  subtitle2: {
    color: "#b3b3b3"
  },
  buttonLable: {
    textTransform: "capitalize",
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    color: "#b3b3b3"
  },
}));
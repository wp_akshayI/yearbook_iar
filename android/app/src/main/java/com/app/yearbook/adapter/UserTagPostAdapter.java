package com.app.yearbook.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.yearbook.ImageActivity;
import com.app.yearbook.R;
import com.app.yearbook.model.studenthome.home.PostListItem;

import java.util.ArrayList;

import tcking.github.com.giraffeplayer2.GiraffePlayer;
import tcking.github.com.giraffeplayer2.Option;
import tcking.github.com.giraffeplayer2.VideoInfo;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

public class UserTagPostAdapter extends RecyclerView.Adapter<UserTagPostAdapter.MyViewHolder> {
    private ArrayList<PostListItem> userLists;
    private Activity ctx;

    @NonNull
    @Override
    public UserTagPostAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_userpost, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserTagPostAdapter.MyViewHolder holder, final int position) {
        final PostListItem userList = userLists.get(position);

        //post img
        if (userList.getPostFile() != null) {

            if (!userList.getPostFile().equals("")) {
                final String uri = userList.getPostFile();
                Log.d("TTT", "Url uri: " + uri);
                String extension = uri.substring(uri.lastIndexOf("."));
                Log.d("TTT", "Url type: " + extension);
                if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {
                    holder.imgPostVideo.setVisibility(View.GONE);
                    if (userList.getPostFile() != null) {
                        Glide.with(ctx)
                                .load(userList.getPostFile())
                                .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(holder.imgPost);
                    }
                } else {
                    holder.imgPostVideo.setVisibility(View.VISIBLE);
                    Glide.with(ctx)
                            .load(userList.getPostThumbnail())
                            .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(holder.imgPost);
                }

                final String path = userList.getPostFile();
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // Toast.makeText(ctx,"Type: "+userList.getPostFileType(),Toast.LENGTH_SHORT).show();
                        if(userList.getPostFileType().equalsIgnoreCase("2")) {
                        VideoInfo videoInfo = new VideoInfo(Uri.parse(userList.getPostFile()))
                                .setTitle(userList.getStaffLastname())
                                .setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT)
                                .addOption(Option.create(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "infbuf", 1L))
                                .addOption(Option.create(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "multiple_requests", 1L))
                                .setShowTopBar(true);

                        GiraffePlayer.play(ctx, videoInfo);
                        ctx.overridePendingTransition(0,0);
                        }
                        else {
                            Intent i = new Intent(ctx, ImageActivity.class);
                            i.putExtra("caption",userList.getPostDescription());
                            i.putExtra("FileUrl", path);
                            ctx.startActivity(i);
                        }
                    }
                });
            }
        }
    }

    public UserTagPostAdapter(ArrayList<PostListItem> userLists, Activity context) {
        this.userLists = userLists;
        this.ctx = context;
    }

    @Override
    public int getItemCount() {
        return userLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPost, imgPostVideo;

        public MyViewHolder(View view) {
            super(view);
            imgPost = view.findViewById(R.id.imgPost);
            imgPostVideo = view.findViewById(R.id.imgPostVideo);

        }
    }
}

package com.app.yearbook.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.yearbook.NotificationDetailActivity;
import com.app.yearbook.R;
import com.app.yearbook.StudentYearbookViewPostActivity;
import com.app.yearbook.databinding.ItemUserpostbookmarkBinding;
import com.app.yearbook.model.profile.BookmarkListItem;

import java.util.ArrayList;

public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.MyViewHolder> {

    private ArrayList<BookmarkListItem> bookmarkLists;
    private Activity ctx;
    public ItemUserpostbookmarkBinding binding;

    @NonNull
    @Override
    public BookmarkAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                , R.layout.item_userpostbookmark, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final BookmarkAdapter.MyViewHolder holder, final int position) {
        holder.binding.setPost(bookmarkLists.get(position));

        //post img
        if (holder.binding.getPost().getPostFile() != null || !holder.binding.getPost().getPostFile().equals("")) {
            final String uri = holder.binding.getPost().getPostFile();
            String extension = uri.substring(uri.lastIndexOf("."));
            Log.d("TTT", "Url type: " + extension);

            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {
                holder.binding.imgPostVideo.setVisibility(View.GONE);
                if (holder.binding.getPost().getPostFile() != null) {
                    Glide.with(ctx)
                            .load(holder.binding.getPost().getPostFile())
                            .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(holder.binding.imgPost);
                }
            } else {
                holder.binding.imgPostVideo.setVisibility(View.VISIBLE);
                Glide.with(ctx)
                        .load(holder.binding.getPost().getPostThumbnail())
                        .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.mipmap.home_post_img_placeholder)
                        .error(R.mipmap.home_post_img_placeholder)
                        .into(holder.binding.imgPost);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(holder.binding.getPost().getFlag().equalsIgnoreCase("staff"))
                    {
//                        StaffYBPostList list=new StaffYBPostList();
//                        list.setPostId(holder.binding.getPost().getPostId());
//                        list.setStaffId(holder.binding.getPost().getStaffId());
//                        list.setSchoolId(holder.binding.getPost().getSchoolId());
//                        list.setCategoryId(holder.binding.getPost().getCategoryId());
//                        list.setPostDescription(holder.binding.getPost().getUserFirstname());
//                        list.setFileType(holder.binding.getPost().getFileType());
//                        list.setPostFile(holder.binding.getPost().getPostFile());
//                        list.setPostThumbnail(holder.binding.getPost().getPostThumbnail());
//                        list.setFileSizeType(holder.binding.getPost().getFileSizeType());
//                        list.setPostDate(holder.binding.getPost().getPostDate());
//                        list.setTotalLike(holder.binding.getPost().getTotalLike());
//                        list.setTotalComment(holder.binding.getPost().getTotalComment());
//                        list.setPostLike(holder.binding.getPost().getPostLike());
//                        list.setPostComment(holder.binding.getPost().getPostComment());
//                        list.setPostBookmark(holder.binding.getPost().getPostBookmark());
//                        list.setUserTagg(holder.binding.getPost().getUserTagg());
//                        list.setFlag(holder.binding.getPost().getFlag());

                        Intent i = new Intent(ctx, StudentYearbookViewPostActivity.class);
                        i.putExtra("PostId",holder.binding.getPost().getPostId());
                        //i.putExtra("StudentYBPostList", list);
                        ctx.startActivity(i);
                    }
                    else
                    {
//                        PostList list=new PostList();
//                        list.setPostLike(holder.binding.getPost().getPostLike());
//                        list.setPostBookmark(holder.binding.getPost().getPostBookmark());
//                        list.setPostPush(holder.binding.getPost().getPostPush());
//                        list.setUserFirstname(holder.binding.getPost().getUserFirstname());
//                        list.setUserLastname(holder.binding.getPost().getUserLastname());
//                        list.setUserImage(holder.binding.getPost().getUserImage());
//                        list.setPostId(holder.binding.getPost().getPostId());
//                        list.setUserId(holder.binding.getPost().getStaffId());
//                        list.setSchoolId(holder.binding.getPost().getSchoolId());
//                        list.setPostDescription(holder.binding.getPost().getUserFirstname());
//                        list.setPostFile(holder.binding.getPost().getPostFile());
//                        list.setPostFileType(holder.binding.getPost().getPostFileType());
//                        list.setPostType(holder.binding.getPost().getPostType());
//                        list.setPostThumbnail(holder.binding.getPost().getPostThumbnail());
//                        list.setPostDate(holder.binding.getPost().getPostDate());
//                        list.setTotalLike(holder.binding.getPost().getTotalLike());
//                        list.setTotalComment(holder.binding.getPost().getTotalComment());
//                        list.setFlag(holder.binding.getPost().getFlag());
//                        list.setUserTagg(holder.binding.getPost().getUserTagg());
//
//                        Intent i = new Intent(ctx, StudentPostEditActivity.class);
//                        i.putExtra("PostObject", list);
//                        ctx.startActivity(i);

                        Intent i = new Intent(ctx, NotificationDetailActivity.class);
                        i.putExtra("postID", holder.binding.getPost().getPostId());
                        ctx.startActivity(i);

                    }
                }
            });
        }
    }

    public BookmarkAdapter(ArrayList<BookmarkListItem> bookmarkLists, Activity context) {
        this.bookmarkLists = bookmarkLists;
        this.ctx = context;
    }

    @Override
    public int getItemCount() {
        return bookmarkLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemUserpostbookmarkBinding binding;

        public MyViewHolder(ItemUserpostbookmarkBinding view) {
            super(view.getRoot());
            binding=view;
        }
    }
}

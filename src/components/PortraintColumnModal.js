import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Image, FlatList } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { colors } from '../utils'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Modal from 'modal-enhanced-react-native-web'
import Actions from '../actions'
import { image_placeholder } from '../assets'
import { FormControl, InputLabel, NativeSelect, Fab } from '@material-ui/core'
import InputBase from '@material-ui/core/InputBase'

export default function PortraintColumnModal ({ isVisible, handleClose, title, onSubmit, student_list, staff_list }) {
  const classes = useStyles()
  const dispatch = useDispatch()
  const { grades } = useSelector(({ components }) => components)

  const [state, setState] = useState({
    userList: null,
    grade_name: null
  })

  useEffect(() => {
    _getUserlist()
    dispatch(Actions.get_students_grade())
  }, [])

  const _getUserlist = async (grade_name = '') => {
    let res = await dispatch(Actions.get_user_images(grade_name))
    res = res || []
    res = res.map(item => {
      const isStudentExist = student_list.filter((s) => s.id === item.id && item.type === 1)
      if (isStudentExist.length > 0) item.is_selected = true
      const isStaffExist = staff_list.filter((s) => s.id === item.id && item.type === 2)
      if (isStaffExist.length > 0) item.is_selected = true
      return item
    })
    setState({ ...state, userList: res || [] })
  }

  const _selectUser = (index) => {
    let { userList } = state
    userList = userList.map((item, i) => {
      if (i === index) item.is_selected = !item.is_selected
      return item
    })
    setState({ ...state, userList })
  }

  const _selectionDone = () => {
    const userList = state.userList
    const students = []; const staff = []
    userList.map((item) => {
      if (item.is_selected) {
        if (item.type === 1) students.push(item)
        if (item.type === 2) staff.push(item)
      }
    })
    onSubmit({ students, staff })
  }

  const _changeGrade = (val) => {
    setState({ ...state, grade_name: val.target.value })
    if (val.target.value === 0) {
      _getUserlist()
    } else {
      _getUserlist(val.target.value)
    }
  }

  const renderItem = ({ item, index }) => {
    const { image, firstname, lastname, is_selected } = item
    const uri = image || image_placeholder
    return (
      <View
        onClick={() => _selectUser(index)}
        style={{ margin: 10, borderWidth: 3, borderColor: is_selected ? '#0561C3' : 'transparent' }}
      >
        <View style={styles.listTitle}>
          <Text
            numberOfLines={1}
            ellipsizeMode='tail'
          >
            {title}
          </Text>
        </View>
        <Image
          resizeMode='cover'
          style={{
            height: 170,
            width: 170
          }}
          source={{ uri }}
        />
        <View style={styles.listTitle}>
          <Text
            numberOfLines={1}
            ellipsizeMode='tail'
          >
            {firstname} {lastname}
          </Text>
        </View>
      </View>
    )
  }

  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={handleClose}
      style={{ alignItems: 'center', margin: 30 }}
    >
      <View style={styles.modal}>

        <View style={styles.drawer}>

          <Fab
            variant='extended'
            size='medium'
            color='primary'
            classes={{ label: classes.buttonLable, sizeMedium: classes.buttonSize }}
            onClick={_selectionDone}
          >
                        Done
          </Fab>
          <View style={styles.divider} />
          <FormControl fullWidth>
            <InputLabel htmlFor='demo-customized-select-native' style={{ color: '#c9c9c9' }}>Student Grade</InputLabel>
            <NativeSelect
              id='demo-customized-select-native'
              input={<BootstrapInput />}
              value={state.grade_name}
              onChange={_changeGrade}
            >
              <option className={classes.option} value={0}>Select Grade</option>
              {
                grades && grades.map((item, index) => (<option className={classes.option} value={item.grade} key={index}>{item.grade}</option>))
              }
            </NativeSelect>
          </FormControl>
          <View style={styles.divider} />
          <Text style={styles.drawerBody}>
                        Any images that are added will be automatically sorted alphabetically.
          </Text>
          <View style={styles.divider} />
          <Text style={styles.drawerBody}>
                        Tip: you can adjust names of images from the list on the right
          </Text>
        </View>
        <View style={{ flex: 1 }}>
          <View style={{ width: '100%' }}>
            <Text style={styles.title}>{title}</Text>
          </View>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <FlatList
              data={state.userList || []}
              numColumns={5}
              keyExtractor={(item, index) => String(index)}
              renderItem={renderItem}
            />
          </View>
        </View>
      </View>
    </Modal>
  )
}

const useStyles = makeStyles((theme) => ({

  buttonLable: {
    textTransform: 'capitalize',
    color: colors.white
  },
  buttonSize: {
    width: '100% !important'
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'auto',
    // backgroundColor: "#B8BCBF",
    width: '75%'
  },
  gridList: {
    // width: 320,
    padding: 5,
    margin: '0px !important'
  },
  imgFullHeight: {
    width: '100%',
    position: 'static',
    transform: 'translateX(0%)',
    height: 100
  },
  option: {
    backgroundColor: 'rgb(63, 63, 63) !important'
  }

}))

const styles = StyleSheet.create({
  modal: {
    backgroundColor: '#eeeeee',
    height: '100%',
    width: '100%',
    flexDirection: 'row'
  },
  drawer: {
    backgroundColor: colors.Gray,
    width: 265,
    padding: 20,
    paddingTop: 40
  },
  drawerBody: {
    color: colors.white,
    fontFamily: 'Calibri',
    fontSize: 16
  },
  divider: {
    padding: 15
  },
  title: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
    marginVertical: 20,
    fontFamily: 'Calibri'
  },
  listTitle: {
    width: 170,
    height: 25,
    padding: 5,
    backgroundColor: '#fff'
  }

})

const BootstrapInput = withStyles(theme => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3)
    }
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    border: '1px solid #ced4da',
    fontSize: 16,
    color: '#fff',
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)'
    }
  }
}))(InputBase)

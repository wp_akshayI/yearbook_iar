
package com.app.yearbook.model.studenthome;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostData {

    @SerializedName("post_Detail")
    @Expose
    private PostList postDetail;
    @SerializedName("comment")
    @Expose
    private Comment comment;

    public PostList getPostDetail() {
        return postDetail;
    }

    public void setPostDetail(PostList postDetail) {
        this.postDetail = postDetail;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

}

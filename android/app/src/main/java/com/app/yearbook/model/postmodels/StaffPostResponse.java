package com.app.yearbook.model.postmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class StaffPostResponse implements Parcelable {

    @SerializedName("ResponseCode")
    private String responseCode;

    @SerializedName("ServerTimeZone")
    private String serverTimeZone;

    @SerializedName("ResponseMsg")
    private String responseMsg;

    @SerializedName("serverTime")
    private String serverTime;

    @SerializedName("user_post")
    private UserPost userPost;

    @SerializedName("Result")
    private String result;

    protected StaffPostResponse(Parcel in) {
        responseCode = in.readString();
        serverTimeZone = in.readString();
        responseMsg = in.readString();
        serverTime = in.readString();
        userPost = in.readParcelable(UserPost.class.getClassLoader());
        result = in.readString();
    }

    public static final Creator<StaffPostResponse> CREATOR = new Creator<StaffPostResponse>() {
        @Override
        public StaffPostResponse createFromParcel(Parcel in) {
            return new StaffPostResponse(in);
        }

        @Override
        public StaffPostResponse[] newArray(int size) {
            return new StaffPostResponse[size];
        }
    };

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setServerTimeZone(String serverTimeZone) {
        this.serverTimeZone = serverTimeZone;
    }

    public String getServerTimeZone() {
        return serverTimeZone;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setUserPost(UserPost userPost) {
        this.userPost = userPost;
    }

    public UserPost getUserPost() {
        return userPost;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(responseCode);
        dest.writeString(serverTimeZone);
        dest.writeString(responseMsg);
        dest.writeString(serverTime);
        dest.writeParcelable(userPost, flags);
        dest.writeString(result);
    }
}
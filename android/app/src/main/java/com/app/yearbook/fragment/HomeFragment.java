package com.app.yearbook.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.yearbook.ImageActivity;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.NewImageVideoActivity;
import com.app.yearbook.NewPollActivity;
import com.app.yearbook.NewTextPostActivity;
import com.app.yearbook.R;
import com.app.yearbook.SearchActivity;
import com.app.yearbook.adapter.AdapterPostList;
import com.app.yearbook.adapter.StudentUserListAdapter;
import com.app.yearbook.databinding.NewPackDialogBinding;
import com.app.yearbook.interfaces.OnMenuOptionClick;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.studenthome.LikeUnlike;
import com.app.yearbook.model.studenthome.home.StaffListItem;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.model.postmodels.StaffPostResponse;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.Constants;
import com.app.yearbook.utils.OptionMenuController;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.ybq.android.spinkit.style.Circle;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.PlayerManager;

public class HomeFragment extends Fragment implements SwipyRefreshLayout.OnRefreshListener {

    private static final String TAG = "HomeFragment";
    public View view;
    public ProgressDialog progressDialog;
    private ArrayList<StaffListItem> userListArray;
    public static ArrayList<PostListItem> PostListArray;
    private RecyclerView rvPostList, rvUserList;
    private LoginUser loginUser;
    public int schoolId, userId, page = 1;
    public static AdapterPostList postListAdapter;
    public StudentUserListAdapter userListAdapter;
    public AllMethods utils;
    private LinearLayout lvNoData;
    private SwipyRefreshLayout swipyrefreshlayout;
    private NestedScrollView nestedScrollView;
    RetrofitClass retrofitClass;
    ConstraintLayout cvDim;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // ((StudentHomeActivity) getActivity()).getSupportActionBar().setTitle(((StudentHomeActivity) getActivity()).studentName+"");
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);
        View();
        return view;
    }

    public void View() {
        //utils
        utils = new AllMethods();

        //nested scroll view
        nestedScrollView = view.findViewById(R.id.nestedScrollView);

        //swipyrefreshlayout
        swipyrefreshlayout = view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setOnRefreshListener(this);

        //SP object
        loginUser = new LoginUser(getActivity());
        if (loginUser.getUserData() != null) {
            schoolId = Integer.parseInt(loginUser.getUserData().getSchoolId());
            userId = Integer.parseInt(loginUser.getUserData().getUserId());
        }

        //cv
        cvDim=view.findViewById(R.id.cvDim);

        //lv
        lvNoData = view.findViewById(R.id.lvNoData);

        //rv
        rvPostList = view.findViewById(R.id.rvPostList);
        rvUserList = view.findViewById(R.id.rvUserList);

        userListArray = new ArrayList<>();
        PostListArray = new ArrayList<>();

        retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        getStudentPost(page);
    }

    public void getStudentPost(int page) {
        Log.d("TTT", "Page: " + page);
        Log.d("TTT", "GetPost: " + userId + " / " + schoolId);
        if (page == 1) {
            //Progress bar
            progressDialog = ProgressDialog.show(getActivity(), "", "", true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.progress_view);
            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
            Circle bounce = new Circle();
            bounce.setColor(Color.BLACK);
            progressBar.setIndeterminateDrawable(bounce);

            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
            final Call<StaffPostResponse> userPost = retrofitClass.getStudentPost(userId, schoolId, page);
            userPost.enqueue(new Callback<StaffPostResponse>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<StaffPostResponse> call, Response<StaffPostResponse> response) {
                    swipyrefreshlayout.setRefreshing(false);
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.body().getResponseCode().equals("1")) {
                        lvNoData.setVisibility(View.GONE);

                        if (response.body().getUserPost().getStaffList().size() > 0) {
                            Log.d("TTT", "User post: " + response.body().getUserPost().getStaffList().size());
                            /*userListArray.clear();
                            userListArray.addAll(response.body().getUserPost().getStaffList());
                            userListAdapter = new StudentUserListAdapter(userListArray, getActivity(), "student");
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                            rvUserList.setLayoutManager(mLayoutManager);
                            rvUserList.setAdapter(userListAdapter);*/
                        }
                        if (response.body().getUserPost().getPostList().size() > 0) {
                            PostListArray.clear();
                            PostListArray.addAll(response.body().getUserPost().getPostList());
                            postListAdapter = new AdapterPostList(PostListArray, new OnRecyclerClick() {

                                @Override
                                public void onVote(int pos, String pollOptionId) {
                                    addPollVote(pos, pollOptionId);
                                }

                                @Override
                                public void onClick(int pos, int type) {
                                    switch (type) {
                                        case Constants.TYPE_BOOKMARK:
                                            if (!PostListArray.get(pos).getPostBookmark()) {
                                                PostListArray.get(pos).setPostBookmark("True");
                                            } else {
                                                PostListArray.get(pos).setPostBookmark("False");
                                            }
                                            addPostBookmark(PostListArray.get(pos));
                                            break;
                                        case Constants.TYPE_LIKE:
                                            if (!PostListArray.get(pos).getPostLike()) {
                                                PostListArray.get(pos).setPostLike("True");
                                                String likes = String.valueOf(Integer.parseInt(PostListArray.get(pos).getTotalLike()) + 1);
                                                PostListArray.get(pos).setTotalLike(likes);
                                            } else {
                                                PostListArray.get(pos).setPostLike("False");
                                                String likes = String.valueOf(Integer.parseInt(PostListArray.get(pos).getTotalLike()) - 1);
                                                PostListArray.get(pos).setTotalLike(likes);
                                            }
                                            addPostLikeDislike(PostListArray.get(pos));
                                            break;
                                        case Constants.TYPE_VIEW_IMAGE:
                                            Intent i = new Intent(getActivity(), ImageActivity.class);
                                            i.putExtra("caption", PostListArray.get(pos).getPostSubTitle());
                                            i.putExtra("FileUrl", PostListArray.get(pos).getPostFile());
                                            startActivity(i);
                                            break;
                                    }
                                }
                            });
                            final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            rvPostList.setLayoutManager(mLayoutManager);
                            rvPostList.setAdapter(postListAdapter);
                            OptionMenuController optionMenuController = new OptionMenuController(getContext(), postListAdapter, PostListArray);
                            optionMenuController.setOptionMenu(new OnMenuOptionClick() {
                                @Override
                                public void onClick(int pos, int type, View view) {
                                    if (PostListArray.get(pos).getPostType().equalsIgnoreCase(Constants.POST_TEXT_INT)) {
                                        startActivityForResult(new Intent(getActivity(), NewTextPostActivity.class)
                                                .putExtra(Constants.INTENT_ISEDIT, true)
                                                .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), Constants.NEW_POST);
                                    } else if (PostListArray.get(pos).getPostType().equalsIgnoreCase(Constants.POST_IMAGE_VIDEO_INT)) {
                                        startActivityForResult(new Intent(getActivity(), NewImageVideoActivity.class)
                                                .putExtra(Constants.POST_TYPE, (PostListArray.get(pos).getPostFileType().equalsIgnoreCase("1")) ? Constants.POST_IMAGE : Constants.POST_VIDEO)
                                                .putExtra(Constants.INTENT_ISEDIT, true)
                                                .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), Constants.NEW_POST);
                                    } else {
                                        startActivityForResult(new Intent(getActivity(), NewPollActivity.class)
                                                .putExtra(Constants.INTENT_ISEDIT, true)
                                                .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), Constants.NEW_POST);
                                    }
                                }
                            });

//                       nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//
//                           @Override
//                           public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                               View view =v.getChildAt(v.getChildCount() - 1);
//                               int diff = (view.getBottom() - (v.getHeight() + v.getScrollY()));
//                              Log.d("TTT","Diff: "+view.getId());
//                           }
//                       });

//                            rvPostList.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                                @Override
//                                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                                    //super.onScrollStateChanged(recyclerView, newState);
//                                    Log.d("TTT","Scroll: "+newState+" / "+SCROLL_STATE_IDLE);
//                                    if (newState == SCROLL_STATE_IDLE) {
//
//                                        int playPosition = mLayoutManager.findFirstVisibleItemPosition();
//                                        Log.d("TTT","playPosition: "+playPosition);
//                                        if (playPosition == -1) {//no visible item
//                                            return;
//                                        }
//                                        int firstCompletelyVisibleItemPosition = mLayoutManager.findFirstCompletelyVisibleItemPosition();
//                                        int lastCompletelyVisibleItemPosition = mLayoutManager.findLastCompletelyVisibleItemPosition();
//
//                                        for (int i = firstCompletelyVisibleItemPosition; i <=lastCompletelyVisibleItemPosition; i++) {
//                                            View viewByPosition = mLayoutManager.findViewByPosition(i);
//                                            if (viewByPosition != null) {
//                                                VideoView videoView = viewByPosition.findViewById(R.id.video_view);
//                                                if (videoView!=null && videoView.isCurrentActivePlayer()) {
//                                                    return;//current active player is visible,do nothing
//                                                }
//                                            }
//                                        }
//
//                                        //try find first visible item (visible part > 50%)
//                                        if (firstCompletelyVisibleItemPosition >= 0 && playPosition != firstCompletelyVisibleItemPosition) {
//                                            int[] recyclerView_xy = new int[2];
//                                            int[] f_xy = new int[2];
//
//                                            VideoView videoView = mLayoutManager.findViewByPosition(playPosition).findViewById(R.id.video_view);
//                                            videoView.getLocationInWindow(f_xy);
//                                            recyclerView.getLocationInWindow(recyclerView_xy);
//                                            int unVisibleY = f_xy[1] - recyclerView_xy[1];
//
//                                            if (unVisibleY < 0 && Math.abs(unVisibleY) * 1.0 / videoView.getHeight() > 0.5) {//No visible part > 50%,play next
//                                                playPosition = firstCompletelyVisibleItemPosition;
//                                            }
//                                        }
//                                        VideoView videoView =mLayoutManager.findViewByPosition(playPosition).findViewById(R.id.video_view);
//                                        if (videoView != null) {
//                                            videoView.getPlayer().start();
//                                        }
//
//                                    }
//                                }
//
//                                @Override
//                                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                                    super.onScrolled(recyclerView, dx, dy);
//                                }
//                            });
                        }
                    } else if (response.body().getResponseCode().equals("10")) {
                        Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        LoginUser loginSP = new LoginUser(getActivity());
                        loginSP.clearData();

                        Intent i = new Intent(getActivity(), LoginActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        lvNoData.setVisibility(View.VISIBLE);
                        // utils.setAlert(getActivity(),"",response.body().getResponseMsg());
                    }

                }

                @Override
                public void onFailure(Call<StaffPostResponse> call, Throwable t) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    utils.setAlert(getActivity(), "", t.getMessage());
                }
            });
        } else {
            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
            final Call<StaffPostResponse> userPost = retrofitClass.getStudentPost(userId, schoolId, page);
            userPost.enqueue(new Callback<StaffPostResponse>() {
                @Override
                public void onResponse(Call<StaffPostResponse> call, Response<StaffPostResponse> response) {
                    swipyrefreshlayout.setRefreshing(false);
                    if (response.body().getResponseCode().equals("1")) {
//                        if (response.body().getUserPost().getUserList().size() > 0) {
//                            Log.d("TTT", "User post: " + response.body().getUserPost().getUserList().size());
//                            userListArray.addAll(response.body().getUserPost().getUserList());
//                            rvUserList.getAdapter().notifyDataSetChanged();
//                        }
                        if (response.body().getUserPost().getPostList().size() > 0) {
                            PostListArray.addAll(response.body().getUserPost().getPostList());
                            rvPostList.getAdapter().notifyItemInserted(PostListArray.size() - response.body().getUserPost().getPostList().size());
                        }
                    } else if (response.body().getResponseCode().equals("10")) {
                        Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        LoginUser loginSP = new LoginUser(getActivity());
                        loginSP.clearData();

                        Intent i = new Intent(getActivity(), LoginActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    }

                }

                @Override
                public void onFailure(Call<StaffPostResponse> call, Throwable t) {
                    swipyrefreshlayout.setRefreshing(false);
                    utils.setAlert(getActivity(), "", t.getMessage());
                }
            });
        }
    }

    private void addPollVote(final int pos, String pollOptionId) {
//        postListAdapter.notifyItemChanged(pos);
        Call<Login> addBookMark = retrofitClass.getUserVotePoll(pollOptionId, LoginUser.getUserId()+"", LoginUser.getUserTypeKey());
        addBookMark.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Log.d(TAG, "onResponse: "+ pos);
                postListAdapter.notifyItemChanged(pos);
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }

    private void addPostLikeDislike(PostListItem postListItem) {
        Call<LikeUnlike> addBookMark = retrofitClass.postLikeDislike(Integer.parseInt(loginUser.getUserData().getUserId()),
                Integer.parseInt(postListItem.getPostId()),
                loginUser.getUserTypeKey());
        addBookMark.enqueue(new Callback<LikeUnlike>() {
            @Override
            public void onResponse(Call<LikeUnlike> call, Response<LikeUnlike> response) {
                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<LikeUnlike> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }

    private void addPostBookmark(PostListItem postListItem) {
        Call<Login> addBookMark = retrofitClass.addBookmarkUnbookmarkStudent(loginUser.getUserData().getUserId(), postListItem.getPostId());
        addBookMark.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        PlayerManager.getInstance().onConfigurationChanged(newConfig);
        Log.d("TTT", "OnConfigggg");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);\
        Log.d("TTT", "Home activityyyyy");
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            Log.d("TTT", "bottom swipeeee");
            swipyrefreshlayout.setRefreshing(true);
            page = page + 1;
            getStudentPost(page);
        } else if (direction == SwipyRefreshLayoutDirection.TOP) {
            swipyrefreshlayout.setRefreshing(true);
            page = 1;
            getStudentPost(page);
            Log.d("TTT", "SwipyRefreshLayoutDirection" + direction);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
//        if(postListAdapter!=null)
//        {
//            StudentPostListAdapter.handler.sendEmptyMessage(101);
//        }

        if (PlayerManager.getInstance().getCurrentPlayer() != null) {
            PlayerManager.getInstance().getCurrentPlayer().stop();
        }
        Log.d("TTT", "OnPauseeee home fragment");
    }


    private void showDialog(int userId) {
//        LayoutInflater factory = LayoutInflater.from(getActivity());
//        final View deleteDialogView = factory.inflate(R.layout.custom_student_profile, null);
//        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
//        deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        deleteDialog.setView(deleteDialogView);
//        deleteDialog.show();

        NewPackDialogBinding binding;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        binding = DataBindingUtil.inflate(inflater, R.layout.new_pack_dialog, null, false);
        builder.setView(binding.getRoot());
        Dialog d = builder.create();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        if (loginUser.getUserData() != null) {
            binding.tvSchool.setText(loginUser.getUserData().getSchoolName());
            binding.tvUserName.setText(loginUser.getUserData().getUserFirstname() + "\n" + loginUser.getUserData().getUserLastname());
            binding.tvId.setText("ID# " + loginUser.getUserData().getUserStudentId());

            Glide.with(getActivity())
                    .load(loginUser.getUserData().getUserImage())
                    .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .placeholder(R.mipmap.home_post_img_placeholder)
                    .error(R.mipmap.home_post_img_placeholder)
                    .into(binding.imgProfile);
        }

        d.show();
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.profile, menu);
//        menuPhoto = menu.findItem(R.id.photo);
    }*/


    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.setting) {
            showDialog(userId);
        }
        return super.onOptionsItemSelected(item);
    }*/
    SearchView searchView;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.searchview, menu);
        // Associate searchable configuration with the SearchView
        final SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (androidx.appcompat.widget.SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setQueryHint("Search users, tags, post and polls");
        ImageView v = searchView.findViewById(R.id.search_button);
        v.setImageResource(R.drawable.ic_search);

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvDim.setVisibility(View.VISIBLE);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!query.equals("")) {

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(searchView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                    cvDim.setVisibility(View.GONE);
                    Intent intent = new Intent(getActivity(), SearchActivity.class);
                    intent.putExtra("query", query);
                    intent.putExtra("type", "student");
                    intent.putExtra("PostListArray", PostListArray);
                    startActivity(intent);

                    searchView.setQuery("", false);
                    searchView.clearFocus();
                    searchView.setIconified(true);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                Log.d(TAG, "onClose: ");
                cvDim.setVisibility(View.GONE);
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            /*if (StaffHomeActivity.handler != null) {
                StaffHomeActivity.handler.sendEmptyMessage(Constants.SEARCH);
            }*/
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            intent.putExtra("type", "student");
            intent.putExtra("PostListArray", PostListArray);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}

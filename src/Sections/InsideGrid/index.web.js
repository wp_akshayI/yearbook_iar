import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native'
import Actions from '../../actions'
import { global, action } from '../../utils'
import { nativePlaceholder } from '../../assets'
const screenWidth = global.width

export default function InsideGrid ({ navigation, screenId, data }) {
  const dispatch = useDispatch()
  // const list = useSelector(({ components }) => components.pageInside)

  const [state, setState] = useState(undefined)

  useEffect(() => {
    _loadList()
    return () => {
      dispatch({ type: action.page_inside_list })
    }
  }, [data])

  const _loadList = async () => {
    const data = await dispatch(Actions.get_pageinside_list(screenId))
    setState(data)
  }

  const _navigate = (screenId, title) => {
    if (global.platform === 'web') return
    navigation.push('Category', { screenId, title })
  }

  const renderItem = ({ item }) => {
    const source = item.thumbnail ? { uri: item.thumbnail } : nativePlaceholder
    return (
      <View style={{
        width: screenWidth / 2,
        alignItems: 'center',
        paddingTop: 15
      }}
      >
        <TouchableOpacity
          activeOpacity={0.8}
          style={{ paddingHorizontal: 10, width: '100%', alignItems: 'center' }}
          onPress={() => _navigate(item.screen_id, item.name)}
        >
          <Image
            source={source}
            style={{
              width: screenWidth / 2 - 10,
              height: screenWidth / 2 - 10
            }}
            resizeMode='cover'
          />
          <Text
            numberOfLines={2}
            ellipsizeMode='tail'
            style={{
              color: '#000',
              fontSize: 14,
              paddingTop: 5
            }}
          >
            {item.name}
          </Text>
        </TouchableOpacity>
      </View>
    )
  }

  return (
    <FlatList
      data={state}
      numColumns={2}
      keyExtractor={(item, index) => item.id}
      contentContainerStyle={{ paddingVertical: 15 }}
      renderItem={renderItem}
    />
  )
}

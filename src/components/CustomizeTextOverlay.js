import React, { useState, useRef, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { colors, global } from '../utils'
import Slider from '@material-ui/core/Slider'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Modal from 'modal-enhanced-react-native-web'
import { SketchPicker } from 'react-color'
import placeholder from '../assets/image_placeholder@3x.png'
import { Rnd } from 'react-rnd'
import { FormControl, InputLabel, FormControlLabel, Switch, NativeSelect, Fab } from '@material-ui/core'
import InputBase from '@material-ui/core/InputBase'
import ImageScal from '../utils/ImageScal'

export default function ImageTextOverlayModal ({ isVisible, data, _done, ...others }) {
  let textOverlay = useRef(null)
  const classes = useStyles()
  const fontFamily = useSelector(({ components }) => components.fontFamily)
  const source = data.component_url && data.component_url !== '' ? data.component_url : placeholder

  const [state, setState] = useState({
    font_color: colors.black,
    font_border: false,
    border_color: '#000',
    font_size: 20,
    font_name: 'Arimo',
    rotation: 0,
    x: 0,
    y: 0,
    height: 0,
    width: global.width,
    displayColorPicker: false
  })

  useEffect(() => {
    _changeState()
  }, [data])

  const _changeState = async () => {
    const { height } = await ImageScal(source)
    if (data) {
      let { x, y, font_color, font_size, font_border, rotation, font_name, border_color } = data
      x = (x / 100 * state.width)
      y = (y / 100 * state.height || height)
      setState((state) => ({
        ...state,
        font_color: font_color || colors.black,
        font_size: font_size || 16,
        font_border: font_border || false,
        border_color: border_color || '#000',
        rotation: rotation || 1,
        x: 0,
        y: 0,
        font_name: font_name || 'Arimo'
      }))
    }
  }

  const _changeFont = (font_name) => {
    font_name = font_name.target.value
    textOverlay.style.setProperty('font-family', font_name, 'important')
    setState({ ...state, font_name })
  }

  const _onDragEnd = (e, value) => {
    const { x, y } = value
    setState({ ...state, x, y })
  }

  const _onSubmit = () => {
    let { x, y, font_color, font_size, font_border, rotation, font_name, width, height, border_color } = state
    x = (x / width * 100)
    y = (y / height * 100)
    _done({ x, y, font_color, font_size, font_border, rotation, font_name, border_color })
  }

  const _onImageload = ({ target: img }) => {
    const { height, width } = img
    setState({ ...state, height })
    // setTimeout(() => {
    //   let { x, y } = data
    //   x = (x / 100 * width)
    //   y = (y / 100 * height)
    //   setState({ ...state, x, y })
    // }, 500)
  }

  if (!isVisible) return null

  return (
    <Modal
      isVisible={isVisible}
      style={{ alignItems: 'center' }}
      {...others}
    >
      <div style={{
        backgroundColor: colors.black,
        height: '100%',
        width: '75%',
        flex: 1
      }}
      >
        <div style={{ display: 'flex', flex: 1, height: '100%' }}>
          <div style={{
            width: '30%',
            justifyContent: 'center',
            display: 'flex',
            height: 'fit-content'
          }}
          >
            <div style={{ padding: 25 }}>
              <FormControl fullWidth>
                <InputLabel htmlFor='demo-customized-select-native' style={{ color: '#c9c9c9' }}>Font Family</InputLabel>
                <NativeSelect
                  id='demo-customized-select-native'
                  value={state.font_name}
                  onChange={_changeFont}
                  input={<BootstrapInput />}
                >
                  {
                    fontFamily.map((item, index) => (<option className={classes.option} value={item} key={index}>{item}</option>))
                  }
                </NativeSelect>
              </FormControl>
              <div style={{ margin: 10 }} />
              <FormControl fullWidth>
                <InputLabel style={{ color: '#c9c9c9' }}>Color</InputLabel>
                <BootstrapInput
                  value={state.font_color}
                  onClick={() => setState({ ...state, displayColorPicker: state.displayColorPicker ? undefined : 'font' })}
                />
              </FormControl>
              {
                (state.displayColorPicker === 'font' || state.displayColorPicker === 'border') && (
                  <div style={{
                    position: 'absolute',
                    zIndex: '2'
                  }}
                  >
                    <div
                      style={{
                        position: 'fixed',
                        top: '0px',
                        right: '0px',
                        bottom: '0px',
                        left: '0px'
                      }}
                      onClick={() => setState({ ...state, displayColorPicker: false })}
                    />
                    <SketchPicker
                      color={state.displayColorPicker === 'font' ? state.font_color : state.border_color}
                      onChange={(e) => {
                        if (state.displayColorPicker === 'font') { setState({ ...state, font_color: e.hex }) } else { setState({ ...state, border_color: e.hex }) }
                      }}
                    />
                  </div>
                )
              }
              <div style={{ marginTop: 15, marginBottom: 15 }}>
                <InputLabel style={{ color: '#c9c9c9', fontSize: 13 }}>Font size</InputLabel>
                <PrettoSlider
                  valueLabelDisplay='auto'
                  value={state.font_size}
                  min={10}
                  max={50}
                  onChange={(e, value) => setState({ ...state, font_size: value })}
                />
              </div>
              <div style={{ marginTop: 15, marginBottom: 15 }}>
                <InputLabel style={{ color: '#c9c9c9', fontSize: 13 }}>Rotation</InputLabel>
                <PrettoSlider
                  valueLabelDisplay='auto'
                  value={state.rotation}
                  min={0}
                  max={360}
                  onChange={(e, value) => setState({ ...state, rotation: value })}
                />
              </div>
              <FormControl fullWidth>
                <FormControlLabel
                  value={state.font_border}
                  control={<Switch />}
                  label='Text Border'
                  labelPlacement='start'
                  onChange={() => setState({ ...state, font_border: !state.font_border })}
                  style={{ color: '#fff', justifyContent: 'space-between' }}
                />
              </FormControl>
              <FormControl fullWidth>
                <InputLabel style={{ color: '#c9c9c9' }}>Border color</InputLabel>
                <BootstrapInput
                  value={state.border_color}
                  onClick={() => setState({ ...state, displayColorPicker: state.displayColorPicker ? undefined : 'border' })}
                />
              </FormControl>
              <div style={{ margin: 30 }} />
              <Fab
                variant='extended'
                size='small'
                classes={{ label: classes.buttonLable, root: classes.buttonSize }}
                onClick={_onSubmit}
              >
                Done
              </Fab>
            </div>
          </div>

          <div style={{ flex: 1, justifyContent: 'center', display: 'flex', alignItems: 'center' }}>

            <div style={{
              // height: '100%',
              // position: 'relative'
              // justifyContent: 'center',
              // display: 'flex',
              // alignItems: 'center',
              width: 320
            }}
            >
              <img
                src={source}
                style={{ width: global.width, position: 'absolute' }}
                onLoad={_onImageload}
              />
              <div
                style={{ position: 'relative', backgroundColor: 'rgba(0,0,0,0.5)', height: state.height || 200, width: 320 }}
              >
                <Rnd
                  position={{ x: state.x, y: state.y }}
                  enableResizing={false}
                  bounds='parent'
                  scale={1}
                  onDragStop={_onDragEnd}
                >
                  <div style={{ display: 'inline-block', cursor: 'pointer', transform: `rotate(${state.rotation}deg)` }}>
                    <span
                      ref={(ref) => textOverlay = ref}
                      style={{
                        color: state.font_color,
                        fontSize: state.font_size,
                        textAlign: 'center',
                        textShadow: state.font_border && `-1px -1px 0 ${state.border_color}, 1px -1px 0 ${state.border_color}, -1px 1px 0 ${state.border_color}, 1px 1px 0 ${state.border_color}`
                      }}
                    >
                      {data.component_title}
                    </span>
                  </div>
                </Rnd>
              </div>
            </div>
          </div>
        </div>

      </div>
    </Modal>
  )
}

const PrettoSlider = withStyles({
  root: {
    color: colors.white,
    height: 4
  },
  thumb: {
    height: 12,
    width: 12,
    backgroundColor: colors.white,
    border: '1px solid currentColor',
    marginTop: -4,
    '&:focus,&:hover,&$active': {
      boxShadow: 'inherit',
      color: colors.black
    }

  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + -6px)'
  },
  track: {
    height: 4,
    borderRadius: 2
  },
  rail: {
    height: 4,
    borderRadius: 2
  }
})(Slider)

const BootstrapInput = withStyles(theme => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3)
    }
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    border: '1px solid #ced4da',
    fontSize: 16,
    color: '#fff',
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)'
    }
  }
}))(InputBase)

const useStyles = makeStyles(() => ({
  buttonLable: {
    textTransform: 'capitalize',
    color: colors.black
  },
  MuiListItemText: {
    fontFamily: 'inherit !important'
  },
  buttonSize: {
    width: '100% !important'
  },
  option: {
    backgroundColor: 'rgb(63, 63, 63) !important'
  }
}))

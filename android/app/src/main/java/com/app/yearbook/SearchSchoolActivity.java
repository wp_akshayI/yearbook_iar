package com.app.yearbook;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.SearchSchoolAdapter;
import com.app.yearbook.model.SchoolList;
import com.app.yearbook.model.SearchSchool;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchSchoolActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private ArrayList<SchoolList> getSchoolList;
    private RecyclerView rvSearchSchool;
    private SearchSchoolAdapter schoolAdapter;
    private androidx.appcompat.widget.SearchView searchView;
    private Toolbar toolbar;
    private LinearLayout lvNoData;
    private AllMethods utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_school);

        //all control
        setView();
    }

    public void setView()
    {
        //toolbar
        setToolbar();

        //rv for school
        rvSearchSchool=findViewById(R.id.rvSearchSchool);

        //lv for no data
        lvNoData=findViewById(R.id.lvNoData);

        utils=new AllMethods();

        //get school
        getAllSchool();
    }

    public void setToolbar()
    {
        toolbar=findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Search school");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        else if(item.getItemId() == R.id.action_search)
        {
            return true;
        }
        else if(item.getItemId() == R.id.addschool)
        {
            addSchool();
        }

        return super.onOptionsItemSelected(item);
    }

    public void addSchool() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SearchSchoolActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_alertbox_report, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alert = dialogBuilder.create();
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        final Button btnSend = dialogView.findViewById(R.id.btnSend);
        final EditText etMessage = dialogView.findViewById(R.id.etMessage);
        etMessage.setLines(1);
        etMessage.setHint("Add school name");

        TextView tvTitle = dialogView.findViewById(R.id.tvTitle);
        tvTitle.setText("Add school");
        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    btnSend.setEnabled(true);
                    btnSend.setBackgroundColor(getResources().getColor(R.color.btnGray));
                    btnSend.setTextColor(getResources().getColor(R.color.white));
                } else {
                    btnSend.setEnabled(false);
                    btnSend.setBackgroundColor(getResources().getColor(R.color.edtBG));
                    btnSend.setTextColor(getResources().getColor(R.color.deselectTab));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AllMethods.hideKeyboard(SearchSchoolActivity.this,v);
                Intent i=new Intent();
                i.putExtra("schoolName",etMessage.getText().toString());
                setResult(RESULT_OK, i);
                finish();


//                progressDialog = ProgressDialog.show(SearchSchoolActivity.this, "", "", true);
//                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                progressDialog.setContentView(R.layout.progress_view);
//                ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
//                Circle bounce = new Circle();
//                bounce.setColor(Color.BLACK);
//                progressBar.setIndeterminateDrawable(bounce);
//
//                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
//                final Call<GiveReport> report = retrofitClass.addSchool(etMessage.getText().toString());
//                report.enqueue(new Callback<GiveReport>() {
//                    @Override
//                    public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
//                        alert.dismiss();
//                        if (progressDialog.isShowing()) {
//                            progressDialog.dismiss();
//                        }
//
//                        if(response.body().getResponseCode().equalsIgnoreCase("1"))
//                        {
//                            getAllSchool();
//                        }
//                        else
//                        {
//                            Toast.makeText(SearchSchoolActivity.this, response.body().getResponseMsg()+ "", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<GiveReport> call, Throwable t) {
//                        alert.dismiss();
//                        if (progressDialog.isShowing()) {
//                            progressDialog.dismiss();
//                        }
//                        Toast.makeText(SearchSchoolActivity.this, t.getMessage() + "", Toast.LENGTH_SHORT).show();
//                    }
//                });
            }
        });
        alert.show();

    }

    public void getAllSchool()
    {
        //Progress bar
        progressDialog=ProgressDialog.show(SearchSchoolActivity.this,"","",true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar=progressDialog.findViewById(R.id.progress);
        Circle bounce=new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<SearchSchool> schoolList= retrofitClass.getAllSchool();
        schoolList.enqueue(new Callback<SearchSchool>() {
            @Override
            public void onResponse(Call<SearchSchool> call, Response<SearchSchool> response) {
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }

                Log.d("TTT","response code: "+response.body().getResponseCode());
                if(response.body().getResponseCode().equals("1")) {
                    if (response.body().getSchoolList().size() != 0 && response.body().getSchoolList() != null) {
                        lvNoData.setVisibility(View.GONE);
                        getSchoolList = new ArrayList<>();
                        getSchoolList.addAll(response.body().getSchoolList());
                        Log.d("TTT", "Data: " + getSchoolList.size());
                        schoolAdapter = new SearchSchoolAdapter(getSchoolList, SearchSchoolActivity.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        rvSearchSchool.setLayoutManager(mLayoutManager);
                        rvSearchSchool.setAdapter(schoolAdapter);
                        schoolAdapter.notifyDataSetChanged();
                    } else {
                        lvNoData.setVisibility(View.VISIBLE);
                        Log.d("TTT", "School not found.");
                    }
                }
                else
                {
                    lvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<SearchSchool> call, Throwable t) {
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
                utils.setAlert(SearchSchoolActivity.this,"",t.getMessage());

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchview, menu);

        MenuItem item=menu.findItem(R.id.addschool);
        item.setVisible(true);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (androidx.appcompat.widget.SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));

        // listening to search query text change
        searchView.setOnQueryTextListener(new androidx.appcompat.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                if(getSchoolList!=null && getSchoolList.size()!=0)
                schoolAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                if(getSchoolList!=null && getSchoolList.size()!=0)
                schoolAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

}

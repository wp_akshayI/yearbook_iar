package com.app.yearbook.model;

import com.google.gson.annotations.SerializedName;

public class CheckUserStatus{

	@SerializedName("ResponseCode")
	private String responseCode;

	@SerializedName("flag")
	private String flag;

	@SerializedName("is_access_code_verify")
	private String isAccessCodeVerify;

	@SerializedName("ResponseMsg")
	private String responseMsg;

	@SerializedName("Result")
	private String result;

	public void setResponseCode(String responseCode){
		this.responseCode = responseCode;
	}

	public String getResponseCode(){
		return responseCode;
	}

	public void setFlag(String flag){
		this.flag = flag;
	}

	public String getFlag(){
		return flag;
	}

	public void setIsAccessCodeVerify(String isAccessCodeVerify){
		this.isAccessCodeVerify = isAccessCodeVerify;
	}

	public String getIsAccessCodeVerify(){
		return isAccessCodeVerify;
	}

	public void setResponseMsg(String responseMsg){
		this.responseMsg = responseMsg;
	}

	public String getResponseMsg(){
		return responseMsg;
	}

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}
}
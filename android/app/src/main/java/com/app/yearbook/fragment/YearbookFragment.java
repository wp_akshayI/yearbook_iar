package com.app.yearbook.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.yearbook.AddSignatureActivity;
import com.app.yearbook.UserDetailActivity;
import com.app.yearbook.adapter.YearbookListAdapter;
import com.app.yearbook.databinding.FragmentYearbookBinding;
import com.app.yearbook.model.YBResponse;
import com.app.yearbook.model.studenthome.GiveReport;
import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.adapter.StudentYearListAdapter;
import com.app.yearbook.model.yearbook.BookListModelForYearbook;
import com.app.yearbook.model.yearbook.YearbookList;
import com.app.yearbook.model.yearbook.getYearBook;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class YearbookFragment extends Fragment {

    public FragmentYearbookBinding binding;

    public ProgressDialog progressDialog;
    private LoginUser loginUser;
    private String userToken;
    private AllMethods allMethods;
    private ArrayList<YBResponse.YearbookList> yearbookLists = new ArrayList<>();

    public YearbookFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_yearbook, container, false);
        setView();
        return binding.getRoot();
    }

    public void setView() {
        allMethods = new AllMethods();

        loginUser = new LoginUser(getActivity());
        if (loginUser.getUserData() != null) {
            userToken = loginUser.getUserToken();
        }

        YearbookListAdapter adapter = new YearbookListAdapter(yearbookLists, loginUser.getUserData().getUserType().equalsIgnoreCase("student")?true:false, getActivity());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        binding.rvYearbook.setLayoutManager(mLayoutManager);
        binding.rvYearbook.setAdapter(adapter);

//        getYearBook();
    }

    @Override
    public void onResume() {
        super.onResume();
        getYearBook();
    }

    public void getYearBook() {
        Log.d("TTT", "user id, school id: " + userToken);
        //Progress bar
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<YBResponse> userPost = retrofitClass.checkYearbookPublish(userToken, "app");
        userPost.enqueue(new Callback<YBResponse>() {
            @Override
            public void onResponse(Call<YBResponse> call, Response<YBResponse> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body() != null) {
                    YBResponse ybResponse = response.body();
                    binding.lblMsg.setText(!TextUtils.isEmpty(ybResponse.getResponseMsg())?ybResponse.getResponseMsg():"");
                    if (ybResponse.getResponseCode().equals("1")) {
                        if (ybResponse.getData().getYearbookList().size() > 0) {
                            binding.rvYearbook.setVisibility(View.VISIBLE);
                            binding.lvNoData.setVisibility(View.GONE);

                            yearbookLists.clear();
                            yearbookLists.addAll(ybResponse.getData().getYearbookList());
                            binding.rvYearbook.getAdapter().notifyDataSetChanged();
                        } else {
                            binding.rvYearbook.setVisibility(View.GONE);
                            binding.lvNoData.setVisibility(View.VISIBLE);
                        }
                    } else if (ybResponse.getResponseCode().equals("10")) {
                        Toast.makeText(getActivity(), ybResponse.getResponseMsg(), Toast.LENGTH_SHORT).show();
                        LoginUser loginSP = new LoginUser(getActivity());
                        loginSP.clearData();

                        Intent i = new Intent(getActivity(), LoginActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        binding.rvYearbook.setVisibility(View.GONE);
                        binding.lvNoData.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<YBResponse> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });

        /*RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<getYearBook> getYearBook = retrofitClass.getYearbook(Integer.parseInt(userId), Integer.parseInt(schoolId), "regular");
        getYearBook.enqueue(new Callback<com.app.yearbook.model.yearbook.getYearBook>() {
            @Override
            public void onResponse(Call<getYearBook> call, Response<getYearBook> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Log.d("TTT", "Response code: " + response.body().getResponseCode());
                if (response.body().getResponseCode().equals("1")) {
                    if (response.body().getYearbookList().size() > 0) {
                        yearbookLists = new ArrayList<>();
                        binding.rvYearbook.setVisibility(View.VISIBLE);
                        binding.lvNoData.setVisibility(View.GONE);

                        ArrayList<YearbookList> al_bookitemlist = new ArrayList<>();
                        bookList = new ArrayList<>();
                        yearbookLists.addAll(response.body().getYearbookList());

                        Log.d("TTT", "Sizeee/ " + yearbookLists.size());

                        for (int i = 0; i < yearbookLists.size(); i++) {
                            al_bookitemlist.add(yearbookLists.get(i));
                            if (((i + 1) % 3) == 0 || i == yearbookLists.size() - 1) {
                                BookListModelForYearbook bookListModel = new BookListModelForYearbook(i, al_bookitemlist);
                                bookList.add(bookListModel);
                                al_bookitemlist = new ArrayList<>();
                            }
                        }

                        if (bookList.size() < 4) {
                            for (int j = 0; j < 4 - bookList.size() + 1; j++) {
                                BookListModelForYearbook bookListModel = new BookListModelForYearbook(bookList.size() + 1 + j, al_bookitemlist);
                                bookList.add(bookListModel);
                                al_bookitemlist = new ArrayList<>();
                            }
                        }
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        binding.rvYearbook.setLayoutManager(mLayoutManager);

                        StudentYearListAdapter mAdapter = new StudentYearListAdapter(bookList, getActivity());
                        binding.rvYearbook.setAdapter(mAdapter);
                    } else {
                        binding.rvYearbook.setVisibility(View.GONE);
                        binding.lvNoData.setVisibility(View.VISIBLE);
                    }
                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(getActivity());
                    loginSP.clearData();

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                } else {
                    binding.rvYearbook.setVisibility(View.GONE);
                    binding.lvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<getYearBook> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });*/
    }

    /*@Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.dummy_menu, menu);
    }*/
}

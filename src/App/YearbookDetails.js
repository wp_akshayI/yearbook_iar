import React, { useEffect, useState } from 'react'
import {
  ActivityIndicator, Dimensions,
  NativeModules, SafeAreaView, StyleSheet,
  Text, View
} from 'react-native'
import ParallaxScrollView from 'react-native-parallax-scroll-view'
import { Header, HeaderBackButton } from 'react-navigation-stack'
import { useDispatch } from 'react-redux'
import Actions from '../actions'
import YBImageSection from '../Sections/YBImageSection'
import { colors } from '../utils'
import SectionList from './components/SectionList'
const { height } = Dimensions.get('window')

YearbookDetails.navigationOptions = { header: null }
export default function YearbookDetails ({ navigation }) {
  const dispatch = useDispatch()

  const [state, setState] = useState({
    data: navigation.getParam('sections'),
    screenId: navigation.getParam('screenId'),
    yearbookSC: null
  })

  useEffect(() => {
    _getPage()
  }, [])

  const _getPage = async () => {
    if (data) return
    const data = await dispatch(Actions.get_screen_detail(state.screenId))
    if (data) {
      let yearbookSC = data.filter(item => item.component_type === 9)
      if (yearbookSC[0]) yearbookSC = yearbookSC[0]
      setState({ ...state, data, yearbookSC })
    } else {
      setState({ ...state, data: [] })
    }
  }

  const renderYearbookSection = (item, index) => {
    return <SectionList data={item} screenId={state.screenId} key={String(index)} />
  }

  const renderNavBar = () => {
    const headerTitle = navigation.getParam('title') || 'Yearbook'
    // const sectionsExist = !!navigation.getParam('sections')
    return (
      <SafeAreaView style={styles.navContainer}>
        <View style={styles.navBar}>
          <HeaderBackButton
            tintColor='#000'
            onPress={() => {
              NativeModules.AppModules.backToYearbookList()
              // if (sectionsExist) {
              //   openDrawer()
              // } else {
              //   navigation.goBack()
              // }
            }}
          />
          <Text
            style={styles.headerTitle}
            ellipsizeMode='tail'
            numberOfLines={1}
          >
            {headerTitle}
          </Text>
          <View style={{ height: 50, width: 50 }} />
        </View>
      </SafeAreaView>
    )
  }

  const renderForeground = () => {
    const { yearbookSC } = state
    if (!yearbookSC) return null
    return (
      <YBImageSection data={yearbookSC} />
    )
  }

  if (!state.data || state.data.length <= 0) {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ height: Header.HEIGHT + 10 }}>
          {
            renderNavBar()
          }
        </View>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          {
            state.data ? (
              <Text style={{ marginTop: 10, textAlign: 'center', color: 'grey' }}>
                                No content has been added yet.
              </Text>
            )
              : <ActivityIndicator size='large' />
          }
        </View>
      </View>
    )
  }

  return (
    <View style={{ flex: 1, backgroundColor: colors.app_background }}>
      <ParallaxScrollView
        backgroundColor={colors.app_background}
        parallaxHeaderHeight={height}
        renderBackground={renderForeground}
        renderStickyHeader={renderNavBar}
        stickyHeaderHeight={Header.HEIGHT + 10}
        outputScaleValue={0.7}
        showsVerticalScrollIndicator={false}
        nestedScrollEnabled
      >
        <SafeAreaView>
          {
            state.data && state.data.map(renderYearbookSection)
          }
        </SafeAreaView>

      </ParallaxScrollView>
    </View>
  )
};

const styles = StyleSheet.create({
  navContainer: {
    height: '100%',
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingVertical: 5
    // borderBottomWidth: 0.4,
    // borderBottomColor: 'rgba(102, 32, 255, 0.20)'
  },
  navBar: {
    height: Header.HEIGHT,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'transparent'
  },
  headerTitle: {
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'calibri',
    flex: 1,
    color: colors.black
  }
})

package com.app.yearbook;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.databinding.DataBindingUtil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.yearbook.databinding.ActivityNewImageVideoBinding;
import com.app.yearbook.model.allpost.ImageVideoPost;
import com.app.yearbook.model.allpost.TextPost;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.BitmapUtils;
import com.app.yearbook.utils.Constants;
import com.app.yearbook.videoTrimmer.interfaces.OnProgressVideoListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.fenchtose.nocropper.BitmapResult;
import com.fenchtose.nocropper.CropInfo;
import com.fenchtose.nocropper.CropResult;
import com.fenchtose.nocropper.CropState;
import com.fenchtose.nocropper.CropperView;
import com.fenchtose.nocropper.ScaledCropper;

import java.io.File;
import java.io.IOException;
import java.util.List;

import gun0912.tedbottompicker.TedBottomPicker;
import tcking.github.com.giraffeplayer2.PlayerManager;
import tcking.github.com.giraffeplayer2.VideoView;

public class NewImageVideoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "NewImageVideoActivity";
    private static final int REQUEST_PERMISSION = 101;
    public static final int FILTER_IMAGE = 111;
    public static final int TRIME_REQUEST = 333;
    private TextView tvPhoto, tvVideo;
    private CropperView imgSelect;
    private VideoView videoSelected;
    private android.widget.VideoView video_loader;
    private TedBottomPicker tedBottomPicker;
    private MenuItem next;
    private String postTag;
    private ImageView imgSnap, imgDummy, icon_video_play;
    private boolean isSnappedToCenter = false;
    private FrameLayout frameLayoutMain;
    private Uri videoUri;
    private RequestManager requestManager;
    private SeekBar mHolderTopView;

    private RelativeLayout layout_surface_view;
    private int mDuration = 0;
    private int mTimeVideo = 0;
    private int mStartPosition = 0;
    private int mEndPosition = 0;
    private boolean mResetSeekBar = true;
    private List<OnProgressVideoListener> mListeners;
    ActivityNewImageVideoBinding binding;
    private int mMaxDuration = 60000;
    String feedType;
    ImageVideoPost imageVideoPost;
    PostListItem postListItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_image_video);
        imageVideoPost = new ImageVideoPost();
        feedType = getIntent().getStringExtra(Constants.POST_TYPE);
        setToolbar();
        if (getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false)) {
            postListItem = getIntent().getParcelableExtra(Constants.INTENT_DATA);
            binding.appbar.toolbarTitle.setText(getString(R.string.edit_post));
        }
        binding.btnPicker.setOnClickListener(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Log.d("TTT", "Permission...");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
            } else {
                setView();
            }
        } else {
            Log.d("TTT", "granted Permission...");
            setView();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void setToolbar() {
        setSupportActionBar(binding.appbar.toolbar);
        binding.appbar.toolbarTitle.setText(getString(R.string.new_post));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_next, menu);
        next = menu.findItem(R.id.action_next);
        next.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_next:
                if (postTag.equalsIgnoreCase("Image")) {

                    //    ScaledCropper result = prepareCropForOriginalImage();
//                    result.crop(new CropperCallback() {
//                        @Override
//                        public void onCropped(Bitmap bitmap) {
//                            Log.d("TTT", "onCropped: bitmap: "+bitmap);
//                            if (bitmap != null) {
//                                try {
//                                    BitmapUtils.writeBitmapToFile(bitmap, new File(Environment.getExternalStorageDirectory() + "/crop_test_info_orig.jpg"), 90);
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }
//                    });

//                    Bitmap bitmap = result.cropBitmap();
//                    if(result!=null)
//                    {
//                        try {
//                            File f = new File(Environment.getExternalStorageDirectory()
//                                    + File.separator + "crop_test_1.jpg");
//                            BitmapUtils.writeBitmapToFile(bitmap, f, 100);
//
//                            Log.d("TTT", "onOptionsItemSelected: "+Uri.fromFile(f));
//                            Intent i = new Intent(getActivity(), ImageFilterActivity.class);
//                            i.putExtra("from", "library");
//                            i.putExtra("selectUri",  Uri.fromFile(f).toString());
//                            getActivity().startActivityForResult(i, 111);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }


                    BitmapResult result = binding.imgSelected.getCroppedBitmap();
                    if (result.getState() == CropState.FAILURE_GESTURE_IN_PROCESS) {
                        Toast.makeText(this, "unable to crop. Gesture in progress", Toast.LENGTH_SHORT).show();
                    } else {
                        Bitmap bitmap = result.getBitmap();
                        Log.d("TTT", "loadNewImage:bitmap height width: " + bitmap.getWidth() + " / " + bitmap.getHeight());
//                        if (bitmap != null) {
//                            Log.d("Cropper", "crop1 bitmap: " + bitmap.getWidth() + ", " + bitmap.getHeight());
//                            try {
//                                BitmapUtils.writeBitmapToFile(bitmap, new File(Environment.getExternalStorageDirectory() + "/crop_test.jpg"), 90);
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        }

                        try {
                            File f = new File(Environment.getExternalStorageDirectory()
                                    + File.separator + "crop_test.jpg");
                            BitmapUtils.writeBitmapToFile(bitmap, f, 100);

                            Log.d("TTT", "onOptionsItemSelected: " + Uri.fromFile(f));
                            Intent i = new Intent(this, ImageFilterActivity.class);
                            i.putExtra("from", "library");
                            i.putExtra("selectUri", Uri.fromFile(f).toString());
                            startActivityForResult(i, FILTER_IMAGE);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        //Uri getUri = AllMethods.getImageUri(getActivity(), bitmap);
                    }


                } else {
                    // Intent i = new Intent(getActivity(), AddPostActivity.class);
                    Intent i = new Intent(this, TrimmerActivity.class);
                    i.putExtra("from", "library");
                    i.putExtra("Type", "Video");
                    i.putExtra("selectUri", videoUri.toString());
                    startActivityForResult(i, TRIME_REQUEST);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v == binding.btnPicker) {
            if (feedType.equalsIgnoreCase(Constants.POST_IMAGE)) {
                openImagePicker();
            } else if (feedType.equalsIgnoreCase(Constants.POST_VIDEO)) {
                openVideoPicker();
            }
        } else if (v.getId() == R.id.imgSnap) {
            if (isSnappedToCenter) {
                binding.imgSelected.cropToCenter();
            } else {
                binding.imgSelected.fitToCenter();
            }
            isSnappedToCenter = !isSnappedToCenter;

            // instacropper.setRatios(DEFAULT_RATIO, DEFAULT_MINIMUM_RATIO, DEFAULT_MAXIMUM_RATIO);

        }
    }

    private void openVideoPicker() {
        if (binding.videoLoader.isPlaying()) {
            binding.iconVideoPlay.setVisibility(View.VISIBLE);
            binding.videoLoader.pause();
        }

//        binding.tvPhoto.setTextColor(getResources().getColor(R.color.deselectTab));
//        binding.tvVideo.setTextColor(getResources().getColor(R.color.black));

        tedBottomPicker = new TedBottomPicker.Builder(this)
                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        // here is selected uri
                        //view view
                        videoUri = uri;
                        postTag = "Video";
                        next.setVisible(true);
                        Log.d("TTT", "Uri: " + uri);
                        binding.imgDummy.setVisibility(View.GONE);
                        binding.frameLayoutMain.setVisibility(View.GONE);
                        //videoSelected.setVisibility(View.VISIBLE);
                        binding.layoutSurfaceView.setVisibility(View.VISIBLE);


//                            videoSelected.reset();
//                            videoSelected.setSource(uri);
//                            Log.d("TTT", "Size: " + videoSelected.getDuration());

//                            requestManager
//                                    .load(uri)
//                                    .into(videoSelected.getPlayer().start());

//                            videoSelected.getVideoInfo().setPortraitWhenFullScreen(false);
//                            videoSelected.setVideoPath(uri.toString());
//                            videoSelected.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player
//                            videoSelected.getPlayer().start();

//                            vd.setVideoURI(uri);
//                            vd.start();

//                            GiraffePlayer player=videoSelected.getPlayer();
//                            if (player.isPlaying()) {
//                                player.pause();
//                            } else {
//                                player.start();
//                            }

//                            if(videoSelected.getPlayer().isPlaying())
//                            {
//                                videoSelected.getPlayer().pause();
//                            }

                        setVideoURI(uri);
                        // video_loader.setVideoURI(uri);
                        //video_loader.start();


                    }
                })
                .setSelectedUri(videoUri)
                .setTitle("Select Video")
                .showVideoMedia()
                .create();

        tedBottomPicker.show(getSupportFragmentManager());
    }

    private void openImagePicker() {
        if (PlayerManager.getInstance().getCurrentPlayer() != null) {
            PlayerManager.getInstance().getCurrentPlayer().stop();
        }
//        binding.tvPhoto.setTextColor(getResources().getColor(R.color.black));
//        binding.tvVideo.setTextColor(getResources().getColor(R.color.deselectTab));

        //img view
        tedBottomPicker = new TedBottomPicker.Builder(this)
                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        // here is selected uri
                        Log.d("TTT", "Uri: " + uri);
                        String extension = uri.toString().substring(uri.toString().lastIndexOf("."));
                        if (extension.equalsIgnoreCase(".mp4")) {
                            Toast.makeText(NewImageVideoActivity.this, "Please select other image", Toast.LENGTH_SHORT).show();
                        } else {
                            isSnappedToCenter = false;
                            postTag = "Image";
                            next.setVisible(true);
                            binding.imgDummy.setVisibility(View.GONE);
                            binding.frameLayoutMain.setVisibility(View.VISIBLE);
                            binding.videoSelected.setVisibility(View.GONE);
                            binding.layoutSurfaceView.setVisibility(View.GONE);

                            Bitmap bitmap = AllMethods.GetBitmap(uri, NewImageVideoActivity.this);
                            loadNewImage(uri.getPath(), uri);

                            /*if (extension.equalsIgnoreCase(".gif")){
                                binding.frameLayoutMain.setVisibility(View.GONE);
                                binding.frameLayoutMainGif.setVisibility(View.VISIBLE);
                                setGif(uri);
                            }else {
                                binding.frameLayoutMain.setVisibility(View.VISIBLE);
                                binding.frameLayoutMainGif.setVisibility(View.GONE);
                                Bitmap bitmap = AllMethods.GetBitmap(uri, NewImageVideoActivity.this);
                                loadNewImage(uri.getPath(), uri);
                            }*/
                        }
                    }
                }).setTitle("Select Image").create();

        tedBottomPicker.show(getSupportFragmentManager());
    }

    private void setGif(Uri uri) {
        Glide.with(this)
                .load(uri)
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(gun0912.tedbottompicker.R.drawable.ic_gallery)
                .error(gun0912.tedbottompicker.R.drawable.img_error)
                .into(binding.imgGif);
    }

    private void onVideoCompleted() {
        binding.videoLoader.seekTo(mStartPosition);
    }

    private void onVideoPrepared(@NonNull MediaPlayer mp) {
        // Adjust the size of the video
        // so it fits on the screen
        int videoWidth = mp.getVideoWidth();
        int videoHeight = mp.getVideoHeight();
        float videoProportion = (float) videoWidth / (float) videoHeight;
        int screenWidth = binding.layoutSurfaceView.getWidth();
        int screenHeight = binding.layoutSurfaceView.getHeight();
        float screenProportion = (float) screenWidth / (float) screenHeight;
        ViewGroup.LayoutParams lp = binding.videoLoader.getLayoutParams();

        if (videoProportion > screenProportion) {
            lp.width = screenWidth;
            lp.height = (int) ((float) screenWidth / videoProportion);
        } else {
            lp.width = (int) (videoProportion * (float) screenHeight);
            lp.height = screenHeight;
        }
        binding.videoLoader.setLayoutParams(lp);

        binding.iconVideoPlay.setVisibility(View.VISIBLE);
        mDuration = binding.videoLoader.getDuration();
        binding.videoLoader.seekTo(mStartPosition);
        // setSeekBarPosition();

//        setTimeFrames();
//        setTimeVideo(0);
//
//        if (mOnHgLVideoListener != null) {
//            mOnHgLVideoListener.onVideoPrepared();
//        }
    }

    private void setProgressBarPosition(int position) {
        if (mDuration > 0) {
            long pos = 1000L * position / mDuration;
            binding.handlerTop.setProgress((int) pos);
        }
    }

    private Bitmap mBitmap;

    private void loadNewImage(String filePath, Uri selectedImage) {
        mBitmap = BitmapFactory.decodeFile(filePath);
        originalBitmap = mBitmap;
        Log.d("TTT", "loadNewImage: " + mBitmap);
        int maxP = Math.max(mBitmap.getWidth(), mBitmap.getHeight());
        float scale1280 = (float) maxP / 1280;

        Log.i("TTT", "scaled: " + scale1280 + " - " + (1 / scale1280));
        if (binding.imgSelected.getWidth() != 0) {
            binding.imgSelected.setMaxZoom(binding.imgSelected.getWidth() * 2 / 1280f);
        } else {

            ViewTreeObserver vto = binding.imgSelected.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    binding.imgSelected.getViewTreeObserver().removeOnPreDrawListener(this);
                    binding.imgSelected.setMaxZoom(binding.imgSelected.getWidth() * 2 / 1280f);
                    return true;
                }
            });
        }

        mBitmap = Bitmap.createScaledBitmap(mBitmap, (int) (mBitmap.getWidth() / scale1280),
                (int) (mBitmap.getHeight() / scale1280), true);

        //-------------------------------------

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(selectedImage.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        Log.d("TTT", "Orientation: " + orientation);

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 90);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 180);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 270);
        }

        //-------------------------------------

        Log.d("TTT", "loadNewImage: bitmap w/h: " + mBitmap.getHeight() / mBitmap.getWidth());
        binding.imgSelected.setImageBitmap(mBitmap);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        Log.d("TTT", "loadNewImage: density: " + metrics.densityDpi);
        int minZoom = Math.min(mBitmap.getWidth(), mBitmap.getHeight());
        int maxZoom = Math.max(mBitmap.getWidth(), mBitmap.getHeight());

        float ratio = (float) mBitmap.getHeight() / (float) mBitmap.getWidth();
        Log.d("TTT", "loadNewImage: diff: " + (mBitmap.getHeight() - mBitmap.getWidth()) + " / " + ratio);
        if (mBitmap.getHeight() > mBitmap.getWidth()) {
            if (ratio > 1.5) {
                if (metrics.densityDpi > 320) {
                    binding.imgSelected.setMinZoom(1.1f);
                } else {
                    binding.imgSelected.setMinZoom(0.75f);
                }
            } else {
                binding.imgSelected.setMinZoom(0f);
            }
        } else {
            binding.imgSelected.setMinZoom(0f);
        }
        Log.d("TTT", "loadNewImage: width height : " + mBitmap.getWidth() + " / " + mBitmap.getHeight());
        Log.d("TTT", "loadNewImage: " + binding.imgSelected.getMinZoom() + " / " + binding.imgSelected.getMaxZoom() + " / " + maxZoom + " / " + minZoom);

    }

    private int rotationCount = 0;
    private Bitmap originalBitmap;

    private ScaledCropper prepareCropForOriginalImage() {

        CropResult result = binding.imgSelected.getCropInfo();
        if (result.getCropInfo() == null) {
            return null;
        }

        float scale;
        if (rotationCount % 2 == 0) {
            // same width and height
            scale = (float) originalBitmap.getWidth() / mBitmap.getWidth();
        } else {
            // width and height are interchanged
            scale = (float) originalBitmap.getWidth() / mBitmap.getHeight();
        }

        CropInfo cropInfo = result.getCropInfo().rotate90XTimes(mBitmap.getWidth(), mBitmap.getHeight(), rotationCount);
        return new ScaledCropper(cropInfo, originalBitmap, scale);
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d("TTT", "LF pause");
        if (PlayerManager.getInstance().getCurrentPlayer() != null) {
            Log.d("TTT", "LF pause PlayerManager");
            PlayerManager.getInstance().getCurrentPlayer().stop();
        }
    }

    public void setVideoURI(final Uri videoURI) {
        binding.videoLoader.setVideoURI(videoURI);
        binding.videoLoader.pause();
    }

    public void setView() {

        if (feedType.equalsIgnoreCase(Constants.POST_IMAGE)) {
            openImagePicker();
        } else if (feedType.equalsIgnoreCase(Constants.POST_VIDEO)) {
            openVideoPicker();
        }

        requestManager = Glide.with(this);

        //dummy img

//        binding.tvPhoto.setOnClickListener(this);
//        binding.tvPhoto.setTextColor(getResources().getColor(R.color.black));

//        binding.tvVideo.setOnClickListener(this);

        //frameLayoutMain

        //resize
        binding.imgSnap.setOnClickListener(this);

        //insta croper
        // instacropper=view.findViewById(R.id.instacropper);

        //image select
        binding.imgSelected.setMakeSquare(false);
        binding.imgSelected.setGestureEnabled(true);
        binding.imgSelected.setDebug(true);
        binding.imgSelected.setGridCallback(new CropperView.GridCallback() {
            @Override
            public boolean onGestureStarted() {
                return true;
            }

            @Override
            public boolean onGestureCompleted() {
                return false;
            }
        });

        //video view


        //default open img view
//        tedBottomPicker = new TedBottomPicker.Builder(getActivity())
//                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
//                    @Override
//                    public void onImageSelected(Uri uri) {
//                        // here is selected uri
//                        String extension = uri.toString().substring(uri.toString().lastIndexOf("."));
//                        if (extension.equalsIgnoreCase(".mp4")) {
//                            Toast.makeText(getActivity(), "Please, select other image", Toast.LENGTH_SHORT).show();
//                        } else {
//                            postTag = "Image";
//                            next.setVisible(true);
//                            imgDummy.setVisibility(View.GONE);
//                            frameLayoutMain.setVisibility(View.VISIBLE);
//                            videoSelected.setVisibility(View.GONE);
//                            layout_surface_view.setVisibility(View.GONE);
//
//                            Bitmap mBitmap = AllMethods.GetBitmap(uri, getActivity());
//                            loadNewImage(mBitmap, uri);
//                        }
//
//                    }
//                })
//                .setTitle("Select Image")
//                .create();
//        tedBottomPicker.show(getChildFragmentManager());

//        mListeners = new ArrayList<>();
//        mListeners.add(new OnProgressVideoListener() {
//            @Override
//            public void updateProgress(int time, int max, float scale) {
//                updateVideoProgress(time);
//            }
//        });
        //mListeners.add(mVideoProgressIndicator);

        final GestureDetector gestureDetector = new
                GestureDetector(this,
                new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapConfirmed(MotionEvent e) {
                        onClickVideoPlayPause();
                        return true;
                    }
                }
        );

        binding.videoLoader.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, @NonNull MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

//        mHolderTopView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            @Override
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                onPlayerIndicatorSeekChanged(progress, fromUser);
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekBar seekBar) {
//                onPlayerIndicatorSeekStart();
//            }
//
//            @Override
//            public void onStopTrackingTouch(SeekBar seekBar) {
//                onPlayerIndicatorSeekStop(seekBar);
//            }
//        });

        // mHolderTopView.setOnSeekBarChangeListener(change);

        binding.videoLoader.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                onVideoPrepared(mp);
            }
        });

        binding.videoLoader.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                onVideoCompleted();
            }
        });
    }

    private void onClickVideoPlayPause() {
        if (binding.videoLoader.isPlaying()) {
            binding.iconVideoPlay.setVisibility(View.VISIBLE);
            // mMessageHandler.removeMessages(SHOW_PROGRESS);
            binding.videoLoader.pause();
        } else {
            binding.iconVideoPlay.setVisibility(View.GONE);
            // mMessageHandler.sendEmptyMessage(SHOW_PROGRESS);

            if (mResetSeekBar) {
                mResetSeekBar = false;
                binding.videoLoader.seekTo(mStartPosition);
            }

            binding.videoLoader.start();
            //play(0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                Log.d("TTT", "Permission... onRequestPermissionsResult");
                setView();
            } else {
                Log.d("TTT", "denyyyy... onRequestPermissionsResult");
                Toast.makeText(this, "Deny", Toast.LENGTH_SHORT).show();
                finish();
                //code for deny
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILTER_IMAGE) {
            if (resultCode == RESULT_OK) {
                imageVideoPost.setUrl(data.getStringExtra("AddPostImage"));
                Intent intent = new Intent(this, NewImageVideoActivity2.class);
                intent.putExtra(Constants.INTENT_ISEDIT, getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false));
                intent.putExtra(Constants.INTENT_DATA, postListItem);
                intent.putExtra(Constants.POST_TYPE, Constants.POST_IMAGE);
                intent.putExtra(Constants.POST_IMAGE, imageVideoPost);
                startActivityForResult(intent, 222);
            }
        } else if (requestCode == 222) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        } else if (requestCode == TRIME_REQUEST) {
            if (resultCode == RESULT_OK) {
                imageVideoPost.setUrl(data.getStringExtra("selectUri"));
                Intent intent = new Intent(this, NewImageVideoActivity2.class);
                intent.putExtra(Constants.INTENT_ISEDIT, getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false));
                intent.putExtra(Constants.INTENT_DATA, postListItem);
                intent.putExtra(Constants.POST_TYPE, Constants.POST_VIDEO);
                intent.putExtra(Constants.POST_VIDEO, imageVideoPost);
                startActivityForResult(intent, 222);
                Log.d(TAG, "onActivityResult: ");
            }
        }
    }


}

package com.app.yearbook;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.StudentNotificationListAdapter;
import com.app.yearbook.model.NotificationAcceptDeny;
import com.app.yearbook.model.profile.NotificationList;
import com.app.yearbook.model.profile.getNotification;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.OnRecyclerClick;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public ProgressDialog progressDialog;
    private String userId;
    private String userType;
    private AllMethods allMethods;
    public static ArrayList<NotificationList> notificationLists;
    public static RecyclerView rvNotification;
    private LinearLayout lvNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_notification);
        setVieW();
    }

    public void setVieW() {
        //toolbar
        setToolbar();

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        //intent
        if (getIntent().getExtras() != null) {
            userId = getIntent().getStringExtra("userId");
            userType = getIntent().getStringExtra("userType");
        }

        //all methods object
        allMethods = new AllMethods();

        //notification
        rvNotification = findViewById(R.id.rvNotification);

        //lv
        lvNoData = findViewById(R.id.lvNoData);

        getNotification();
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Notifications");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void getNotification() {
        //Progress bar
        progressDialog = ProgressDialog.show(NotificationActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<getNotification> notification = retrofitClass.getNotification(Integer.parseInt(userId), Integer.parseInt(userType));
        notification.enqueue(new Callback<getNotification>() {
            @Override
            public void onResponse(Call<getNotification> call, Response<getNotification> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (response.body().getResponseCode().equals("1")) {
                    if (response.body().getNotificationList().size() > 0) {
                        rvNotification.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        notificationLists = new ArrayList<>();
                        notificationLists.addAll(response.body().getNotificationList());

                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(NotificationActivity.this);
                        rvNotification.setLayoutManager(mLayoutManager);

                        StudentNotificationListAdapter mAdapter = new StudentNotificationListAdapter(notificationLists, NotificationActivity.this, new OnRecyclerClick() {
                            @Override
                            public void onClick(int pos, View v, int type) {
                                if (type == 1) {
                                    //accept
                                    callAPI(NotificationActivity.this, notificationLists.get(pos).getNotificationId(), "1", pos);
                                } else if (type == 2) {
                                    //deny
                                    callAPI(NotificationActivity.this, notificationLists.get(pos).getNotificationId(), "2", pos);
                                }else if (type == 3) {
                                    //deny
                                    Intent intent = new Intent(NotificationActivity.this, NewSignatureActivity.class);
                                    intent.putExtra("notification", notificationLists.get(pos));
                                    startActivity(intent);

                                }
                            }
                        });
                        rvNotification.setAdapter(mAdapter);
                    } else {
                        rvNotification.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(NotificationActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(NotificationActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(NotificationActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    rvNotification.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<getNotification> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(NotificationActivity.this, "", t.getMessage() + "");
            }
        });

    }

    public void callAPI(final Context context, String id, final String flag, final int pos) {
        //Progress bar
        progressDialog = ProgressDialog.show(NotificationActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<NotificationAcceptDeny> userPost = retrofitClass.notificationStatus(Integer.parseInt(id), Integer.parseInt(flag));
        userPost.enqueue(new Callback<NotificationAcceptDeny>() {
            @Override
            public void onResponse(Call<NotificationAcceptDeny> call, Response<NotificationAcceptDeny> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equalsIgnoreCase("1") || response.body().getResponseCode().equalsIgnoreCase("2")) {
                    notificationLists.get(pos).setIsAccept(flag);
                    rvNotification.getAdapter().notifyDataSetChanged();
                }
                Toast.makeText(context, "" + response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<NotificationAcceptDeny> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(context, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}

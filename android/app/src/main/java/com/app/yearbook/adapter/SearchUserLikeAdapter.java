package com.app.yearbook.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.makeramen.roundedimageview.RoundedImageView;
import com.app.yearbook.R;
import com.app.yearbook.model.studenthome.LikeList;

import java.util.ArrayList;

public class SearchUserLikeAdapter extends RecyclerView.Adapter<SearchUserLikeAdapter.MyViewHolder> {
    private ArrayList<LikeList> userLists;
    private Activity ctx;
    public ProgressDialog progressDialog;

    @NonNull
    @Override
    public SearchUserLikeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_searchuser, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchUserLikeAdapter.MyViewHolder holder, final int position) {
        final LikeList userList = userLists.get(position);
        holder.tvUserName.setText(userList.getUserFirstname() + "  " + userList.getUserLastname());
        holder.tvUserType.setVisibility(View.GONE);

        Glide.with(ctx)
                .load(userList.getUserImage())
                .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.profile_placeholder)
                .error(R.mipmap.profile_placeholder)
                .into(holder.imgUserPhoto);


    }

    public SearchUserLikeAdapter(ArrayList<LikeList> userLists, Activity context) {
        this.userLists = userLists;
        this.ctx = context;
    }

    @Override
    public int getItemCount() {
        return userLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvUserName, tvUserType;
        public RoundedImageView imgUserPhoto;
        public ImageView imgReport;
        public View view;


        public MyViewHolder(View view) {
            super(view);
            tvUserType = view.findViewById(R.id.tvUserType);
            tvUserName = view.findViewById(R.id.tvUserName);
            imgUserPhoto = view.findViewById(R.id.imgUserImage);
            imgReport=view.findViewById(R.id.imgReport);
            view=view.findViewById(R.id.view);
            view.setVisibility(View.VISIBLE);

        }
    }
}

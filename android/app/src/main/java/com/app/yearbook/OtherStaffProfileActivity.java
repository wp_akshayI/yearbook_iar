package com.app.yearbook;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.app.yearbook.databinding.ActivityStaffProfileBinding;
import com.app.yearbook.databinding.ActivityUserProfileBinding;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.StaffProfile;
import com.app.yearbook.model.StaffProfileResponse;
import com.app.yearbook.model.UserDetail;
import com.app.yearbook.model.UserProfileResponse;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.Constants;
import com.github.ybq.android.spinkit.style.Circle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtherStaffProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "OtUsProActy";
    ActivityStaffProfileBinding binding;
    PostListItem postListItem;
    RetrofitClass retrofitClass;
    public ProgressDialog progressDialog;
    String userType = "";
    int viewUserType = 1;
    StaffProfile userDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_staff_profile);
        userType = LoginUser.getUserData().getUserType();
        if (getIntent().hasExtra(Constants.USER_DATA)) {
            postListItem = getIntent().getParcelableExtra(Constants.USER_DATA);
            retrofitClass = APIClient.getClient().create(RetrofitClass.class);
            getUserProfile();
        }
        setToolbar();
    }

    private void getUserProfile() {
        progressDialog = ProgressDialog.show(this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        viewUserType = postListItem.getFlag().equalsIgnoreCase("staff") ? 2: 1;

        Call<StaffProfileResponse> getUserProfile = retrofitClass.getStaffProfile(viewUserType == 2 ? postListItem.getStaffId() : postListItem.getUserId());
        getUserProfile.enqueue(new Callback<StaffProfileResponse>() {
            @Override
            public void onResponse(Call<StaffProfileResponse> call, Response<StaffProfileResponse> response) {
                Log.d(TAG, "onResponse: ");
                progressDialog.dismiss();
                StaffProfileResponse userProfileResponse = response.body();
                if (userProfileResponse != null) {
                    userDetail = userProfileResponse.getUserProfile();
                    binding.setModel(userDetail);

                    binding.conParent.setVisibility(View.VISIBLE);

                    binding.conAsk.setVisibility(View.VISIBLE);
//                    if (userDetail.isYearbookPublishPeriod()) {
                        Drawable background = binding.btnAsk.getBackground();
                        if (background instanceof ShapeDrawable) {
                            ((ShapeDrawable) background).getPaint().setColor(ContextCompat.getColor(OtherStaffProfileActivity.this, R.color.black));
                        } else if (background instanceof GradientDrawable) {
                            ((GradientDrawable) background).setColor(ContextCompat.getColor(OtherStaffProfileActivity.this, R.color.colorBlack));
                        } else if (background instanceof ColorDrawable) {
                            ((ColorDrawable) background).setColor(ContextCompat.getColor(OtherStaffProfileActivity.this, R.color.colorBlack));
                        }

                        if (userDetail.isYearbookPublish()) {
                            binding.btnAsk.setOnClickListener(OtherStaffProfileActivity.this);
                        } else {
                            disableButtonSign();
                        }
                    /*}else
                        disableButtonSign();*/
                }
            }

            @Override
            public void onFailure(Call<StaffProfileResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
                progressDialog.dismiss();
            }
        });
    }

    private void disableButton() {
        Drawable background = binding.btnAsk.getBackground();
        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable) background).getPaint().setColor(ContextCompat.getColor(OtherStaffProfileActivity.this, R.color.colorGap));
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable) background).setColor(ContextCompat.getColor(OtherStaffProfileActivity.this, R.color.colorGap));
        } else if (background instanceof ColorDrawable) {
            ((ColorDrawable) background).setColor(ContextCompat.getColor(OtherStaffProfileActivity.this, R.color.colorGap));
        }
        binding.btnAsk.setEnabled(false);
    }

    private void disableButtonSign() {
        disableButton();
        binding.txtLblAsk.setText(getString(R.string.signature_will_be_available));
    }


    private void setToolbar() {
        setSupportActionBar(binding.appbar.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (LoginUser.getUserData() != null && LoginUser.getUserData().getSchoolName() != null) {
            binding.appbar.toolbarTitle.setText(LoginUser.getUserData().getSchoolName());
        }else {
            binding.appbar.toolbarTitle.setText(getString(R.string.app_name));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        addSignatureRequest();
    }

    private void addSignatureRequest() {
        progressDialog = ProgressDialog.show(this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        Call<Login> addSignatureRequest = retrofitClass.addSignatureRequest(LoginUser.getUserId()+"",
                viewUserType == 2 ? postListItem.getStaffId() : postListItem.getUserId(), LoginUser.getUserTypeKey()+"", LoginUser.getUserTypeKey(postListItem.getFlag())+"");

        addSignatureRequest.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                progressDialog.dismiss();
                Login login = response.body();
                if (login.getResult().equalsIgnoreCase("true") || login.getResponseMsg().contains("already"))
                    disableButton();
                Toast.makeText(OtherStaffProfileActivity.this, login.getResponseMsg(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                progressDialog.dismiss();
                Log.d(TAG, "onResponse: ");
            }
        });

    }
}

package com.app.yearbook.model.allpost;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.app.yearbook.BR;

public class OptionsModel extends BaseObservable implements Parcelable {
    String option;
    boolean isSelected;
    boolean isEdited;

    @Bindable
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
        notifyPropertyChanged(BR.selected);
    }


    public OptionsModel(String option) {
        this.option = option;
    }

    public OptionsModel(String option, boolean isEdited) {
        this.option = option;
        this.isEdited = isEdited;
    }

    protected OptionsModel(Parcel in) {
        option = in.readString();
    }

    public static final Creator<OptionsModel> CREATOR = new Creator<OptionsModel>() {
        @Override
        public OptionsModel createFromParcel(Parcel in) {
            return new OptionsModel(in);
        }

        @Override
        public OptionsModel[] newArray(int size) {
            return new OptionsModel[size];
        }
    };

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(option);
    }

    public boolean isEdited() {
        return isEdited;
    }

    public void setEdited(boolean edited) {
        isEdited = edited;
    }
}

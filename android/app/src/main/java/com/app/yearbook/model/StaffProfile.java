package com.app.yearbook.model;

import com.google.gson.annotations.SerializedName;

public class StaffProfile {

    @SerializedName("staff_id")
    private String staffId;

    @SerializedName("staff_firstname")
    private String staffFirstname;

    @SerializedName("staff_lastname")
    private String staffLastname;

    @SerializedName("school_id")
    private String schoolId;

    @SerializedName("school_name")
    private String schoolName;

    @SerializedName("school_start_date")
    private String schoolStartDate;

    @SerializedName("school_end_date")
    private String schoolEndDate;

    @SerializedName("staff_phone")
    private Object staffPhone;

    @SerializedName("staff_email")
    private String staffEmail;

    @SerializedName("staff_password")
    private String staffPassword;

    @SerializedName("staff_image")
    private String staffImage;

    @SerializedName("staff_device_type")
    private String staffDeviceType;

    @SerializedName("staff_device_token")
    private String staffDeviceToken;

    @SerializedName("staff_access_code")
    private String staffAccessCode;

    @SerializedName("user_type")
    private String userType;

    @SerializedName("staff_request_status")
    private String staffRequestStatus;

    @SerializedName("staff_token")
    private String staffToken;

    @SerializedName("staff_is_agree")
    private String staffIsAgree;

    @SerializedName("yearbook_publish")
    private boolean yearbookPublish;

    @SerializedName("in_yearbook_publish_period")
    private String inYearbookPublishPeriod;

    public boolean isYearbookPublishPeriod() {
        return (inYearbookPublishPeriod!=null && inYearbookPublishPeriod.equalsIgnoreCase("Yes"));
    }
    public String getInYearbookPublishPeriod() {
        return inYearbookPublishPeriod;
    }
    public void setInYearbookPublishPeriod(String inYearbookPublishPeriod) {
        this.inYearbookPublishPeriod = inYearbookPublishPeriod;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffFirstname() {
        return staffFirstname;
    }

    public void setStaffFirstname(String staffFirstname) {
        this.staffFirstname = staffFirstname;
    }

    public String getStaffLastname() {
        return staffLastname;
    }

    public void setStaffLastname(String staffLastname) {
        this.staffLastname = staffLastname;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolStartDate() {
        return schoolStartDate;
    }

    public void setSchoolStartDate(String schoolStartDate) {
        this.schoolStartDate = schoolStartDate;
    }

    public String getSchoolEndDate() {
        return schoolEndDate;
    }

    public void setSchoolEndDate(String schoolEndDate) {
        this.schoolEndDate = schoolEndDate;
    }

    public Object getStaffPhone() {
        return staffPhone;
    }

    public void setStaffPhone(Object staffPhone) {
        this.staffPhone = staffPhone;
    }

    public String getStaffEmail() {
        return staffEmail;
    }

    public void setStaffEmail(String staffEmail) {
        this.staffEmail = staffEmail;
    }

    public String getStaffPassword() {
        return staffPassword;
    }

    public void setStaffPassword(String staffPassword) {
        this.staffPassword = staffPassword;
    }

    public String getStaffImage() {
        return staffImage;
    }

    public void setStaffImage(String staffImage) {
        this.staffImage = staffImage;
    }

    public String getStaffDeviceType() {
        return staffDeviceType;
    }

    public void setStaffDeviceType(String staffDeviceType) {
        this.staffDeviceType = staffDeviceType;
    }

    public String getStaffDeviceToken() {
        return staffDeviceToken;
    }

    public void setStaffDeviceToken(String staffDeviceToken) {
        this.staffDeviceToken = staffDeviceToken;
    }

    public String getStaffAccessCode() {
        return staffAccessCode;
    }

    public void setStaffAccessCode(String staffAccessCode) {
        this.staffAccessCode = staffAccessCode;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getStaffRequestStatus() {
        return staffRequestStatus;
    }

    public void setStaffRequestStatus(String staffRequestStatus) {
        this.staffRequestStatus = staffRequestStatus;
    }

    public String getStaffToken() {
        return staffToken;
    }

    public void setStaffToken(String staffToken) {
        this.staffToken = staffToken;
    }

    public String getStaffIsAgree() {
        return staffIsAgree;
    }

    public void setStaffIsAgree(String staffIsAgree) {
        this.staffIsAgree = staffIsAgree;
    }

    public boolean isYearbookPublish() {
        return yearbookPublish;
    }

    public void setYearbookPublish(boolean yearbookPublish) {
        this.yearbookPublish = yearbookPublish;
    }
}
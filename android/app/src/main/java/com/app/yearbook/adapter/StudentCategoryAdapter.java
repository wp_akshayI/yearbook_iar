package com.app.yearbook.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.DisplayStudentYearbookActivity;
import com.app.yearbook.DisplayYearbookActivity;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.yearbook.CategoryList;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentCategoryAdapter extends RecyclerView.Adapter<StudentCategoryAdapter.MyViewHolder> {
    private ArrayList<CategoryList> categoryLists;
    private Activity ctx;
    private String type;
    public ProgressDialog progressDialog;

    @NonNull
    @Override
    public StudentCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_studentyear_list, parent, false);

        return new MyViewHolder(itemView);
    }

    public StudentCategoryAdapter(ArrayList<CategoryList> categoryLists, Activity context, String type) {
        this.categoryLists = categoryLists;
        this.ctx = context;
        this.type = type;
    }

    @Override
    public void onBindViewHolder(@NonNull final StudentCategoryAdapter.MyViewHolder holder, final int position) {

        final CategoryList userList = categoryLists.get(position);
        holder.tvYear.setText(userList.getCategoryName() + "");

        if (type.equalsIgnoreCase("staff")) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, DisplayYearbookActivity.class);
                    i.putExtra("category", userList);
                    ctx.startActivity(i);
                    //Toast.makeText(ctx,userList.getYearbookId()+"",Toast.LENGTH_SHORT).show();
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Log.d("TTT", "Long click");
                    PopupMenu popup = new PopupMenu(ctx, holder.imgArrow);
                    popup.getMenuInflater().inflate(R.menu.same_student_opt, popup.getMenu());

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            Log.d("TTT", "Item: " + item.getTitle());
                            final String id = userList.getCategoryId();
                            if (item.getTitle().equals("Edit")) {

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ctx);
                                LayoutInflater inflater = ctx.getLayoutInflater();
                                final View dialogView = inflater.inflate(R.layout.custom_alertbox_report, null);
                                dialogBuilder.setView(dialogView);
                                final AlertDialog alert = dialogBuilder.create();
                                alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                                final Button btnSend = dialogView.findViewById(R.id.btnSend);
                                final EditText etMessage = dialogView.findViewById(R.id.etMessage);
                                etMessage.setLines(1);
                                etMessage.setHint("Add category name");
                                etMessage.setText(userList.getCategoryName());

                                TextView tvTitle = dialogView.findViewById(R.id.tvTitle);
                                tvTitle.setText("Edit category");
                                etMessage.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                        if (s.length() > 0) {
                                            btnSend.setEnabled(true);
                                            btnSend.setBackgroundColor(ctx.getResources().getColor(R.color.btnGray));
                                            btnSend.setTextColor(ctx.getResources().getColor(R.color.white));
                                        } else {
                                            btnSend.setEnabled(false);
                                            btnSend.setBackgroundColor(ctx.getResources().getColor(R.color.edtBG));
                                            btnSend.setTextColor(ctx.getResources().getColor(R.color.deselectTab));
                                        }
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {

                                    }
                                });
                                btnSend.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        progressDialog = ProgressDialog.show(ctx, "", "", true);
                                        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                        progressDialog.setContentView(R.layout.progress_view);
                                        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                                        Circle bounce = new Circle();
                                        bounce.setColor(Color.BLACK);
                                        progressBar.setIndeterminateDrawable(bounce);

                                        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                        final Call<GiveReport> report = retrofitClass.editCategory(Integer.parseInt(userList.getYearbookId()), Integer.parseInt(id), etMessage.getText().toString());
                                        report.enqueue(new Callback<GiveReport>() {
                                            @Override
                                            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                                alert.dismiss();
                                                if (progressDialog.isShowing()) {
                                                    progressDialog.dismiss();
                                                }

                                                if (response.body().getResponseCode().equalsIgnoreCase("1")) {
                                                    for (int i = 0; i < categoryLists.size(); i++) {
                                                        if (categoryLists.get(i).getCategoryId().equalsIgnoreCase(userList.getCategoryId())) {
                                                            categoryLists.get(i).setCategoryName(etMessage.getText().toString());
                                                            notifyItemChanged(i);
                                                        }
                                                    }
                                                }
                                                else if(response.body().getResponseCode().equals("10"))
                                                {
                                                    Toast.makeText(ctx,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                                                    LoginUser loginSP = new LoginUser(ctx);
                                                    loginSP.clearData();

                                                    Intent i = new Intent(ctx, LoginActivity.class);
                                                    ctx.startActivity(i);
                                                    ctx.finish();
                                                }
                                                else {
                                                    Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<GiveReport> call, Throwable t) {
                                                alert.dismiss();
                                                if (progressDialog.isShowing()) {
                                                    progressDialog.dismiss();
                                                }
                                                Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                });
                                alert.show();
                            } else if (item.getTitle().equals("Delete")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                                builder.setMessage("Are you sure you want to delete the category?")
                                        .setCancelable(false)
                                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                progressDialog = ProgressDialog.show(ctx, "", "", true);
                                                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                                progressDialog.setContentView(R.layout.progress_view);
                                                ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                                                Circle bounce = new Circle();
                                                bounce.setColor(Color.BLACK);
                                                progressBar.setIndeterminateDrawable(bounce);

                                                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                                final Call<GiveReport> deletePost = retrofitClass.deleteCategory(Integer.parseInt(id));
                                                deletePost.enqueue(new Callback<GiveReport>() {
                                                    @Override
                                                    public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                                        if (progressDialog.isShowing()) {
                                                            progressDialog.dismiss();
                                                        }

                                                        if (response.body().getResponseCode().equals("1")) {
                                                            for (int i = 0; i < categoryLists.size(); i++) {
                                                                if (categoryLists.get(i).getCategoryId().equals(String.valueOf(id))) {
                                                                    Log.d("TTT", "Removeeee " + id + " / " + i);
                                                                    categoryLists.remove(i);
                                                                    notifyItemRemoved(i);
                                                                }
                                                            }
                                                        }
                                                        else if (response.body().getResponseCode().equals("10")) {
                                                            Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                                            LoginUser loginSP = new LoginUser(ctx);
                                                            loginSP.clearData();

                                                            Intent i = new Intent(ctx, LoginActivity.class);
                                                            ctx.startActivity(i);
                                                            ctx.finish();
                                                        }
                                                        else {
                                                            Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<GiveReport> call, Throwable t) {
                                                        if (progressDialog.isShowing()) {
                                                            progressDialog.dismiss();
                                                        }
                                                        Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                            }
                                        })
                                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.dismiss();
                                                    }
                                                }

                                        );

                                //Creating dialog box
                                AlertDialog alert = builder.create();
                                alert.setTitle("");
                                alert.show();
                            }
                            return true;
                        }
                    });

                    popup.show();
                    return true;
                }
            });
        } else if (type.equalsIgnoreCase("user")) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, DisplayStudentYearbookActivity.class);
                    i.putExtra("category", userList);
                    ctx.startActivity(i);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return categoryLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvYear, tvSchoolName;
        private ImageView imgArrow;

        public MyViewHolder(View view) {
            super(view);
            tvYear = view.findViewById(R.id.tvYear);
            tvSchoolName = view.findViewById(R.id.tvSchoolName);
            tvSchoolName.setVisibility(View.GONE);
            imgArrow = view.findViewById(R.id.imgArrow);

        }
    }
}

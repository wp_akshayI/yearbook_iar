package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.SearchUserLikeAdapter;
import com.app.yearbook.model.studenthome.GetLike;
import com.app.yearbook.model.studenthome.LikeList;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetAllStudentLikeActivity extends AppCompatActivity {

    private int postId;
    private Toolbar toolbar;
    public ProgressDialog progressDialog;
    private AllMethods allMethods;
    private RecyclerView rvLike;
    private LinearLayout lvNoData;
    private String Type="";
    public static ArrayList<LikeList> getLikeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_all_student_like);

        setView();
    }

    public void setView()
    {
        //get intent data
        if(getIntent().getExtras()!=null)
        {
            postId=Integer.parseInt(getIntent().getStringExtra("postId"));
            Type=getIntent().getStringExtra("Type");
            Log.d("TTT","post id: "+postId);
        }

        //set toolbar
        setToolbar();

        //all methods object
        allMethods = new AllMethods();

        //rv
        rvLike=findViewById(R.id.rvLike);

        //lvNoData
        lvNoData=findViewById(R.id.lvNoData);

        //chk like type
        if(Type.equalsIgnoreCase("yearbook"))
        {
            getYearbookLike();
        }
        else {
            getLike();
        }
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("All likes");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //display all like
    public void getLike()
    {
        //Progress bar
        progressDialog = ProgressDialog.show(GetAllStudentLikeActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetLike> userlike = retrofitClass.getAllLike(postId);
        userlike.enqueue(new Callback<GetLike>() {
            @Override
            public void onResponse(Call<GetLike> call, Response<GetLike> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if(response.body().getResponseCode().equalsIgnoreCase("1"))
                {
                    getLikeList=new ArrayList<>();
                    if (response.body().getLikeList().size() > 0) {
                        rvLike.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        getLikeList.addAll(response.body().getLikeList());
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(GetAllStudentLikeActivity.this);
                        rvLike.setLayoutManager(mLayoutManager);

                        SearchUserLikeAdapter mAdapter = new SearchUserLikeAdapter(getLikeList, GetAllStudentLikeActivity.this);
                        rvLike.setAdapter(mAdapter);

                    } else {
                        rvLike.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10")) {
                    Toast.makeText(GetAllStudentLikeActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(GetAllStudentLikeActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(GetAllStudentLikeActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    rvLike.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<GetLike> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(GetAllStudentLikeActivity.this, "", t.getMessage() + "");
            }
        });
    }

    //get yearbook's like
    public void getYearbookLike()
    {
        //Progress bar
        progressDialog = ProgressDialog.show(GetAllStudentLikeActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetLike> userlike = retrofitClass.getAllYearbookLike(postId);
        userlike.enqueue(new Callback<GetLike>() {
            @Override
            public void onResponse(Call<GetLike> call, Response<GetLike> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if(response.body().getResponseCode().equalsIgnoreCase("1"))
                {
                    getLikeList=new ArrayList<>();
                        rvLike.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        if (response.body().getLikeList().size() > 0) {
                        getLikeList.addAll(response.body().getLikeList());
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(GetAllStudentLikeActivity.this);
                        rvLike.setLayoutManager(mLayoutManager);

                        SearchUserLikeAdapter mAdapter = new SearchUserLikeAdapter(getLikeList, GetAllStudentLikeActivity.this);
                        rvLike.setAdapter(mAdapter);

                    } else {
                        rvLike.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(GetAllStudentLikeActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(GetAllStudentLikeActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(GetAllStudentLikeActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    rvLike.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<GetLike> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(GetAllStudentLikeActivity.this, "", t.getMessage() + "");
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.app.yearbook.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.app.yearbook.R;
import com.app.yearbook.databinding.FgAccessCodeDisableDetailBinding;

public class AccessCodeDisableDetail extends Fragment {

    FgAccessCodeDisableDetailBinding binding;
    int txtFor = 1;

    public AccessCodeDisableDetail(int txtFor) {
        this.txtFor = txtFor;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fg_access_code_disable_detail, container, false);

        if (txtFor == 1)
            binding.txt.setText(getString(R.string.access_no_bookmar_have_made_ybook));
        else
            binding.txt.setText(getString(R.string.access_no_notification_for_current_));

        return binding.getRoot();
    }
}

import { action, global } from '../utils'
import { postResponse } from '../utils/funtions'
import { set_yearbook } from './Yearbook'

export const setAuthToken = (token) => {
  return {
    type: action.SET_AUTH_TOKEN,
    payload: token
  }
}

export function check_local_token () {
  return async (dispatch) => {
    try {
      let data = await localStorage.getItem(action.user_auth)
      if (data) {
        data = JSON.parse(data)
        return data
      }
    } catch (error) {
      return null
    }
  }
}

export const setInitUrl = (url) => {
  return {
    type: action.INIT_URL,
    payload: url
  }
}

export const userAuthSuccess = (data) => {
  return {
    type: action.AUTH_SUCCESS,
    payload: data
  }
}

export function check_auth_token (token, id) {
  return async (dispatch) => {
    const request = {
      token,
      id
    }
    const res = await postResponse('login.php', request)
    if (res.Result === false) return res
    const data = res.Data
    dispatch(setAuthToken(data.token))
    dispatch(set_yearbook(data.yearbook.yearbook_id))
    dispatch({
      type: action.get_yearbook_details,
      yearbookDetails: data.yearbook
    })
    localStorage.setItem(action.user_auth, JSON.stringify({ token: data.token, yearbook_id: id }))
    return res
  }
}

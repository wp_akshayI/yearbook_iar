import React from 'react';
import { useDispatch } from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import MobilePaper from './MobilePaper';
import AddIcon from '@material-ui/icons/Add';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { action } from '../utils';
import { withRouter } from "react-router-dom"

const useStyles = makeStyles(theme => ({
    modal: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    paper: {
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
        display: "flex"
    },
    listTitle: {
        color: "#808080",
        paddingBottom: theme.spacing(1)
    },
    subtitle2: {
        color: "#A9A9A9",
        textAlign: "center"
    },
    button: {
        margin: theme.spacing(1),
    },
    title: {
        color: "#fff"
    }
}));

function AddCategoryDialog({ title, onSubmit }) {
    const dispatch = useDispatch();
    const classes = useStyles();
    const [state, setState] = React.useState({
        open: false,
        title: ""
    });

    const handleOpen = () => {
        setState({ ...state, open: true });
    };

    const handleClose = () => {
        setState({ ...state, open: false });
    };

    const onSave = () => {
        if (state.title === "") return dispatch({ type: action.open_notification, message: "Name is required!", variant: "warning" });
        onSubmit(state.title);
        setState({ ...state, title: "", open: false });
    }

    return (
        <>
            <div style={{ height: "100%", width: 220, margin: 15, cursor: "pointer" }}>
                <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center", height: 45 }}>
                    <Typography variant="subtitle1" className={classes.listTitle}>
                        {title}
                    </Typography>
                </div>
                <MobilePaper>
                    <div
                        className={classes.paper}
                        onClick={handleOpen}
                    >
                        <div style={{ textAlign: "center" }}>
                            <AddIcon color="disabled" fontSize="large" />
                            <Typography
                                className={classes.subtitle2}
                                variant="body1"
                                noWrap>
                                {title}
                            </Typography>
                        </div>
                    </div>
                </MobilePaper>
            </div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={state.open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={state.open}>
                    <div style={{ backgroundColor: "#fff", width: "30%" }}>
                        <div style={{ height: 60, backgroundColor: "#000", display: "flex", alignItems: "center", paddingLeft: 20 }}>
                            <Typography className={classes.title} variant="h6" noWrap>
                                {title}
                            </Typography>
                        </div>
                        <div style={{ padding: 20 }}>
                            <TextField
                                id="outlined-full-width"
                                label="Name"
                                fullWidth
                                value={state.title}
                                onChange={(e) => setState({ ...state, title: e.target.value })}
                                margin="normal"
                                variant="outlined"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </div>
                        <div style={{ display: "flex", float: "right", paddingRight: 10, paddingBottom: 5 }}>
                            <Button
                                variant="contained"
                                color="primary"
                                className={classes.button}
                                onClick={onSave}
                            >
                                Save
                            </Button>
                            <Button
                                variant="outlined"
                                color="primary"
                                className={classes.button}
                                onClick={handleClose}
                            >
                                Close
                            </Button>
                        </div>
                    </div>
                </Fade>
            </Modal>
        </>
    );
}

export default withRouter(AddCategoryDialog);
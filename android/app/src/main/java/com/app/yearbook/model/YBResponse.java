package com.app.yearbook.model;

import android.text.TextUtils;

import com.app.yearbook.model.studenthome.GiveReport;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YBResponse extends GiveReport {
    @SerializedName("Data")
    public Data data = new Data();

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        @SerializedName("yearbook_list")
        public List<YearbookList> yearbookList = null;
        @SerializedName("user_type")
        public String userType;
        @SerializedName("is_archive")
        public String isArchive;

        public List<YearbookList> getYearbookList() {
            return yearbookList;
        }

        public void setYearbookList(List<YearbookList> yearbookList) {
            this.yearbookList = yearbookList;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getIsArchive() {
            return isArchive;
        }

        public void setIsArchive(String isArchive) {
            this.isArchive = isArchive;
        }
    }

    public class YearbookList {
        @SerializedName("yearbook_id")
        public String yearbookId;
        @SerializedName("school_id")
        public String schoolId;
        @SerializedName("yearbook_name")
        public String yearbookName;
        @SerializedName("publish_year")
        public String publishYear;
        @SerializedName("yearbook_amount")
        public String yearbookAmount;
        @SerializedName("admin_yearbook_amount")
        public String adminYearbookAmount;
        @SerializedName("yearbook_cover")
        public String yearbookCover;
        @SerializedName("yearbook_status")
        public String yearbookStatus;
        @SerializedName("progress")
        public String progress;
        @SerializedName("yearbook_created")
        public String yearbookCreated;
        @SerializedName("is_deleted")
        public String isDeleted;
        @SerializedName("deleted_at")
        public Object deletedAt;
        @SerializedName("screen_id")
        public Integer screenId;
        @SerializedName("snap_shot")
        public String snapShot;
        @SerializedName("is_purchased")
        public String isPurchased;

        public String getYearbookId() {
            return yearbookId;
        }

        public void setYearbookId(String yearbookId) {
            this.yearbookId = yearbookId;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getYearbookName() {
            return yearbookName;
        }

        public void setYearbookName(String yearbookName) {
            this.yearbookName = yearbookName;
        }

        public String getPublishYear() {
            return publishYear;
        }

        public void setPublishYear(String publishYear) {
            this.publishYear = publishYear;
        }

        public String getYearbookAmount() {
            return yearbookAmount;
        }

        public void setYearbookAmount(String yearbookAmount) {
            this.yearbookAmount = yearbookAmount;
        }

        public String getAdminYearbookAmount() {
            return adminYearbookAmount;
        }

        public void setAdminYearbookAmount(String adminYearbookAmount) {
            this.adminYearbookAmount = adminYearbookAmount;
        }

        public String getYearbookCover() {
            return yearbookCover;
        }

        public void setYearbookCover(String yearbookCover) {
            this.yearbookCover = yearbookCover;
        }

        public String getYearbookStatus() {
            return yearbookStatus;
        }

        public void setYearbookStatus(String yearbookStatus) {
            this.yearbookStatus = yearbookStatus;
        }

        public String getProgress() {
            return progress;
        }

        public void setProgress(String progress) {
            this.progress = progress;
        }

        public String getYearbookCreated() {
            return yearbookCreated;
        }

        public void setYearbookCreated(String yearbookCreated) {
            this.yearbookCreated = yearbookCreated;
        }

        public String getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(String isDeleted) {
            this.isDeleted = isDeleted;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public Integer getScreenId() {
            return screenId;
        }

        public void setScreenId(Integer screenId) {
            this.screenId = screenId;
        }

        public String getSnapShot() {
            return snapShot;
        }

        public void setSnapShot(String snapShot) {
            this.snapShot = snapShot;
        }

        public String getIsPurchased() {
            return isPurchased;
        }

        public void setIsPurchased(String isPurchased) {
            this.isPurchased = isPurchased;
        }

        public boolean isPurchased(){

            return (!TextUtils.isEmpty(isPurchased) && !isPurchased.equalsIgnoreCase("No"))?true:false;
        }
    }
}
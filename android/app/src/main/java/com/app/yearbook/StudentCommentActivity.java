package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.StudentCommentListAdapter;
import com.app.yearbook.adapter.StudentYearbookCommentListAdapter;
import com.app.yearbook.fragment.HomeFragment;
import com.app.yearbook.fragment.StaffHomeFragment;
import com.app.yearbook.model.studenthome.AddComment;
import com.app.yearbook.model.studenthome.Comment;
import com.app.yearbook.model.studenthome.EditPost;
import com.app.yearbook.model.studenthome.GetComment;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.studenthome.PostList;
import com.app.yearbook.model.yearbook.CommentList;
import com.app.yearbook.model.yearbook.GetYearBookComment;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentCommentActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private ArrayList<Comment> getComments;
    private ArrayList<CommentList> getYearBookComments;
    private ProgressDialog progressDialog;
    public String postId, userId, type = "", getCommentFrom = "", yearbook = "";
    private LoginUser loginUser;
    private AllMethods allMethods;
    private RecyclerView rvComment;
    private ImageView btnSend;
    private StudentCommentListAdapter studentCommentListAdapter;
    private StudentYearbookCommentListAdapter studentYearbookCommentListAdapter;
    private EditText etComment;
    private LinearLayout lvSendMsg, lvNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_comment);

        setView();
    }

    public void setView() {
        //SP object
        loginUser = new LoginUser(getApplicationContext());
        if (loginUser.getUserData() != null) {
            userId = loginUser.getUserData().getUserId();
            Log.d("TTT", "User Id: " + userId);
        }

        //lv send msg
        lvSendMsg = findViewById(R.id.lvSendMsg);
        lvNoData = findViewById(R.id.lvNoData);

        //set toolbar
        setToolbar();

        //all methods object
        allMethods = new AllMethods();

        //intent value
        if (getIntent().getExtras() != null) {
            postId = getIntent().getStringExtra("PostId");
            // type = getIntent().getStringExtra("type");
            Log.d("TTT", "Post id: " + postId);
            getCommentFrom = getIntent().getStringExtra("comment");

            if (getCommentFrom.equalsIgnoreCase("staff")) {
                lvSendMsg.setVisibility(View.GONE);
            }

            if (getIntent().getStringExtra("displayComment") != null) {
                yearbook = getIntent().getStringExtra("displayComment");
            }
        }

        //rv
        rvComment = findViewById(R.id.rvStudentComment);

        //API
        if (yearbook.equalsIgnoreCase("yearbook")) {
            Log.d("TTT","getYearbookComments");
            getYearbookComments(Integer.parseInt(postId));
        } else {
            Log.d("TTT","getComments");
            getComments(Integer.parseInt(postId));
        }

        //btn send
        btnSend = findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);

        //edit text
        etComment = findViewById(R.id.etComment);
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Comments");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void getYearbookComments(int postId) {

        Log.d("TTT", "post iddd: " + postId);
        //Progress bar
        progressDialog = ProgressDialog.show(StudentCommentActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetYearBookComment> getComment = retrofitClass.getYearBookComments(postId);
        getComment.enqueue(new Callback<GetYearBookComment>() {
            @Override
            public void onResponse(Call<GetYearBookComment> call, Response<GetYearBookComment> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (response.body().getResponseCode().equals("1")) {

                    lvNoData.setVisibility(View.GONE);
                    rvComment.setVisibility(View.VISIBLE);

                    if (response.body().getCommentList().size() > 0) {
                        getYearBookComments = new ArrayList<>();
                        Log.d("TTT","Sizeee: "+response.body().getCommentList().size());
                        getYearBookComments.addAll(response.body().getCommentList());
                        Log.d("TTT","Sizeee getYearBookComments: "+getYearBookComments.size());
                        studentYearbookCommentListAdapter = new StudentYearbookCommentListAdapter(userId, getCommentFrom, getYearBookComments, StudentCommentActivity.this, new StudentYearbookCommentListAdapter.commentListner() {
                            @Override
                            public void onClickAtButton(int position, String flag) {
                                removeYearbookComment(position,flag);
                            }

                        });
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        rvComment.setLayoutManager(mLayoutManager);
                        rvComment.setAdapter(studentYearbookCommentListAdapter);
                    }
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(StudentCommentActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StudentCommentActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StudentCommentActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    lvNoData.setVisibility(View.VISIBLE);
                    rvComment.setVisibility(View.GONE);
                    //allMethods.setAlert(StudentCommentActivity.this, "", response.body().getResponseMsg() + "");
                }
            }

            @Override
            public void onFailure(Call<GetYearBookComment> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StudentCommentActivity.this, "", t.getMessage() + "");
            }
        });
    }

    public void getComments(int postId) {
        //Progress bar
        progressDialog = ProgressDialog.show(StudentCommentActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetComment> getComment = retrofitClass.getComments(postId);
        getComment.enqueue(new Callback<GetComment>() {
            @Override
            public void onResponse(Call<GetComment> call, Response<GetComment> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (response.body().getResponseCode().equals("1")) {

                    lvNoData.setVisibility(View.GONE);
                    rvComment.setVisibility(View.VISIBLE);

                    if (response.body().getCommentList().size() > 0) {
                        getComments = new ArrayList<>();
                        getComments.addAll(response.body().getCommentList());
                        studentCommentListAdapter = new StudentCommentListAdapter(userId, getCommentFrom, getComments, StudentCommentActivity.this, new StudentCommentListAdapter.commentListner() {
                            @Override
                            public void onClickAtButton(int position, String flag) {
                                removeComment(position,flag);
                            }
                        });
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        rvComment.setLayoutManager(mLayoutManager);
                        rvComment.setAdapter(studentCommentListAdapter);
                    }
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(StudentCommentActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StudentCommentActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StudentCommentActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    lvNoData.setVisibility(View.VISIBLE);
                    rvComment.setVisibility(View.GONE);
                    //allMethods.setAlert(StudentCommentActivity.this, "", response.body().getResponseMsg() + "");
                }
            }

            @Override
            public void onFailure(Call<GetComment> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StudentCommentActivity.this, "", t.getMessage() + "");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSend) {
            if (!etComment.getText().toString().equals("")) {
                AllMethods.hideKeyboard(getApplicationContext(),v);
                if (yearbook.equalsIgnoreCase("yearbook")) {
                    addYearboookComment();
                } else {
                    addComment();
                }
            } else {
                allMethods.setAlert(StudentCommentActivity.this, "", "Please enter message." + "");
            }
        }
    }

    public void addYearboookComment() {
        //Progress bar
        progressDialog = ProgressDialog.show(StudentCommentActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<AddComment> addComments = retrofitClass.addYearbookComments(Integer.parseInt(userId), Integer.parseInt(postId), etComment.getText().toString());
        addComments.enqueue(new Callback<AddComment>() {
            @Override
            public void onResponse(Call<AddComment> call, Response<AddComment> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    etComment.setText("");

                    Intent i = new Intent();
                    i.putExtra("Comment", response.body().getPostData().getPostDetail().getTotalComment());
                    i.putExtra("PostComment", response.body().getPostData().getPostDetail().getPostComment());
                    i.putExtra("PostId",response.body().getPostData().getPostDetail().getPostId());
                    setResult(RESULT_OK, i);
                    finish();
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(StudentCommentActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StudentCommentActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StudentCommentActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    allMethods.setAlert(StudentCommentActivity.this, "", response.body().getResponseMsg() + "");
                }
            }

            @Override
            public void onFailure(Call<AddComment> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StudentCommentActivity.this, "", t.getMessage() + "");
            }
        });
    }

    public void addComment() {
        //Progress bar
        progressDialog = ProgressDialog.show(StudentCommentActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<AddComment> addComments = retrofitClass.addComments(Integer.parseInt(userId), Integer.parseInt(postId), etComment.getText().toString());
        addComments.enqueue(new Callback<AddComment>() {
            @Override
            public void onResponse(Call<AddComment> call, Response<AddComment> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    etComment.setText("");

                    //home
                    if (HomeFragment.PostListArray != null) {
                        for (int i = 0; i < HomeFragment.PostListArray.size(); i++) {
                            if (HomeFragment.PostListArray.get(i).getPostId().equals(response.body().getPostData().getPostDetail().getPostId())) {
                                PostList newList = response.body().getPostData().getPostDetail();
                                HomeFragment.PostListArray.get(i).setTotalComment(newList.getTotalComment());
                                Log.d("TTT", "Set Total commet: " + newList.getTotalComment());
                                Log.d("TTT", "Get Total commet: " + HomeFragment.PostListArray.get(i).getTotalComment());
                            }
                        }
                    }

                    //profile
//                    if (ProfileFragment.getUserList != null) {
//                        for (int i = 0; i < ProfileFragment.getUserList.size(); i++) {
//                            if (ProfileFragment.getUserList.get(i).getPostId().equals(response.body().getPostData().getPostDetail().getPostId())) {
//                                PostList newList = response.body().getPostData().getPostDetail();
//                                ProfileFragment.getUserList.get(i).setTotalComment(newList.getTotalComment());
//                            }
//                        }
//                    }

                    //bookmark
                    if (StudentBookmarkActivity.bookmarkList != null) {
                        for (int i = 0; i < StudentBookmarkActivity.bookmarkList.size(); i++) {
                            if (StudentBookmarkActivity.bookmarkList.get(i).getPostId().equals(response.body().getPostData().getPostDetail().getPostId())) {
                                PostList newList = response.body().getPostData().getPostDetail();
                                StudentBookmarkActivity.bookmarkList.get(i).setTotalComment(newList.getTotalComment());
                            }
                        }
                    }

                    //user detail
//                    if (UserDetailActivity.getUserList != null) {
//                        for (int i = 0; i < UserDetailActivity.getUserList.size(); i++) {
//                            if (UserDetailActivity.getUserList.get(i).getPostId().equals(response.body().getPostData().getPostDetail().getPostId())) {
//                                PostList newList = response.body().getPostData().getPostDetail();
//                                UserDetailActivity.getUserList.get(i).setTotalComment(newList.getTotalComment());
//                            }
//                        }
//                    }

                    //user tag post
                    if (StudentPostTagImageActivity.getUserList != null) {
                        for (int i = 0; i < StudentPostTagImageActivity.getUserList.size(); i++) {
                            if (StudentPostTagImageActivity.getUserList.get(i).getPostId().equals(response.body().getPostData().getPostDetail().getPostId())) {
                                PostList newList = response.body().getPostData().getPostDetail();
                                StudentPostTagImageActivity.getUserList.get(i).setTotalComment(newList.getTotalComment());
                            }
                        }
                    }

                    Intent i = new Intent();
                    i.putExtra("Comment", response.body().getPostData().getPostDetail().getTotalComment());
                    setResult(RESULT_OK, i);
                    finish();
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(StudentCommentActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StudentCommentActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StudentCommentActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    allMethods.setAlert(StudentCommentActivity.this, "", response.body().getResponseMsg() + "");
                }
            }

            @Override
            public void onFailure(Call<AddComment> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StudentCommentActivity.this, "", t.getMessage() + "");
            }
        });
    }

    public void removeComment(final int id,String flag) {
        Log.d("TTT","flagg: "+flag);
        //Progress bar
        progressDialog = ProgressDialog.show(StudentCommentActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> deleteComments = retrofitClass.deleteComment(id, flag);
        deleteComments.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                Log.d("TTT", "Delete comment: " + response.body().getResponseMsg());
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    for (int i = 0; i < getComments.size(); i++) {
                        if (getComments.get(i).getCommentId().equals(String.valueOf(id))) {
                            getComments.remove(i);
                            rvComment.getAdapter().notifyDataSetChanged();
                        }
                    }

                    if (getComments.size() == 0) {
                        lvNoData.setVisibility(View.VISIBLE);
                        rvComment.setVisibility(View.GONE);
                    }

                    //home
                    if (HomeFragment.PostListArray != null) {
                        for (int i = 0; i < HomeFragment.PostListArray.size(); i++) {
                            if (HomeFragment.PostListArray.get(i).getPostId().equals(postId)) {
                                Log.d("TTT", "Get Total commet::: " + getComments.size());
                                HomeFragment.PostListArray.get(i).setTotalComment(getComments.size() + "");
                                Log.d("TTT", "Get Total commet: " + HomeFragment.PostListArray.get(i).getTotalComment());
                            }
                        }
                    }

                    //profile
//                    if (ProfileFragment.getUserList != null) {
//                        for (int i = 0; i < ProfileFragment.getUserList.size(); i++) {
//                            if (ProfileFragment.getUserList.get(i).getPostId().equals(postId)) {
//                                ProfileFragment.getUserList.get(i).setTotalComment(getComments.size() + "");
//                            }
//                        }
//                    }
//
                    //bookmark
                    if (StudentBookmarkActivity.bookmarkList != null) {
                        for (int i = 0; i < StudentBookmarkActivity.bookmarkList.size(); i++) {
                            if (StudentBookmarkActivity.bookmarkList.get(i).getPostId().equals(postId)) {
                                StudentBookmarkActivity.bookmarkList.get(i).setTotalComment(getComments.size() + "");
                            }
                        }
                    }

//                //user detail
//                    if (UserDetailActivity.getUserList != null) {
//                        for (int i = 0; i < UserDetailActivity.getUserList.size(); i++) {
//                            if (UserDetailActivity.getUserList.get(i).getPostId().equals(postId)) {
//                                UserDetailActivity.getUserList.get(i).setTotalComment(getComments.size() + "");
//                            }
//                        }
//                    }
                }
                else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(StudentCommentActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StudentCommentActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StudentCommentActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    allMethods.setAlert(StudentCommentActivity.this, "", response.body().getResponseMsg() + "");
                }
                    //staff side
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StudentCommentActivity.this, "", t.getMessage() + "");
            }
        });
    }

    public void removeYearbookComment(final int id,String flag) {

        Log.d("TTT","removeYearbookComment:  "+id+" / "+flag);
        //Progress bar
        progressDialog = ProgressDialog.show(StudentCommentActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<EditPost> deleteComments = retrofitClass.DeleteYaerbookComment(id);
        deleteComments.enqueue(new Callback<EditPost>() {
            @Override
            public void onResponse(Call<EditPost> call, Response<EditPost> response) {
                Log.d("TTT", "Delete comment: " + response.body().getResponseMsg());
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (response.body().getResponseCode().equals("1")) {
                    for (int i = 0; i < getYearBookComments.size(); i++) {
                        if (getYearBookComments.get(i).getCommentId().equals(String.valueOf(id))) {
                            getYearBookComments.remove(i);
                            rvComment.getAdapter().notifyDataSetChanged();
                        }
                    }

                    //yearbook home
                    if (StaffHomeFragment.PostListArray != null) {
                        for (int i = 0; i < StaffHomeFragment.PostListArray.size(); i++) {
                            if (StaffHomeFragment.PostListArray.get(i).getPostId().equals(postId)) {
                                Log.d("TTT", "Get Total commet::: " + getComments.size());
                                StaffHomeFragment.PostListArray.get(i).setTotalComment(getComments.size() + "");
                                Log.d("TTT", "Get Total commet: " + HomeFragment.PostListArray.get(i).getTotalComment());
                            }
                        }
                    }

                    //manage staff post activity
                    if (ManageStaffPostActivity.postList != null) {
                        ManageStaffPostActivity.postList.setTotalComment(response.body().getPostDetail().get(0).getTotalComment());
                    }

                    if (getYearBookComments.size() == 0) {
                        lvNoData.setVisibility(View.VISIBLE);
                        rvComment.setVisibility(View.GONE);
                    }
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(StudentCommentActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StudentCommentActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StudentCommentActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    Toast.makeText(StudentCommentActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EditPost> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StudentCommentActivity.this, "", t.getMessage() + "");
            }
        });
    }


}

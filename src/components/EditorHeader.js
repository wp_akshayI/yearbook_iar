import React from 'react';
import { withRouter } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import PageTitle from './Header/PageTitle';
import logo from "../assets/logo.png"

function EditorHeader({ title, onTitleChange, history, onSave, lastUpdated }) {
  const classes = useStyles();
  // const path = location.pathname;
  // const [state, setState] = useState({
  //   title: title
  // });
  // console.log(state, title)
  const _navigate = (pathname) => {
    return history.push({ pathname })
  }

  const _onSave = () => {
    onSave();
  }

  return (
    <div >
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <div className={classes.sectionDesktop}>

            <Button
              classes={{ label: classes.buttonLable }}
              onClick={() => _navigate("/app/dashboard")}
              color="inherit">
              Home
            </Button>

            <Button
              classes={{ label: classes.buttonLable }}
              color="inherit"
              onClick={_onSave}>
              Save
              </Button>

            {/* <Button classes={{ label: classes.buttonLable }} color="inherit">Jump to page</Button> */}

          </div>

          <div className={classes.grow} />

          <PageTitle
            onChange={(e) => onTitleChange(e.target.value)}
            value={title}
          />

          <div className={classes.grow} />

          <Typography className={classes.subtitle2} variant="subtitle2" noWrap>
            Auto save at {lastUpdated}
          </Typography>

          <div className={classes.grow} />

          <div className={classes.sectionDesktop} style={{ width: 35 }}>
            <img src={logo} style={{ height: 25, objectFit: "contain" }} />
          </div>

        </Toolbar>
      </AppBar>
    </div>
  );
}

export default withRouter(EditorHeader)

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  title: {
    display: 'none',
    color: "#b3b3b3",
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  hightlight: {
    color: "#FFF",
  },
  search: {
    position: 'relative',
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
    borderBottomWidth: 1,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderStyle: "solid"
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 1),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 300,
    },
  },
  sectionDesktop: {
    // display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  subtitle2: {
    color: "#b3b3b3"
  },
  buttonLable: {
    textTransform: "capitalize",
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    color: "#b3b3b3"
  },
}));
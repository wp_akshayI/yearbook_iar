package com.app.yearbook.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GradesResponse {

    @SerializedName("ResponseCode")
    private String responseCode;

    @SerializedName("ServerTimeZone")
    private String serverTimeZone;

    @SerializedName("ResponseMsg")
    private String responseMsg;

    @SerializedName("serverTime")
    private String serverTime;

    @SerializedName("grade_list")
    private List<GradeListItem> gradeList;

    @SerializedName("Result")
    private String result;

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setServerTimeZone(String serverTimeZone) {
        this.serverTimeZone = serverTimeZone;
    }

    public String getServerTimeZone() {
        return serverTimeZone;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setGradeList(List<GradeListItem> gradeList) {
        this.gradeList = gradeList;
    }

    public List<GradeListItem> getGradeList() {
        return gradeList;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}
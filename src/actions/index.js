import * as Yearbook from './Yearbook'
import * as Components from './Components'
import * as Category from './Category'
import * as Common from './Common'
import * as Auth from './Auth'

const Actions = {
  ...Yearbook,
  ...Components,
  ...Category,
  ...Common,
  ...Auth
}

export default Actions

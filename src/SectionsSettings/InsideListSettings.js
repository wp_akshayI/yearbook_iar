import React from "react"
import { View, Text, FlatList } from "react-native";
import { useSelector } from "react-redux"
import { colors } from "../utils";

export default function InsideListSettings({ name }) {
    const list = useSelector(({ components }) => components.pageInside)

    return (
        <View style={{ padding: 10, flex: 1 }}>

            <Text style={styles.title}>
                Pages Inside of <b>{name}</b>
            </Text>

            <View style={{ paddingTop: 15 }}>
                <FlatList
                    data={list || []}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={({ item, index }) => (
                        <Text style={styles.drowerList}>
                            {index + 1}. {item.name}
                        </Text>
                    )}
                />
            </View>
        </View>
    )
}

const styles = {
    title: {
        color: colors.white,
        fontSize: 15
    },
    drowerList: {
        color: colors.white,
        fontSize: 15,
        paddingVertical: 10
    }
}
package com.app.yearbook.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.adapter.StudentNotificationListAdapter;
import com.app.yearbook.model.NotificationAcceptDeny;
import com.app.yearbook.model.profile.NotificationList;
import com.app.yearbook.model.profile.getNotification;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.OnRecyclerClick;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFirstVersionFragment extends Fragment implements View.OnClickListener{

    public View view;
    public ProgressDialog progressDialog;
    private String userId;
    private AllMethods allMethods;
    private ArrayList<NotificationList> notificationLists;
    private RecyclerView rvNotification;
    private LinearLayout lvNoData;
    private LoginUser loginUser;

    public NotificationFirstVersionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_notification, container, false);
        setHasOptionsMenu(true);
        setView();
        return view;
    }

    public void setView() {
        //SP
        loginUser = new LoginUser(getActivity());
        if (loginUser.getUserData() != null) {
            userId = loginUser.getUserData().getUserId();
        }
        //all methods object
        allMethods = new AllMethods();

        //notification
        rvNotification=view.findViewById(R.id.rvNotification);

        //lv
        lvNoData=view.findViewById(R.id.lvNoData);

        getNotification();
    }
    public void getNotification()
    {
        //Progress bar
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<getNotification> notification = retrofitClass.getNotification(Integer.parseInt(userId), LoginUser.getUserTypeKey());
        notification.enqueue(new Callback<getNotification>() {
            @Override
            public void onResponse(Call<getNotification> call, Response<getNotification> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if(response.body().getResponseCode().equals("1"))
                {
                    if(response.body().getNotificationList().size()>0)
                    {
                        rvNotification.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        notificationLists=new ArrayList<>();
                        notificationLists.addAll(response.body().getNotificationList());

                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        rvNotification.setLayoutManager(mLayoutManager);

                        StudentNotificationListAdapter mAdapter = new StudentNotificationListAdapter(notificationLists, getActivity(), new OnRecyclerClick() {
                            @Override
                            public void onClick(int pos, View v, int type) {
                                if(type==1)
                                {
                                    //accept
                                    callAPI(getActivity(),notificationLists.get(pos).getNotificationId(),"1",pos);
                                }
                                else if(type==2)
                                {
                                    //deny
                                    callAPI(getActivity(),notificationLists.get(pos).getNotificationId(),"2",pos);
                                }
                            }
                        });
                        rvNotification.setAdapter(mAdapter);

                    }
                    else
                    {
                        rvNotification.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10")) {
                    Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(getActivity());
                    loginSP.clearData();

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);

                }
                else
                {
                    rvNotification.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<getNotification> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }

    public void callAPI(final Context context, String id, final String flag, final int pos)
    {
        //Progress bar
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<NotificationAcceptDeny> userPost = retrofitClass.notificationStatus(Integer.parseInt(id),Integer.parseInt(flag));
        userPost.enqueue(new Callback<NotificationAcceptDeny>() {
            @Override
            public void onResponse(Call<NotificationAcceptDeny> call, Response<NotificationAcceptDeny> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if(response.body().getResponseCode().equalsIgnoreCase("0"))
                {
                    notificationLists.get(pos).setIsAccept(flag);
                    rvNotification.getAdapter().notifyDataSetChanged();
                }
                Toast.makeText(context,""+response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<NotificationAcceptDeny> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(context,""+t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });

    }

}

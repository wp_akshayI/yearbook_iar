
package com.app.yearbook.model.staff.yearbook;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetStudentYearbook {

    @SerializedName("post_Detail")
    @Expose
    private List<StaffYBPostList> postList = null;

    @SerializedName("ResponseCode")
    @Expose
    private String responseCode;

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;

    @SerializedName("Result")
    @Expose
    private String result;

    @SerializedName("ServerTimeZone")
    @Expose
    private String serverTimeZone;

    @SerializedName("serverTime")
    @Expose
    private String serverTime;

    public List<StaffYBPostList> getPostList() {
        return postList;
    }

    public void setPostList(List<StaffYBPostList> postList) {
        this.postList = postList;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getServerTimeZone() {
        return serverTimeZone;
    }

    public void setServerTimeZone(String serverTimeZone) {
        this.serverTimeZone = serverTimeZone;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

}

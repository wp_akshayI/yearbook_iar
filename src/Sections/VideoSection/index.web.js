import React, { useState } from 'react'
import { play_ic, video_placeholder } from '../../assets'

export default function VideoSection ({ data }) {
  const [state, setstate] = useState(false)

  const _play = () => {
    if (data.component_url) {
      setstate(!state)
    }
  }

  return (
    <div style={{ width: '100%', position: 'relative', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
      {
        state ? (
          <video
            preload='auto'
            style={{ objectFit: 'fill', height: 'auto', width: '100%' }}
            src={data.component_url}
            autoPlay
            controls
          >
                        Your browser does not support the video tag.
          </video>
        )
          : (
            <>
              <img
                src={play_ic}
                style={{ width: 50, height: 50, objectFit: 'contain', position: 'absolute' }}
                onClick={_play}
              />
              <img
                src={data.component_thumb_url || video_placeholder}
                style={{ objectFit: 'contain', width: '100%' }}
              />
            </>
          )
      }
    </div>
  )
}

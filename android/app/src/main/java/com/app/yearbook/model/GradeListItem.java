package com.app.yearbook.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.app.yearbook.BR;
import com.google.gson.annotations.SerializedName;

public class GradeListItem extends BaseObservable implements Parcelable {

    @SerializedName("grade")
    private String grade;

    @SerializedName("student_grade_id")
    private String studentGradeId;


    public GradeListItem(String grade, String studentGradeId, boolean isSelected) {
        this.grade = grade;
        this.studentGradeId = studentGradeId;
        this.isSelected = isSelected;
    }

    boolean isSelected;

    protected GradeListItem(Parcel in) {
        grade = in.readString();
        studentGradeId = in.readString();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<GradeListItem> CREATOR = new Creator<GradeListItem>() {
        @Override
        public GradeListItem createFromParcel(Parcel in) {
            return new GradeListItem(in);
        }

        @Override
        public GradeListItem[] newArray(int size) {
            return new GradeListItem[size];
        }
    };

    @Bindable
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
        notifyPropertyChanged(BR.selected);
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getGrade() {
        return grade;
    }

    public void setStudentGradeId(String studentGradeId) {
        this.studentGradeId = studentGradeId;
    }

    public String getStudentGradeId() {
        return studentGradeId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(grade);
        dest.writeString(studentGradeId);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }
}
package com.app.yearbook.utils;

public class BaseUrl {

    //live url
    private boolean isLocal = false;
    private String url;
    private String hostLive = "https://classof.app";    //Live
    private String hostLocal = "http://192.168.0.3/";    //Local

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHost() {
        if (isLocal)
            return hostLocal;
        else
            return hostLive;
    }

    public BaseUrl() {
        if (isLocal)
            url = hostLocal + "/Yearbook/api/";        //Local
        else
            url = hostLive + "/api/";     //Live
    }
}

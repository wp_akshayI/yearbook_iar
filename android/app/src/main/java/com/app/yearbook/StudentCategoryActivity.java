package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.StudentCategoryAdapter;
import com.app.yearbook.model.yearbook.CategoryList;
import com.app.yearbook.model.yearbook.GetCategory;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentCategoryActivity extends AppCompatActivity {

    private String yearbookId,year;
    private ArrayList<CategoryList> categoryLists;
    private Toolbar toolbar;
    private AllMethods allMethods;
    public ProgressDialog progressDialog;
    private RecyclerView rvCategory;
    private LinearLayout lvNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_category);

        setView();
    }

    public void setView()
    {
        //get intent
        if(getIntent().getExtras()!=null)
        {
            yearbookId=getIntent().getStringExtra("yearBookId");
            year=getIntent().getStringExtra("year");
        }

        //all methods object
        allMethods = new AllMethods();

        //set toolbar
        setToolbar();

        //category
        rvCategory=findViewById(R.id.rvCategory);

        //lv
        lvNoData=findViewById(R.id.lvNoData);

        getCategory();
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(year);
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void getCategory()
    {
        Log.d("TTT","Yeaybook Id: "+yearbookId);
        //Progress bar
        progressDialog = ProgressDialog.show(StudentCategoryActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RequestBody id= RequestBody.create(MediaType.parse("text/plain"), yearbookId);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetCategory> category = retrofitClass.getCategory(id);
        category.enqueue(new Callback<GetCategory>() {
            @Override
            public void onResponse(Call<GetCategory> call, Response<GetCategory> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if(response.body().getResponseCode().equals("1"))
                {
                    if(response.body().getCategoryList().size()>0)
                    {
                        rvCategory.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        categoryLists=new ArrayList<>();
                        categoryLists.addAll(response.body().getCategoryList());

                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(StudentCategoryActivity.this);
                        rvCategory.setLayoutManager(mLayoutManager);

                        StudentCategoryAdapter mAdapter = new StudentCategoryAdapter(categoryLists, StudentCategoryActivity.this,"user");
                        rvCategory.setAdapter(mAdapter);

                    }
                    else
                    {
                        rvCategory.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10")) {
                    Toast.makeText(StudentCategoryActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StudentCategoryActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StudentCategoryActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    rvCategory.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<GetCategory> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StudentCategoryActivity.this, "", t.getMessage() + "");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}

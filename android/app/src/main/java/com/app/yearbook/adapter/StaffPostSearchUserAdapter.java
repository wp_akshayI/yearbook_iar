package com.app.yearbook.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.makeramen.roundedimageview.RoundedImageView;
import com.app.yearbook.R;
import com.app.yearbook.model.search.User;

import java.util.ArrayList;

public class StaffPostSearchUserAdapter extends RecyclerView.Adapter<StaffPostSearchUserAdapter.MyViewHolder> {
    private ArrayList<User> userLists;
    private Activity ctx;
    public ProgressDialog progressDialog;

    @NonNull
    @Override
    public StaffPostSearchUserAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_searchuser, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final StaffPostSearchUserAdapter.MyViewHolder holder, final int position) {
        final User userList = userLists.get(position);
        holder.tvUserType.setText(userList.getUserType() + "");
        holder.tvUserName.setText(userList.getUserFirstname() + "  " + userList.getUserLastname());

        Glide.with(ctx)
                .load(userList.getUserImage())
                .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.profile_placeholder)
                .error(R.mipmap.profile_placeholder)
                .into(holder.imgUserPhoto);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(ctx,"hello",Toast.LENGTH_SHORT).show();
                Intent i = new Intent();
                i.putExtra("SearchUser",userList);
                ctx.setResult(ctx.RESULT_OK, i);
                ctx.finish();
            }
        });

    }

    public StaffPostSearchUserAdapter(ArrayList<User> userLists, Activity context) {
        this.userLists = userLists;
        this.ctx = context;
    }

    @Override
    public int getItemCount() {
        return userLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvUserName, tvUserType;
        public RoundedImageView imgUserPhoto;
        public ImageView imgReport;


        public MyViewHolder(View view) {
            super(view);
            tvUserType = view.findViewById(R.id.tvUserType);
            tvUserName = view.findViewById(R.id.tvUserName);
            imgUserPhoto = view.findViewById(R.id.imgUserImage);
            imgReport=view.findViewById(R.id.imgReport);

        }
    }
}

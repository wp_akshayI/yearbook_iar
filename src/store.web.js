import { createStore, applyMiddleware, compose } from "redux"
import thunk from "redux-thunk";
import rootReducer from "./reducers/index.web"
import { createHashHistory } from "history"
import { routerMiddleware } from "connected-react-router";

// const history = createBrowserHistory({ basename: '/editor/' });
const history = createHashHistory({ hashType: 'slash', getUserConfirmation: (message, callback) => callback(window.confirm(message)) });
const routeMiddleware = routerMiddleware(history);
const middlewares = [thunk, routeMiddleware];
const composedEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function configureStore(initialState) {
    const store = createStore(rootReducer(history), initialState, composedEnhancer( applyMiddleware(...middlewares)));

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('./reducers/index.web', () => {
            const nextRootReducer = require('./reducers/index.web');
            store.replaceReducer(nextRootReducer);
        });
    }
    return store;
}

export { history };

package com.app.yearbook.model.yearbook;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CategoryList implements Serializable{

    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("yearbook_id")
    @Expose
    private String yearbookId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_template_type")
    @Expose
    private String categoryTemplateType;
    @SerializedName("template")
    @Expose
    private Template template;
    @SerializedName("category_created")
    @Expose
    private String categoryCreated;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getYearbookId() {
        return yearbookId;
    }

    public void setYearbookId(String yearbookId) {
        this.yearbookId = yearbookId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryTemplateType() {
        return categoryTemplateType;
    }

    public void setCategoryTemplateType(String categoryTemplateType) {
        this.categoryTemplateType = categoryTemplateType;
    }

    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public String getCategoryCreated() {
        return categoryCreated;
    }

    public void setCategoryCreated(String categoryCreated) {
        this.categoryCreated = categoryCreated;
    }

    public CategoryList(String yearbookId, String categoryName) {
        this.yearbookId = yearbookId;
        this.categoryName = categoryName;
    }
}

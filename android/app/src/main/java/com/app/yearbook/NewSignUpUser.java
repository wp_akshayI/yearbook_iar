package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.app.yearbook.databinding.ActivityNewUserSignupBinding;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.StaffStudent;
import com.app.yearbook.model.UserSignup;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.Constants;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.PlayerManager;

public class NewSignUpUser extends AppCompatActivity {

    ActivityNewUserSignupBinding mBinding;
    RetrofitClass retrofitClass;
    LoginUser loginUserSP;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_new_user_signup);

        setToolbar();

        retrofitClass = APIClient.getClient().create(RetrofitClass.class);

        mBinding.btnRegi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBinding.edtEmail.getText().toString().trim().length() <= 0) {
                    Toast.makeText(NewSignUpUser.this, "Enter email address", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mBinding.edtReemail.getText().toString().trim().length() <= 0) {
                    Toast.makeText(NewSignUpUser.this, "Enter confirm email address", Toast.LENGTH_SHORT).show();
                    return;
                }
                /*if (mBinding.edtReemail.getText().toString().trim().contentEquals(mBinding.edtEmail.getText().toString().trim())){
                    Toast.makeText(NewSignUpUser.this, "Email address mismatch", Toast.LENGTH_SHORT).show();
                    return;
                }*/

                if (mBinding.edtPassword.getText().toString().trim().length() <= 0) {
                    Toast.makeText(NewSignUpUser.this, "Enter password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mBinding.edtRepassword.getText().toString().trim().length() <= 0) {
                    Toast.makeText(NewSignUpUser.this, "Enter confirm password", Toast.LENGTH_SHORT).show();
                    return;
                }
                /*if (mBinding.edtRepassword.getText().toString().trim().contentEquals(mBinding.edtPassword.getText().toString().trim())){
                    Toast.makeText(NewSignUpUser.this, "Password mismatch", Toast.LENGTH_SHORT).show();
                    return;
                }*/

                callApi();
            }
        });

        String token = FirebaseInstanceId.getInstance().getToken();
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("Token", token).apply();

//        SpannableString ss = new SpannableString("By signing up, you agree to our Terms and Privacy Policy.");
        SpannableString ss = new SpannableString("By signing up, you are agreeing to the Terms of Use and Privacy Policy.");
        ClickableSpan clickableTerms = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(NewSignUpUser.this, UserProcessWithWeb.class);
                intent.putExtra("action", Constants.USER_TERMS);
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ClickableSpan clickablePolicy = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(NewSignUpUser.this, UserProcessWithWeb.class);
                intent.putExtra("action", Constants.USER_PRIVACY);
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        ss.setSpan(clickableTerms, 39, 51, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickablePolicy, 56, 70, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        ss.setSpan(new UnderlineSpan(), 39, 51, 0);
        ss.setSpan(new UnderlineSpan(), 56, 70, 0);

        ss.setSpan(new StyleSpan(Typeface.BOLD), 39, 51, 0);
        ss.setSpan(new StyleSpan(Typeface.BOLD),  56, 70, 0);

        mBinding.txtTermsPolicy.setText(ss);
        mBinding.txtTermsPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        mBinding.txtTermsPolicy.setHighlightColor(Color.TRANSPARENT);
    }

    public void setToolbar() {
        mBinding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        /*toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
//        toolbar_title.setText("Post to Yearbook");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    ProgressDialog progressDialog;

    private void callApi() {

        final String token = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("Token", "");

        progressDialog = ProgressDialog.show(NewSignUpUser.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        Call<UserSignup> signUpUser = retrofitClass.newUserSignup(mBinding.edtEmail.getText().toString(),
                mBinding.edtPassword.getText().toString(),
                "android",
                token);
        signUpUser.enqueue(new Callback<UserSignup>() {
            @Override
            public void onResponse(Call<UserSignup> call, Response<UserSignup> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Log.d("TTT", new Gson().toJson(response.body().getData() + ""));
                if (response.body().getResponseCode().equals(String.valueOf(1))) {
                    loginUserSP = new LoginUser(NewSignUpUser.this);
                    loginUserSP.setUserData(new StaffStudent());
                    StaffStudent user = loginUserSP.getUserData();
                    user.setUserId(response.body().getUserId());
                    user.setUserEmail(mBinding.edtEmail.getText().toString().trim());
                    user.setUserPassword(mBinding.edtPassword.getText().toString().trim());
                    user.setUserToken(token);
                    loginUserSP.setUserData(user);

                    startActivity(new Intent(NewSignUpUser.this, AccessCode.class));
                    finish();
                } else {
                    new AllMethods().setAlert(NewSignUpUser.this, getResources().getString(R.string.app_name), response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<UserSignup> call, Throwable t) {
                Log.d("Fail", "onResponse: ");
            }
        });
    }
}

package com.app.yearbook.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.ybq.android.spinkit.style.Circle;
import com.makeramen.roundedimageview.RoundedImageView;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.StudentBookmarkActivity;
import com.app.yearbook.StudentHomeActivity;
import com.app.yearbook.ViewSignatureActivity;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.profile.ChangeProfilePhoto;
import com.app.yearbook.model.profile.getUserProfile;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.studenthome.PostList;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.BitmapUtils;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FirstVersionProfileFragment extends Fragment implements View.OnClickListener {

    private static final int REQUEST_PERMISSION = 101;
    public View view;
    private ImageView imgTagPhotos, imgSetting, imgBookmark, imgAddPhoto;
    private RoundedImageView imgUserImage;
    private LinearLayout lvSign;
    public static RecyclerView rvUserPost;
    private LinearLayout lvNoData;
    public ProgressDialog progressDialog;
    private AllMethods allMethods;
    private LoginUser loginUser;
    private String userId, profile, schoolId, mCurrentPhotoPath, userPwd, studentName;
    public static ArrayList<PostList> getUserList;
    private int GALLERY = 1, CAMERA = 2;
    private Uri resultUri;
    private File fileImage;
    private MultipartBody.Part part;
    //private MenuItem menuPhoto;
    private EditText edtOldPassword, edtNewPassword, edtConfirmPassword;
    private TextInputLayout input_layout_newpassword, input_layout_confirmpassword, input_layout_oldpassword;
    private ProgressBar progressBar;
    private TextView tvName, tvStudentId, tvGrade;

    public FirstVersionProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // ((StudentHomeActivity) getActivity()).getSupportActionBar().setTitle("Profile");
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_firstversion_profile, container, false);
        setHasOptionsMenu(true);
        setView();
        return view;

//        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
//        setView();
//        setHasOptionsMenu(true);
//        return binding.getRoot();
    }

    public void setView() {

        //common methods
        allMethods = new AllMethods();

        //progress bar
        progressBar = view.findViewById(R.id.progress);

        //profile img
        imgUserImage = view.findViewById(R.id.imgUserImage);

        //signature
        lvSign = view.findViewById(R.id.lvSign);
        lvSign.setOnClickListener(this);

        //tag post
        imgTagPhotos = view.findViewById(R.id.imgTagPhotos);
        imgTagPhotos.setOnClickListener(this);

        //notification
        imgSetting = view.findViewById(R.id.imgSetting);
        imgSetting.setOnClickListener(this);

        //bookmark
        imgBookmark = view.findViewById(R.id.imgBookmark);
        imgBookmark.setOnClickListener(this);

        //profile pic dialog
        imgAddPhoto = view.findViewById(R.id.imgAddPhoto);
        imgAddPhoto.setOnClickListener(this);

        //tvname, tngrad,tvId
        tvGrade = view.findViewById(R.id.tvGrade);
        tvName = view.findViewById(R.id.tvName);
        tvStudentId = view.findViewById(R.id.tvStudentId);


        //SP
        loginUser = new LoginUser(getActivity());

        if (loginUser.getUserData() != null) {

            userId = loginUser.getUserData().getUserId();
            profile = loginUser.getUserData().getUserImage();
            schoolId = loginUser.getUserData().getSchoolId();
            userPwd = loginUser.getUserData().getUserPassword();

            studentName = loginUser.getUserData().getUserFirstname() + " " + loginUser.getUserData().getUserLastname();
//            if (!studentName.equals("")) {
//                studentName = AllMethods.getCapsSentences(studentName);
//            }

            tvName.setText(studentName);
            tvGrade.setText("Grade: " + loginUser.getUserData().getUserGrade());
            tvStudentId.setText("Student ID# " + loginUser.getUserData().getUserStudentId());

            Log.d("TTT", "Profileee: " + profile);
            //set profile photo
            if (profile != null && !profile.equals("")) {
                progressBar.setVisibility(View.VISIBLE);
                Glide.with(getActivity())
                        .load(profile)
                        .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .listener(new RequestListener<String, Bitmap>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .centerCrop()
                        .placeholder(R.mipmap.profile_placeholder)
                        .error(R.mipmap.profile_placeholder)
                        .into(imgUserImage);
            }
        }

        //lv no
        lvNoData = view.findViewById(R.id.lvNoData);

        //rv
        rvUserPost = view.findViewById(R.id.rvUserPost);

        getUserPost();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.lvSign) {
            Intent i = new Intent(getActivity(), ViewSignatureActivity.class);
            i.putExtra("Id", userId);
            startActivity(i);
        }
//        else if (v.getId() == R.id.imgTagPhotos) {
//            Intent i = new Intent(getActivity(), SearchUserTagPhotoActivity.class);
//            i.putExtra("schoolId", schoolId);
//            i.putExtra("userId", userId);
//            startActivity(i);
//        }
        else if (v.getId() == R.id.imgSetting) {
            showMenu(v);
//            Intent i = new Intent(getActivity(), NotificationActivity.class);
//            i.putExtra("userId", userId);
//            startActivity(i);
        } else if (v.getId() == R.id.imgBookmark) {
            Intent i = new Intent(getActivity(), StudentBookmarkActivity.class);
            i.putExtra("userId", userId);
            startActivity(i);
        } else if (v.getId() == R.id.imgAddPhoto) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Log.d("TTT", "Permission...");
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
            } else {
                Log.d("TTT", "granted Permission...");
                setDialog();
            }
        }
    }

    public void setDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Available actions");
        builder.setCancelable(false);
        builder.setItems(new CharSequence[]{"Pick from gallery", "Capture picture", "Remove current photo", "Cancel"},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                            case 2:
                                dialog.dismiss();
                                removePhoto();
                                break;
                            case 3:
                                dialog.dismiss();
                                break;
                        }
                    }
                });
        builder.create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    Log.d("TTT", "Permission... onRequestPermissionsResult");
                    setDialog();
                } else {
                    Log.d("TTT", "denyyyy... onRequestPermissionsResult");
                    Toast.makeText(getActivity(), "Deny", Toast.LENGTH_SHORT).show();
                    //code for deny
                }
                break;
        }
    }

    public void removePhoto() {
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<Login> userPost = retrofitClass.removeProfilePhoto(Integer.parseInt(userId));
        userPost.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equalsIgnoreCase("1")) {
                    //set profile photo
                    loginUser.setUserData(response.body().getData());
                    Glide.with(getActivity())
                            .load(R.mipmap.profile_placeholder)
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.profile_placeholder)
                            .error(R.mipmap.profile_placeholder)
                            .into(imgUserImage);
                    allMethods.setAlert(getActivity(), "", "Profile photo remove successfully");

                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(getActivity());
                    loginSP.clearData();

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                } else {
                    allMethods.setAlert(getActivity(), "", response.body().getResponseMsg() + "");
                }

            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });

    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);

    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        Log.d("TTT", "Resulttt: " + requestCode + " / " + UCrop.REQUEST_CROP);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                });
                resultUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getActivity().getContentResolver().query(resultUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                Log.d("TTT", "PicturePath: " + picturePath);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                Log.w("TTT", "Pathhhh:  " + resultUri.getPath() + " / " + thumbnail);
                //imgUserImage.setImageBitmap(thumbnail);

                if (thumbnail != null) {
                    Glide.with(getActivity())
                            .load(resultUri)
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .centerCrop()
                            .listener(new RequestListener<Uri, Bitmap>() {
                                @Override
                                public boolean onException(Exception e, Uri model, Target<Bitmap> target, boolean isFirstResource) {
                                    Log.d("TTT", "OnException: ");
                                    progressBar.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Bitmap resource, Uri model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    Log.d("TTT", "Readyy: ");
                                    progressBar.setVisibility(View.GONE);
                                    showPopup();
                                    return false;
                                }
                            })
                            .placeholder(R.mipmap.profile_placeholder)
                            .error(R.mipmap.profile_placeholder)
                            .into(imgUserImage);


                    fileImage = new File(BitmapUtils.getFilePathFromUri(getActivity(), AllMethods.getImageUri(getActivity(), thumbnail)));
                    RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
                    part = MultipartBody.Part.createFormData("user_image", fileImage.getName(), filePost);
                    // menuPhoto.setVisible(true);
                } else {
                    Toast.makeText(getActivity(), "Select other image", Toast.LENGTH_SHORT).show();
                }

            }

        } else if (requestCode == CAMERA) {
            if (data != null) {

                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                imgUserImage.setImageBitmap(thumbnail);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                File wallpaperDirectory = new File(
                        Environment.getExternalStorageDirectory() + "/yearbook");
                // have the object build the directory structure, if needed.
                if (!wallpaperDirectory.exists()) {
                    wallpaperDirectory.mkdirs();
                }

                try {
                    fileImage = new File(wallpaperDirectory, Calendar.getInstance()
                            .getTimeInMillis() + ".jpg");
                    fileImage.createNewFile();
                    FileOutputStream fo = new FileOutputStream(fileImage);
                    fo.write(bytes.toByteArray());
                    MediaScannerConnection.scanFile(getActivity(),
                            new String[]{fileImage.getPath()},
                            new String[]{"image/jpeg"}, null);
                    fo.close();
                    Log.d("TAG", "File Saved::--->" + fileImage.getAbsolutePath());

                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                fileImage = new File(BitmapUtils.getFilePathFromUri(getActivity(), AllMethods.getImageUri(getActivity(), thumbnail)));
                RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
                part = MultipartBody.Part.createFormData("user_image", fileImage.getName(), filePost);
                //menuPhoto.setVisible(true);
                showPopup();
            }
        } else if (resultCode == getActivity().RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            Log.d("TTT", "Crop img: " + UCrop.getOutput(data));
            resultUri = UCrop.getOutput(data);
            imgUserImage.setImageURI(resultUri);

            fileImage = new File(BitmapUtils.getFilePathFromUri(getActivity(), resultUri));
            RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
            part = MultipartBody.Part.createFormData("user_image", fileImage.getName(), filePost);

        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Toast.makeText(getActivity(), cropError.getMessage() + "", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.logout, menu);
//        menuPhoto = menu.findItem(R.id.photo);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.setting) {
//            View menuItemView = getActivity().findViewById(R.id.setting);
//            showMenu(menuItemView);
//
//        } else if (item.getItemId() == R.id.photo) {
//            AddEditPhoto();
//
//        }
        return super.onOptionsItemSelected(item);
    }

    public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(getActivity(), v);
        popup.getMenuInflater().inflate(R.menu.profileseeting, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Change password")) {
                    androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.custom_alertbox, null);
                    dialogBuilder.setView(dialogView);
                    final androidx.appcompat.app.AlertDialog alert = dialogBuilder.create();
                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    alert.setCancelable(false);

                    edtNewPassword = dialogView.findViewById(R.id.edtNewPassword);
                    edtNewPassword.setOnFocusChangeListener(newPassword);

                    edtOldPassword = dialogView.findViewById(R.id.edtOldPassword);
                    edtOldPassword.setOnFocusChangeListener(oldPassword);

                    edtConfirmPassword = dialogView.findViewById(R.id.edtConfirmPassword);
                    edtConfirmPassword.setOnFocusChangeListener(confirmPassword);

                    input_layout_oldpassword = dialogView.findViewById(R.id.input_layout_oldpassword);
                    input_layout_newpassword = dialogView.findViewById(R.id.input_layout_newpassword);
                    input_layout_confirmpassword = dialogView.findViewById(R.id.input_layout_confirmpassword);

                    Button btnCancel = dialogView.findViewById(R.id.btnCancel);
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    Button btnOk = dialogView.findViewById(R.id.btnOk);
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (validation()) {
                                if (!userPwd.toString().equals(edtOldPassword.getText().toString())) {
                                    Log.d("TTT", "wrongg pwd");
                                    input_layout_oldpassword.setErrorEnabled(true);
                                    input_layout_oldpassword.setError("Your old password is wrong");
                                } else {
                                    alert.dismiss();
                                    changePassword(edtNewPassword.getText().toString());
                                }
                            }
                        }
                    });
                    alert.show();
                } else if (item.getTitle().equals("Logout")) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setCancelable(false);
                    dialog.setTitle("");
                    dialog.setMessage("Are you sure you want to logout ?");
                    dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //Action for "Delete".
                            dialog.dismiss();

                            String token = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Token", "");
                            Log.d("TTT", "token: " + token);

                            if (!token.equals("")) {
                                progressDialog = ProgressDialog.show(getActivity(), "", "", true);
                                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                progressDialog.setContentView(R.layout.progress_view);
                                ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                                Circle bounce = new Circle();
                                bounce.setColor(Color.BLACK);
                                progressBar.setIndeterminateDrawable(bounce);

                                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                final Call<GiveReport> userPost = retrofitClass.logout(token, "user");
                                userPost.enqueue(new Callback<GiveReport>() {
                                    @Override
                                    public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }

                                        LoginUser loginSP = new LoginUser(getActivity());
                                        loginSP.clearData();

                                        Intent i = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(i);
                                        ((StudentHomeActivity) getActivity()).finish();
                                    }

                                    @Override
                                    public void onFailure(Call<GiveReport> call, Throwable t) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }
                                        allMethods.setAlert(getActivity(), "", t.getMessage() + "");
                                    }
                                });
                            } else {
                                LoginUser loginSP = new LoginUser(getActivity());
                                loginSP.clearData();

                                Intent i = new Intent(getActivity(), LoginActivity.class);
                                startActivity(i);
                                getActivity().finish();
                            }
                        }


                    });
                    dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    final AlertDialog alert = dialog.create();
                    alert.show();

                }
                return true;
            }
        });
        popup.show();

    }

    private View.OnFocusChangeListener oldPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtOldPassword.getText().toString().equals("")) {
                    input_layout_oldpassword.setErrorEnabled(true);
                    input_layout_oldpassword.setError("Your old password is empty");
                } else {
                    input_layout_oldpassword.setErrorEnabled(false);
                    input_layout_oldpassword.setError("");
                }
            }

        }
    };


    private View.OnFocusChangeListener newPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtNewPassword.getText().toString().equals("")) {
                    input_layout_newpassword.setErrorEnabled(true);
                    input_layout_newpassword.setError("Your new password is empty");
                } else {
                    input_layout_newpassword.setErrorEnabled(false);
                    input_layout_newpassword.setError("");
                }
            }

        }
    };

    private View.OnFocusChangeListener confirmPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtConfirmPassword.getText().toString().equals("")) {
                    input_layout_confirmpassword.setErrorEnabled(true);
                    input_layout_confirmpassword.setError("Your confirm password is empty");
                } else {
                    input_layout_confirmpassword.setErrorEnabled(false);
                    input_layout_confirmpassword.setError("");
                }
            }

        }
    };

    public boolean validation() {
        boolean result = false;

        //old pwd
        Log.d("TTT", "Old Pwd: " + userPwd + " / " + edtOldPassword.getText().toString());

        if (edtOldPassword.getText().toString().trim().equals("")) {
            input_layout_oldpassword.setErrorEnabled(true);
            input_layout_oldpassword.setError("Your old password is empty");
            result = false;
        } else if (!userPwd.toString().equals(edtOldPassword.getText().toString())) {
            Log.d("TTT", "wrongg pwd");
            input_layout_oldpassword.setErrorEnabled(true);
            input_layout_oldpassword.setError("Your old password is wrong");
            result = false;
        } else {
            input_layout_oldpassword.setErrorEnabled(false);
            input_layout_oldpassword.setError("");
            result = true;
        }

        //new pwd
        if (edtNewPassword.getText().toString().trim().equals("")) {
            input_layout_newpassword.setErrorEnabled(true);
            input_layout_newpassword.setError("Your new password is empty");
            result = false;
        } else {
            input_layout_newpassword.setErrorEnabled(false);
            input_layout_newpassword.setError("");
            result = true;
        }

        //confirm new pwd
        if (edtConfirmPassword.getText().toString().trim().equals("")) {
            input_layout_confirmpassword.setErrorEnabled(true);
            input_layout_confirmpassword.setError("Your confirm password is empty");
            result = false;
        } else if (!edtConfirmPassword.getText().toString().equals(edtNewPassword.getText().toString())) {
            input_layout_confirmpassword.setErrorEnabled(true);
            input_layout_confirmpassword.setError("Confirm password and new password is must be same");
            result = false;
        } else {
            input_layout_confirmpassword.setErrorEnabled(false);
            input_layout_confirmpassword.setError("");
            result = true;

        }

        Log.d("TTT", "Result: " + result);
        return result;

    }

    public void changePassword(String pwd) {
        //Progress bar
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> userPost = retrofitClass.changePassword(Integer.parseInt(userId), "user", pwd);
        userPost.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", response.body().getResponseMsg() + "");
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });
    }

    public void showPopup() {
        androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
        dialogBuilder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog alert = dialogBuilder.create();
        alert.setCancelable(false);
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
        tvMessage.setText("Are you sure you would like to make this change?");

        TextView tvYes = dialogView.findViewById(R.id.tvYes);
        TextView tvNo = dialogView.findViewById(R.id.tvNo);

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                AddEditPhoto();
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                imgUserImage.setImageDrawable(getActivity().getResources().getDrawable(R.mipmap.profile_placeholder));
            }
        });
        alert.show();
    }

    public void AddEditPhoto() {
        //Progress bar
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RequestBody Id = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());

        Log.d("TTT", "retrofittt: " + userId + " / " + fileImage.getName() + " / " + part.toString());

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<ChangeProfilePhoto> userPost = retrofitClass.addEditUserPhoto(Id, rbFile);
        userPost.enqueue(new Callback<ChangeProfilePhoto>() {
            @Override
            public void onResponse(Call<ChangeProfilePhoto> call, Response<ChangeProfilePhoto> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    Log.d("TTT", "Tokennnn: " + response.body().getData().getUserToken());
                    loginUser.setUserData(response.body().getData());
                    Log.d("TTT", "SP: " + loginUser.getUserData().getUserType());
                    allMethods.setAlert(getActivity(), getResources().getString(R.string.app_name), "Profile edit successfully");

                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(getActivity());
                    loginSP.clearData();

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                } else {
                    allMethods.setAlert(getActivity(), "", response.body().getResponseMsg() + "");
                }

            }

            @Override
            public void onFailure(Call<ChangeProfilePhoto> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });
    }

    public void getUserPost() {
        //Progress bar
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        Log.d("TTT", "Search user tag photo: " + userId + " / " + schoolId);
        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<getUserProfile> userPost = retrofitClass.getUserTag(Integer.parseInt(userId), Integer.parseInt(schoolId));
        userPost.enqueue(new Callback<getUserProfile>() {
            @Override
            public void onResponse(Call<getUserProfile> call, Response<getUserProfile> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                getUserList = new ArrayList<>();
                if (response.body().getResponseCode().equals("1")) {
                    if (response.body().getPostList().size() > 0) {
                        rvUserPost.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        getUserList.addAll(response.body().getPostList());
                        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
                        rvUserPost.setLayoutManager(mLayoutManager);
//                        UserTagPostAdapter mAdapter = new UserTagPostAdapter(getUserList, getActivity());
//                        rvUserPost.setAdapter(mAdapter);

                    } else {
                        rvUserPost.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(getActivity());
                    loginSP.clearData();

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                } else {
                    rvUserPost.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(Call<getUserProfile> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });
    }

}

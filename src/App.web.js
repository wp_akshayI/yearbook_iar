import React, { Component } from "react";
import { Redirect, Route, Switch, HashRouter } from "react-router-dom";
import { connect } from "react-redux";
import Actions from "./actions";

import MainApp from './web/index';
import PageNotFound from "./web/extraPages/404";
import SettingsProvider from "./utils/SettingsProvider";
import Loading from "./web/extraPages/Loading";
import { ThemeProvider } from "@material-ui/styles";
import { createMuiTheme } from "@material-ui/core";

const { setInitUrl } = Actions;

const theme = createMuiTheme({
  palette: {
    primary: { main: "#000", light: "#fff" },
    secondary: { main: "#808080" },
  },
});

const RestrictedRoute = ({ component: Component, authUser, search, ...rest }) =>
  <Route
    {...rest}
    render={props =>
      authUser
        ? <Component {...props} />
        : <Redirect
          to={{
            pathname: "/editor-starting",
            state: { redirect: props.location },
            search
          }}
        />}
  />;

class App extends Component {

  render() {
    const { match, authUser, location } = this.props;
    if (location.pathname === '/')
      return (<Redirect
        to={{
          pathname: "/app/dashboard",
          state: location.state,
          search: location.search
        }}
      />);
    return (
      <ThemeProvider theme={theme}>
        <SettingsProvider>
          <Switch>
            <Route path="/editor-starting" component={Loading} />
            <RestrictedRoute path={`${match.url}app`} authUser={authUser}
              component={MainApp} search={location.search} />
            <Route component={PageNotFound} />
          </Switch>
        </SettingsProvider>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = ({ auth, router }) => {
  const { authUser, initURL } = auth;
  return { initURL, authUser, query: router.location.query }
};

export default connect(mapStateToProps, { setInitUrl })(App);
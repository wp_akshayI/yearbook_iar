package com.app.yearbook.model.studenthome.profile;

public class StudentProfile {

    String path;
    boolean isChecked;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public StudentProfile() {
    }

    public StudentProfile(String path, boolean isChecked) {
        this.path = path;
        this.isChecked = isChecked;
    }
}

package com.app.yearbook.adapter;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.app.yearbook.R;
import com.app.yearbook.databinding.AdapterGradeSelectionBinding;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.GradeListItem;

import java.util.List;

public class AdapterGrades extends RecyclerView.Adapter<AdapterGrades.GradeHolder> {

    List<GradeListItem> gradeListItems;
    OnRecyclerClick onRecyclerClick;
    boolean isEdite;

    public AdapterGrades(List<GradeListItem> gradeListItems, OnRecyclerClick onRecyclerClick) {
        this.gradeListItems = gradeListItems;
        this.onRecyclerClick = onRecyclerClick;
    }

    public void setIsEdit(){
        isEdite = true;
    }

    @NonNull
    @Override
    public GradeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterGradeSelectionBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_grade_selection, parent, false);
        return new GradeHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull GradeHolder holder, int position) {
        holder.binding.setModel(gradeListItems.get(position));
        holder.binding.executePendingBindings();

        if(position==0)
        {
            holder.binding.rbGrade.setText(holder.binding.getModel().getGrade()+" Grades");
        }
        else {
            holder.binding.rbGrade.setText(holder.binding.getModel().getGrade()+" Grade");
        }
        if (isEdite){
            holder.binding.rbGrade.setEnabled(false);
        }
    }

    @Override
    public int getItemCount() {
        return gradeListItems.size();
    }

    public class GradeHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {
        AdapterGradeSelectionBinding binding;

        public GradeHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            binding.rbGrade.setOnCheckedChangeListener(this);
        }


        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            onRecyclerClick.onClick(getAdapterPosition(), 0);
        }
    }
}

package com.app.yearbook.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.fenchtose.nocropper.BitmapResult;
import com.fenchtose.nocropper.CropInfo;
import com.fenchtose.nocropper.CropResult;
import com.fenchtose.nocropper.CropState;
import com.fenchtose.nocropper.CropperView;
import com.fenchtose.nocropper.ScaledCropper;
import com.app.yearbook.ImageFilterActivity;
import com.app.yearbook.R;
import com.app.yearbook.TrimmerActivity;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.BitmapUtils;
import com.app.yearbook.videoTrimmer.interfaces.OnProgressVideoListener;

import java.io.File;
import java.io.IOException;
import java.util.List;

import gun0912.tedbottompicker.TedBottomPicker;
import tcking.github.com.giraffeplayer2.PlayerManager;
import tcking.github.com.giraffeplayer2.VideoView;

public class LibraryFragment extends Fragment implements View.OnClickListener {

    public View view;
    private static final int REQUEST_PERMISSION = 101;
    private TextView tvPhoto, tvVideo;
    private CropperView imgSelect;
    private VideoView videoSelected;
    private android.widget.VideoView video_loader;
    private TedBottomPicker tedBottomPicker;
    private MenuItem next;
    private String postTag;
    private ImageView imgSnap, imgDummy, icon_video_play;
    private boolean isSnappedToCenter = false;
    private FrameLayout frameLayoutMain;
    private Uri videoUri;
    private RequestManager requestManager;
    private SeekBar mHolderTopView;

    private RelativeLayout layout_surface_view;
    private int mDuration = 0;
    private int mTimeVideo = 0;
    private int mStartPosition = 0;
    private int mEndPosition = 0;
    private boolean mResetSeekBar = true;
    private List<OnProgressVideoListener> mListeners;

    public LibraryFragment() {
        // Required empty public constructor
        //https://github.com/jayrambhia/CropperNoCropper
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // ((StudentHomeActivity) getActivity()).getSupportActionBar().setTitle("Library");

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_library, container, false);
        setHasOptionsMenu(true);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Log.d("TTT", "Permission...");
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
        } else {
            Log.d("TTT", "granted Permission...");
            setView();
        }

        return view;
    }

    public void setView() {

        requestManager = Glide.with(this);
        layout_surface_view = view.findViewById(R.id.layout_surface_view);

        //dummy img
        imgDummy = view.findViewById(R.id.imgDummy);

        tvPhoto = view.findViewById(R.id.tvPhoto);
        tvPhoto.setOnClickListener(this);
        tvPhoto.setTextColor(getResources().getColor(R.color.black));

        tvVideo = view.findViewById(R.id.tvVideo);
        tvVideo.setOnClickListener(this);

        //frameLayoutMain
        frameLayoutMain = view.findViewById(R.id.frameLayoutMain);

        //resize
        imgSnap = view.findViewById(R.id.imgSnap);
        imgSnap.setOnClickListener(this);

        //insta croper
        // instacropper=view.findViewById(R.id.instacropper);

        //image select
        imgSelect = view.findViewById(R.id.imgSelected);
        imgSelect.setMakeSquare(false);
        imgSelect.setGestureEnabled(true);
        imgSelect.setDebug(true);
        imgSelect.setGridCallback(new CropperView.GridCallback() {
            @Override
            public boolean onGestureStarted() {
                return true;
            }

            @Override
            public boolean onGestureCompleted() {
                return false;
            }
        });

        //video view
        videoSelected = view.findViewById(R.id.videoSelected);
        video_loader = view.findViewById(R.id.video_loader);

        icon_video_play = view.findViewById(R.id.icon_video_play);

        mHolderTopView = view.findViewById(R.id.handlerTop);

        //default open img view
//        tedBottomPicker = new TedBottomPicker.Builder(getActivity())
//                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
//                    @Override
//                    public void onImageSelected(Uri uri) {
//                        // here is selected uri
//                        String extension = uri.toString().substring(uri.toString().lastIndexOf("."));
//                        if (extension.equalsIgnoreCase(".mp4")) {
//                            Toast.makeText(getActivity(), "Please, select other image", Toast.LENGTH_SHORT).show();
//                        } else {
//                            postTag = "Image";
//                            next.setVisible(true);
//                            imgDummy.setVisibility(View.GONE);
//                            frameLayoutMain.setVisibility(View.VISIBLE);
//                            videoSelected.setVisibility(View.GONE);
//                            layout_surface_view.setVisibility(View.GONE);
//
//                            Bitmap mBitmap = AllMethods.GetBitmap(uri, getActivity());
//                            loadNewImage(mBitmap, uri);
//                        }
//
//                    }
//                })
//                .setTitle("Select Image")
//                .create();
//        tedBottomPicker.show(getChildFragmentManager());

//        mListeners = new ArrayList<>();
//        mListeners.add(new OnProgressVideoListener() {
//            @Override
//            public void updateProgress(int time, int max, float scale) {
//                updateVideoProgress(time);
//            }
//        });
        //mListeners.add(mVideoProgressIndicator);

        final GestureDetector gestureDetector = new
                GestureDetector(getContext(),
                new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapConfirmed(MotionEvent e) {
                        onClickVideoPlayPause();
                        return true;
                    }
                }
        );

        video_loader.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, @NonNull MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

//        mHolderTopView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            @Override
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                onPlayerIndicatorSeekChanged(progress, fromUser);
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekBar seekBar) {
//                onPlayerIndicatorSeekStart();
//            }
//
//            @Override
//            public void onStopTrackingTouch(SeekBar seekBar) {
//                onPlayerIndicatorSeekStop(seekBar);
//            }
//        });

        // mHolderTopView.setOnSeekBarChangeListener(change);

        video_loader.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                onVideoPrepared(mp);
            }
        });

        video_loader.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                onVideoCompleted();
            }
        });
    }

    private void onVideoCompleted() {
        video_loader.seekTo(mStartPosition);
    }

    private void setUpMargins() {
        // int marge = mRangeSeekBarView.getThumbs().get(0).getWidthBitmap();
        int widthSeek = mHolderTopView.getThumb().getMinimumWidth() / 2;

//        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mHolderTopView.getLayoutParams();
//        lp.setMargins(marge - widthSeek, 0, marge - widthSeek, 0);
//        mHolderTopView.setLayoutParams(lp);

//        lp = (RelativeLayout.LayoutParams) mTimeLineView.getLayoutParams();
//        lp.setMargins(marge, 0, marge, 0);
//        mTimeLineView.setLayoutParams(lp);
//
//        lp = (RelativeLayout.LayoutParams) mVideoProgressIndicator.getLayoutParams();
//        lp.setMargins(marge, 0, marge, 0);
//        mVideoProgressIndicator.setLayoutParams(lp);
    }

    private void onVideoPrepared(@NonNull MediaPlayer mp) {
        // Adjust the size of the video
        // so it fits on the screen
        int videoWidth = mp.getVideoWidth();
        int videoHeight = mp.getVideoHeight();
        float videoProportion = (float) videoWidth / (float) videoHeight;
        int screenWidth = layout_surface_view.getWidth();
        int screenHeight = layout_surface_view.getHeight();
        float screenProportion = (float) screenWidth / (float) screenHeight;
        ViewGroup.LayoutParams lp = video_loader.getLayoutParams();

        if (videoProportion > screenProportion) {
            lp.width = screenWidth;
            lp.height = (int) ((float) screenWidth / videoProportion);
        } else {
            lp.width = (int) (videoProportion * (float) screenHeight);
            lp.height = screenHeight;
        }
        video_loader.setLayoutParams(lp);

        icon_video_play.setVisibility(View.VISIBLE);
        mDuration = video_loader.getDuration();
        video_loader.seekTo(mStartPosition);
        // setSeekBarPosition();

//        setTimeFrames();
//        setTimeVideo(0);
//
//        if (mOnHgLVideoListener != null) {
//            mOnHgLVideoListener.onVideoPrepared();
//        }
    }

    private void updateVideoProgress(int time) {
        if (video_loader == null) {
            return;
        }

        if (time >= mEndPosition) {
            //mMessageHandler.removeMessages(SHOW_PROGRESS);
            video_loader.pause();
            icon_video_play.setVisibility(View.VISIBLE);
            mResetSeekBar = true;
            return;
        }

        if (mHolderTopView != null) {
            // use long to avoid overflow
            setProgressBarPosition(time);
        }
        //setTimeVideo(time);
    }

    private void setProgressBarPosition(int position) {
        if (mDuration > 0) {
            long pos = 1000L * position / mDuration;
            mHolderTopView.setProgress((int) pos);
        }
    }

    private void onPlayerIndicatorSeekChanged(int progress, boolean fromUser) {

        int duration = (int) ((mDuration * progress) / 1000L);

        if (fromUser) {
            if (duration < mStartPosition) {
                setProgressBarPosition(mStartPosition);
                duration = mStartPosition;
            } else if (duration > mEndPosition) {
                setProgressBarPosition(mEndPosition);
                duration = mEndPosition;
            }
            // setTimeVideo(duration);
        }
    }

    private void onPlayerIndicatorSeekStart() {
        // mMessageHandler.removeMessages(SHOW_PROGRESS);
        video_loader.pause();
        icon_video_play.setVisibility(View.VISIBLE);
        notifyProgressUpdate(false);
    }

    private void onPlayerIndicatorSeekStop(@NonNull SeekBar seekBar) {
        // mMessageHandler.removeMessages(SHOW_PROGRESS);
        video_loader.pause();
        icon_video_play.setVisibility(View.VISIBLE);

        int duration = (int) ((mDuration * seekBar.getProgress()) / 1000L);
        video_loader.seekTo(duration);
        //setTimeVideo(duration);
        notifyProgressUpdate(false);
    }

    private Bitmap mBitmap;

    private void loadNewImage(String filePath, Uri selectedImage) {
        mBitmap = BitmapFactory.decodeFile(filePath);
        originalBitmap = mBitmap;
        Log.d("TTT", "loadNewImage: " + mBitmap);
        int maxP = Math.max(mBitmap.getWidth(), mBitmap.getHeight());
        float scale1280 = (float) maxP / 1280;

        Log.i("TTT", "scaled: " + scale1280 + " - " + (1 / scale1280));
        if (imgSelect.getWidth() != 0) {
            imgSelect.setMaxZoom(imgSelect.getWidth() * 2 / 1280f);
        } else {

            ViewTreeObserver vto = imgSelect.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    imgSelect.getViewTreeObserver().removeOnPreDrawListener(this);
                    imgSelect.setMaxZoom(imgSelect.getWidth() * 2 / 1280f);
                    return true;
                }
            });
        }

        mBitmap = Bitmap.createScaledBitmap(mBitmap, (int) (mBitmap.getWidth() / scale1280),
                (int) (mBitmap.getHeight() / scale1280), true);

        //-------------------------------------

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(selectedImage.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        Log.d("TTT", "Orientation: " + orientation);

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 90);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 180);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 270);
        }

        //-------------------------------------

        Log.d("TTT", "loadNewImage: bitmap w/h: " + mBitmap.getHeight() / mBitmap.getWidth());
        imgSelect.setImageBitmap(mBitmap);

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        Log.d("TTT", "loadNewImage: density: " + metrics.densityDpi);
        int minZoom = Math.min(mBitmap.getWidth(), mBitmap.getHeight());
        int maxZoom = Math.max(mBitmap.getWidth(), mBitmap.getHeight());

        float ratio = (float) mBitmap.getHeight() / (float) mBitmap.getWidth();
        Log.d("TTT", "loadNewImage: diff: " + (mBitmap.getHeight() - mBitmap.getWidth()) + " / " + ratio);
        if (mBitmap.getHeight() > mBitmap.getWidth()) {
            if (ratio > 1.5) {
                if (metrics.densityDpi > 320) {
                    imgSelect.setMinZoom(1.1f);
                } else {
                    imgSelect.setMinZoom(0.75f);
                }
            } else {
                imgSelect.setMinZoom(0f);
            }
        } else {
            imgSelect.setMinZoom(0f);
        }
        Log.d("TTT", "loadNewImage: width height : " + mBitmap.getWidth() + " / " + mBitmap.getHeight());
        Log.d("TTT", "loadNewImage: " + imgSelect.getMinZoom() + " / " + imgSelect.getMaxZoom() + " / " + maxZoom + " / " + minZoom);

    }

    public void setVideoURI(final Uri videoURI) {
        video_loader.setVideoURI(videoURI);
        video_loader.pause();
    }

    private void notifyProgressUpdate(boolean all) {
        if (mDuration == 0) return;

        int position = video_loader.getCurrentPosition();
        if (all) {
            for (OnProgressVideoListener item : mListeners) {
                item.updateProgress(position, mDuration, ((position * 100) / mDuration));
            }
        } else {
            mListeners.get(1).updateProgress(position, mDuration, ((position * 100) / mDuration));
        }
    }

    private SeekBar.OnSeekBarChangeListener change = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            int progress = seekBar.getProgress();
            if (video_loader != null && video_loader.isPlaying()) {
                video_loader.seekTo(progress);

            }
        }
    };

    private boolean isPlaying;

    protected void play(int msec) {
        Log.i("TTT", " Access to video files.");
        // String path = videoUri.getText().toString().trim();
        File file = new File(videoUri.toString());
        if (!file.exists()) {
            Toast.makeText(getActivity(), "Video file path is wrong", Toast.LENGTH_SHORT).show();
            return;
        }

        Log.i("TTT", "The specified video source path");
        video_loader.setVideoPath(file.getAbsolutePath());
        Log.i("TTT", "Start playing");
        video_loader.start();

        // Play according to the initial position
        video_loader.seekTo(msec);
        // Maximum play time length setting maximum progress bar for streaming video
        mHolderTopView.setMax(video_loader.getDuration());

        // To update the progress bar thread, scale
        new Thread() {

            @Override
            public void run() {
                try {
                    isPlaying = true;
                    while (isPlaying) {
                        // If you are playing, no 0.5. milliseconds to update a progress bar
                        int current = video_loader.getCurrentPosition();
                        mHolderTopView.setProgress(current);

                        sleep(500);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
        // After playing a set the playback button is not available
        //btn_play.setEnabled(false);

        video_loader.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // Be back in play.
                //btn_play.setEnabled(true);
            }
        });

        video_loader.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // Error resume play
                play(0);
                isPlaying = false;
                return false;
            }
        });
    }

    private int mMaxDuration = 60000;

    private void setSeekBarPosition() {

        if (mDuration >= mMaxDuration) {
            mStartPosition = mDuration / 2 - mMaxDuration / 2;
            mEndPosition = mDuration / 2 + mMaxDuration / 2;

            // mRangeSeekBarView.setThumbValue(0, (mStartPosition * 100) / mDuration);
            // mRangeSeekBarView.setThumbValue(1, (mEndPosition * 100) / mDuration);

        } else {
            mStartPosition = 0;
            mEndPosition = mDuration;
        }

        setProgressBarPosition(mStartPosition);
        video_loader.seekTo(mStartPosition);

        // mTimeVideo = mDuration;
        // mRangeSeekBarView.initMaxWidth();
    }


    private void onClickVideoPlayPause() {
        if (video_loader.isPlaying()) {
            icon_video_play.setVisibility(View.VISIBLE);
            // mMessageHandler.removeMessages(SHOW_PROGRESS);
            video_loader.pause();
        } else {
            icon_video_play.setVisibility(View.GONE);
            // mMessageHandler.sendEmptyMessage(SHOW_PROGRESS);

            if (mResetSeekBar) {
                mResetSeekBar = false;
                video_loader.seekTo(mStartPosition);
            }

            video_loader.start();
            //play(0);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    Log.d("TTT", "Permission... onRequestPermissionsResult");
                    setView();
                } else {
                    Log.d("TTT", "denyyyy... onRequestPermissionsResult");
                    Toast.makeText(getActivity(), "Deny", Toast.LENGTH_SHORT).show();
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d("TTT", "LF pause");
        if (PlayerManager.getInstance().getCurrentPlayer() != null) {
            Log.d("TTT", "LF pause PlayerManager");
            PlayerManager.getInstance().getCurrentPlayer().stop();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvPhoto) {
            if (PlayerManager.getInstance().getCurrentPlayer() != null) {
                PlayerManager.getInstance().getCurrentPlayer().stop();
            }
            tvPhoto.setTextColor(getResources().getColor(R.color.black));
            tvVideo.setTextColor(getResources().getColor(R.color.deselectTab));

            //img view
            tedBottomPicker = new TedBottomPicker.Builder(getActivity())
                    .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                        @Override
                        public void onImageSelected(Uri uri) {
                            // here is selected uri
                            Log.d("TTT", "Uri: " + uri);
                            String extension = uri.toString().substring(uri.toString().lastIndexOf("."));
                            if (extension.equalsIgnoreCase(".mp4")) {
                                Toast.makeText(getActivity(), "Please select other image", Toast.LENGTH_SHORT).show();
                            } else {
                                isSnappedToCenter = false;
                                postTag = "Image";
                                next.setVisible(true);
                                imgDummy.setVisibility(View.GONE);
                                frameLayoutMain.setVisibility(View.VISIBLE);
                                videoSelected.setVisibility(View.GONE);
                                layout_surface_view.setVisibility(View.GONE);

                                Bitmap bitmap = AllMethods.GetBitmap(uri, getActivity());
                                loadNewImage(uri.getPath(), uri);

//                                String absPath=BitmapUtils.getFilePathFromUri(getActivity(), uri);
//
//                                Bitmap bitmap = AllMethods.GetBitmap(Uri.parse(absPath), getActivity());
//                                loadNewImage(bitmap, uri);
                                //instacropper.setImageUri(uri);

                            }

                        }
                    }).setTitle("Select Image").create();

            tedBottomPicker.show(getChildFragmentManager());
        } else if (v.getId() == R.id.tvVideo) {

            if (video_loader.isPlaying()) {
                icon_video_play.setVisibility(View.VISIBLE);
                video_loader.pause();
            }

            tvPhoto.setTextColor(getResources().getColor(R.color.deselectTab));
            tvVideo.setTextColor(getResources().getColor(R.color.black));

            tedBottomPicker = new TedBottomPicker.Builder(getActivity())
                    .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                        @Override
                        public void onImageSelected(Uri uri) {
                            // here is selected uri
                            //view view
                            videoUri = uri;
                            postTag = "Video";
                            next.setVisible(true);
                            Log.d("TTT", "Uri: " + uri);
                            imgDummy.setVisibility(View.GONE);
                            frameLayoutMain.setVisibility(View.GONE);
                            //videoSelected.setVisibility(View.VISIBLE);
                            layout_surface_view.setVisibility(View.VISIBLE);


//                            videoSelected.reset();
//                            videoSelected.setSource(uri);
//                            Log.d("TTT", "Size: " + videoSelected.getDuration());

//                            requestManager
//                                    .load(uri)
//                                    .into(videoSelected.getPlayer().start());

//                            videoSelected.getVideoInfo().setPortraitWhenFullScreen(false);
//                            videoSelected.setVideoPath(uri.toString());
//                            videoSelected.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player
//                            videoSelected.getPlayer().start();

//                            vd.setVideoURI(uri);
//                            vd.start();

//                            GiraffePlayer player=videoSelected.getPlayer();
//                            if (player.isPlaying()) {
//                                player.pause();
//                            } else {
//                                player.start();
//                            }

//                            if(videoSelected.getPlayer().isPlaying())
//                            {
//                                videoSelected.getPlayer().pause();
//                            }

                            setVideoURI(uri);
                            // video_loader.setVideoURI(uri);
                            //video_loader.start();


                        }
                    })
                    .setSelectedUri(videoUri)
                    .setTitle("Select Video")
                    .showVideoMedia()
                    .create();

            tedBottomPicker.show(getChildFragmentManager());

        } else if (v.getId() == R.id.imgSnap) {
            if (isSnappedToCenter) {
                imgSelect.cropToCenter();
            } else {
                imgSelect.fitToCenter();
            }
            isSnappedToCenter = !isSnappedToCenter;

            // instacropper.setRatios(DEFAULT_RATIO, DEFAULT_MINIMUM_RATIO, DEFAULT_MAXIMUM_RATIO);

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.selectimage, menu);
        next = menu.findItem(R.id.next);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private int rotationCount = 0;
    private Bitmap originalBitmap;

    private ScaledCropper prepareCropForOriginalImage() {

        CropResult result = imgSelect.getCropInfo();
        if (result.getCropInfo() == null) {
            return null;
        }

        float scale;
        if (rotationCount % 2 == 0) {
            // same width and height
            scale = (float) originalBitmap.getWidth() / mBitmap.getWidth();
        } else {
            // width and height are interchanged
            scale = (float) originalBitmap.getWidth() / mBitmap.getHeight();
        }

        CropInfo cropInfo = result.getCropInfo().rotate90XTimes(mBitmap.getWidth(), mBitmap.getHeight(), rotationCount);
        return new ScaledCropper(cropInfo, originalBitmap, scale);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.next:
                if (postTag.equalsIgnoreCase("Image")) {

                    //    ScaledCropper result = prepareCropForOriginalImage();
//                    result.crop(new CropperCallback() {
//                        @Override
//                        public void onCropped(Bitmap bitmap) {
//                            Log.d("TTT", "onCropped: bitmap: "+bitmap);
//                            if (bitmap != null) {
//                                try {
//                                    BitmapUtils.writeBitmapToFile(bitmap, new File(Environment.getExternalStorageDirectory() + "/crop_test_info_orig.jpg"), 90);
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }
//                    });

//                    Bitmap bitmap = result.cropBitmap();
//                    if(result!=null)
//                    {
//                        try {
//                            File f = new File(Environment.getExternalStorageDirectory()
//                                    + File.separator + "crop_test_1.jpg");
//                            BitmapUtils.writeBitmapToFile(bitmap, f, 100);
//
//                            Log.d("TTT", "onOptionsItemSelected: "+Uri.fromFile(f));
//                            Intent i = new Intent(getActivity(), ImageFilterActivity.class);
//                            i.putExtra("from", "library");
//                            i.putExtra("selectUri",  Uri.fromFile(f).toString());
//                            getActivity().startActivityForResult(i, 111);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }


                    BitmapResult result = imgSelect.getCroppedBitmap();
                    if (result.getState() == CropState.FAILURE_GESTURE_IN_PROCESS) {
                        Toast.makeText(getActivity(), "unable to crop. Gesture in progress", Toast.LENGTH_SHORT).show();
                    } else {
                        Bitmap bitmap = result.getBitmap();
                        Log.d("TTT", "loadNewImage:bitmap height width: " + bitmap.getWidth() + " / " + bitmap.getHeight());
//                        if (bitmap != null) {
//                            Log.d("Cropper", "crop1 bitmap: " + bitmap.getWidth() + ", " + bitmap.getHeight());
//                            try {
//                                BitmapUtils.writeBitmapToFile(bitmap, new File(Environment.getExternalStorageDirectory() + "/crop_test.jpg"), 90);
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        }

                        try {
                            File f = new File(Environment.getExternalStorageDirectory()
                                    + File.separator + "crop_test.jpg");
                            BitmapUtils.writeBitmapToFile(bitmap, f, 100);

                            Log.d("TTT", "onOptionsItemSelected: " + Uri.fromFile(f));
                            Intent i = new Intent(getActivity(), ImageFilterActivity.class);
                            i.putExtra("from", "library");
                            i.putExtra("selectUri", Uri.fromFile(f).toString());
                            getActivity().startActivityForResult(i, 111);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        //Uri getUri = AllMethods.getImageUri(getActivity(), bitmap);
                    }


                } else {
                    // Intent i = new Intent(getActivity(), AddPostActivity.class);
                    Intent i = new Intent(getActivity(), TrimmerActivity.class);
                    i.putExtra("from", "library");
                    i.putExtra("Type", "Video");
                    i.putExtra("selectUri", videoUri.toString());
                    getActivity().startActivityForResult(i, 111);
                }
                break;
        }
        return true;
    }

}

package com.app.yearbook.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.yearbook.R;
import com.app.yearbook.StoryPhotoActivity;
import com.app.yearbook.model.staff.livefeed.StaffListItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import tcking.github.com.giraffeplayer2.GiraffePlayer;
import tcking.github.com.giraffeplayer2.Option;
import tcking.github.com.giraffeplayer2.VideoInfo;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

public class StaffUserListAdapter extends RecyclerView.Adapter<StaffUserListAdapter.MyViewHolder> {
    private ArrayList<StaffListItem> userLists;
    private Activity ctx;
    private String type;

    @NonNull
    @Override
    public StaffUserListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_studentposts_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final StaffUserListAdapter.MyViewHolder holder, final int position) {
        final StaffListItem userList = userLists.get(position);
        holder.tvUserId.setText(userList.getStaffId() + "");
        holder.tvUserName.setText(userList.getStaffLastname());

        Glide.with(ctx)
                .load(userList.getPostImage())
                .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.profile_placeholder)
                .error(R.mipmap.profile_placeholder)
                .into(holder.imgUserPhoto);

        holder.imgUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String uri = userList.getPostFile();
                String extension = uri.substring(uri.lastIndexOf("."));
                Log.d("TTT", "Url type: " + extension);

                if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {

                    Intent i = new Intent(ctx, StoryPhotoActivity.class);
                    i.putExtra("FileUrl", userList.getPostFile());
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(ctx,
                                    holder.imgUserPhoto,
                                    ViewCompat.getTransitionName(holder.imgUserPhoto));
                    ctx.startActivity(i, options.toBundle());
                }
                else
                {
                    VideoInfo videoInfo = new VideoInfo(Uri.parse(userList.getPostFile()))
                            .setTitle(userList.getStaffFirstname())
                            .setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT)
                            .addOption(Option.create(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "infbuf", 1L))
                            .addOption(Option.create(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "multiple_requests", 1L))
                            .setShowTopBar(true);

                    GiraffePlayer.play(ctx, videoInfo);
                    ctx.overridePendingTransition(0,0);

                    //getActivity().overridePendingTransition(0, 0);
//                    Intent i = new Intent(ctx, FullscreenActivity.class);
//                    i.putExtra("FileUrl", userList.getPostFile());
//                    ctx.startActivity(i);
                }
            }
        });

//        holder.imgUserPhoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(ctx, UserDetailActivity.class);
//                i.putExtra("Photo",userList.getUserImage());
//                i.putExtra("type",type);
//                i.putExtra("Id", userList.getUserId());
//                i.putExtra("schoolId", userList.getSchoolId());
//                i.putExtra("Name", userList.getUserFirstname() + " " + userList.getUserLastname());
//                ctx.startActivity(i);
//            }
//        });
    }

    public StaffUserListAdapter(ArrayList<StaffListItem> userLists, Activity context, String type) {
        this.userLists = userLists;
        this.ctx = context;
        this.type = type;
    }

    @Override
    public int getItemCount() {
        return userLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvUserId, tvUserName;
        public RoundedImageView imgUserPhoto;

        public MyViewHolder(View view) {
            super(view);
            tvUserId = view.findViewById(R.id.tvUserId);
            tvUserName = view.findViewById(R.id.tvUserName);
            imgUserPhoto = view.findViewById(R.id.imgUserImage);

        }
    }
}

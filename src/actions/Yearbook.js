import { action } from '../utils'
import { postResponse, failerResponse } from '../utils/funtions'
import { push } from 'connected-react-router'
import { platform } from '../utils/const'

export function get_yearbook_list () {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    const request = {
      Token: authUser
    }
    const res = await postResponse('get_yearbook_list.php', request)
    if (res.Result) {
      dispatch({
        type: action.yearbook_list,
        payload: res.Data
      })
      return res.Data
    }
    failerResponse('get_yearbook_list', res)
  }
}

export function set_yearbook (id) {
  return async (dispatch) => {
    dispatch({
      type: action.set_yearbook,
      payload: id
    })
  }
}

export function get_yearbook_detail () {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    const { yearbookId } = getState().yearbook
    if (!yearbookId || yearbookId === null) return dispatch(push('/app/yearbooks'))
    const request = {
      Token: authUser,
      yearbook_id: yearbookId
    }
    const res = await postResponse('get_yearbook_detail.php', request)
    if (res.Result) {
      dispatch({
        type: action.get_yearbook_details,
        yearbookDetails: res.Data
      })
      localStorage.setItem(action.set_yearbook, JSON.stringify(res.Data))
      return res.Data
    }
    failerResponse('get_yearbook_detail', res)
  }
}

export function check_yearbook_publish () {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    try {
      const request = {
        Token: authUser,
        platform
      }
      const res = await postResponse('check_yearbook_publish.php', request)
      return res
    } catch (error) {
      failerResponse('check_yearbook_publish', error)
    }
  }
}

package com.app.yearbook.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.SignatureDetailActivity;
import com.app.yearbook.adapter.StudentSignatureListAdapter;
import com.app.yearbook.databinding.FragmentStudentSignatureBinding;
import com.app.yearbook.model.SignatureYearsListModel;
import com.app.yearbook.model.profile.SignatureList;
import com.app.yearbook.model.profile.getSignature;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.OnRecyclerClick;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentSignatureFragment extends Fragment {
    FragmentStudentSignatureBinding binding;
    private ArrayList<SignatureList> signatureList;
    StudentSignatureListAdapter studentSignatureListAdapter;
    private AllMethods allMethods;
    boolean isProgress = false;
    int choosenYear = 0;
    SignatureYearsListModel res;

    public StudentSignatureFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_student_signature, container, false);

        return binding.getRoot();
        // return inflater.inflate(R.layout.fragment_staff_notification, container, false);
    }

    Comparator<String> compareByDegree = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
//            return Double.valueOf(String.format("%.6f", Double.parseDouble(o1.getDistance()))).compareTo(Double.valueOf(String.format("%.6f", Double.parseDouble(o2.getDistance()))));
            return o1.compareTo(o2);
        }
    };

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //all methods object
        allMethods = new AllMethods();

        binding.swipyrefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                binding.swipyrefreshlayout.setRefreshing(true);
                isProgress = true;
                getSignature();
            }
        });

        choosenYear = Calendar.getInstance().get(Calendar.YEAR);
        binding.txtYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(v.getContext(), new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                        binding.txtYear.setText("Signatures year: "+Integer.toString(selectedYear));
                        choosenYear = selectedYear;
                        getSignature();
                    }
                }, choosenYear, 0);

                if (res!=null && res.getSignatureYearsList().size()>0){
                    builder.showYearOnly()
                            .setYearRange(Integer.parseInt(res.getSignatureYearsList().get(0)), Integer.parseInt(res.getSignatureYearsList().get(res.getSignatureYearsList().size()-1)))
                            .build()
                            .show();
                }else {
                    builder.showYearOnly()
                            .setYearRange(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.YEAR))
                            .build()
                            .show();
                }
            }
        });

        getSignatureYears();
        getSignature();
    }

    public void getSignature() {
        if (isProgress) {
            binding.progressBar.setVisibility(View.GONE);
            isProgress = false;
        }
        else
        {
            binding.progressBar.setVisibility(View.VISIBLE);
        }

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<getSignature> signature = retrofitClass.getSignature(
                LoginUser.getUserId(), LoginUser.getUserTypeKey(), choosenYear==0?"":choosenYear+"");
        signature.enqueue(new Callback<getSignature>() {
            @Override
            public void onResponse(Call<getSignature> call, Response<getSignature> response) {

                if (response.body() != null) {
                    if (response.body().getResponseCode().equals("1")) {
                        signatureList = new ArrayList<>();
                        if (response.body().getSignatureList().size() > 0) {
                            binding.rcNotifications.setVisibility(View.VISIBLE);
                            binding.txtNoDta.setVisibility(View.GONE);
                            signatureList.addAll(response.body().getSignatureList());
                            studentSignatureListAdapter = new StudentSignatureListAdapter(signatureList, getActivity(), new OnRecyclerClick() {
                                @Override
                                public void onClick(int pos, View v, int type) {
                                    Intent i = new Intent(getActivity(), SignatureDetailActivity.class);
                                    i.putExtra("Signature", signatureList.get(pos));
                                    startActivity(i);
                                }
                            });
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            binding.rcNotifications.setLayoutManager(mLayoutManager);
                            //binding.rcNotifications.setItemAnimator(new DefaultItemAnimator());
                            // binding.rcNotifications.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
                            binding.rcNotifications.setAdapter(studentSignatureListAdapter);

                        } else {
                            binding.rcNotifications.setVisibility(View.GONE);
                            binding.txtNoDta.setVisibility(View.VISIBLE);
                        }
                    } else if (response.body().getResponseCode().equals("10")) {
                        Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        LoginUser loginSP = new LoginUser(getActivity());
                        loginSP.clearData();

                        Intent i = new Intent(getActivity(), LoginActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        binding.rcNotifications.setVisibility(View.GONE);
                        binding.txtNoDta.setVisibility(View.VISIBLE);
                    }
                }

                binding.swipyrefreshlayout.setRefreshing(false);
                binding.progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<getSignature> call, Throwable t) {
                binding.swipyrefreshlayout.setRefreshing(false);
                binding.progressBar.setVisibility(View.GONE);
                binding.progressBar.setVisibility(View.GONE);
                binding.txtNoDta.setVisibility(View.VISIBLE);
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });

    }

    private void getSignatureYears() {
        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<SignatureYearsListModel> userPost = retrofitClass.getSignatureYear(LoginUser.getUserId(), LoginUser.getUserTypeKey());
        userPost.enqueue(new Callback<SignatureYearsListModel>() {
            @Override
            public void onResponse(Call<SignatureYearsListModel> call, Response<SignatureYearsListModel> response) {
                try {
                    res = response.body();
                    Collections.sort(res.getSignatureYearsList(), compareByDegree);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SignatureYearsListModel> call, Throwable t) {
                allMethods.setAlert(getContext(), getResources().getString(R.string.app_name), t.getMessage());
            }
        });
    }
}

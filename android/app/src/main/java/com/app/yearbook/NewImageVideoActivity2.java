package com.app.yearbook;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.app.yearbook.databinding.ActivityNewImageVideo2Binding;
import com.app.yearbook.model.allpost.ImageVideoPost;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.utils.Constants;

public class NewImageVideoActivity2 extends AppCompatActivity {

    ActivityNewImageVideo2Binding binding;
    ImageVideoPost model;
    String postType;
    PostListItem postListItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_image_video2);
        postType = getIntent().getStringExtra(Constants.POST_TYPE);
        if (postType.equalsIgnoreCase(Constants.POST_IMAGE)) {
            model = getIntent().getParcelableExtra(Constants.POST_IMAGE);
        }else {
            model = getIntent().getParcelableExtra(Constants.POST_VIDEO);
        }

        setToolbar();
        if (getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false)){
            postListItem = getIntent().getParcelableExtra(Constants.INTENT_DATA);
            model.setSubject(postListItem.getPostTitle());
            model.setDescription(postListItem.getPostSubTitle());
            binding.appbar.toolbarTitle.setText(getString(R.string.edit_post));
        }
        binding.setModel(model);
    }

    private void setToolbar() {
        setSupportActionBar(binding.appbar.toolbar);
        binding.appbar.toolbarTitle.setText(getString(R.string.new_post));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_next, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_next:
                validate();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void validate() {
        if (TextUtils.isEmpty(model.getSubject())) {
            binding.edtSubject.setError("Please enter subject");
        } else if (TextUtils.isEmpty(model.getDescription())) {
            binding.edtSubject.setError("Please enter description");
        } else {
            Intent intent = new Intent(this, PostPreviewActivity.class);
            intent.putExtra(Constants.INTENT_ISEDIT, getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false));
            intent.putExtra(Constants.INTENT_DATA, postListItem);
            intent.putExtra(Constants.POST_TYPE, postType);
            if (postType.equalsIgnoreCase(Constants.POST_IMAGE)) {
                intent.putExtra(Constants.POST_IMAGE, model);
            } else if (postType.equalsIgnoreCase(Constants.POST_VIDEO)) {
                intent.putExtra(Constants.POST_VIDEO, model);
            }
            startActivityForResult(intent, 111);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }
}

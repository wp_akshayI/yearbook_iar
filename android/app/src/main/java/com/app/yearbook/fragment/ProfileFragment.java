package com.app.yearbook.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import androidx.annotation.NonNull;

import com.app.yearbook.AccessCode;
import com.app.yearbook.EmployeeHomeActivity;
import com.app.yearbook.StaffHomeActivity;
import com.app.yearbook.TermsConditionActivity;
import com.app.yearbook.TermsConditionStaffActivity;
import com.app.yearbook.UserProcessWithWeb;
import com.app.yearbook.model.StaffStudent;
import com.app.yearbook.utils.Constants;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.PopupMenu;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.makeramen.roundedimageview.RoundedImageView;
import com.app.yearbook.ImageActivity;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.SetStudentProfileActivity;
import com.app.yearbook.StudentBookmarkActivity;
import com.app.yearbook.StudentHomeActivity;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.studenthome.home.PostListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.BitmapUtils;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private static final int REQUEST_PERMISSION = 101;
    private static final int EDIT_PROFILE = 1000;
    public View view;
    private ImageView imgSignature, imgTagPhotos, imgNotification, imgBookmark, imgAddPhoto;
    private RoundedImageView imgUserImage;
    // private LinearLayout lvSign;
    // public RecyclerView rvUserPost;
    /// private LinearLayout lvNoData;
    public ProgressDialog progressDialog;
    private AllMethods allMethods;
    private LoginUser loginUser;
    private String userId, profile, schoolId, mCurrentPhotoPath, userPwd, studentName;
    public ArrayList<PostListItem> getUserList;
    private ArrayList<String> getUserImage;
    private int GALLERY = 1, CAMERA = 2;
    private Uri resultUri;
    private File fileImage;
    private MultipartBody.Part part;
    private MenuItem menuPhoto;
    private EditText edtOldPassword, edtNewPassword, edtConfirmPassword;
    private TextInputLayout input_layout_newpassword, input_layout_confirmpassword, input_layout_oldpassword;
    private ProgressBar progressBar;
    private TextView tvName, tvStudentId, tvGrade, tvAccessCode;
    private TabLayout tab_layout;
    private ViewPager viewPager;
    // SwipyRefreshLayout swipyRefreshLayout;
    private static final String TAG = "ProfileFragment";

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        setHasOptionsMenu(true);
        setView();
        getUserProfileData();
        return view;
    }

    public void setView() {

        //common methods
        allMethods = new AllMethods();

        //tab
        tab_layout = view.findViewById(R.id.tab_layout);

        //view pager
        viewPager = view.findViewById(R.id.view_pager);

        viewPager.setOffscreenPageLimit(0);

        //progress bar
        progressBar = view.findViewById(R.id.progress);

        //profile img
        imgUserImage = view.findViewById(R.id.imgUserImage);
//        imgUserImage.setOnClickListener(this);

        //profile pic dialog
        imgAddPhoto = view.findViewById(R.id.imgAddPhoto);
        imgAddPhoto.setOnClickListener(this);

        //tvname, tngrad,tvId
        tvGrade = view.findViewById(R.id.tvGrade);
        tvAccessCode = view.findViewById(R.id.tvAccessCode);
        tvName = view.findViewById(R.id.tvName);
        tvStudentId = view.findViewById(R.id.tvStudentId);

        //SP
        loginUser = new LoginUser(getActivity());

        notifyUserData();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.getString("redirectOn").contentEquals("notification")) {
                viewPager.setCurrentItem(1);
            }
        }

        view.findViewById(R.id.view_access_code).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AccessCode.class);
                startActivity(i);
            }
        });
    }

    private void notifyUserData() {
        if (loginUser.getUserData() != null) {

            userId = loginUser.getUserData().getUserId();
            profile = loginUser.getUserData().getUserImage();
            schoolId = loginUser.getUserData().getSchoolId();
            userPwd = loginUser.getUserData().getUserPassword();

            studentName = loginUser.getUserData().getUserFirstname() + " " + loginUser.getUserData().getUserLastname();
//            if (!studentName.equals("")) {
//                studentName = AllMethods.getCapsSentences(studentName);
//            }
            tvName.setText(studentName);
            tvGrade.setText("Grade: " + loginUser.getUserData().getUserGrade());
            tvAccessCode.setText("Access Code: " + loginUser.getUserData().getUserAccessCode());
            tvStudentId.setText("Student ID# " + loginUser.getUserData().getUserStudentId());

            Log.d("TTT", "Profileee: " + profile);
            if (profile != null && !profile.equals("")) {
                Glide.with(getActivity())
                        .load(profile)
                        .asBitmap()//.diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .placeholder(R.mipmap.profile_placeholder)
                        .error(R.mipmap.profile_placeholder)
                        .into(imgUserImage);
            }

            if (StudentHomeActivity.instance!=null){
                StudentHomeActivity.instance.notifyToolbarTitle(loginUser.getUserData().getSchoolName());
            }
            //set profile photo
        }

        if (loginUser.getUserData().getIsAccessCodeVerify() != null
                && loginUser.getUserData().getIsAccessCodeVerify().equalsIgnoreCase("Yes")) {
            view.findViewById(R.id.view_access_code).setVisibility(View.GONE);
            view.findViewById(R.id.view_profile).setVisibility(View.VISIBLE);
        } else {
            view.findViewById(R.id.view_access_code).setVisibility(View.VISIBLE);
            view.findViewById(R.id.view_profile).setVisibility(View.GONE);
        }

        viewPager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));
        tab_layout.setupWithViewPager(viewPager);
        tab_layout.getTabAt(0).setIcon(R.drawable.bookmark_tab_ic);
        tab_layout.getTabAt(1).setIcon(R.drawable.notification_tab_ic);
        tab_layout.getTabAt(2).setIcon(R.drawable.yearbook_tab_ic);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.getString("redirectOn").contentEquals("notification")) {
                viewPager.setCurrentItem(1);
            }
        }
    }

    private void getUserProfileData() {
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<JsonObject> userPost = retrofitClass.get_user_profile(userId, schoolId);
        userPost.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    JsonObject res = response.body();
                    JSONObject jsonObject = new JSONObject(res.toString());
                    if (jsonObject.getString("ResponseCode").equals(String.valueOf(1))) {

                        String userDetail = jsonObject.getJSONObject("user_profile").getString("user_detail");

                        StaffStudent dd = new Gson().fromJson(userDetail, StaffStudent.class);

                        if (dd != null) {
                            loginUser.setUserData(dd);
                            Log.d("TTT", "SP: " + loginUser.getUserData().getSchoolId());
                            notifyUserData();
                        } else {
                            allMethods.setAlert(getContext(), getResources().getString(R.string.app_name), jsonObject.getString("ResponseMsg"));
                        }
                    } else {
                        allMethods.setAlert(getContext(), getResources().getString(R.string.app_name), jsonObject.getString("ResponseMsg"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

//
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getContext(), getResources().getString(R.string.app_name), t.getMessage());
            }
        });
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    if (loginUser.getUserData().getIsAccessCodeVerify() != null && loginUser.getUserData().getIsAccessCodeVerify().equalsIgnoreCase("Yes")) {
                        return new StudentBookmarkFragment();
                    } else {
                        return new AccessCodeDisableDetail(1);
                    }
                case 1:
                    if (loginUser.getUserData().getIsAccessCodeVerify() != null && loginUser.getUserData().getIsAccessCodeVerify().equalsIgnoreCase("Yes")) {
                        return new StudentNotificationFragment();
                    } else {
                        return new AccessCodeDisableDetail(2);
                    }
                case 2:
                    return new StudentSignatureFragment();
                default:
                    return new StudentSignatureFragment();

            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }


    @Override
    public void onClick(View v) {
//        if (v.getId() == R.id.imgSignature) {
//            Intent i = new Intent(getActivity(), ViewSignatureActivity.class);
//            i.putExtra("Id", userId);
//            startActivity(i);
//        } else

        if (v.getId() == R.id.imgTagPhotos) {
//            Intent i = new Intent(getActivity(), SearchUserTagPhotoActivity.class);
//            i.putExtra("schoolId", schoolId);
//            i.putExtra("userId", userId);
//            startActivity(i);
        }
//            else if (v.getId() == R.id.imgNotification) {
//            Intent i = new Intent(getActivity(), NotificationActivity.class);
//            i.putExtra("userId", userId);
//            startActivity(i);
//        }

        else if (v.getId() == R.id.imgBookmark) {
            Intent i = new Intent(getActivity(), StudentBookmarkActivity.class);
            i.putExtra("userId", userId);
            startActivity(i);
        } else if (v.getId() == R.id.imgAddPhoto || v.getId() == R.id.imgUserImage) {
            if (getUserImage != null) {
                Intent i = new Intent(getActivity(), SetStudentProfileActivity.class);
                i.putExtra("userId", userId);
                i.putStringArrayListExtra("UserImage", getUserImage);
                startActivityForResult(i, EDIT_PROFILE);
            } else {
                alertMsg("User images not found");
            }
//            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                Log.d("TTT", "Permission...");
//                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
//            } else {
//                Log.d("TTT", "granted Permission...");
//                setDialog();
//            }
        } else if (v.getId() == R.id.imgUserImage) {
            if (profile != null && !profile.equals("")) {
                Intent i = new Intent(getActivity(), ImageActivity.class);
                i.putExtra("FileUrl", profile);
                startActivity(i);
            }
        }
    }

    public void setDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Available actions");
        builder.setCancelable(false);
        builder.setItems(new CharSequence[]{"Pick from gallery", "Capture picture", "Remove current photo", "Cancel"},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                            case 2:
                                dialog.dismiss();
                                removePhoto();
                                break;
                            case 3:
                                dialog.dismiss();
                                break;
                        }
                    }
                });
        builder.create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    Log.d("TTT", "Permission... onRequestPermissionsResult");
                    setDialog();
                } else {
                    Log.d("TTT", "denyyyy... onRequestPermissionsResult");
                    Toast.makeText(getActivity(), "Deny", Toast.LENGTH_SHORT).show();
                    //code for deny
                }
                break;
        }
    }

    public void removePhoto() {
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<Login> userPost = retrofitClass.removeProfilePhoto(Integer.parseInt(userId));
        userPost.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equalsIgnoreCase("1")) {
                    //set profile photo
                    loginUser.setUserData(response.body().getData());
                    Glide.with(getActivity())
                            .load(R.mipmap.profile_placeholder)
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.profile_placeholder)
                            .error(R.mipmap.profile_placeholder)
                            .into(imgUserImage);
                    allMethods.setAlert(getActivity(), "", "Profile photo remove successfully");

                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(getActivity());
                    loginSP.clearData();

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                } else {
                    allMethods.setAlert(getActivity(), "", response.body().getResponseMsg() + "");
                }

            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });

    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);

    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        Log.d("TTT", "Resulttt: " + requestCode + " / " + UCrop.REQUEST_CROP);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                });
                resultUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getActivity().getContentResolver().query(resultUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                Log.d("TTT", "PicturePath: " + picturePath);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                Log.w("TTT", "Pathhhh:  " + resultUri.getPath() + " / " + thumbnail);
                //imgUserImage.setImageBitmap(thumbnail);

                if (thumbnail != null) {
                    Glide.with(getActivity())
                            .load(resultUri)
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .centerCrop()
                            .listener(new RequestListener<Uri, Bitmap>() {
                                @Override
                                public boolean onException(Exception e, Uri model, Target<Bitmap> target, boolean isFirstResource) {
                                    Log.d("TTT", "OnException: ");
                                    progressBar.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Bitmap resource, Uri model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    Log.d("TTT", "Readyy: ");
                                    progressBar.setVisibility(View.GONE);
                                    showPopup();
                                    return false;
                                }
                            })
                            .placeholder(R.mipmap.profile_placeholder)
                            .error(R.mipmap.profile_placeholder)
                            .into(imgUserImage);


                    fileImage = new File(BitmapUtils.getFilePathFromUri(getActivity(), AllMethods.getImageUri(getActivity(), thumbnail)));
                    RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
                    part = MultipartBody.Part.createFormData("user_image", fileImage.getName(), filePost);
                    // menuPhoto.setVisible(true);
                } else {
                    Toast.makeText(getActivity(), "Select other image", Toast.LENGTH_SHORT).show();
                }

            }

        } else if (requestCode == CAMERA) {
            if (data != null) {

                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                imgUserImage.setImageBitmap(thumbnail);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                File wallpaperDirectory = new File(
                        Environment.getExternalStorageDirectory() + "/yearbook");
                // have the object build the directory structure, if needed.
                if (!wallpaperDirectory.exists()) {
                    wallpaperDirectory.mkdirs();
                }

                try {
                    fileImage = new File(wallpaperDirectory, Calendar.getInstance()
                            .getTimeInMillis() + ".jpg");
                    fileImage.createNewFile();
                    FileOutputStream fo = new FileOutputStream(fileImage);
                    fo.write(bytes.toByteArray());
                    MediaScannerConnection.scanFile(getActivity(),
                            new String[]{fileImage.getPath()},
                            new String[]{"image/jpeg"}, null);
                    fo.close();
                    Log.d("TAG", "File Saved::--->" + fileImage.getAbsolutePath());

                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                fileImage = new File(BitmapUtils.getFilePathFromUri(getActivity(), AllMethods.getImageUri(getActivity(), thumbnail)));
                RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
                part = MultipartBody.Part.createFormData("user_image", fileImage.getName(), filePost);
                //menuPhoto.setVisible(true);
                showPopup();
            }
        } else if (resultCode == getActivity().RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            Log.d("TTT", "Crop img: " + UCrop.getOutput(data));
            resultUri = UCrop.getOutput(data);
            imgUserImage.setImageURI(resultUri);

            fileImage = new File(BitmapUtils.getFilePathFromUri(getActivity(), resultUri));
            RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
            part = MultipartBody.Part.createFormData("user_image", fileImage.getName(), filePost);

            menuPhoto.setVisible(true);

        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Toast.makeText(getActivity(), cropError.getMessage() + "", Toast.LENGTH_SHORT).show();
        } else if (requestCode == EDIT_PROFILE && resultCode == getActivity().RESULT_OK) {
            profile = data.getStringExtra("profile");
            Glide.with(getActivity())
                    .load(profile)
                    .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .placeholder(R.mipmap.profile_placeholder)
                    .error(R.mipmap.profile_placeholder)
                    .into(imgUserImage);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.logout, menu);
        menuPhoto = menu.findItem(R.id.photo);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.setting) {
            View menuItemView = getActivity().findViewById(R.id.setting);
            showMenu(menuItemView);

        } else if (item.getItemId() == R.id.photo) {
            // AddEditPhoto();

        }
        return super.onOptionsItemSelected(item);
    }

    public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(getActivity(), v);
        popup.getMenuInflater().inflate(R.menu.profileseeting, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Manage Account")) {
                    Intent intent = new Intent(getActivity(), UserProcessWithWeb.class);
                    intent.putExtra("action", Constants.USER_WEB_PROFILE);
                    startActivity(intent);
                } else if (item.getTitle().equals("Change password")) {
                    androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.custom_alertbox, null);
                    dialogBuilder.setView(dialogView);
                    final androidx.appcompat.app.AlertDialog alert = dialogBuilder.create();
                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    alert.setCancelable(false);

                    edtNewPassword = dialogView.findViewById(R.id.edtNewPassword);
                    edtNewPassword.setOnFocusChangeListener(newPassword);

                    edtOldPassword = dialogView.findViewById(R.id.edtOldPassword);
                    edtOldPassword.setOnFocusChangeListener(oldPassword);

                    edtConfirmPassword = dialogView.findViewById(R.id.edtConfirmPassword);
                    edtConfirmPassword.setOnFocusChangeListener(confirmPassword);

                    input_layout_oldpassword = dialogView.findViewById(R.id.input_layout_oldpassword);
                    input_layout_newpassword = dialogView.findViewById(R.id.input_layout_newpassword);
                    input_layout_confirmpassword = dialogView.findViewById(R.id.input_layout_confirmpassword);

                    Button btnCancel = dialogView.findViewById(R.id.btnCancel);
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    Button btnOk = dialogView.findViewById(R.id.btnOk);
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (validation()) {
                                if (!userPwd.toString().equals(edtOldPassword.getText().toString())) {
                                    Log.d("TTT", "wrongg pwd");
                                    input_layout_oldpassword.setErrorEnabled(true);
                                    input_layout_oldpassword.setError("Your old password is wrong");
                                } else {
                                    alert.dismiss();
                                    changePassword(edtNewPassword.getText().toString());
                                }
                            }
                        }
                    });
                    alert.show();
                } else if (item.getTitle().equals("Logout")) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setCancelable(false);
                    dialog.setTitle("");
                    dialog.setMessage("Are you sure you want to logout ?");
                    dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //Action for "Delete".
                            dialog.dismiss();

                            String token = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Token", "");
                            Log.d("TTT", "token: " + token);

                            if (!token.equals("")) {
                                progressDialog = ProgressDialog.show(getActivity(), "", "", true);
                                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                progressDialog.setContentView(R.layout.progress_view);
                                ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                                Circle bounce = new Circle();
                                bounce.setColor(Color.BLACK);
                                progressBar.setIndeterminateDrawable(bounce);

                                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                final Call<GiveReport> userPost = retrofitClass.logout(token, "user");
                                userPost.enqueue(new Callback<GiveReport>() {
                                    @Override
                                    public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }

                                        LoginUser loginSP = new LoginUser(getActivity());
                                        loginSP.clearData();

                                        Intent i = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(i);
                                        ((StudentHomeActivity) getActivity()).finish();
                                    }

                                    @Override
                                    public void onFailure(Call<GiveReport> call, Throwable t) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }
                                        allMethods.setAlert(getActivity(), "", t.getMessage() + "");
                                    }
                                });
                            } else {
                                LoginUser loginSP = new LoginUser(getActivity());
                                loginSP.clearData();

                                Intent i = new Intent(getActivity(), LoginActivity.class);
                                startActivity(i);
                                getActivity().finish();
                            }
                        }


                    });
                    dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    final AlertDialog alert = dialog.create();
                    alert.show();

                }
                return true;
            }
        });
        popup.show();

    }

    private View.OnFocusChangeListener oldPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtOldPassword.getText().toString().equals("")) {
                    input_layout_oldpassword.setErrorEnabled(true);
                    input_layout_oldpassword.setError("Your old password is empty");
                } else {
                    input_layout_oldpassword.setErrorEnabled(false);
                    input_layout_oldpassword.setError("");
                }
            }

        }
    };


    private View.OnFocusChangeListener newPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtNewPassword.getText().toString().equals("")) {
                    input_layout_newpassword.setErrorEnabled(true);
                    input_layout_newpassword.setError("Your new password is empty");
                } else {
                    input_layout_newpassword.setErrorEnabled(false);
                    input_layout_newpassword.setError("");
                }
            }

        }
    };

    private View.OnFocusChangeListener confirmPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtConfirmPassword.getText().toString().equals("")) {
                    input_layout_confirmpassword.setErrorEnabled(true);
                    input_layout_confirmpassword.setError("Your confirm password is empty");
                } else {
                    input_layout_confirmpassword.setErrorEnabled(false);
                    input_layout_confirmpassword.setError("");
                }
            }

        }
    };

    public boolean validation() {
        boolean result = false;

        //old pwd
        Log.d("TTT", "Old Pwd: " + userPwd + " / " + edtOldPassword.getText().toString());

        if (edtOldPassword.getText().toString().trim().equals("")) {
            input_layout_oldpassword.setErrorEnabled(true);
            input_layout_oldpassword.setError("Your old password is empty");
            result = false;
        } else if (!userPwd.toString().equals(edtOldPassword.getText().toString())) {
            Log.d("TTT", "wrongg pwd");
            input_layout_oldpassword.setErrorEnabled(true);
            input_layout_oldpassword.setError("Your old password is wrong");
            result = false;
        } else {
            input_layout_oldpassword.setErrorEnabled(false);
            input_layout_oldpassword.setError("");
            result = true;
        }

        //new pwd
        if (edtNewPassword.getText().toString().trim().equals("")) {
            input_layout_newpassword.setErrorEnabled(true);
            input_layout_newpassword.setError("Your new password is empty");
            result = false;
        } else {
            input_layout_newpassword.setErrorEnabled(false);
            input_layout_newpassword.setError("");
            result = true;
        }

        //confirm new pwd
        if (edtConfirmPassword.getText().toString().trim().equals("")) {
            input_layout_confirmpassword.setErrorEnabled(true);
            input_layout_confirmpassword.setError("Your confirm password is empty");
            result = false;
        } else if (!edtConfirmPassword.getText().toString().equals(edtNewPassword.getText().toString())) {
            input_layout_confirmpassword.setErrorEnabled(true);
            input_layout_confirmpassword.setError("Confirm password and new password is must be same");
            result = false;
        } else {
            input_layout_confirmpassword.setErrorEnabled(false);
            input_layout_confirmpassword.setError("");
            result = true;

        }

        Log.d("TTT", "Result: " + result);
        return result;

    }

    public void changePassword(String pwd) {
        //Progress bar
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> userPost = retrofitClass.changePassword(Integer.parseInt(userId), "user", pwd);
        userPost.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", response.body().getResponseMsg() + "");
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });
    }

    public void showPopup() {
        androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
        dialogBuilder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog alert = dialogBuilder.create();
        alert.setCancelable(false);
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
        tvMessage.setText("Are you sure you would like to make this change?");

        TextView tvYes = dialogView.findViewById(R.id.tvYes);
        TextView tvNo = dialogView.findViewById(R.id.tvNo);

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                // AddEditPhoto();
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                imgUserImage.setImageDrawable(getActivity().getResources().getDrawable(R.mipmap.profile_placeholder));
            }
        });
        alert.show();
    }


//    public void getUserPost() {
//        //Progress bar
//        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
//        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        progressDialog.setContentView(R.layout.progress_view);
//        ProgressBar progressBar1 = progressDialog.findViewById(R.id.progress);
//        Circle bounce = new Circle();
//        bounce.setColor(Color.BLACK);
//        progressBar1.setIndeterminateDrawable(bounce);
//
//        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
//        final Call<GetUserProfile> userPost = retrofitClass.getUserProfile(Integer.parseInt(userId), Integer.parseInt(schoolId));
//        userPost.enqueue(new Callback<GetUserProfile>() {
//            @Override
//            public void onResponse(Call<GetUserProfile> call, Response<GetUserProfile> response) {
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//                getUserList = new ArrayList<>();
//                if (response.body().getResponseCode().equals("1")) {
//                    if (response.body().getUserProfile().getPostList().size() > 0) {
//                        rvUserPost.setVisibility(View.VISIBLE);
//                        lvNoData.setVisibility(View.GONE);
//
//                        getUserList.addAll(response.body().getUserProfile().getPostList());
//                        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
//                        rvUserPost.setLayoutManager(mLayoutManager);
//                        UserTagPostAdapter mAdapter = new UserTagPostAdapter(getUserList, getActivity());
//                        rvUserPost.setAdapter(mAdapter);
//
//                    } else {
//                        rvUserPost.setVisibility(View.GONE);
//                        lvNoData.setVisibility(View.VISIBLE);
//                    }
//
//                    if (response.body().getUserProfile().getUserDetail() != null) {
//                        getUserImage.clear();
//                        profile = response.body().getUserProfile().getUserDetail().getUserImage();
//
//                        StaffStudent staffUser = loginUser.getUserData();
//                        staffUser.setUserImage(profile);
//
//                        loginUser.setUserData(staffUser);
//
//                        if (response.body().getUserProfile().getUserDetail().getUserImages().size() > 0) {
//                            getUserImage.addAll(response.body().getUserProfile().getUserDetail().getUserImages());
//                        }
//                        if (profile != null && !profile.equals("")) {
//                            progressBar.setVisibility(View.VISIBLE);
//                            Glide.with(getActivity())
//                                    .load(profile)
//                                    .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                                    .listener(new RequestListener<String, Bitmap>() {
//                                        @Override
//                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
//                                            progressBar.setVisibility(View.GONE);
//                                            return false;
//                                        }
//
//                                        @Override
//                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                                            progressBar.setVisibility(View.GONE);
//                                            return false;
//                                        }
//                                    })
//                                    .centerCrop()
//                                    .placeholder(R.mipmap.profile_placeholder)
//                                    .error(R.mipmap.profile_placeholder)
//                                    .into(imgUserImage);
//                        }
//                    }
//                } else if (response.body().getResponseCode().equals("10")) {
//                    Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
//                    LoginUser loginSP = new LoginUser(getActivity());
//                    loginSP.clearData();
//
//                    Intent i = new Intent(getActivity(), LoginActivity.class);
//                    startActivity(i);
//                    getActivity().finish();
//                } else {
//                    rvUserPost.setVisibility(View.GONE);
//                    lvNoData.setVisibility(View.VISIBLE);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GetUserProfile> call, Throwable t) {
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
//            }
//        });
//    }


    void alertMsg(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setCancelable(false);
        dialog.setTitle("");
        dialog.setMessage(msg);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
                dialog.dismiss();
            }
        });
        /*dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });*/

        final AlertDialog alert = dialog.create();
        alert.show();
    }
}

import React from "react"
import { withNavigation } from "react-navigation";
import { TitleSubTitle, HeadingTitle, ImageSection, ImageTextOverlay, InsideGrid, InsideList, PortraitColumns, VideoSection, Gallery } from "../../Sections";

function SectionList({ data, screenId, navigation }) {
    if (data.component_type === 0) return <TitleSubTitle data={data} />
    else if (data.component_type === 1) return <HeadingTitle data={data} />
    else if (data.component_type === 2) return <ImageSection data={data} />
    else if (data.component_type === 3) return <ImageTextOverlay data={data} />
    else if (data.component_type === 4) return <VideoSection data={data} />
    else if (data.component_type === 5) return <PortraitColumns data={data} />
    else if (data.component_type === 6) return <InsideGrid data={data} navigation={navigation} screenId={screenId} />
    else if (data.component_type === 7) return <InsideList data={data} navigation={navigation} screenId={screenId}/>
    else if (data.component_type === 8) return <Gallery data={data} />
    else return null;
}

export default withNavigation(SectionList);
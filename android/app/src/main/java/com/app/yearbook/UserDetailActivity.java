package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.StudentProfileAdapter;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.studenthome.profile.GetUserProfile;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.OnRecyclerClick;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDetailActivity extends AppCompatActivity implements View.OnClickListener{

    private Toolbar toolbar;
    public String userId, userName,schoolId,type;
    //private TextView tvsign;
    private ImageView imgUserImage;
    private Uri getPhoto;
    private ImageView imgSignYearbook;
    private ImageView imgPhotos,imgTagPhotos;
    public ProgressDialog progressDialog;
    private AllMethods allMethods;
   // public static ArrayList<PostList> getUserList;
    public static RecyclerView rvUserPost;
    private LinearLayout lvNoData;
    ArrayList<String> studentProfiles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        //set view
        setView();
    }

    public void setView() {
        //intent
        if (getIntent().getExtras() != null) {
            userId = getIntent().getStringExtra("Id");
            type = getIntent().getStringExtra("type");
            userName = getIntent().getStringExtra("Name");

            if(getIntent().getStringExtra("Photo") != null) {
                getPhoto = Uri.parse(getIntent().getStringExtra("Photo"));
            }

            if(getIntent().getStringExtra("schoolId") != null) {
                schoolId=getIntent().getStringExtra("schoolId");
            }

            Log.d("TTT", "Id:: " + userId + " / " + getPhoto+" / "+schoolId+" / "+type);
        }

        //img
        imgTagPhotos=findViewById(R.id.imgTagPhotos);
        imgTagPhotos.setOnClickListener(this);
        imgPhotos=findViewById(R.id.imgPhotos);
        imgPhotos.setOnClickListener(this);

        //lvsign
        imgSignYearbook=findViewById(R.id.imgSignYearbook);
        imgSignYearbook.setOnClickListener(sign);

        //set toolbar
        setToolbar();

//        tvsign = findViewById(R.id.tvSign);
//        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/brasileirinha_personal_use.ttf");
//        tvsign.setTypeface(typeface);

        imgUserImage = findViewById(R.id.imgUserImage);
        imgUserImage.setOnClickListener(this);
        Glide.with(getApplicationContext())
                .load(getPhoto)
                .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.home_post_img_placeholder)
                .error(R.mipmap.home_post_img_placeholder)
                .into(imgUserImage);

        //all methods object
        allMethods = new AllMethods();

        //lv no
        lvNoData =findViewById(R.id.lvNoData);

        //rv
        rvUserPost = findViewById(R.id.rvUserPost);

        studentProfiles=new ArrayList<>();
        getUserPost();
    }

    public View.OnClickListener sign=new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Log.d("TTT","Type: "+type);
            if(type.equalsIgnoreCase("staff"))
            {
                Intent i = new Intent(UserDetailActivity.this, ViewSignatureActivity.class);
                i.putExtra("Id", userId);
                startActivity(i);
            }
            else {

                checkYearbookPublisgOrNot();
            }
        }
    };

    private void checkYearbookPublisgOrNot() {
        //Progress bar
        progressDialog = ProgressDialog.show(UserDetailActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> userPost = retrofitClass.checkYearbookPublish(Integer.parseInt(schoolId));
        userPost.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if(response.body()!=null)
                {
                    if(response.body().getResponseCode().equals("1"))
                    {
                        Intent i = new Intent(UserDetailActivity.this, AddSignatureActivity.class);
                        i.putExtra("searchUserId", userId);
                        startActivity(i);
                    }
                    else  if(response.body().getResponseCode().equals("3"))
                    {
                        allMethods.setAlert(UserDetailActivity.this, "", response.body().getResponseMsg()+"");
                    }
                    else
                    {
                        allMethods.setAlert(UserDetailActivity.this, "", response.body().getResponseMsg()+"");
                    }
                }
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(UserDetailActivity.this, "", t.getMessage() + "");
            }
        });


    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(userName);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
           if(v.getId()==R.id.imgTagPhotos)
            {
                Intent i=new Intent(getApplicationContext(),SearchUserTagPhotoActivity.class);
                i.putExtra("schoolId",schoolId);
                i.putExtra("userId",userId);
                startActivity(i);

            }
            else if(v.getId()==R.id.imgUserImage)
           {
               if(getPhoto!=null)
               {
                   Intent i = new Intent(UserDetailActivity.this, ImageActivity.class);
                   i.putExtra("FileUrl",getPhoto.toString());
                   startActivity(i);
               }
           }
    }

    public void getUserPost() {
        //Progress bar
        progressDialog = ProgressDialog.show(UserDetailActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetUserProfile> userPost = retrofitClass.getUserProfile(Integer.parseInt(userId),Integer.parseInt(schoolId));
        userPost.enqueue(new Callback<GetUserProfile>() {
            @Override
            public void onResponse(Call<GetUserProfile> call, Response<GetUserProfile> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
               // getUserList = new ArrayList<>();
                if (response.body().getResponseCode().equals("1")) {
                    if (response.body().getUserProfile().getUserDetail()!=null) {
                        rvUserPost.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);

                        studentProfiles.addAll(response.body().getUserProfile().getUserDetail().getUserImages());
                        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
                        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserPost.setLayoutManager(gridLayoutManager);
                        rvUserPost.setItemAnimator(new DefaultItemAnimator());

                        StudentProfileAdapter userProfileAdapter = new StudentProfileAdapter(UserDetailActivity.this, studentProfiles, new OnRecyclerClick() {
                            @Override
                            public void onClick(int pos, View v, int type) {

                                Intent i = new Intent(UserDetailActivity.this, ImageActivity.class);
                                i.putExtra("FileUrl", studentProfiles.get(pos));
                                startActivity(i);
                            }
                        });

                        rvUserPost.setAdapter(userProfileAdapter);

//                        getUserList.addAll(response.body().getPostList());
//                        GridLayoutManager mLayoutManager = new GridLayoutManager(UserDetailActivity.this, 3);
//                        rvUserPost.setLayoutManager(mLayoutManager);
//
//                        UserDetailPostAdapter mAdapter = new UserDetailPostAdapter(getUserList, UserDetailActivity.this,type);
//                        rvUserPost.setAdapter(mAdapter);

                    } else {
                        rvUserPost.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(UserDetailActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(UserDetailActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(UserDetailActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    rvUserPost.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                    //allMethods.setAlert(UserDetailActivity.this, "", response.body().getResponseMsg() + "");
                }
            }

            @Override
            public void onFailure(Call<GetUserProfile> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(UserDetailActivity.this, "", t.getMessage() + "");
            }
        });
    }

}

package com.app.yearbook.adapter;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.yearbook.R;
import com.app.yearbook.utils.OnRecyclerClick;

import java.util.ArrayList;

public class UserImageAdapter extends RecyclerView.Adapter<UserImageAdapter.MyViewHolderGallery> {

    private Context context;
    private ArrayList<String> imgList;
    private LayoutInflater inflater;
    private OnRecyclerClick onRecyclerClick;
    private static final String TAG = "UserImageAdapter";

    public UserImageAdapter(Context context, ArrayList<String> imgList, OnRecyclerClick onRecyclerClick) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.imgList = imgList;
        this.onRecyclerClick = onRecyclerClick;
    }

    @Override
    public UserImageAdapter.MyViewHolderGallery onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = inflater.inflate(R.layout.studentimage_rowlayout, parent, false);
        MyViewHolderGallery holder = new MyViewHolderGallery(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(MyViewHolderGallery holder, int position) {
        // holder.imgIcon.setImageURI(imgList.get(position).toString());

        Log.d(TAG, "onBindViewHolder: " + imgList.get(position).toString());
        Glide.with(context)
                .load(imgList.get(position))
                .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.home_post_img_placeholder)
                .error(R.mipmap.home_post_img_placeholder)
                .into(holder.imgIcon);
    }

    @Override
    public int getItemCount() {
        return imgList.size();
    }

    class MyViewHolderGallery extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgIcon, ivOption;

        public MyViewHolderGallery(View itemView) {
            super(itemView);
            imgIcon = itemView.findViewById(R.id.image);
            ivOption = itemView.findViewById(R.id.ivOption);

            ivOption.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.ivOption) {
                // onRecyclerClick.onClick(getAdapterPosition(), v, 0);
                showDialogForSameStudent(v, getAdapterPosition());
            }
        }
    }

    public void showDialogForSameStudent(final View view, final int getAdapterPosition) {

        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.same_student_opt, popup.getMenu());

        String patter = "^(http|https)://.*$";
        String url=imgList.get(getAdapterPosition);

        if (url.matches(patter)){
            Log.d(TAG, "showDialogForSameStudent: https");
            MenuItem itemNotification = popup.getMenu().findItem(R.id.edit);
            itemNotification.setVisible(true);

        }else{
            Log.d(TAG, "showDialogForSameStudent:");
            MenuItem itemNotification = popup.getMenu().findItem(R.id.edit);
            itemNotification.setVisible(false);
        }


        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Edit")) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Are you sure you want to edit the image?")
                            .setCancelable(false)
                            .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    onRecyclerClick.onClick(getAdapterPosition, view, 1);
                                }
                            })
                            .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    }
                            );

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    alert.setTitle("");
                    alert.show();

                } else if (item.getTitle().equals("Delete")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Are you sure you want to delete the image?")
                            .setCancelable(false)
                            .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    onRecyclerClick.onClick(getAdapterPosition, view, 0);
                                }
                            })
                            .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    }
                            );

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    alert.setTitle("");
                    alert.show();

                }
                return true;
            }
        });
        popup.show();//showing popup menu
    }
}



package com.app.yearbook.adapter;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.app.yearbook.R;
import com.app.yearbook.model.employee.UserListItem;
import com.app.yearbook.utils.OnRecyclerClick;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

public class StudentPhotosAdapter extends RecyclerView.Adapter<StudentPhotosAdapter.MyViewHolderGallery> implements Filterable {

    private Context context;
    private ArrayList<UserListItem> imgListFilter;
    private ArrayList<UserListItem> imgList;
    private LayoutInflater inflater;
    private OnRecyclerClick onRecyclerClick;
    private static final String TAG = "UserImageAdapter";

    public StudentPhotosAdapter(Context context, ArrayList<UserListItem> imgList, OnRecyclerClick onRecyclerClick) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.imgListFilter = imgList;
        this.imgList=imgList;
        this.onRecyclerClick = onRecyclerClick;
    }

    @Override
    public StudentPhotosAdapter.MyViewHolderGallery onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = inflater.inflate(R.layout.layout_rowimage, parent, false);
        MyViewHolderGallery holder = new MyViewHolderGallery(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(MyViewHolderGallery holder, int position) {
        // holder.imgIcon.setImageURI(imgList.get(position).toString());

        Log.d(TAG, "onBindViewHolder/// " + imgListFilter.get(position).getUserSinglePhoto());
        Glide.with(context)
                .load(imgListFilter.get(position).getUserSinglePhoto())
                .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.home_post_img_placeholder)
                .error(R.mipmap.home_post_img_placeholder)
                .into(holder.imgIcon);
    }

    @Override
    public int getItemCount() {
        return imgListFilter.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    imgListFilter = imgList;
                } else {
                    ArrayList<UserListItem> filteredList = new ArrayList<>();
                    for (UserListItem row : imgList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getUserFirstname().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                            Log.d("TTT","Filter size: "+filteredList.size());
                        }
                    }
                    imgListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = imgListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                imgListFilter = (ArrayList<UserListItem>) filterResults.values;
                Log.d("TTT","publishResults: "+imgListFilter.size());
                notifyDataSetChanged();
            }
        };
    }

    class MyViewHolderGallery extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgIcon;

        public MyViewHolderGallery(View itemView) {
            super(itemView);
            imgIcon = itemView.findViewById(R.id.image);
            imgIcon.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.image) {
                 onRecyclerClick.onClick(getAdapterPosition(), v, 0);
            }
        }
    }
}



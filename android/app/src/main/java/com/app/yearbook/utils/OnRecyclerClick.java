package com.app.yearbook.utils;

import android.view.View;


public interface OnRecyclerClick {
    void onClick(int pos, View v, int type);

}

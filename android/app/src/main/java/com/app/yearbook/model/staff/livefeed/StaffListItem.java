package com.app.yearbook.model.staff.livefeed;

import com.google.gson.annotations.SerializedName;

public class StaffListItem{

	@SerializedName("staff_image")
	private String staffImage;

	@SerializedName("post_image")
	private String postImage;

	@SerializedName("post_id")
	private String postId;

	@SerializedName("school_id")
	private String schoolId;

	@SerializedName("staff_lastname")
	private String staffLastname;

	@SerializedName("staff_id")
	private String staffId;

	@SerializedName("post_file")
	private String postFile;

	@SerializedName("staff_firstname")
	private String staffFirstname;

	@SerializedName("post_file_type")
	private String postFileType;

	public void setStaffImage(String staffImage){
		this.staffImage = staffImage;
	}

	public String getStaffImage(){
		return staffImage;
	}

	public void setPostImage(String postImage){
		this.postImage = postImage;
	}

	public String getPostImage(){
		return postImage;
	}

	public void setPostId(String postId){
		this.postId = postId;
	}

	public String getPostId(){
		return postId;
	}

	public void setSchoolId(String schoolId){
		this.schoolId = schoolId;
	}

	public String getSchoolId(){
		return schoolId;
	}

	public void setStaffLastname(String staffLastname){
		this.staffLastname = staffLastname;
	}

	public String getStaffLastname(){
		return staffFirstname+" "+staffLastname;
	}

	public void setStaffId(String staffId){
		this.staffId = staffId;
	}

	public String getStaffId(){
		return staffId;
	}

	public void setPostFile(String postFile){
		this.postFile = postFile;
	}

	public String getPostFile(){
		return postFile;
	}

	public void setStaffFirstname(String staffFirstname){
		this.staffFirstname = staffFirstname;
	}

	public String getStaffFirstname(){
		return staffFirstname;
	}

	public void setPostFileType(String postFileType){
		this.postFileType = postFileType;
	}

	public String getPostFileType(){
		return postFileType;
	}
}
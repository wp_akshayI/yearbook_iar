package com.app.yearbook.model.yearbook;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Divyesh Weapplinse on 09-Apr-18.
 */
public class BookListModelForYearbook implements Serializable {
    private int position;
    private ArrayList<YearbookList> al_books;

    public BookListModelForYearbook() {
    }

    public BookListModelForYearbook(int position, ArrayList<YearbookList> al_books) {
        this.position = position;
        this.al_books = al_books;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public ArrayList<YearbookList> getAl_books() {
        return al_books;
    }

    public void setAl_books(ArrayList<YearbookList> al_books) {
        this.al_books = al_books;
    }
}

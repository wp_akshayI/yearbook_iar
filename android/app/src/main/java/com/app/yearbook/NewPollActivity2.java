package com.app.yearbook;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.Toast;

import com.app.yearbook.adapter.AdapterGrades;
import com.app.yearbook.databinding.ActivityNewPollPost2Binding;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.GradeListItem;
import com.app.yearbook.model.allpost.PollPost;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.utils.Constants;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NewPollActivity2 extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener,
        DatePickerDialog.OnCancelListener {

    private static final String TAG = "NewPollActivity2";
    ActivityNewPollPost2Binding binding;
    AdapterGrades adapterGrades;
    List<GradeListItem> gradeListItems;
    DatePickerDialog datePickerDialog;
    PollPost model;
    PostListItem postListItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_poll_post2);
        model = getIntent().getParcelableExtra(Constants.POST_POll);
        binding.setModel(model);

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        setToolbar();
        if (getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false)){
            postListItem = getIntent().getParcelableExtra(Constants.INTENT_DATA);
            binding.appbar.toolbarTitle.setText(getString(R.string.edit_poll));

            model.setLastDate(postListItem.getPostExpiryDate());
            try {
                Date oneWayTripDate = Constants.dbDateFormat.parse(model.getLastDate());
                binding.edtLastDate.setText(Constants.calFormat.format(oneWayTripDate)+"");

                c.setTime(oneWayTripDate);
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (model.getLastDate() != null) {
            binding.edtLastDate.setText(Constants.getDisplayDate(
                    model.getLastDate(),
                    Constants.simpleDateFormat));
        }

        if (getIntent().hasExtra(Constants.GRADE_ITEM)) {
            gradeListItems = new ArrayList<>();
            gradeListItems.addAll(getIntent().<GradeListItem>getParcelableArrayListExtra(Constants.GRADE_ITEM));
            gradeListItems.add(0,new GradeListItem("All","-1",false));
            if (model.getGradeListItem() != null) {
                for (GradeListItem gradeListItem : gradeListItems) {
                    if (gradeListItem.getStudentGradeId().equalsIgnoreCase(model.getGradeListItem().getStudentGradeId())) {
                        gradeListItem.setSelected(true);
                    }
                }
            }
            if (getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false)){
                for (GradeListItem gradeListItem : gradeListItems) {
                    if (gradeListItem.getGrade().equalsIgnoreCase(postListItem.getPostForGrade())) {
                        gradeListItem.setSelected(true);
                        break;
                    }
                }
            }else{
                gradeListItems.get(0).setSelected(true);
            }

            adapterGrades = new AdapterGrades(gradeListItems, new OnRecyclerClick() {

                @Override
                public void onVote(int pos, String pollOptionId) {

                }

                @Override
                public void onClick(int pos, int type) {
                    for (GradeListItem gradeListItem : gradeListItems) {
                        gradeListItem.setSelected(false);
                    }
                    gradeListItems.get(pos).setSelected(true);
                    model.setGradeListItem(gradeListItems.get(pos));
                }
            });
            if (getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false)) {
                adapterGrades.setIsEdit();
            }
            binding.rcGrades.setAdapter(adapterGrades);
        }

        datePickerDialog = new DatePickerDialog(
                this, this, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        long daysPlus = 1000 * 60 * 60 * 24 * 14;
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + daysPlus);
        binding.edtLastDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyBoard(binding.edtLastDate);
                    binding.edtLastDate.clearFocus();
                    datePickerDialog.show();
                }
            }
        });
    }

    private void hideKeyBoard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }

    }

    private void setToolbar() {
        setSupportActionBar(binding.appbar.toolbar);
        binding.appbar.toolbarTitle.setText(getString(R.string.new_poll));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }
    }

    MenuItem next;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_next, menu);
        next=menu.findItem(R.id.action_next);

        if(!binding.edtLastDate.getText().toString().equals(""))
        {
            next.setVisible(true);
        }
        else
        {
            next.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                hideKeyBoard(binding.edtLastDate);
                finish();
                break;
            case R.id.action_next:
                validate();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void validate() {
        Date min, max;   // assume these are set to something
        Date selectDate;

        //add 14 days
        Calendar cal=Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_MONTH, 14);

        if (TextUtils.isEmpty(model.getLastDate())) {
            Toast.makeText(getApplicationContext(),"Please select date",Toast.LENGTH_SHORT).show();
           // binding.edtLastDate.setError("Please select date");
        }
        else if (model.getGradeListItem() == null) {
            Toast.makeText(this, "Please select grade ", Toast.LENGTH_SHORT).show();
        } else {
            try {
                //selected date
                selectDate=Constants.simpleDateFormat.parse(model.getLastDate());

                //current date
                Date todayDate = Calendar.getInstance().getTime();
                String todayString = Constants.simpleDateFormat.format(todayDate);
                min=Constants.simpleDateFormat.parse(todayString);

                //add 14 days
                Date add14Dys = cal.getTime();
                String dayString = Constants.simpleDateFormat.format(add14Dys);
                max=Constants.simpleDateFormat.parse(dayString);

                if(selectDate.compareTo(min) >= 0 && selectDate.compareTo(max) <= 0)
                {
                    Log.d(TAG, "validate: date: ");
                    Intent intent = new Intent(this, PostPreviewActivity.class);
                    intent.putExtra(Constants.INTENT_ISEDIT, getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false));
                    intent.putExtra(Constants.INTENT_DATA, postListItem);
                    intent.putExtra(Constants.POST_TYPE, Constants.POST_POll);
                    intent.putExtra(Constants.POST_POll, model);
                    startActivityForResult(intent, 111);
                }
                else
                {
                    Log.d(TAG, "validate: not date");
                    Toast.makeText(getApplicationContext(),"Maximum 14 days allowed",Toast.LENGTH_SHORT).show();
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Log.d(TAG, "onDateSet: " + year + ":" + (month + 1) + ":" + dayOfMonth);
        String lastDate = year + "-" + (month + 1) + "-" + dayOfMonth;
        model.setLastDate(lastDate);

        try {
            Date oneWayTripDate = Constants.simpleDateFormat.parse(model.getLastDate());
            binding.edtLastDate.setText(Constants.calFormat.format(oneWayTripDate)+"");

            if(!binding.edtLastDate.getText().toString().equals(""))
            {
                next.setVisible(true);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

//        binding.edtLastDate.setText(Constants.getDisplayDate(
//                model.getLastDate(),
//                Constants.simpleDateFormat));
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        Log.d(TAG, "onCancel: ");
    }

    boolean isFinishWithoutSaving;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111 && resultCode == RESULT_OK) {
            isFinishWithoutSaving = true;
            setResult(RESULT_OK);
            finish();
        }

    }

    @Override
    public void finish() {
        if (!isFinishWithoutSaving) {
            Intent intent = new Intent();
            intent.putExtra(Constants.POST_POll, model);
            setResult(RESULT_CANCELED, intent);
        }
        super.finish();
    }
}

import { action } from "../utils";

let initialState = {
    list: undefined,
    yearbookId:  null,
    yearbookDetails: undefined,
}

export default function YearbookReducer(state = initialState, data) {

    switch (data.type) {

        case action.get_yearbook_details:
            return {
                ...state,
                yearbookDetails: data.yearbookDetails
            }

        case action.set_yearbook:
            return {
                ...state,
                yearbookId: data.payload
            }

        case action.yearbook_list:
            return {
                ...state,
                list: data.payload
            }

        case action.INITIAL_STATE:
            return {
                ...initialState
            }

        default:
            break;
    }
    return state;
}
package com.app.yearbook;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.StudentListAdapter;
import com.app.yearbook.model.employee.GetUserSchoolWise;
import com.app.yearbook.model.employee.UserListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ProgressDialog progressDialog;
    private TextView toolbar_title;
    String userId, empId;
    private LoginUser loginUser;
    private AllMethods utils;
    private String schoolName = "", schoolId = "";
    public static ArrayList<UserListItem> userPostItems;
    private static final String TAG = "StudentListActivity";
    public static RecyclerView recyclerViewStudent;
    StudentListAdapter schoolAdapter;
    LinearLayout lvNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);

        setView();
    }

    public void setView() {
        //set toolbar
        setToolbar();

        utils = new AllMethods();
        userPostItems=new ArrayList<>();
        lvNoData=findViewById(R.id.lvNoData);
        recyclerViewStudent=findViewById(R.id.recyclerViewStudent);

        //SP object
        loginUser = new LoginUser(StudentListActivity.this);
        if (loginUser.getUserData() != null) {
            empId = loginUser.getUserData().getEmployeeId();
            Log.d("TTT", "setView: emp id: " + empId);
        }

        if (getIntent().getExtras() != null) {
            schoolName = getIntent().getStringExtra("schoolName");
            schoolId = getIntent().getStringExtra("schoolId");
            Log.d(TAG, "setView: SID: " + schoolId);
            //get all students
            getSchoolUser(schoolId);
        }

    }

    private void getSchoolUser(String schoolId) {
        progressDialog = ProgressDialog.show(StudentListActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetUserSchoolWise> schoolList = retrofitClass.getUserSchool(schoolId);
        schoolList.enqueue(new Callback<GetUserSchoolWise>() {
            @Override
            public void onResponse(Call<GetUserSchoolWise> call, Response<GetUserSchoolWise> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Log.d(TAG, "onResponse: " + response.toString());
                if (response.body().getResponseCode().equals("1")) {

                    if (response.body().getUserList() != null && response.body().getUserList().size() != 0) {
                        lvNoData.setVisibility(View.GONE);
                        recyclerViewStudent.setVisibility(View.VISIBLE);

                        userPostItems.clear();
                        userPostItems.addAll(response.body().getUserList());

                        schoolAdapter = new StudentListAdapter(userPostItems, StudentListActivity.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerViewStudent.setLayoutManager(mLayoutManager);
                        recyclerViewStudent.setAdapter(schoolAdapter);
                        recyclerViewStudent.getAdapter().notifyDataSetChanged();

                    } else {
                        utils.setAlert(StudentListActivity.this, "", response.body().getResponseMsg());
                        lvNoData.setVisibility(View.VISIBLE);
                        recyclerViewStudent.setVisibility(View.GONE);
                    }
                } else {
                    utils.setAlert(StudentListActivity.this, "", response.body().getResponseMsg());
                    lvNoData.setVisibility(View.VISIBLE);
                    recyclerViewStudent.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<GetUserSchoolWise> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                utils.setAlert(StudentListActivity.this, "", t.getMessage());
                lvNoData.setVisibility(View.VISIBLE);
                recyclerViewStudent.setVisibility(View.GONE);
            }
        });


    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Students");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchview, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        androidx.appcompat.widget.SearchView searchView = (androidx.appcompat.widget.SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));

        // listening to search query text change
        searchView.setOnQueryTextListener(new androidx.appcompat.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                if(userPostItems.size()!=0)
                    schoolAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                if(userPostItems.size()!=0)
                    schoolAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }
}

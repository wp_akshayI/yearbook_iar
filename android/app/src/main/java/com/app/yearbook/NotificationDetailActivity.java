package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.yearbook.model.studenthome.home.PostListItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.ybq.android.spinkit.style.Circle;
import com.volokh.danylo.hashtaghelper.HashTagHelper;
import com.app.yearbook.databinding.ActivityNotificationDetailBinding;
import com.app.yearbook.fragment.HomeFragment;
import com.app.yearbook.model.studenthome.AddBookMark;
import com.app.yearbook.model.studenthome.GetUserPost;
import com.app.yearbook.model.studenthome.LikeUnlike;
import com.app.yearbook.model.staff.yearbook.UserTagg;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.VideoInfo;

public class NotificationDetailActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private PostListItem postData;
    private HashTagHelper mEditTextHashTagHelper;
    private AllMethods allMethods;
    ActivityNotificationDetailBinding binding;
    private LoginUser loginUser;
    private String getUserId,getPostId;
    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification_detail);
        //setContentView(R.layout.activity_notification_detail);

        setView();
    }

    public void setView()
    {
        //toolbar
        setToolbar();

        //common method
        allMethods=new AllMethods();

        //SP object
        loginUser = new LoginUser(NotificationDetailActivity.this);
        if (loginUser.getUserData() != null) {
            getUserId = loginUser.getUserData().getUserId();
        }

        //intent
        if(getIntent().getExtras()!=null)
        {
            getPostId=getIntent().getStringExtra("postID");
        }

        getPost(getUserId,getPostId);
    }

    public void getPost(String getUserId,String getPostId)
    {
        progressDialog = ProgressDialog.show(NotificationDetailActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);


        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetUserPost> userPost = retrofitClass.getUserPost(Integer.parseInt(getUserId),Integer.parseInt(getPostId));
        userPost.enqueue(new Callback<GetUserPost>() {
            @Override
            public void onResponse(Call<GetUserPost> call, Response<GetUserPost> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if(response.body().getResponseCode().equals("1")) {
                    postData = response.body().getPostDetail().get(0);
                    setData();
                }
                else if(response.body().getResponseCode().equals("10")) {
                    Toast.makeText(NotificationDetailActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(NotificationDetailActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(NotificationDetailActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    Toast.makeText(NotificationDetailActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GetUserPost> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(NotificationDetailActivity.this,"",""+t.getMessage());
            }
        });
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Post");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setData()
    {
        binding.setPList(postData);
        //set img
        if (postData.getStaffImage() != null) {
            Glide.with(getApplicationContext())
                    .load(postData.getStaffImage())
                    .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.mipmap.post_place_holder)
                    .error(R.mipmap.post_place_holder)
                    .into(binding.imgUserImage);

            binding.imgUserImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(NotificationDetailActivity.this, ImageActivity.class);
                    i.putExtra("FileUrl", postData.getStaffImage());
                    startActivity(i);
                }
            });
        }

        //set post img or video
        final String uri = postData.getPostFile();
        if (uri != null && !uri.equals("")) {
            String extension = uri.substring(uri.lastIndexOf("."));
            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {
                binding.imgUserPost.setVisibility(View.VISIBLE);
                binding.videoUserPost.setVisibility(View.GONE);
                if (postData.getPostFile() != null) {
                    Glide.with(getApplicationContext())
                            .load(postData.getPostFile())
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(binding.imgUserPost);
                }
            } else {
                binding.imgUserPost.setVisibility(View.GONE);
                binding.videoUserPost.setVisibility(View.VISIBLE);

                //binding.videoUserPost.reset();
                //binding.videoUserPost.setSource(Uri.parse(postData.getPostFile()));

                if (binding.videoUserPost.getCoverView() != null) {
                    Glide.with(NotificationDetailActivity.this)
                            .load(binding.getPList().getPostThumbnail())
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(binding.videoUserPost.getCoverView());
                }

                binding.videoUserPost.getVideoInfo().setPortraitWhenFullScreen(false);
                binding.videoUserPost.setVideoPath(binding.getPList().getPostFile());
                binding.videoUserPost.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player

            }
        }
        //description
        mEditTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.hashtag), null);
        mEditTextHashTagHelper.handle(binding.tvPostDescription);

        //like-unlike:
        Log.d("TTT", "Like dislike val: " + postData.getPostLike());
        if (postData.getPostLike().equalsIgnoreCase("true")) {
            binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
        } else {
            binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
        }

        //tvtotal like
        binding.tvTotalLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NotificationDetailActivity.this, GetAllStudentLikeActivity.class);
                i.putExtra("postId", binding.getPList().getPostId());
                i.putExtra("Type", "student");
                startActivity(i);
            }
        });
        binding.imgLikeDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TTT", "LikeDisLike: " + getUserId + " / " + postData.getPostId());

                if (binding.getPList().getPostLike().equalsIgnoreCase("true")) {
                    YoYo.with(Techniques.RubberBand)
                            .duration(500)
                            .playOn(binding.imgLikeDislike);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
                        }
                    },200);

                } else if (binding.getPList().getPostLike().equalsIgnoreCase("false")) {

                    YoYo.with(Techniques.RubberBand)
                            .duration(500)
                            .playOn(binding.imgLikeDislike);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
                        }
                    },200);
                }

                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                Call<LikeUnlike> userPost = retrofitClass.postLikeDislike(Integer.parseInt(getUserId), Integer.parseInt(postData.getPostId()), LoginUser.getUserTypeKey());
                userPost.enqueue(new Callback<LikeUnlike>() {
                    @Override
                    public void onResponse(Call<LikeUnlike> call, Response<LikeUnlike> response) {
                        if (response.body().getResponseCode().equals("1")) {
                            if (response.body().getPostDetail().get(0).getPostLike().equalsIgnoreCase("true")) {
                                binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
                            } else if (response.body().getPostDetail().get(0).getPostLike().equalsIgnoreCase("false")) {
                                binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
                            }
                            postData.setTotalLike(response.body().getPostDetail().get(0).getTotalLike());
                            postData.setPostLike(response.body().getPostDetail().get(0).getPostLike());

//                            if(ProfileFragment.getUserList!=null) {
//                                for (int i = 0; i < ProfileFragment.getUserList.size(); i++) {
//                                    if (ProfileFragment.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
//                                        PostListItem newList = response.body().getPostDetail().get(0);
//                                        ProfileFragment.getUserList.get(i).setPostLike(newList.getPostLike());
//                                        ProfileFragment.getUserList.get(i).setTotalLike(newList.getTotalLike());
//                                    }
//                                }
//                            }

                            if(StudentBookmarkActivity.bookmarkList!=null) {
                                for (int i = 0; i < StudentBookmarkActivity.bookmarkList.size(); i++) {
                                    if (StudentBookmarkActivity.bookmarkList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                        PostListItem newList = response.body().getPostDetail().get(0);
                                        StudentBookmarkActivity.bookmarkList.get(i).setPostLike(newList.getPostLike());
                                        StudentBookmarkActivity.bookmarkList.get(i).setTotalLike(newList.getTotalLike());
                                    }
                                }
                            }

                            if(HomeFragment.PostListArray!=null) {
                                for (int i = 0; i < HomeFragment.PostListArray.size(); i++) {
                                    if (HomeFragment.PostListArray.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                        PostListItem newList = response.body().getPostDetail().get(0);
//                                        HomeFragment.PostListArray.get(i).setPostLike(newList.getPostLike());
                                        HomeFragment.PostListArray.get(i).setTotalLike(newList.getTotalLike());
                                    }
                                }
                            }

//                            if(UserDetailActivity.getUserList!=null) {
//                                for (int i = 0; i < UserDetailActivity.getUserList.size(); i++) {
//                                    if (UserDetailActivity.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
//                                        PostListItem newList = response.body().getPostDetail().get(0);
//                                        UserDetailActivity.getUserList.get(i).setPostLike(newList.getPostLike());
//                                        UserDetailActivity.getUserList.get(i).setTotalLike(newList.getTotalLike());
//                                    }
//                                }
//                            }

                        }
                        else if(response.body().getResponseCode().equals("10"))
                        {
                            Toast.makeText(NotificationDetailActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                            LoginUser loginSP = new LoginUser(NotificationDetailActivity.this);
                            loginSP.clearData();

                            Intent i = new Intent(NotificationDetailActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                        else {
                            allMethods.setAlert(NotificationDetailActivity.this, "", response.body().getResponseMsg() + "");
                        }
                    }

                    @Override
                    public void onFailure(Call<LikeUnlike> call, Throwable t) {
                        Log.d("TTT", "Failure: " + t.getMessage());
                        allMethods.setAlert(NotificationDetailActivity.this, "", t.getMessage() + "");
                    }
                });
            }
        });

        //bookmark
        Log.d("TTT", "Bookmark: " + postData.getPostBookmark());
        if (postData.getPostBookmark().equalsIgnoreCase("true")) {
            binding.imgBookmark.setImageResource(R.mipmap.bookmark_selected_ic);
        } else {
            binding.imgBookmark.setImageResource(R.mipmap.bookmark_ic);
        }

        binding.imgBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                Call<AddBookMark> bookmark = retrofitClass.addBookmark(Integer.parseInt(getUserId), Integer.parseInt(postData.getPostId()));
                bookmark.enqueue(new Callback<AddBookMark>() {
                    @Override
                    public void onResponse(Call<AddBookMark> call, Response<AddBookMark> response) {
                        if (response.body().getResponseCode().equals("1")) {
                            if (response.body().getPostDetail().get(0).getPostBookmark().equalsIgnoreCase("true")) {
                                binding.imgBookmark.setImageResource(R.mipmap.bookmark_selected_ic);
                            } else if (response.body().getPostDetail().get(0).getPostBookmark().equalsIgnoreCase("false")) {
                                binding.imgBookmark.setImageResource(R.mipmap.bookmark_ic);
                            }
                            postData.setPostBookmark(response.body().getPostDetail().get(0).getPostBookmark());

//                            if(ProfileFragment.getUserList!=null) {
//                                for (int i = 0; i < ProfileFragment.getUserList.size(); i++) {
//                                    if (ProfileFragment.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
//                                        PostListItem newList = response.body().getPostDetail().get(0);
//                                        ProfileFragment.getUserList.get(i).setPostBookmark(newList.getPostBookmark());
//                                    }
//                                }
//                            }

                            if(StudentBookmarkActivity.bookmarkList!=null) {
                                for (int i = 0; i < StudentBookmarkActivity.bookmarkList.size(); i++) {
                                    if (StudentBookmarkActivity.bookmarkList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                        PostListItem newList = response.body().getPostDetail().get(0);
                                        StudentBookmarkActivity.bookmarkList.get(i).setPostBookmark(newList.getPostBookmark());
                                    }
                                }
                            }

//                            if(UserDetailActivity.getUserList!=null) {
//                                for (int i = 0; i < UserDetailActivity.getUserList.size(); i++) {
//                                    if (UserDetailActivity.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
//                                        PostListItem newList = response.body().getPostDetail().get(0);
//                                        UserDetailActivity.getUserList.get(i).setPostBookmark(newList.getPostBookmark());
//                                    }
//                                }
//                            }

                            if(HomeFragment.PostListArray!=null) {
                                for (int i = 0; i < HomeFragment.PostListArray.size(); i++) {
                                    if (HomeFragment.PostListArray.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                        PostListItem newList = response.body().getPostDetail().get(0);
                                        HomeFragment.PostListArray.get(i).setPostBookmark(newList.getPostBookmark());
                                    }
                                }
                            }



                        }
                        else if(response.body().getResponseCode().equals("10"))
                        {
                            Toast.makeText(NotificationDetailActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                            LoginUser loginSP = new LoginUser(NotificationDetailActivity.this);
                            loginSP.clearData();

                            Intent i = new Intent(NotificationDetailActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                        else {
                            allMethods.setAlert(NotificationDetailActivity.this, "", response.body().getResponseMsg() + "");
                        }
                    }

                    @Override
                    public void onFailure(Call<AddBookMark> call, Throwable t) {
                        allMethods.setAlert(NotificationDetailActivity.this, "", t.getMessage() + "");
                    }
                });
            }
        });

        //date
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat output ;

        try {
            String date=binding.getPList().getPostDate();
            if(date.endsWith("1") && !date.endsWith("11"))
            {
                output = new SimpleDateFormat("MMMM d'st', yyyy");
            }
            else if(date.endsWith("2") && !date.endsWith("12"))
            {
                output = new SimpleDateFormat("MMMM d'nd', yyyy");
            }
            else if(date.endsWith("3") && !date.endsWith("13"))
            {
                output = new SimpleDateFormat("MMMM d'rd', yyyy");
            }
            else
            {
                output = new SimpleDateFormat("MMMM d'th', yyyy");
            }

            Date oneWayTripDate = input.parse(date);                 // parse input
            binding.tvDate.setText(output.format(oneWayTripDate));    // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //comment
//        Log.d("TTT","Total comment : "+postData.getTotalComment());
//
//        binding.tvComment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), StudentCommentActivity.class);
//                i.putExtra("comment", "user");
//                i.putExtra("PostId", postData.getPostId());
//                startActivityForResult(i,123);
//            }
//        });
//
//        binding.imgComment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), StudentCommentActivity.class);
//                i.putExtra("comment", "user");
//                i.putExtra("PostId", postData.getPostId());
//                startActivityForResult(i,123);
//            }
//        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TTT","Resulttt: "+requestCode+" / "+resultCode);
        if(resultCode==RESULT_OK) {
            if (requestCode == 123)
            {
                String comment=data.getStringExtra("Comment");
                Log.d("TTT","cmntttttttt: "+comment);
                postData.setTotalComment(comment);
            }
            else if(requestCode==124)
            {
                String desc=data.getStringExtra("Description");
                postData.setPostDescription(desc);

                if((ArrayList<UserTagg>) data.getSerializableExtra("taggedUser") !=null) {
                    ArrayList<UserTagg> userId = (ArrayList<UserTagg>) data.getSerializableExtra("taggedUser");
                    postData.setUserTagg(userId);
                }

            }
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

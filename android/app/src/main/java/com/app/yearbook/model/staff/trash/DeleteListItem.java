package com.app.yearbook.model.staff.trash;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.google.gson.annotations.SerializedName;
import com.app.yearbook.BR;

public class DeleteListItem extends BaseObservable {

    @SerializedName("staff_image")
    private String staffImage;

    @SerializedName("total_repoted")
    private String totalRepoted;

    @SerializedName("flag")
    private String flag;

    @SerializedName("staff_lastname")
    private String staffLastname;

    @SerializedName("post_file")
    private String postFile;

    @SerializedName("total_comment")
    private String totalComment;

    @SerializedName("post_description")
    private String postDescription;

    @SerializedName("staff_firstname")
    private String staffFirstname;

    @SerializedName("post_file_type")
    private String postFileType;

    @SerializedName("post_id")
    private String postId;

    @SerializedName("school_id")
    private String schoolId;

    @SerializedName("post_date")
    private String postDate;

    @SerializedName("staff_id")
    private String staffId;

    @SerializedName("post_thumbnail")
    private String postThumbnail;

    @SerializedName("post_staff_firstname")
    private String postStaffFirstname;

    @SerializedName("post_staff_lastname")
    private String postStaffLastname;

    @SerializedName("post_type")
    private String postType;

    @SerializedName("deleted_time")
    private String deletedTime;

    public void setStaffImage(String staffImage) {
        this.staffImage = staffImage;
        notifyPropertyChanged(BR.staffImage);

    }

    @Bindable
    public String getStaffImage() {
        return staffImage;
    }

    public void setTotalRepoted(String totalRepoted) {
        this.totalRepoted = totalRepoted;
        notifyPropertyChanged(BR.totalRepoted);
    }

    @Bindable
    public String getTotalRepoted() {
        return totalRepoted;
    }

    public void setFlag(String flag) {
        this.flag = flag;
        notifyPropertyChanged(BR.flag);
    }

    @Bindable
    public String getFlag() {
        return flag;
    }

    public void setStaffLastname(String staffLastname) {
        this.staffLastname = staffLastname;
        notifyPropertyChanged(BR.staffLastname);
    }

    @Bindable
    public String getStaffLastname() {
        return "Deleted by " + staffFirstname + " " + staffLastname;
    }

    public void setPostFile(String postFile) {
        this.postFile = postFile;
        notifyPropertyChanged(BR.postFile);
    }

    @Bindable
    public String getPostFile() {
        return postFile;
    }

    public void setTotalComment(String totalComment) {
        this.totalComment = totalComment;
        notifyPropertyChanged(BR.totalComment);
    }

    @Bindable
    public String getTotalComment() {
        return totalComment;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
        notifyPropertyChanged(BR.postDescription);
    }

    @Bindable
    public String getPostDescription() {
        return postDescription;
    }

    public void setStaffFirstname(String staffFirstname) {
        this.staffFirstname = staffFirstname;
        notifyPropertyChanged(BR.staffFirstname);
    }

    @Bindable
    public String getStaffFirstname() {
        return staffFirstname;
    }

    public void setPostFileType(String postFileType) {
        this.postFileType = postFileType;
        notifyPropertyChanged(BR.postFileType);
    }

    @Bindable
    public String getPostFileType() {
        return postFileType;
    }

    public void setPostId(String postId) {
        this.postId = postId;
        notifyPropertyChanged(BR.postId);
    }

    @Bindable
    public String getPostId() {
        return postId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
        notifyPropertyChanged(BR.schoolId);
    }

    @Bindable
    public String getSchoolId() {
        return schoolId;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
        notifyPropertyChanged(BR.postDate);
    }

    @Bindable
    public String getPostDate() {
        return postDate;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
        notifyPropertyChanged(BR.staffId);
    }

    @Bindable
    public String getStaffId() {
        return staffId;
    }

    public void setPostThumbnail(String postThumbnail) {
        this.postThumbnail = postThumbnail;
        notifyPropertyChanged(BR.postThumbnail);
    }

    @Bindable
    public String getPostThumbnail() {
        return postThumbnail;

    }

    public void setPostStaffFirstname(String postStaffFirstname) {
        this.postStaffFirstname = postStaffFirstname;
        notifyPropertyChanged(BR.postStaffFirstname);
    }

    @Bindable
    public String getPostStaffFirstname() {
        return postStaffFirstname;
    }

    public void setPostStaffLastname(String postStaffLastname) {
        this.postStaffLastname = postStaffLastname;
        notifyPropertyChanged(BR.postStaffLastname);
    }

    @Bindable
    public String getPostStaffLastname() {
        return postStaffFirstname + "  " + postStaffLastname;
    }

    public void setPostType(String postType) {
        this.postType = postType;
        notifyPropertyChanged(BR.postType);
    }

    @Bindable
    public String getPostType() {
        return postType;
    }

    public void setDeletedTime(String deletedTime) {
        this.deletedTime = deletedTime;
        notifyPropertyChanged(BR.deletedTime);
    }

    @Bindable
    public String getDeletedTime() {
        return deletedTime;
    }
}
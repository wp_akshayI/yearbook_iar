import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Image, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native'
// import { useDispatch } from 'react-redux'
import { global, colors } from '../utils'

// import Actions from '../../actions'
import { image_placeholder } from '../assets'
import ImageLightBox from '../components/ImageLightBox'

export default function PortraitColumns ({ data }) {
  // const dispatch = useDispatch()
  const [state, setState] = useState({
    data: undefined,
    isLightbox: false,
    lightbox_uri: '',
    lightbox_title: ''
  })

  useEffect(() => {
    const users = [...data.staff_list, ...data.student_list]
    setState({ ...state, data: users })
    // _get_userslist()
  }, [data.student_list, data.staff_list])

  // const _get_userslist = async () => {
  //   const { student_list, staff_list } = data
  //   const res = await dispatch(Actions.get_users_info({ student_list, staff_list }))
  //   setState({ ...state, data: res })
  // }

  const renderItem = ({ item, index }) => {
    const column = Number(data.grid_row)
    const uri = item.image
    const title = item.firstname + ' ' + item.lastname;
    return (
      <TouchableOpacity
        style={[styles.list, { flex: 1 / column }]}
        onPress={() => setState({ ...state, isLightbox: true, lightbox_uri: uri, lightbox_title: title })}
      >
        <View style={styles.listHeader}>
          <Text
            numberOfLines={1}
            style={styles.listTitle}
          >
            {data.component_title}
          </Text>
        </View>
        <ImagePlaceholder
          source={uri ? { uri } : image_placeholder}
          column={column}
        />
        <View style={styles.listHeader}>
          <Text
            numberOfLines={1}
            style={styles.listTitle}
          >
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }

  const renderSingleItem = ({ item }) => {
    const uri = item.image
    return (
      <TouchableOpacity
        style={styles.list}
        onPress={() => setState({ ...state, isLightbox: true, lightbox_uri: uri })}
        activeOpacity={1}
      >
        <View style={styles.listHeader}>
          <Text
            numberOfLines={1}
            style={styles.listTitle}
            adjustsFontSizeToFit
          >
            {data.component_title}
          </Text>
        </View>
        <Image
          resizeMode='cover'
          style={{
            width: global.width,
            height: global.width
          }}
          source={uri ? { uri } : image_placeholder}
        />
        <View style={styles.listHeader}>
          <Text
            numberOfLines={1}
            style={styles.listTitle}
            adjustsFontSizeToFit
          >
            {item.firstname} {item.lastname}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }

  return (
    <View>
      <Text style={styles.columnTitle}>
        {data.component_title}
      </Text>
      <View style={styles.container}>
        {
          Number(data.grid_row) === 1 && (
            <FlatList
              data={state.data || []}
              keyExtractor={(item, index) => String(index)}
              renderItem={renderSingleItem}
              key={String(data.grid_row)}
            />
          )
        }
        {
          Number(data.grid_row) === 2 && (
            <FlatList
              data={state.data || []}
              numColumns={2}
              keyExtractor={(item, index) => String(index)}
              renderItem={renderItem}
              key={String(data.grid_row)}
            />
          )
        }
        {
          Number(data.grid_row) === 3 && (
            <FlatList
              data={state.data || []}
              numColumns={3}
              keyExtractor={(item, index) => String(index)}
              renderItem={renderItem}
              key={String(data.grid_row)}
            />
          )
        }
      </View>
      <ImageLightBox
        isVisible={state.isLightbox}
        handleClose={() => setState({ ...state, isLightbox: false })}
        uri={state.lightbox_uri}
        title={state.lightbox_title}
      />
    </View>
  )
}

function ImagePlaceholder ({ column, ...props }) {
  const [state, setState] = useState(true)
  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        width: (global.width / column) - 4,
        height: (global.width / column) - 4
      }}
    >
      <Image
        style={{
          resizeMode: 'cover',
          height: '100%',
          width: '100%'
        }}
        onLoad={() => setState(false)}
        {...props}
      />
      {
        state && <ActivityIndicator color={colors.white} style={{ position: 'absolute' }} />
      }
    </View>
  )
}

const styles = StyleSheet.create({
  columnTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: 'Calibri',
    padding: 10,
    color: '#000'
  },
  container: {
    backgroundColor: '#bbbaba',
    paddingVertical: 15
  },
  list: {
    margin: 2
  },
  listHeader: {
    backgroundColor: '#fff',
    paddingHorizontal: 8,
    alignItems: 'center',
    justifyContent: 'center',
    height: 25
  },
  listTitle: {
    color: '#000',
    fontSize: 12,
    textAlign: 'center'
  }
})

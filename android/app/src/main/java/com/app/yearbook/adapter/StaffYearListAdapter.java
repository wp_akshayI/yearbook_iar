package com.app.yearbook.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.yearbook.R;
import com.app.yearbook.StaffCategoryActivity;
import com.app.yearbook.model.yearbook.BookListModelForYearbook;
import com.app.yearbook.model.yearbook.YearbookList;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

public class StaffYearListAdapter extends RecyclerView.Adapter<StaffYearListAdapter.MyViewHolder> {
    private ArrayList<BookListModelForYearbook> yearbookLists;
    private Activity ctx;

    @NonNull
    @Override
    public StaffYearListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_bookrowitem, parent, false);
//item_stafftyear_list
        return new MyViewHolder(itemView);
    }

    public StaffYearListAdapter(ArrayList<BookListModelForYearbook> yearbookLists, Activity context) {
        this.yearbookLists = yearbookLists;
        this.ctx=context;
    }


    @Override
    public void onBindViewHolder(@NonNull final StaffYearListAdapter.MyViewHolder holder, final int position) {

       //final BookListModelForYearbook userList=yearbookLists.get(position);
        holder.bindData(yearbookLists.get(position),position);
//        holder.tvYear.setText(userList.getYearbookName()+"");
//        holder.tvSchoolName.setText(userList.getSchoolName());
//
//        //post img
//        if(userList.getYearbookCover()!= null || !userList.getYearbookCover().equals("")) {
//            if (userList.getYearbookCover() != null) {
//                Glide.with(ctx)
//                        .load(userList.getYearbookCover())
//                        .asBitmap()
//                        .placeholder(R.mipmap.home_post_img_placeholder)
//                        .error(R.mipmap.home_post_img_placeholder)
//                        .into(holder.imgCoverPic);
//            }
//        }

//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Log.d("TTT","Yearbook: "+userList.getYearbookId()+" // "+userList.getYearbookName());
//                    Intent i = new Intent(ctx, StaffCategoryActivity.class);
//                    i.putExtra("yearBookId", userList.getYearbookId());
//                    i.putExtra("year", userList.getYearbookName());
//                    ctx.startActivity(i);
//                }
//            });

    }

    @Override
    public int getItemCount() {
        return yearbookLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout rl_rowbook_size;
        public LinearLayout ll_row_book_lists;
        public View view_data;
        public ImageView iv_row_bookbg;

//        public TextView tvYear,tvSchoolName;
//        public ImageView imgCoverPic;

        public MyViewHolder(View view) {
            super(view);
//            tvYear = view.findViewById(R.id.tvYear);
//            tvSchoolName = view.findViewById(R.id.tvSchoolName);
            iv_row_bookbg=view.findViewById(R.id.iv_row_bookbg);
            ll_row_book_lists = itemView.findViewById(R.id.ll_row_book_lists);
            rl_rowbook_size =itemView.findViewById(R.id.rl_rowbook_size);
            view_data = itemView;
        }

        void bindData(final BookListModelForYearbook bookListModel, int pos) {
            try {
                rl_rowbook_size.getLayoutParams().height = (int) (AllMethods.getDisplayHeight(ctx) * 0.28);
                Log.e("TTT", "Book size: " + yearbookLists.size());
                ll_row_book_lists.removeAllViews();
                for (int j = 0; j < bookListModel.getAl_books().size(); j++) {

                    final YearbookList bookItemPOJO = bookListModel.getAl_books().get(j);
                    View view = ctx.getLayoutInflater().inflate(R.layout.row_bookitem, null, false);

                    ImageView iv_row_bookimage = view.findViewById(R.id.iv_row_bookimage);
                    TextView tv_book_title = view.findViewById(R.id.tv_book_title);
                    tv_book_title.setText(bookItemPOJO.getYearbookName());
                    iv_row_bookimage.getLayoutParams().width = (int) (AllMethods.getDisplayWidth(ctx) * 0.28);
                    iv_row_bookimage.getLayoutParams().height = (int) (AllMethods.getDisplayHeight(ctx) * 0.24);
                    Glide.with(ctx)
                            .load(bookItemPOJO.getYearbookCover())
                            .centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder_white)
                            .error(R.drawable.placeholder_white)
                            .into(iv_row_bookimage);

                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d("TTT","Yearbook: "+bookItemPOJO.getYearbookId()+" // "+bookItemPOJO.getYearbookName());
                    Intent i = new Intent(ctx, StaffCategoryActivity.class);
                    i.putExtra("yearBookId", bookItemPOJO.getYearbookId());
                    i.putExtra("year", bookItemPOJO.getYearbookName());
                    ctx.startActivity(i);
                        }
                    });
                    ll_row_book_lists.addView(view);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}

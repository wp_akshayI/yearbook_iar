
package com.app.yearbook.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserSignup {

    @SerializedName("ResponseCode")
    @Expose
    private String responseCode;
    @SerializedName(value = "ResponseMsg", alternate = {"message"})
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private String result;
    @SerializedName("Data")
    @Expose
    private StaffStudent data;

    @SerializedName("status")
    private String status;

    @SerializedName("user_id")
    private String userId;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public StaffStudent getData() {
        return data;
    }

    public void setData(StaffStudent data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

package com.app.yearbook.model.staff.trash;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetDeleteList{

	@SerializedName("ResponseCode")
	private String responseCode;

	@SerializedName("ResponseMsg")
	private String responseMsg;

	@SerializedName("delete_list")
	private List<DeleteListItem> deleteList;

	@SerializedName("Result")
	private String result;

	public void setResponseCode(String responseCode){
		this.responseCode = responseCode;
	}

	public String getResponseCode(){
		return responseCode;
	}

	public void setResponseMsg(String responseMsg){
		this.responseMsg = responseMsg;
	}

	public String getResponseMsg(){
		return responseMsg;
	}

	public void setDeleteList(List<DeleteListItem> deleteList){
		this.deleteList = deleteList;
	}

	public List<DeleteListItem> getDeleteList(){
		return deleteList;
	}

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}
}
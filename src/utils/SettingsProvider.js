import React, { Component } from 'react'
import SnackMessage from '../components/SnackMessage'
import axios from 'axios'
import { global } from '.'

axios.defaults.baseURL = global.api
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

class SettingsProvider extends Component {
  render () {
    return (
      <>
        {this.props.children}
        <SnackMessage />
      </>
    )
  }
}

export default SettingsProvider

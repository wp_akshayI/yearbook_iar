package com.app.yearbook.utils;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.provider.FontRequest;
import androidx.emoji.text.EmojiCompat;
import androidx.emoji.text.FontRequestEmojiCompatConfig;
import androidx.multidex.MultiDex;

import com.app.yearbook.R;

public class CustomFontApp extends Application {

    private static final String TAG = "CustomFontApp";
    public CustomFontApp() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FontRequest fontRequest = new FontRequest(
                "com.example.fontprovider",
                "com.example",
                "emoji compat Font Query",
                R.array.com_google_android_gms_fonts_certs);
//        EmojiCompat.Config config = new FontRequestEmojiCompatConfig(getApplicationContext(), fontRequest);
//        EmojiCompat.init(config);

        EmojiCompat.init(new FontRequestEmojiCompatConfig(getApplicationContext(),fontRequest).setReplaceAll(true).registerInitCallback(new EmojiCompat.InitCallback() {
                    @Override
                    public void onInitialized() {
                        super.onInitialized();
                        Log.d(TAG, "EmojiCompat onInitialized: ");
                    }

                    @Override
                    public void onFailed(@Nullable Throwable throwable) {
                        super.onFailed(throwable);
                        Log.d(TAG, "EmojiCompat initialization onFailed: ");
                    }
                }));

        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/AvenirLTStd-Medium.otf");
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}

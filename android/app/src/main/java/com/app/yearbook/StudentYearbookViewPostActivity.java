package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.yearbook.model.studenthome.home.PostListItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.TaggStudentUserListAdapter;
import com.app.yearbook.databinding.ActivityStudentYearbookViewPostBinding;
import com.app.yearbook.model.studenthome.AddBookMark;
import com.app.yearbook.model.studenthome.LikeUnlike;
import com.app.yearbook.model.studenthome.OnOffNotification;
import com.app.yearbook.model.studenthome.PostList;
import com.app.yearbook.model.staff.yearbook.GetStudentYearbook;
import com.app.yearbook.model.staff.yearbook.StaffYBPostList;
import com.app.yearbook.model.staff.yearbook.UserTagg;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.PlayerManager;
import tcking.github.com.giraffeplayer2.VideoInfo;

public class StudentYearbookViewPostActivity extends AppCompatActivity {

    private StaffYBPostList postListData;
    private Toolbar toolbar;
    private AllMethods allMethods;
    private int IntentData = 678;
    private String userId, postId;
    public ProgressDialog progressDialog;
    private LoginUser loginUser;
    private ArrayList<UserTagg> tagUserListArray;
    private ActivityStudentYearbookViewPostBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_student_yearbook_view_post);
        setView();
    }

    public void setView() {

        //SP object
        loginUser = new LoginUser(getApplicationContext());
        if (loginUser.getUserData() != null) {
            userId = loginUser.getUserData().getUserId();
            Log.d("TTT", "User Id: " + userId);
        }

        //all methods object
        allMethods = new AllMethods();

        //set toolbar
        setToolbar();

        //get intent
        if (getIntent().getExtras() != null) {
            //postList = (StaffYBPostList) getIntent().getSerializableExtra("StudentYBPostList");
            postId = getIntent().getStringExtra("PostId");
            getStudentYearbook();
        }
    }

    public void setData() {

        //post img
        if (binding.getPost().getPostFile() != null || !binding.getPost().getPostFile().equals("")) {
            final String uri = binding.getPost().getPostFile();
            Log.d("TTT", "Url : " + uri);
            if (uri != null && !uri.equals("")) {
                String extension = uri.substring(uri.lastIndexOf("."));
                Log.d("TTT", "Url type: " + extension);

                if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {
                    binding.imgUserPost.setVisibility(View.VISIBLE);
                    binding.videoUserPost.setVisibility(View.GONE);
                    if (binding.getPost().getPostFile() != null) {
                        Glide.with(this)
                                .load(binding.getPost().getPostFile())
                                .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                        binding.imgUserPost.setImageBitmap(resource);
                                    }
                                });
                    }
                } else {
                    binding.imgUserPost.setVisibility(View.GONE);
                    binding.videoUserPost.setVisibility(View.VISIBLE);

//                binding.videoUserPost.reset();
//                binding.videoUserPost.setSource(Uri.parse(binding.getPost().getPostFile()));

                    if (binding.videoUserPost.getCoverView() != null) {
                        Glide.with(StudentYearbookViewPostActivity.this)
                                .load(binding.getPost().getPostThumbnail())
                                .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(binding.videoUserPost.getCoverView());
                    }

                    binding.videoUserPost.getVideoInfo().setPortraitWhenFullScreen(false);
                    binding.videoUserPost.setVideoPath(binding.getPost().getPostFile());
                    binding.videoUserPost.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player

                    Log.d("TTT", "Video urllllll: " + binding.getPost().getPostFile());
                }
            }
            //get tag user
            if (binding.getPost().getUserTagg().size() > 0) {
                binding.lvTaggPpl.setVisibility(View.VISIBLE);

                for (int i = 0; i < binding.getPost().getUserTagg().size(); i++) {
                    Log.d("TTT", "Id : " + binding.getPost().getUserTagg().get(i).getUserId() + " / " + userId);
                    if (binding.getPost().getUserTagg().get(i).getUserId().equals(userId)) {
                        binding.lvOption.setVisibility(View.VISIBLE);
                        binding.imgOption.setVisibility(View.VISIBLE);
                        binding.imgOption.setOnClickListener(optionClick);
                    }
                }

                //set recycler view
                tagUserListArray = new ArrayList<>();
                tagUserListArray.addAll(binding.getPost().getUserTagg());
                TaggStudentUserListAdapter userListAdapter = new TaggStudentUserListAdapter(tagUserListArray, StudentYearbookViewPostActivity.this, "studentyearbook");
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(StudentYearbookViewPostActivity.this, LinearLayoutManager.HORIZONTAL, false);
                binding.rvTaggUserList.setLayoutManager(mLayoutManager);
                binding.rvTaggUserList.setAdapter(userListAdapter);
            }

            binding.tvTotalLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("TTT", "post id adapter: " + binding.getPost().getPostId());
                    Intent i = new Intent(StudentYearbookViewPostActivity.this, GetAllStudentLikeActivity.class);
                    i.putExtra("postId", binding.getPost().getPostId());
                    i.putExtra("Type", "yearbook");
                    startActivity(i);
                }
            });

            //image touch
            binding.imgUserPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), ImageActivity.class);
                    i.putExtra("caption",binding.getPost().getPostDescription());
                    i.putExtra("FileUrl", uri);
                    startActivity(i);
                }
            });
        }

        //like
        if (binding.getPost().getPostLike().equalsIgnoreCase("true")) {
            binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
        } else {
            binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
        }

//        if (binding.getPost().getTotalLike().equals("0") || binding.getPost().getTotalLike().equals("1")) {
////            binding.tvTotalLike.setText(binding.getPost().getTotalLike() + " Like");
////        } else {
////            binding.tvTotalLike.setText(binding.getPost().getTotalLike() + " Likes");
////        }

        binding.imgLikeDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (binding.getPost().getPostLike().equalsIgnoreCase("true")) {
                    YoYo.with(Techniques.RubberBand)
                            .duration(500)
                            .playOn(binding.imgLikeDislike);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
                        }
                    }, 200);

                } else if (binding.getPost().getPostLike().equalsIgnoreCase("false")) {

                    YoYo.with(Techniques.RubberBand)
                            .duration(500)
                            .playOn(binding.imgLikeDislike);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
                        }
                    }, 200);
                }

                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                Call<LikeUnlike> userPost = retrofitClass.postYearbookLikeDislike(Integer.parseInt(userId), Integer.parseInt(binding.getPost().getPostId()));
                userPost.enqueue(new Callback<LikeUnlike>() {
                    @Override
                    public void onResponse(Call<LikeUnlike> call, Response<LikeUnlike> response) {
                        if (response.body().getResponseCode().equals("1")) {
                            if (response.body().getPostDetail().get(0).getPostLike().equalsIgnoreCase("true")) {
                                binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
                            } else if (response.body().getPostDetail().get(0).getPostLike().equalsIgnoreCase("false")) {
                                binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
                            }
                            binding.getPost().setTotalLike(response.body().getPostDetail().get(0).getTotalLike());
                            binding.getPost().setPostLike(response.body().getPostDetail().get(0).getPostLike());

                            if (binding.getPost().getTotalLike().equals("0") || binding.getPost().getTotalLike().equals("1")) {
                                binding.tvTotalLike.setText(binding.getPost().getTotalLike() + " Like");
                            } else {
                                binding.tvTotalLike.setText(binding.getPost().getTotalLike() + " Likes");
                            }

                            if (DisplayStudentYearbookActivity.postLists != null) {
                                for (int i = 0; i < DisplayStudentYearbookActivity.postLists.size(); i++) {
                                    if (DisplayStudentYearbookActivity.postLists.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                        PostListItem newList = response.body().getPostDetail().get(0);
                                        DisplayStudentYearbookActivity.postLists.get(i).setPostLike(newList.getPostLike());
                                        DisplayStudentYearbookActivity.postLists.get(i).setTotalLike(newList.getTotalLike());
                                    }
                                }
                            }

                            if (StudentBookmarkActivity.bookmarkList != null) {
                                for (int i = 0; i < StudentBookmarkActivity.bookmarkList.size(); i++) {
                                    if (StudentBookmarkActivity.bookmarkList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                        PostListItem newList = response.body().getPostDetail().get(0);
                                        StudentBookmarkActivity.bookmarkList.get(i).setPostLike(newList.getPostLike());
                                        StudentBookmarkActivity.bookmarkList.get(i).setTotalLike(newList.getTotalLike());
                                    }
                                }
                            }

                        } else if (response.body().getResponseCode().equals("10")) {
                            Toast.makeText(StudentYearbookViewPostActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                            LoginUser loginSP = new LoginUser(StudentYearbookViewPostActivity.this);
                            loginSP.clearData();

                            Intent i = new Intent(StudentYearbookViewPostActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            allMethods.setAlert(StudentYearbookViewPostActivity.this, "", response.body().getResponseMsg() + "");
                        }
                    }

                    @Override
                    public void onFailure(Call<LikeUnlike> call, Throwable t) {
                        Log.d("TTT", "Failure: " + t.getMessage());
                        allMethods.setAlert(StudentYearbookViewPostActivity.this, "", t.getMessage() + "");
                    }
                });
            }
        });

        //img comment click
        binding.imgComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), StudentCommentActivity.class);
                i.putExtra("PostId", binding.getPost().getPostId());
                i.putExtra("displayComment", "yearbook");
                i.putExtra("comment", "user");
                startActivityForResult(i, IntentData);
            }
        });

        //tv comment click
        binding.tvComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), StudentCommentActivity.class);
                i.putExtra("PostId", binding.getPost().getPostId());
                i.putExtra("displayComment", "yearbook");
                i.putExtra("comment", "user");
                startActivityForResult(i, IntentData);
            }
        });

        //bookmark
        if (binding.getPost().getPostBookmark().equalsIgnoreCase("true")) {
            binding.imgBookmark.setImageResource(R.mipmap.bookmark_selected_ic);
        } else {
            binding.imgBookmark.setImageResource(R.mipmap.bookmark_ic);
        }

        binding.imgBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                Call<AddBookMark> bookmark = retrofitClass.addYearbookBookmark(Integer.parseInt(userId), Integer.parseInt(binding.getPost().getPostId()));
                bookmark.enqueue(new Callback<AddBookMark>() {
                    @Override
                    public void onResponse(Call<AddBookMark> call, Response<AddBookMark> response) {
                        if (response.body().getResponseCode().equals("1")) {
                            if (response.body().getPostDetail().get(0).getPostBookmark().equalsIgnoreCase("true")) {
                                binding.imgBookmark.setImageResource(R.mipmap.bookmark_selected_ic);
                            } else if (response.body().getPostDetail().get(0).getPostBookmark().equalsIgnoreCase("false")) {
                                binding.imgBookmark.setImageResource(R.mipmap.bookmark_ic);
                            }
                            binding.getPost().setPostBookmark(response.body().getPostDetail().get(0).getPostBookmark());

                            if (DisplayStudentYearbookActivity.postLists != null) {
                                for (int i = 0; i < DisplayStudentYearbookActivity.postLists.size(); i++) {
                                    if (DisplayStudentYearbookActivity.postLists.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                        PostListItem newList = response.body().getPostDetail().get(0);
                                        DisplayStudentYearbookActivity.postLists.get(i).setPostBookmark(newList.getPostBookmark());
                                    }
                                }
                            }

                            if (StudentBookmarkActivity.bookmarkList != null) {
                                for (int i = 0; i < StudentBookmarkActivity.bookmarkList.size(); i++) {
                                    if (StudentBookmarkActivity.bookmarkList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                        PostListItem newList = response.body().getPostDetail().get(0);
                                        StudentBookmarkActivity.bookmarkList.get(i).setPostBookmark(newList.getPostBookmark());
                                    }
                                }
                            }

                        } else if (response.body().getResponseCode().equals("10")) {
                            Toast.makeText(StudentYearbookViewPostActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                            LoginUser loginSP = new LoginUser(StudentYearbookViewPostActivity.this);
                            loginSP.clearData();

                            Intent i = new Intent(StudentYearbookViewPostActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            allMethods.setAlert(StudentYearbookViewPostActivity.this, "", response.body().getResponseMsg() + "");
                        }
                    }

                    @Override
                    public void onFailure(Call<AddBookMark> call, Throwable t) {
                        allMethods.setAlert(StudentYearbookViewPostActivity.this, "", t.getMessage() + "");
                    }
                });
            }
        });

        //desc
//        if(postList.getPostDescription()!=null && !postList.getPostDescription().equals(""))
//        {
//            tvPostDescription.setVisibility(View.VISIBLE);
//            tvPostDescription.setText(postList.getPostDescription());
//        }
//        else
//        {
//            tvPostDescription.setVisibility(View.GONE);
//        }
    }

    public View.OnClickListener optionClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("TTT", "Notification: " + binding.getPost().getPostPush() + "/" + binding.getPost().getPostId());

            String notification = binding.getPost().getPostPush();
            PopupMenu popup = new PopupMenu(StudentYearbookViewPostActivity.this, binding.imgOption);
            popup.getMenuInflater().inflate(R.menu.different_student_opt, popup.getMenu());

            final MenuItem report = popup.getMenu().findItem(R.id.report);
            report.setVisible(false);

            if (notification.equalsIgnoreCase("true")) {
                popup.getMenu().findItem(R.id.notification).setTitle("Turn off notification");
            } else {
                popup.getMenu().findItem(R.id.notification).setTitle("Turn on notification");
            }

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getTitle().equals("Turn on notification") || item.getTitle().equals("Turn off notification")) {
                        progressDialog = ProgressDialog.show(StudentYearbookViewPostActivity.this, "", "", true);
                        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        progressDialog.setContentView(R.layout.progress_view);
                        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                        Circle bounce = new Circle();
                        bounce.setColor(Color.BLACK);
                        progressBar.setIndeterminateDrawable(bounce);


                        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                        Call<OnOffNotification> notification = retrofitClass.taggUserNotification(Integer.parseInt(userId), Integer.parseInt(binding.getPost().getPostId()));
                        notification.enqueue(new Callback<OnOffNotification>() {
                            @Override
                            public void onResponse(Call<OnOffNotification> call, Response<OnOffNotification> response) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                                if (response.body().getResponseCode().equals("1")) {

                                    Log.d("TTT", "Notification response: " + response.body().getPostDetail().get(0).getPostPush());
                                    binding.getPost().setPostPush(response.body().getPostDetail().get(0).getPostPush());
                                    if (DisplayStudentYearbookActivity.postLists != null) {
                                        for (int i = 0; i < DisplayStudentYearbookActivity.postLists.size(); i++) {
                                            if (DisplayStudentYearbookActivity.postLists.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                                PostList newList = response.body().getPostDetail().get(0);
                                                DisplayStudentYearbookActivity.postLists.get(i).setPostPush(newList.getPostPush());
                                            }
                                        }
                                    }

                                    Toast.makeText(StudentYearbookViewPostActivity.this, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                } else if (response.body().getResponseCode().equals("10")) {
                                    Toast.makeText(StudentYearbookViewPostActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                    LoginUser loginSP = new LoginUser(StudentYearbookViewPostActivity.this);
                                    loginSP.clearData();

                                    Intent i = new Intent(StudentYearbookViewPostActivity.this, LoginActivity.class);
                                    startActivity(i);
                                    finish();
                                } else {

                                    Toast.makeText(StudentYearbookViewPostActivity.this, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<OnOffNotification> call, Throwable t) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                                Toast.makeText(StudentYearbookViewPostActivity.this, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                    Log.d("TTT", "item: " + item.getItemId());
                    return true;
                }
            });
            popup.show();
        }
    };

    //get yearbook detail
    public void getStudentYearbook() {

        Log.d("TTT", "getStudentYearbook: " + userId + " / " + postId);
        progressDialog = ProgressDialog.show(StudentYearbookViewPostActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetStudentYearbook> userPost = retrofitClass.getStudentYBDetail(Integer.parseInt(userId), Integer.parseInt(postId));
        userPost.enqueue(new Callback<GetStudentYearbook>() {
            @Override
            public void onResponse(Call<GetStudentYearbook> call, Response<GetStudentYearbook> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    postListData = response.body().getPostList().get(0);
                    binding.setPost(postListData);
                    setData();
                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(StudentYearbookViewPostActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StudentYearbookViewPostActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StudentYearbookViewPostActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(StudentYearbookViewPostActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetStudentYearbook> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StudentYearbookViewPostActivity.this, "", "" + t.getMessage());
            }
        });
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Yearbook post");
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == IntentData) {
                String comment = data.getStringExtra("Comment");
                String PostId = data.getStringExtra("PostId");
                String PostComment = data.getStringExtra("PostComment");

                Log.d("TTT", "cmntttttttt: " + comment);
                binding.getPost().setTotalComment(comment);

                if (DisplayStudentYearbookActivity.postLists != null) {
                    for (int i = 0; i < DisplayStudentYearbookActivity.postLists.size(); i++) {
                        if (DisplayStudentYearbookActivity.postLists.get(i).getPostId().equals(PostId)) {
                            DisplayStudentYearbookActivity.postLists.get(i).setPostComment(PostComment);
                            DisplayStudentYearbookActivity.postLists.get(i).setTotalComment(comment);
                        }
                    }
                }

                if (StudentBookmarkActivity.bookmarkList != null) {
                    for (int i = 0; i < StudentBookmarkActivity.bookmarkList.size(); i++) {
                        if (StudentBookmarkActivity.bookmarkList.get(i).getPostId().equals(PostId)) {
                            StudentBookmarkActivity.bookmarkList.get(i).setPostComment(PostComment);
                            StudentBookmarkActivity.bookmarkList.get(i).setTotalComment(comment);
                        }
                    }
                }


            }
        }
    }

    @Override
    public void onBackPressed() {
        if (PlayerManager.getInstance().onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package com.app.yearbook.adapter;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.app.yearbook.fragment.DeletedFragment;
import com.app.yearbook.fragment.ReportedFragment;

public class TrashTabViewPagerAdapter extends FragmentPagerAdapter {

    private static int TAB_COUNT = 2,page;

    public TrashTabViewPagerAdapter(FragmentManager fm,int page) {
        super(fm);
        this.page=page;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle myBundle = new Bundle();
        Fragment fragment = null;
        myBundle .putString("page", page+"");
        switch (position) {
            case 0:
                fragment=new ReportedFragment();
                break;
            case 1:
                fragment=new DeletedFragment();
                break;
        }

        fragment.setArguments(myBundle);
        return fragment;
    }

    @Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Reported";

            case 1:
                return "Deleted";
        }
        return super.getPageTitle(position);
    }

}

import axios from 'axios'
import { Image } from 'react-native'
import Swal from 'sweetalert2'
import { global } from '.'

export async function postResponse (url, data, ...other) {
  try {
    const formdata = new FormData()
    Object.keys(data).map(item => formdata.append(item, data[item]))
    let response = await axios({
      method: 'POST',
      url,
      data: formdata
      // ...other
    })
    response = response.data
    return response
  } catch (error) {
    return error
  }
}

export async function failerResponse (method, error) {
  console.warn('===>', method, '===>', error)
}

export async function getImagesize (img) {
  return new Promise(resolve => {
    Image.getSize(img, (width, height) => {
      height = global.width * height / width
      resolve({ height, width })
    })
  })
}

export async function exitConfirm (callBack) {
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then(async (result) => {
    if (result.value) {
      callBack()
    }
  })
}

export async function confirmDialog (callBack) {
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then(async (result) => {
    if (result.value) {
      const res = await callBack()
      if (res) {
        Swal.fire(
          'Deleted!',
          'Deleted success.',
          'success'
        )
      }
    }
  })
}

export function secondsToTime (secs) {
  var hours = Math.floor(secs / (60 * 60))

  var divisor_for_minutes = secs % (60 * 60)
  var minutes = Math.floor(divisor_for_minutes / 60)

  var divisor_for_seconds = divisor_for_minutes % 60
  var seconds = Math.ceil(divisor_for_seconds)
  if (hours === 0) {
    return ('0' + minutes).slice(-2) + ':' + ('0' + seconds).slice(-2)
  }
  return ('0' + hours).slice(-2) + ':' + ('0' + minutes).slice(-2) + ':' + ('0' + seconds).slice(-2)
}

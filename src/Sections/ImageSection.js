import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Text, Image, ImageBackground, TouchableOpacity } from "react-native";
import ImageScal from "../utils/ImageScal"
import { img_section_gradient, diamond_ic, image_placeholder, diamond_ic_selected } from "../assets";
import Actions from "../actions";
import { colors } from "../utils";

let lastTap = null;

export default function ImageSection({ data }) {
    const dispatch = useDispatch();
    const { component_url, total_likes, is_bookmark, is_liked } = data;

    const [state, setState] = useState({
        height: 200,
        like: total_likes,
        is_liked,
        is_bookmark
    })

    useEffect(() => {
        _getScal();
    }, [component_url]);

    const _getScal = async () => {
        if (!component_url) return;
        const { height } = await ImageScal(data.component_url)
        if (height)
            setState({ ...state, height })
    }

    const _toggleLike = async () => {
        let { like, is_liked } = state;
        if (is_liked) like = like - 1
        else like = like + 1
        setState({ ...state, like, is_liked: !is_liked })
        const res = await dispatch(Actions.sectionLike(data.component_id));
        if (!res.Result) {
            if (is_liked) like = like + 1
            else like = like - 1
            setState({ ...state, like, is_liked })
        }
    }

    const _doubleTap = () => {
        const now = Date.now();
        const DOUBLE_PRESS_DELAY = 300;
        if (lastTap && (now - lastTap) < DOUBLE_PRESS_DELAY) {
            _toggleLike();
        } else {
            lastTap = now;
        }
      }

    // const _bookmark = async () => {
    //     let { is_bookmark } = state;
    //     setState({ ...state, is_bookmark: !is_bookmark });
    //     const res = await dispatch(Actions.sectionLike(data.component_id));
    //     if (!res.Result) {
    //         setState({ ...state, is_bookmark })
    //     }
    // }

    return (
        <TouchableOpacity
            style={{ flex: 1 }}
            activeOpacity={1}
            onPress={_doubleTap}
        >
            <Image
                source={component_url ? { uri: component_url } : image_placeholder}
                style={{ height: state.height, width: "100%" }}
                resizeMode="stretch"
            />
            <ImageBackground
                style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute"
                }}
                source={img_section_gradient}>

                <TouchableOpacity
                    style={{
                        position: "absolute",
                        bottom: 15,
                        left: 15,
                        flexDirection: "row",
                        alignItems: "center"
                    }}
                    onPress={_toggleLike}
                    activeOpacity={0.6}
                >
                    <Image
                        source={state.is_liked ? diamond_ic_selected : diamond_ic}
                        style={{
                            width: 24,
                            height: 24,
                        }}
                        resizeMode="contain"
                    />
                    <Text
                        style={{
                            fontSize: 16,
                            color: colors.white,
                            paddingLeft: 5
                        }}
                        numberOfLines={1}
                    >
                        {state.like}
                    </Text>
                </TouchableOpacity>

                {/* <TouchableOpacity
                    style={{
                        position: "absolute",
                        bottom: 15,
                        right: 15,
                        flexDirection: "row",
                        alignItems: "center"
                    }}
                    onPress={_bookmark}
                    activeOpacity={0.6}
                >
                    <Image
                        source={state.is_bookmark ? bookmark_fill_ic : bookmark_ic}
                        style={{
                            width: 20,
                            height: 20,
                        }}
                        resizeMode="contain"
                    />
                </TouchableOpacity> */}
            </ImageBackground>
        </TouchableOpacity>
    )
}
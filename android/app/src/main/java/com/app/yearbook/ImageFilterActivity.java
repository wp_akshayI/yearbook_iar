package com.app.yearbook;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.yearbook.adapter.ThumbnailsAdapter;
import com.app.yearbook.utils.BitmapUtils;
import com.app.yearbook.utils.Constants;
import com.app.yearbook.utils.ThumbnailCallback;
import com.app.yearbook.utils.ThumbnailItem;
import com.app.yearbook.utils.ThumbnailsManager;
import com.github.ybq.android.spinkit.style.Circle;
import com.zomato.photofilters.FilterPack;
import com.zomato.photofilters.imageprocessors.Filter;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ImageFilterActivity extends AppCompatActivity implements ThumbnailCallback {

    static {
        System.loadLibrary("NativeImageProcessor");
    }

    private Activity activity;
    private Toolbar toolbar;
    private Uri getUri;
    private RecyclerView thumbListView;
    private ImageView placeHolderImageView;
    private Bitmap thumbImage;
    private String getType, CategoryId;
    private int CropType;
    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_filter2);
        setView();
    }

    public void setView() {

        //toolbar
        setToolbar();

        progressDialog = ProgressDialog.show(ImageFilterActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        if (getIntent().getExtras() != null) {
            getUri = Uri.parse(getIntent().getStringExtra("selectUri"));
            Log.d("TTT", "setView: getUri: " + getUri);
            getType = getIntent().getStringExtra("from");

            if (getType.equalsIgnoreCase("staff")) {
                CategoryId = getIntent().getStringExtra("CategoryId");
                CropType = getIntent().getIntExtra("CropType", 1);
            }

            try {
                thumbImage = MediaStore.Images.Media.getBitmap(getContentResolver(), getUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

        }

        activity = this;

        initUIWidgets();
    }


    private void initUIWidgets() {
        thumbListView = findViewById(R.id.thumbnails);
        placeHolderImageView = findViewById(R.id.place_holder_imageview);
        placeHolderImageView.setImageURI(getUri);

        //  placeHolderImageView.setImageBitmap(Bitmap.createScaledBitmap(thumbImage, 640, 640, false));
        initHorizontalList();
    }

    @Override
    protected void onStart() {
        super.onStart();
//        initHorizontalList();
    }

    private void initHorizontalList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.scrollToPosition(0);
        thumbListView.setLayoutManager(layoutManager);
        thumbListView.setHasFixedSize(true);
        bindDataToAdapter();
    }

    //adapter for bind filter
    private void bindDataToAdapter() {
        final Context context = this.getApplication();
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {

                ThumbnailsManager.clearThumbs();
                // add normal bitmap first
                ThumbnailItem thumbnailItem = new ThumbnailItem();
                thumbnailItem.image = thumbImage;
                thumbnailItem.filter.setName(getString(R.string.filter_normal));
                ThumbnailsManager.addThumb(thumbnailItem);

                List<Filter> filters = FilterPack.getFilterPack(getApplicationContext());

                for (Filter filter : filters) {
                    ThumbnailItem tI = new ThumbnailItem();
                    tI.image = thumbImage;
                    tI.filter = filter;
                    ThumbnailsManager.addThumb(tI);
                }

                List<ThumbnailItem> thumbs = ThumbnailsManager.processThumbs(context);


                final ThumbnailsAdapter adapter = new ThumbnailsAdapter(getApplicationContext(), thumbs, (ThumbnailCallback) activity);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        thumbListView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        placeHolderImageView.setVisibility(View.VISIBLE);
                    }
                });


            }
        };
        //handler.post(r);
        new Thread(r).start();
    }

    //set toolbar
    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Filter");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
//        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
//        upArrow.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.next) {
            BitmapDrawable drawable = (BitmapDrawable) placeHolderImageView.getDrawable();
            Bitmap bitmap = drawable.getBitmap();

            File f;
            if (getType.equalsIgnoreCase(Constants.UPDATE_PROFILE)) {
                f = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "profile_" + SystemClock.currentThreadTimeMillis() + ".jpg");
            } else {
                f = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "crop_test_filter_" + SystemClock.currentThreadTimeMillis() + ".jpg");
            }


            try {
                BitmapUtils.writeBitmapToFile(bitmap, f, 100);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Uri uri= AllMethods.getImageUri(getApplicationContext(),bitmap);

            if (getType.equalsIgnoreCase("library")) {
                Intent i = new Intent(getApplicationContext(), AddPostActivity.class);
                i.putExtra("Type", "Image");
                i.putExtra("AddPostImage", Uri.fromFile(f).toString());
                setResult(RESULT_OK, i);// New Flow
                finish(); // New Flow
//                startActivityForResult(i, 222); // Old Flow
            } else if (getType.equalsIgnoreCase("staff")) {
                Intent i = new Intent(getApplicationContext(), AddYearbookPostActivity.class);
                i.putExtra("CategoryId", CategoryId);
                i.putExtra("Type", "Image");
                i.putExtra("AddPostImage", Uri.fromFile(f).toString());
                i.putExtra("CropType", CropType);
                startActivityForResult(i, 222);
            } else if (getType.equalsIgnoreCase("advertisement")) {
                Intent i = new Intent(getApplicationContext(), AdvertismentFinalPostActivity.class);
                i.putExtra("AddPostImage", Uri.fromFile(f).toString());
                startActivityForResult(i, 222);
            } else if (getType.equalsIgnoreCase(Constants.UPDATE_PROFILE)) {
                Intent intent = new Intent();
                intent.putExtra(Constants.IMAGE_URI, Uri.fromFile(f).toString());
                setResult(RESULT_OK, intent);
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onThumbnailClick(Filter filter) {
        Bitmap.Config config = null;
        //int width = thumbImage.getWidth();
        //int height = thumbImage.getHeight();

        if (thumbImage.getConfig() == null) {
            config = Bitmap.Config.ARGB_8888;
        } else {
            config = thumbImage.getConfig();
        }

        int pixel;
        // Here you ll get the immutable Bitmap
        Bitmap copyBitmap = thumbImage.copy(config, true);
        // scan through all pixels
//        for (int x = 0; x < width; ++x) {
//            for (int y = 0; y < height; ++y) {
//                // get pixel color
//                pixel = thumbImage.getPixel(x, y);
//                if (x == 400 && y == 200) {
//                    copyBitmap.setPixel(x, y,
//                            Color.argb(0xFF, Color.red(pixel), 0, 0));
//                } else {
//                    copyBitmap.setPixel(x, y, pixel);
//                }
//            }
//        }

        placeHolderImageView.setImageBitmap(filter.processFilter(Bitmap.createScaledBitmap(copyBitmap, thumbImage.getWidth(), thumbImage.getHeight(), false)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selectimage, menu);
        MenuItem next = menu.findItem(R.id.next);
        next.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 222 && resultCode == RESULT_OK) {
            Log.d("TTTT", "ImageFilterActivity onActivityResult");
            Intent i = getIntent();
            setResult(RESULT_OK, i);
            finish();
        }
    }


}

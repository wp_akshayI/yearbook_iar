import { action } from '../utils'

const fontFamily = [
  'Arimo',
  'Calibri',
  'Baskerville',
  'Helvetica',
  'Cambria',
  'Fantasia',

  'TeamSpiritNF',
  'YoureGone-Regular',
  'Baveuse-Regular',
  'Nervous',
  'WCWunderbachRoughBta',
  'ItLivesInTheSwampBRK',
  'Gwibble',
  'Ransom',
  'Anklepants',
  'Kredit-Regular',
  'RicksAmericanNF',
  'Sofachrome',
  'ShogunsClan',
  'AirstreamNF',
  'peachsundress',
  'Zoikabold',
  'Equivalent-Bold',
  'AurachTri',
  'Rye-Regular',
  'betterPersonalUse',
  'Answer-Regular',
  'Doughnut-Monster__G',
  'SoccerLeague-Regular',
  'Suggested-Regular',
  'Games',
  'BudmoJiggler-Regular'
]

const initialState = {
  fontFamily,
  pageInside: undefined,
  grades: null
}

export default function ComponentsReducer (state = initialState, data) {
  switch (data.type) {
    case action.page_inside_list:
      return {
        ...state,
        pageInside: data.payload
      }

    case action.get_grades:
      return {
        ...state,
        grades: data.payload
      }

    case action.INITIAL_STATE:
      return {
        ...initialState
      }

    default:
      break
  }
  return state
}

import React from "react"
import InputText from "../components/InputText";


export default function TitleSubTitleSetting({ data, updateComponents }) {
    const _title = (e) => {
        let value = { component_title: e.target.value }
        updateComponents(value, data.component_order_by)
    }

    const _description = (e) => {
        let value = { component_description: e.target.value }
        updateComponents(value, data.component_order_by)
    }

    return (
        <div style={{ padding: 5, marginTop: 10, marginBottom: 20 }}>

            <InputText
                title="Subtitle"
                defaultValue={data.component_title}
                onChange={_title}
            />
            <div style={{ height: 10 }} />
            <InputText
                title="Body Text"
                defaultValue={data.component_description}
                onChange={_description}
                multiline
                rows="4"
                labelWidth={75}
            />
        </div>
    )
}
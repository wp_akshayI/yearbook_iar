package com.app.yearbook.utils;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.app.yearbook.model.NotificationAcceptDeny;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActionReceiver extends BroadcastReceiver {

    private static final String TAG = "ActionReceiver";
    int number;
    String id;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action=intent.getStringExtra("action");
        number=intent.getIntExtra("Notification_ID",0);
        id=intent.getStringExtra("NID");


        Log.d(TAG, "onReceive: action: "+action);
        if(action.equals("accept")){
            performAction1(context);
        }
        else if(action.equals("deny")){
            performAction2(context);
        }
//        
    }

    private void performAction2(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if(notificationManager!=null)
        {
            notificationManager.cancel(number);
        }
       // callAPI(context,id,"2");
    }

    private void performAction1(Context context) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if(notificationManager!=null)
        {
            notificationManager.cancel(number);
        }

       // callAPI(context,id,"1");
    }

    public void callAPI(final Context context, String id, String flag)
    {
        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<NotificationAcceptDeny> userPost = retrofitClass.notificationStatus(Integer.parseInt(id),Integer.parseInt(flag));
        userPost.enqueue(new Callback<NotificationAcceptDeny>() {
            @Override
            public void onResponse(Call<NotificationAcceptDeny> call, Response<NotificationAcceptDeny> response) {
                Toast.makeText(context,""+response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<NotificationAcceptDeny> call, Throwable t) {
                Toast.makeText(context,""+t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });

    }

}

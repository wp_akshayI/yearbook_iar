import { Image } from 'react-native'
import { global } from '../utils'
const screenWidth = global.width

async function ImageScal (uri) {
  return new Promise((resolve) => {
    Image.getSize(uri, (width, height) => {
      height = screenWidth * height / width
      resolve({ height, width })
    })
  })
}

export default ImageScal

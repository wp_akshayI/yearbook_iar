import react from "react";
import { NativeModules } from "react-native";

const getToken = (callBack) => NativeModules.AppModules.getToken(callBack)
const openDrawer = () => NativeModules.AppModules.openDrawer()
const backDialog = () => NativeModules.AppModules.backDialog()

export {
    openDrawer,
    getToken,
    backDialog
}
package com.app.yearbook.adapter;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.app.yearbook.R;
import com.app.yearbook.databinding.AdapterOptionsBinding;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.allpost.OptionsModel;

import java.util.List;

public class AdapterOptions extends RecyclerView.Adapter<AdapterOptions.OptionHolder> {

    List<OptionsModel> optionsModels;
    OnRecyclerClick onRecyclerClick;

    public AdapterOptions(List<OptionsModel> optionsModels, OnRecyclerClick onRecyclerClick) {
        this.optionsModels = optionsModels;
        this.onRecyclerClick = onRecyclerClick;
    }

    @NonNull
    @Override
    public OptionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterOptionsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_options, parent, false);
        return new OptionHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull OptionHolder holder, int position) {
        holder.binding.setModel(optionsModels.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return optionsModels.size();
    }

    public class OptionHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        AdapterOptionsBinding binding;

        public OptionHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            binding.rbOption.setEnabled(false);
//            binding.rbOption.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            onRecyclerClick.onClick(getAdapterPosition(), 0);
        }
    }
}

package com.app.yearbook.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.app.yearbook.R;
import com.app.yearbook.adapter.AdapterPostList;
import com.app.yearbook.interfaces.OnMenuOptionClick;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.github.ybq.android.spinkit.style.Circle;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OptionMenuController {

    Context context;
    AdapterPostList postListAdapter;
    ArrayList<PostListItem> PostListArray;
    ProgressDialog progressDialog;
    OnMenuOptionClick menuOptionClick;

    public OptionMenuController(Context context, AdapterPostList adapterPostList, ArrayList<PostListItem> postListArray) {
        this.context = context;
        this.postListAdapter= adapterPostList;
        this.PostListArray = postListArray;
    }

    public void setOptionMenu(OnMenuOptionClick menuOptionClick1){
        this.menuOptionClick = menuOptionClick1;
        postListAdapter.setPopupOpenListener(new OnMenuOptionClick() {
            @Override
            public void onClick(final int pos, final int type, View view) {
                final PopupWindow popup;
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View viewWindow = inflater.inflate(R.layout.popup_option_menu, null);

                TextView btnEdit = viewWindow.findViewById(R.id.txt_edit);
                TextView btnDelete = viewWindow.findViewById(R.id.txt_delete);
                TextView txtPosted = viewWindow.findViewById(R.id.txt_posted);

                txtPosted.setText("Posted by: "+ PostListArray.get(pos).getStaffFirstname() + " " + PostListArray.get(pos).getStaffLastname());

                popup = new PopupWindow(viewWindow, RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true);

                if (LoginUser.getUserTypeKey()==2){
                    boolean isExpired = PostListArray.get(pos).getStaffId().contentEquals(LoginUser.getUserData().getStaffId());
                    if (PostListArray.get(pos).getPostType().contentEquals("4")) {
                        isExpired = PostListArray.get(pos).isDaysLeft();
                        if (isExpired)
                            isExpired = PostListArray.get(pos).getStaffId().contentEquals(LoginUser.getUserData().getStaffId());
                        btnEdit.setVisibility(isExpired ? View.VISIBLE: View.GONE);
                    }
                    if(isExpired) {
                        btnEdit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                menuOptionClick.onClick(pos, type,v);
                                popup.dismiss();
                            }
                        });
                    }else{
                        btnEdit.setVisibility(View.GONE);
                    }
                    btnDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openDeletePopUp(pos);
                            popup.dismiss();
                        }
                    });

                }else{
                    btnEdit.setVisibility(View.GONE);
                    btnDelete.setVisibility(View.GONE);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    popup.setElevation(20);
                }

                /*manage up-down display*/
                int[] values = new int[2];
                view.getLocationInWindow(values);
                int positionOfIcon = values[1];
                System.out.println("Position Y:" + positionOfIcon);

                //Get the height of 2/3rd of the height of the screen
                DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
                int height = (displayMetrics.heightPixels * 2) / 3;
                System.out.println("Height:" + height);

                //If the position of menu icon is in the bottom 2/3rd part of the screen then we provide menu height as offset  but in negative as we want to open our menu to the top
                int yOff = 0;
                if (positionOfIcon > height) {
                    /*if (btnDelete.getVisibility() == View.VISIBLE && btnEdit.getVisibility() == View.VISIBLE)
                        yOff = -320;
                    else if (btnDelete.getVisibility() == View.VISIBLE)
                        yOff = -220;
                    else
                        yOff = -130;*/

                    yOff = (int) context.getResources().getDimension(R.dimen._minus45sdp);
                    if (btnDelete.getVisibility() == View.VISIBLE && btnEdit.getVisibility() == View.VISIBLE)
                        yOff = yOff*3;
                    else if (btnDelete.getVisibility() == View.VISIBLE)
                        yOff = yOff*2;
                    else
                        yOff = yOff*1;
                }
                popup.showAsDropDown(view, 0, yOff);
                /*manage up-down display*/

                //popup.showAsDropDown(view);
            }
        });
    }

    private void openDeletePopUp(final int pos) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        callDeletePostApi(pos);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getString(R.string.delete_msg)).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void callDeletePostApi(final int pos) {
        progressDialog = ProgressDialog.show(context, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> deletePost = retrofitClass.deletePost(Integer.parseInt(PostListArray.get(pos).getPostId()));
        deletePost.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    Toast.makeText(context, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                    PostListArray.remove(pos);
                    postListAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(context, t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

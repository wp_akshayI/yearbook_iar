package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.UserPostAdapter;
import com.app.yearbook.model.staff.livefeed.PostListItem;
import com.app.yearbook.model.staff.trash.GetReportedPost;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentPostTagImageActivity extends AppCompatActivity {

    private String tagName="",type="",setType="";
    private Toolbar toolbar;
    public ProgressDialog progressDialog;
    private AllMethods allMethods;
    private LoginUser loginUser;
    private int schoolId,userID, IntentCodeStaff=890, IntentCodeStudent=780;
    private LinearLayout lvNoData;
    public static RecyclerView rvUserTagPost;
    public static ArrayList<PostListItem> getUserList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_post_tag_image);
        setView();
    }

    public void setView()
    {
        if(getIntent().getExtras()!=null)
        {
            tagName=getIntent().getStringExtra("Tagname");
            type=getIntent().getStringExtra("type");
            Log.d("TTT","Tag name: "+tagName);
        }

        loginUser = new LoginUser(getApplicationContext());
        if (loginUser.getUserData() != null) {
            schoolId = Integer.parseInt(loginUser.getUserData().getSchoolId());

            if(type.equalsIgnoreCase("staff"))
            {
                setType="yearbook";
                userID = Integer.parseInt(loginUser.getUserData().getStaffId());

                Log.d("TTT","staff: "+userID);
            }
            else if(type.equalsIgnoreCase("regular")) {
                setType="regular";
                userID = Integer.parseInt(loginUser.getUserData().getUserId());
                Log.d("TTT","regular: "+userID);
            }
        }

        //lvNoData
        lvNoData=findViewById(R.id.lvNoData);

        //rv
        rvUserTagPost=findViewById(R.id.rvUserTagPost);

        //set toolbar
        setToolbar();

        //all methods object
        allMethods = new AllMethods();

        getUserPost();


    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Post");
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void getUserPost() {
        //Progress bar
        progressDialog = ProgressDialog.show(StudentPostTagImageActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetReportedPost> userPost = retrofitClass.getSearchTagPost(userID,schoolId,tagName,setType);
        userPost.enqueue(new Callback<GetReportedPost>() {
            @Override
            public void onResponse(Call<GetReportedPost> call, Response<GetReportedPost> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                getUserList = new ArrayList<>();
                if (response.body().getResponseCode().equals("1")) {
                    if (response.body().getPostList().size() > 0) {
                        rvUserTagPost.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        getUserList.addAll(response.body().getPostList());
                        GridLayoutManager mLayoutManager = new GridLayoutManager(StudentPostTagImageActivity.this, 3);
                        rvUserTagPost.setLayoutManager(mLayoutManager);
                        UserPostAdapter mAdapter = new UserPostAdapter(getUserList, StudentPostTagImageActivity.this,type);
                        rvUserTagPost.setAdapter(mAdapter);

                    } else {
                        rvUserTagPost.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10")) {
                    Toast.makeText(StudentPostTagImageActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StudentPostTagImageActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StudentPostTagImageActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    rvUserTagPost.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                    //allMethods.setAlert(UserDetailActivity.this, "", response.body().getResponseMsg() + "");
                }
            }

            @Override
            public void onFailure(Call<GetReportedPost> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StudentPostTagImageActivity.this, "", t.getMessage() + "");
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentCodeStaff && resultCode == RESULT_OK) {

            if(data.getStringExtra("deleteUser")!=null)
            {
                Log.d("TTT","IntentCodeStaff: delete user");
                String id=data.getStringExtra("deleteUser");

                if(getUserList!=null) {
                    for (int x = getUserList.size() - 1; x >= 0; x--) {
                        Log.d("TTT", "loop: " + x);
                        if (getUserList.get(x).getStaffId().equals(String.valueOf(id))) {
                            Log.d("TTT", "position: " + x);
                            getUserList.remove(x);
                            rvUserTagPost.getAdapter().notifyItemRemoved(x);
                        }
                    }
                }

                if(getUserList.size()==0)
                {
                    rvUserTagPost.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                }

            }
            else if(data.getStringExtra("deletePost")!=null)
            {
                Log.d("TTT","IntentCodeStaff: delete post");
                String id=data.getStringExtra("deletePost");

                if(getUserList!=null) {
                    for (int i = 0; i <getUserList.size(); i++) {
                        if (getUserList.get(i).getPostId().equals(String.valueOf(id))) {
                            Log.d("TTT", "Remove " + id + " / " + i);
                            getUserList.remove(i);
                            rvUserTagPost.getAdapter().notifyItemRemoved(i);
                        }
                    }
                }

                if(getUserList.size()==0)
                {
                    rvUserTagPost.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                }

            }
        }
        else  if (requestCode == IntentCodeStudent && resultCode == RESULT_OK) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

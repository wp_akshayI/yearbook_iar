
package com.app.yearbook.model.staff.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentList {

    @SerializedName("repoted_post_id")
    @Expose
    private String repotedPostId;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("repoted_message")
    @Expose
    private String repotedMessage;
    @SerializedName("repoted_post_created")
    @Expose
    private String repotedPostCreated;
    @SerializedName("user_firstname")
    @Expose
    private String userFirstname;
    @SerializedName("user_lastname")
    @Expose
    private String userLastname;
    @SerializedName("user_image")
    @Expose
    private String userImage;

    public String getRepotedPostId() {
        return repotedPostId;
    }

    public void setRepotedPostId(String repotedPostId) {
        this.repotedPostId = repotedPostId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRepotedMessage() {
        return repotedMessage;
    }

    public void setRepotedMessage(String repotedMessage) {
        this.repotedMessage = repotedMessage;
    }

    public String getRepotedPostCreated() {
        return repotedPostCreated;
    }

    public void setRepotedPostCreated(String repotedPostCreated) {
        this.repotedPostCreated = repotedPostCreated;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

}

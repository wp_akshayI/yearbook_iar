import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Image, ActivityIndicator, TouchableOpacity } from 'react-native'
import { global } from '../../utils'
import { image_placeholder } from '../../assets'
import ImageScal from '../../utils/ImageScal'
import ImageLightBox from '../../components/ImageLightBox'

export default function Gallery ({ data }) {
  const [state, setState] = useState({
    isLightbox: false,
    lightbox_uri: ''

  })

  const renderItem = (item, index) => {
    const column = Number(data.grid_row)
    const uri = item || image_placeholder
    return (
      <TouchableOpacity
        style={[styles.list]}
        onPress={() => setState({ ...state, isLightbox: true, lightbox_uri: uri })}
        key={String(index)}
      >
        <Image
          resizeMode='cover'
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: global.width / column - 4,
            height: global.width / column - 4
          }}
          source={{ uri }}
        />
      </TouchableOpacity>
    )
  }

  const renderSingleItem = (item, index) => {
    const uri = item || image_placeholder
    return (
      <TouchableOpacity
        style={styles.list}
        onPress={() => setState({ ...state, isLightbox: true, lightbox_uri: uri })}
        activeOpacity={1}
        key={String(index)}
      >
        <ImageWithFullHeight
          uri={uri}
        />
      </TouchableOpacity>
    )
  }

  return (
    <View style={styles.container}>
      {
        Number(data.grid_row) === 1 && data.images && data.images.map(renderSingleItem)
      }
      {
        Number(data.grid_row) != 1 && (
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            {
              data.images && data.images.map(renderItem)
            }
          </View>
        )
      }
      <ImageLightBox
        isVisible={state.isLightbox}
        handleClose={() => setState({ ...state, isLightbox: false })}
        uri={state.lightbox_uri}
      />
    </View>
  )
}

function ImageWithFullHeight ({ uri, ...props }) {
  const [state, setState] = useState({
    isLoading: true,
    height: global.height * 0.4
  })

  useEffect(() => {
    _getScal()
  }, [uri])

  const _getScal = async () => {
    if (!uri) return
    const { height } = await ImageScal(uri)
    if (height) { setState({ ...state, height }) }
  }

  return (
    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
      <Image
        resizeMode='contain'
        style={{
          width: '100%',
          height: state.height
        }}
        source={{ uri }}
        onLoad={() => setState({ ...state, isLoading: false })}
        {...props}
      />
      {
        state.isLoading && <ActivityIndicator style={{ position: 'absolute' }} />
      }
    </View>
  )
}

const styles = StyleSheet.create({
  columnTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: 'Calibri',
    padding: 10,
    color: '#000'
  },
  container: {
    backgroundColor: '#fff'
  },
  list: {
    margin: 2
  }
})

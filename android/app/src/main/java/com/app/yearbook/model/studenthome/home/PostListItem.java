package com.app.yearbook.model.studenthome.home;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.app.yearbook.BR;
import com.app.yearbook.model.staff.yearbook.UserTagg;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
public class PostListItem extends BaseObservable implements Serializable {

    @SerializedName("staff_image")
    private String staffImage;

    @SerializedName("total_like")
    private String totalLike;

    @SerializedName("staff_lastname")
    private String staffLastname;

    @SerializedName("post_file")
    private String postFile;

    @SerializedName("post_like")
    private String postLike;

    @SerializedName("total_comment")
    private String totalComment;

    @SerializedName("staff_firstname")
    private String staffFirstname;

    @SerializedName("post_description")
    private String postDescription;

    @SerializedName("post_file_type")
    private String postFileType;

    @SerializedName("post_notification")
    private String postNotification;

    @SerializedName("post_id")
    private String postId;

    @SerializedName("school_id")
    private String schoolId;

    @SerializedName("post_date")
    private String postDate;

    @SerializedName("staff_id")
    private String staffId;

    @SerializedName("post_thumbnail")
    private String postThumbnail;

    @SerializedName("post_push")
    private String postPush;

    @SerializedName("post_type")
    private String postType;

    @SerializedName("post_bookmark")
    private String postBookmark;

    @SerializedName("user_tagg")
    private List<UserTagg> userTagg;

    @SerializedName("post_staff_id")
    private String postStaffId;

    @SerializedName("post_title")
    private String postTitle;

    @SerializedName("flag")
    private String flag;

    @SerializedName("yearbook_school_id")
    private String yearbookSchoolId;

    @SerializedName("post_user_id")
    private String postUserId;


    @Bindable
    public String getPostUserId() {
        return postUserId;
    }

    public void setPostUserId(String postUserId) {
        this.postUserId = postUserId;
        notifyPropertyChanged(BR.postUserId);
    }

    public void setStaffImage(String staffImage) {
        this.staffImage = staffImage;
        notifyPropertyChanged(BR.staffImage);
    }

    @Bindable
    public String getStaffImage() {
        return staffImage;
    }

    public void setTotalLike(String totalLike) {
        this.totalLike = totalLike;
        notifyPropertyChanged(BR.totalLike);
    }

    @Bindable
    public String getTotalLike() {
        return totalLike;
    }

    public void setStaffLastname(String staffLastname) {
        this.staffLastname = staffLastname;
        notifyPropertyChanged(BR.staffLastname);
    }

    @Bindable
    public String getStaffLastname() {
        return staffFirstname + "  " + staffLastname;
    }

    public void setPostFile(String postFile) {
        this.postFile = postFile;
        notifyPropertyChanged(BR.postFile);
    }

    @Bindable
    public String getPostFile() {
        return postFile;
    }

    public void setPostLike(String postLike) {
        this.postLike = postLike;
        notifyPropertyChanged(BR.postLike);
    }

    @Bindable
    public String getPostLike() {
        return postLike;
    }

    public void setTotalComment(String totalComment) {
        this.totalComment = totalComment;
        notifyPropertyChanged(BR.totalComment);
    }

    @Bindable
    public String getTotalComment() {
        return totalComment;
    }

    public void setStaffFirstname(String staffFirstname) {
        this.staffFirstname = staffFirstname;
        notifyPropertyChanged(BR.staffLastname);

    }

    @Bindable
    public String getStaffFirstname() {
        return staffFirstname;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
        notifyPropertyChanged(BR.postDescription);
    }

    @Bindable
    public String getPostDescription() {
        return postDescription;
    }

    public void setPostFileType(String postFileType) {
        this.postFileType = postFileType;
        notifyPropertyChanged(BR.postFileType);
    }

    @Bindable
    public String getPostFileType() {
        return postFileType;
    }

    public void setPostNotification(String postNotification) {
        this.postNotification = postNotification;
        notifyPropertyChanged(BR.postNotification);
    }

    @Bindable
    public String getPostNotification() {
        return postNotification;
    }

    public void setPostId(String postId) {
        this.postId = postId;
        notifyPropertyChanged(BR.postId);
    }

    @Bindable
    public String getPostId() {
        return postId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
       notifyPropertyChanged(BR.schoolId);
    }

    @Bindable
    public String getSchoolId() {
        return schoolId;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
        notifyPropertyChanged(BR.postDate);
    }

    @Bindable
    public String getPostDate() {
        return postDate;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
        notifyPropertyChanged(BR.staffId);
    }

    @Bindable
    public String getStaffId() {
        return staffId;
    }

    public void setPostThumbnail(String postThumbnail) {
        this.postThumbnail = postThumbnail;
        notifyPropertyChanged(BR.postThumbnail);
    }

    @Bindable
    public String getPostThumbnail() {
        return postThumbnail;
    }

    public void setPostPush(String postPush) {
        this.postPush = postPush;
        notifyPropertyChanged(BR.postPush);
    }

    @Bindable
    public String getPostPush() {
        return postPush;
    }

    public void setPostType(String postType) {
        this.postType = postType;
        notifyPropertyChanged(BR.postType);
    }

    @Bindable
    public String getPostType() {
        return postType;
    }

    public void setPostBookmark(String postBookmark) {
        this.postBookmark = postBookmark;
        notifyPropertyChanged(BR.postBookmark);
    }

    @Bindable
    public String getPostBookmark() {
        return postBookmark;
    }

    public void setUserTagg(List<UserTagg> userTagg) {
        this.userTagg = userTagg;
        notifyPropertyChanged(BR.userTagg);
    }

    @Bindable
    public List<UserTagg> getUserTagg() {
        return userTagg;
    }

    public void setPostStaffId(String postStaffId) {
        this.postStaffId = postStaffId;
        notifyPropertyChanged(BR.postStaffId);
    }

    @Bindable
    public String getPostStaffId() {
        return postStaffId;
    }

    @Bindable
    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
        notifyPropertyChanged(BR.postTitle);
    }
    @Bindable
    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
        notifyPropertyChanged(BR.flag);
    }
    @Bindable
    public String getYearbookSchoolId() {
        return yearbookSchoolId;
    }

    public void setYearbookSchoolId(String yearbookSchoolId) {
        this.yearbookSchoolId = yearbookSchoolId;
        notifyPropertyChanged(BR.yearbookSchoolId);
    }
}
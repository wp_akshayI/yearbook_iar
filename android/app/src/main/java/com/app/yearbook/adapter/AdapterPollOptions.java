package com.app.yearbook.adapter;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.app.yearbook.R;
import com.app.yearbook.databinding.AdapterPollOptionsBinding;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.postmodels.PollOption;

import java.util.List;

public class AdapterPollOptions extends RecyclerView.Adapter<AdapterPollOptions.OptionHolder> {

    List<PollOption> pollOptions;
    boolean isAnswered;
    OnRecyclerClick onRecyclerClick;

    public AdapterPollOptions(List<PollOption> pollOptions, boolean isAnswered, OnRecyclerClick onRecyclerClick) {
        this.pollOptions = pollOptions;
        this.isAnswered = isAnswered;
        this.onRecyclerClick = onRecyclerClick;
    }

    @NonNull
    @Override
    public OptionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterPollOptionsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_poll_options, parent, false);
        return new OptionHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull OptionHolder holder, int position) {
        holder.binding.setModel(pollOptions.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return pollOptions.size();
    }

    public class OptionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        AdapterPollOptionsBinding binding;

        OptionHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            if (binding != null) {
                binding.rbOption.setOnClickListener(this);
                if (isAnswered) {
                    binding.rbOption.setEnabled(false);
                }
            }
        }

        @Override
        public void onClick(View v) {
            onRecyclerClick.onClick(getAdapterPosition(), 0);
        }
    }
}

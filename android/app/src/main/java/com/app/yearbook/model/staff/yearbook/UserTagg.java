
package com.app.yearbook.model.staff.yearbook;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.app.yearbook.BR;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserTagg extends BaseObservable implements Serializable {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_firstname")
    @Expose
    private String userFirstname;
    @SerializedName("user_lastname")
    @Expose
    private String userLastname;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("school_id")
    @Expose
    private String schoolId;


    @Bindable
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        notifyPropertyChanged(BR.userId);
    }

    @Bindable
    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
        notifyPropertyChanged(BR.userFirstname);
    }

    @Bindable
    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
        notifyPropertyChanged(BR.userLastname);
    }

    @Bindable
    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
        notifyPropertyChanged(BR.userImage);
    }

    @Bindable
    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
        notifyPropertyChanged(BR.schoolId);
    }
}

package com.app.yearbook;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fenchtose.nocropper.BitmapResult;
import com.fenchtose.nocropper.CropState;
import com.fenchtose.nocropper.CropperView;
import com.app.yearbook.utils.AllMethods;

import java.io.IOException;

import gun0912.tedbottompicker.TedBottomPicker;

public class AddAdvertisementActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TedBottomPicker tedBottomPicker;
    private ImageView imgDummy,imgUpArrow;
    private CropperView imgSelect;
    private MenuItem next;
    private static final int REQUEST_PERMISSION = 101;
    private LinearLayout lvCrop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_advertisement);

        if (ActivityCompat.checkSelfPermission(this,android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Log.d("TTT", "Permission...");
            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
        } else {
            Log.d("TTT", "granted Permission...");
            setView();
        }
    }

    public void setView()
    {
        //toolbar
        setToolbar();

        imgDummy=findViewById(R.id.imgDummy);
        imgUpArrow=findViewById(R.id.imgUpArrow);
        imgSelect=findViewById(R.id.imgSelected);
        lvCrop=findViewById(R.id.lvCrop);

        imgSelect.setMakeSquare(false);
        imgSelect.setDebug(true);
        imgSelect.setGestureEnabled(true);
        imgSelect.setGridCallback(new CropperView.GridCallback() {
            @Override
            public boolean onGestureStarted() {
                return true;
            }

            @Override
            public boolean onGestureCompleted() {
                return false;
            }
        });

        //click of dummy image
        imgDummy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tedBottomPicker = new TedBottomPicker.Builder(AddAdvertisementActivity.this)
                        .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                            @Override
                            public void onImageSelected(Uri uri) {
                                // here is selected uri
                                 next.setVisible(true);
                                imgDummy.setVisibility(View.GONE);
                                lvCrop.setVisibility(View.VISIBLE);
                                Bitmap mBitmap = AllMethods.GetBitmap(uri, getApplicationContext());
                                loadNewImage(mBitmap,uri);
                            }
                        })
                        .create();
                tedBottomPicker.show(getSupportFragmentManager());
            }
        });

        imgUpArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tedBottomPicker = new TedBottomPicker.Builder(AddAdvertisementActivity.this)
                        .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                            @Override
                            public void onImageSelected(Uri uri) {
                                // here is selected uri
                                next.setVisible(true);
                                imgDummy.setVisibility(View.GONE);
                                lvCrop.setVisibility(View.VISIBLE);
                                Bitmap mBitmap = AllMethods.GetBitmap(uri, getApplicationContext());
                                loadNewImage(mBitmap,uri);
                            }
                        })
                        .create();
                tedBottomPicker.show(getSupportFragmentManager());
            }
        });

        imgSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tedBottomPicker = new TedBottomPicker.Builder(AddAdvertisementActivity.this)
                        .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                            @Override
                            public void onImageSelected(Uri uri) {
                                // here is selected uri
                                next.setVisible(true);
                                imgDummy.setVisibility(View.GONE);
                                lvCrop.setVisibility(View.VISIBLE);
                                Bitmap mBitmap = AllMethods.GetBitmap(uri, getApplicationContext());
                                loadNewImage(mBitmap,uri);
                            }
                        })
                        .create();
                tedBottomPicker.show(getSupportFragmentManager());
            }
        });

        //default picker for image view
        tedBottomPicker = new TedBottomPicker.Builder(getApplicationContext())
                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        // here is selected uri
                       next.setVisible(true);
                        imgDummy.setVisibility(View.GONE);
                        lvCrop.setVisibility(View.VISIBLE);
                        Bitmap mBitmap = AllMethods.GetBitmap(uri, AddAdvertisementActivity.this);
                        loadNewImage(mBitmap,uri);
                    }
                })
                .create();
        tedBottomPicker.show(getSupportFragmentManager());
    }

    //load selected image
    private void loadNewImage(Bitmap mBitmap,Uri selectedImage) {
        int maxP = Math.max(mBitmap.getWidth(), mBitmap.getHeight());
        float scale1280 = (float) maxP / 1280;
        Log.i("TTT", "scaled: " + scale1280 + " - " + (1 / scale1280));

        if (imgSelect.getWidth() != 0) {
            imgSelect.setMaxZoom(imgSelect.getWidth() * 2 / 1280f);
        } else {

            ViewTreeObserver vto = imgSelect.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    imgSelect.getViewTreeObserver().removeOnPreDrawListener(this);
                    imgSelect.setMaxZoom(imgSelect.getWidth() * 2 / 1280f);
                    return true;
                }
            });
        }

        mBitmap = Bitmap.createScaledBitmap(mBitmap, (int) (mBitmap.getWidth() / scale1280),
                (int) (mBitmap.getHeight() / scale1280), true);

        //fix rotate issue for samsung device

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(selectedImage.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        Log.d("TTT","Orientation: "+orientation);

        if(orientation==ExifInterface.ORIENTATION_ROTATE_90)
        {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 90);
        }
        else if(orientation==ExifInterface.ORIENTATION_ROTATE_180)
        {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 180);
        }
        else if(orientation==ExifInterface.ORIENTATION_ROTATE_270)
        {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 270);
        }
        //---------------------------------------
        Log.d("TTT","Bitmap: "+mBitmap);
        imgSelect.setImageBitmap(mBitmap);
    }

    //set toolbar
    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Add Advertisement");
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        else if(item.getItemId()==R.id.next)
        {
            BitmapResult result = imgSelect.getCroppedBitmap();
            if (result.getState() == CropState.FAILURE_GESTURE_IN_PROCESS) {
                Toast.makeText(this, "unable to crop. Gesture in progress", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Bitmap bitmap = result.getBitmap();
                Uri getUri = AllMethods.getImageUri(this, bitmap);
                Intent i = new Intent(this, ImageFilterActivity.class);
                i.putExtra("from","advertisement");
                i.putExtra("selectUri", getUri.toString());
                startActivityForResult(i, 111);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selectimage, menu);
        next = menu.findItem(R.id.next);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    Log.d("TTT", "Permission... onRequestPermissionsResult");
                    setView();
                } else {
                    Log.d("TTT", "denyyyy... onRequestPermissionsResult");
                    Toast.makeText(this, "Deny", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==111 && resultCode==RESULT_OK) {
            Intent i=getIntent();
            setResult(RESULT_OK,i);
            finish();
        }
    }
}

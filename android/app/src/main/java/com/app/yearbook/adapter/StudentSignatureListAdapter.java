package com.app.yearbook.adapter;

import android.app.Activity;
import androidx.databinding.DataBindingUtil;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.yearbook.databinding.ItemAllsignatureListBinding;
import com.app.yearbook.utils.OnRecyclerClick;
import com.app.yearbook.R;
import com.app.yearbook.model.profile.SignatureList;

import java.util.ArrayList;

public class StudentSignatureListAdapter extends RecyclerView.Adapter<StudentSignatureListAdapter.MyViewHolder> {
    private ArrayList<SignatureList> signatureLists;
    private Activity ctx;
    private float textSize;
    private Typeface typeface;
    ItemAllsignatureListBinding binding;
    private OnRecyclerClick onRecyclerClick;

    @NonNull
    @Override
    public StudentSignatureListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_allsignature_list, parent, false);

        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_allsignature_list, parent, false);
        return new MyViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull final StudentSignatureListAdapter.MyViewHolder holder, final int position) {
        final SignatureList userList = signatureLists.get(position);
        Log.d("TTT", "Signature delete: " + userList.getSignatureId());
        holder.binding.setModel(userList);

        //set font type
//        if(userList.getSignatureFontType().equalsIgnoreCase("1"))
//        {
//            typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/Honeymoon Personal Use.ttf");
//            holder.tvUserComment.setTypeface(typeface);
//            textSize=16f;
//            holder.tvUserComment.setTextSize(16f);
//        }
//        else if(userList.getSignatureFontType().equalsIgnoreCase("2"))
//        {
//            typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/Angelique-Rose-font-FFP_0.ttf");
//            holder.tvUserComment.setTypeface(typeface);
//
//            textSize=26f;
//            holder.tvUserComment.setTextSize(textSize);
//        }
//        else if(userList.getSignatureFontType().equalsIgnoreCase("3"))
//        {
//            typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/Brasileirinha Personal Use.ttf");
//            holder.tvUserComment.setTypeface(typeface);
//
//            textSize=26f;
//            holder.tvUserComment.setTextSize(textSize);
//        }
//
//        Log.d("TTT", "onBindViewHolder: text cmnt: "+userList.getSignatureMessage());
//        holder.tvUserComment.setText(userList.getSignatureMessage()+"");
//
//        holder.tvUserComment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if(holder.tvUserComment.getLineCount()>1)
//                {
//                    android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(ctx);
//                    LayoutInflater inflater = ctx.getLayoutInflater();
//                    final View dialogView = inflater.inflate(R.layout.custom_alertbox_viewsignature, null);
//                    dialogBuilder.setView(dialogView);
//                    final android.support.v7.app.AlertDialog alert = dialogBuilder.create();
//                    alert.setCancelable(false);
//                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
//
//                    TextView tvYes=dialogView.findViewById(R.id.tvYes);
//                    tvYes.setTextColor(ctx.getResources().getColor(R.color.black));
//
//                    ImageView imgClose=dialogView.findViewById(R.id.imgClose);
//
//                    imgClose.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alert.dismiss();
//                        }
//                    });
//
//                    tvYes.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alert.dismiss();
//                        }
//                    });
//
//                    if(userList.getSignatureFontType().equalsIgnoreCase("1")) {
//                        typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/Honeymoon Personal Use.ttf");
//                        textSize=18f;
//                    }
//                    else if(userList.getSignatureFontType().equalsIgnoreCase("2"))
//                    {
//                        typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/Angelique-Rose-font-FFP_0.ttf");
//                        textSize=28f;
//                    }
//                    else if(userList.getSignatureFontType().equalsIgnoreCase("3")) {
//                        typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/Brasileirinha Personal Use.ttf");
//                        textSize=28f;
//                    }
//
//                    TextView tvMessage=dialogView.findViewById(R.id.tvMessage);
//                    tvMessage.setTypeface(typeface);
//                    tvMessage.setText(userList.getSignatureMessage()+"");
//                    tvMessage.setTextSize(textSize);
//
//                    alert.show();
//                }
//
//            }
//        });
//        holder.tvUserName.setText(userList.getUserFirstname()+"  "+userList.getUserLastname());
//        holder.tvUserTime.setText(userList.getSignatureCreated());
//
//        Glide.with(ctx)
//                .load(userList.getUserImage())
//                .asBitmap()
//                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                .placeholder(R.mipmap.profile_placeholder)
//                .error(R.mipmap.profile_placeholder)
//                .into(holder.imgUserPhoto);


//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        sdf.setTimeZone(TimeZone.getTimeZone("CST6CDT"));
//
//        Date date = null;
//        try {
//
//            date = sdf.parse(userList.getSignatureCreated());
//            sdf.setTimeZone(TimeZone.getDefault());
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        SimpleDateFormat sameDateSDF=new SimpleDateFormat("dd-MM-yyyy");
//        sameDateSDF.setTimeZone(TimeZone.getTimeZone("CST6CDT"));
//
//        sameDateSDF.setTimeZone(TimeZone.getDefault());
//        String DBDate = sameDateSDF.format(date);
//        String currentDate=sameDateSDF.format(new Date());
//        Log.d("TTT","Date: "+DBDate+" / "+currentDate);
//
//        if(DBDate.equalsIgnoreCase(currentDate))
//        {
//            //same date
//            SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//            //dateFormat.setTimeZone(TimeZone.getTimeZone("CST"));
//            Date currDate = new Date();
//
//            //dateFormat.setTimeZone(TimeZone.getDefault());
//            String dd = dateFormat.format(currDate);
//            Log.d("TTT","Dateeeee: "+dd +" / "+DBDate);
//
//            try {
//                currDate=dateFormat.parse(dd);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//            long different = currDate.getTime() - date.getTime();
//            long secondsInMilli = 1000;
//            long minutesInMilli = secondsInMilli * 60;
//            long hoursInMilli = minutesInMilli * 60;
//            long daysInMilli = hoursInMilli * 24;
//
//            long elapsedDays = different / daysInMilli;
//            different = different % daysInMilli;
//
//            long elapsedHours = different / hoursInMilli;
//            different = different % hoursInMilli;
//
//            long elapsedMinutes = different / minutesInMilli;
//            different = different % minutesInMilli;
//
//            long elapsedSeconds = different / secondsInMilli;
//
//            Log.d("TTT","DDDD : "+elapsedDays+" / "+elapsedHours+" / "+elapsedMinutes+" / "+elapsedSeconds);
//
//            if(elapsedHours<1)
//            {
//                holder.tvUserTime.setText(elapsedMinutes+"m");
//            }
//            else {
//                holder.tvUserTime.setText(elapsedHours+"h");
//            }
//        }
//        else
//        {
//            //not same
//            String outputPattern = "MMM dd, yyyy -hh:mm aa";
//            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
//
//            String str = null;
//            str = outputFormat.format(date);
//
//            holder.tvUserTime.setText(str+"");
//        }
    }

    public StudentSignatureListAdapter(ArrayList<SignatureList> signatureLists, Activity context,OnRecyclerClick onRecyclerClick) {
        this.signatureLists = signatureLists;
        this.ctx = context;
        this.onRecyclerClick=onRecyclerClick;
    }

    public void removeItem(int position) {
        signatureLists.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return signatureLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemAllsignatureListBinding binding;

//        public TextView  tvUserName,tvUserTime;
//        public EmojiTextView tvUserComment;
//        public RoundedImageView imgUserPhoto;
//        public RelativeLayout lviewForeground,lviewBackground;

        public MyViewHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
            binding.tvReadMore.setOnClickListener(this);

//            tvUserComment = view.findViewById(R.id.tvUserComment);
//            tvUserName = view.findViewById(R.id.tvUserName);
//            tvUserTime = view.findViewById(R.id.tvUserTime);
//            imgUserPhoto=view.findViewById(R.id.imgUserImage);
//            lviewBackground=view.findViewById(R.id.lviewBackground);
//            lviewForeground=view.findViewById(R.id.lviewForeground);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.tvReadMore) {
                onRecyclerClick.onClick(getAdapterPosition(),v,1);
            }
        }
    }
}

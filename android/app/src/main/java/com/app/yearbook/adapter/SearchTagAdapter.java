package com.app.yearbook.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.StudentPostTagImageActivity;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.staff.search.TagsListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchTagAdapter extends RecyclerView.Adapter<SearchTagAdapter.MyViewHolder> {
    private ArrayList<TagsListItem> tagLists;
    private Activity ctx;
    private String type;
    public ProgressDialog progressDialog;

    @NonNull
    @Override
    public SearchTagAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_searchtag, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchTagAdapter.MyViewHolder holder, final int position) {
        final TagsListItem tagList = tagLists.get(position);

        holder.tvTagName.setText(tagList.getPostDescription());
        holder.tvTagView.setText(tagList.getTotal() + " public posts");

        if(type.equalsIgnoreCase("staff"))
        {
            holder.imgReport.setVisibility(View.VISIBLE);
            holder.imgReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ctx);
                        LayoutInflater inflater = ctx.getLayoutInflater();
                        final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
                        dialogBuilder.setView(dialogView);
                        final AlertDialog alert = dialogBuilder.create();
                        alert.setCancelable(false);
                        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                        TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
                        tvMessage.setText("Are you sure you want to remove the tag?");

                        TextView tvYes = dialogView.findViewById(R.id.tvYes);
                        TextView tvNo = dialogView.findViewById(R.id.tvNo);

                        tvYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                alert.dismiss();
                                final String tag = tagList.getPostDescription();

                                Log.d("TTT","tag: "+tag);
                                progressDialog = ProgressDialog.show(ctx, "", "", true);
                                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                progressDialog.setContentView(R.layout.progress_view);
                                ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                                Circle bounce = new Circle();
                                bounce.setColor(Color.BLACK);
                                progressBar.setIndeterminateDrawable(bounce);

                                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                final Call<GiveReport> userPost = retrofitClass.staffDeleteTag(tag);
                                userPost.enqueue(new Callback<GiveReport>() {
                                    @Override
                                    public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }

                                        if(response.body().getResponseCode().equals("1")) {
                                            for (int i = 0; i < tagLists.size(); i++) {
                                                if (tagLists.get(i).getPostDescription().equals(String.valueOf(tag))) {
                                                    Log.d("TTT", "position: " + i);
                                                    tagLists.remove(i);
                                                    notifyItemRemoved(i);
                                                }
                                            }
                                            Toast.makeText(ctx, "Successfully delete the tag.", Toast.LENGTH_SHORT).show();
                                        }
                                        else if(response.body().getResponseCode().equals("10"))
                                        {
                                            Toast.makeText(ctx,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                                            LoginUser loginSP = new LoginUser(ctx);
                                            loginSP.clearData();

                                            Intent i = new Intent(ctx, LoginActivity.class);
                                            ctx.startActivity(i);
                                            ctx.finish();
                                        }
                                        else
                                        {
                                            Toast.makeText(ctx,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<GiveReport> call, Throwable t) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }
                                        Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        });

                        tvNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alert.dismiss();
                            }
                        });
                        alert.show();
                    }
                }
            });
        }

    }

    public SearchTagAdapter(ArrayList<TagsListItem> tagLists, Activity context, String type) {
        this.tagLists = tagLists;
        this.ctx = context;
        this.type = type;
    }

    @Override
    public int getItemCount() {
        return tagLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTagName, tvTagView;
        public ImageView imgReport;
        public LinearLayout lvTagData;

        public MyViewHolder(View view) {
            super(view);

            tvTagName = view.findViewById(R.id.tvTagName);
            tvTagView = view.findViewById(R.id.tvTagView);
            imgReport=view.findViewById(R.id.imgReport);
            lvTagData=view.findViewById(R.id.lvTagData);

            view.setOnClickListener(this);
            tvTagName.setOnClickListener(this);
            tvTagView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if(type.equalsIgnoreCase("staff"))
            {
                Intent i=new Intent(ctx, StudentPostTagImageActivity.class);
                i.putExtra("Tagname",tagLists.get(getAdapterPosition()).getPostDescription());
                i.putExtra("type","staff");
                ctx.startActivity(i);
            }
            else {
                Intent i = new Intent(ctx, StudentPostTagImageActivity.class);
                i.putExtra("Tagname", tagLists.get(getAdapterPosition()).getPostDescription());
                i.putExtra("type", "regular");
                ctx.startActivity(i);
            }
        }
    }
}

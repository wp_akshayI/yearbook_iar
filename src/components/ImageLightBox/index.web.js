import React, { useState, useEffect } from "react"
import { View, Text, StyleSheet, Image, FlatList, ActivityIndicator } from "react-native";
import { useDispatch } from "react-redux"
import { colors } from "../../utils";
import Modal from "modal-enhanced-react-native-web";
import CloseIcon from '@material-ui/icons/Close';
import { IconButton } from '@material-ui/core';

export default function ImageLightBox({ isVisible, handleClose, uri }) {


    return (
        <Modal
            isVisible={isVisible}
            onBackdropPress={handleClose}
            style={{ margin: 0, justifyContent: "center", alignItems: "center" }}
        >
            <IconButton
                onClick={handleClose}
                style={{ top: 0, right: 0, position: "absolute", backgroundColor: "rgba(0,0,0,0.5)", }}
            >
                <CloseIcon
                    style={{ color: "#fff" }}
                />
            </IconButton>
            <View style={{ height: "90%", width: "90%"}}>

                <Image
                    source={uri}
                    resizeMode="contain"
                    style={{ height: "100%", width: "100%" }}
                />
            </View>
        </Modal>
    )
}
package com.app.yearbook;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.app.yearbook.databinding.ActivityNewSignatureBinding;
import com.app.yearbook.model.profile.NotificationList;
import com.app.yearbook.model.SignatureModel;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.Constants;

public class NewSignatureActivity extends AppCompatActivity {

    ActivityNewSignatureBinding binding;
    SignatureModel signatureModel;
    NotificationList notificationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_signature);
        notificationList = (NotificationList) getIntent().getSerializableExtra("notification");
        String userName = notificationList.getUserFirstname() +" "+ notificationList.getUserLastname();
        signatureModel = new SignatureModel("",
                "",
                "1",
                0,
                notificationList.getSenderId(),
                notificationList.getUserImage(),
                userName,
                notificationList.getSenderType());
        binding.setModel(signatureModel);
        setToolbar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_next, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void setToolbar() {
        setSupportActionBar(binding.appbar.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (LoginUser.getUserData() != null && LoginUser.getUserData().getSchoolName() != null) {
            binding.appbar.toolbarTitle.setText(LoginUser.getUserData().getSchoolName());
        } else {
            binding.appbar.toolbarTitle.setText(getString(R.string.app_name));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_next:
                validate();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void validate() {
        if (TextUtils.isEmpty(signatureModel.getGreeting())) {
            binding.edtGreeting.setError("Please add greeting");
        } else if (TextUtils.isEmpty(signatureModel.getMessage())) {
            binding.edtGreeting.setError("Please add message");
        } else {
            Intent intent = new Intent(this, DesignSignatureActivity.class);
            intent.putExtra(Constants.SIGNATURE_DATA, signatureModel);
            startActivityForResult(intent, 100);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }
}

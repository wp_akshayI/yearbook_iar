package com.app.yearbook.model.staff.livefeed;

import com.google.gson.annotations.SerializedName;

public class UserTaggItem{

	@SerializedName("user_id")
	private String userId;

	@SerializedName("user_image")
	private String userImage;

	@SerializedName("user_firstname")
	private String userFirstname;

	@SerializedName("user_lastname")
	private String userLastname;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setUserImage(String userImage){
		this.userImage = userImage;
	}

	public String getUserImage(){
		return userImage;
	}

	public void setUserFirstname(String userFirstname){
		this.userFirstname = userFirstname;
	}

	public String getUserFirstname(){
		return userFirstname;
	}

	public void setUserLastname(String userLastname){
		this.userLastname = userLastname;
	}

	public String getUserLastname(){
		return userLastname;
	}
}
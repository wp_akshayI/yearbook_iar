import React, { useEffect, useState } from 'react'
import { View } from 'react-native-web'
import { useSelector, useDispatch } from 'react-redux'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import Fab from '@material-ui/core/Fab'
import MobilePaper from '../../components/MobilePaper'
import GridList from '@material-ui/core/GridList'
import Actions from '../../actions'
import { colors } from '../../utils'
import AddCategoryDialog from '../../components/AddCategoryDialog'
import DeleteIcon from '@material-ui/icons/Delete'
import IconButton from '@material-ui/core/IconButton'
import { confirmDialog } from '../../utils/funtions'

export default function Dashboard (props) {
  const classes = useStyles()
  let delete_id = null
  const dispatch = useDispatch()
  const { yearbookDetails } = useSelector(({ yearbook }) => yearbook)
  const { categories } = useSelector(({ category }) => category)

  const [state] = useState({
    yearbookTitle: '',
    yearbookSubtitle: '',
    yearbookBackground: undefined
  })

  useEffect(() => {
    dispatch(Actions.get_yearbook_detail())
    dispatch(Actions.get_category_list())
  }, [])

  const _addCategory = (name) => {
    dispatch(Actions.add_category(name))
  }

  const _navigateEditor = () => {
    if (!yearbookDetails) return
    const { screen_id, name } = yearbookDetails
    return props.history.push({ pathname: '/app/page-editor', search: '_id=' + screen_id + '&_name=' + name })
  }

  const _alert = (id) => {
    delete_id = id
    confirmDialog(_delete)
  }

  const _delete = async () => {
    const res = await dispatch(Actions.delete_screen(delete_id))
    if (res) {
      dispatch(Actions.get_category_list())
    }
  }

  return (
    <div>
      <Grid
        container
        spacing={6}
        className={classes.root}
      >
        <MobilePaper
          source={yearbookDetails && yearbookDetails.screen_snapshot}
        >
          <View style={styles.backgroundOpacity}>
            <div className={classes.paper}>
              <div>
                <Typography className={classes.yearbookTitle}>
                  {state.yearbookTitle}
                </Typography>

                <Typography className={classes.yearbookSubtitle}>
                  {state.yearbookSubtitle}
                </Typography>
              </div>

            </div>
          </View>
        </MobilePaper>

        <Grid item xs={8}>
          <Typography variant='h4' className={classes.title}>
                        Yearbook Homepage
          </Typography>

          <div className={classes.space} />

          <span className={classes.intro}>
                        This is the first page everybody will see when they open the yearbook. Fill it with a variety of the best memories and moments that represents the school year.
          </span>

          <div className={classes.space} />

          <Fab
            variant='extended'
            size='medium'
            color='primary'
            classes={{ label: classes.buttonLable, sizeMedium: classes.buttonSize }}
            onClick={_navigateEditor}
          >
                        Edit Homepage
          </Fab>
        </Grid>

      </Grid>

      <Grid container spacing={6} className={classes.container}>
        <Typography variant='h4' className={classes.title}>
                    Category Pages
        </Typography>
        <span className={classes.intro}>
                    Use category pages for <span style={{ fontWeight: 'bold' }}>broad topics</span>, for example "Sports" or "Activities". You'll be able to add pages with more specific topics inside of these category pages.
        </span>
      </Grid>

      <GridList className={classes.gridList}>

        {
          categories && categories.map((item, index) => {
            return (
              <div style={{ height: '100%', width: 220, margin: 15 }} key={index}>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', height: 45 }}>
                  <Typography variant='subtitle1' className={classes.listTitle}>
                    {item.category_name}
                  </Typography>
                  <IconButton
                    aria-label='delete'
                    onClick={() => _alert(item.screen_id)}
                  >
                    <DeleteIcon fontSize='small' />
                  </IconButton>
                </div>

                <MobilePaper
                  navigate
                  pageInside
                  data={item}
                  source={{ uri: item.snap_shot }}
                />
              </div>

            )
          })
        }
        <AddCategoryDialog
          title='New Category Page'
          onSubmit={_addCategory}
        />
      </GridList>
    </div>
  )
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: '#fff',
    padding: theme.spacing(5),
    paddingTop: theme.spacing(6)
  },
  paper: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex'
  },
  subtitle2: {
    color: '#A9A9A9',
    textAlign: 'center'
  },
  intro: {
    color: '#A9A9A9',
    fontSize: 18,
    fontWeight: 'normal',
    marginBottom: theme.spacing(5)
  },
  title: {
    fontWeight: 'bold',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(2)
  },
  buttonLable: {
    textTransform: 'capitalize'
  },
  buttonSize: {
    minWidth: '230px !important'
  },
  space: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2)
  },
  container: {
    backgroundColor: '#fff',
    marginTop: theme.spacing(6),
    paddingLeft: theme.spacing(5),
    paddingRight: theme.spacing(5)
  },
  gridList: {
    width: '100%',
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1)
  },
  listTitle: {
    color: '#808080',
    paddingBottom: theme.spacing(1)
  },
  yearbookTitle: {
    color: colors.white,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  yearbookSubtitle: {
    color: colors.white,
    fontSize: 12,
    textAlign: 'center',
    paddingTop: 10
  }
}))

const styles = {
  backgroundOpacity: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    padding: 5
  }
}

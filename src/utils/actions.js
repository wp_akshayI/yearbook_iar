export const INITIAL_STATE = "re initilization reducer states"
export const INIT_URL = 'init_url';
export const SET_AUTH_TOKEN = 'SET lOGIN TOKEN';
export const AUTH_SUCCESS = 'auth_success';
export const user_auth = '_id';

export const CATEGORIES_LIST = "Yearbook all categories";

// Drawer
export const SET_DRAWER_TITLE = "set-drawer-Title";
export const OPEN_DRAWER = "open-drawer";

// Editor
export const UPDATE_PAGE_TITLE = "Update-page-Title";
export const UPDATE_SELECTED_COMPONENT = "Update-component-value";
export const close_notification = "close snackmessage notification";
export const open_notification = "open snackmessage notification";

export const yearbook_list = "yearbook list";
export const set_yearbook = "selected yearbook";
export const get_categories = "yearbook categories";
export const get_yearbook_details = "yearbook details";
export const set_category_page_title = "set_category_page_title";
export const get_gride_sub_category = "page inside gride component sub categories";

export const page_inside_list = "screen page inside list";
export const get_grades = "students grade";
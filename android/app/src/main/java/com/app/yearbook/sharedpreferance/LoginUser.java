package com.app.yearbook.sharedpreferance;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.yearbook.model.StaffStudent;
import com.google.gson.Gson;

public class LoginUser {
    public static final String PREF_NAME = "LoginUser";
    public static final String PREF_NAME_REMEMBER = "LoginUser_REMEMBER";
    public static final int MODE = Context.MODE_PRIVATE;
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    public LoginUser(Context mContext) {
        sharedPreferences = mContext.getSharedPreferences(PREF_NAME, MODE);
        editor=sharedPreferences.edit();
    }

    public void setUserData(StaffStudent user){

        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString("staffstudent", json);
        editor.putBoolean("isLoggedIn", true);
        editor.apply();
    }
    public static StaffStudent getUserData(){
        Gson gson = new Gson();
        String json = sharedPreferences.getString("staffstudent", "");
        StaffStudent user = gson.fromJson(json, StaffStudent.class);
        return user;
    }

    public static boolean isUserStudent(){
        return getUserData().getUserType().equalsIgnoreCase("staff")?false:true;
    }

    public static int getUserTypeKey(){
        return getUserData().getUserType().equalsIgnoreCase("staff")?2:1;
    }
    public static String getUserToken(){
        return getUserTypeKey()==2?getUserData().getStaffToken():getUserData().getUserToken();
    }
    public static int getUserId(){
        return Integer.parseInt(getUserTypeKey()==2?getUserData().getStaffId():getUserData().getUserId());
    }

    public static String getLoginUserToken(){
        return getUserTypeKey()==2?getUserData().getStaffToken():getUserData().getUserToken();
    }

    public void clearData(){
        sharedPreferences.edit().clear().commit();
    }

    public static int getUserTypeKey(String keyString){
        return keyString.equalsIgnoreCase("staff")?2:1;
    }

}

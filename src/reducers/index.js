import { combineReducers } from "redux";
import auth from "./Auth";
import yearbook from "./Yearbook";
import navigation from "./Navigation";
import components from "./Components";
import category from "./Category";

export default () => combineReducers({
    auth,
    navigation,
    yearbook,
    components,
    category
})
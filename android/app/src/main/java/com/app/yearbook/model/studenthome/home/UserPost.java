package com.app.yearbook.model.studenthome.home;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class UserPost{

	@SerializedName("post_list")
	private List<PostListItem> postList;

	@SerializedName("staff_list")
	private List<StaffListItem> staffList;

	public void setPostList(List<PostListItem> postList){
		this.postList = postList;
	}

	public List<PostListItem> getPostList(){
		return postList;
	}

	public void setStaffList(List<StaffListItem> staffList){
		this.staffList = staffList;
	}

	public List<StaffListItem> getStaffList(){
		return staffList;
	}
}
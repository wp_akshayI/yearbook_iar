import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router"

import auth from "./Auth";
import yearbook from "./Yearbook";
import navigation from "./Navigation";
import components from "./Components";
import category from "./Category";

export default (history) => combineReducers({
    router: connectRouter(history),
    auth,
    navigation,
    yearbook,
    components,
    category
});

import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    View,
    StatusBar,
    FlatList,
    ActivityIndicator,
    Text,
    Dimensions,
    StyleSheet
} from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import Actions from '../actions';
import SectionList from './components/SectionList';
import { colors } from '../utils';

Category.navigationOptions = ({ navigation }) => {
    let headerTitle = navigation.getParam("title") || "Category";
    return {
        headerTitle
    }
}
export default function Category(props) {
    const dispatch = useDispatch();

    const [state, setState] = useState({
        data: undefined,
        screenId: props.navigation.getParam("screenId")
    })

    useEffect(() => {
        _getPage();
    }, [])

    const _getPage = async () => {
        let data = await dispatch(Actions.get_screen_detail(state.screenId));
        setState({ ...state, data: data || [] });
    }

    const renderItem = ({ item, index }) => {
        return <SectionList data={item} screenId={state.screenId} />
    }

    const renderEmpty = () => {
        return (
            <View style={styles.emptyContainer}>
                {
                    state.data ? (
                        <Text style={styles.emptyTitle}>
                            Looks like this Section is not ready yet
                        </Text>
                    ) : (
                            <ActivityIndicator />
                        )
                }
            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: colors.app_background }}>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={{ flex: 1 }}>
                <FlatList
                    data={state.data || []}
                    renderItem={renderItem}
                    keyExtractor={(item) => String(item.component_id)}
                    ListEmptyComponent={renderEmpty}
                />
            </SafeAreaView>
        </View>
    );
};

const styles = StyleSheet.create({
    emptyContainer: {
        height: Dimensions.get("screen").height - 80,
        justifyContent: "center",
        alignItems: "center"
    },
    emptyTitle: {
        fontSize: 14,
        color: "grey"
    }
})

package com.app.yearbook.model.studenthome;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.app.yearbook.BR;
import com.app.yearbook.model.staff.yearbook.UserTagg;

import java.io.Serializable;
import java.util.List;

public class PostList extends BaseObservable implements Serializable  {

    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("post_file_type")
    @Expose
    private String postFileType;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("post_file")
    @Expose
    private String postFile;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("user_firstname")
    @Expose
    private String userFirstname;
    @SerializedName("user_lastname")
    @Expose
    private String userLastname;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("post_description")
    @Expose
    private String postDescription;
    @SerializedName("post_thumbnail")
    @Expose
    private String postThumbnail;
    @SerializedName("post_like")
    @Expose
    public String postLike;
    @SerializedName("post_bookmark")
    @Expose
    private String postBookmark;
    @SerializedName("post_push")
    @Expose
    private String postPush;
    @SerializedName("total_like")
    @Expose
    public String totalLike;
    @SerializedName("total_comment")
    @Expose
    private String totalComment;

    @SerializedName("total_repoted")
    @Expose
    private String totalRepoted;

    //-----
    @SerializedName("post_comment")
    @Expose
    private String postComment;

    @SerializedName("staff_id")
    @Expose
    private String staffId;

    @SerializedName("category_id")
    @Expose
    private String categoryId;

    @SerializedName("file_type")
    @Expose
    private String fileType;

    @SerializedName("file_size_type")
    @Expose
    private String fileSizeType;
    @SerializedName("file_position")
    @Expose
    private String filePosition;

    @SerializedName("user_tagg")
    @Expose
    private List<UserTagg> userTagg = null;

    @SerializedName("flag")
    @Expose
    private String flag;

    @SerializedName("post_notification")
    @Expose
    private String postNotification;

    @SerializedName("post_title")
    @Expose
    private String postTitle;

    @Bindable
    public String getTotalRepoted() {
        return totalRepoted;
    }

    public void setTotalRepoted(String totalRepoted) {
        this.totalRepoted = totalRepoted;
        notifyPropertyChanged(BR.totalRepoted);
    }

    @Bindable
    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
        notifyPropertyChanged(BR.postId);
    }

    @Bindable
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        notifyPropertyChanged(BR.userId);
    }

    @Bindable
    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
        notifyPropertyChanged(BR.schoolId);
    }

    @Bindable
    public String getPostFileType() {
        return postFileType;
    }

    public void setPostFileType(String postFileType) {
        this.postFileType = postFileType;
        notifyPropertyChanged(BR.postFileType);
    }

    @Bindable
    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
        notifyPropertyChanged(BR.postType);
    }

    @Bindable
    public String getPostFile() {
        return postFile;
    }

    public void setPostFile(String postFile) {
        this.postFile = postFile;
        notifyPropertyChanged(BR.postFile);
    }

    @Bindable
    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
        notifyPropertyChanged(BR.postDate);
    }

    @Bindable
    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
        notifyPropertyChanged(BR.userFirstname);

    }

    @Bindable
    public String getUserLastname() {
        return userFirstname +" "+userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
        notifyPropertyChanged(BR.userLastname);
    }

    @Bindable
    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
        notifyPropertyChanged(BR.userImage);
    }

    @Bindable
    public String getPostDescription() {
        return postDescription;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
        notifyPropertyChanged(BR.postDescription);
    }

    @Bindable
    public String getPostThumbnail() {
        return postThumbnail;
    }

    public void setPostThumbnail(String postThumbnail) {
        this.postThumbnail = postThumbnail;
        notifyPropertyChanged(BR.postThumbnail);
    }

    @Bindable
    public String getPostLike() {
        return postLike;
    }

    public void setPostLike(String postLike) {
        this.postLike = postLike;
        notifyPropertyChanged(BR.postLike);
    }

    @Bindable
    public String getPostBookmark() {
        return postBookmark;
    }

    public void setPostBookmark(String postBookmark) {
        this.postBookmark = postBookmark;
        notifyPropertyChanged(BR.postBookmark);
    }

    @Bindable
    public String getPostPush() {
        return postPush;
    }

    public void setPostPush(String postPush) {
        this.postPush = postPush;
        notifyPropertyChanged(BR.postPush);
    }

    @Bindable
    public String getTotalLike() {
        return totalLike;
    }

    public void setTotalLike(String totalLike) {
        this.totalLike = totalLike;
       notifyPropertyChanged(BR.totalLike);
    }

    @Bindable
    public String getTotalComment() {
        return totalComment;
    }

    public void setTotalComment(String totalComment) {
        this.totalComment = totalComment;
        notifyPropertyChanged(BR.totalComment);
    }

    @Bindable
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
        notifyPropertyChanged(BR.staffId);
    }

    @Bindable
    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
        notifyPropertyChanged(BR.categoryId);
    }

    @Bindable
    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
        notifyPropertyChanged(BR.fileType);
    }

    @Bindable
    public String getFileSizeType() {
        return fileSizeType;
    }

    public void setFileSizeType(String fileSizeType) {
        this.fileSizeType = fileSizeType;
        notifyPropertyChanged(BR.fileSizeType);
    }

    @Bindable
    public String getFilePosition() {
        return filePosition;
    }

    public void setFilePosition(String filePosition) {
        this.filePosition = filePosition;
        notifyPropertyChanged(BR.filePosition);
    }

    @Bindable
    public List<UserTagg> getUserTagg() {
        return userTagg;
    }

    public void setUserTagg(List<UserTagg> userTagg) {
        this.userTagg = userTagg;
        notifyPropertyChanged(BR.userTagg);
    }

    @Bindable
    public String getPostComment() {
        return postComment;
    }

    public void setPostComment(String postComment) {
        this.postComment = postComment;
        notifyPropertyChanged(BR.postComment);
    }

    @Bindable
    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
        notifyPropertyChanged(BR.flag);
    }

    @Bindable
    public String getPostNotification() {
        return postNotification;
    }

    public void setPostNotification(String postNotification) {
        this.postNotification = postNotification;
        notifyPropertyChanged(BR.postNotification);
    }

    @Bindable
    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
        notifyPropertyChanged(BR.postTitle);
    }

    //    public PostList(String postLike, String totalLike) {
//        this.postLike = postLike;
//        this.totalLike = totalLike;
//    }

}

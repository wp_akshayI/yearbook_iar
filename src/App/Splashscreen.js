import React, { useEffect, useState } from 'react'
import {
  NativeModules,
  ActivityIndicator,
  View,
  Platform,
  Image,
  Text
} from 'react-native'
import { useDispatch } from 'react-redux'
import Actions from '../actions'
import { StackActions, NavigationActions } from 'react-navigation'
import { menuIcon } from '../assets'
import { openDrawer } from '../Native'
import { colors } from '../utils'
import { HeaderBackButton } from 'react-navigation-stack'
import AppHeader from '../components/AppHeader'

export default function Splashscreen (props) {
  const dispatch = useDispatch()

  const [state, setState] = useState({
    message: null
  })

  useEffect(() => {
    getLoginToken()
  }, [])

  const getLoginToken = () => {
    console.log(props.screenProps)
    if (Platform.OS === 'ios') {
      console.log('iOS user token :-', props.screenProps)
      dispatch(Actions.setAuthToken(props.screenProps.token))
      return _navigate('YearbookDetails', { title: props.screenProps.yearbook_name, screenId: props.screenProps.screen_id })
      // _check_yearbook_publish()
    } else {
      NativeModules.AppModules.getToken((token, type) => {
        console.log('Android user token:- ', token)
        dispatch(Actions.setAuthToken(token))
        NativeModules.AppModules.getLastYearbookClicked((data) => {
          data = JSON.parse(data)
          return _navigate('YearbookDetails', { title: data.yearbook_name, screenId: data.screen_id })
        })
      })
    }
  }

  const _check_yearbook_publish = async () => {
    let data = await dispatch(Actions.check_yearbook_publish())
    if (data.Result === false) {
      return setState({ ...state, message: data.ResponseMsg })
    }
    data = data.Data
    // if (data.is_publish === 0 || data.is_publish === "0") {
    //     return setState({ ...state, message: "Looks like the yearbooks is not ready yet, please notify your school." });
    // } else if (data.is_purchase === 0 || data.is_purchase === "0") {
    //     return setState({ ...state, message: "You don't have a purchased yearbook!" });
    // }
    // if (data.user_type === "student") {
    //     dispatch(Actions.set_yearbook(data.yearbook_id));
    //     return _navigate("YearbookDetails", { sections: data.sections, screenId: data.screen_id });
    // }
    return _navigate('Yearbooks', { Yearbookslist: data.yearbook_list })
  }

  const _navigate = (root, params) => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: root, params })]
    })
    return props.navigation.dispatch(resetAction)
  }

  return <View style={{ flex: 1, backgroundColor: colors.app_background }} />

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.app_background
      }}
    >
      {Platform.OS === 'android' && <AppHeader />}
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        {
          state.message === null || state.message === ''
            ? (
              <ActivityIndicator size='large' />
            )
            : (
              <Text style={{ textAlign: 'center', marginHorizontal: 40 }}>
                {state.message}
              </Text>
            )
        }
      </View>
    </View>
  )
};

Splashscreen.navigationOptions = () => {
  return {
    header: null
  }
  // if (Platform.OS === 'android') {
  //   return {
  //     header: null
  //   }
  // }
  // return {
  //   headerTitle: 'Yearbook',
  //   headerLeft: (
  //     <HeaderBackButton
  //       onPress={() => { openDrawer() }}
  //       backImage={(
  //         <Image
  //           source={menuIcon}
  //           style={{ height: 20, width: 20, marginLeft: 15 }}
  //           resizeMode='contain'
  //         />
  //       )}
  //     />
  //   )
  // }
}

package com.app.yearbook;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.app.yearbook.databinding.ActivityUserWebProfileBinding;
import com.app.yearbook.model.StaffStudent;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.BaseUrl;
import com.app.yearbook.utils.Constants;
import com.github.ybq.android.spinkit.style.Circle;

import org.json.JSONException;
import org.json.JSONObject;

public class UserProcessWithWeb extends AppCompatActivity {

    private Toolbar toolbar;
    public ProgressDialog progressDialog;
    private AllMethods allMethods;
    private LoginUser loginUserSP;
    private TextView toolbar_title;
    private ActivityUserWebProfileBinding mBinding;
    private String currentActionOfPage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_web_profile);
        setView();
    }

    public void setView() {
        //common methods
//        allMethods = new AllMethods();

        if (getIntent().hasExtra("action")) {
            currentActionOfPage = getIntent().getStringExtra("action");
        } else
            currentActionOfPage = Constants.USER_WEB_PROFILE;

        //toolbar
        setToolbar();

        //SP
        loginUserSP = new LoginUser(UserProcessWithWeb.this);

        progressDialog = ProgressDialog.show(UserProcessWithWeb.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        setClient();

        WebSettings webSettings = mBinding.webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mBinding.webview.loadUrl(getGeneratedUrl());
        mBinding.webview.getSettings().setUserAgentString("Mozilla/5.0 (iPhone; CPU iPhone OS 9_3 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13E233 Safari/601.1");
        mBinding.webview.addJavascriptInterface(new WebAppInterface(this), "Android");

        /*String datta = "<input type=\"button\" value=\"Say hello\" onClick=\"showAndroidToast('Hello Android!')\" />\n" +
                "\n" +
                "<script type=\"text/javascript\">\n" +
                "    function showAndroidToast(toast) {\n" +
                "        Android.showToast(toast);\n" +
                "    }\n" +
                "</script>";
        mBinding.webview.loadData(datta,"text/html", "UTF-8");*/
    }

    private void setClient() {
        mBinding.webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e("url", url);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(UserProcessWithWeb.this, description, Toast.LENGTH_SHORT).show();
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });
    }

    private String getGeneratedUrl() {
        if (currentActionOfPage.equalsIgnoreCase(Constants.USER_TERMS))
            return new BaseUrl().getHost() + "/termsofuse";
        else if (currentActionOfPage.equalsIgnoreCase(Constants.USER_PRIVACY))
            return new BaseUrl().getHost() + "/privacypolicy";
        else if (currentActionOfPage.equalsIgnoreCase(Constants.USER_WEB_PAYMENT))
            return new BaseUrl().getHost() + "/auth/direct_auth/" + loginUserSP.getLoginUserToken() + "?cmd=purchase";
        else
            return new BaseUrl().getHost() + "/auth/direct_auth/" + loginUserSP.getLoginUserToken() + "?cmd=profile";
    }

    private String getToolbarTitle() {
        if (currentActionOfPage.equalsIgnoreCase(Constants.USER_TERMS))
            return "Terms of Use";
        else if (currentActionOfPage.equalsIgnoreCase(Constants.USER_PRIVACY))
            return "Privacy Policy";
        else if (currentActionOfPage.equalsIgnoreCase(Constants.USER_WEB_PAYMENT))
            return "";
        else
            return "";
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getToolbarTitle());
//        toolbar_title.setText("Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        if (!currentActionOfPage.equalsIgnoreCase(Constants.USER_WEB_PAYMENT)) {
            super.onBackPressed();
        } else {
            if (getIntent().getBooleanExtra("fromYBList", false)) {
                finish();
            }else {
                startActivity(new Intent(this, LoginActivity.class));
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public class WebAppInterface {
        Context mContext;

        /**
         * Instantiate the interface and set the context
         */
        WebAppInterface(Context c) {
            mContext = c;
        }

        /**
         * Show a toast from the web page
         */
        @JavascriptInterface
        public void showToast(String toast) {
            Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        }

        /**
         * Stating here...
         */
        @JavascriptInterface
        public void paymentCallBackAction(String jsonObj) {
//            Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
            /*Intent i = new Intent(UserProcessWithWeb.this, StudentHomeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();*/
            try {
                Log.e("callback", jsonObj);
                JSONObject jsonObject = new JSONObject(jsonObj);
                if (!jsonObject.getBoolean("status")) {
                    return;
                }
                StaffStudent dataObj = loginUserSP.getUserData();
                dataObj.setIsYearbookPurchased("Yes");
                loginUserSP.setUserData(dataObj);
                if (loginUserSP.getUserData().getUserIsAgree().equalsIgnoreCase("2")) {
                    Intent i = new Intent(UserProcessWithWeb.this, TermsConditionActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                            Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                } else {
                    if (getIntent().getBooleanExtra("fromYBList", false)) {
                        finish();
                    } else {
                        Intent i = new Intent(UserProcessWithWeb.this, StudentHomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

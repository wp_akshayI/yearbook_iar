import { Platform } from 'react-native'
import { action } from '../utils'
import { postResponse, failerResponse } from '../utils/funtions'
import { push } from 'connected-react-router'

// Yearbook All categories list
export function get_category_list () {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    const { yearbookId } = getState().yearbook
    if (Platform.OS === 'web' && !yearbookId || yearbookId === null) return dispatch(push('/app/yearbooks'))
    const request = {
      Token: authUser,
      yearbook_id: yearbookId
    }
    const res = await postResponse('get_category_list.php', request)
    if (res.Result === false) failerResponse('get_category_list', res)
    else {
      dispatch({
        type: action.get_categories,
        categories: res.Data
      })
      return res.Data
    }
  }
}

//  add new category in yearbook
export function add_category (name) {
  return async (dispatch, getState) => {
    dispatch({ type: action.open_notification, message: 'Please wait...', variant: 'loader' })
    const { authUser } = getState().auth
    const { yearbookId } = getState().yearbook
    if (!yearbookId || yearbookId === null) return dispatch(push('/app/yearbooks'))
    const request = {
      Token: authUser,
      yearbook_id: yearbookId,
      category_name: name
    }
    const res = await postResponse('add_category.php', request)
    if (res.Result) {
      dispatch(get_category_list())
      dispatch({ type: action.open_notification, message: 'Category added successfully!', variant: 'success' })
    } else {
      dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: 'error' })
      failerResponse('add_category', res)
    }
    return res
  }
}

// subcategory list by category id
export function get_sub_category_list (category_id) {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      category_id
    }
    const res = await postResponse('get_sub_category_list.php', request)
    if (res.Result === false) failerResponse('get_sub_category_list', res)
    dispatch({ type: action.get_gride_sub_category, subCategories: res.Data })
    return res.Data
  }
}

//  add new sub category
export function add_sub_category (category_id, sub_category_name) {
  return async (dispatch, getState) => {
    dispatch({ type: action.open_notification, message: 'Please wait...', variant: 'loader' })
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      category_id,
      sub_category_name
    }
    const res = await postResponse('add_sub_category.php', request)
    if (res.Result) {
      dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: 'success' })
    } else {
      dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: 'error' })
      failerResponse('add_sub_category', res)
    }
    return res
  }
}

export function delete_screen (screen_id) {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      screen_id
    }
    const res = await postResponse('delete_screen_types.php', request)
    if (res.Result) {
      dispatch(get_category_list())
      dispatch({ type: action.open_notification, message: 'Deleted successfully!', variant: 'success' })
    }
    return res.Result
  }
}

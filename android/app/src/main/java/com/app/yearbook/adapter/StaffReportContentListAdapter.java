package com.app.yearbook.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.makeramen.roundedimageview.RoundedImageView;
import com.app.yearbook.ImageActivity;
import com.app.yearbook.R;
import com.app.yearbook.model.staff.home.ContentList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class StaffReportContentListAdapter extends RecyclerView.Adapter<StaffReportContentListAdapter.MyViewHolder> {
    private ArrayList<ContentList> contentLists;
    private Activity ctx;

    @NonNull
    @Override
    public StaffReportContentListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contentreport_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final StaffReportContentListAdapter.MyViewHolder holder, final int position) {
        final ContentList userList = contentLists.get(position);
        holder.tvUserComment.setText(userList.getRepotedMessage() + "");
        holder.tvUserName.setText(userList.getUserFirstname() + "  " + userList.getUserLastname());

        if (userList.getUserImage() != null) {
            Glide.with(ctx)
                    .load(userList.getUserImage())
                    .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.mipmap.profile_placeholder)
                    .error(R.mipmap.profile_placeholder)
                    .into(holder.imgUserPhoto);

            holder.imgUserPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, ImageActivity.class);
                    i.putExtra("FileUrl", userList.getUserImage());
                    ctx.startActivity(i);
                }
            });
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("CST6CDT"));

        Date date = null;
        try {

            date = sdf.parse(userList.getRepotedPostCreated());
            sdf.setTimeZone(TimeZone.getDefault());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat sameDateSDF = new SimpleDateFormat("dd-MM-yyyy");
        sameDateSDF.setTimeZone(TimeZone.getTimeZone("CST6CDT"));

        sameDateSDF.setTimeZone(TimeZone.getDefault());
        String DBDate = sameDateSDF.format(date);
        String currentDate = sameDateSDF.format(new Date());
        Log.d("TTT", "Date: " + DBDate + " / " + currentDate);

        if (DBDate.equalsIgnoreCase(currentDate)) {
            //same date
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //dateFormat.setTimeZone(TimeZone.getTimeZone("CST"));
            Date currDate = new Date();

            //dateFormat.setTimeZone(TimeZone.getDefault());
            String dd = dateFormat.format(currDate);
            Log.d("TTT", "Dateeeee: " + dd + " / " + DBDate);

            try {
                currDate = dateFormat.parse(dd);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long different = currDate.getTime() - date.getTime();
            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            Log.d("TTT", "DDDD : " + elapsedDays + " / " + elapsedHours + " / " + elapsedMinutes + " / " + elapsedSeconds);

            if (elapsedHours < 1) {
                holder.tvUserTime.setText(elapsedMinutes + "m");
            } else {
                holder.tvUserTime.setText(elapsedHours + "h");
            }
        } else {
            //not same
            String outputPattern = "MMM dd, yyyy -hh:mm aa";
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            String str = null;
            str = outputFormat.format(date);

            holder.tvUserTime.setText(str + "");
        }

        holder.tvUserComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.tvUserComment.getLineCount() > 1) {
                    androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(ctx);
                    LayoutInflater inflater = ctx.getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
                    dialogBuilder.setView(dialogView);
                    final androidx.appcompat.app.AlertDialog alert = dialogBuilder.create();
                    alert.setCancelable(false);
                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                    LinearLayout lvNo = dialogView.findViewById(R.id.lvNo);
                    View view = dialogView.findViewById(R.id.view);

                    view.setVisibility(View.GONE);
                    lvNo.setVisibility(View.GONE);

                    TextView tvYes = dialogView.findViewById(R.id.tvYes);
                    tvYes.setTextColor(ctx.getResources().getColor(R.color.black));
                    tvYes.setText("Ok");

                    tvYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
                    tvMessage.setText(userList.getRepotedMessage() + "");

                    alert.show();
                }

            }
        });


    }

    public StaffReportContentListAdapter(ArrayList<ContentList> signatureLists, Activity context) {
        this.contentLists = signatureLists;
        this.ctx = context;
    }

    @Override
    public int getItemCount() {
        return contentLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvUserComment, tvUserName, tvUserTime;
        public RoundedImageView imgUserPhoto;

        public MyViewHolder(View view) {
            super(view);
            tvUserComment = view.findViewById(R.id.tvUserComment);
            tvUserName = view.findViewById(R.id.tvUserName);
            tvUserTime = view.findViewById(R.id.tvUserTime);
            imgUserPhoto = view.findViewById(R.id.imgUserImage);
            tvUserTime.setVisibility(View.VISIBLE);
        }
    }
}

package com.app.yearbook;

import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.app.yearbook.utils.ZoomableImageView;

public class ImageActivity extends AppCompatActivity {

    private String fileUrl;
    private ProgressBar progressBar;
    private ZoomableImageView touch;
    private TextView tvCaption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

//        Blurry.with(ImageActivity.this)
//                .radius(10)
//                .sampling(8)
//                .color(Color.argb(66, 255, 255, 0))
//                .async()
//                .animate(500)
//                .onto((ViewGroup) findViewById(R.id.content));
//
//
//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);

        // setView();
    }

    public void setView()
    {
        tvCaption=findViewById(R.id.tvCaption);
        if(getIntent().getExtras()!=null)
        {
            fileUrl=getIntent().getStringExtra("FileUrl");
            Log.d("TTT", "setView: "+fileUrl);
            touch = findViewById(R.id.imgPost);
            progressBar=findViewById(R.id.progress);

            Glide.with(ImageActivity.this)
                    .load(fileUrl)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.mipmap.home_post_img_placeholder)
                    .error(R.mipmap.home_post_img_placeholder)
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(touch);

            if(getIntent().getStringExtra("caption")!=null) {
                String caption = getIntent().getStringExtra("caption");
                tvCaption.setText(caption);
            }


        }
        else
        {
            Toast.makeText(getApplicationContext(),"Image not found",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

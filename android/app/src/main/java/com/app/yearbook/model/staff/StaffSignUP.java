package com.app.yearbook.model.staff;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.app.yearbook.model.StaffUser;

public class StaffSignUP {
    @SerializedName("ResponseCode")
    @Expose
    private String responseCode;
    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private String result;
    @SerializedName("Data")
    @Expose
    private StaffUser data;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public StaffUser getData() {
        return data;
    }

    public void setData(StaffUser data) {
        this.data = data;
    }
}

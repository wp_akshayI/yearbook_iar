const img_section_gradient = require('./img_section_gradient.png')
const published = require('./published.png')
const UnAthorized_ic = require('./UnAthorized_ic.png')
const warning_ic = require('./warning_ic.jpg')
const bookmark_ic = require('./icons/bookmark_ic.png')
const bookmark_fill_ic = require('./icons/bookmark_fill_ic.png')
const diamond_ic = require('./icons/diamond_ic.png')
const cancelIcon = require('./icons/cancel_ic.png')
const diamond_ic_selected = require('./icons/diamond_ic_selected.png')
const pause_ic = require('./icons/pause_ic.png')
const play_ic = require('./icons/play.png')
const menuIcon = require('./icons/menu_ic.png')
const arrow_right = require('./icons/arrow_right.png')
const down_arrow_ic = require('./icons/down_arrow_ic.png')
const up_arrow_ic = require('./icons/up_arrow_ic.png')
const image_placeholder = require('./image_placeholder.png')
const nativePlaceholder = require('./placeholder/placeholder.png')

export {
  img_section_gradient,
  bookmark_ic,
  bookmark_fill_ic,
  diamond_ic,
  diamond_ic_selected,
  pause_ic,
  play_ic,
  menuIcon,
  published,
  UnAthorized_ic,
  warning_ic,
  arrow_right,
  down_arrow_ic,
  up_arrow_ic,
  image_placeholder,
  nativePlaceholder,
  cancelIcon
}

package com.app.yearbook;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.EmpSchoolAdapter;
import com.app.yearbook.databinding.ActivityEmployeeHomeBinding;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.employee.GetSchool;
import com.app.yearbook.model.employee.SchoolListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeHomeActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public String empName = "";
    private LoginUser loginUser;
    private TextView toolbar_title;
    private ProgressDialog progressDialog;
    ActivityEmployeeHomeBinding binding;
    private AllMethods utils;
    private ArrayList<SchoolListItem> getSchoolList;
    private AllMethods allMethods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_employee_home);
        binding=DataBindingUtil.setContentView(this,R.layout.activity_employee_home);
        setView();
    }

    public void setView() {

        utils=new AllMethods();
        allMethods=new AllMethods();

        //SP object
        loginUser = new LoginUser(EmployeeHomeActivity.this);
        if (loginUser.getUserData() != null) {
            empName = loginUser.getUserData().getEmployeeFirstname() + " " + loginUser.getUserData().getEmployeeLastname();
            Log.d("TTT", "setView: emp name: "+empName);
        }

        //set toolbar
        setToolbar();

        //get school
        getAllSchool();

    }

    public void getAllSchool()
    {
        //Progress bar
        progressDialog=ProgressDialog.show(EmployeeHomeActivity.this,"","",true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar=progressDialog.findViewById(R.id.progress);
        Circle bounce=new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetSchool> schoolList= retrofitClass.getEmpSchool();
        schoolList.enqueue(new Callback<GetSchool>() {
            @Override
            public void onResponse(Call<GetSchool> call, Response<GetSchool> response) {
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }

                Log.d("TTT","response code: "+response.body().getResponseCode());
                if(response.body().getResponseCode().equals("1")) {
                    if (response.body().getSchoolList().size() != 0 && response.body().getSchoolList() != null) {
                        binding.lvNoData.setVisibility(View.GONE);
                        getSchoolList = new ArrayList<>();
                        getSchoolList.addAll(response.body().getSchoolList());
                        Log.d("TTT", "Data: " + getSchoolList.size());
                        EmpSchoolAdapter schoolAdapter = new EmpSchoolAdapter(getSchoolList, EmployeeHomeActivity.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        binding.rvSearchSchool.setLayoutManager(mLayoutManager);
                        binding.rvSearchSchool.setAdapter(schoolAdapter);
                        schoolAdapter.notifyDataSetChanged();
                    } else {
                        binding.lvNoData.setVisibility(View.VISIBLE);
                        Log.d("TTT", "School not found.");
                    }
                }
                else
                {
                    binding.lvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<GetSchool> call, Throwable t) {
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
                utils.setAlert(EmployeeHomeActivity.this,"",t.getMessage());

            }
        });
    }


    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(empName);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.setting) {
            logout();
        }
        return super.onOptionsItemSelected(item);
    }


    public void logout()
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(EmployeeHomeActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle("");
        dialog.setMessage("Are you sure you want to logout ?");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
                dialog.dismiss();

                String token = PreferenceManager.getDefaultSharedPreferences(EmployeeHomeActivity.this).getString("Token", "");
                Log.d("TTT", "token: " + token);

                if (!token.equals("")) {
                    progressDialog = ProgressDialog.show(EmployeeHomeActivity.this, "", "", true);
                    progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    progressDialog.setContentView(R.layout.progress_view);
                    ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                    Circle bounce = new Circle();
                    bounce.setColor(Color.BLACK);
                    progressBar.setIndeterminateDrawable(bounce);

                    RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                    final Call<GiveReport> userPost = retrofitClass.logout(token, "employee");
                    userPost.enqueue(new Callback<GiveReport>() {
                        @Override
                        public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            LoginUser loginSP = new LoginUser(EmployeeHomeActivity.this);
                            loginSP.clearData();

                            Intent i = new Intent(EmployeeHomeActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        }

                        @Override
                        public void onFailure(Call<GiveReport> call, Throwable t) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            allMethods.setAlert(EmployeeHomeActivity.this, "", t.getMessage() + "");
                        }
                    });
                }
                else
                {
                    LoginUser loginSP = new LoginUser(EmployeeHomeActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(EmployeeHomeActivity.this, LoginActivity.class);
                    startActivity(i);
                   finish();
                }
            }


        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog alert = dialog.create();
        alert.show();

    }
}

package com.app.yearbook.model.employee;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class UserListItem implements Serializable {

	@SerializedName("user_device_token")
	private String userDeviceToken;

	@SerializedName("parent_email")
	private String parentEmail;

	@SerializedName("user_password")
	private String userPassword;

	@SerializedName("user_access_code")
	private String userAccessCode;

	@SerializedName("user_device_type")
	private String userDeviceType;

	@SerializedName("user_is_suspend")
	private String userIsSuspend;

	@SerializedName("user_lastname")
	private String userLastname;

	@SerializedName("user_address")
	private String userAddress;

	@SerializedName("user_created")
	private String userCreated;

	@SerializedName("user_grade")
	private String userGrade;

	@SerializedName("user_year")
	private String userYear;

	@SerializedName("user_type")
	private String userType;

	@SerializedName("school_id")
	private String schoolId;

	@SerializedName("is_access_code_verify")
	private String isAccessCodeVerify;

	@SerializedName("user_student_id")
	private String userStudentId;

	@SerializedName("user_phone")
	private String userPhone;

	@SerializedName("user_token")
	private String userToken;

	@SerializedName("user_status")
	private String userStatus;

	@SerializedName("parent_lastname")
	private String parentLastname;

	@SerializedName("user_email")
	private String userEmail;

	@SerializedName("user_is_agree")
	private String userIsAgree;

	@SerializedName("user_image")
	private String userImage;

	@SerializedName("user_firstname")
	private String userFirstname;

	@SerializedName("user_registered")
	private String userRegistered;

	@SerializedName("user_birthdate")
	private String userBirthdate;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("parent_phone")
	private String parentPhone;

	@SerializedName("parent_firstname")
	private String parentFirstname;

	@SerializedName("deleted_time")
	private String deletedTime;

	@SerializedName("user_images")
	private List<String> userImages;

	private String userSinglePhoto;


	public void setUserDeviceToken(String userDeviceToken){
		this.userDeviceToken = userDeviceToken;
	}

	public String getUserDeviceToken(){
		return userDeviceToken;
	}

	public void setParentEmail(String parentEmail){
		this.parentEmail = parentEmail;
	}

	public String getParentEmail(){
		return parentEmail;
	}

	public void setUserPassword(String userPassword){
		this.userPassword = userPassword;
	}

	public String getUserPassword(){
		return userPassword;
	}

	public void setUserAccessCode(String userAccessCode){
		this.userAccessCode = userAccessCode;
	}

	public String getUserAccessCode(){
		return userAccessCode;
	}

	public void setUserDeviceType(String userDeviceType){
		this.userDeviceType = userDeviceType;
	}

	public String getUserDeviceType(){
		return userDeviceType;
	}

	public void setUserIsSuspend(String userIsSuspend){
		this.userIsSuspend = userIsSuspend;
	}

	public String getUserIsSuspend(){
		return userIsSuspend;
	}

	public void setUserLastname(String userLastname){
		this.userLastname = userLastname;
	}

	public String getUserLastname(){
		return userLastname;
	}

	public void setUserAddress(String userAddress){
		this.userAddress = userAddress;
	}

	public String getUserAddress(){
		return userAddress;
	}

	public void setUserCreated(String userCreated){
		this.userCreated = userCreated;
	}

	public String getUserCreated(){
		return userCreated;
	}

	public void setUserGrade(String userGrade){
		this.userGrade = userGrade;
	}

	public String getUserGrade(){
		return userGrade;
	}

	public void setUserYear(String userYear){
		this.userYear = userYear;
	}

	public String getUserYear(){
		return userYear;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setSchoolId(String schoolId){
		this.schoolId = schoolId;
	}

	public String getSchoolId(){
		return schoolId;
	}

	public void setIsAccessCodeVerify(String isAccessCodeVerify){
		this.isAccessCodeVerify = isAccessCodeVerify;
	}

	public String getIsAccessCodeVerify(){
		return isAccessCodeVerify;
	}

	public void setUserStudentId(String userStudentId){
		this.userStudentId = userStudentId;
	}

	public String getUserStudentId(){
		return userStudentId;
	}

	public void setUserPhone(String userPhone){
		this.userPhone = userPhone;
	}

	public String getUserPhone(){
		return userPhone;
	}

	public void setUserToken(String userToken){
		this.userToken = userToken;
	}

	public String getUserToken(){
		return userToken;
	}

	public void setUserStatus(String userStatus){
		this.userStatus = userStatus;
	}

	public String getUserStatus(){
		return userStatus;
	}

	public void setParentLastname(String parentLastname){
		this.parentLastname = parentLastname;
	}

	public String getParentLastname(){
		return parentLastname;
	}

	public void setUserEmail(String userEmail){
		this.userEmail = userEmail;
	}

	public String getUserEmail(){
		return userEmail;
	}

	public void setUserIsAgree(String userIsAgree){
		this.userIsAgree = userIsAgree;
	}

	public String getUserIsAgree(){
		return userIsAgree;
	}

	public void setUserImage(String userImage){
		this.userImage = userImage;
	}

	public String getUserImage(){
		return userImage;
	}

	public void setUserFirstname(String userFirstname){
		this.userFirstname = userFirstname;
	}

	public String getUserFirstname(){
		return userFirstname;
	}

	public void setUserRegistered(String userRegistered){
		this.userRegistered = userRegistered;
	}

	public String getUserRegistered(){
		return userRegistered;
	}

	public void setUserBirthdate(String userBirthdate){
		this.userBirthdate = userBirthdate;
	}

	public String getUserBirthdate(){
		return userBirthdate;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setParentPhone(String parentPhone){
		this.parentPhone = parentPhone;
	}

	public String getParentPhone(){
		return parentPhone;
	}

	public void setParentFirstname(String parentFirstname){
		this.parentFirstname = parentFirstname;
	}

	public String getParentFirstname(){
		return parentFirstname;
	}

	public void setDeletedTime(String deletedTime){
		this.deletedTime = deletedTime;
	}

	public String getDeletedTime(){
		return deletedTime;
	}

	public void setUserImages(List<String> userImages){
		this.userImages = userImages;
	}

	public List<String> getUserImages(){
		return userImages;
	}

	public String getUserSinglePhoto() {
		return userSinglePhoto;
	}

	public void setUserSinglePhoto(String userSinglePhoto) {
		this.userSinglePhoto = userSinglePhoto;
	}

	public UserListItem(String userFirstname, String userLastname) {
		this.userLastname = userLastname;
		this.userFirstname = userFirstname;
	}

	public UserListItem() {
	}
}
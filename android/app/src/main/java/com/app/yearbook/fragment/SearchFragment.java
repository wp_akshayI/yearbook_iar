package com.app.yearbook.fragment;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.app.yearbook.R;
import com.app.yearbook.adapter.TabViewPagerAdapter;

public class SearchFragment extends Fragment implements View.OnClickListener{

    public View view;
    private ViewPager mViewPager;
    private TabViewPagerAdapter mViewPagerAdapter;
    private TabLayout mTabLayout;

    private LinearLayout lvRepotrted,lvDeleted;
    private TextView tvReported,tvDeleted;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //((StudentHomeActivity) getActivity()).getSupportActionBar().setTitle("Search");

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search, container, false);
        setHasOptionsMenu(true);
        setView();
        return view;
    }

    public void setView() {
        mViewPager = view.findViewById(R.id.view_pager);
        mViewPagerAdapter = new TabViewPagerAdapter(getChildFragmentManager(), "User");
        mViewPager.setAdapter(mViewPagerAdapter);

       // mTabLayout = view.findViewById(R.id.tab_layout);
        //mTabLayout.setupWithViewPager(mViewPager);

        //linear view
        lvRepotrted=view.findViewById(R.id.lvRepotrted);
        lvRepotrted.setOnClickListener(this);
        lvDeleted=view.findViewById(R.id.lvDeleted);
        lvDeleted.setOnClickListener(this);

        //text view
        tvReported=view.findViewById(R.id.tvReported);
        tvDeleted=view.findViewById(R.id.tvDeleted);

        //default selection
        lvRepotrted.setBackground(getResources().getDrawable(R.drawable.rounded_lineardarkgray));
        lvDeleted.setBackground(getResources().getDrawable(R.drawable.rounded_lineargray));
        tvReported.setTextColor(getResources().getColor(R.color.selectedtitle));
        tvDeleted.setTextColor(getResources().getColor(R.color.deselectTab));

        mViewPager.setCurrentItem(0);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d("TTT","Position: "+position);
                mViewPager.getAdapter().notifyDataSetChanged();
                if(position==0)
                {
                    YoYo.with(Techniques.Shake)
                            .duration(100)
                            .playOn(lvRepotrted);
                    lvRepotrted.setBackground(getResources().getDrawable(R.drawable.rounded_lineardarkgray));
                    lvDeleted.setBackground(getResources().getDrawable(R.drawable.rounded_lineargray));
                    tvReported.setTextColor(getResources().getColor(R.color.selectedtitle));
                    tvDeleted.setTextColor(getResources().getColor(R.color.deselectTab));
                }
                else
                {
                    YoYo.with(Techniques.Shake)
                            .duration(100)
                            .playOn(lvDeleted);
                    lvRepotrted.setBackground(getResources().getDrawable(R.drawable.rounded_lineargray));
                    lvDeleted.setBackground(getResources().getDrawable(R.drawable.rounded_lineardarkgray));
                    tvReported.setTextColor(getResources().getColor(R.color.deselectTab));
                    tvDeleted.setTextColor(getResources().getColor(R.color.selectedtitle));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.lvRepotrted)
        {
            mViewPager.setCurrentItem(0);
            lvRepotrted.setBackground(getResources().getDrawable(R.drawable.rounded_lineardarkgray));
            lvDeleted.setBackground(getResources().getDrawable(R.drawable.rounded_lineargray));
            tvReported.setTextColor(getResources().getColor(R.color.gray));
            tvDeleted.setTextColor(getResources().getColor(R.color.deselectTab));
            YoYo.with(Techniques.Shake)
                    .duration(100)
                    .playOn(lvRepotrted);
        }
        else if(v.getId()==R.id.lvDeleted)
        {
            mViewPager.setCurrentItem(1);
            lvRepotrted.setBackground(getResources().getDrawable(R.drawable.rounded_lineargray));
            lvDeleted.setBackground(getResources().getDrawable(R.drawable.rounded_lineardarkgray));
            tvReported.setTextColor(getResources().getColor(R.color.deselectTab));
            tvDeleted.setTextColor(getResources().getColor(R.color.gray));
            YoYo.with(Techniques.Shake)
                    .duration(100)
                    .playOn(lvDeleted);
        }
    }
}

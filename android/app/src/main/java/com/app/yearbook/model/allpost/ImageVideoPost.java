package com.app.yearbook.model.allpost;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageVideoPost implements Parcelable {
    String url;
    String subject;
    String description;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ImageVideoPost(String url, String subject, String description, String date) {
        this.url = url;
        this.subject = subject;
        this.description = description;
        this.date = date;
    }

    String date;

    public ImageVideoPost(String url, String subject, String description) {
        this.url = url;
        this.subject = subject;
        this.description = description;
    }

    public ImageVideoPost() {
    }

    protected ImageVideoPost(Parcel in) {
        url = in.readString();
        subject = in.readString();
        description = in.readString();
        date = in.readString();
    }

    public static final Creator<ImageVideoPost> CREATOR = new Creator<ImageVideoPost>() {
        @Override
        public ImageVideoPost createFromParcel(Parcel in) {
            return new ImageVideoPost(in);
        }

        @Override
        public ImageVideoPost[] newArray(int size) {
            return new ImageVideoPost[size];
        }
    };

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(subject);
        dest.writeString(description);
        dest.writeString(date);
    }
}

package com.app.yearbook.util;

public interface OnBackPressListener {
    void onBackPress();
}

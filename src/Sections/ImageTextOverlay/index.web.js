import React, { useState, useEffect, useRef } from "react"
import { View, Image, ImageBackground } from "react-native";
import { colors, global } from "../../utils";
import { image_placeholder } from "../../assets";


export default function ImageTextOverlay({ data }) {
    let textOverlay = useRef(null);
    let { x, y, font_color, font_size, font_border, rotation, font_name, component_url, border_color } = data || []
    border_color = border_color || "#000";
    let source = component_url || image_placeholder

    const [state, setState] = useState({
        height: 0
    })

    useEffect(() => {
        textOverlay.style.setProperty("font-family", font_name, "important");
        Image.getSize(data.component_url && data.component_url !== "" ? data.component_url : image_placeholder, (width, height) => {
            setState({ height: global.width * height / width });
        });
    }, [data]);

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground
                source={source}
                style={{ height: state.height, width: "100%" }}
            >
                <span
                    ref={(ref) => textOverlay = ref}
                    style={{
                        fontSize: font_size || 14,
                        color: font_color || colors.black,
                        top: y + "%",
                        left: x + "%",
                        position: "absolute",
                        transform: `rotate(${rotation}deg)`,
                        textShadow: font_border && `-1px -1px 0 ${border_color}, 1px -1px 0 ${border_color}, -1px 1px 0 ${border_color}, 1px 1px 0 ${border_color}`
                    }}
                >
                    {data.component_title}
                </span>
            </ImageBackground>
        </View>
    )
}
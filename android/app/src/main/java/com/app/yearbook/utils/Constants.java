package com.app.yearbook.utils;

//import androidx.core.app.INotificationSideChannel;

import com.app.yearbook.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;


public class Constants {
    /*Dev*/
    public static final String USER_WEB_PROFILE = "action_profile";
    public static final String USER_WEB_PAYMENT = "action_payment";
    public static final String USER_TERMS = "action_conditions";
    public static final String USER_PRIVACY = "action_policy";
    /*/Dev*/
    public static final int SEARCH = 100;
    public static final String UPDATE_PROFILE = "update_profile";
    public static final String FROM = "from";
    public static final String IMAGE_URI = "image_uri";
    public static final String POST_TEXT = "post_text";
    public static final String POST_TYPE = "post_type";
    public static final String POST_POll = "post_poll";
    public static final String GRADE_ITEM = "grade_item";
    public static final SimpleDateFormat displayDateFormat = new SimpleDateFormat("MMM dd, yyyy");
    public static final SimpleDateFormat calFormat=new SimpleDateFormat("MMMM dd, yyyy");
    public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final String POST_IMAGE = "post_image";
    public static final String POST_VIDEO = "post_video";
    public static final int TYPE_BOOKMARK = 101;
    public static final int TYPE_VIEW_IMAGE = 102;
    public static final int TYPE_LIKE = 103;
    public static final int SHOW_PICKER = 104;
    public static final int VIEW_PROFILE = 105;
    public static final String USER_DATA = "user_data";
    public static final String SIGNATURE_DATA = "signature_data";
    public static final String SIGNATURE_TYPE_ADD = "add_signature";
    public static final String SIGNATURE_TYPE_ACCEPT = "signature_accept";

    public static int VIEW_HEADERS = 0;
    public static int VIEW_ITEM = 1;

    public static final int VIEW_OPTION_MENU = 111;
    public static final String INTENT_DATA = "intent_data";
    public static final String INTENT_ISEDIT = "intent_isedit";
    public static final String POST_IMAGE_VIDEO_INT = "1";
    public static final String POST_TEXT_INT = "3";
    public static final String POST_POLL_INT = "4";
    public static final int NEW_POST = 178;

    public static String convertDateFormat(String date, SimpleDateFormat sourceDateFormet, SimpleDateFormat targetDateFormat) {
        Date sourceDate = null;
        try {
            sourceDate = sourceDateFormet.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetDateFormat.format(sourceDate);
    }

    public static String getDisplayDate(String date, SimpleDateFormat sourceDateFormet) {
        SimpleDateFormat output;
        try {
            if (date.endsWith("1") && !date.endsWith("11")) {
                output = new SimpleDateFormat("MMMM d'st', yyyy");
            } else if (date.endsWith("2") && !date.endsWith("12")) {
                output = new SimpleDateFormat("MMMM d'nd', yyyy");
            } else if (date.endsWith("3") && !date.endsWith("13")) {
                output = new SimpleDateFormat("MMMM d'rd', yyyy");
            } else {
                output = new SimpleDateFormat("MMMM d'th', yyyy");
            }

            Date oneWayTripDate = sourceDateFormet.parse(date);                 // parse input
            return output.format(oneWayTripDate);// format output
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    /*public static String getDaysLeft(String inputDateString){
        Calendar calCurr = Calendar.getInstance();
        Calendar day = Calendar.getInstance();
        try {
            day.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(inputDateString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(day.after(calCurr)){
            return String.valueOf((day.get(Calendar.DAY_OF_MONTH) - (calCurr.get(Calendar.DAY_OF_MONTH)))+" days left to vote");
        }
        return "";
    }*/

    //1 minute = 60 seconds
//1 hour = 60 x 60 = 3600
//1 day = 3600 x 24 = 86400

    public static boolean isDaysLeft(String inputDateString) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date c = Calendar.getInstance().getTime();
        Date endDate = null;
        Date startDate = null;
        try {
            endDate = simpleDateFormat.parse(inputDateString);
            String formattedDate = simpleDateFormat.format(c);
            startDate = simpleDateFormat.parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return endDate.getTime()>=startDate.getTime();
    }

    public static String getDaysLeft(String inputDateString) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date c = Calendar.getInstance().getTime();
        Date endDate = null;
        Date startDate = null;
        try {
            endDate = simpleDateFormat.parse(inputDateString);
            String formattedDate = simpleDateFormat.format(c);
            startDate = simpleDateFormat.parse(formattedDate);


        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (endDate.getTime()<startDate.getTime()){
            return "Expired";
        }

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        return elapsedDays + " Days left to Vote";
    }

    String getDayOfMonthSuffix(final int n) {
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static String format(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return format(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + format(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();

    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    public static HashMap<String, Integer> fontMap = new HashMap<String, Integer>() {{
        put("1", R.font.magnificent);
        put("2", R.font.edwardian_scr_itc_tt);
        put("3", R.font.angelface);
        put("4", R.font.arabella);
        put("5", R.font.argentina_script);

        put("6", R.font.im_fell_french_canon_regular);
        put("7", R.font.adine_kirnberg);
        put("8", R.font.allura_regular);
        put("9", R.font.gessele);
        put("10", R.font.garineldo);
        put("11", R.font.leckerli_one_regular);
        put("12", R.font.fabfeltscript_bold);
        put("13", R.font.marck_script_regular);
        put("14", R.font.sweet_honey);
        put("15", R.font.digory_doodles);
        put("16", R.font.overthe_rainbow);
    }};

    public static HashMap<String, Integer> fontMap1 = new HashMap<String, Integer>() {{
        put("1", R.font.brightlight);
        put("2", R.font.magnificent);
        put("3", R.font.otto);
        put("4", R.font.edwardian_scr_itc_tt);
        put("5", R.font.angelface);
        put("6", R.font.arabella);
        put("7", R.font.argentina_script);
        put("8", R.font.mistral);

        put("9", R.font.im_fell_french_canon_regular);
        put("10", R.font.adine_kirnberg);
        put("11", R.font.allura_regular);
        put("12", R.font.gessele);
        put("13", R.font.garineldo);
        put("14", R.font.leckerli_one_regular);
        put("15", R.font.fabfeltscript_bold);
        put("16", R.font.marck_script_regular);
        put("17", R.font.mistral);
        put("18", R.font.digory_doodles);
        put("19", R.font.overthe_rainbow);
    }};
}

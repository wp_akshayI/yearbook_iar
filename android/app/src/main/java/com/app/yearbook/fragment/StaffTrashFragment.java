package com.app.yearbook.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.AdvertiseActivity;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.adapter.TrashTabViewPagerAdapter;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffTrashFragment extends Fragment implements View.OnClickListener {

    public View view;
    private ViewPager mViewPager;
    private TrashTabViewPagerAdapter mViewPagerAdapter;
   // private TabLayout mTabLayout;
    private EditText edtOldPassword, edtNewPassword, edtConfirmPassword;
    private TextInputLayout input_layout_newpassword, input_layout_confirmpassword, input_layout_oldpassword;
    public ProgressDialog progressDialog;
    private AllMethods allMethods;
    private String userPwd,userId;
    private LoginUser loginUser;

    private LinearLayout lvRepotrted,lvDeleted;
    private TextView tvReported,tvDeleted;

    public StaffTrashFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       // ((StaffHomeActivity) getActivity()).getSupportActionBar().setTitle("Bin");

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_trash, container, false);
        setHasOptionsMenu(true);
        setView();
        return view;
    }

    public void setView() {
        //common methods
        allMethods = new AllMethods();

        loginUser = new LoginUser(getActivity());
        if (loginUser.getUserData() != null) {
            userId = loginUser.getUserData().getStaffId();
            userPwd = loginUser.getUserData().getStaffPassword();
        }

        mViewPager = view.findViewById(R.id.view_pager);
        mViewPagerAdapter = new TrashTabViewPagerAdapter(getChildFragmentManager(),1);
        mViewPager.setAdapter(mViewPagerAdapter);

//        mTabLayout = view.findViewById(R.id.tab_layout);
//        mTabLayout.setupWithViewPager(mViewPager);

        //linear view
        lvRepotrted=view.findViewById(R.id.lvRepotrted);
        lvRepotrted.setOnClickListener(this);
        lvDeleted=view.findViewById(R.id.lvDeleted);
        lvDeleted.setOnClickListener(this);

        //text view
        tvReported=view.findViewById(R.id.tvReported);
        tvDeleted=view.findViewById(R.id.tvDeleted);

        //default selection
        lvRepotrted.setBackground(getResources().getDrawable(R.drawable.rounded_lineardarkgray));
        lvDeleted.setBackground(getResources().getDrawable(R.drawable.rounded_lineargray));
        tvReported.setTextColor(getResources().getColor(R.color.selectedtitle));
        tvDeleted.setTextColor(getResources().getColor(R.color.deselectTab));

        mViewPager.setCurrentItem(0);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d("TTT","Position: "+position);
                mViewPager.getAdapter().notifyDataSetChanged();
                if(position==0)
                {
                    YoYo.with(Techniques.Shake)
                            .duration(100)
                            .playOn(lvRepotrted);
                    lvRepotrted.setBackground(getResources().getDrawable(R.drawable.rounded_lineardarkgray));
                    lvDeleted.setBackground(getResources().getDrawable(R.drawable.rounded_lineargray));
                    tvReported.setTextColor(getResources().getColor(R.color.selectedtitle));
                    tvDeleted.setTextColor(getResources().getColor(R.color.deselectTab));
                }
                else
                {
                    YoYo.with(Techniques.Shake)
                            .duration(100)
                            .playOn(lvDeleted);
                    lvRepotrted.setBackground(getResources().getDrawable(R.drawable.rounded_lineargray));
                    lvDeleted.setBackground(getResources().getDrawable(R.drawable.rounded_lineardarkgray));
                    tvReported.setTextColor(getResources().getColor(R.color.deselectTab));
                    tvDeleted.setTextColor(getResources().getColor(R.color.selectedtitle));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.logout, menu);
//        MenuItem menuPhoto = menu.findItem(R.id.setting);
//        menuPhoto.setIcon(R.mipmap.logout_ic);
//        super.onCreateOptionsMenu(menu, inflater);
//    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.logout, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.setting) {
//            LoginUser loginSP = new LoginUser(getActivity());
//            loginSP.clearData();
//
//            Intent i = new Intent(getActivity(), LoginActivity.class);
//            startActivity(i);
//            getActivity().finish();
//
//        }
        if (item.getItemId() == R.id.setting) {
            View menuItemView = getActivity().findViewById(R.id.setting);
            showMenu(menuItemView);

        }
        return super.onOptionsItemSelected(item);
    }

    private View.OnFocusChangeListener newPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtNewPassword.getText().toString().equals("")) {
                    input_layout_newpassword.setErrorEnabled(true);
                    input_layout_newpassword.setError("Your new password is empty");
                } else {
                    input_layout_newpassword.setErrorEnabled(false);
                    input_layout_newpassword.setError("");
                }
            }
        }
    };

    private View.OnFocusChangeListener confirmPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtConfirmPassword.getText().toString().equals("")) {
                    input_layout_confirmpassword.setErrorEnabled(true);
                    input_layout_confirmpassword.setError("Your confirm password is empty");
                } else {
                    input_layout_confirmpassword.setErrorEnabled(false);
                    input_layout_confirmpassword.setError("");
                }
            }
        }
    };

    private View.OnFocusChangeListener oldPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtOldPassword.getText().toString().equals("")) {
                    input_layout_oldpassword.setErrorEnabled(true);
                    input_layout_oldpassword.setError("Your old password is empty");
                } else {
                    input_layout_oldpassword.setErrorEnabled(false);
                    input_layout_oldpassword.setError("");
                }
            }
        }
    };

    public boolean validation() {
        boolean result = false;

        //old pwd
        Log.d("TTT", "Old Pwd: " + userPwd + " / " + edtOldPassword.getText().toString());

        if (edtOldPassword.getText().toString().trim().equals("")) {
            input_layout_oldpassword.setErrorEnabled(true);
            input_layout_oldpassword.setError("Your old password is empty");
            result = false;
        } else if (!userPwd.toString().equals(edtOldPassword.getText().toString())) {
            Log.d("TTT", "wrongg pwd");
            input_layout_oldpassword.setErrorEnabled(true);
            input_layout_oldpassword.setError("Your old password is wrong");
            result = false;
        } else {
            input_layout_oldpassword.setErrorEnabled(false);
            input_layout_oldpassword.setError("");
            result = true;
        }

        //new pwd
        if (edtNewPassword.getText().toString().trim().equals("")) {
            input_layout_newpassword.setErrorEnabled(true);
            input_layout_newpassword.setError("Your new password is empty");
            result = false;
        } else {
            input_layout_newpassword.setErrorEnabled(false);
            input_layout_newpassword.setError("");
            result = true;
        }

        //confirm new pwd
        if (edtConfirmPassword.getText().toString().trim().equals("")) {
            input_layout_confirmpassword.setErrorEnabled(true);
            input_layout_confirmpassword.setError("Your confirm password is empty");
            result = false;
        } else if (!edtConfirmPassword.getText().toString().equals(edtNewPassword.getText().toString())) {
            input_layout_confirmpassword.setErrorEnabled(true);
            input_layout_confirmpassword.setError("Confirm password and new password is must be same");
            result = false;
        } else {
            input_layout_confirmpassword.setErrorEnabled(false);
            input_layout_confirmpassword.setError("");
            result = true;

        }

        Log.d("TTT", "Result: " + result);
        return result;
    }


    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.lvRepotrted)
        {
            mViewPager.setCurrentItem(0);
            lvRepotrted.setBackground(getResources().getDrawable(R.drawable.rounded_lineardarkgray));
            lvDeleted.setBackground(getResources().getDrawable(R.drawable.rounded_lineargray));
            tvReported.setTextColor(getResources().getColor(R.color.gray));
            tvDeleted.setTextColor(getResources().getColor(R.color.deselectTab));
            YoYo.with(Techniques.Shake)
                    .duration(100)
                    .playOn(lvRepotrted);
        }
        else if(v.getId()==R.id.lvDeleted)
        {
            mViewPager.setCurrentItem(1);
            lvRepotrted.setBackground(getResources().getDrawable(R.drawable.rounded_lineargray));
            lvDeleted.setBackground(getResources().getDrawable(R.drawable.rounded_lineardarkgray));
            tvReported.setTextColor(getResources().getColor(R.color.deselectTab));
            tvDeleted.setTextColor(getResources().getColor(R.color.gray));
            YoYo.with(Techniques.Shake)
                    .duration(100)
                    .playOn(lvDeleted);
        }
    }

    public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(getActivity(), v);
        popup.getMenuInflater().inflate(R.menu.profileseeting, popup.getMenu());
        MenuItem ad=popup.getMenu().findItem(R.id.managead);
        ad.setVisible(true);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getTitle().equals("Manage Ad"))
                {
                        Intent i=new Intent(getActivity(), AdvertiseActivity.class);
                        startActivity(i);
                }
                else if (item.getTitle().equals("Change password")) {
                    androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.custom_alertbox, null);
                    dialogBuilder.setView(dialogView);
                    final androidx.appcompat.app.AlertDialog alert = dialogBuilder.create();
                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    alert.setCancelable(false);

                    edtNewPassword = dialogView.findViewById(R.id.edtNewPassword);
                    edtNewPassword.setOnFocusChangeListener(newPassword);

                    edtOldPassword = dialogView.findViewById(R.id.edtOldPassword);
                    edtOldPassword.setOnFocusChangeListener(oldPassword);

                    edtConfirmPassword = dialogView.findViewById(R.id.edtConfirmPassword);
                    edtConfirmPassword.setOnFocusChangeListener(confirmPassword);

                    input_layout_oldpassword = dialogView.findViewById(R.id.input_layout_oldpassword);
                    input_layout_newpassword = dialogView.findViewById(R.id.input_layout_newpassword);
                    input_layout_confirmpassword = dialogView.findViewById(R.id.input_layout_confirmpassword);

                    Button btnCancel = dialogView.findViewById(R.id.btnCancel);
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    Button btnOk = dialogView.findViewById(R.id.btnOk);
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (validation()) {
                                if (!userPwd.toString().equals(edtOldPassword.getText().toString())) {
                                    Log.d("TTT", "wrongg pwd");
                                    input_layout_oldpassword.setErrorEnabled(true);
                                    input_layout_oldpassword.setError("Your old password is wrong");
                                } else {
                                    alert.dismiss();
                                    changePassword(edtNewPassword.getText().toString());
                                }
                            }
                        }
                    });
                    alert.show();
                } else if (item.getTitle().equals("Logout")) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setCancelable(false);
                    dialog.setTitle("");
                    dialog.setMessage("Are you sure you want to logout ?");
                    dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //Action for "Delete".
                            dialog.dismiss();

                            String token = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Token", "");
                            Log.d("TTT", "token: " + token);

                            if (!token.equals("")) {
                                progressDialog = ProgressDialog.show(getActivity(), "", "", true);
                                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                progressDialog.setContentView(R.layout.progress_view);
                                ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                                Circle bounce = new Circle();
                                bounce.setColor(Color.BLACK);
                                progressBar.setIndeterminateDrawable(bounce);

                                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                final Call<GiveReport> userPost = retrofitClass.logout(token, "staff");
                                userPost.enqueue(new Callback<GiveReport>() {
                                    @Override
                                    public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }

                                        LoginUser loginSP = new LoginUser(getActivity());
                                        loginSP.clearData();

                                        Intent i = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(i);
                                        getActivity().finish();
                                    }

                                    @Override
                                    public void onFailure(Call<GiveReport> call, Throwable t) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }
                                        allMethods.setAlert(getActivity(), "", t.getMessage() + "");
                                    }
                                });
                            }
                            else
                            {
                                LoginUser loginSP = new LoginUser(getActivity());
                                loginSP.clearData();

                                Intent i = new Intent(getActivity(), LoginActivity.class);
                                startActivity(i);
                                (getActivity()).finish();
                            }
                        }


                    });

                    dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    final AlertDialog alert = dialog.create();
                    alert.show();

                }
                return true;
            }
        });
        popup.show();

    }

    public void changePassword(String pwd) {
        //Progress bar
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> userPost = retrofitClass.changePassword(Integer.parseInt(userId), "staff", pwd);
        userPost.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", response.body().getResponseMsg() + "");
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });
    }
}

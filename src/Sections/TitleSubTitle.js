import React from "react";
import { View, Text } from "react-native";

export default function TitleSubTitle({ data }) {
    const { component_title, component_description } = data;
    return (
        <View style={{ paddingHorizontal: 12, paddingVertical: 8 }}>
            <Text
                ellipsizeMode="tail"
                style={{
                    color: "#000",
                    fontSize: 15,
                    fontWeight: "bold",
                    flexWrap: "wrap"
                }}>
                {component_title}
            </Text>
            <Text
                ellipsizeMode="tail"
                style={{
                    color: "#3F3F3F",
                    fontSize: 14,
                    marginTop: 10,
                    flexWrap: "wrap"
                }}>
                {component_description}
            </Text>
        </View>
    )
}
package com.app.yearbook.interfaces;

import android.view.View;

public interface OnMenuOptionClick {
    void onClick(int pos, int type, View view);
}

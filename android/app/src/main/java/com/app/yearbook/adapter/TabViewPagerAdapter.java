package com.app.yearbook.adapter;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.app.yearbook.fragment.SearchPeopleFragment;
import com.app.yearbook.fragment.SearchTagFragment;

public class TabViewPagerAdapter extends FragmentStatePagerAdapter {

    private static int TAB_COUNT = 2;
    private String type="";

    public TabViewPagerAdapter(FragmentManager fm,String type) {
        super(fm);
        this.type=type;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle myBundle = new Bundle();

        Fragment fragment = null;
        myBundle .putString("type", type);
        switch (position) {
            case 0:
                fragment=new SearchPeopleFragment();
                break;
            case 1:
                fragment=new SearchTagFragment();
                break;

        }
        fragment.setArguments( myBundle  );
        return fragment;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Users";

            case 1:
                return "Tags";
        }
        return super.getPageTitle(position);
    }

}

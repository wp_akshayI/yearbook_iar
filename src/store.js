import { createStore, applyMiddleware, compose } from "redux"
import thunk from "redux-thunk";
import rootReducer from "./reducers"

const middlewares = [thunk];
const composedEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function configureStore(initialState) {
    const store = createStore(rootReducer(), initialState, composedEnhancer( applyMiddleware(...middlewares)));
    return store;
}
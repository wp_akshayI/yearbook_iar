package com.app.yearbook.util;

import android.os.Environment;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by caizhiming on 2016/11/18.
 */

public class AppUtil {
    /**
     * 获取SD卡路径
     *
     * @return
     */
    public static String getSDPath() {
        File sdDir;
        boolean sdCardExist = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED); //判断sd卡是否存在
        if (sdCardExist) {
            sdDir = Environment.getExternalStorageDirectory();//获取跟目录
            return sdDir.toString();
        } else {
            return null;
        }
    }
    public static String getAppDir(){
        String appDir = AppUtil.getSDPath();
        appDir += "/" + "xc";
        File file = new File(appDir);
        if(!file.exists()){
            file.mkdir();
        }
        appDir +="/"+"videocompress";
        file = new File(appDir);
        if(!file.exists()){
            file.mkdir();
        }
        return appDir;
    }

    public static String getEncodeUTF8String(String str){
        /*try {
            return URLEncoder.encode(str,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            return str;
        }*/
        return str;
    }
    public static String getDecodeUTF8String(String str){
        /*try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return str;
        }*/
        return str;
    }
}

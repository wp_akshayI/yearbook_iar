package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.doodle.android.chips.ChipsView;
import com.doodle.android.chips.model.Contact;
import com.github.ybq.android.spinkit.style.Circle;
import com.volokh.danylo.hashtaghelper.HashTagHelper;
import com.app.yearbook.model.search.User;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.staff.yearbook.UserTagg;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.BitmapUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.PlayerManager;
import tcking.github.com.giraffeplayer2.VideoInfo;
import tcking.github.com.giraffeplayer2.VideoView;

public class EditYearBookPostActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private AllMethods allMethods;
    private Toolbar toolbar;
    private String postId, postDesc, fileType, getUrl,getThumbnail, tagUserList = "", getMsg = "";
    private ArrayList<UserTagg> userTaggs;
    private ImageView imgPost, imgEditPhoto;
    private VideoView videoSelected;
    private EditText edtPostMsg;
    private ChipsView mChipsView;
    private LinearLayout lvTag, lvTagPeople;
    private HashTagHelper mEditTextHashTagHelper;
    private List<String> tagUser = new ArrayList<>();
    private int IntentCode = 765, IntentData = 101,fileSizeType;
    private File fileImage = null, fileVideo = null;
    private RequestBody rbFile = null, rbFileThumb = null;
    private MultipartBody.Part part = null, thumbPart = null;
    private String flag = "",post_type,post_user_id;
    private Bitmap thumbImage = null;
    private LoginUser loginUser;
    private int schoolId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_year_book_post);

        setView();

        //edit data
        // EditPost();
    }

    public void setView() {
        //common methods
        allMethods = new AllMethods();

        //toolbar
        setToolbar();

        //img
        imgPost = findViewById(R.id.imgPost);

        //video
        videoSelected = findViewById(R.id.videoSelected);

        //msg
        edtPostMsg = findViewById(R.id.edtPostMsg);

        //chip view
        mChipsView = findViewById(R.id.chipsView);

        //img for edit photo or video
        imgEditPhoto = findViewById(R.id.imgEditPhoto);
        imgEditPhoto.setOnClickListener(editPhotoVideo);

        //lv
        lvTag = findViewById(R.id.lvTag);
        lvTagPeople = findViewById(R.id.lvTagPeople);

        lvTagPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditYearBookPostActivity.this, SearchUserActivity.class);
                i.putExtra("Type","staff");
                startActivityForResult(i, IntentCode);
            }
        });

        //SP object
        loginUser = new LoginUser(EditYearBookPostActivity.this);
        if (loginUser.getUserData() != null) {
            schoolId = Integer.parseInt(loginUser.getUserData().getSchoolId());
        }

        //get intent value
        if (getIntent().getExtras() != null) {
            postId = getIntent().getStringExtra("postId");
            postDesc = getIntent().getStringExtra("desc");
            fileType = getIntent().getStringExtra("fileType");
            getUrl = getIntent().getStringExtra("imagevideofile");
            getThumbnail=getIntent().getStringExtra("imgThumbnail");
            fileSizeType = getIntent().getIntExtra("filesizetype",1);
            userTaggs = (ArrayList<UserTagg>) getIntent().getSerializableExtra("tagUserId");

            post_user_id=getIntent().getStringExtra("post_user_id");
            post_type=getIntent().getStringExtra("post_type");


            //flag = getIntent().getStringExtra("flag");
            Log.d("TTT", "user tag:  " + userTaggs.size() + " " + postDesc + " / " + postId + " / " + flag);
            Log.d("TTT", "fileType / fileSizeType:  " + fileType  + " / " + getUrl);
            setData();
        }


        mChipsView.setChipsListener(new ChipsView.ChipsListener() {
            @Override
            public void onChipAdded(ChipsView.Chip chip) {
                // chip added
            }

            @Override
            public void onChipDeleted(ChipsView.Chip chip) {
                // chip deleted
                Log.d("TTT", "Chip delete: " + chip.getContact().getDisplayName());
                String id = chip.getContact().getDisplayName();

                for (int i = 0; i < userTaggs.size(); i++) {
                    if (userTaggs.get(i).getUserId().equalsIgnoreCase(id)) {
                        userTaggs.remove(i);
                    }
                }

                if (userTaggs.size() == 0) {

                    mChipsView.setVisibility(View.GONE);
                    lvTag.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence text) {
                text = null;
                // text was changed
            }

            @Override
            public boolean onInputNotRecognized(String text) {
                // return true to delete the input
                return false; // keep the typed text
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentCode && resultCode == RESULT_OK) {
            if (data.getExtras() != null) {
                User user = (User) data.getSerializableExtra("SearchUser");
                mChipsView.setVisibility(View.VISIBLE);
                lvTag.setVisibility(View.GONE);
                if (userTaggs.size() > 0) {
                    String flag = "add";
                    for (int i = 0; i < userTaggs.size(); i++) {
                        if (userTaggs.get(i).getUserId().equals(user.getUserId())) {
                            flag = "notadd";
                        }
                    }
                    if (flag.equals("add")) {
                        UserTagg userTagg = new UserTagg();
                        userTagg.setUserId(user.getUserId());
                        userTagg.setUserFirstname(user.getUserFirstname());
                        userTagg.setUserLastname(user.getUserLastname());
                        userTaggs.add(userTagg);
                        Contact c = new Contact(user.getUserFirstname(), user.getUserLastname(), user.getUserId(), "", null);
                        mChipsView.addChip(user.getUserFirstname() + " " + user.getUserLastname(), "", c);
                    }
                } else {
                    UserTagg userTagg = new UserTagg();
                    userTagg.setUserId(user.getUserId());
                    userTagg.setUserFirstname(user.getUserFirstname());
                    userTagg.setUserLastname(user.getUserLastname());

                    userTaggs.add(userTagg);
                    Contact c = new Contact(user.getUserFirstname(), user.getUserLastname(), user.getUserId(), "", Uri.parse(user.getUserImage()));
                    mChipsView.addChip(user.getUserFirstname() + " " + user.getUserLastname(), Uri.parse(user.getUserImage()), c);
                }
                Log.d("TTT", "Sizeee: " + userTaggs.size());
            }
        } else if (requestCode == IntentData && resultCode == RESULT_OK) {
            Log.d("TTT", "OnActivity");
            if (data.getExtras() != null) {
                flag = "mediachange";
                String type = "";
                if (data.getStringExtra("Type") != null) {
                    type = data.getStringExtra("Type");
                    Log.d("TTT", "OnActivity type: " + type);

                    if (type.equalsIgnoreCase("Image")) {

                        fileSizeType = data.getIntExtra("CropType",1);
                        getUrl = data.getStringExtra("AddPostImage");

                        Log.d("TTT","FileSizeType: "+fileSizeType);
                        try {
                            thumbImage = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(getUrl));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                       // imgPost.setImageBitmap(thumbImage);
                      //  imgPost.setImageURI(Uri.parse(getUrl));

                        Glide.with(EditYearBookPostActivity.this)
                                .load(Uri.parse(getUrl))
                                .asBitmap()
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                        imgPost.setImageBitmap(resource);
                                    }
                                });

                        if (!flag.equals("") && flag.equalsIgnoreCase("mediachange")) {

                            if(post_type.equals("1"))
                            {
                                fileImage = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), Uri.parse(getUrl)));
                                RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
                                part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);
                                rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());
                            }
                            else
                            {
                                fileImage = new File(getUrl);
                                RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage.getName());
                                part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);
                                rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());
                            }
                        }

                    } else if (type.equalsIgnoreCase("Video")) {
                        getUrl = data.getStringExtra("selectUri");

                        imgPost.setVisibility(View.GONE);
                        videoSelected.setVisibility(View.VISIBLE);

//                        videoSelected.reset();
//                        videoSelected.setSource(Uri.parse(getUrl));

                        videoSelected.getVideoInfo().setPortraitWhenFullScreen(false);
                        videoSelected.setVideoPath(getUrl);
                        videoSelected.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player
                        videoSelected.getPlayer().start();

                        if (!flag.equals("") && flag.equalsIgnoreCase("mediachange")) {
                            //for video
                            fileImage = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), Uri.parse(getUrl)));
                            RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
                            part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);
                            rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());

                            //for thumbnail
                            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(fileImage.getAbsolutePath(), MediaStore.Video.Thumbnails.MINI_KIND);
                            fileVideo = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), AllMethods.getImageUri(getApplicationContext(), bMap)));
                            RequestBody filePostThumb = RequestBody.create((MediaType.parse("*/*")), fileVideo);
                            thumbPart = MultipartBody.Part.createFormData("post_thumbnail", fileVideo.getName(), filePostThumb);
                            rbFileThumb = RequestBody.create(MediaType.parse("text/plain"), fileVideo.getName());
                        }
                    }
                }
            }
        }
    }

    //function for set data of year book post
    public void setData() {
        Log.d("TTT", "flag: " + flag);
        if (fileType.equals("1")) {
            imgPost.setVisibility(View.VISIBLE);
            videoSelected.setVisibility(View.GONE);

            if(getUrl!=null && !getUrl.equals("")) {
                Glide.with(getApplicationContext())
                        .load(Uri.parse(getUrl)).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.mipmap.home_post_img_placeholder)
                        .error(R.mipmap.home_post_img_placeholder)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                imgPost.setImageBitmap(resource);
                            }
                        });
                Log.d("TTT", "URI: " + Uri.parse(getUrl));

                if(post_type.equals("1"))
                {
                    fileImage = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), Uri.parse(getUrl)));
                    RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
                    part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);
                    rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());
                }
                else
                {
                    fileImage = new File(getUrl);
                    RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage.getName());
                    part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);
                    rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());
                }
            }

        } else if (fileType.equals("2")) {
            imgPost.setVisibility(View.GONE);
            videoSelected.setVisibility(View.VISIBLE);

//            videoSelected.reset();
//            videoSelected.setSource(Uri.parse(getUrl));

            if (videoSelected.getCoverView() != null) {
                Glide.with(EditYearBookPostActivity.this)
                        .load(getThumbnail)
                        .asBitmap()
                        .placeholder(R.mipmap.home_post_img_placeholder)
                        .error(R.mipmap.home_post_img_placeholder)
                        .into(videoSelected.getCoverView());
            }

            videoSelected.getVideoInfo().setPortraitWhenFullScreen(false);
            videoSelected.setVideoPath(getUrl);
            videoSelected.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player
            //videoSelected.getPlayer().start();

        }


        //description
        edtPostMsg.setText(postDesc);
        mEditTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.hashtag), null);
        mEditTextHashTagHelper.handle(edtPostMsg);

        //setTag user
        Log.d("TTT", "Sizeee: " + userTaggs.size());
        if (userTaggs != null && userTaggs.size() > 0) {
            lvTag.setVisibility(View.GONE);
            mChipsView.setVisibility(View.VISIBLE);

            for (int i = 0; i < userTaggs.size(); i++) {
                Contact c = new Contact(userTaggs.get(i).getUserFirstname(), userTaggs.get(i).getUserLastname(), userTaggs.get(i).getUserId(), "", null);
                mChipsView.addChip(userTaggs.get(i).getUserFirstname() + " " + userTaggs.get(i).getUserLastname(), "", c);
            }
        }
    }


    //set toolbar
    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Post");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selectimage, menu);
        MenuItem next = menu.findItem(R.id.next);
        next.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.next) {
            AllMethods.hideKeyboard(EditYearBookPostActivity.this,getCurrentFocus());
            EditPost();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (PlayerManager.getInstance().onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    //edit post
    public void EditPost() {

       // videoSelected.getPlayer().stop();
        Log.d("TTT", "Total size: " + userTaggs.size());
        tagUser.clear();
        for (int i = 0; i < userTaggs.size(); i++) {
            tagUser.add(userTaggs.get(i).getUserId());
        }

        tagUserList = "";
        if (tagUser.size() > 0) {
            tagUserList = TextUtils.join(",", tagUser);
        }
        Log.d("TTT", "tagUserList: " + tagUserList+" / "+fileSizeType);

        //Progress bar
        progressDialog = ProgressDialog.show(EditYearBookPostActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        getMsg = edtPostMsg.getText().toString();
        RequestBody pId = RequestBody.create(MediaType.parse("text/plain"), postId);
        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), tagUserList);
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), getMsg);
        RequestBody selectType = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(fileSizeType));
        RequestBody mediaType = RequestBody.create(MediaType.parse("text/plain"), fileType + "");

        RequestBody postType=RequestBody.create(MediaType.parse("text/plain"),post_type+"");
        RequestBody postUserId=RequestBody.create(MediaType.parse("text/plain"),post_user_id);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<GiveReport> login = retrofitClass.editStaffPost(pId, mediaType, userId, desc, part, rbFile, thumbPart, rbFileThumb, selectType,postUserId,postType);
        login.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EditYearBookPostActivity.this);
                    builder.setMessage(response.body().getResponseMsg())
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent i = getIntent();
                                    i.putExtra("Desc", getMsg);
                                    i.putExtra("userId", tagUserList);
                                    i.putExtra("url", getUrl);
                                    i.putExtra("postId", postId);
                                    i.putExtra("fileType",fileType);
                                    i.putExtra("userTaggs", userTaggs);
                                    setResult(RESULT_OK, i);
                                    finish();
                                }
                            });

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    alert.setTitle("");
                    alert.show();
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(EditYearBookPostActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(EditYearBookPostActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(EditYearBookPostActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    allMethods.setAlert(EditYearBookPostActivity.this, "", response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(EditYearBookPostActivity.this, getResources().getString(R.string.app_name), t.getMessage());
            }
        });
    }

    public View.OnClickListener editPhotoVideo = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (fileType.equalsIgnoreCase("1")) {

                if(post_type!=null && post_type.equals("1"))
                {
                    Log.d("TTT","Croppppp: "+fileSizeType);
                    Intent i = new Intent(EditYearBookPostActivity.this, StaffPhotoVideoSelectActivity.class);
                    i.putExtra("actions", "photo");
                    i.putExtra("EditYearPost", "StaffPost");
                    startActivityForResult(i, IntentData);
                }
                else if(post_type!=null && post_type.equals("2"))
                {
                    Intent i = new Intent(EditYearBookPostActivity.this, SchoolAllStudentActivity.class);
                   // Intent i = new Intent(EditYearBookPostActivity.this, StaffPhotoVideoSelectActivity.class);
                    i.putExtra("post", "studentPortrait");
                    i.putExtra("SchoolId", schoolId);
                    startActivityForResult(i, IntentData);
                }

            } else if (fileType.equalsIgnoreCase("2")) {
                Intent ii = new Intent(EditYearBookPostActivity.this, StaffPhotoVideoSelectActivity.class);
                ii.putExtra("actions", "video");
                ii.putExtra("EditYearPost", "StaffPost");
                startActivityForResult(ii, IntentData);
            }
        }
    };
}

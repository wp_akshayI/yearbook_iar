package com.app.yearbook.interfaces;

public interface OnRecyclerClick {
    void onClick(int pos, int type);

    void onVote(int pos, String pollOptionId);
}

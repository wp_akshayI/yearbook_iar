package com.app.yearbook.model.postmodels;


import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.app.yearbook.BR;
import com.app.yearbook.util.AppUtil;
import com.google.gson.annotations.SerializedName;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class PollOption extends BaseObservable implements Parcelable {

    @SerializedName("option_description")
    private String optionDescription;

    @SerializedName("option_id")
    private String optionId;

    @SerializedName("total_response_on_option")
    private double totalResponseOnOption;

    @SerializedName("response_on_option")
    private double responseOnOption;

    @SerializedName("option_select")
    private String optionSelect;

    @Bindable
    public boolean getOptionSelect() {
        if (optionSelect != null) {
            return optionSelect.equalsIgnoreCase("True");
        }
        return false;
    }

    public void setOptionSelect(String optionSelect) {
        this.optionSelect = optionSelect;
        notifyPropertyChanged(BR.optionSelect);
    }

    boolean isSelected;


    protected PollOption(Parcel in) {
        optionDescription = in.readString();
        optionId = in.readString();
        optionSelect = in.readString();
        totalResponseOnOption = in.readDouble();
        responseOnOption = in.readDouble();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<PollOption> CREATOR = new Creator<PollOption>() {
        @Override
        public PollOption createFromParcel(Parcel in) {
            return new PollOption(in);
        }

        @Override
        public PollOption[] newArray(int size) {
            return new PollOption[size];
        }
    };

    @Bindable
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
        notifyPropertyChanged(BR.selected);
    }


    public void setOptionDescription(String optionDescription) {
        this.optionDescription = optionDescription;
    }

    public String getOptionDescription() {
        return AppUtil.getDecodeUTF8String(optionDescription);
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setTotalResponseOnOption(double totalResponseOnOption) {
        this.totalResponseOnOption = totalResponseOnOption;
    }

    public int getTotalResponseOnOption() {
        return (int) totalResponseOnOption;
    }

    public void setResponseOnOption(double responseOnOption) {
        this.responseOnOption = responseOnOption;
    }

    public int getResponseOnOption() {
        return (int)responseOnOption;
    }
    public String getResponseOnOption1() {
        try {
            NumberFormat nf = new DecimalFormat("##.##");
            return nf.format(responseOnOption);
        }catch (Exception e){
            return String.format("%.2f", responseOnOption);
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(optionDescription);
        dest.writeString(optionId);
        dest.writeString(optionSelect);
        dest.writeDouble(totalResponseOnOption);
        dest.writeDouble(responseOnOption);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }
}
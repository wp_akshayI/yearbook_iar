package com.app.yearbook.model;

import com.google.gson.annotations.SerializedName;

public class StaffProfileResponse {

    @SerializedName("ResponseCode")
    private String responseCode;

    @SerializedName("ServerTimeZone")
    private String serverTimeZone;

    @SerializedName("ResponseMsg")
    private String responseMsg;

    @SerializedName("serverTime")
    private String serverTime;

    @SerializedName("staff_profile")
    private StaffProfile userProfile;

    @SerializedName("Result")
    private String result;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getServerTimeZone() {
        return serverTimeZone;
    }

    public void setServerTimeZone(String serverTimeZone) {
        this.serverTimeZone = serverTimeZone;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public StaffProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(StaffProfile userProfile) {
        this.userProfile = userProfile;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
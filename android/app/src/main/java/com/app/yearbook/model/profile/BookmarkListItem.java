package com.app.yearbook.model.profile;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.app.yearbook.BR;
import com.google.gson.annotations.SerializedName;
import com.app.yearbook.model.staff.yearbook.UserTagg;
import java.io.Serializable;
import java.util.List;

public class BookmarkListItem extends BaseObservable implements Serializable{

	@SerializedName("total_like")
	private String totalLike;

	@SerializedName("flag")
	private String flag;

	@SerializedName("post_file")
	private String postFile;

	@SerializedName("post_like")
	private String postLike;

	@SerializedName("total_comment")
	private String totalComment;

	@SerializedName("post_description")
	private String postDescription;

	@SerializedName("file_size_type")
	private String fileSizeType;

	@SerializedName("file_position")
	private String filePosition;

	@SerializedName("post_id")
	private String postId;

	@SerializedName("school_id")
	private String schoolId;

	@SerializedName("category_id")
	private String categoryId;

	@SerializedName("file_type")
	private String fileType;

	@SerializedName("post_date")
	private String postDate;

	@SerializedName("staff_id")
	private String staffId;

	@SerializedName("post_comment")
	private String postComment;

	@SerializedName("post_thumbnail")
	private String postThumbnail;

	@SerializedName("post_bookmark")
	private String postBookmark;

	@SerializedName("user_tagg")
	private List<UserTagg> userTagg;

	@SerializedName("bookmark_created")
	private String bookmarkCreated;

	@SerializedName("staff_image")
	private String staffImage;

	@SerializedName("staff_firstname")
	private String staffFirstname;

	@SerializedName("staff_lastname")
	private String staffLastname;

	@SerializedName("post_file_type")
	private String postFileType;

	@SerializedName("post_title")
	private String postTitle;

	@SerializedName("post_push")
	private String postPush;

	@SerializedName("post_type")
	private String postType;

	@SerializedName("post_notification")
	private String postNotification;


	public void setTotalLike(String totalLike){
		this.totalLike = totalLike;
		notifyPropertyChanged(BR.totalLike);
	}

	@Bindable
	public String getTotalLike(){
		return totalLike;
	}

	public void setFlag(String flag){
		this.flag = flag;
		notifyPropertyChanged(BR.flag);
	}

	@Bindable
	public String getFlag(){
		return flag;
	}

	public void setPostFile(String postFile){
		this.postFile = postFile;
		notifyPropertyChanged(BR.postFile);
	}

	@Bindable
	public String getPostFile(){
		return postFile;
	}

	public void setPostLike(String postLike){
		this.postLike = postLike;
		notifyPropertyChanged(BR.postLike);
	}

	@Bindable
	public String getPostLike(){
		return postLike;
	}

	public void setTotalComment(String totalComment){
		this.totalComment = totalComment;
		notifyPropertyChanged(BR.totalComment);
	}

	@Bindable
	public String getTotalComment(){
		return totalComment;
	}

	public void setPostDescription(String postDescription){
		this.postDescription = postDescription;
		notifyPropertyChanged(BR.postDescription);
	}

	@Bindable
	public String getPostDescription(){
		return postDescription;
	}

	public void setFileSizeType(String fileSizeType){
		this.fileSizeType = fileSizeType;
		notifyPropertyChanged(BR.fileSizeType);
	}

	@Bindable
	public String getFileSizeType(){
		return fileSizeType;
	}

	public void setFilePosition(String filePosition){
		this.filePosition = filePosition;
		notifyPropertyChanged(BR.filePosition);
	}

	@Bindable
	public String getFilePosition(){
		return filePosition;
	}

	public void setPostId(String postId){
		this.postId = postId;
		notifyPropertyChanged(BR.postId);
	}

	@Bindable
	public String getPostId(){
		return postId;
	}

	public void setSchoolId(String schoolId){
		this.schoolId = schoolId;
		notifyPropertyChanged(BR.schoolId);
	}

	@Bindable
	public String getSchoolId(){
		return schoolId;
	}

	public void setCategoryId(String categoryId){
		this.categoryId = categoryId;
		notifyPropertyChanged(BR.categoryId);
	}

	@Bindable
	public String getCategoryId(){
		return categoryId;
	}

	public void setFileType(String fileType){
		this.fileType = fileType;
		notifyPropertyChanged(BR.fileType);
	}

	@Bindable
	public String getFileType(){
		return fileType;
	}

	public void setPostDate(String postDate){
		this.postDate = postDate;
		notifyPropertyChanged(BR.postDate);
	}

	@Bindable
	public String getPostDate(){
		return postDate;
	}

	public void setStaffId(String staffId){
		this.staffId = staffId;
		notifyPropertyChanged(BR.staffId);
	}

	@Bindable
	public String getStaffId(){
		return staffId;
	}

	public void setPostComment(String postComment){
		this.postComment = postComment;
		notifyPropertyChanged(BR.postComment);
	}

	@Bindable
	public String getPostComment(){
		return postComment;
	}

	public void setPostThumbnail(String postThumbnail){
		this.postThumbnail = postThumbnail;
		notifyPropertyChanged(BR.postThumbnail);
	}

	@Bindable
	public String getPostThumbnail(){
		return postThumbnail;
	}

	public void setPostBookmark(String postBookmark){
		this.postBookmark = postBookmark;
		notifyPropertyChanged(BR.postBookmark);
	}

	@Bindable
	public String getPostBookmark(){
		return postBookmark;
	}

	public void setUserTagg(List<UserTagg> userTagg){
		this.userTagg = userTagg;
		notifyPropertyChanged(BR.userTagg);
	}

	@Bindable
	public List<UserTagg> getUserTagg(){
		return userTagg;
	}

	public void setBookmarkCreated(String bookmarkCreated){
		this.bookmarkCreated = bookmarkCreated;
	}

	@Bindable
	public String getBookmarkCreated(){
		return bookmarkCreated;
	}

	public void setUserImage(String userImage){
		this.staffImage = userImage;
		notifyPropertyChanged(BR.staffImage);
	}

	@Bindable
	public String getUserImage(){
		return staffImage;
	}

	public void setUserFirstname(String userFirstname){
		this.staffFirstname = userFirstname;
		notifyPropertyChanged(BR.staffFirstname);
	}

	@Bindable
	public String getUserFirstname(){
		return staffFirstname;
	}

	public void setUserLastname(String userLastname){
		this.staffLastname = userLastname;
		notifyPropertyChanged(BR.staffLastname);
	}

	@Bindable
	public String getUserLastname(){
		return staffLastname;
	}

	public void setPostFileType(String postFileType){
		this.postFileType = postFileType;
		notifyPropertyChanged(BR.postFileType);
	}

	@Bindable
	public String getPostFileType(){
		return postFileType;
	}



	public void setPostPush(String postPush){
		this.postPush = postPush;
		notifyPropertyChanged(BR.postPush);
	}

	@Bindable
	public String getPostPush(){
		return postPush;
	}

	public void setPostType(String postType){
		this.postType = postType;
		notifyPropertyChanged(BR.postType);
	}

	@Bindable
	public String getPostType(){
		return postType;
	}

	@Bindable
	public String getPostNotification() {
		return postNotification;
	}

	public void setPostNotification(String postNotification) {
		this.postNotification = postNotification;
		notifyPropertyChanged(BR.postNotification);
	}
}
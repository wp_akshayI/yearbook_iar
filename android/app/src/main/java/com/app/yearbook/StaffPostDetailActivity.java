package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.databinding.ActivityStaffPostDetailBinding;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.staff.GetStaffPostDEtail;
import com.app.yearbook.model.staff.livefeed.PostListItem;
import com.app.yearbook.model.staff.yearbook.UserTagg;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.PlayerManager;
import tcking.github.com.giraffeplayer2.VideoInfo;

public class StaffPostDetailActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private PostListItem postData;
    private AllMethods allMethods;
    public ProgressDialog progressDialog;
    private ActivityStaffPostDetailBinding binding;
    private LoginUser loginUser;
    private int userId, postId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_staff_post_detail);
        setView();
    }

    private void setView() {
        //toolbar
        setToolbar();

        //SP object
        loginUser = new LoginUser(StaffPostDetailActivity.this);
        if (loginUser.getUserData() != null) {
            userId = Integer.parseInt(loginUser.getUserData().getStaffId());
        }

        //all methods object
        allMethods = new AllMethods();

        if (getIntent().getExtras() != null) {
            postId = Integer.parseInt(getIntent().getStringExtra("PostId"));
            Log.d("TTT", "Post id: " + postId);
            getPost(postId);
            //postData = (PostList) getIntent().getSerializableExtra("PostObject");
        }
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Post");
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void getPost(int pid) {
        //Progress bar
        progressDialog = ProgressDialog.show(StaffPostDetailActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetStaffPostDEtail> staffPost = retrofitClass.getStaffPost(pid);
        staffPost.enqueue(new Callback<GetStaffPostDEtail>() {
            @Override
            public void onResponse(Call<GetStaffPostDEtail> call, Response<GetStaffPostDEtail> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (response.body().getResponseCode().equals("1")) {
                    Log.d("TTT", "Sizeee: " + response.body().getPostList().get(0).getPostId());
                    postData = response.body().getPostList().get(0);
                    binding.setStaffpostlist(postData);
                    setValue();
                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(StaffPostDetailActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StaffPostDetailActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StaffPostDetailActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    allMethods.setAlert(StaffPostDetailActivity.this, "", response.body().getResponseMsg() + "");
                }

            }

            @Override
            public void onFailure(Call<GetStaffPostDEtail> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StaffPostDetailActivity.this, "", t.getMessage() + "");
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (PlayerManager.getInstance().onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    public void setValue() {

        Log.d("TTT", "Reporttt: " + binding.getStaffpostlist().getTotalRepoted());
        //user img
        if (binding.getStaffpostlist().getStaffImage() != null) {
            Glide.with(this)
                    .load(binding.getStaffpostlist().getStaffImage())
                    .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.mipmap.post_place_holder)
                    .error(R.mipmap.post_place_holder)
                    .into(binding.imgUserImage);

            binding.imgUserImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(StaffPostDetailActivity.this, ImageActivity.class);
                    i.putExtra("FileUrl", postData.getStaffImage());
                    startActivity(i);
                }
            });
        }

        //post img
        if (binding.getStaffpostlist().getPostFile() != null || !binding.getStaffpostlist().getPostFile().equals("")) {
            final String uri = binding.getStaffpostlist().getPostFile();
            String extension = uri.substring(uri.lastIndexOf("."));
            Log.d("TTT", "Url type: " + extension);

            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {
                binding.imgUserPost.setVisibility(View.VISIBLE);
                binding.videoUserPost.setVisibility(View.GONE);
                if (binding.getStaffpostlist().getPostFile() != null) {
                    Glide.with(this)
                            .load(binding.getStaffpostlist().getPostFile())
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    binding.imgUserPost.setImageBitmap(resource);
                                }
                            });
                }
            } else {
                binding.imgUserPost.setVisibility(View.GONE);
                binding.videoUserPost.setVisibility(View.VISIBLE);

//            binding.videoUserPost.reset();
//            binding.videoUserPost.setSource(Uri.parse(binding.getStaffpostlist().getPostFile()));

                if (binding.videoUserPost.getCoverView() != null) {
                    Glide.with(StaffPostDetailActivity.this)
                            .load(binding.getStaffpostlist().getPostThumbnail())
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(binding.videoUserPost.getCoverView());
                }

                binding.videoUserPost.getVideoInfo().setPortraitWhenFullScreen(false);
                binding.videoUserPost.setVideoPath(binding.getStaffpostlist().getPostFile());
                binding.videoUserPost.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player

                Log.d("TTT", "Video urllllll: " + binding.getStaffpostlist().getPostFile());

            }

            //image touch
            binding.imgUserPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), ImageActivity.class);
                    i.putExtra("caption", binding.getStaffpostlist().getPostDescription());
                    i.putExtra("FileUrl", uri);
                    startActivity(i);
                }
            });
        }

        //comment
//        binding.tvComment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), StudentCommentActivity.class);
//                i.putExtra("PostId", binding.getStaffpostlist().getPostId());
//                i.putExtra("comment", "staff");
//                startActivity(i);
//            }
//        });

        if (userId == Integer.parseInt(binding.getStaffpostlist().getStaffId())) {
            binding.imgOption.setVisibility(View.VISIBLE);
            binding.imgReport.setVisibility(View.GONE);
        } else {
            binding.imgOption.setVisibility(View.GONE);
            binding.imgReport.setVisibility(View.VISIBLE);
        }

        //user delete
        binding.imgOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialogForSameStudent(binding.getStaffpostlist(),binding.imgOption);

//                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(StaffPostDetailActivity.this);
//                LayoutInflater inflater = getLayoutInflater();
//                final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
//                dialogBuilder.setView(dialogView);
//                final AlertDialog alert = dialogBuilder.create();
//                alert.setCancelable(false);
//                alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
//
//                TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
//                tvMessage.setText("Are you sure you want to remove the user?");
//
//                TextView tvYes = dialogView.findViewById(R.id.tvYes);
//                TextView tvNo = dialogView.findViewById(R.id.tvNo);
//
//                tvYes.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {

//                        alert.dismiss();
//                        final String id = binding.getStaffpostlist().getStaffId();
//
//                        progressDialog = ProgressDialog.show(StaffPostDetailActivity.this, "", "", true);
//                        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                        progressDialog.setContentView(R.layout.progress_view);
//                        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
//                        Circle bounce = new Circle();
//                        bounce.setColor(Color.BLACK);
//                        progressBar.setIndeterminateDrawable(bounce);
//
//                        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
//                        final Call<GiveReport> userPost = retrofitClass.staffDeleteUser(Integer.parseInt(id));
//                        userPost.enqueue(new Callback<GiveReport>() {
//                            @Override
//                            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
//                                if (progressDialog.isShowing()) {
//                                    progressDialog.dismiss();
//                                }
//
//                                if (response.body().getResponseCode().equals("1")) {
//                                    if (UserDetailActivity.getUserList != null) {
//                                        for (int x = UserDetailActivity.getUserList.size() - 1; x >= 0; x--) {
//                                            Log.d("TTT", "loop: " + x);
//                                            if (UserDetailActivity.getUserList.get(x).getUserId().equals(String.valueOf(id))) {
//                                                Log.d("TTT", "position: " + x);
//                                                UserDetailActivity.getUserList.remove(x);
//                                                UserDetailActivity.rvUserPost.getAdapter().notifyItemRemoved(x);
//                                            }
//                                        }
//                                    }
//
////                                if(StudentPostTagImageActivity.getUserList!=null) {
////                                    for (int x = StudentPostTagImageActivity.getUserList.size() - 1; x >= 0; x--) {
////                                        Log.d("TTT", "loop: " + x);
////                                        if (StudentPostTagImageActivity.getUserList.get(x).getUserId().equals(String.valueOf(id))) {
////                                            Log.d("TTT", "position: " + x);
////                                            StudentPostTagImageActivity.getUserList.remove(x);
////                                            StudentPostTagImageActivity.rvUserTagPost.getAdapter().notifyItemRemoved(x);
////                                        }
////                                    }
////                                }
//
//
//                                    Toast.makeText(getApplicationContext(), "Successfully delete the user.", Toast.LENGTH_SHORT).show();
//                                    Intent i = new Intent();
//                                    i.putExtra("deleteUser", id);
//                                    setResult(RESULT_OK, i);
//                                    finish();
//                                } else if (response.body().getResponseCode().equals("10")) {
//                                    Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
//                                    LoginUser loginSP = new LoginUser(StaffPostDetailActivity.this);
//                                    loginSP.clearData();
//
//                                    Intent i = new Intent(StaffPostDetailActivity.this, LoginActivity.class);
//                                    startActivity(i);
//                                    finish();
//                                } else {
//                                    Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
//                                }
//
//                            }
//
//                            @Override
//                            public void onFailure(Call<GiveReport> call, Throwable t) {
//                                if (progressDialog.isShowing()) {
//                                    progressDialog.dismiss();
//                                }
//                                Toast.makeText(getApplicationContext(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
//                            }
//                        });
//                    }
//                });
//
//                tvNo.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        alert.dismiss();
//                    }
//                });
//                alert.show();
            }
        });

        //view content
        binding.tvReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("TTT", "Post id: " + binding.getStaffpostlist().getPostId());
                if (!binding.tvReport.getText().toString().trim().equalsIgnoreCase("Not reported yet")) {
                    Intent i = new Intent(getApplicationContext(), ReportContentActivity.class);
                    i.putExtra("postId", binding.getStaffpostlist().getPostId());
                    startActivity(i);
                }
            }
        });


        //report delete
        binding.imgReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(StaffPostDetailActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
                dialogBuilder.setView(dialogView);
                final AlertDialog alert = dialogBuilder.create();
                alert.setCancelable(false);
                alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
                tvMessage.setText("Are you sure you want to remove the post?");

                TextView tvYes = dialogView.findViewById(R.id.tvYes);
                TextView tvNo = dialogView.findViewById(R.id.tvNo);

                tvYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        alert.dismiss();
                        final String id = binding.getStaffpostlist().getPostId();
                        progressDialog = ProgressDialog.show(StaffPostDetailActivity.this, "", "", true);
                        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        progressDialog.setContentView(R.layout.progress_view);
                        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                        Circle bounce = new Circle();
                        bounce.setColor(Color.BLACK);
                        progressBar.setIndeterminateDrawable(bounce);

                        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                        final Call<GiveReport> userPost = retrofitClass.staffDeletePost(Integer.parseInt(id), userId);
                        userPost.enqueue(new Callback<GiveReport>() {
                            @Override
                            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                                if (response.body().getResponseCode().equals("1")) {

//                                    if (UserDetailActivity.getUserList != null) {
//                                        for (int i = 0; i < UserDetailActivity.getUserList.size(); i++) {
//                                            if (UserDetailActivity.getUserList.get(i).getPostId().equals(String.valueOf(id))) {
//                                                Log.d("TTT", "Remove " + id + " / " + i);
//                                                UserDetailActivity.getUserList.remove(i);
//                                                UserDetailActivity.rvUserPost.getAdapter().notifyItemRemoved(i);
//                                            }
//                                        }
//                                    }
//                                if(StudentPostTagImageActivity.getUserList!=null) {
//                                    for (int i = 0; i < StudentPostTagImageActivity.getUserList.size(); i++) {
//                                        if (StudentPostTagImageActivity.getUserList.get(i).getPostId().equals(String.valueOf(id))) {
//                                            Log.d("TTT", "Remove " + id + " / " + i);
//                                            StudentPostTagImageActivity.getUserList.remove(i);
//                                            StudentPostTagImageActivity.rvUserTagPost.getAdapter().notifyItemRemoved(i);
//                                        }
//                                    }
//                                }

                                    Toast.makeText(getApplicationContext(), "Successfully delete the post.", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent();
                                    i.putExtra("deletePost", id);
                                    setResult(RESULT_OK, i);
                                    finish();
                                } else if (response.body().getResponseCode().equals("10")) {
                                    Toast.makeText(StaffPostDetailActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                    LoginUser loginSP = new LoginUser(StaffPostDetailActivity.this);
                                    loginSP.clearData();

                                    Intent i = new Intent(StaffPostDetailActivity.this, LoginActivity.class);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Toast.makeText(StaffPostDetailActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<GiveReport> call, Throwable t) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                                Toast.makeText(getApplicationContext(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });

                tvNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });
                alert.show();
            }

        });

    }

    public void showDialogForSameStudent(final PostListItem postList, ImageView imgOption) {

        PopupMenu popup = new PopupMenu(StaffPostDetailActivity.this, imgOption);
        popup.getMenuInflater().inflate(R.menu.same_staff_opt, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Edit")) {
                    Log.d("TTT", "Postttt dialog: " + postList.getPostId() + " / " + postList.getUserTagg().size());
                    Intent i = new Intent(StaffPostDetailActivity.this, AddPostActivity.class);
                    i.putExtra("Edit", "Edit");
                    i.putExtra("Thumb", postList.getPostThumbnail());
                    i.putExtra("PostId", postList.getPostId());
                    i.putExtra("Description", postList.getPostDescription());
                    i.putExtra("Title", postList.getPostTitle());
                    i.putExtra("postUrl", postList.getPostFile());
                    i.putExtra("tagUserId", (ArrayList<UserTagg>) postList.getUserTagg());
                    startActivity(i);
                } else if (item.getTitle().equals("Delete")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(StaffPostDetailActivity.this);
                    builder.setMessage("Are you sure you want to delete the post?")
                            .setCancelable(false)
                            .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    progressDialog = ProgressDialog.show(StaffPostDetailActivity.this, "", "", true);
                                    progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    progressDialog.setContentView(R.layout.progress_view);
                                    ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                                    Circle bounce = new Circle();
                                    bounce.setColor(Color.BLACK);
                                    progressBar.setIndeterminateDrawable(bounce);

                                    RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                    final Call<GiveReport> deletePost = retrofitClass.deletePost(Integer.parseInt(postList.getPostId()));
                                    deletePost.enqueue(new Callback<GiveReport>() {
                                        @Override
                                        public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                            if (progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }

                                            if (response.body().getResponseCode().equals("1")) {
                                                Toast.makeText(StaffPostDetailActivity.this, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
//                                                for (int i = 0; i < postLists.size(); i++) {
//                                                    if (postLists.get(i).getPostId().equals(String.valueOf(postList.getPostId()))) {
//                                                        Log.d("TTT", "Removeeee " + postList.getPostId() + " / " + i);
//                                                        postLists.remove(i);
//                                                        notifyItemRemoved(i);
//                                                    }
//                                                }

                                                if (StudentPostTagImageActivity.getUserList != null) {
                                                    for (int i = 0; i < StudentPostTagImageActivity.getUserList.size(); i++) {
                                                        if (StudentPostTagImageActivity.getUserList.get(i).getPostId().equals(postList.getPostId())) {
                                                            Log.d("TTT", "Removeeee " + postList.getPostId() + " / " + i);
                                                            StudentPostTagImageActivity.getUserList.remove(i);
                                                            StudentPostTagImageActivity.rvUserTagPost.getAdapter().notifyItemRemoved(i);
                                                        }
                                                    }
                                                }
                                                finish();

                                            } else if (response.body().getResponseCode().equals("10")) {
                                                Toast.makeText(StaffPostDetailActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                                LoginUser loginSP = new LoginUser(StaffPostDetailActivity.this);
                                                loginSP.clearData();

                                                Intent i = new Intent(StaffPostDetailActivity.this, LoginActivity.class);
                                                startActivity(i);
                                                finish();
                                            } else {
                                                Toast.makeText(StaffPostDetailActivity.this, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<GiveReport> call, Throwable t) {
                                            if (progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }
                                            Toast.makeText(StaffPostDetailActivity.this, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            })
                            .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    }
                            );

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    alert.setTitle("");
                    alert.show();

                }
                return true;
            }
        });
        popup.show();//showing popup menu
    }
}

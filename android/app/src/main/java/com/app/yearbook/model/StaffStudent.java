package com.app.yearbook.model;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StaffStudent {
    //staff-------------------------------------------------------------------------------------
    @SerializedName("staff_id")
    @Expose
    private String staffId;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("staff_phone")
    @Expose
    private String staffPhone;
    @SerializedName("staff_email")
    @Expose
    private String staffEmail;
    @SerializedName("staff_password")
    @Expose
    private String staffPassword;
    @SerializedName("staff_device_type")
    @Expose
    private String staffDeviceType;
    @SerializedName("staff_device_token")
    @Expose
    private String staffDeviceToken;
    @SerializedName("staff_access_code")
    @Expose
    private String staffAccessCode;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("staff_request_status")
    @Expose
    private String staffRequestStatus;
    @SerializedName("staff_token")
    @Expose
    private String staffToken;

    @SerializedName("staff_is_agree")
    @Expose
    private String staffIsAgree;

    @SerializedName("staff_firstname")
    @Expose
    private String staffFirstname;

    @SerializedName("staff_lastname")
    @Expose
    private String staffLastname;

    @SerializedName("staff_image")
    @Expose
    private String staffImage;

    @SerializedName("is_yearbook_purchased")
    private String isYearbookPurchased;

    @SerializedName("is_archived")
    private String isArchived;

    public String getIsArchived() {
        return isArchived;
    }
    public boolean isArchived() {
        return (!TextUtils.isEmpty(isArchived) && isArchived.equalsIgnoreCase("Yes"))?true:false;
    }
    public void setIsArchived(String isArchived) {
        this.isArchived = isArchived;
    }

    public String getIsYearbookPurchased() {
        return isYearbookPurchased;
    }
    public boolean isYearbookPurchased() {
        return (!TextUtils.isEmpty(isYearbookPurchased) && isYearbookPurchased.equalsIgnoreCase("Yes"))?true:false;
    }
    public void setIsYearbookPurchased(String isYearbookPurchased) {
        this.isYearbookPurchased = isYearbookPurchased;
    }

    public String getStaffFirstname() {
        return staffFirstname;
    }

    public void setStaffFirstname(String staffFirstname) {
        this.staffFirstname = staffFirstname;
    }

    public String getStaffLastname() {
        return staffLastname;
    }

    public void setStaffLastname(String staffLastname) {
        this.staffLastname = staffLastname;
    }

    public String getStaffImage() {
        return staffImage;
    }

    public void setStaffImage(String staffImage) {
        this.staffImage = staffImage;
    }

    public String getStaffToken() {
        return staffToken;
    }

    public void setStaffToken(String staffToken) {
        this.staffToken = staffToken;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getStaffPhone() {
        return staffPhone;
    }

    public void setStaffPhone(String staffPhone) {
        this.staffPhone = staffPhone;
    }

    public String getStaffEmail() {
        return staffEmail;
    }

    public void setStaffEmail(String staffEmail) {
        this.staffEmail = staffEmail;
    }

    public String getStaffPassword() {
        return staffPassword;
    }

    public void setStaffPassword(String staffPassword) {
        this.staffPassword = staffPassword;
    }

    public String getStaffDeviceType() {
        return staffDeviceType;
    }

    public void setStaffDeviceType(String staffDeviceType) {
        this.staffDeviceType = staffDeviceType;
    }

    public String getStaffDeviceToken() {
        return staffDeviceToken;
    }

    public void setStaffDeviceToken(String staffDeviceToken) {
        this.staffDeviceToken = staffDeviceToken;
    }

    public String getStaffAccessCode() {
        return staffAccessCode;
    }

    public void setStaffAccessCode(String staffAccessCode) {
        this.staffAccessCode = staffAccessCode;
    }

    // "user" and "staff"
    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getStaffRequestStatus() {
        return staffRequestStatus;
    }

    public void setStaffRequestStatus(String staffRequestStatus) {
        this.staffRequestStatus = staffRequestStatus;
    }

    public String getStaffIsAgree() {
        return staffIsAgree;
    }

    public void setStaffIsAgree(String staffIsAgree) {
        this.staffIsAgree = staffIsAgree;
    }

    //student-------------------------------------------------------------------------------------
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_firstname")
    @Expose
    private String userFirstname;
    @SerializedName("user_lastname")
    @Expose
    private String userLastname;
    @SerializedName("user_address")
    @Expose
    private String userAddress;
    @SerializedName("user_phone")
    @Expose
    private String userPhone;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("user_password")
    @Expose
    private String userPassword;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("user_student_id")
    @Expose
    private String userStudentId;
    @SerializedName("user_device_type")
    @Expose
    private String userDeviceType;
    @SerializedName("user_device_token")
    @Expose
    private String userDeviceToken;
    @SerializedName("user_access_code")
    @Expose
    private String userAccessCode;
    @SerializedName("is_access_code_verify")
    @Expose
    private String isAccessCodeVerify;
    @SerializedName("user_is_agree")
    @Expose
    private String userIsAgree;

    @SerializedName("user_token")
    @Expose
    private String userToken;

    @SerializedName("user_grade")
    @Expose
    private String userGrade;

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserStudentId() {
        return userStudentId;
    }

    public void setUserStudentId(String userStudentId) {
        this.userStudentId = userStudentId;
    }

    public String getUserDeviceType() {
        return userDeviceType;
    }

    public void setUserDeviceType(String userDeviceType) {
        this.userDeviceType = userDeviceType;
    }

    public String getUserDeviceToken() {
        return userDeviceToken;
    }

    public void setUserDeviceToken(String userDeviceToken) {
        this.userDeviceToken = userDeviceToken;
    }

    public String getUserAccessCode() {
        return userAccessCode;
    }

    public void setUserAccessCode(String userAccessCode) {
        this.userAccessCode = userAccessCode;
    }

    public String getIsAccessCodeVerify() {
        return isAccessCodeVerify;
    }

    public void setIsAccessCodeVerify(String isAccessCodeVerify) {
        this.isAccessCodeVerify = isAccessCodeVerify;
    }

    public String getUserIsAgree() {
        return userIsAgree;
    }

    public void setUserIsAgree(String userIsAgree) {
        this.userIsAgree = userIsAgree;
    }

    public String getUserGrade() {
        return userGrade;
    }

    public void setUserGrade(String userGrade) {
        this.userGrade = userGrade;
    }

    //employee-------------------------------------------------------------------------------------
    @SerializedName("employee_id")
    @Expose
    private String employeeId;
    @SerializedName("employee_firstname")
    @Expose
    private String employeeFirstname;
    @SerializedName("employee_lastname")
    @Expose
    private String employeeLastname;
    @SerializedName("employee_token")
    @Expose
    private String employeeToken;


    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeFirstname() {
        return employeeFirstname;
    }

    public void setEmployeeFirstname(String employeeFirstname) {
        this.employeeFirstname = employeeFirstname;
    }

    public String getEmployeeLastname() {
        return employeeLastname;
    }

    public void setEmployeeLastname(String employeeLastname) {
        this.employeeLastname = employeeLastname;
    }

    public String getEmployeeToken() {
        return employeeToken;
    }

    public void setEmployeeToken(String employeeToken) {
        this.employeeToken = employeeToken;
    }


}

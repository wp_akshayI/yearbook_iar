package com.app.yearbook.utils;

import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("MyRefreshedToken", token);

        saveValue(token);
        //eiKFKMbZMEo:APA91bEx6Q0xDzhD0wqINmTCrfjEgQxWalLsGa4BAaJrFZjDuwBMpJQso1UcVKz2mS92U6scI1UywV_ruxdVw__kwaWWj9FqB6NXzmbgI0xYGXTKVZlTtrJ2DF0vw5fuYKwSfh5L_SLz
    }

    public void saveValue(String token){
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("Token", token).apply();
    }
}

package com.app.yearbook;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.yearbook.model.profile.BookmarkListItem;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

public class StudentBookmarkActivity extends AppCompatActivity {

    public static RecyclerView rvUserBookmark;
    private LinearLayout lvNoData;
    public ProgressDialog progressDialog;
    private AllMethods allMethods;
    private String userId;
    public static ArrayList<BookmarkListItem> bookmarkList;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_bookmark);

        setView();
    }

    public void setView()
    {
        //toolbar
        setToolbar();

        //allMethods
        allMethods=new AllMethods();

        //lv no
        lvNoData =findViewById(R.id.lvNoData);

        //rv
        rvUserBookmark = findViewById(R.id.rvUserBookmark);

        //intent
        if (getIntent().getExtras() != null) {
            userId = getIntent().getStringExtra("userId");
        }

        getBookMark();
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Bookmarks");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getBookMark()
    {
        //Progress bar
//        progressDialog = ProgressDialog.show(StudentBookmarkActivity.this, "", "", true);
//        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        progressDialog.setContentView(R.layout.progress_view);
//        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
//        Circle bounce = new Circle();
//        bounce.setColor(Color.BLACK);
//        progressBar.setIndeterminateDrawable(bounce);
//
//        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
//        final Call<GetAllBookmark> bookmark = retrofitClass.getBookmark(Integer.parseInt(userId));
//        bookmark.enqueue(new Callback<GetAllBookmark>() {
//            @Override
//            public void onResponse(Call<GetAllBookmark> call, Response<GetAllBookmark> response) {
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//                if (response.body().getResponseCode().equals("1")) {
//                    bookmarkList=new ArrayList<>();
//                    if(response.body().getBookmarkList().size()>0)
//                    {
//                        rvUserBookmark.setVisibility(View.VISIBLE);
//                        lvNoData.setVisibility(View.GONE);
//                        bookmarkList.addAll(response.body().getBookmarkList());
//
//                        GridLayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
//                        rvUserBookmark.setLayoutManager(mLayoutManager);
//
//                        BookmarkAdapter mAdapter = new BookmarkAdapter(bookmarkList, StudentBookmarkActivity.this);
//                        rvUserBookmark.setAdapter(mAdapter);
//                    }
//                }
//                else if(response.body().getResponseCode().equals("10")) {
//                    Toast.makeText(StudentBookmarkActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
//                    LoginUser loginSP = new LoginUser(StudentBookmarkActivity.this);
//                    loginSP.clearData();
//
//                    Intent i = new Intent(StudentBookmarkActivity.this, LoginActivity.class);
//                    startActivity(i);
//                    finish();
//                }
//                else
//                {
//                    rvUserBookmark.setVisibility(View.GONE);
//                    lvNoData.setVisibility(View.VISIBLE);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GetAllBookmark> call, Throwable t) {
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//                allMethods.setAlert(StudentBookmarkActivity.this, "", t.getMessage() + "");
//            }
//        });


    }
}

package com.app.yearbook;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.app.yearbook.sharedpreferance.LoginUser;

public class UserBlockActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_block);

        //toolbar
        setToolbar();

        TextView tvMsg=findViewById(R.id.tvMsg);

        if(getIntent().getExtras()!=null)
        {
            String msg=getIntent().getStringExtra("Msg");
            if(msg.equalsIgnoreCase("block"))
            {
                tvMsg.setText(getResources().getString(R.string.blockuser));
            }
            else if(msg.equalsIgnoreCase("school"))
            {
                tvMsg.setText(getResources().getString(R.string.deleteschool));
            }
        }
    }

    @Override
    public void onBackPressed() {

        LoginUser loginSP = new LoginUser(UserBlockActivity.this);
        if (loginSP.getUserData() != null) {
            loginSP.clearData();
        }

        Intent i = new Intent(UserBlockActivity.this, LoginActivity.class);
        startActivity(i);
        finish();

        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}

const img_section_gradient = require('./img_section_gradient.png')
const published = require('./published.png')
const UnAthorized_ic = require('./UnAthorized_ic.png')
const warning_ic = require('./warning_ic.jpg')
const bookmark_ic = require('./icons/bookmark_ic@3x.png')
const bookmark_fill_ic = require('./icons/bookmark_fill_ic@3x.png')
const diamond_ic = require('./icons/diamond_ic@3x.png')
const diamond_ic_selected = require('./icons/diamond_ic_selected@3x.png')
const pause_ic = require('./icons/pause_ic@3x.png')
const play_ic = require('./icons/play@3x.png')
const menu_ic = require('./icons/menu_ic@3x.png')
const arrow_right = require('./icons/arrow_right.png')
const video_placeholder = require('./video_placeholder@3x.png')
const image_placeholder = require('./image_placeholder@3x.png')
const nativePlaceholder = require('./image_placeholder@3x.png')
const down_arrow_ic = require('./icons/down_arrow_ic@3x.png')
const up_arrow_ic = require('./icons/up_arrow_ic@3x.png')

export {
  img_section_gradient,
  bookmark_ic,
  bookmark_fill_ic,
  diamond_ic,
  diamond_ic_selected,
  pause_ic,
  play_ic,
  menu_ic,
  published,
  UnAthorized_ic,
  warning_ic,
  arrow_right,
  video_placeholder,
  down_arrow_ic,
  up_arrow_ic,
  image_placeholder,
  nativePlaceholder
}

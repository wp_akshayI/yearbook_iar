package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddSignatureActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private EditText edtMsg;
    private ProgressDialog progressDialog;
    private MenuItem next;
    private String userId, searchUserId;
    private LoginUser loginUser;
    public AllMethods utils;
    private int fontType=1;
    private TextView tvFontHoney,tvFontAngelique,tvFontBrasileirinha;
    private Typeface typefaceHoney,typefaceAngelique,typefaceBrasileir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_signature);
        setView();
    }

    public void setView() {

        //utils
        utils = new AllMethods();

        //toolbar
        setToolbar();

        loginUser = new LoginUser(AddSignatureActivity.this);
        if (loginUser.getUserData() != null) {
            userId = loginUser.getUserData().getUserId();
        }

        //search user id
        if (getIntent().getExtras() != null) {
            searchUserId = getIntent().getStringExtra("searchUserId");
        }

        //text view
        tvFontHoney=findViewById(R.id.tvFontHoney);
        tvFontHoney.setOnClickListener(this);
        typefaceHoney = Typeface.createFromAsset(getAssets(), "fonts/Honeymoon Personal Use.ttf");
        tvFontHoney.setTypeface(typefaceHoney);

        tvFontAngelique=findViewById(R.id.tvFontAngelique);
        tvFontAngelique.setOnClickListener(this);
        typefaceAngelique = Typeface.createFromAsset(getAssets(), "fonts/Angelique-Rose-font-FFP_0.ttf");
        tvFontAngelique.setTypeface(typefaceAngelique);

        tvFontBrasileirinha=findViewById(R.id.tvFontBrasileirinha);
        tvFontBrasileirinha.setOnClickListener(this);
        typefaceBrasileir = Typeface.createFromAsset(getAssets(), "fonts/Brasileirinha Personal Use.ttf");
        tvFontBrasileirinha.setTypeface(typefaceBrasileir);


        //msg
        edtMsg = findViewById(R.id.edtMsg);
        edtMsg.setTextSize(20f);
        edtMsg.setTypeface(typefaceHoney);

        edtMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    next.setVisible(true);
                } else {
                    next.setVisible(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //toolbar
    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Signature");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.next) {

            Log.d("TTT", "onOptionsItemSelected: "+edtMsg.getText().toString());
            AllMethods.hideKeyboard(this,this.getCurrentFocus());
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to post this? You can only sign this person’s yearbook once. You will not be able to edit this once posted.")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                            dialog.dismiss();
                            addSignature();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

        }
        return super.onOptionsItemSelected(item);
    }

    //function for add signature
    public void addSignature() {
        //Progress bar
        progressDialog = ProgressDialog.show(AddSignatureActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> userPost = retrofitClass.addSignature(Integer.parseInt(userId), Integer.parseInt(searchUserId), edtMsg.getText().toString(),fontType);
        userPost.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if(response.body().getResponseCode().equals("1"))
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddSignatureActivity.this);
                    builder.setMessage(response.body().getResponseMsg())
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                            finish();
                                        }
                                    }
                            );

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                else if(response.body().getResponseCode().equals("10")) {
                    Toast.makeText(AddSignatureActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(AddSignatureActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(AddSignatureActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    utils.setAlert(AddSignatureActivity.this, "",response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                utils.setAlert(AddSignatureActivity.this, "", t.getMessage());
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selectimage, menu);
        next = menu.findItem(R.id.next);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.tvFontHoney)
        {
            fontType=1;
            edtMsg.setTextSize(20f);
            edtMsg.setTypeface(typefaceHoney);
            setColorToTextView(tvFontHoney,R.color.deselectTab,R.color.gray);
            setColorToTextView(tvFontAngelique,R.color.gray,R.color.deselectTab);
            setColorToTextView(tvFontBrasileirinha,R.color.gray,R.color.deselectTab);

            YoYo.with(Techniques.Tada)
                    .duration(500)
                    .playOn(findViewById(R.id.tvFontHoney));

        }
        else if(v.getId()==R.id.tvFontAngelique)
        {
            fontType=2;
            edtMsg.setTextSize(30f);
            edtMsg.setTypeface(typefaceAngelique);
            setColorToTextView(tvFontHoney,R.color.gray,R.color.deselectTab);
            setColorToTextView(tvFontAngelique,R.color.deselectTab,R.color.gray);
            setColorToTextView(tvFontBrasileirinha,R.color.gray,R.color.deselectTab);

            YoYo.with(Techniques.Tada)
                    .duration(500)
                    .playOn(findViewById(R.id.tvFontAngelique));
        }
        else if(v.getId()==R.id.tvFontBrasileirinha)
        {
            fontType=3;
            edtMsg.setTextSize(30f);
            edtMsg.setTypeface(typefaceBrasileir);
            setColorToTextView(tvFontHoney,R.color.gray,R.color.deselectTab);
            setColorToTextView(tvFontAngelique,R.color.gray,R.color.deselectTab);
            setColorToTextView(tvFontBrasileirinha,R.color.deselectTab,R.color.gray);

            YoYo.with(Techniques.Tada)
                    .duration(500)
                    .playOn(findViewById(R.id.tvFontBrasileirinha));
        }
    }

    public void setColorToTextView(TextView tvFont,int backgroundColor,int textColor)
    {
        tvFont.setBackgroundColor(getResources().getColor(backgroundColor));
        tvFont.setTextColor(getResources().getColor(textColor));
    }
}

package com.app.yearbook.fragment;

import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.yearbook.R;

public class VideoFragment extends Fragment {

    public View view;
    private NavigationView navigationView;
    private DrawerLayout drawer;

    public VideoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.test, container, false);
        setView();
        return view;
    }

    public void setView()
    {
        drawer = view.findViewById(R.id.drawer_layout);
        navigationView = view.findViewById(R.id.nav_view);

        setUpNavigationView();
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//
//            // This method will trigger on item Click of navigation menu
//            @Override
//            public boolean onNavigationItemSelected(MenuItem menuItem) {
//
//                //Check to see which item was being clicked and perform appropriate action
//                switch (menuItem.getItemId()) {
//                    //Replacing the main content with ContentFragment Which is our Inbox View;
//                    case R.id.tv1:
//                        break;
//                    case R.id.tv2:
//                        break;
//                    case R.id.tv3:
//                        break;
//
////                    case R.id.nav_about_us:
////                        // launch new intent instead of loading fragment
////                        startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
////                        drawer.closeDrawers();
////                        return true;
////                    case R.id.nav_privacy_policy:
////                        // launch new intent instead of loading fragment
////                        startActivity(new Intent(MainActivity.this, PrivacyPolicyActivity.class));
////                        drawer.closeDrawers();
////                        return true;
//                    default:
//                        break;
//                }
//
//                //Checking if the item is in checked state or not, if not make it in checked state
//                if (menuItem.isChecked()) {
//                    menuItem.setChecked(false);
//                } else {
//                    menuItem.setChecked(true);
//                }
//                menuItem.setChecked(true);
//
//                loadHomeFragment();
//
//                return true;
//            }
//        });

        drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(v.getId()==R.id.tv1)
//                {
//                    Toast.makeText(getActivity(),"1",Toast.LENGTH_SHORT).show();
//                }
//                else  if(v.getId()==R.id.tv2)
//                {
//                    Toast.makeText(getActivity(),"2",Toast.LENGTH_SHORT).show();
//                }
//                else  if(v.getId()==R.id.tv3)
//                {
//                    Toast.makeText(getActivity(),"3",Toast.LENGTH_SHORT).show();
//                }
            }
        });
        Toolbar toolbar =getActivity().findViewById(R.id.toolbar);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawer,toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        //navigationView.getMenu().getItem(navItemIndex).setChecked(true);

        // set toolbar title
        //setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
//        if (getActivity().getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
//            drawer.closeDrawers();
//
//            // show or hide the fab button
//            //toggleFab();
//            return;
//        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
//        Runnable mPendingRunnable = new Runnable() {
//            @Override
//            public void run() {
//                // update the main content by replacing fragments
//                Fragment fragment = getHomeFragment();
//                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
//                        android.R.anim.fade_out);
//                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
//                fragmentTransaction.commitAllowingStateLoss();
//            }
//        };

        // If mPendingRunnable is not null, then add to the message queue
//        if (mPendingRunnable != null) {
//            mHandler.post(mPendingRunnable);
//        }


        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        getActivity().invalidateOptionsMenu();
    }

}

package com.app.yearbook;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.yearbook.fragment.AccessCodeDisable;
import com.app.yearbook.fragment.LibraryFragment;
import com.app.yearbook.fragment.StaffHomeFragment;
import com.app.yearbook.fragment.StaffProfileFragment;
import com.app.yearbook.fragment.StaffSearchFragment;
import com.app.yearbook.fragment.StaffTrashFragment;
import com.app.yearbook.fragment.StaffYearbookFragment;
import com.app.yearbook.fragment.YearBookFG;
import com.app.yearbook.fragment.YearbookFragment;
import com.app.yearbook.model.studenthome.GetSchoolDuration;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.Constants;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.PlayerManager;

public class StaffHomeActivity extends AppCompatActivity{

    public AllMethods utils;
    private Toolbar toolbar;
    private LoginUser loginUser;
    public String studentName = "", schoolName = "", schoolId = "";
    // private BottomNavigationView bottomNavigationView;
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private StaffHomeFragment homeFragment;
    private AccessCodeDisable accessCodeFragment;
    private StaffSearchFragment searchFragment;
    private StaffTrashFragment staffTrashFragment;
    private StaffProfileFragment staffProfileFragment;
    private LibraryFragment libraryFragment;
    private FragmentTransaction transaction;
    private YearbookFragment sttafYearbookFragment;
//    public static YearBookFG sttafYearbookFragment;
    private Fragment fragment;
    // private BottomNavigationViewEx bnve;
    private TextView toolbar_title, toolbar_title_left;
    public ProgressDialog progressDialog;
    public String start_date, end_date;

    private DrawerLayout drawerLayout;
    NavigationView navigationView;
    public static int navItemIndex = 0;
    ActionBarDrawerToggle actionBarDrawerToggle;
    FloatingActionButton btnAddPost;
    public static Handler handler;

    private  boolean isFgYearBookOpen = false;
    AppBarLayout mainToolbar;
    public static StaffHomeActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_home);
        instance = this;
        initHandler();
        mainToolbar = findViewById(R.id.main_toolbar);
        //all control
        setView();

        if (getIntent().getBooleanExtra("isRedirect", false)){
            if (getIntent().getStringExtra("redirectOn").contentEquals("notification")){

                Bundle bundle = new Bundle();
                bundle.putString("redirectOn", getIntent().getStringExtra("redirectOn"));


                navItemIndex = 4;
                toolbarVisibility(1, "");
                staffProfileFragment = new StaffProfileFragment();
                fragment = staffProfileFragment;
                fragment.setArguments(bundle);
                transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.frame_layout, staffProfileFragment).commit();
                btnAddPost.setVisibility(View.GONE);
            }
        }
    }

    private void initHandler() {
        handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                if (msg.what == Constants.SEARCH) {
//                    navigationView.setCheckedItem(R.id.action_search);
//                    navItemIndex = 3;
                    toolbarVisibility(2, getString(R.string.lbl_search));
                    searchFragment = new StaffSearchFragment();
                    fragment = searchFragment;
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.frame_layout, searchFragment).commit();
                }
                return false;
            }
        });
    }

    public void setView() {
        //SP object
        loginUser = new LoginUser(StaffHomeActivity.this);
        if (loginUser.getUserData() != null) {
            studentName = loginUser.getUserData().getSchoolName();
            schoolName = loginUser.getUserData().getSchoolName();
            schoolId = loginUser.getUserData().getSchoolId();
        }

        //utils
        utils = new AllMethods();

        //set toolbar
        setToolbar();

        //1:transaprent toolbar, 2:white toolbar
        //toolbarVisibility(1, "");

        checkCameraOption();
        //-------------------------------drawer---------------------------------
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        setUpNavigationView();

        btnAddPost = findViewById(R.id.btn_add_post);

        if (loginUser.getUserData().getIsAccessCodeVerify() != null && loginUser.getUserData().getIsAccessCodeVerify().equalsIgnoreCase("Yes")) {
            homeFragment = new StaffHomeFragment();
            fragment = homeFragment;
            btnAddPost.setVisibility(View.VISIBLE);
        }else{
            accessCodeFragment = new AccessCodeDisable();
            fragment = accessCodeFragment;
            btnAddPost.setVisibility(View.GONE);
        }

        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_layout, fragment).commit();

//        bnve = findViewById(R.id.bnve);
//        bnve.enableAnimation(false);
//        bnve.enableShiftingMode(false);
//        bnve.enableItemShiftingMode(false);
//        bnve.setTextVisibility(false);
//        bnve.setPadding(0, 5, 0, 10);
//
//        bnve.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                switch (item.getItemId()) {
//                    case R.id.action_home:
//                        if (fragment instanceof StaffHomeFragment) {
//                            return false;
//                        } else {
//                            toolbar_title_left.setVisibility(View.VISIBLE);
//                            toolbar_title.setVisibility(View.GONE);
//                            toolbar_title_left.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/timesnewroman.ttf"));
//                            toolbar_title_left.setText(schoolName);
//                            homeFragment = new StaffHomeFragment();
//                            fragment = homeFragment;
//                            transaction = fragmentManager.beginTransaction();
//                            transaction.replace(R.id.frame_layout, homeFragment).commit();
//                        }
//                        break;
//                    case R.id.action_yearbook:
//                        if (fragment instanceof StaffYearbookFragment) {
//                            return false;
//                        } else {
//                            toolbar_title_left.setVisibility(View.GONE);
//                            toolbar_title.setVisibility(View.VISIBLE);
//                            toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd-Medium.otf"));
//                            toolbar_title.setText("Yearbooks");
//                            sttafYearbookFragment = new StaffYearbookFragment();
//
//                            //for 1st version
//                            // sttafYearbookFragment.setHasOptionsMenu(true);
//
//                            fragment = sttafYearbookFragment;
//                            transaction = fragmentManager.beginTransaction();
//                            transaction.replace(R.id.frame_layout, sttafYearbookFragment).commit();
//                        }
//                        break;
//                    case R.id.action_search:
//                        if (fragment instanceof StaffSearchFragment) {
//                            return false;
//                        } else {
//                            toolbar_title_left.setVisibility(View.GONE);
//                            toolbar_title.setVisibility(View.VISIBLE);
//                            toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd-Medium.otf"));
//                            toolbar_title.setText("Search");
//                            searchFragment = new StaffSearchFragment();
//                            fragment = searchFragment;
//                            transaction = fragmentManager.beginTransaction();
//                            transaction.replace(R.id.frame_layout, searchFragment).commit();
//                        }
//                        break;
//                    case R.id.action_bin:
//                        if (fragment instanceof StaffTrashFragment) {
//                            return false;
//                        } else {
//                            toolbar_title_left.setVisibility(View.GONE);
//                            toolbar_title.setVisibility(View.VISIBLE);
//                            toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd-Medium.otf"));
//                            toolbar_title.setText("Bin");
//                            staffTrashFragment = new StaffTrashFragment();
//                            fragment = staffTrashFragment;
//                            transaction = fragmentManager.beginTransaction();
//                            transaction.replace(R.id.frame_layout, staffTrashFragment).commit();
//                        }
//                        break;
//                    case R.id.action_camera:
//                        if (fragment instanceof LibraryFragment) {
//                            return false;
//                        } else {
//                            toolbar_title_left.setVisibility(View.GONE);
//                            toolbar_title.setVisibility(View.VISIBLE);
//                            toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd-Medium.otf"));
//                            toolbar_title.setText("Library");
//                            libraryFragment = new LibraryFragment();
//                            fragment = libraryFragment;
//                            transaction = fragmentManager.beginTransaction();
//                            transaction.replace(R.id.frame_layout, libraryFragment).commit();
//                        }
//                        break;
//                }
//                return true;
//            }
//        });

//        bottomNavigationView = findViewById(R.id.bottom_navigation);
//        BottomNavigationViewHelper.removeShiftMode(bottomNavigationView);

        //for 1st version
//        setVersionView(1);

//        bottomNavigationView.setOnNavigationItemSelectedListener(
//                new BottomNavigationView.OnNavigationItemSelectedListener() {
//                    @Override
//                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                        switch (item.getItemId()) {
//                            case R.id.action_home:
//                                if (fragment instanceof StaffHomeFragment) {
//                                    return false;
//                                } else {
//                                    homeFragment = new StaffHomeFragment();
//                                    fragment = homeFragment;
//                                    transaction = fragmentManager.beginTransaction();
//                                    transaction.replace(R.id.frame_layout, homeFragment).commit();
//                                }
//                                break;
//                            case R.id.action_yearbook:
//                                if (fragment instanceof StaffYearbookFragment) {
//                                    return false;
//                                } else {
//                                    sttafYearbookFragment = new StaffYearbookFragment();
//                                    fragment = sttafYearbookFragment;
//                                    transaction = fragmentManager.beginTransaction();
//                                    transaction.replace(R.id.frame_layout, sttafYearbookFragment).commit();
//                                }
//                                break;
//                            case R.id.action_search:
//                                if (fragment instanceof StaffSearchFragment) {
//                                    return false;
//                                } else {
//                                    searchFragment = new StaffSearchFragment();
//                                    fragment = searchFragment;
//                                    transaction = fragmentManager.beginTransaction();
//                                    transaction.replace(R.id.frame_layout, searchFragment).commit();
//                                }
//                                break;
//                            case R.id.action_bin:
//                                if (fragment instanceof StaffTrashFragment) {
//                                    return false;
//                                } else {
//                                    staffTrashFragment = new StaffTrashFragment();
//                                    fragment = staffTrashFragment;
//                                    transaction = fragmentManager.beginTransaction();
//                                    transaction.replace(R.id.frame_layout, staffTrashFragment).commit();
//                                }
//                                break;
//                        }
//                        return true;
//                    }
//                });

        btnAddPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (StaffHomeFragment.handler != null) {
                    StaffHomeFragment.handler.sendEmptyMessage(Constants.SHOW_PICKER);
                }
            }
        });
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int pxToDp(int px) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private void toolbarVisibility(int i, String title) {
        if (i == 1) {
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Window w = getWindow();
                w.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }*/

            /*int dp = pxToDp(getStatusBarHeight());
            Log.d("TTT", "toolbarVisibility: dp: " + dp + " / " + getStatusBarHeight());
            LinearLayout.LayoutParams paramsToolbar = (LinearLayout.LayoutParams) toolbar.getLayoutParams();
            paramsToolbar.setMargins(0, dp, 0, 0);
            toolbar.setLayoutParams(paramsToolbar);*/

            /*LinearLayout layout = findViewById(R.id.linear_layout);
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) layout.getLayoutParams();
            params.setMargins(0, 0, 0, 0);
            layout.setLayoutParams(params);*/


            getSupportActionBar().setElevation(5);
            toolbar_title_left.setVisibility(View.GONE);
            toolbar_title.setVisibility(View.VISIBLE);
            toolbar_title.setText(loginUser.getUserData().getSchoolName());

        } else {
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Window w = getWindow();
                w.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                w.setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            }*/
//            getSupportActionBar().setTitle("");
            getSupportActionBar().setElevation(5);

            /*LinearLayout.LayoutParams paramsToolbar = (LinearLayout.LayoutParams) toolbar.getLayoutParams();
            paramsToolbar.setMargins(0, 0, 0, 0);
            toolbar.setLayoutParams(paramsToolbar);*/

            Log.d("TTT", "toolbarVisibility: " + toolbar.getHeight());
            /*LinearLayout layout = findViewById(R.id.linear_layout);
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) layout.getLayoutParams();
            params.setMargins(0, toolbar.getHeight(), 0, 0);
            layout.setLayoutParams(params);*/

            toolbar.setBackgroundColor(Color.WHITE);
            toolbar_title_left.setVisibility(View.GONE);
            toolbar_title.setVisibility(View.VISIBLE);
            toolbar_title.setText(title);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        Log.d("TTT", "Resulttt code: " + requestCode);
//        if (requestCode == 111 && resultCode == RESULT_OK) {
//            Log.d("TTTT", "Home activity onActivityResult: " + requestCode);
//
//            //bnve.setSelectedItemId(R.id.action_home);
//            navigationView.getMenu().getItem(0).setChecked(true);
//            toolbarVisibility(1, "");
//            homeFragment = new StaffHomeFragment();
//            transaction = fragmentManager.beginTransaction();
//            transaction.replace(R.id.frame_layout, homeFragment).commit();
//        }

    }

    private void setUpNavigationView() {

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setItemIconTintList(null);
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @SuppressLint("RestrictedApi")
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                showToolbar();
                isFgYearBookOpen = false;
                //Check to see which item was being clicked and perform appropriate action
                drawerLayout.closeDrawers();
                toolbar.findViewById(R.id.view_icon).setVisibility(View.GONE);
                switch (menuItem.getItemId()) {
                    case R.id.action_home:
                        navItemIndex = 0;

                        toolbarVisibility(1, "");
                        if (loginUser.getUserData().getIsAccessCodeVerify() != null && loginUser.getUserData().getIsAccessCodeVerify().equalsIgnoreCase("Yes")) {
                            homeFragment = new StaffHomeFragment();
                            fragment = homeFragment;
                            btnAddPost.setVisibility(View.VISIBLE);
                        }else{
                            accessCodeFragment = new AccessCodeDisable();
                            fragment = accessCodeFragment;
                        }
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.frame_layout, fragment).commit();
                        break;

                    case R.id.action_yearbook:
                        navItemIndex = 1;

                        isFgYearBookOpen = true;
                        toolbarVisibility(2, "Yearbooks");
//                        hideToolbar();
//                        toolbar.setVisibility(View.GONE);
                        toolbar.findViewById(R.id.view_icon).setVisibility(View.VISIBLE);
                        sttafYearbookFragment = new YearbookFragment();
//                        sttafYearbookFragment = new YearBookFG();
                        fragment = sttafYearbookFragment;
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.frame_layout, sttafYearbookFragment).commit();
                        btnAddPost.setVisibility(View.GONE);
                        break;
                    /*case R.id.action_live:
                        navItemIndex = 2;
                        toolbarVisibility(2, "Library");

                        libraryFragment = new LibraryFragment();
                        fragment = libraryFragment;
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.frame_layout, libraryFragment).commit();
                        break;*/
                    case R.id.action_search:
                        navItemIndex = 3;
                        toolbarVisibility(2, "Search");

                        searchFragment = new StaffSearchFragment();
                        fragment = searchFragment;
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.frame_layout, searchFragment).commit();
                        btnAddPost.setVisibility(View.GONE);
                        break;
                    case R.id.action_profile:
                        navItemIndex = 4;
                        toolbarVisibility(1, "");
                        staffProfileFragment = new StaffProfileFragment();
                        fragment = staffProfileFragment;
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.frame_layout, staffProfileFragment).commit();
                        btnAddPost.setVisibility(View.GONE);
                        break;
                    case R.id.action_bin:
                        navItemIndex = 5;
                        toolbarVisibility(2, "Settings");
                        staffTrashFragment = new StaffTrashFragment();
                        fragment = staffTrashFragment;
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.frame_layout, staffTrashFragment).commit();
                        btnAddPost.setVisibility(View.GONE);
                        break;

                    default:
                        navItemIndex = 0;

                        toolbarVisibility(1, "");
                        homeFragment = new StaffHomeFragment();
                        fragment = homeFragment;
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.frame_layout, homeFragment).commit();
                        btnAddPost.setVisibility(View.VISIBLE);
                        break;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                // loadHomeFragment();

                return true;
            }
        });


        actionBarDrawerToggle = new ActionBarDrawerToggle(StaffHomeActivity.this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                AllMethods.hideKeyboard(StaffHomeActivity.this, getCurrentFocus());
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                actionBarDrawerToggle.setHomeAsUpIndicator(R.drawable.menu_ic);
                actionBarDrawerToggle.syncState();
            }
        });

        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });


        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        //Closing drawer on item click
        drawerLayout.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    public void checkCameraOption() {

        //Progress bar
        progressDialog = ProgressDialog.show(StaffHomeActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetSchoolDuration> userPost = retrofitClass.getSchoolDuration(schoolId);
        userPost.enqueue(new Callback<GetSchoolDuration>() {
            @Override
            public void onResponse(Call<GetSchoolDuration> call, Response<GetSchoolDuration> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    start_date = response.body().getSchoolDetails().getSchoolStartDate();
                    end_date = response.body().getSchoolDetails().getSchoolEndDate();

                    Date startDate = null, endDate = null, cDateObject = null;   // assume these are set to something
                    Date currentDate = new Date();          // the date in question
                    String cDate = null;

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        startDate = format.parse(start_date);
                        endDate = format.parse(end_date);
                        cDate = format.format(currentDate);
                        cDateObject = format.parse(cDate);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Boolean b = (cDateObject.after(startDate) || cDateObject.equals(startDate)) && (cDateObject.before(endDate) || cDateObject.equals(endDate));
                    Log.d("TTT", "Camera option::: " + b + " // " + start_date + " / " + end_date + " / " + cDate);

                    if (!b) {
//                        navigationView.getMenu().removeItem(R.id.action_live);

//                        bnve.enableAnimation(false);
//                        bnve.enableShiftingMode(false);
//                        bnve.enableItemShiftingMode(false);
//                        bnve.setTextVisibility(false);
//                        bnve.setPadding(0, 5, 0, 10);
                    }

                    //default call 1st fragment
                    //setVersionView(2);

                    toolbarVisibility(1, "");

                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(StaffHomeActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StaffHomeActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StaffHomeActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    utils.setAlert(StaffHomeActivity.this, "", response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<GetSchoolDuration> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                utils.setAlert(StaffHomeActivity.this, "", t.getMessage());
            }
        });

    }

    public void setVersionView(int version) {
        if (version == 1) {
//            bnve.getMenu().removeItem(R.id.action_home);
//            bnve.getMenu().removeItem(R.id.action_bin);
//            bnve.enableAnimation(false);
//            bnve.enableShiftingMode(false);
//            bnve.enableItemShiftingMode(false);
//            bnve.setTextVisibility(false);
//            bnve.setPadding(0, 5, 0, 10);

            if (fragment instanceof YearBookFG) {
            } else {
                hideToolbar();
                toolbar_title_left.setVisibility(View.GONE);
                toolbar_title.setVisibility(View.VISIBLE);
                toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd-Medium.otf"));
                toolbar_title.setText("Yearbooks");
                toolbar.setVisibility(View.GONE);
//                sttafYearbookFragment = new YearBookFG();
                sttafYearbookFragment = new YearbookFragment();
                sttafYearbookFragment.setHasOptionsMenu(true);
                fragment = sttafYearbookFragment;
                transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.frame_layout, sttafYearbookFragment).commit();
            }

            //default call 1st fragmnet
//            if (fragment instanceof StaffYearbookFragment) {
//            } else {
//                getSupportActionBar().hide();
//                toolbar_title_left.setVisibility(View.GONE);
//                toolbar_title.setVisibility(View.VISIBLE);
//                toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd-Medium.otf"));
//                toolbar_title.setText("Yearbooks");
//                sttafYearbookFragment = new StaffYearbookFragment();
//                sttafYearbookFragment.setHasOptionsMenu(true);
//                fragment = sttafYearbookFragment;
//                transaction = fragmentManager.beginTransaction();
//                transaction.replace(R.id.frame_layout, sttafYearbookFragment).commit();
//            }
        } else {
            //default call 1st fragmnet

//            toolbar_title_left.setVisibility(View.VISIBLE);
//            toolbar_title.setVisibility(View.GONE);
//            toolbar_title_left.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/timesnewroman.ttf"));
//            toolbar_title_left.setText(schoolName);

            if (fragment instanceof StaffHomeFragment) {
            } else {
                homeFragment = new StaffHomeFragment();
                fragment = homeFragment;
                transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.frame_layout, homeFragment).commit();
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        /*if (isFgYearBookOpen){
            sttafYearbookFragment.onBackPressed();
        }
        else*/ if (PlayerManager.getInstance().onBackPressed()) {
            return;
        } else {
            finishDialog();
        }
    }

    public void finishDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(StaffHomeActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alert = dialogBuilder.create();
        alert.setCancelable(true);
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
        tvMessage.setText("Are you sure you want to exit?");

        TextView tvYes = dialogView.findViewById(R.id.tvYes);
        TextView tvNo = dialogView.findViewById(R.id.tvNo);

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //isFgYearBookOpen = true;
                alert.dismiss();
            }
        });
        alert.show();
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title_left = toolbar.findViewById(R.id.toolbar_title_left);

        //toolbar_title.setText(studentName);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setContentInsetsAbsolute(0, 0);

    }

    public void hideToolbar(){
        getSupportActionBar().hide();
        mainToolbar.setVisibility(View.GONE);
        CoordinatorLayout.LayoutParams param = (CoordinatorLayout.LayoutParams) findViewById(R.id.frame_layout).getLayoutParams();
        param.setBehavior(null);
        int paddingDp = 25;
        float density = getResources().getDisplayMetrics().density;
        int paddingPixel = (int)(paddingDp * density);
        findViewById(R.id.frame_layout).setPadding(0,paddingPixel,0,0);
    }
    public void showToolbar(){
        getSupportActionBar().show();
        mainToolbar.setVisibility(View.VISIBLE);
//        CoordinatorLayout.LayoutParams param = (CoordinatorLayout.LayoutParams) findViewById(R.id.frame_layout).getLayoutParams();
//        param.setBehavior(AppBarLayout.ScrollingViewBehavior());
        CoordinatorLayout.LayoutParams params =
                (CoordinatorLayout.LayoutParams) findViewById(R.id.frame_layout).getLayoutParams();
        params.setBehavior(new AppBarLayout.ScrollingViewBehavior());
        findViewById(R.id.frame_layout).requestLayout();
        findViewById(R.id.frame_layout).setPadding(0, 0,0,0);
    }

    public void openDrawer(){
        drawerLayout.openDrawer(GravityCompat.START);
    }
    public void closeDrawer(){
        drawerLayout.closeDrawers();
    }
/*
    @Override
    public void invokeDefaultOnBackPressed() {
        if (YearBookFG.mReactInstanceManager != null) {
            YearBookFG.mReactInstanceManager.onBackPressed();
        } else {
            getActivity().onBackPressed();
        }
    }*/
}

import React, { useEffect, useState } from 'react'
import {
  View,
  FlatList,
  Image,
  Platform,
  Text,
  ActivityIndicator
} from 'react-native'
// import { useDispatch } from 'react-redux'
import { menuIcon } from '../assets'
import YearbookList from './components/YearbookList'
import { openDrawer } from '../Native'
import { colors } from '../utils'
import { HeaderBackButton } from 'react-navigation-stack'
import AppHeader from '../components/AppHeader'

function Yearbooks (props) {
  //   const dispatch = useDispatch()

  const [state] = useState({
    data: props.navigation.getParam('Yearbookslist') || []
  })

  useEffect(() => {
    // let backHandler = BackHandler.addEventListener("hardwareBackPress", _backPress);
    // let focusListener = props.navigation.addListener("didFocus", () => backHandler = BackHandler.addEventListener("hardwareBackPress", _backPress));
    // let blurListener = props.navigation.addListener("didBlur", () => backHandler.remove());

    // return (() => {
    //     focusListener.remove();
    //     blurListener.remove();
    // })
  }, [])

  // const _backPress = () => backDialog();

  const renderYearbook = ({ item }) => {
    return <YearbookList data={item} />
  }

  const renderEmpty = () => {
    const { data } = state
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        {
          data ? (
            <>
              <Text style={{ textAlign: 'center', color: 'grey' }}>
                Looks like the yearbooks is not ready yet, please notify your school.
              </Text>
            </>
          )
            : <ActivityIndicator size='large' />
        }
      </View>
    )
  }

  return (
    <View style={{ flex: 1, backgroundColor: colors.app_background }}>
      {Platform.OS === 'android' && <AppHeader />}
      <FlatList
        data={state.data}
        renderItem={renderYearbook}
        contentContainerStyle={(state.data && state.data.length <= 0) && { flex: 1 }}
        keyExtractor={(item, index) => String(index)}
        ListEmptyComponent={renderEmpty}
      />
    </View>
  )
};

export default Yearbooks

Yearbooks.navigationOptions = () => {
  if (Platform.OS === 'android') {
    return {
      header: null
    }
  }

  return {
    headerTitle: 'Yearbook',
    headerLeft: (
      <HeaderBackButton
        onPress={() => { openDrawer() }}
        backImage={(
          <Image
            source={menuIcon}
            style={{ height: 20, width: 20, marginLeft: 15 }}
            resizeMode='contain'
          />
        )}
      />
    )
  }
}

import * as action from "./actions";
import * as colors from "./colors";
import * as global from "./const";

export {
    action,
    colors,
    global
}
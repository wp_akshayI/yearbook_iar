package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.app.yearbook.databinding.ActivityUpdateProfilePhotoBinding;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.BitmapUtils;
import com.app.yearbook.utils.Constants;
import com.fenchtose.nocropper.BitmapResult;
import com.fenchtose.nocropper.CropState;

import java.io.File;
import java.io.IOException;

import gun0912.tedbottompicker.TedBottomPicker;

public class UpdateProfilePhotoActivity extends AppCompatActivity {

    private static final String TAG = "UpdateProfilePhotoActiv";
    ActivityUpdateProfilePhotoBinding binding;
    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_update_profile_photo);
        setToolbar();
//        binding.imgPhoto.setAspectRatio(1, 1);
        binding.btnPickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPicker();
            }
        });

       /* binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTaskRunner runner = new AsyncTaskRunner();
                runner.execute();
            }
        });*/

        /*progressDialog = ProgressDialog.show(this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);
        progressDialog.dismiss();*/
        openPicker();
    }

    /*private class AsyncTaskRunner extends AsyncTask<String, String, Bitmap> {

        Bitmap b = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(UpdateProfilePhotoActivity.this, "", "", true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.progress_view);
            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
            Circle bounce = new Circle();
            bounce.setColor(Color.BLACK);
            progressBar.setIndeterminateDrawable(bounce);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap == null) {
                Toast.makeText(UpdateProfilePhotoActivity.this, "Please Select Image First", Toast.LENGTH_SHORT).show();
                return;
            }
            try {

                File f = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "crop_test.jpg");
                BitmapUtils.writeBitmapToFile(bitmap, f, 100);

                Log.d("TTT", "onOptionsItemSelected: " + Uri.fromFile(f));
                Intent i = new Intent(UpdateProfilePhotoActivity.this, ImageFilterActivity.class);
                i.putExtra(Constants.FROM, Constants.UPDATE_PROFILE);
                i.putExtra("selectUri", Uri.fromFile(f).toString());
                startActivityForResult(i, 111);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            b = binding.imgPhoto.getCroppedImage();
            return b;

        }

    }*/





    /*@Override
    protected void onPreExecute() {
        progressDialog = ProgressDialog.show(UpdateProfilePhotoActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);
    }*/


    @Override
    protected void onStart() {
        super.onStart();

    }

    private void openPicker() {
        TedBottomPicker tedBottomPicker = new TedBottomPicker.Builder(this)
                .setPeekHeight(800)
                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        next.setVisible(true);
                        loadNewImage(uri.getPath(), uri);
                    }
                })
                .create();

        tedBottomPicker.show(getSupportFragmentManager());
    }

    private void setToolbar() {
        setSupportActionBar(binding.appbar.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        binding.appbar.toolbarTitle.setText("Update Photo");
    }


    private Bitmap mBitmap;
    private Bitmap originalBitmap;

    private void loadNewImage(String filePath, Uri selectedImage) {

        mBitmap = BitmapFactory.decodeFile(filePath);
        originalBitmap = mBitmap;
        Log.d("TTT", "loadNewImage: " + mBitmap);
        int maxP = Math.max(mBitmap.getWidth(), mBitmap.getHeight());
        float scale1280 = (float) maxP / 1280;

        Log.i("TTT", "scaled: " + scale1280 + " - " + (1 / scale1280));
        if (binding.imgSelected.getWidth() != 0) {
            binding.imgSelected.setMaxZoom(binding.imgSelected.getWidth() * 2 / 1280f);
        } else {

            ViewTreeObserver vto = binding.imgSelected.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    binding.imgSelected.getViewTreeObserver().removeOnPreDrawListener(this);
                    binding.imgSelected.setMaxZoom(binding.imgSelected.getWidth() * 2 / 1280f);
                    return true;
                }
            });
        }

        mBitmap = Bitmap.createScaledBitmap(mBitmap, (int) (mBitmap.getWidth() / scale1280),
                (int) (mBitmap.getHeight() / scale1280), true);

        //-------------------------------------

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(selectedImage.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        Log.d("TTT", "Orientation: " + orientation);

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 90);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 180);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 270);
        }

        //-------------------------------------

        Log.d("TTT", "loadNewImage: bitmap w/h: " + mBitmap.getHeight() / mBitmap.getWidth());
        binding.imgSelected.setImageBitmap(mBitmap);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        Log.d("TTT", "loadNewImage: density: " + metrics.densityDpi);
        int minZoom = Math.min(mBitmap.getWidth(), mBitmap.getHeight());
        int maxZoom = Math.max(mBitmap.getWidth(), mBitmap.getHeight());

        float ratio = (float) mBitmap.getHeight() / (float) mBitmap.getWidth();
        Log.d("TTT", "loadNewImage: diff: " + (mBitmap.getHeight() - mBitmap.getWidth()) + " / " + ratio);
        if (mBitmap.getHeight() > mBitmap.getWidth()) {
            if (ratio > 1.5) {
                if (metrics.densityDpi > 320) {
                    binding.imgSelected.setMinZoom(1.1f);
                } else {
                    binding.imgSelected.setMinZoom(0.75f);
                }
            } else {
                binding.imgSelected.setMinZoom(0f);
            }
        } else {
            binding.imgSelected.setMinZoom(0f);
        }
        Log.d("TTT", "loadNewImage: width height : " + mBitmap.getWidth() + " / " + mBitmap.getHeight());
        Log.d("TTT", "loadNewImage: " + binding.imgSelected.getMinZoom() + " / " + binding.imgSelected.getMaxZoom() + " / " + maxZoom + " / " + minZoom);

    }

    private MenuItem next;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selectimage, menu);
        next = menu.findItem(R.id.next);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.next:
                BitmapResult result = binding.imgSelected.getCroppedBitmap();
                if (result.getState() == CropState.FAILURE_GESTURE_IN_PROCESS) {
                    Toast.makeText(this, "unable to crop. Gesture in progress", Toast.LENGTH_SHORT).show();
                } else {
                    Bitmap bitmap = result.getBitmap();
                    Log.d("TTT", "loadNewImage:bitmap height width: " + bitmap.getWidth() + " / " + bitmap.getHeight());
//                        if (bitmap != null) {
//                            Log.d("Cropper", "crop1 bitmap: " + bitmap.getWidth() + ", " + bitmap.getHeight());
//                            try {
//                                BitmapUtils.writeBitmapToFile(bitmap, new File(Environment.getExternalStorageDirectory() + "/crop_test.jpg"), 90);
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        }

                    try {
                        File f = new File(Environment.getExternalStorageDirectory()
                                + File.separator + "crop_test.jpg");
                        BitmapUtils.writeBitmapToFile(bitmap, f, 100);

                        Log.d("TTT", "onOptionsItemSelected: " + Uri.fromFile(f));
                        Intent i = new Intent(this, ImageFilterActivity.class);
                        i.putExtra(Constants.FROM, Constants.UPDATE_PROFILE);
                        i.putExtra("selectUri", Uri.fromFile(f).toString());
                        startActivityForResult(i, 111);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //Uri getUri = AllMethods.getImageUri(getActivity(), bitmap);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111 && resultCode == RESULT_OK) {
            setResult(RESULT_OK, data);
            finish();
        }
    }
}

import React, { useState } from "react"
import { useDispatch } from "react-redux"
import { colors, action } from "../utils";
import Fab from "@material-ui/core/Fab";
import { makeStyles } from "@material-ui/core/styles";
import InputText from "../components/InputText";
import CustomizeTextOverlay from "../components/CustomizeTextOverlay";
import Actions from "../actions";
import DragAndDrop from "../components/DragAndDrop";


export default function ImageTextOverlaySetting({ data, updateComponents, obj }) {
    const dispatch = useDispatch();

    const classes = useStyles();
    const [state, setState] = useState({
        isCostomize: false
    })

    const _handleTitle = (e) => {
        updateComponents({ component_title: e.target.value }, data.component_order_by)
    }

    const _drop = (files) => {
        if (!files || files.length === 0) return;
        let file = files[0];
        uploadImage(file);
    }

    const uploadImage = async (file) => {
        try {
            let res = await dispatch(Actions.uploadImage(file));
            if (res.Result) {
                updateComponents({ component_url: res.Data.image_url }, data.component_order_by)
            } else {
                dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: "warning" });
            }
        } catch (error) {
            console.log(error)
        }
    }

    const _submitEditModal = (attribute) => {
        if (!attribute) return;
        updateComponents({ ...attribute }, data.component_order_by);
        setState({ ...state, isCostomize: false })
    }

    return (
        <div style={{ padding: 10, marginTop: 5 }}>
            <InputText
                title="Overlay Text"
                defaultValue={data.component_title}
                onChange={_handleTitle}
                labelWidth={90}
            />
            <div style={{ height: 10 }} />
            <DragAndDrop handleDrop={_drop}>
                <div style={{ height: 100, borderColor: "#c9c9c9", borderWidth: 1, borderStyle: "dashed", borderRadius: 4, display: "flex", justifyContent: "center", alignItems: "center" }}>
                    <div style={{ display: "grid" }}>
                        <span style={{ color: "#c9c9c9", fontSize: 14, textAlign: "center" }}>
                            Upload Image
                        </span>
                        <span style={{ color: "#c9c9c9", fontSize: 12, textAlign: "center", paddingTop: 5 }}>
                            Click Or Drag & Drop
                        </span>
                    </div>
                </div>
            </DragAndDrop>
            <div style={{ height: 10 }} />
            <Fab
                variant="extended"
                size="medium"
                color="primary"
                classes={{ label: classes.buttonLable, sizeMedium: classes.buttonSize }}
                onClick={() => setState({ ...state, isCostomize: !state.isCostomize })}
            >
                Customize
            </Fab>
            <CustomizeTextOverlay
                isVisible={state.isCostomize}
                onBackdropPress={() => setState({ ...state, isCostomize: false })}
                data={obj.state.settingsComponent}
                _done={_submitEditModal}
            />
        </div>
    )
}

const useStyles = makeStyles(() => ({
    modal: {
        backgroundColor: colors.black,
        height: 350,
    },
    modalContainer: {
        display: "flex",
        height: 280
    },
    modal_image: {
        width: "100%",
        height: "100%",
        objectFit: "cover"
    },
    modalLeft: {
        width: "30%",
        justifyContent: "center",
        display: "flex",
        height: "fit-content"
    },

    buttonLable: {
        textTransform: "capitalize",
    },
    buttonSize: {
        width: "100% !important"
    },

}));
package com.app.yearbook.model.profile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignatureList implements Parcelable {

    @SerializedName("signature_id")
    @Expose
    private String signatureId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("receiver_id")
    @Expose
    private String receiverId;
    @SerializedName("signature_message")
    @Expose
    private String signatureMessage;
    @SerializedName("signature_created")
    @Expose
    private String signatureCreated;
    @SerializedName("user_firstname")
    @Expose
    private String userFirstname;
    @SerializedName("user_lastname")
    @Expose
    private String userLastname;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("signature_font_type")
    @Expose
    private String signatureFontType;

    @SerializedName("sender_name")
    @Expose
    private String senderName;

    @SerializedName("signature_greeting")
    @Expose
    private String signatureGreeting;

    public SignatureList() {
    }

    protected SignatureList(Parcel in) {
        signatureId = in.readString();
        userId = in.readString();
        receiverId = in.readString();
        signatureMessage = in.readString();
        signatureCreated = in.readString();
        userFirstname = in.readString();
        userLastname = in.readString();
        userImage = in.readString();
        signatureFontType = in.readString();
        senderName = in.readString();
        signatureGreeting = in.readString();
    }

    public static final Creator<SignatureList> CREATOR = new Creator<SignatureList>() {
        @Override
        public SignatureList createFromParcel(Parcel in) {
            return new SignatureList(in);
        }

        @Override
        public SignatureList[] newArray(int size) {
            return new SignatureList[size];
        }
    };

    public String getSignatureId() {
        return signatureId;
    }

    public void setSignatureId(String signatureId) {
        this.signatureId = signatureId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getSignatureMessage() {
        return signatureMessage;
    }

    public void setSignatureMessage(String signatureMessage) {
        this.signatureMessage = signatureMessage;
    }

    public String getSignatureCreated() {
        return signatureCreated;
    }

    public void setSignatureCreated(String signatureCreated) {
        this.signatureCreated = signatureCreated;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getSignatureFontType() {
        return signatureFontType;
    }

    public void setSignatureFontType(String signatureFontType) {
        this.signatureFontType = signatureFontType;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSignatureGreeting() {
        return signatureGreeting;
    }

    public void setSignatureGreeting(String signatureGreeting) {
        this.signatureGreeting = signatureGreeting;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(signatureId);
        dest.writeString(userId);
        dest.writeString(receiverId);
        dest.writeString(signatureMessage);
        dest.writeString(signatureCreated);
        dest.writeString(userFirstname);
        dest.writeString(userLastname);
        dest.writeString(userImage);
        dest.writeString(signatureFontType);
        dest.writeString(senderName);
        dest.writeString(signatureGreeting);
    }
}

package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.yearbook.model.employee.GetImageResponse;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.utils.AllMethods;
import com.github.ybq.android.spinkit.style.Circle;
import com.naver.android.helloyako.imagecrop.view.ImageCropView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentPhotoCropActivity extends AppCompatActivity {

    String path = "";
    ImageCropView imageCropView;
    private Toolbar toolbar;
    private ProgressDialog progressDialog;
    private TextView toolbar_title;
    private static final String TAG = "StudentPhotoCropActivit";
    String userId, empId, oldPath, flag, oldImage;
    private AllMethods utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_photo_crop);
        setView();
    }

    public void setView() {
        setToolbar();

        utils = new AllMethods();

        imageCropView = findViewById(R.id.imgPhoto);
        imageCropView.setAspectRatio(1, 1);

        if (getIntent().getExtras() != null) {
            path = getIntent().getStringExtra("Path");
            imageCropView.setImageFilePath(path);
            userId = getIntent().getStringExtra("UserID");
            empId = getIntent().getStringExtra("EmpId");
            flag = getIntent().getStringExtra("flag");
            oldImage = getIntent().getStringExtra("OldImageName");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selectimage, menu);
        MenuItem next = menu.findItem(R.id.next);
        next.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        //toolbar.setBackgroundColor(Color.TRANSPARENT);
        toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.next) {

            AsyncTaskRunner runner = new AsyncTaskRunner();
            runner.execute();

        }
        return super.onOptionsItemSelected(item);
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        File studentFile;

        @Override
        protected String doInBackground(String... params) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }

            if (!imageCropView.isChangingScale()) {
                Bitmap b = imageCropView.getCroppedImage();
                if (b != null) {
                    studentFile = bitmapConvertToFile(b);
                } else {
                    Toast.makeText(StudentPhotoCropActivity.this, "Fail to crop", Toast.LENGTH_SHORT).show();
                }
            }
            result = studentFile.getPath();
            Log.d(TAG, "onPostExecute: " + result);
            if (result != null && !result.equals("")) {

                if (flag.equalsIgnoreCase("add")) {
                    uploadImage(result);
                } else {
                    editImage(result);
                }

//                Intent i = new Intent();
//                i.putExtra("Path", result);
//                setResult(RESULT_OK, i);
//                finish();
            } else {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        }


        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(StudentPhotoCropActivity.this, "", "", true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.progress_view);
            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
            Circle bounce = new Circle();
            bounce.setColor(Color.BLACK);
            progressBar.setIndeterminateDrawable(bounce);
        }
    }

    public File bitmapConvertToFile(Bitmap bitmap) {
        FileOutputStream fileOutputStream = null;
        File bitmapFile = null;
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory("yearbook_student"), "");
            if (!file.exists()) {
                file.mkdir();
            }

            bitmapFile = new File(file, "IMG_" + (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime()) + ".jpg");
            fileOutputStream = new FileOutputStream(bitmapFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            MediaScannerConnection.scanFile(this, new String[]{bitmapFile.getAbsolutePath()}, null, new MediaScannerConnection.MediaScannerConnectionClient() {
                @Override
                public void onMediaScannerConnected() {

                }

                @Override
                public void onScanCompleted(String path, Uri uri) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // Toast.makeText(StudentPhotoCropActivity.this, "file saved", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (Exception e) {
                }
            }
        }

        return bitmapFile;
    }

    private void uploadImage(String result) {

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[1];
        // for (int i = 0; i < newUserImagePath.size(); i++) {
        File file = new File(result);
        try {
            File compressedImageFile = new Compressor(StudentPhotoCropActivity.this).compressToFile(file);
            Log.d(TAG, "uploadImage: " + compressedImageFile.getPath());
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), compressedImageFile);
            surveyImagesParts[0] = MultipartBody.Part.createFormData("images[]", compressedImageFile.getName(), surveyBody);
            Log.d(TAG, "showUriList: part: " + surveyImagesParts[0].toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        // }

        RequestBody requestBodyUID = RequestBody.create(MediaType.parse("text/plain*"), userId);
        RequestBody requestBodyEID = RequestBody.create(MediaType.parse("text/plain*"), empId);

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("user_id", requestBodyUID);
        map.put("employee_id", requestBodyEID);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetImageResponse> schoolList = retrofitClass.uploadImages(map, surveyImagesParts);
        schoolList.enqueue(new Callback<GetImageResponse>() {
            @Override
            public void onResponse(Call<GetImageResponse> call, Response<GetImageResponse> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Log.d(TAG, "onResponse: " + response.toString());
                if (response.body().getResult().equalsIgnoreCase("true")) {
                    // userImagePath.clear();
                    //userImagePath.addAll(response.body().getData());
//                    Log.d(TAG, "onResponse: currentPosition: " + currentPosition);
//                    userImagePath.clear();
//                    userImagePath.addAll(response.body().getData());

//                    if(StudentListActivity.userPostItems.get(position).getUserImages()!=null) {
//                        StudentListActivity.userPostItems.get(position).getUserImages().clear();
//                        StudentListActivity.userPostItems.get(position).getUserImages().addAll(userImagePath);
//
//                        if(StudentListActivity.recyclerViewStudent.getAdapter()!=null) {
//                            StudentListActivity.recyclerViewStudent.getAdapter().notifyDataSetChanged();
//                        }
//                    }

//                    if (binding.recyclerViewImage.getAdapter() != null) {
//                        binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
//                    }

                    //utils.setAlert(StudentPhotoCropActivity.this, "", response.body().getResponseMsg());

                    Toast.makeText(StudentPhotoCropActivity.this, "" + response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    Intent i = new Intent();
                    i.putStringArrayListExtra("Images", (ArrayList<String>) response.body().getData());
                    setResult(RESULT_OK, i);
                    finish();

                    // newUserImagePath.clear();
                } else {
                    utils.setAlert(StudentPhotoCropActivity.this, "", response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<GetImageResponse> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                utils.setAlert(StudentPhotoCropActivity.this, "", t.getMessage());
            }
        });
    }


    private void editImage(String path) {

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[1];
        File file = new File(path);
        try {
            File compressedImageFile = new Compressor(StudentPhotoCropActivity.this).compressToFile(file);
            Log.d(TAG, "uploadImage: " + compressedImageFile.getPath());
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), compressedImageFile);
            surveyImagesParts[0] = MultipartBody.Part.createFormData("images", compressedImageFile.getName(), surveyBody);
            Log.d(TAG, "showUriList: part: " + surveyImagesParts[0].toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        final File userImage = new File(oldImage);
        RequestBody requestBodyUID = RequestBody.create(MediaType.parse("text/plain*"), userId);
        final RequestBody requestBodyEID = RequestBody.create(MediaType.parse("text/plain*"), empId);
        RequestBody requestBodyImageName = RequestBody.create(MediaType.parse("text/plain*"), userImage.getName());

        Log.d(TAG, "editImage: " + userImage.getName());
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("user_id", requestBodyUID);
        map.put("employee_id", requestBodyEID);
        map.put("old_image_name", requestBodyImageName);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetImageResponse> schoolList = retrofitClass.editImages(map, surveyImagesParts);
        schoolList.enqueue(new Callback<GetImageResponse>() {
            @Override
            public void onResponse(Call<GetImageResponse> call, Response<GetImageResponse> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Log.d(TAG, "onResponse: " + response.body().getResult());
                if (response.body().getResult().equalsIgnoreCase("true")) {
                    Toast.makeText(StudentPhotoCropActivity.this, "" + response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    Intent i = new Intent();
                    i.putStringArrayListExtra("Images", (ArrayList<String>) response.body().getData());
                    setResult(RESULT_OK, i);
                    finish();

                    //    utils.setAlert(StudentPhotoCropActivity.this, "", response.body().getResponseMsg());
                    //  Log.d(TAG, "onResponse: currentPosition: " + currentPosition);

//                    userImagePath.clear();
//                    userImagePath.addAll(response.body().getData());

//                    if (StudentListActivity.userPostItems.get(position).getUserImages() != null) {
//                        StudentListActivity.userPostItems.get(position).getUserImages().clear();
//                        StudentListActivity.userPostItems.get(position).getUserImages().addAll(userImagePath);
//
//                        if (StudentListActivity.recyclerViewStudent.getAdapter() != null) {
//                            StudentListActivity.recyclerViewStudent.getAdapter().notifyDataSetChanged();
//                        }
//                    }
//
//                    if (binding.recyclerViewImage.getAdapter() != null) {
//                        binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
//                    }
                } else {
                    utils.setAlert(StudentPhotoCropActivity.this, "", response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<GetImageResponse> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Log.d(TAG, "onFailure: " + t.getMessage());
                utils.setAlert(StudentPhotoCropActivity.this, "", t.getMessage());
            }
        });
    }

}

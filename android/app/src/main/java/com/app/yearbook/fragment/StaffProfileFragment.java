package com.app.yearbook.fragment;


import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.databinding.DataBindingUtil;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.yearbook.AccessCode;
import com.app.yearbook.EmployeeHomeActivity;
import com.app.yearbook.StaffHomeActivity;
import com.app.yearbook.StudentHomeActivity;
import com.app.yearbook.TermsConditionActivity;
import com.app.yearbook.TermsConditionStaffActivity;
import com.app.yearbook.UserProcessWithWeb;
import com.google.android.material.textfield.TextInputLayout;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.FragmentStatePagerAdapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.UpdateProfilePhotoActivity;
import com.app.yearbook.adapter.TabAdapter;
import com.app.yearbook.databinding.FragmentStaffProfileBinding;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.StaffStudent;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.staff.EditProfileResponse;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.BitmapUtils;
import com.app.yearbook.utils.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class StaffProfileFragment extends Fragment implements
        View.OnClickListener {


    private static final int REQUEST_PERMISSION = 101;
    private static final int EDIT_PROFILE = 1000;
    private static final String TAG = "StaffProfileFragment";
    FragmentStaffProfileBinding binding;
    private LoginUser loginUser;
    StaffStudent user;
    private EditText edtOldPassword, edtNewPassword, edtConfirmPassword;
    private TextInputLayout input_layout_newpassword, input_layout_confirmpassword, input_layout_oldpassword;
    private String userPwd, userId;

    public StaffProfileFragment() {
        // Required empty public constructor
    }

    private TabAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_staff_profile, container, false);
        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        allMethods = new AllMethods();
        loginUser = new LoginUser(getActivity());

        if (loginUser.getUserData() != null) {
            userId = loginUser.getUserData().getStaffId();
            userPwd = loginUser.getUserData().getStaffPassword();
        }


        notifyUserData(0);
        binding.imgAddPhoto.setOnClickListener(this);
//        binding.imgUserImage.setOnClickListener(this);
        /*binding.viewPager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.tabLayout.getTabAt(0).setIcon(R.drawable.bookmark_tab_ic);
        binding.tabLayout.getTabAt(1).setIcon(R.drawable.notification_tab_ic);
        binding.tabLayout.getTabAt(2).setIcon(R.drawable.yearbook_tab_ic);*/

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.getString("redirectOn").contentEquals("notification")) {
                binding.viewPager.setCurrentItem(1);
            }
        }

        getUserProfileData();

        view.findViewById(R.id.view_access_code).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AccessCode.class);
                startActivity(i);
            }
        });
    }

    private void getUserProfileData() {
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<JsonObject> userPost = retrofitClass.get_staff_profile(userId);
        userPost.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                JsonObject res = response.body();
                try {
                    JSONObject jsonObject = new JSONObject(res.toString());
                    if (jsonObject.getString("ResponseCode").equals(String.valueOf(1))) {

//                        String userDetail = jsonObject.getJSONObject("user_profile").getString("user_detail");
                        String userDetail = jsonObject.getString("staff_profile");

                        StaffStudent dd = new Gson().fromJson(userDetail, StaffStudent.class);

                        if (dd != null) {
                            loginUser.setUserData(dd);
                            Log.d("TTT", "SP: " + loginUser.getUserData().getSchoolId());
                            notifyUserData(1);
                        } else {
                            allMethods.setAlert(getContext(), getResources().getString(R.string.app_name), jsonObject.getString("ResponseMsg"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getContext(), getResources().getString(R.string.app_name), t.getMessage());
            }
        });
    }

    private void notifyUserData(int loadinAt) {
        user = loginUser.getUserData();
        String userName = user.getStaffFirstname() + " " + user.getStaffLastname();
        binding.txtName.setText(userName);
        if (loadinAt == 0)
            updateImage(user.getStaffImage());
        else{
            Glide.with(getActivity())
                    .load(user.getStaffImage())
                    .asBitmap()//.diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .placeholder(R.mipmap.profile_placeholder)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(binding.imgUserImage);
        }

        if (loginUser.getUserData().getIsAccessCodeVerify() != null && loginUser.getUserData().getIsAccessCodeVerify().equalsIgnoreCase("Yes")) {
            binding.viewAccessCode.setVisibility(View.GONE);
            binding.viewProfile.setVisibility(View.VISIBLE);
        } else {
            binding.viewAccessCode.setVisibility(View.VISIBLE);
            binding.viewProfile.setVisibility(View.GONE);
        }

        binding.viewPager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.tabLayout.getTabAt(0).setIcon(R.drawable.bookmark_tab_ic);
        binding.tabLayout.getTabAt(1).setIcon(R.drawable.notification_tab_ic);
        binding.tabLayout.getTabAt(2).setIcon(R.drawable.yearbook_tab_ic);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.getString("redirectOn").contentEquals("notification")) {
                binding.viewPager.setCurrentItem(1);
            }
        }
    }

    private void updateImage(String staffImage) {
//        binding.progress.setVisibility(View.VISIBLE);
        Glide.with(getActivity())
                .load(staffImage)
                .asBitmap()//.diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
//                        binding.progress.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        binding.progress.setVisibility(View.GONE);
                        return false;
                    }
                })
                .centerCrop()
                .placeholder(R.mipmap.profile_placeholder)
                .error(R.mipmap.profile_placeholder)
                .into(binding.imgUserImage);
    }

    @Override
    public void onClick(View v) {
        if (v == binding.imgAddPhoto || v == binding.imgUserImage) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Log.d("TTT", "Permission...");
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
            } else {
                Log.d("TTT", "granted Permission...");
                setDialog();
            }
        }
    }

    public void setDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Available actions");
        builder.setCancelable(false);
        builder.setItems(new CharSequence[]{"Update current photo", "Remove current photo", "Cancel"},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                updateProfilePhoto();
                                break;
                            case 1:
                                removePhoto();
                                break;
                            case 2:
                                dialog.dismiss();
                                break;
                        }
                    }
                });
        builder.create().show();
    }

    private void updateProfilePhoto() {
        startActivityForResult(new Intent(getActivity(), UpdateProfilePhotoActivity.class), 111);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {
            if (resultCode == RESULT_OK) {
                if (data.hasExtra(Constants.IMAGE_URI)) {
                    Uri imageUri = Uri.parse(data.getExtras().getString(Constants.IMAGE_URI, ""));
                    editProfile(imageUri);
                } else {
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public ProgressDialog progressDialog;
    private AllMethods allMethods;

    public void removePhoto() {

        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<Login> userPost = retrofitClass.removeStaffImage(Integer.parseInt(user.getStaffId()));
        userPost.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equalsIgnoreCase("1")) {
                    //set profile photo
                    loginUser.setUserData(response.body().getData());
                    Glide.with(getActivity())
                            .load(R.mipmap.profile_placeholder)
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.profile_placeholder)
                            .error(R.mipmap.profile_placeholder)
                            .into(binding.imgUserImage);
                    allMethods.setAlert(getActivity(), "", "Profile photo remove successfully");

                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(getActivity());
                    loginSP.clearData();

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                } else {
                    allMethods.setAlert(getActivity(), "", response.body().getResponseMsg() + "");
                }

            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });

    }

    private MultipartBody.Part part;
    private File fileImage;
    private RequestBody staffID, rbFile;

    public void editProfile(Uri getUri) {
        binding.imgUserImage.setImageBitmap(null);
        Glide.with(getActivity())
                .load(getUri)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .placeholder(R.mipmap.profile_placeholder)
                .error(R.mipmap.profile_placeholder)
                .into(binding.imgUserImage);

        fileImage = new File(BitmapUtils.getFilePathFromUri(getActivity(), getUri));

        RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
        part = MultipartBody.Part.createFormData("staff_image", fileImage.getName(), filePost);
        rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());

        staffID = RequestBody.create(MediaType.parse("text/plain"), user.getStaffId());

        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        final ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<EditProfileResponse> userPost = retrofitClass.editStaffProfile(staffID, part/*,rbFile*/);
        userPost.enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                progressDialog.dismiss();
                EditProfileResponse editProfileResponse = response.body();
                if (editProfileResponse != null) {
                    if (editProfileResponse.getResponseCode().equals("1")) {
                        user.setStaffImage(editProfileResponse.getStaffProfile().getStaffImage());
                        loginUser.setUserData(user);
                        updateImage(user.getStaffImage());
                    } else {
                        Toast.makeText(getActivity(), editProfileResponse.getResponseMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                Log.d("TTT", "Permission... onRequestPermissionsResult");
                setDialog();
            } else {
                Log.d("TTT", "denyyyy... onRequestPermissionsResult");
                Toast.makeText(getActivity(), "Deny", Toast.LENGTH_SHORT).show();
                //code for deny
            }
        }
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    if (loginUser.getUserData().getIsAccessCodeVerify() != null && loginUser.getUserData().getIsAccessCodeVerify().equalsIgnoreCase("Yes")) {
                        return new StaffBookmarkFragment();
                    } else {
                        return new AccessCodeDisableDetail(1);
                    }
                case 1:
                    if (loginUser.getUserData().getIsAccessCodeVerify() != null && loginUser.getUserData().getIsAccessCodeVerify().equalsIgnoreCase("Yes")) {
                        return new StudentNotificationFragment();
                    } else {
                        return new AccessCodeDisableDetail(2);
                    }
                case 2:
                    return new StudentSignatureFragment();
                default:
                    return new StudentSignatureFragment();

            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.logout, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.setting) {
            View menuItemView = getActivity().findViewById(R.id.setting);
            showMenu(menuItemView);

        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validation() {
        boolean result = false;

        //old pwd
        Log.d("TTT", "Old Pwd: " + userPwd + " / " + edtOldPassword.getText().toString());

        if (edtOldPassword.getText().toString().trim().equals("")) {
            input_layout_oldpassword.setErrorEnabled(true);
            input_layout_oldpassword.setError("Your old password is empty");
            result = false;
        } else if (!userPwd.toString().equals(edtOldPassword.getText().toString())) {
            Log.d("TTT", "wrongg pwd");
            input_layout_oldpassword.setErrorEnabled(true);
            input_layout_oldpassword.setError("Your old password is wrong");
            result = false;
        } else {
            input_layout_oldpassword.setErrorEnabled(false);
            input_layout_oldpassword.setError("");
            result = true;
        }

        //new pwd
        if (edtNewPassword.getText().toString().trim().equals("")) {
            input_layout_newpassword.setErrorEnabled(true);
            input_layout_newpassword.setError("Your new password is empty");
            result = false;
        } else {
            input_layout_newpassword.setErrorEnabled(false);
            input_layout_newpassword.setError("");
            result = true;
        }

        //confirm new pwd
        if (edtConfirmPassword.getText().toString().trim().equals("")) {
            input_layout_confirmpassword.setErrorEnabled(true);
            input_layout_confirmpassword.setError("Your confirm password is empty");
            result = false;
        } else if (!edtConfirmPassword.getText().toString().equals(edtNewPassword.getText().toString())) {
            input_layout_confirmpassword.setErrorEnabled(true);
            input_layout_confirmpassword.setError("Confirm password and new password is must be same");
            result = false;
        } else {
            input_layout_confirmpassword.setErrorEnabled(false);
            input_layout_confirmpassword.setError("");
            result = true;

        }

        Log.d("TTT", "Result: " + result);
        return result;
    }

    public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(getActivity(), v);
        popup.getMenuInflater().inflate(R.menu.profileseeting, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
//                if(item.getTitle().equals("Manage Ad"))
//                {
//                    Intent i=new Intent(getActivity(), AdvertiseActivity.class);
//                    startActivity(i);
//                }
                if (item.getTitle().equals("Manage Account")) {
                    Intent intent = new Intent(getActivity(), UserProcessWithWeb.class);
                    intent.putExtra("action", Constants.USER_WEB_PROFILE);
                    startActivity(intent);
                } else if (item.getTitle().equals("Change password")) {
                    androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.custom_alertbox, null);
                    dialogBuilder.setView(dialogView);
                    final androidx.appcompat.app.AlertDialog alert = dialogBuilder.create();
                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    alert.setCancelable(false);

                    edtNewPassword = dialogView.findViewById(R.id.edtNewPassword);
                    edtNewPassword.setOnFocusChangeListener(newPassword);

                    edtOldPassword = dialogView.findViewById(R.id.edtOldPassword);
                    edtOldPassword.setOnFocusChangeListener(oldPassword);

                    edtConfirmPassword = dialogView.findViewById(R.id.edtConfirmPassword);
                    edtConfirmPassword.setOnFocusChangeListener(confirmPassword);

                    input_layout_oldpassword = dialogView.findViewById(R.id.input_layout_oldpassword);
                    input_layout_newpassword = dialogView.findViewById(R.id.input_layout_newpassword);
                    input_layout_confirmpassword = dialogView.findViewById(R.id.input_layout_confirmpassword);

                    Button btnCancel = dialogView.findViewById(R.id.btnCancel);
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    Button btnOk = dialogView.findViewById(R.id.btnOk);
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (validation()) {
                                if (!userPwd.toString().equals(edtOldPassword.getText().toString())) {
                                    Log.d("TTT", "wrongg pwd");
                                    input_layout_oldpassword.setErrorEnabled(true);
                                    input_layout_oldpassword.setError("Your old password is wrong");
                                } else {
                                    alert.dismiss();
                                    changePassword(edtNewPassword.getText().toString());
                                }
                            }
                        }
                    });
                    alert.show();
                } else if (item.getTitle().equals("Logout")) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setCancelable(false);
                    dialog.setTitle("");
                    dialog.setMessage("Are you sure you want to logout ?");
                    dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //Action for "Delete".
                            dialog.dismiss();

                            String token = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Token", "");
                            Log.d("TTT", "token: " + token);

                            if (!token.equals("")) {
                                progressDialog = ProgressDialog.show(getActivity(), "", "", true);
                                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                progressDialog.setContentView(R.layout.progress_view);
                                ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                                Circle bounce = new Circle();
                                bounce.setColor(Color.BLACK);
                                progressBar.setIndeterminateDrawable(bounce);

                                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                final Call<GiveReport> userPost = retrofitClass.logout(token, "staff");
                                userPost.enqueue(new Callback<GiveReport>() {
                                    @Override
                                    public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }

                                        LoginUser loginSP = new LoginUser(getActivity());
                                        loginSP.clearData();

                                        Intent i = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(i);
                                        getActivity().finish();
                                    }

                                    @Override
                                    public void onFailure(Call<GiveReport> call, Throwable t) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }
                                        allMethods.setAlert(getActivity(), "", t.getMessage() + "");
                                    }
                                });
                            } else {
                                LoginUser loginSP = new LoginUser(getActivity());
                                loginSP.clearData();

                                Intent i = new Intent(getActivity(), LoginActivity.class);
                                startActivity(i);
                                (getActivity()).finish();
                            }
                        }


                    });

                    dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    final AlertDialog alert = dialog.create();
                    alert.show();

                }
                return true;
            }
        });
        popup.show();

    }

    private View.OnFocusChangeListener newPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtNewPassword.getText().toString().equals("")) {
                    input_layout_newpassword.setErrorEnabled(true);
                    input_layout_newpassword.setError("Your new password is empty");
                } else {
                    input_layout_newpassword.setErrorEnabled(false);
                    input_layout_newpassword.setError("");
                }
            }
        }
    };

    private View.OnFocusChangeListener confirmPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtConfirmPassword.getText().toString().equals("")) {
                    input_layout_confirmpassword.setErrorEnabled(true);
                    input_layout_confirmpassword.setError("Your confirm password is empty");
                } else {
                    input_layout_confirmpassword.setErrorEnabled(false);
                    input_layout_confirmpassword.setError("");
                }
            }
        }
    };

    private View.OnFocusChangeListener oldPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtOldPassword.getText().toString().equals("")) {
                    input_layout_oldpassword.setErrorEnabled(true);
                    input_layout_oldpassword.setError("Your old password is empty");
                } else {
                    input_layout_oldpassword.setErrorEnabled(false);
                    input_layout_oldpassword.setError("");
                }
            }
        }
    };

    public void changePassword(String pwd) {
        //Progress bar
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> userPost = retrofitClass.changePassword(Integer.parseInt(userId), "staff", pwd);
        userPost.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", response.body().getResponseMsg() + "");
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });
    }
}

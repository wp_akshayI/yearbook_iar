import React, { useState, useEffect } from "react"
import { View, Text, StyleSheet, Image, FlatList, ActivityIndicator } from "react-native";
import { useDispatch } from "react-redux"
import { colors } from "../utils";
import Fab from "@material-ui/core/Fab";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "modal-enhanced-react-native-web";
import { image_placeholder } from "../assets";
import { uploadGalleryImage, removeTampfile } from "actions/Common";
import CloseIcon from '@material-ui/icons/Close';
import { IconButton } from '@material-ui/core';

export default function MediaUpload({ isVisible, handleClose, onSubmit, items }) {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [state, setState] = useState({
        items,
        isloading: false,
    })

    useEffect(() => {
        setState({ ...state, items })
    }, [items])

    const _selectionDone = async () => {
        onSubmit(state.items);
    }

    const handleClick = async (e) => {
        if (!e.target.files || e.target.files.length === 0) return;
        setState({ ...state, isloading: true });
        let res = await dispatch(uploadGalleryImage(e.target.files));
        if (res) {
            setState((preState) => {
                if (!preState.items) {
                    preState.items = res;
                } else {
                    preState.items = [...preState.items, ...res];
                }
                return { ...state, ...preState, isloading: false }
            })
        }
    }

    const removeImage = (request) => {
        try {
            let items = state.items;
            let url = items.splice(request, 1);
            setState({ ...state, items: items });
            dispatch(removeTampfile(1, url[0]));
        } catch (error) {
            console.log(error);
        }
    }

    const renderItem = ({ item, index }) => {
        const uri = item || image_placeholder;
        return (
            <View
                style={{ margin: 10, borderWidth: 3, borderColor: "transparent", flex: 1 }}
            >
                <Image
                    resizeMode="contain"
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: 175,
                        width: 175
                    }}
                    source={{ uri }}
                />
                <IconButton
                    style={{
                        position: "absolute",
                        right: 0,
                        backgroundColor: "rgba(0,0,0,0.5)"
                    }}
                    onClick={() => removeImage(index)}
                >
                    <CloseIcon style={{ color: "#fff" }} />
                </IconButton>

            </View>
        )
    }

    return (
        <Modal
            isVisible={isVisible}
            onBackdropPress={handleClose}
            style={{ alignItems: "center", margin: 30 }}
        >
            <View style={styles.modal}>

                <View style={styles.drawer}>
                    <Fab
                        variant="extended"
                        size="medium"
                        color="primary"
                        classes={{ label: classes.buttonLable, sizeMedium: classes.buttonSize }}
                        onClick={() => document.getElementById("CompImg").click()}
                    >
                        Upload
                    </Fab>
                    <View style={{ height: 10 }} />
                    <Fab
                        variant="extended"
                        size="medium"
                        color="primary"
                        classes={{ label: classes.buttonLable, sizeMedium: classes.buttonSize }}
                        onClick={_selectionDone}
                    >
                        Done
                    </Fab>
                    <View style={styles.divider} />
                    <View style={styles.divider} />
                    <Text style={styles.drawerBody}>
                        Any images that are added will be automatically sorted alphabetically.
                    </Text>
                    <View style={styles.divider} />
                    <Text style={styles.drawerBody}>
                        Tip: you can adjust names of images from the list on the right
                    </Text>
                </View>
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <FlatList
                        data={state.items}
                        extraData={state}
                        numColumns={5}
                        keyExtractor={(item) => item}
                        renderItem={renderItem}
                    />
                </View>
            </View>
            <input
                type="file"
                onChange={handleClick}
                id="CompImg"
                style={{ display: "none" }}
                onClick={(e) => e.currentTarget.value = null}
                multiple
            />
            {
                state.isloading && (
                    <View style={{ position: "absolute", backgroundColor: "rgba(0,0,0,0.5)", height: "100%", width: "100%", justifyContent: "center", alignItems: "center" }}>
                        <ActivityIndicator size="large" color={colors.black} />
                    </View>
                )
            }
        </Modal>
    )
}


const useStyles = makeStyles((theme) => ({

    buttonLable: {
        textTransform: "capitalize",
        color: colors.white,
    },
    buttonSize: {
        width: "100% !important"
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: "auto",
        // backgroundColor: "#B8BCBF",
        width: "75%"
    },
    gridList: {
        // width: 320,
        padding: 5,
        margin: "0px !important",
    },
    imgFullHeight: {
        width: "100%",
        position: "static",
        transform: "translateX(0%)",
        height: 100,
    }

}));

const styles = StyleSheet.create({
    modal: {
        backgroundColor: "#eeeeee",
        height: "100%",
        width: "100%",
        flexDirection: "row"
    },
    drawer: {
        backgroundColor: colors.Gray,
        width: 265,
        padding: 20,
        paddingTop: 40
    },
    drawerBody: {
        color: colors.white,
        fontFamily: "Calibri",
        fontSize: 16
    },
    divider: {
        padding: 15
    },
    title: {
        textAlign: "center",
        fontSize: 24,
        fontWeight: "bold",
        marginVertical: 20,
        fontFamily: "Calibri"
    }


})
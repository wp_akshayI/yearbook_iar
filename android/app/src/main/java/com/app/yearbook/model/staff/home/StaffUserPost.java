
package com.app.yearbook.model.staff.home;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.app.yearbook.model.studenthome.UserList;

public class StaffUserPost {

    @SerializedName("user_list")
    @Expose
    private List<UserList> userList = null;
    @SerializedName("post_list")
    @Expose
    private List<StaffPostList> postList = null;

    public List<UserList> getUserList() {
        return userList;
    }

    public void setUserList(List<UserList> userList) {
        this.userList = userList;
    }

    public List<StaffPostList> getPostList() {
        return postList;
    }

    public void setPostList(List<StaffPostList> postList) {
        this.postList = postList;
    }

}

import React, { useState } from 'react'
import { View, Text } from 'react-native'
import CloseIcon from '@material-ui/icons/Close'
import IconButton from '@material-ui/core/IconButton'
import InputText from '../../InputText'
import { colors } from '../../../utils'

export default function HomepageSettings ({ data, updateComponents }) {
  const [state, setState] = useState({
    uploadTitle: 'Choose an image to upload'
  })

  const _handleTitle = (e) => {
    updateComponents({ title: e.target.value })
  }

  const _handlesubTitle = (e) => {
    updateComponents({ subTitle: e.target.value })
  }

  const _removeImage = () => {
    window.URL.revokeObjectURL(data.source)
    updateComponents({ source: undefined, file: undefined })
    setState({
      ...state,
      uploadTitle: 'Choose an image to upload'
    })
  }

  const _handleBackground = (e) => {
    if (!e.target.files || e.target.files.length === 0) return
    const file = e.target.files[0]
    const uri = URL.createObjectURL(file)
    setState({
      ...state,
      uploadTitle: file.name
    })
    updateComponents({ source: uri, file })
  }

  return (
    <View style={{ margin: 10 }}>

      <InputText
        title='Title'
        defaultValue={data.title}
        onChange={_handleTitle}
      />

      <View style={{ padding: 10 }} />

      <InputText
        title='Sub Title'
        defaultValue={data.subTitle}
        onChange={_handlesubTitle}
      />

      <View style={{ padding: 10 }} />

      <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
        <Text style={{ color: colors.white }}>
                    Upload Image
        </Text>
        <View style={{ height: 50 }}>
          {
            data.source && data.source !== '' && (
              <IconButton
                key='close'
                aria-label='close'
                onClick={_removeImage}
                color='inherit'
                style={{ alignSelf: 'flex-end' }}
              >
                <CloseIcon size={10} style={{ color: '#fff' }} />
              </IconButton>
            )
          }
        </View>

      </View>

      <View
        style={{ backgroundColor: colors.white, height: 140, width: '100%', cursor: 'pointer', justifyContent: 'center', alignItems: 'center' }}
        onClick={() => document.getElementById('yearbookBG').click()}
      >
        <Text
          numberOfLines={3}
          style={{ textAlign: 'center', flexWrap: 'wrap', width: '100%' }}
        >
          {state.uploadTitle}
        </Text>
      </View>
      <input
        type='file'
        onChange={_handleBackground}
        id='yearbookBG'
        style={{ display: 'none' }}
        accept='image/*'
      />

    </View>
  )
}

package com.app.yearbook;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.model.staff.StaffRequest;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.utils.AllMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffRequestActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvSignIn;
    private Toolbar toolbar;
    private LinearLayout lvSchool, lvFirstname, lvLastname, lvEmail, lvDone, lvPhone, lvGetSchool;
    private EditText etSchoolname, etFirstName, etLastName, etEmail, etPhone;
    private Button btnSchool, btnNextFname, btnNextLname, btnEmail, btnPhone, btnDone;
    private AllMethods allMethods;
    private String schoolName, schoolId = "", school,flag="";
    public static final int SUB_ACTIVITY_CREATE_USER = 10;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_request);

        setView();
    }

    public void setView() {
        //set toolbar
        setToolbar();

        //all methods object
        allMethods = new AllMethods();

        //tv sign in
        tvSignIn = findViewById(R.id.tvSignIn);
        tvSignIn.setOnClickListener(this);

        //btn
        btnSchool = findViewById(R.id.btnSchool);
        btnSchool.setOnClickListener(this);

        btnNextFname = findViewById(R.id.btnNextFname);
        btnNextFname.setOnClickListener(this);

        btnNextLname = findViewById(R.id.btnNextLname);
        btnNextLname.setOnClickListener(this);

        btnEmail = findViewById(R.id.btnEmail);
        btnEmail.setOnClickListener(this);

        btnPhone = findViewById(R.id.btnPhone);
        btnPhone.setOnClickListener(this);

        btnDone = findViewById(R.id.btnDone);
        btnDone.setOnClickListener(this);

        //linear view
        lvSchool = findViewById(R.id.lvSchool);
        lvGetSchool = findViewById(R.id.lvGetSchool);
        lvGetSchool.setOnClickListener(this);
        lvFirstname = findViewById(R.id.lvFirstname);
        lvLastname = findViewById(R.id.lvLastname);
        lvEmail = findViewById(R.id.lvEmail);
        lvPhone = findViewById(R.id.lvPhone);
        lvDone = findViewById(R.id.lvDone);

        //edit text
        etSchoolname = findViewById(R.id.etSchoolname);
        etSchoolname.setOnClickListener(this);
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etEmail = findViewById(R.id.etEmail);
        etPhone = findViewById(R.id.etPhone);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            etPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher("US"));
        } else {
            etPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        }

        //if i get formatted number
//        public String getMobileNo() {
//        if (mobileNo != null) {
//            return mobileNo.replaceAll("[^\\d]", "");
//        } else {
//            return mobileNo;
//        }
//    }

    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Request Access Code");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(6f);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Log.d("TTT", "OnBack");
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setVisibility(LinearLayout hideView, LinearLayout showView) {
        hideView.setVisibility(View.GONE);
        showView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SUB_ACTIVITY_CREATE_USER && resultCode == Activity.RESULT_OK) {

            AllMethods.hideKeyboard(StaffRequestActivity.this,getCurrentFocus());
            Bundle extras = data.getExtras();
            if (extras != null) {
                schoolName = extras.getString("schoolName");
                school = schoolName;
                if (extras.getString("schoolId") != null) {
                    schoolId = extras.getString("schoolId");
                    school = schoolId;
                    flag="update";
                }
                else
                {
                    flag="add";
                }

                Log.d("TTT", "Intent data: " + schoolName + " / " + schoolId);
                etSchoolname.setText(schoolName);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.lvGetSchool) {
            Intent i = new Intent(getApplicationContext(), SearchSchoolActivity.class);
            startActivityForResult(i, SUB_ACTIVITY_CREATE_USER);
        } else if (v.getId() == R.id.tvSignIn) {
            finish();
        } else if (v.getId() == R.id.btnSchool) {
            if (etSchoolname.getText().toString().trim().equals("")) {
                allMethods.setAlert(StaffRequestActivity.this, getResources().getString(R.string.app_name), "Please enter school name");
            } else {
                //lvFirstname.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_to_left));
                setVisibility(lvSchool, lvEmail);
            }
        }
//        else if(v.getId()==R.id.btnNextFname)
//        {
//            if(etFirstName.getText().toString().trim().equals(""))
//            {
//                allMethods.setAlert(StaffRequestActivity.this,"Yearbook","Please enter first name");
//            }
//            else
//            {
//                setVisibility(lvFirstname,lvLastname);
//                //lvLastname.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_to_left));
//            }
//        }
//        else if(v.getId()==R.id.btnNextLname)
//        {
//            if(etLastName.getText().toString().trim().equals(""))
//            {
//                allMethods.setAlert(StaffRequestActivity.this,"Yearbook","Please enter last name");
//            }
//            else
//            {
//                setVisibility(lvLastname,lvEmail);
//                //lvEmail.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_to_left));
//            }
//        }
        else if (v.getId() == R.id.btnEmail) {
            if (etEmail.getText().toString().trim().equals("")) {
                allMethods.setAlert(StaffRequestActivity.this, getResources().getString(R.string.app_name), "Please enter email");
            } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                allMethods.setAlert(StaffRequestActivity.this, getResources().getString(R.string.app_name), "Please enter valid email address");
            } else {
                setVisibility(lvEmail, lvPhone);
                //lvPhone.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_to_left));
            }
        } else if (v.getId() == R.id.btnPhone) {
            if (etPhone.getText().toString().trim().equals("")) {
                allMethods.setAlert(StaffRequestActivity.this, getResources().getString(R.string.app_name), "Please enter Phone no");
            } else {
                //requestStaff();
                requestStaffNew();
                //lvDone.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_to_left));
            }
        } else if (v.getId() == R.id.btnDone) {
            finish();
        }
    }

    //old API
    public void requestStaff() {
        //Progress bar
        progressDialog = ProgressDialog.show(StaffRequestActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        String token = "";
        token = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("Token", "");
        Log.d("TTT", "token: " + token);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<StaffRequest> staffRequest = retrofitClass.staffRequestCode(Integer.parseInt(schoolId), etFirstName.getText().toString(), etLastName.getText().toString(), etEmail.getText().toString(), etPhone.getText().toString(), "yb_staff", "android", token);
        staffRequest.enqueue(new Callback<StaffRequest>() {
            @Override
            public void onResponse(Call<StaffRequest> call, Response<StaffRequest> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(StaffRequestActivity.this);
                    dialog.setCancelable(false);
                    dialog.setMessage(response.body().getRequestDetail().getResponseMsg());
                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //Action for "Delete".
                            toolbar.setVisibility(View.GONE);
                            dialog.dismiss();
                            setVisibility(lvPhone, lvDone);
                        }
                    });

                    final AlertDialog alert = dialog.create();
                    alert.show();

                } else {
                    allMethods.setAlert(StaffRequestActivity.this, "", response.body().getResponseMsg() + "");
                }
            }

            @Override
            public void onFailure(Call<StaffRequest> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StaffRequestActivity.this, "", t.getMessage() + "");
            }
        });
    }

    //new API
    public void requestStaffNew() {
        Log.d("TTT", "school: " + school);
        //Progress bar
        progressDialog = ProgressDialog.show(StaffRequestActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        String token = "";
        token = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("Token", "");
        Log.d("TTT", "token: " + token);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<StaffRequest> staffRequest = retrofitClass.schoolAccessCode(school, etEmail.getText().toString(), flag,etPhone.getText().toString());
        staffRequest.enqueue(new Callback<StaffRequest>() {
            @Override
            public void onResponse(Call<StaffRequest> call, Response<StaffRequest> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(StaffRequestActivity.this);
                    dialog.setCancelable(false);
                    dialog.setMessage(response.body().getRequestDetail().getResponseMsg());
                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //Action for "Delete".
                            toolbar.setVisibility(View.GONE);
                            dialog.dismiss();
                            setVisibility(lvPhone, lvDone);
                        }
                    });

                    final AlertDialog alert = dialog.create();
                    alert.show();

                } else {
                    allMethods.setAlert(StaffRequestActivity.this, "", response.body().getResponseMsg() + "");
                }
            }

            @Override
            public void onFailure(Call<StaffRequest> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StaffRequestActivity.this, "", t.getMessage() + "");
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (lvFirstname.getVisibility() == View.VISIBLE) {
            //lvFirstname.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_from_right));
            //lvSchool.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_to_right));
            setVisibility(lvFirstname, lvSchool);
        } else if (lvLastname.getVisibility() == View.VISIBLE) {
            setVisibility(lvLastname, lvFirstname);
        } else if (lvEmail.getVisibility() == View.VISIBLE) {
            setVisibility(lvEmail, lvSchool);
        } else if (lvPhone.getVisibility() == View.VISIBLE) {
            setVisibility(lvPhone, lvEmail);
        } else if (lvDone.getVisibility() == View.VISIBLE) {
            finish();
        } else if (lvSchool.getVisibility() == View.VISIBLE) {

            finish();
            super.onBackPressed();
        }
    }
}

import React from 'react'
import { View, Text, FlatList } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { colors, action } from '../utils'
import Actions from '../actions'
import Button from '@material-ui/core/Button'

export default function InsideGridSettings ({ name, screenId, updateComponents, data }) {
  const dispatch = useDispatch()
  const list = useSelector(({ components }) => components.pageInside)

  const _uploadThmb = async (e, screen_id) => {
    if (!e.target.files || e.target.files.length === 0) return
    const file = e.target.files[0]
    const res = await dispatch(Actions.pageInsideThumb(file, screen_id))
    if (res.Result) {
      updateComponents({ refesh: new Date() }, data.component_order_by)
    //   dispatch(Actions.get_pageinside_list(screenId))
    } else {
      dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: 'warning' })
    }
  }

  const _renderItem = ({ item, index }) => {
    return (
      <View style={styles.settingrow}>
        <Text style={styles.drowerList}>
          {index + 1}. {item.name}
        </Text>
        <Button
          size='small'
          style={{ color: '#fff', textTransform: 'capitalize', fontSize: 14 }}
          onClick={() => document.getElementById('insideImage' + index).click()}
        >
                    Change Image
        </Button>
        <input
          type='file'
          onChange={(e) => _uploadThmb(e, item.screen_id)}
          id={'insideImage' + index}
          style={{ display: 'none' }}
          accept='image/*'
        />
      </View>
    )
  }

  return (
    <View style={{ padding: 10, flex: 1 }}>
      <Text style={styles.Settingtitle}>
                Pages Inside of <b>{name}</b>
      </Text>
      <View style={{ paddingTop: 15 }}>
        <FlatList
          data={list || []}
          keyExtractor={(item, index) => String(index)}
          renderItem={_renderItem}
        />
      </View>
    </View>
  )
}

const styles = {
  Settingtitle: {
    color: colors.white,
    fontSize: 15,
    paddingTop: 5
  },
  drowerList: {
    color: colors.white,
    fontSize: 14,
    paddingVertical: 10
  },
  settingrow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
}

package com.app.yearbook.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.yearbook.R;
import com.app.yearbook.SchoolUserImageActivity;
import com.app.yearbook.databinding.ItemAllstudentListBinding;
import com.app.yearbook.model.employee.UserListItem;

import java.util.ArrayList;

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.MyViewHolder> implements Filterable {
    private ArrayList<UserListItem> studentLists;
    private ArrayList<UserListItem> studentListFiltered;
    private Activity ctx;
    ItemAllstudentListBinding binding;

    @NonNull
    @Override
    public StudentListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding=DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.item_allstudent_list, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final StudentListAdapter.MyViewHolder holder, final int position) {
        final UserListItem userList=studentListFiltered.get(position);
        holder.binding.setUser(userList);
        holder.binding.executePendingBindings();

        holder.binding.tvUserName.setText(holder.binding.getUser().getUserFirstname()+"  "+holder.binding.getUser().getUserLastname());

        Glide.with(ctx)
                .load(holder.binding.getUser().getUserImage())
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.profile_placeholder)
                .error(R.mipmap.profile_placeholder)
                .into(holder.binding.imgUserImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ctx,SchoolUserImageActivity.class);
                i.putExtra("UserData",holder.binding.getUser());
                i.putExtra("position",position);
//                i.putStringArrayListExtra("userImage", (ArrayList<String>) holder.binding.getUser().getUserImages());
                ctx.startActivity(i);
            }
        });

        binding.tvUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ctx,SchoolUserImageActivity.class);
                i.putExtra("UserData",holder.binding.getUser());
                i.putExtra("position",position);
//                i.putStringArrayListExtra("userImage", (ArrayList<String>) holder.binding.getUser().getUserImages());
                ctx.startActivity(i);
            }
        });
    }

    public StudentListAdapter(ArrayList<UserListItem> signatureLists, Activity context) {
        this.studentListFiltered = signatureLists;
        this.studentLists=signatureLists;
        this.ctx=context;
    }


    @Override
    public int getItemCount() {
        return studentListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    studentListFiltered = studentLists;
                } else {
                    ArrayList<UserListItem> filteredList = new ArrayList<>();
                    for (UserListItem row : studentLists) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name
                        if (row.getUserFirstname().toLowerCase().contains(charString.toLowerCase()) || row.getUserLastname().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                            Log.d("TTT","Filter size: "+filteredList.size());
                        }
                    }
                    studentListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = studentListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                studentListFiltered = (ArrayList<UserListItem>) filterResults.values;
                Log.d("TTT","publishResults: "+studentListFiltered.size());
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ItemAllstudentListBinding binding;

        public MyViewHolder(ItemAllstudentListBinding view) {
            super(view.getRoot());
            binding=view;
        }
    }
}

import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
// import banner from "../../../assets/images/Image2-min.png"
// import Footer from '../../../components/Footer';


export default function PageNotFound() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid item xs={12} sm={6}>
          {/* <img src={banner} style={{ height: "100%", width: "100%" }}/> */}
        </Grid>
        <Grid item xs={12} sm={6}>
          <Container component="main" className={classes.main} maxWidth="sm">
            <Typography variant="h2" component="h1" gutterBottom style={{ color: "red" }}>
              <u>404</u>
            </Typography>
            <Typography variant="h6" component="h2" gutterBottom style={{fontFamily: "honeymoon_personal_use"}}>
              Oops, the page you'r looking for can't found
            </Typography>
            <div style={{ paddingTop: 40 }} />
            <Link
              href="/"
            >
              <Fab
                variant="extended"
                size="medium"
                color="primary"
                classes={{ label: classes.buttonLable, sizeMedium: classes.buttonSize }}
              >
                Home
                    </Fab>
            </Link>

          </Container>
        </Grid>
      </Grid>
    </div >
  )
}


const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
  },
  main: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(2),
  },
  buttonLable: {
    textTransform: "capitalize",
    fontSize: 16
  },
  buttonSize: {
    minWidth: "230px !important"
  },
}));
package com.app.yearbook.utils;


import android.graphics.Bitmap;
import android.graphics.Typeface;

import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.app.yearbook.R;
import com.app.yearbook.model.profile.NotificationList;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.makeramen.roundedimageview.RoundedImageView;

import tcking.github.com.giraffeplayer2.VideoInfo;
import tcking.github.com.giraffeplayer2.VideoView;

public class BindingAdapter {

//    @android.databinding.BindingAdapter(value={"imageUrl", "placeholder"}, requireAll=false)
//    public static void loadImage(ImageView view, String imageUrl,Drawable placeholder) {
//        if (imageUrl==null || imageUrl == "") {
//            view.setImageDrawable(placeholder);
//        }else{
//            Glide.with(view.getContext())
//                    .load(imageUrl)
//                    .placeholder(R.mipmap.home_post_img_placeholder)
//                    .into(view);
//        }
//    }

    @androidx.databinding.BindingAdapter(value = {"viewVisibility"}, requireAll = true)
    public static void checkVisibility(View view, String path) {
        if (path == null || path.length() == 0) {
            view.setVisibility(View.GONE);
        } else if (path.equalsIgnoreCase("#")) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }

    @androidx.databinding.BindingAdapter(value = {"like"}, requireAll = true)
    public static void LikeVisibility(TextView view, String like) {
        if (like != null) {
            if (like.equals("0") || like.equals("1")) {
                view.setText(like + " Like");
            } else {
                view.setText(like + " Likes");
            }
        }
    }

    @androidx.databinding.BindingAdapter(value = {"commentStaff"}, requireAll = true)
    public static void CommentStaff(TextView view, String comment) {
        if (comment != null) {
            if (comment.equals("0")) {
                view.setText("No comments");
            } else if (comment.equals("1")) {
                view.setText("View " + comment + " Comment");
            } else {
                view.setText("View All " + comment + " Comments");
            }
        }
    }

    @androidx.databinding.BindingAdapter(value = {"comment"}, requireAll = true)
    public static void Comment(TextView view, String comment) {
        if (comment != null) {
            if (comment.equals("0")) {
                view.setText("Be the first to comment on this");
            } else if (comment.equals("1")) {
                view.setText("View " + comment + " Comment");
            } else {
                view.setText("View All " + comment + " Comments");
            }
        }
    }

    @androidx.databinding.BindingAdapter(value = {"report"}, requireAll = true)
    public static void reported(TextView view, String report) {
        if (report != null) {
            if (report.equals("0")) {
                view.setText("Not reported yet");
            } else if (report.equals("1")) {
                view.setText(report + " time reported");
            } else {
                view.setText(report + " times reported");
            }
        }
    }

    @androidx.databinding.BindingAdapter(value = {"setPostImageUrl"}, requireAll = false)
    public static void setPostImage(AppCompatImageView view, String url) {
        Glide.with(view.getContext())
                .load(url)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.home_post_img_placeholder)
                .error(R.mipmap.home_post_img_placeholder)
                .into(view);
    }

    @androidx.databinding.BindingAdapter(value = {"fontResourceId"}, requireAll = false)
    public static void setTypeface(AppCompatTextView view, int resourceId) {
//        Typeface typeface = ResourcesCompat.getFont(view.getContext(), resourceId);
        Typeface typeface = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            typeface = view.getContext().getResources().getFont(resourceId);
        } else {
            typeface = ResourcesCompat.getFont(view.getContext(), resourceId);
        }
        view.setTypeface(typeface);
    }

    @androidx.databinding.BindingAdapter(value = {"userImageUrl"}, requireAll = false)
    public static void setUserImage(RoundedImageView view, String url) {
        Glide.with(view.getContext())
                .load(url)
                .asBitmap()
//                .skipMemoryCache(true)
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .centerCrop()
                .placeholder(R.mipmap.profile_placeholder)
                .error(R.mipmap.profile_placeholder)
                .into(view);
    }

    @androidx.databinding.BindingAdapter(value = {"videoUrl", "thumbnailUrl"}, requireAll = false)
    public static void setVideoUrl(VideoView videoView, String url, String thumbUrl) {
        if (videoView.getCoverView() != null && !TextUtils.isEmpty(thumbUrl)) {

            Glide.with(videoView.getContext())
                    .load(thumbUrl)
                    .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.mipmap.home_post_img_placeholder)
                    .error(R.mipmap.home_post_img_placeholder)
                    .into(videoView.getCoverView());
        }
        videoView.getVideoInfo().setPortraitWhenFullScreen(false);
        videoView.setVideoPath(url);
        videoView.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player
        //videoView.getPlayer().setVolume(-1,-1);
        // videoView.getVideoInfo().setLooping(true);
        Log.d("TTT", "setVideoUrl: Adapter: " + videoView.getPlayer().isMute());
    }

    @androidx.databinding.BindingAdapter(value = {"notificationView"}, requireAll = false)
    public static void setNotificationADView(View view, NotificationList list) {
        if (list.getNotificationType().equalsIgnoreCase(Constants.SIGNATURE_TYPE_ADD)) {
            if (list.getIsAccept().equals("0")) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @androidx.databinding.BindingAdapter(value = {"notificationAddView"}, requireAll = false)
    public static void setNotificationAddView(View view, NotificationList list) {
        if (list.getNotificationType().equalsIgnoreCase(Constants.SIGNATURE_TYPE_ACCEPT)) {
            if (list.getIsAccept().equals("1")) {
                view.setVisibility(View.GONE);
            } else {
                view.setVisibility(View.VISIBLE);
            }
        } else {
            view.setVisibility(View.GONE);
        }
    }
}

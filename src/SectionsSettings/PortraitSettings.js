import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { colors } from '../utils'
import InputText from '../components/InputText'
import Fab from '@material-ui/core/Fab'
import PortraintColumnModal from '../components/PortraintColumnModal'
import { Button } from '@material-ui/core'

export default function PortraitSettings ({ data, updateComponents }) {
  const classes = useStyles()

  const [state, setState] = useState({
    grid_row: data.grid_row,
    isVisible: false,
    component_title: data.component_title,
    student_list: data.student_list,
    staff_list: data.staff_list
  })

  const _selectionDone = ({ students, staff }) => {
    updateComponents({ student_list: students, staff_list: staff }, data.component_order_by)
    setState({ ...state, isVisible: false })
  }

  const _handleTitle = (e) => {
    const component_title = e.target.value
    updateComponents({ component_title }, data.component_order_by)
    setState({ ...state, component_title })
  }

  const _column = (grid_row) => {
    updateComponents({ grid_row }, data.component_order_by)
    setState({ ...state, grid_row })
  }

  return (
    <div style={{ padding: 10, flex: 1 }}>
      <div>
        <InputText
          title='Name of Column'
          defaultValue={data.title}
          onChange={_handleTitle}
          value={state.component_title}
          labelWidth={120}
        />
      </div>
      <div style={{ padding: 8 }} />
      <span style={{ color: colors.white }}>
                Portraits Per Row
      </span>
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', paddingTop: 10 }}>
        <Button
          variant='contained'
          color={state.grid_row === 1 || state.grid_row === '1' ? 'primary' : 'inherit'}
          onClick={() => _column(1)}
        >
                    One
        </Button>
        <Button
          variant='contained'
          color={state.grid_row === 2 || state.grid_row === '2' ? 'primary' : 'inherit'}
          onClick={() => _column(2)}
        >
                    Two
        </Button>
        <Button
          variant='contained'
          color={state.grid_row === 3 || state.grid_row === '3' ? 'primary' : 'inherit'}
          onClick={() => _column(3)}
        >
                    Three
        </Button>
      </div>
      <div style={{ padding: 15 }} />
      <Fab
        variant='extended'
        size='medium'
        color='primary'
        classes={{ label: classes.buttonLable, sizeMedium: classes.buttonSize }}
        onClick={() => setState({ ...state, isVisible: !state.isVisible })}
      >
                Manage Column Images
      </Fab>
      <PortraintColumnModal
        isVisible={state.isVisible}
        handleClose={() => setState({ ...state, isVisible: false })}
        onSubmit={_selectionDone}
        title={state.component_title}
        student_list={state.student_list}
        staff_list={state.staff_list}
      />
    </div>
  )
}

const useStyles = makeStyles(() => ({
  buttonLable: {
    textTransform: 'capitalize'
  },
  buttonSize: {
    width: '100% !important'
  }
}))

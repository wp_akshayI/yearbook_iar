package com.app.yearbook;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Typeface;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.app.yearbook.adapter.AdapterFontList;
import com.app.yearbook.databinding.ActivityDesignSignatureBinding;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.SignFontModel;
import com.app.yearbook.model.SignatureModel;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.Constants;
import com.app.yearbook.utils.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.List;

public class DesignSignatureActivity extends AppCompatActivity {

    private static final String TAG = "DesignSignatureActivity";
    ActivityDesignSignatureBinding binding;
    SignatureModel signatureModel;
    List<SignFontModel> signFontModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_design_signature);
        if (getIntent().hasExtra(Constants.SIGNATURE_DATA)) {
            signatureModel = getIntent().getParcelableExtra(Constants.SIGNATURE_DATA);
        }
        setToolbar();

        signFontModelList = getSignModelList();
        binding.rcSignature.addItemDecoration(new ItemOffsetDecoration(this, R.dimen.item_offset));
        binding.rcSignature.setAdapter(new AdapterFontList(signFontModelList, new OnRecyclerClick() {
            @Override
            public void onClick(int pos, int type) {
                Log.d(TAG, "onClick: ");
                setTypeFace(signFontModelList.get(pos));
            }

            @Override
            public void onVote(int pos, String pollOptionId) {
                Log.d(TAG, "onClick: ");
            }
        }));

        try{
            binding.txtSignature.setText(LoginUser.getUserTypeKey()==2?LoginUser.getUserData().getStaffFirstname():LoginUser.getUserData().getUserFirstname());
        }catch (Exception e){
            binding.txtSignature.setText(getString(R.string.app_name));
        }
        /*if (LoginUser.getUserData() != null && LoginUser.getUserData().getUserFirstname() != null) {
            binding.txtSignature.setText(LoginUser.getUserData().getUserFirstname());
        } else {
            binding.txtSignature.setText(getString(R.string.app_name));
        }*/

        if (signFontModelList != null && signFontModelList.size() > 0) {
            setTypeFace(signFontModelList.get(0));
        }

    }

    private void setTypeFace(SignFontModel signFontModel) {
        Typeface typeface = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            typeface = getResources().getFont(signFontModel.getResourceId());
        } else {
            typeface = ResourcesCompat.getFont(this, signFontModel.getResourceId());
        }
        binding.txtSignature.setTypeface(typeface);
        if (signatureModel != null) {
            signatureModel.setFontResourceId(signFontModel.getResourceId());
            signatureModel.setFontId(signFontModel.getId());
        }
    }

    private void setToolbar() {
        setSupportActionBar(binding.appbar.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (LoginUser.getUserData() != null && LoginUser.getUserData().getSchoolName() != null) {
            binding.appbar.toolbarTitle.setText(LoginUser.getUserData().getSchoolName());
        } else {
            binding.appbar.toolbarTitle.setText(getString(R.string.app_name));
        }
    }

    private List<SignFontModel> getSignModelList() {
        List<SignFontModel> signFontModelList = new ArrayList<>();
        signFontModelList.add(new SignFontModel(R.font.magnificent, "Magnificent", "1"));
        signFontModelList.add(new SignFontModel(R.font.edwardian_scr_itc_tt, "Edwardian", "2"));
        signFontModelList.add(new SignFontModel(R.font.angelface, "Angelugue", "3"));
        signFontModelList.add(new SignFontModel(R.font.arabella, "Arabella", "4"));
        signFontModelList.add(new SignFontModel(R.font.argentina_script, "Synthesia", "5"));

        signFontModelList.add(new SignFontModel(R.font.im_fell_french_canon_regular, "IM FELL French Canon", "6"));
        signFontModelList.add(new SignFontModel(R.font.adine_kirnberg, "Adine Kirnberg", "7"));
        signFontModelList.add(new SignFontModel(R.font.allura_regular, "Allura", "8"));
        signFontModelList.add(new SignFontModel(R.font.gessele, "Gessele", "9"));
        signFontModelList.add(new SignFontModel(R.font.garineldo, "Garineldo", "10"));
        signFontModelList.add(new SignFontModel(R.font.leckerli_one_regular, "Leckerli One", "11"));
        signFontModelList.add(new SignFontModel(R.font.fabfeltscript_bold, "FabfletScript", "12"));
        signFontModelList.add(new SignFontModel(R.font.marck_script_regular, "Marck Script", "13"));
        signFontModelList.add(new SignFontModel(R.font.sweet_honey, "Honey Script", "14"));
        signFontModelList.add(new SignFontModel(R.font.digory_doodles, "Digory Doodles", "15"));
        signFontModelList.add(new SignFontModel(R.font.overthe_rainbow, "Over the Rainbow", "16"));
        return signFontModelList;
    }

    private List<SignFontModel> getSignModelList1() {
        List<SignFontModel> signFontModelList = new ArrayList<>();
//        signFontModelList.add(new SignFontModel(R.font.brightlight, "Brightlight", "1"));
        signFontModelList.add(new SignFontModel(R.font.magnificent, "Magnificent", "2"));
//        signFontModelList.add(new SignFontModel(R.font.otto, "Otto", "3"));
        signFontModelList.add(new SignFontModel(R.font.edwardian_scr_itc_tt, "Edwardian", "4"));
        signFontModelList.add(new SignFontModel(R.font.angelface, "Angelugue", "5"));
        signFontModelList.add(new SignFontModel(R.font.arabella, "Arabella", "6"));
        signFontModelList.add(new SignFontModel(R.font.argentina_script, "Synthesia", "7"));
//        signFontModelList.add(new SignFontModel(R.font.mistral, "Mistral", "8"));

        signFontModelList.add(new SignFontModel(R.font.im_fell_french_canon_regular, "IM FELL French Canon", "9"));
        signFontModelList.add(new SignFontModel(R.font.adine_kirnberg, "Adine Kirnberg", "10"));
        signFontModelList.add(new SignFontModel(R.font.allura_regular, "Allura", "11"));
        signFontModelList.add(new SignFontModel(R.font.gessele, "Gessele", "12"));
        signFontModelList.add(new SignFontModel(R.font.garineldo, "Garineldo", "13"));
        signFontModelList.add(new SignFontModel(R.font.leckerli_one_regular, "Leckerli One", "14"));
        signFontModelList.add(new SignFontModel(R.font.fabfeltscript_bold, "FabfletScript", "15"));
        signFontModelList.add(new SignFontModel(R.font.marck_script_regular, "Marck Script", "16"));
        signFontModelList.add(new SignFontModel(R.font.sweet_honey, "Honey Script", "17"));
        signFontModelList.add(new SignFontModel(R.font.digory_doodles, "Digory Doodles", "18"));
        signFontModelList.add(new SignFontModel(R.font.overthe_rainbow, "Over the Rainbow", "19"));
        return signFontModelList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_next:
                Intent intent = new Intent(this, SignaturePreviewActivity.class);
                intent.putExtra(Constants.SIGNATURE_DATA, signatureModel);
                startActivityForResult(intent, 100);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_next, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }
}

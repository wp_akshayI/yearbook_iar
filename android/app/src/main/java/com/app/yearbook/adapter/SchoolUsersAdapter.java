package com.app.yearbook.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.yearbook.R;
import com.app.yearbook.model.employee.UserListItem;

import java.util.ArrayList;

public class SchoolUsersAdapter extends ArrayAdapter<UserListItem> {

    public static final int TYPE_MINUS = 0;
    public static final int TYPE_PLUS = 1;
    ArrayList<UserListItem> arrayList;
    Context context;


    public SchoolUsersAdapter(Context context, ArrayList<UserListItem> arrayList) {
        super(context, 0, arrayList);
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        return super.getDropDownView(position, convertView, parent);

        return rowview(convertView, position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);
        return rowview(convertView, position);
    }

    private View rowview(final View convertView, int position) {
        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            v = inflater.inflate(R.layout.spinner_item_schooluser, null);
        }
        TextView lbl = v.findViewById(R.id.tvUsers);
        lbl.setText(arrayList.get(position).getUserFirstname()+" "+arrayList.get(position).getUserLastname());

        return v;
    }

//    @Override
//    public int getCount() {
//        return arrayList.size();
//    }

}

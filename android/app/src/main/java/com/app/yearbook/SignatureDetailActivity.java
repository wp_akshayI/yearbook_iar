package com.app.yearbook;

import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.graphics.Typeface;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.app.yearbook.adapter.StudentSignatureListAdapter;
import com.app.yearbook.databinding.ActivitySignatureDetailBinding;
import com.app.yearbook.model.profile.NotificationList;
import com.app.yearbook.model.profile.SignatureList;
import com.app.yearbook.model.profile.getNotification;
import com.app.yearbook.model.profile.getSignature;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.Constants;
import com.app.yearbook.utils.OnRecyclerClick;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignatureDetailActivity extends AppCompatActivity {

    ActivitySignatureDetailBinding binding;
    SignatureList signatureList;
    private static final String TAG = "SignatureDetailActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_signature_detail);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_signature_detail);
    
        setView();
    }

    private void setView() {

        setToolbar();

        if(getIntent().getExtras()!=null){
            if (getIntent().getBooleanExtra("isPush", false)){
                getSignature(getIntent().getStringExtra("signId"));
            }else {
                signatureList = getIntent().getParcelableExtra("Signature");
                signatureList.setUserFirstname(signatureList.getSenderName().split(" ")[0]);

                if(signatureList.getSignatureFontType() == null)
                    return;

                binding.setModel(signatureList);
                setTypeFace();
                binding.txtSignature.setText(binding.getModel().getUserFirstname());
                String createdDate = binding.getModel().getSignatureCreated();

                Date chkDate = null;
                try {
                    chkDate = Constants.dbDateFormat.parse(createdDate);
                    String formattedDate = Constants.simpleDateFormat.format(chkDate);
                    binding.txtDate.setText("Signed on " + Constants.getDisplayDate(formattedDate, Constants.simpleDateFormat));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void getSignature(final String signId) {
        RetrofitClass retrofitClass =  retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<getNotification> staffPostResponseCall = retrofitClass.getNotification(LoginUser.getUserId(), LoginUser.getUserTypeKey());
        staffPostResponseCall.enqueue(new Callback<getNotification>() {
            @Override
            public void onResponse(Call<getNotification> call, Response<getNotification> response) {
                getNotification staffPostResponse = response.body();
                try {
                    if (staffPostResponse != null) {
                        if (staffPostResponse.getResult().equalsIgnoreCase("true")) {
                            if (staffPostResponse.getNotificationList().size() > 0) {
                                List<NotificationList> notificationLists = new ArrayList<>();
                                notificationLists.addAll(staffPostResponse.getNotificationList());

                                for (int i = 0; i < notificationLists.size(); i++) {
                                    signatureList = new Gson().fromJson(notificationLists.get(i).getSignatureData().toString(), SignatureList.class);
                                    if (notificationLists.get(i).getNotificationId().contentEquals(signId)){
                                        signatureList.setUserFirstname(signatureList.getSenderName().split(" ")[0]);
                                        binding.setModel(signatureList);
                                        setTypeFace();
                                        binding.txtSignature.setText(binding.getModel().getUserFirstname());
                                        String createdDate=binding.getModel().getSignatureCreated();

                                        Date chkDate = null;
                                        try {
                                            chkDate = Constants.dbDateFormat.parse(createdDate);
                                            String formattedDate = Constants.simpleDateFormat.format(chkDate);
                                            binding.txtDate.setText("Signed on " + Constants.getDisplayDate(formattedDate, Constants.simpleDateFormat));
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<getNotification> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }

    private void setTypeFace() {
        Typeface typeface = null;
        Log.d(TAG, "setTypeFace: " + Constants.fontMap.size());
        Log.d(TAG, "setTypeFace: " + signatureList.getSignatureFontType());

        if (signatureList.getSignatureFontType() == null)
            signatureList.setSignatureFontType("2");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            typeface = getResources().getFont(Constants.fontMap.get(signatureList.getSignatureFontType()));
        } else {
            typeface = ResourcesCompat.getFont(this, Constants.fontMap.get(signatureList.getSignatureFontType()));
        }
        binding.txtSignature.setTypeface(typeface);

    }

    private void setToolbar() {
        setSupportActionBar(binding.appbar.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (LoginUser.getUserData() != null && LoginUser.getUserData().getSchoolName() != null) {
            binding.appbar.toolbarTitle.setText(LoginUser.getUserData().getSchoolName());
        }else {
            binding.appbar.toolbarTitle.setText(getString(R.string.app_name));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

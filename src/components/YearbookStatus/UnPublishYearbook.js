import React from "react"
import { View, Image, Text } from "react-native";
import { Header } from "react-navigation-stack";
import { published } from "../../assets"


export default function UnPublishYearbook() {
    return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingHorizontal: 30, marginTop: -Header.HEIGHT }}>

            <Image
                source={published}
                resizeMode="contain"
                style={{ height: 75, width: 75 }}
            />

            <Text style={{ textAlign: "center", fontWeight: "bold", fontSize: 15, marginTop: 30, marginBottom: 5 }}>
                Unpublished Yearbook!
            </Text>

            <Text style={{ textAlign: "center", fontSize: 14 }}>
                You can't see yearbook. you need to publish or purchase a yearbook.
            </Text>

        </View>
    )
}
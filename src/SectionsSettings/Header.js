import React from "react"
import { View, Text, StyleSheet } from "react-native";
import { colors } from "../utils";

function AppHeader({ title }) {
    return(
        <View style={styles.root}>

            <View />

            <View style={{ maxWidth: "70%" }}>
                <Text 
                    style={styles.title}
                    ellipsizeMode="tail"
                    numberOfLines={1}
                >
                    {title || "Untitled Page"}
                </Text>
            </View>

            <View />

        </View>
    )
    
}

export default AppHeader;

const styles = StyleSheet.create({
    root: {
        flex: 1,
        height: 45,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        borderBottomWidth: 0.4,
        borderBottomColor: "#d8d3d3"
    },
    title: {
        fontSize: 18,
        fontWeight: "500",
        color: colors.gray,
        textAlign: "center",
        fontFamily: "Calibri",
        fontWeight: "bold"
    }
})
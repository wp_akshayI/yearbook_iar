package com.app.yearbook.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.yearbook.ImageActivity;
import com.app.yearbook.R;
import com.app.yearbook.StaffPostDetailActivity;
import com.app.yearbook.StudentPostEditActivity;
import com.app.yearbook.databinding.ItemUserpostBinding;
import com.app.yearbook.model.studenthome.PostList;

import java.util.ArrayList;

public class UserDetailPostAdapter extends RecyclerView.Adapter<UserDetailPostAdapter.MyViewHolder> {
    private ArrayList<PostList> userLists;
    private Activity ctx;
    private String type;
    private int IntentCodeStaff = 890;
    private ItemUserpostBinding binding;

    @NonNull
    @Override
    public UserDetailPostAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.item_userpost, parent, false);

        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                , R.layout.item_userpost, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserDetailPostAdapter.MyViewHolder holder, final int position) {

        holder.binding.setPost(userLists.get(position));

        //post img
        if (holder.binding.getPost().getPostFile() != null && !holder.binding.getPost().getPostFile().equals("")) {
            holder.binding.lvMain.setVisibility(View.VISIBLE);
            final String uri = holder.binding.getPost().getPostFile();
            String extension = uri.substring(uri.lastIndexOf("."));
            Log.d("TTT", "Url type: " + extension);

            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {

                holder.binding.imgPostVideo.setVisibility(View.GONE);
                if (holder.binding.getPost().getPostFile() != null) {
                    Glide.with(ctx)
                            .load(holder.binding.getPost().getPostFile())
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(holder.binding.imgPost);
                }
            } else {
                holder.binding.imgPostVideo.setVisibility(View.VISIBLE);
                Glide.with(ctx)
                        .load(holder.binding.getPost().getPostThumbnail())
                        .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.mipmap.home_post_img_placeholder)
                        .error(R.mipmap.home_post_img_placeholder)
                        .into(holder.binding.imgPost);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (type.equalsIgnoreCase("staff")) {
                        Log.d("TTT", "Like chng staff: " + holder.binding.getPost().getTotalLike());
                        Log.d("TTT", "User Post Adapter staff: " + holder.binding.getPost().getPostId() + " / " + holder.binding.getPost().getFlag());

                        if (holder.binding.getPost().getFlag().equalsIgnoreCase("tagg")) {
                            Intent i = new Intent(ctx, ImageActivity.class);
                            i.putExtra("caption", holder.binding.getPost().getPostDescription());
                            i.putExtra("FileUrl", holder.binding.getPost().getPostFile());
                            ctx.startActivity(i);
                        } else {
                            Intent i = new Intent(ctx, StaffPostDetailActivity.class);
                            i.putExtra("PostId", holder.binding.getPost().getPostId());
                            ctx.startActivityForResult(i, IntentCodeStaff);
                        }

                    } else {
                        Log.d("TTT", "User Post Adapter student: " + holder.binding.getPost().getPostId() + " / " + holder.binding.getPost().getFlag());

                        if (holder.binding.getPost().getFlag().equalsIgnoreCase("tagg")) {
                            Intent i = new Intent(ctx, ImageActivity.class);
                            i.putExtra("caption", holder.binding.getPost().getPostDescription());
                            i.putExtra("FileUrl", holder.binding.getPost().getPostFile());
                            ctx.startActivity(i);
                        } else {
                            Intent i = new Intent(ctx, StudentPostEditActivity.class);
                            i.putExtra("PostObject", holder.binding.getPost());
                            ctx.startActivity(i);
                        }
                    }
                }
            });
        } else {
            Log.d("TTT", "UnKnown item: " + holder.binding.getPost().getPostId() + " / " + holder.binding.getPost().getUserId());
            holder.binding.lvMain.setVisibility(View.GONE);
            holder.binding.imgPostVideo.setVisibility(View.GONE);
            holder.binding.imgPost.setVisibility(View.GONE);
        }
    }

    public UserDetailPostAdapter(ArrayList<PostList> userLists, Activity context, String type) {
        this.userLists = userLists;
        this.ctx = context;
        this.type = type;
    }

    @Override
    public int getItemCount() {
        return userLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemUserpostBinding binding;

        public MyViewHolder(ItemUserpostBinding view) {
            super(view.getRoot());
            binding = view;

        }
    }
}

import React from 'react'
import { View, Image, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { arrow_right, image_placeholder } from '../../assets'
import { withNavigation } from 'react-navigation'
import { colors } from '../../utils'

function YearbookList ({ data, navigation }) {
  const _navigate = () => {
    return navigation.navigate(
      'YearbookDetails',
      {
        title: data.yearbook_name,
        screenId: data.screen_id
      })
  }
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={_navigate}
    >
      <View
        activeOpacity={0.8}
        style={styles.item}
      >
        <Image
          source={data.yearbook_cover ? { uri: data.yearbook_cover } : image_placeholder}
          style={styles.itemAvatar}
          onError={(error) => {
            console.log(error)
          }}
        />
        <View style={styles.itemContain}>
          <Text style={styles.itemTitle}>
            {data.yearbook_name}
          </Text>
          <Text>
            {data.yearbook_status === 1 || data.yearbook_status === '1' ? 'Published' : 'Unpublished'}
          </Text>
        </View>

        <View>
          <Image source={arrow_right} style={styles.rightIc} />
        </View>
      </View>
    </TouchableOpacity>
  )
}

export default withNavigation(YearbookList)

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 6,
    marginTop: 8,
    backgroundColor: colors.white
  },
  itemAvatar: {
    height: 80,
    width: 80,
    resizeMode: 'contain'
  },
  itemContain: {
    marginLeft: 8,
    flex: 1
  },
  itemTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingBottom: 5,
    flexWrap: 'wrap'
  },
  rightIc: {
    height: 16,
    width: 16,
    resizeMode: 'contain',
    paddingHorizontal: 15
  }
})

package com.app.yearbook.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.ybq.android.spinkit.style.Circle;
import com.makeramen.roundedimageview.RoundedImageView;
import com.app.yearbook.ImageActivity;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.UserDetailActivity;
import com.app.yearbook.model.search.User;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserAdapter.MyViewHolder> {
    private ArrayList<User> userLists;
    private Activity ctx;
    private String type;
    public ProgressDialog progressDialog;

    @NonNull
    @Override
    public SearchUserAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_searchuser, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchUserAdapter.MyViewHolder holder, final int position) {
        final User userList = userLists.get(position);
        holder.tvUserType.setText(userList.getUserType() + "");
        holder.tvUserName.setText(userList.getUserFirstname() + "  " + userList.getUserLastname());

        Glide.with(ctx)
                .load(userList.getUserImage())
                .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.profile_placeholder)
                .error(R.mipmap.profile_placeholder)
                .into(holder.imgUserPhoto);

        if(userList.getUserImage()!=null)
        {
            holder.imgUserPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, ImageActivity.class);
                    i.putExtra("FileUrl", userList.getUserImage());
                    ctx.startActivity(i);
                }
            });
        }

        if(type.equalsIgnoreCase("staff"))
        {
            holder.imgReport.setVisibility(View.VISIBLE);
            holder.imgReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ctx);
                    LayoutInflater inflater = ctx.getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
                    dialogBuilder.setView(dialogView);
                    final AlertDialog alert = dialogBuilder.create();
                    alert.setCancelable(false);
                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                    TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
                    tvMessage.setText("Are you sure you want to remove the user?");

                    TextView tvYes = dialogView.findViewById(R.id.tvYes);
                    TextView tvNo = dialogView.findViewById(R.id.tvNo);

                    tvYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            alert.dismiss();
                            final String id = userList.getUserId();

                            Log.d("TTT","Id: "+id);
                            progressDialog = ProgressDialog.show(ctx, "", "", true);
                            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            progressDialog.setContentView(R.layout.progress_view);
                            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                            Circle bounce = new Circle();
                            bounce.setColor(Color.BLACK);
                            progressBar.setIndeterminateDrawable(bounce);

                            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                            final Call<GiveReport> userPost = retrofitClass.staffDeleteUser(Integer.parseInt(id));
                            userPost.enqueue(new Callback<GiveReport>() {
                                @Override
                                public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }

                                    if(response.body().getResponseCode().equals("1")) {
                                        for (int i = 0; i < userLists.size(); i++) {
                                            if (userLists.get(i).getUserId().equals(String.valueOf(id))) {
                                                Log.d("TTT", "position: " + i);
                                                userLists.remove(i);
                                                notifyItemRemoved(i);
                                            }
                                        }
                                        Toast.makeText(ctx, "Successfully delete the user.", Toast.LENGTH_SHORT).show();
                                    }
                                    else if(response.body().getResponseCode().equals("10"))
                                    {
                                        Toast.makeText(ctx,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                                        LoginUser loginSP = new LoginUser(ctx);
                                        loginSP.clearData();

                                        Intent i = new Intent(ctx, LoginActivity.class);
                                        ctx.startActivity(i);
                                        ctx.finish();
                                    }
                                    else
                                    {
                                        Toast.makeText(ctx,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<GiveReport> call, Throwable t) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                    Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

                    tvNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });
                    alert.show();
                }
            });
        }

//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               // Toast.makeText(ctx,"hello",Toast.LENGTH_SHORT).show();
//                Log.d("TTT","Type user search: "+type);
//                Intent i = new Intent(ctx, UserDetailActivity.class);
//                i.putExtra("Photo",userList.getUserImage());
//                i.putExtra("type",type);
//                i.putExtra("Id", userList.getUserId());
//                i.putExtra("schoolId", userList.getSchoolId());
//                i.putExtra("Name", userList.getUserFirstname() + " " + userList.getUserLastname());
//                ctx.startActivity(i);
//            }
//        });

    }

    public SearchUserAdapter(ArrayList<User> userLists, Activity context,String type) {
        this.userLists = userLists;
        this.ctx = context;
        this.type=type;
    }

    @Override
    public int getItemCount() {
        return userLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvUserName, tvUserType;
        public RoundedImageView imgUserPhoto;
        public ImageView imgReport;


        public MyViewHolder(View view) {
            super(view);
            tvUserType = view.findViewById(R.id.tvUserType);
            tvUserName = view.findViewById(R.id.tvUserName);
            imgUserPhoto = view.findViewById(R.id.imgUserImage);
            imgReport=view.findViewById(R.id.imgReport);

            view.setOnClickListener(this);
            tvUserName.setOnClickListener(this);
            tvUserType.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Log.d("TTT","Type user search: "+type);
            Intent i = new Intent(ctx, UserDetailActivity.class);
            i.putExtra("Photo",userLists.get(getAdapterPosition()).getUserImage());
            i.putExtra("type",type);
            i.putExtra("Id", userLists.get(getAdapterPosition()).getUserId());
            i.putExtra("schoolId", userLists.get(getAdapterPosition()).getSchoolId());
            i.putExtra("Name", userLists.get(getAdapterPosition()).getUserFirstname() + " " + userLists.get(getAdapterPosition()).getUserLastname());
            ctx.startActivity(i);
        }
    }
}

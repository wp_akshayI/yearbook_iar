package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.StaffReportContentListAdapter;
import com.app.yearbook.model.staff.home.ContentList;
import com.app.yearbook.model.staff.home.GetContent;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportContentActivity extends AppCompatActivity {

    private int postId;
    private RecyclerView rvReportContent;
    private Toolbar toolbar;
    private LinearLayout lvSendMsg,lvNoData;
    private ArrayList<ContentList> getContent;
    private ProgressDialog progressDialog;
    private AllMethods allMethods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_content);

        setView();
    }
    public void setView()
    {
        //set toolbar
        setToolbar();

        if(getIntent().getExtras()!=null)
        {
            postId=Integer.parseInt(getIntent().getStringExtra("postId"));
        }

        //lv
        lvNoData=findViewById(R.id.lvNoData);

        //recycler view
        rvReportContent=findViewById(R.id.rvReportContent);

        getReport(postId);
    }
    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Reported content");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }
    public void getReport(int postId) {
        //Progress bar
        progressDialog = ProgressDialog.show(ReportContentActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetContent> getComment = retrofitClass.getContent(postId);
        getComment.enqueue(new Callback<GetContent>() {
            @Override
            public void onResponse(Call<GetContent> call, Response<GetContent> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (response.body().getResponseCode().equals("1")) {

                    lvNoData.setVisibility(View.GONE);
                    rvReportContent.setVisibility(View.VISIBLE);

                    if (response.body().getContentList().size() > 0) {
                        getContent = new ArrayList<>();
                        getContent.addAll(response.body().getContentList());
                        StaffReportContentListAdapter staffReportContentListAdapter = new StaffReportContentListAdapter(getContent, ReportContentActivity.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        rvReportContent.setLayoutManager(mLayoutManager);
                        rvReportContent.setAdapter(staffReportContentListAdapter);
                    }
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(ReportContentActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(ReportContentActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(ReportContentActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    lvNoData.setVisibility(View.VISIBLE);
                    rvReportContent.setVisibility(View.GONE);
                    //allMethods.setAlert(StudentCommentActivity.this, "", response.body().getResponseMsg() + "");
                }
            }

            @Override
            public void onFailure(Call<GetContent> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(ReportContentActivity.this, "", t.getMessage() + "");
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

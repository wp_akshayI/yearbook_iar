package com.app.yearbook.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserProfile {

    @SerializedName("post_list")
    private List<Object> postList;

    @SerializedName("user_detail")
    private UserDetail userDetail;

    public void setPostList(List<Object> postList) {
        this.postList = postList;
    }

    public List<Object> getPostList() {
        return postList;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }
}
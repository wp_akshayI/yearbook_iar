package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.yearbook.model.employee.UserListItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.doodle.android.chips.ChipsView;
import com.doodle.android.chips.model.Contact;
import com.github.ybq.android.spinkit.style.Circle;
import com.volokh.danylo.hashtaghelper.HashTagHelper;
import com.app.yearbook.model.library.AddPost;
import com.app.yearbook.model.search.User;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.BitmapUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.PlayerManager;
import tcking.github.com.giraffeplayer2.VideoInfo;
import tcking.github.com.giraffeplayer2.VideoView;

public class AddYearbookPostActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageView imgPostPhoto;
    private VideoView videoSelected;
    private EditText edtPostMsg;
    private Uri getUri;
    private Bitmap thumbImage;
    private LoginUser loginUser;
    private AllMethods allMethods;
    private HashTagHelper mEditTextHashTagHelper;
    private String getType = "",getMsg,getStaffId="",getSchoolId="",CategoryId="",tagUserList="",post_user_id="";
    private MultipartBody.Part part, thumbPart = null;
    private ProgressDialog progressDialog;
    private File fileImage= null , fileVideo = null;
    private RequestBody rbFile= null, rbFileThumb = null, type;
    private LinearLayout lvTagPeople,lvTag;
    private int IntentCode = 777,CropType=2,post_type=1;
    private ArrayList<User> getUser;
    private User user;
    private ChipsView mChipsView;
    private List<String> tagUser=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_yearbook_empty);

        setView();
    }

    public void setView() {
        //common methods
        allMethods = new AllMethods();

        //toolbar
        setToolbar();

        //user array
        getUser = new ArrayList<>();

        //SP object
        loginUser = new LoginUser(AddYearbookPostActivity.this);
        if (loginUser.getUserData() != null) {
            getStaffId = loginUser.getUserData().getStaffId();
            getSchoolId = loginUser.getUserData().getSchoolId();
        }

        //chip view
        mChipsView =findViewById(R.id.chipsView);
        mChipsView.setChipsListener(new ChipsView.ChipsListener() {
            @Override
            public void onChipAdded(ChipsView.Chip chip) {
                // chip added
            }

            @Override
            public void onChipDeleted(ChipsView.Chip chip) {
                // chip deleted
                Log.d("TTT","Chip delete: "+chip.getContact().getDisplayName());
                String id=chip.getContact().getDisplayName();

                for (int i=0;i<getUser.size();i++)
                {
                    if(getUser.get(i).getUserId().equalsIgnoreCase(id))
                    {
                        getUser.remove(i);
                    }
                }

                if(getUser.size()==0)
                {
                    mChipsView.setVisibility(View.GONE);
                    lvTag.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence text) {
                // text was changed
            }

            @Override
            public boolean onInputNotRecognized(String text) {
                // return true to delete the input
                return false; // keep the typed text
            }
        });

        //img
        imgPostPhoto = findViewById(R.id.imgPost);

        //video
        videoSelected = findViewById(R.id.videoSelected);

        //et msg
        edtPostMsg = findViewById(R.id.edtPostMsg);

        //lv
        lvTag=findViewById(R.id.lvTag);
        lvTagPeople = findViewById(R.id.lvTagPeople);

        lvTagPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddYearbookPostActivity.this, SearchUserActivity.class);
                i.putExtra("Type","staff");
                startActivityForResult(i, IntentCode);
            }
        });

        //hash tag
        mEditTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.hashtag), null);
        mEditTextHashTagHelper.handle(edtPostMsg);

        //get Post (Add)
        if (getIntent().getExtras() != null) {
            CategoryId=getIntent().getStringExtra("CategoryId");
            getType = getIntent().getStringExtra("Type");
            CropType=getIntent().getIntExtra("CropType",2);


            if (getType.equalsIgnoreCase("Image")) {
                imgPostPhoto.setVisibility(View.VISIBLE);
                videoSelected.setVisibility(View.GONE);

                //for student portrait
                UserListItem UserData= (UserListItem) getIntent().getSerializableExtra("UserData");
                if(UserData!=null)
                {
                    //for chip view
//                    User user=new User();
//                    user.setUserId(UserData.getUserId());
//                    user.setUserFirstname(UserData.getUserFirstname());
//                    user.setUserLastname(UserData.getUserLastname());
//                    user.setSchoolId(UserData.getSchoolId());
//                    user.setUserImage(UserData.getUserImage());
//
//                    getUser.add(user);
//
//                    mChipsView.setVisibility(View.VISIBLE);
//                    lvTag.setVisibility(View.GONE);
//
//                    Contact c=new Contact(UserData.getUserFirstname(),UserData.getUserLastname(),UserData.getUserId(),"",Uri.parse(UserData.getUserImage()));
//                    mChipsView.addChip(UserData.getUserFirstname()+" "+UserData.getUserLastname(), Uri.parse(UserData.getUserImage()), c);

                    post_user_id=UserData.getUserId();
                    Glide.with(AddYearbookPostActivity.this)
                            .load(UserData.getUserSinglePhoto())
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(imgPostPhoto);

                    fileImage = new File(UserData.getUserSinglePhoto());

                    Log.d("TTT", "setView: file name: "+fileImage.getName());
                    RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage.getName());
                    part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);
                    rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());
                    CropType=3;
                    post_type=2;
                }
                else {
                    getUri = Uri.parse(getIntent().getStringExtra("AddPostImage"));
                    Log.d("TTT", "setView: Uri: " + getUri.getPath());
                    try {
                        thumbImage = MediaStore.Images.Media.getBitmap(getContentResolver(), getUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //imgPostPhoto.setImageBitmap(Bitmap.createScaledBitmap(thumbImage, 640, 640, false));
                    imgPostPhoto.setImageURI(getUri);

                    //image upload
                    //get file from uri
                    fileImage = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), getUri));

                    RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
                    part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);
                    rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());
                    post_type=1;
                }
                //set type
                type = RequestBody.create(MediaType.parse("text/plain"), "1");
            } else if (getType.equalsIgnoreCase("Video")) {
                imgPostPhoto.setVisibility(View.GONE);
                videoSelected.setVisibility(View.VISIBLE);

                getUri = Uri.parse(getIntent().getStringExtra("selectUri"));

//                videoSelected.reset();
//                videoSelected.setSource(getUri);

                //for video
                fileImage = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), getUri));
                RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
                part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);
                rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());

                //for thumbnail
                //get thumbnail
                Bitmap bMap = ThumbnailUtils.createVideoThumbnail(fileImage.getAbsolutePath(), MediaStore.Video.Thumbnails.MINI_KIND);
                fileVideo = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), AllMethods.getImageUri(getApplicationContext(), bMap)));
                RequestBody filePostThumb = RequestBody.create((MediaType.parse("*/*")), fileVideo);
                thumbPart = MultipartBody.Part.createFormData("post_thumbnail", fileVideo.getName(), filePostThumb);
                rbFileThumb = RequestBody.create(MediaType.parse("text/plain"), fileVideo.getName());


//                if (videoSelected.getCoverView() != null) {
//                    Glide.with(AddYearbookPostActivity.this)
//                            .load(bMap)
//                            .asBitmap()
//                            .placeholder(R.mipmap.home_post_img_placeholder)
//                            .error(R.mipmap.home_post_img_placeholder)
//                            .into(videoSelected.getCoverView());
//                }

                videoSelected.getVideoInfo().setPortraitWhenFullScreen(false);
                videoSelected.setVideoPath(getIntent().getStringExtra("selectUri"));
                videoSelected.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player
                videoSelected.getPlayer().start();

                //set type
                type = RequestBody.create(MediaType.parse("text/plain"), "2");
            } else {
                allMethods.setAlert(getApplicationContext(), getResources().getString(R.string.app_name), "Something wrong, please try again");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentCode && resultCode == RESULT_OK) {
            if (data.getExtras() != null) {
                user = (User) data.getSerializableExtra("SearchUser");

                mChipsView.setVisibility(View.VISIBLE);
                lvTag.setVisibility(View.GONE);
                if(getUser.size()>0)
                {
                    if(getUser.contains(user))
                    {
                        Log.d("TTT","Same record");
                    }
                    else
                    {
                        getUser.add(user);
                        Contact c=new Contact(user.getUserFirstname(),user.getUserLastname(),user.getUserId(),"",Uri.parse(user.getUserImage()));
                        mChipsView.addChip(user.getUserFirstname()+" "+user.getUserLastname(), Uri.parse(user.getUserImage()), c);
                    }
                }
                else {
                    getUser.add(user);
                    Contact c=new Contact(user.getUserFirstname(),user.getUserLastname(),user.getUserId(),"",Uri.parse(user.getUserImage()));
                    mChipsView.addChip(user.getUserFirstname()+" "+user.getUserLastname(), Uri.parse(user.getUserImage()), c);
                }
                Log.d("TTT", "Sizeee: " + getUser.size());
            }
        }
    }

    //set toolbar
    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Post Yearbook");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.next) {

            AllMethods.hideKeyboard(AddYearbookPostActivity.this,getCurrentFocus());
            AddPost();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (PlayerManager.getInstance().onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    //add post
    public void AddPost() {

        tagUser.clear();
        for(int i=0;i<getUser.size();i++)
        {
            tagUser.add(getUser.get(i).getUserId());
        }

        tagUserList="";
        if(tagUser.size()>0) {
             tagUserList = TextUtils.join(",", tagUser);
        }
        Log.d("TTT","tagUserList: "+tagUserList);

        //Progress bar
        progressDialog = ProgressDialog.show(AddYearbookPostActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(getResources().getColor(R.color.gray));
        progressBar.setIndeterminateDrawable(bounce);

        getMsg = edtPostMsg.getText().toString();
        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), tagUserList);
        RequestBody staffId = RequestBody.create(MediaType.parse("text/plain"), getStaffId);
        RequestBody schoolId = RequestBody.create(MediaType.parse("text/plain"), getSchoolId);
        RequestBody categoryId = RequestBody.create(MediaType.parse("text/plain"), CategoryId);
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), getMsg);
        RequestBody selectType = RequestBody.create(MediaType.parse("text/plain"), CropType+"");
        RequestBody postType=RequestBody.create(MediaType.parse("text/plain"),post_type+"");
        RequestBody postUserId=RequestBody.create(MediaType.parse("text/plain"),post_user_id);

        Log.d("TTT","staffPost: "+staffId+" / "+schoolId+" / "+categoryId+" / "+CropType);
        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<AddPost> login = retrofitClass.addStaffPost(userId, schoolId,staffId,categoryId, desc, part, rbFile, type, thumbPart, rbFileThumb,selectType,postUserId,postType);
        login.enqueue(new Callback<AddPost>() {
            @Override
            public void onResponse(Call<AddPost> call, Response<AddPost> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddYearbookPostActivity.this);
                    builder.setMessage(response.body().getResponseMsg())
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent i = getIntent();
                                    setResult(RESULT_OK, i);
                                    finish();
                                }
                            });

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    alert.setTitle("");
                    alert.show();
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(AddYearbookPostActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(AddYearbookPostActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(AddYearbookPostActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    allMethods.setAlert(AddYearbookPostActivity.this, "", response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<AddPost> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(AddYearbookPostActivity.this, getResources().getString(R.string.app_name), t.getMessage());
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selectimage, menu);
        MenuItem next = menu.findItem(R.id.next);
        next.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }
}

import React from 'react'
import { Platform } from 'react-native'
import { action, global } from '../utils'
import { postResponse, failerResponse } from '../utils/funtions'

//  get editor screen components list
export function get_screen_detail (screen_id, loader = true) {
  return async (dispatch, getState) => {
    if (loader) dispatch({ type: action.open_notification, message: 'Please wait...', variant: 'loader' })
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      screen_id,
      platform: Platform.OS === 'web' ? 'web' : 'app'
    }
    const res = await postResponse('get_screen_detail.php', request)
    if (res.Result === false) {
      dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: 'error' })
      return failerResponse('get_screen_detail', res)
    }
    if (loader) dispatch({ type: action.close_notification })
    return res.Data
  }
}

//  update screen components

export function add_screen_component (screen_id, name, data, snapshot) {
  return async (dispatch, getState) => {
    let errorStatus
    data.map((item) => {
      if (item.component_type === 2) {
        if (!item.component_url || item.component_url === '') errorStatus = item.component_order_by + '. Section image required!'
      }
      // else if (item.component_type === 0) {
      //     if (!item.component_title || item.component_title === "") errorStatus = item.component_order_by + ". Section title required!"
      //     if (!item.component_description || item.component_description === "") errorStatus = item.component_order_by + ". Section description required!"
      // }
      delete item.name
      return item
    })
    if (errorStatus) return { Result: false, ResponseMsg: errorStatus }
    dispatch({ type: action.open_notification, message: 'Please wait...', variant: 'loader' })
    const { authUser } = getState().auth
    data = JSON.stringify(data)
    const request = {
      Token: authUser,
      screen_id,
      name,
      component_array: data,
      snapshot: snapshot || ''
    }
    const res = await postResponse('add_screen_component.php', request)
    if (res.Result) {
      dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: 'success' })
    } else {
      dispatch({ type: action.open_notification, message: res.ResponseMsg || 'Something is wrong, please check again try', variant: 'error' })
      failerResponse('add_screen_component', res)
    }
    return res
  }
}

// upload pageInside Grid Thumb (categories, sub categories)

export function pageInsideThumb (file, screen_id) {
  return async (dispatch, getState) => {
    dispatch({ type: action.open_notification, message: 'Thumbnail uploading...', variant: 'loader' })
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      screen_id,
      image: file
    }
    const res = await postResponse('add_page_grid_image.php', request)
    if (res.Result === false) {
      dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: 'error' })
      failerResponse('add_page_grid_image', res)
    } else {
      dispatch({ type: action.close_notification })
    }
    return res
  }
}

export function sectionLike (component_id) {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      component_id,
      platform: Platform.OS === 'web' ? 'web' : 'app'
    }
    const res = await postResponse('add_component_like_unlike.php', request)
    return res
  }
}

export function sectionBookmark (component_id, screenId) {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      component_id,
      platform: Platform.OS === 'web' ? 'web' : 'app'
    }
    const res = await postResponse('add_component_bookmark_unbookmark.php', request)
    if (res.Result) { dispatch(get_screen_detail(screenId)) }
    return res
  }
}

export function get_pageinside_list (screen_id) {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      screen_id,
      platform: global.platform
    }
    const res = await postResponse('get_pageinside_list.php', request)
    if (res.Result) {
      dispatch({
        type: action.page_inside_list,
        payload: res.Data
      })
    }
    return res.Data
  }
}

export function get_user_images (grade) {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      platform: global.platform,
      grade
    }
    const res = await postResponse('get_user_images.php', request)
    return res.Data
  }
}

export function get_users_info ({ student_list, staff_list }) {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      platform: global.platform,
      student_list,
      staff_list
    }
    const res = await postResponse('get_users_info.php', request)
    if (!res || !res.Result) return
    const data = res.Data
    return [...data.staff_list, ...data.student_list]
  }
}

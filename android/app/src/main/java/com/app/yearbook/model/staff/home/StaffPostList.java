
package com.app.yearbook.model.staff.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StaffPostList {

    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("post_description")
    @Expose
    private String postDescription;
    @SerializedName("post_file_type")
    @Expose
    private String postFileType;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("post_file")
    @Expose
    private String postFile;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("user_firstname")
    @Expose
    private String userFirstname;
    @SerializedName("user_lastname")
    @Expose
    private String userLastname;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("post_thumbnail")
    @Expose
    private String postThumbnail;
    @SerializedName("total_comment")
    @Expose
    private String totalComment;
    @SerializedName("total_repoted")
    @Expose
    private String totalRepoted;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getPostDescription() {
        return postDescription;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
    }

    public String getPostFileType() {
        return postFileType;
    }

    public void setPostFileType(String postFileType) {
        this.postFileType = postFileType;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getPostFile() {
        return postFile;
    }

    public void setPostFile(String postFile) {
        this.postFile = postFile;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getPostThumbnail() {
        return postThumbnail;
    }

    public void setPostThumbnail(String postThumbnail) {
        this.postThumbnail = postThumbnail;
    }

    public String getTotalComment() {
        return totalComment;
    }

    public void setTotalComment(String totalComment) {
        this.totalComment = totalComment;
    }

    public String getTotalRepoted() {
        return totalRepoted;
    }

    public void setTotalRepoted(String totalRepoted) {
        this.totalRepoted = totalRepoted;
    }

}

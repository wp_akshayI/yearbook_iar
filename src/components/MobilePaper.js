import React from 'react';
import { TouchableOpacity, Text, ImageBackground } from "react-native-web";
import Paper from '@material-ui/core/Paper';
import { withRouter } from "react-router-dom";

function MobilePaper({ pageInside, data, history, children, navigate, location, ...others }) {

    const _navigateEditor = () => {
        if (!data || !navigate) return;
        const { screen_id, name, category_id } = data;
        let search = "_id=" + screen_id + "&_name=" + name;
        if (location.pathname !== "/app/dashboard/sub-categories")
            search = search + "&_category=" + category_id;
        return history.push({ pathname: "/app/page-editor", search })
    }

    const viewButton = (
        <TouchableOpacity
            style={{ backgroundColor: "#000", width: 50, height: 50, borderRadius: 25, position: "absolute", alignItems: "center", justifyContent: "center", bottom: 20, right: 20, display: "flex" }}
            activeOpacity={1}
            onClick={() => history.push({ pathname: "/app/dashboard/sub-categories", search: "_id=" + data.category_id + "&name=" + data.name })}
        >
            <Text style={{ color: "#fff", textAlign: "center", fontSize: "75%", fontFamily: "calibri" }}>Page Inside</Text>
        </TouchableOpacity>
    )

    return (
        <Paper
            elevation={3}
            style={{ overflow: "hidden" }}
        >
            <ImageBackground
                style={{ height: 360, width: 220 }}
                resizeMode='stretch'
                {...others}
                onClick={_navigateEditor}
            >
                {children}
                {pageInside && viewButton}
            </ImageBackground>
        </Paper>
    )
}

export default withRouter(MobilePaper);
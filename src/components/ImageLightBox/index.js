import React, { useEffect, useState } from 'react'
import { Image, View, Text, StyleSheet, TouchableOpacity, SafeAreaView } from 'react-native'
import { global, colors } from '../../utils'
import ImageScal from '../../utils/ImageScal'
import Modal from 'react-native-modal'
import { cancelIcon } from '../../assets'

export default function ImageLightBox ({ isVisible, handleClose, uri, title }) {
  const [height, setHeight] = useState(0)
  useEffect(() => {
    _imageScal()
  }, [uri])

  const _imageScal = async () => {
    if (uri) {
      const { height } = await ImageScal(uri)
      setHeight(height)
    } else {
      setHeight(global.height * 0.6)
    }
  }

  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={handleClose}
      onBackButtonPress={handleClose}
      animationIn='fadeIn'
      style={{ margin: 0 }}
    //   swipeDirection={['up', 'down']}
    //   onSwipeComplete={handleClose}
    >
      <View style={styles.rootViewStyle}>
        <Image
          source={{ uri }}
          resizeMode='contain'
          style={{ width: global.width, height }}
        />
        <View style={styles.titleViewStyle}>
          <Text style={styles.titleStyle}>{title}</Text>
        </View>
          <SafeAreaView style={{ position: 'absolute', top: 10, right: 10 }}>
              <TouchableOpacity
                style={styles.closeButtonStyle}
                onPress={handleClose}
              >
                <Image source={cancelIcon} style={styles.closeIcon} />
              </TouchableOpacity>
          </SafeAreaView>
        
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  rootViewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleViewStyle: {
    backgroundColor: colors.white,
    width: '100%',
    padding: 20
  },
  titleStyle: {
    fontSize: 16,
    textAlign: 'center',
    color: colors.black
  },
  closeButtonStyle: {
//    position: 'absolute',
    backgroundColor: colors.black,
    padding: 10,
    borderRadius: 30,
//    top: 10,
//    right: 10
  },
  closeIcon: {
    tintColor: colors.white,
    resizeMode: 'contain',
    height: 12,
    width: 12
  }
})

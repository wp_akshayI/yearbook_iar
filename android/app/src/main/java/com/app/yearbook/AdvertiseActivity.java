package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.AdvertisementPostAdapter;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.staff.GetStaffPostDEtail;
import com.app.yearbook.model.staff.livefeed.PostListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdvertiseActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public static RecyclerView rvAdvertisement;
    private LinearLayout lvNoData;
    private AllMethods allMethods;
    public ProgressDialog progressDialog;
    private ArrayList<PostListItem> getPost;
    private LoginUser loginUser;
    private int schoolId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertise);
        setView();
    }

    public void setView()
    {
        //SP object
        loginUser = new LoginUser(AdvertiseActivity.this);
        if (loginUser.getUserData() != null) {
            schoolId=Integer.parseInt(loginUser.getUserData().getSchoolId());
        }

        //toolbar
        setToolbar();

        //all methods object
        allMethods = new AllMethods();

        //lv no
        lvNoData =findViewById(R.id.lvNoData);

        //rv
        rvAdvertisement = findViewById(R.id.rvAdvertisement);

        getAllAdvertisement(schoolId);

    }

    //set toolbar
    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Advertisement");
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //get advertisment
    public void getAllAdvertisement(int sid)
    {
        //Progress bar
        progressDialog = ProgressDialog.show(AdvertiseActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetStaffPostDEtail> advertisement = retrofitClass.getAdvertise(sid);
        advertisement.enqueue(new Callback<GetStaffPostDEtail>() {
            @Override
            public void onResponse(Call<GetStaffPostDEtail> call, Response<GetStaffPostDEtail> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if(response.body().getResponseCode().equals("1"))
                {
                    if(response.body().getPostList().size()>0)
                    {
                        getPost=new ArrayList<>();
                        rvAdvertisement.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);

                        getPost.addAll(response.body().getPostList());
                        GridLayoutManager mLayoutManager = new GridLayoutManager(AdvertiseActivity.this, 2);
                        rvAdvertisement.setLayoutManager(mLayoutManager);

                        AdvertisementPostAdapter mAdapter = new AdvertisementPostAdapter(getPost, AdvertiseActivity.this, new AdvertisementPostAdapter.ListAdapterListener() {
                            @Override
                            public void onClickAtOKButton(final int position) {

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AdvertiseActivity.this);
                                LayoutInflater inflater = getLayoutInflater();
                                final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
                                dialogBuilder.setView(dialogView);
                                final AlertDialog alert = dialogBuilder.create();
                                alert.setCancelable(false);
                                alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                                TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
                                tvMessage.setText("Are you sure you want to remove the advertisement?");

                                TextView tvYes = dialogView.findViewById(R.id.tvYes);
                                TextView tvNo = dialogView.findViewById(R.id.tvNo);

                                tvYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        alert.dismiss();
                                        deleteAdvertisement(position);

                                    }
                                });

                                tvNo.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                    }
                                });
                                alert.show();
                            }
                        });
                        rvAdvertisement.setAdapter(mAdapter);
                    }
                }
                else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(AdvertiseActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(AdvertiseActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(AdvertiseActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    rvAdvertisement.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<GetStaffPostDEtail> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(AdvertiseActivity.this, "", t.getMessage() + "");
            }
        });
    }

    //delete advertismnet from id
    public void deleteAdvertisement(final int id)
    {
        //Progress bar
        progressDialog = ProgressDialog.show(AdvertiseActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> deleteAdvertisement = retrofitClass.deleteAdvertise(id);
        deleteAdvertisement.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if(response.body().getResponseCode().equals("1"))
                {
                    for (int i = 0; i < getPost.size(); i++) {
                        if (getPost.get(i).getPostId().equals(String.valueOf(id))) {
                            Log.d("TTT", "Remove " + id + " / " + i);
                            getPost.remove(i);
                            rvAdvertisement.getAdapter().notifyItemRemoved(i);
                        }
                    }

                    if(getPost.size()==0)
                    {
                        lvNoData.setVisibility(View.VISIBLE);
                        rvAdvertisement.setVisibility(View.GONE);
                    }
                }
                else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(AdvertiseActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(AdvertiseActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(AdvertiseActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    allMethods.setAlert(AdvertiseActivity.this, "", response.body().getResponseMsg()+ "");
                }
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(AdvertiseActivity.this, "", t.getMessage() + "");

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_advertisement, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        else if(item.getItemId()==R.id.advertisement)
        {
            Intent i=new Intent(AdvertiseActivity.this,AddAdvertisementActivity.class);
            startActivityForResult(i,222);

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==222 && resultCode==RESULT_OK) {
            getAllAdvertisement(schoolId);
        }
    }

}

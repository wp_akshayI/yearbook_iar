package com.app.yearbook.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.makeramen.roundedimageview.RoundedImageView;
import com.app.yearbook.AddSignatureActivity;
import com.app.yearbook.R;
import com.app.yearbook.UserDetailActivity;
import com.app.yearbook.model.staff.yearbook.UserTagg;

import java.util.ArrayList;

public class TaggStudentUserListAdapter extends RecyclerView.Adapter<TaggStudentUserListAdapter.MyViewHolder> {
    private ArrayList<UserTagg> userLists;
    private Activity ctx;
    private String type;

    @NonNull
    @Override
    public TaggStudentUserListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tagstudentposts_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final TaggStudentUserListAdapter.MyViewHolder holder, final int position) {
        final UserTagg userList = userLists.get(position);
        holder.tvUserId.setText(userList.getUserId() + "");
        // holder.tvUserName.setText(userList.getUserFirstname());
        Glide.with(ctx)
                .load(userList.getUserImage())
                .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.profile_placeholder)
                .error(R.mipmap.profile_placeholder)
                .into(holder.imgUserPhoto);

        if (type.equalsIgnoreCase("studentyearbook")) {
            holder.imgUserPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setView(userList);
                }
            });

        } else {
            holder.imgUserPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, UserDetailActivity.class);
                    i.putExtra("Photo", userList.getUserImage());
                    i.putExtra("type", type);
                    i.putExtra("Id", userList.getUserId());
                    i.putExtra("schoolId", userList.getSchoolId());
                    i.putExtra("Name", userList.getUserFirstname() + " " + userList.getUserLastname());
                    ctx.startActivity(i);
                }
            });
        }
    }

    public TaggStudentUserListAdapter(ArrayList<UserTagg> userLists, Activity context, String type) {
        this.userLists = userLists;
        this.ctx = context;
        this.type = type;
    }

    @Override
    public int getItemCount() {
        return userLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvUserId, tvUserName;
        public RoundedImageView imgUserPhoto;

        public MyViewHolder(View view) {
            super(view);
            tvUserId = view.findViewById(R.id.tvUserId);
            tvUserName = view.findViewById(R.id.tvUserName);
            imgUserPhoto = view.findViewById(R.id.imgUserImage);

        }
    }

    public void setView(final UserTagg usertagg) {
        androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(ctx);
        LayoutInflater inflater = ctx.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_alertbox_userprofile, null);
        dialogBuilder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog alert = dialogBuilder.create();
        alert.setCancelable(false);
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        TextView tvName = dialogView.findViewById(R.id.tvName);
        tvName.setText(usertagg.getUserFirstname() + " " + usertagg.getUserLastname());

        tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                Intent i = new Intent(ctx, UserDetailActivity.class);
                i.putExtra("Photo", usertagg.getUserImage());
                i.putExtra("type", type);
                i.putExtra("Id", usertagg.getUserId());
                i.putExtra("schoolId", usertagg.getSchoolId());
                i.putExtra("Name", usertagg.getUserFirstname() + " " + usertagg.getUserLastname());
                ctx.startActivity(i);
            }
        });

        LinearLayout lvSign = dialogView.findViewById(R.id.lvSign);

        TextView tvSign = dialogView.findViewById(R.id.tvSign);
        Typeface typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/Brasileirinha Personal Use.ttf");
        tvSign.setTypeface(typeface);

        lvSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ctx, AddSignatureActivity.class);
                i.putExtra("searchUserId", usertagg.getUserId());
                ctx.startActivity(i);
            }
        });
        ImageView imgUserPhoto = dialogView.findViewById(R.id.imgUserPhoto);
        Glide.with(ctx)
                .load(usertagg.getUserImage())
                .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.home_post_img_placeholder)
                .error(R.mipmap.home_post_img_placeholder)
                .into(imgUserPhoto);

        ImageView imgBack = dialogView.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        imgUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                Intent i = new Intent(ctx, UserDetailActivity.class);
                i.putExtra("Photo", usertagg.getUserImage());
                i.putExtra("type", type);
                i.putExtra("Id", usertagg.getUserId());
                i.putExtra("schoolId", usertagg.getSchoolId());
                i.putExtra("Name", usertagg.getUserFirstname() + " " + usertagg.getUserLastname());
                ctx.startActivity(i);
            }
        });

        alert.show();


    }
}

package com.app.yearbook.model.staff;

import com.google.gson.annotations.SerializedName;

public class StaffProfile {

    @SerializedName("staff_image")
    private String staffImage;

    @SerializedName("staff_token")
    private String staffToken;

    @SerializedName("staff_lastname")
    private String staffLastname;

    @SerializedName("school_name")
    private String schoolName;

    @SerializedName("staff_firstname")
    private String staffFirstname;

    @SerializedName("staff_request_status")
    private String staffRequestStatus;

    @SerializedName("school_end_date")
    private String schoolEndDate;

    @SerializedName("school_start_date")
    private String schoolStartDate;

    @SerializedName("staff_access_code")
    private String staffAccessCode;

    @SerializedName("staff_phone")
    private String staffPhone;

    @SerializedName("user_type")
    private String userType;

    @SerializedName("school_id")
    private String schoolId;

    @SerializedName("staff_is_agree")
    private String staffIsAgree;

    @SerializedName("staff_id")
    private String staffId;

    @SerializedName("staff_device_type")
    private String staffDeviceType;

    @SerializedName("staff_device_token")
    private String staffDeviceToken;

    @SerializedName("staff_password")
    private String staffPassword;

    @SerializedName("staff_email")
    private String staffEmail;

    public void setStaffImage(String staffImage) {
        this.staffImage = staffImage;
    }

    public String getStaffImage() {
        return staffImage;
    }

    public void setStaffToken(String staffToken) {
        this.staffToken = staffToken;
    }

    public String getStaffToken() {
        return staffToken;
    }

    public void setStaffLastname(String staffLastname) {
        this.staffLastname = staffLastname;
    }

    public String getStaffLastname() {
        return staffLastname;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setStaffFirstname(String staffFirstname) {
        this.staffFirstname = staffFirstname;
    }

    public String getStaffFirstname() {
        return staffFirstname;
    }

    public void setStaffRequestStatus(String staffRequestStatus) {
        this.staffRequestStatus = staffRequestStatus;
    }

    public String getStaffRequestStatus() {
        return staffRequestStatus;
    }

    public void setSchoolEndDate(String schoolEndDate) {
        this.schoolEndDate = schoolEndDate;
    }

    public String getSchoolEndDate() {
        return schoolEndDate;
    }

    public void setSchoolStartDate(String schoolStartDate) {
        this.schoolStartDate = schoolStartDate;
    }

    public String getSchoolStartDate() {
        return schoolStartDate;
    }

    public void setStaffAccessCode(String staffAccessCode) {
        this.staffAccessCode = staffAccessCode;
    }

    public String getStaffAccessCode() {
        return staffAccessCode;
    }

    public void setStaffPhone(String staffPhone) {
        this.staffPhone = staffPhone;
    }

    public String getStaffPhone() {
        return staffPhone;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserType() {
        return userType;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setStaffIsAgree(String staffIsAgree) {
        this.staffIsAgree = staffIsAgree;
    }

    public String getStaffIsAgree() {
        return staffIsAgree;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffDeviceType(String staffDeviceType) {
        this.staffDeviceType = staffDeviceType;
    }

    public String getStaffDeviceType() {
        return staffDeviceType;
    }

    public void setStaffDeviceToken(String staffDeviceToken) {
        this.staffDeviceToken = staffDeviceToken;
    }

    public String getStaffDeviceToken() {
        return staffDeviceToken;
    }

    public void setStaffPassword(String staffPassword) {
        this.staffPassword = staffPassword;
    }

    public String getStaffPassword() {
        return staffPassword;
    }

    public void setStaffEmail(String staffEmail) {
        this.staffEmail = staffEmail;
    }

    public String getStaffEmail() {
        return staffEmail;
    }
}
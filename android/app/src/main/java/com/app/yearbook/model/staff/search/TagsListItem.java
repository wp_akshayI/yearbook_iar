package com.app.yearbook.model.staff.search;

import com.google.gson.annotations.SerializedName;

public class TagsListItem{

	@SerializedName("total")
	private String total;

	@SerializedName("post_description")
	private String postDescription;

	public void setTotal(String total){
		this.total = total;
	}

	public String getTotal(){
		return total;
	}

	public void setPostDescription(String postDescription){
		this.postDescription = postDescription;
	}

	public String getPostDescription(){
		return postDescription;
	}
}
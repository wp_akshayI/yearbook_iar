import React from "react"
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import { makeStyles } from '@material-ui/core/styles';

function PageTitle(props) {
  // const dispatch = useDispatch();
  // let pageTitle = useSelector(({ yearbook }) => yearbook.pageTitle)
  // let categoryPageTitle = useSelector(({ category }) => category.categoryPageTitle)
  const classes = useStyles();
  // let title = type === "yearbook" ? pageTitle : categoryPageTitle

  return (
    <div style={{ display: "flex", alignItems: "center",  }}>
      <Typography className={classes.title} variant="subtitle2" noWrap>
        Name of Page:
      </Typography>
      <div className={classes.search}>
        <InputBase
          placeholder="Untitled Page"
          // onChange={(e) => dispatch(Actions.setPageTitle(e.target.value))}
          // value={pageTitle}
          classes={{
            root: classes.inputRoot,
            input: classes.inputInput,
          }}
          style={{color: "#fff"}}
          {...props}
        />
      </div>
      <div className={classes.grow} />
    </div>
  )
}


export default PageTitle;

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },

  title: {
    display: 'none',
    color: "#b3b3b3",
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },

  search: {
    position: 'relative',
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
    borderBottomWidth: 1,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderStyle: "solid"
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 1),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 300,
    },
  }
}));
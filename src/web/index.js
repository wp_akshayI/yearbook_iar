import React from 'react'
import { Route, Switch } from 'react-router-dom'

import Dashboard from './dashboard'
import Editor from './Editor'

export default function App (props) {
  // const _backConfirm = async () => {
  //     const { history, location, match } = props;
  //     if (!window.isEditor) {
  //         Swal.fire({
  //             title: 'Are you sure?',
  //             text: 'Confirm that your changes have been saved before leaving the page!',
  //             icon: 'warning',
  //             showCancelButton: true,
  //             confirmButtonText: 'Leave',
  //             cancelButtonText: 'Cancel'
  //         }).then((result) => {
  //             if (result.value) {
  //                 history.goBack();
  //                 window.isEditor = true;
  //             } else {
  //                 window.history.pushState(null, document.title, location.href);
  //             }
  //         });
  //     }

  // }

  return (
    <Switch>
      <Route path='/app/dashboard' component={Dashboard} />
      <Route
        path='/app/page-editor'
        component={Editor}
      />
    </Switch>
  )
}

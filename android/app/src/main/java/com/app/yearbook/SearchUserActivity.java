package com.app.yearbook;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.StaffPostSearchUserAdapter;
import com.app.yearbook.model.search.User;
import com.app.yearbook.model.search.getPeople;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUserActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private LoginUser loginUser;
    private String userId="";
    public int schoolId;
    private RecyclerView rvSearchUser;
    private LinearLayout lvNoData;
    private androidx.appcompat.widget.SearchView searchView;
    private ArrayList<User> getUser;
    public ProgressDialog progressDialog;
    public AllMethods utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);

        setView();
    }

    public void setView()
    {
        //toolbar
        setToolbar();

        loginUser = new LoginUser(getApplicationContext());
        if (loginUser.getUserData() != null) {
            schoolId = Integer.parseInt(loginUser.getUserData().getSchoolId());

            if(getIntent().getExtras()!=null)
            {
                String type=getIntent().getStringExtra("Type");

                if(type.equalsIgnoreCase("student"))
                {
                    //userId=loginUser.getUserData().getUserId();
                    userId=loginUser.getUserData().getStaffId();
                }
            }

        }

        //utils
        utils = new AllMethods();

        //rv
        rvSearchUser = findViewById(R.id.rvSearchPeople);

        //lv
        lvNoData = findViewById(R.id.lvNoData);

    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Search user");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.searchview, menu);
        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        searchView = (androidx.appcompat.widget.SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Search by username");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!query.equals("")) {
                    searchView.clearFocus();
                    InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(SearchUserActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    searchPeople(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    public void searchPeople(String user) {
        progressDialog = ProgressDialog.show(SearchUserActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<getPeople> userPost = retrofitClass.searchStudent(user, "people",userId,String.valueOf(schoolId));
        userPost.enqueue(new Callback<getPeople>() {
            @Override
            public void onResponse(Call<getPeople> call, Response<getPeople> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    getUser = new ArrayList<>();
                    if (response.body().getUserList().size() > 0) {
                        rvSearchUser.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        getUser.addAll(response.body().getUserList());
                        StaffPostSearchUserAdapter searchUserAdapter = new StaffPostSearchUserAdapter(getUser, SearchUserActivity.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchUserActivity.this);
                        rvSearchUser.setLayoutManager(mLayoutManager);
                        rvSearchUser.setAdapter(searchUserAdapter);
                    } else {
                        rvSearchUser.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(SearchUserActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(SearchUserActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(SearchUserActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    rvSearchUser.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                    // utils.setAlert(getActivity(),"",response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<getPeople> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                utils.setAlert(SearchUserActivity.this, "", t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

package com.app.yearbook.model.postmodels;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.app.yearbook.BR;
import com.app.yearbook.util.AppUtil;
import com.app.yearbook.utils.Constants;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.List;


public class PostListItem extends BaseObservable implements Parcelable {

    @SerializedName("staff_image")
    private String staffImage;

    @SerializedName("post_title")
    private String postTitle;

    @SerializedName("total_repoted")
    private String totalRepoted;

    @SerializedName("total_like")
    private String totalLike;

    @SerializedName("staff_lastname")
    private String staffLastname;

    @SerializedName("post_file")
    private String postFile;

    @SerializedName("total_comment")
    private String totalComment;

    @SerializedName("staff_firstname")
    private String staffFirstname;

    @SerializedName("post_description")
    private String postDescription;

    @SerializedName("post_file_type")
    private String postFileType;

    @SerializedName("post_id")
    private String postId;

    @SerializedName("school_id")
    private String schoolId;

    @SerializedName("post_date")
    private String postDate;

    @SerializedName("staff_id")
    private String staffId;

    @SerializedName("post_thumbnail")
    private String postThumbnail;

    @SerializedName("post_type")
    private String postType;

    @SerializedName("post_sub_title")
    private String postSubTitle;

    @SerializedName("post_bookmark")
    private String postBookmark;

    @SerializedName("user_tagg")
    private List<Object> userTagg;

    @SerializedName("post_staff_id")
    private String postStaffId;

    @SerializedName("post_expiry_date")
    private String postExpiryDate;

    @SerializedName("post_for_grade")
    private String postForGrade;

    @SerializedName("post_like")
    private String postLike;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("user_firstname")
    private String userFirstName;

    @SerializedName("user_lastname")
    private String userLastName;

    @SerializedName("user_image")
    private String userImage;

    @SerializedName("user_type")
    private String userType;

    @SerializedName("flag")
    private String flag;

    public static final Creator<PostListItem> CREATOR = new Creator<PostListItem>() {
        @Override
        public PostListItem createFromParcel(Parcel in) {
            return new PostListItem(in);
        }

        @Override
        public PostListItem[] newArray(int size) {
            return new PostListItem[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Bindable
    public boolean getPostLike() {
        if (postLike != null) {
            return postLike.equalsIgnoreCase("True");
        } else {
            return false;
        }
    }

    public void setPostLike(String postLike) {
        this.postLike = postLike;
        notifyPropertyChanged(BR.postLike);
    }
/*@Bindable
    public boolean getIsBookmarked() {
        return postBookmark.equalsIgnoreCase("True");
    }*/


    public String getPostForGrade() {
        return postForGrade;
    }

    public void setPostForGrade(String postForGrade) {
        this.postForGrade = postForGrade;
    }

    public String getPostExpiryDate() {
        return postExpiryDate;
    }

    public String getRemainingDays() {
        return Constants.getDaysLeft(postExpiryDate);
    }

    public boolean isDaysLeft() {
        return Constants.isDaysLeft(postExpiryDate);
    }

    public void setPostExpiryDate(String postExpiryDate) {
        this.postExpiryDate = postExpiryDate;
    }

    @SerializedName("poll_options")
    private List<PollOption> pollOptions;

    public List<PollOption> getPollOptions() {
        return pollOptions;
    }

    public void setPollOptions(List<PollOption> pollOptions) {
        this.pollOptions = pollOptions;
    }


    public void setStaffImage(String staffImage) {
        this.staffImage = staffImage;
    }

    public String getStaffImage() {
        return staffImage;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostTitle() {
        return AppUtil.getDecodeUTF8String(postTitle);
    }

    public void setTotalRepoted(String totalRepoted) {
        this.totalRepoted = totalRepoted;
    }

    public String getTotalRepoted() {
        return totalRepoted;
    }

    public void setTotalLike(String totalLike) {
        this.totalLike = totalLike;
        notifyPropertyChanged(BR.totalLike);
    }

    @Bindable
    public String getTotalLike() {
        if (totalLike != null) {
            return Constants.format(Long.parseLong(totalLike));
        } else {
            return "0";
        }
    }

    public void setStaffLastname(String staffLastname) {
        this.staffLastname = staffLastname;
    }

    public String getStaffLastname() {
        return staffLastname;
    }

    public void setPostFile(String postFile) {
        this.postFile = postFile;
    }

    public String getPostFile() {
        return postFile;
    }

    public void setTotalComment(String totalComment) {
        this.totalComment = totalComment;
    }

    public String getTotalComment() {
        return totalComment;
    }

    public void setStaffFirstname(String staffFirstname) {
        this.staffFirstname = staffFirstname;
    }

    public String getStaffFirstname() {
        return staffFirstname;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
    }

    public String getPostDescription() {
        return AppUtil.getDecodeUTF8String(postDescription);
    }

    public void setPostFileType(String postFileType) {
        this.postFileType = postFileType;
    }

    public String getPostFileType() {
        return postFileType;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostId() {
        return postId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getPostDate() {
        return Constants.getDisplayDate(Constants.convertDateFormat(postDate,
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), Constants.simpleDateFormat),
                Constants.simpleDateFormat);
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setPostThumbnail(String postThumbnail) {
        this.postThumbnail = postThumbnail;
    }

    public String getPostThumbnail() {
        return postThumbnail;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getPostType() {
        if (getFlag() != null && getFlag().equalsIgnoreCase("people")) {
            setPostType("5");
        }else if (getFlag() != null && getFlag().equalsIgnoreCase("staff")) {
            setPostType("6");
        }
        return postType;
    }

    public void setPostSubTitle(String postSubTitle) {
        this.postSubTitle = postSubTitle;
    }

    public String getPostSubTitle() {
        return AppUtil.getDecodeUTF8String(postSubTitle);
    }

    public void setPostBookmark(String postBookmark) {
        this.postBookmark = postBookmark;
        notifyPropertyChanged(BR.postBookmark);
    }

    @Bindable
    public boolean getPostBookmark() {
        if (postBookmark != null) {
            return postBookmark.equalsIgnoreCase("True");
        } else {
            return false;
        }
    }

    public void setUserTagg(List<Object> userTagg) {
        this.userTagg = userTagg;
    }

    public List<Object> getUserTagg() {
        return userTagg;
    }

    public void setPostStaffId(String postStaffId) {
        this.postStaffId = postStaffId;
    }

    public String getPostStaffId() {
        return postStaffId;
    }

    public int getPostTypeInt() {
        return Integer.parseInt(postFileType);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(staffImage);
        dest.writeString(postTitle);
        dest.writeString(totalRepoted);
        dest.writeString(totalLike);
        dest.writeString(staffLastname);
        dest.writeString(postFile);
        dest.writeString(totalComment);
        dest.writeString(staffFirstname);
        dest.writeString(postDescription);
        dest.writeString(postFileType);
        dest.writeString(postId);
        dest.writeString(schoolId);
        dest.writeString(postDate);
        dest.writeString(staffId);
        dest.writeString(postThumbnail);
        dest.writeString(postType);
        dest.writeString(postSubTitle);
        dest.writeString(postBookmark);
        dest.writeString(postStaffId);
        dest.writeString(postExpiryDate);
        dest.writeString(postForGrade);
        dest.writeString(postLike);
        dest.writeString(userId);
        dest.writeString(userFirstName);
        dest.writeString(userLastName);
        dest.writeString(userImage);
        dest.writeString(userType);
        dest.writeString(flag);
        dest.writeTypedList(pollOptions); }
    protected PostListItem(Parcel in) {
        staffImage = in.readString();
        postTitle = in.readString();
        totalRepoted = in.readString();
        totalLike = in.readString();
        staffLastname = in.readString();
        postFile = in.readString();
        totalComment = in.readString();
        staffFirstname = in.readString();
        postDescription = in.readString();
        postFileType = in.readString();
        postId = in.readString();
        schoolId = in.readString();
        postDate = in.readString();
        staffId = in.readString();
        postThumbnail = in.readString();
        postType = in.readString();
        postSubTitle = in.readString();
        postBookmark = in.readString();
        postStaffId = in.readString();
        postExpiryDate = in.readString();
        postForGrade = in.readString();
        postLike = in.readString();
        userId = in.readString();
        userFirstName = in.readString();
        userLastName = in.readString();
        userImage = in.readString();
        userType = in.readString();
        flag = in.readString();
        pollOptions = in.createTypedArrayList(PollOption.CREATOR);
    }
}
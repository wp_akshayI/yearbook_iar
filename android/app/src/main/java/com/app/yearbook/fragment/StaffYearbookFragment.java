package com.app.yearbook.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.adapter.StaffYearListAdapter;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.yearbook.BookListModelForYearbook;
import com.app.yearbook.model.yearbook.YearbookList;
import com.app.yearbook.model.yearbook.getYearBook;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffYearbookFragment extends Fragment {

    public View view;
    private RecyclerView rvYearbook;
    private LinearLayout lvNoData;
    public ProgressDialog progressDialog;
    private LoginUser loginUser;
    private String userId, schoolId,userPwd;
    private AllMethods allMethods;
    private ArrayList<YearbookList> yearbookLists;
    private ArrayList<BookListModelForYearbook> bookList;

    private EditText edtOldPassword, edtNewPassword, edtConfirmPassword;
    private TextInputLayout input_layout_newpassword, input_layout_confirmpassword, input_layout_oldpassword;

    public StaffYearbookFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       // ((StaffHomeActivity) getActivity()).getSupportActionBar().setTitle("Class of");
        // Inflate the layout for this fragment
        //fragment_yearbook
        view = inflater.inflate(R.layout.fragment_yearbook, container, false);
        setView();

        //setYearbookView();
        return view;
    }

//    public void setYearbookView()
//    {
//
//        //common methods
//        allMethods = new AllMethods();
//
//        //SP
//        loginUser = new LoginUser(getActivity());
//        if (loginUser.getUserData() != null) {
//            userId = loginUser.getUserData().getStaffId();
//            schoolId = loginUser.getUserData().getSchoolId();
//        }
//
//        getYearBookTest();
//    }
//
//    public void getYearBookTest() {
//        //Progress bar
//        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
//        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        progressDialog.setContentView(R.layout.progress_view);
//        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
//        Circle bounce = new Circle();
//        bounce.setColor(Color.BLACK);
//        progressBar.setIndeterminateDrawable(bounce);
//
//        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
//        final Call<getYearBook> getYearBook = retrofitClass.getYearbook(0,Integer.parseInt(schoolId),"yearbook");
//        getYearBook.enqueue(new Callback<com.weapplinse.yearbook.model.yearbook.getYearBook>() {
//            @Override
//            public void onResponse(Call<getYearBook> call, Response<getYearBook> response) {
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//                if (response.body().getResponseCode().equals("1")) {
//                    if (response.body().getYearbookList().size() > 0) {
//                        yearbookLists=new ArrayList<>();
//                        yearbookLists.addAll(response.body().getYearbookList());
//
//                        //setTableLayout();
//                    }
//                } else {
//                    allMethods.setAlert(getActivity(), "", response.body().getResponseMsg() + "");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<getYearBook> call, Throwable t) {
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
//            }
//        });
//    }

//    public void setTableLayout()
//    {
//        int numRow = (int) Math.ceil(yearbookLists.size()/3);
//        int numCol = 3;
//
//        Log.d("TTT","Array size: "+yearbookLists.size()+" / row: "+numRow);
//        TableLayout tblLayout = view.findViewById(R.id.tblLayout);
//
//        for(int i = 0; i < numRow; i++) {
////            HorizontalScrollView HSV = new HorizontalScrollView(getActivity());
////            HSV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
////                    LinearLayout.LayoutParams.WRAP_CONTENT));
//
//            TableRow tblRow = new TableRow(getActivity());
//            tblRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
//            tblRow.setBackgroundResource(R.mipmap.book_list_bg_img);
//
//            for(int j = 0; j < numCol; j++)
//            {
//                ImageView imageView = new ImageView(getActivity());
//               // imageView.setLayoutParams(new LinearLayout.LayoutParams(140,200));
//               // imageView.setLayoutParams(new LinearLayout.LayoutParams(140, 200));
//                //imageView.setPadding(0,90,0,30);
//                imageView.setImageResource(R.mipmap.ic_launcher);
//
//                TextView textView = new TextView(getActivity());
//                textView.setText("Java Tester");
//                textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT));
//
//                tblRow.addView(imageView,j);
//            }
//
//            //HSV.addView(tblRow);
//            tblLayout.addView(tblRow, i);
//        }
//    }


    public void setView() {
        //common methods
        allMethods = new AllMethods();

        //SP
        loginUser = new LoginUser(getActivity());
        if (loginUser.getUserData() != null) {
            userId = loginUser.getUserData().getStaffId();
            schoolId = loginUser.getUserData().getSchoolId();
            userPwd = loginUser.getUserData().getStaffPassword();
        }

        //rv
        rvYearbook = view.findViewById(R.id.rvYearbook);

        //lv
        lvNoData = view.findViewById(R.id.lvNoData);

        getYearBook();
    }

    public void getYearBook() {
        //Progress bar
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<getYearBook> getYearBook = retrofitClass.getYearbook(0, Integer.parseInt(schoolId), "yearbook");
        getYearBook.enqueue(new Callback<com.app.yearbook.model.yearbook.getYearBook>() {
            @Override
            public void onResponse(Call<getYearBook> call, Response<getYearBook> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (response.body().getResponseCode().equals("1")) {
                    if (response.body().getYearbookList().size() > 0) {
                        yearbookLists = new ArrayList<>();
                        rvYearbook.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);

                        ArrayList<YearbookList> al_bookitemlist = new ArrayList<>();
                        bookList = new ArrayList<>();
                        yearbookLists.addAll(response.body().getYearbookList());
                        //LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                        Log.d("TTT", "Sizeee/ " + yearbookLists.size());

                        for (int i = 0; i < yearbookLists.size(); i++) {

                            al_bookitemlist.add(yearbookLists.get(i));

                            //Log.d("TTT","al_bookitemlist: "+al_bookitemlist.get(i).getYearbookName());
                            if (((i + 1) % 3) == 0 || i == yearbookLists.size() - 1) {
                                BookListModelForYearbook bookListModel = new BookListModelForYearbook(i, al_bookitemlist);
                                bookList.add(bookListModel);
                                al_bookitemlist = new ArrayList<>();
                            }
                        }

                        if (bookList.size() < 4) {
                            for (int j = 0; j < 4 - bookList.size() + 1; j++) {
                                BookListModelForYearbook bookListModel = new BookListModelForYearbook(bookList.size() + 1 + j, al_bookitemlist);
                                bookList.add(bookListModel);
                                al_bookitemlist = new ArrayList<>();
                            }
                        }

                        //GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        rvYearbook.setLayoutManager(mLayoutManager);

                        StaffYearListAdapter mAdapter = new StaffYearListAdapter(bookList, getActivity());
                        rvYearbook.setAdapter(mAdapter);

                    } else {
                        rvYearbook.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10")) {
                    Toast.makeText(getActivity(),response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(getActivity());
                    loginSP.clearData();

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                }
                else {
                    rvYearbook.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<getYearBook> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.dummy_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean validation() {
        boolean result = false;

        //old pwd
        Log.d("TTT", "Old Pwd: " + userPwd + " / " + edtOldPassword.getText().toString());

        if (edtOldPassword.getText().toString().trim().equals("")) {
            input_layout_oldpassword.setErrorEnabled(true);
            input_layout_oldpassword.setError("Your old password is empty");
            result = false;
        } else if (!userPwd.toString().equals(edtOldPassword.getText().toString())) {
            Log.d("TTT", "wrongg pwd");
            input_layout_oldpassword.setErrorEnabled(true);
            input_layout_oldpassword.setError("Your old password is wrong");
            result = false;
        } else {
            input_layout_oldpassword.setErrorEnabled(false);
            input_layout_oldpassword.setError("");
            result = true;
        }

        //new pwd
        if (edtNewPassword.getText().toString().trim().equals("")) {
            input_layout_newpassword.setErrorEnabled(true);
            input_layout_newpassword.setError("Your new password is empty");
            result = false;
        } else {
            input_layout_newpassword.setErrorEnabled(false);
            input_layout_newpassword.setError("");
            result = true;
        }

        //confirm new pwd
        if (edtConfirmPassword.getText().toString().trim().equals("")) {
            input_layout_confirmpassword.setErrorEnabled(true);
            input_layout_confirmpassword.setError("Your confirm password is empty");
            result = false;
        } else if (!edtConfirmPassword.getText().toString().equals(edtNewPassword.getText().toString())) {
            input_layout_confirmpassword.setErrorEnabled(true);
            input_layout_confirmpassword.setError("Confirm password and new password is must be same");
            result = false;
        } else {
            input_layout_confirmpassword.setErrorEnabled(false);
            input_layout_confirmpassword.setError("");
            result = true;

        }

        Log.d("TTT", "Result: " + result);
        return result;
    }

    public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(getActivity(), v);
        popup.getMenuInflater().inflate(R.menu.profileseeting, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Change password")) {
                    androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.custom_alertbox, null);
                    dialogBuilder.setView(dialogView);
                    final androidx.appcompat.app.AlertDialog alert = dialogBuilder.create();
                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    alert.setCancelable(false);

                    edtNewPassword = dialogView.findViewById(R.id.edtNewPassword);
                    edtNewPassword.setOnFocusChangeListener(newPassword);

                    edtOldPassword = dialogView.findViewById(R.id.edtOldPassword);
                    edtOldPassword.setOnFocusChangeListener(oldPassword);

                    edtConfirmPassword = dialogView.findViewById(R.id.edtConfirmPassword);
                    edtConfirmPassword.setOnFocusChangeListener(confirmPassword);

                    input_layout_oldpassword = dialogView.findViewById(R.id.input_layout_oldpassword);
                    input_layout_newpassword = dialogView.findViewById(R.id.input_layout_newpassword);
                    input_layout_confirmpassword = dialogView.findViewById(R.id.input_layout_confirmpassword);

                    Button btnCancel = dialogView.findViewById(R.id.btnCancel);
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    Button btnOk = dialogView.findViewById(R.id.btnOk);
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (validation()) {
                                if (!userPwd.toString().equals(edtOldPassword.getText().toString())) {
                                    Log.d("TTT", "wrongg pwd");
                                    input_layout_oldpassword.setErrorEnabled(true);
                                    input_layout_oldpassword.setError("Your old password is wrong");
                                } else {
                                    alert.dismiss();
                                    changePassword(edtNewPassword.getText().toString());
                                }
                            }
                        }
                    });
                    alert.show();
                } else if (item.getTitle().equals("Logout")) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setCancelable(false);
                    dialog.setTitle("");
                    dialog.setMessage("Are you sure you want to logout ?");
                    dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //Action for "Delete".
                            dialog.dismiss();

                            String token = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Token", "");
                            Log.d("TTT", "token: " + token);

                            if (!token.equals("")) {
                                progressDialog = ProgressDialog.show(getActivity(), "", "", true);
                                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                progressDialog.setContentView(R.layout.progress_view);
                                ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                                Circle bounce = new Circle();
                                bounce.setColor(Color.BLACK);
                                progressBar.setIndeterminateDrawable(bounce);

                                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                final Call<GiveReport> userPost = retrofitClass.logout(token, "staff");
                                userPost.enqueue(new Callback<GiveReport>() {
                                    @Override
                                    public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }

                                        LoginUser loginSP = new LoginUser(getActivity());
                                        loginSP.clearData();

                                        Intent i = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(i);
                                        getActivity().finish();
                                    }

                                    @Override
                                    public void onFailure(Call<GiveReport> call, Throwable t) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }
                                        allMethods.setAlert(getActivity(), "", t.getMessage() + "");
                                    }
                                });
                            }
                            else
                            {
                                LoginUser loginSP = new LoginUser(getActivity());
                                loginSP.clearData();

                                Intent i = new Intent(getActivity(), LoginActivity.class);
                                startActivity(i);
                                (getActivity()).finish();
                            }
                        }


                    });
                    dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    final AlertDialog alert = dialog.create();
                    alert.show();

                }
                return true;
            }
        });
        popup.show();
    }

    private View.OnFocusChangeListener newPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtNewPassword.getText().toString().equals("")) {
                    input_layout_newpassword.setErrorEnabled(true);
                    input_layout_newpassword.setError("Your new password is empty");
                } else {
                    input_layout_newpassword.setErrorEnabled(false);
                    input_layout_newpassword.setError("");
                }
            }
        }
    };

    private View.OnFocusChangeListener confirmPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtConfirmPassword.getText().toString().equals("")) {
                    input_layout_confirmpassword.setErrorEnabled(true);
                    input_layout_confirmpassword.setError("Your confirm password is empty");
                } else {
                    input_layout_confirmpassword.setErrorEnabled(false);
                    input_layout_confirmpassword.setError("");
                }
            }
        }
    };

    private View.OnFocusChangeListener oldPassword = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (edtOldPassword.getText().toString().equals("")) {
                    input_layout_oldpassword.setErrorEnabled(true);
                    input_layout_oldpassword.setError("Your old password is empty");
                } else {
                    input_layout_oldpassword.setErrorEnabled(false);
                    input_layout_oldpassword.setError("");
                }
            }
        }
    };

    public void changePassword(String pwd) {
        //Progress bar
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> userPost = retrofitClass.changePassword(Integer.parseInt(userId), "staff", pwd);
        userPost.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", response.body().getResponseMsg() + "");
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(getActivity(), "", t.getMessage() + "");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.setting) {
            View menuItemView = getActivity().findViewById(R.id.setting);
            showMenu(menuItemView);

        }
        return super.onOptionsItemSelected(item);
    }

}

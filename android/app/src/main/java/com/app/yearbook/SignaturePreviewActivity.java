package com.app.yearbook;

import android.app.ProgressDialog;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.yearbook.databinding.ActivitySignaturePreviewBinding;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.SignatureModel;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.Constants;
import com.github.ybq.android.spinkit.style.Circle;

import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignaturePreviewActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "SignaturePreviewActivit";
    ActivitySignaturePreviewBinding binding;
    SignatureModel signatureModel;
    RetrofitClass retrofitClass;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signature_preview);
        retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        if (getIntent().hasExtra(Constants.SIGNATURE_DATA)) {
            signatureModel = getIntent().getParcelableExtra(Constants.SIGNATURE_DATA);

            try{
                binding.txtSignature.setText(LoginUser.getUserTypeKey()==2?LoginUser.getUserData().getStaffFirstname():LoginUser.getUserData().getUserFirstname());
            }catch (Exception e){
                binding.txtSignature.setText(getString(R.string.app_name));
            }

            binding.setModel(signatureModel);
            setTypeFace();

        }
        setToolbar();

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        String formattedDate = Constants.simpleDateFormat.format(c);
        binding.txtDate.setText("Signed on " + Constants.getDisplayDate(formattedDate, Constants.simpleDateFormat));
        binding.btnSend.setOnClickListener(this);
        binding.btnSend1.setOnClickListener(this);
    }

    private void setTypeFace() {
        Typeface typeface = null;
        Log.d(TAG, "setTypeFace: " + Constants.fontMap.size());
        Log.d(TAG, "setTypeFace: " + signatureModel.getFontId());

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            typeface = getResources().getFont(Constants.fontMap.get(signatureModel.getFontId()));
        } else {
            typeface = ResourcesCompat.getFont(this, Constants.fontMap.get(signatureModel.getFontId()));
        }
        binding.txtSignature.setTypeface(typeface);
    }

    private void setToolbar() {
        setSupportActionBar(binding.appbar.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (LoginUser.getUserData() != null && LoginUser.getUserData().getSchoolName() != null) {
            binding.appbar.toolbarTitle.setText(LoginUser.getUserData().getSchoolName());
        } else {
            binding.appbar.toolbarTitle.setText(getString(R.string.app_name));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v == binding.btnSend) {
            sendSignature();
        }
        if (v == binding.btnSend1) {
            sendSignature();
        }
    }

    private void sendSignature() {
        progressDialog = ProgressDialog.show(this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        final ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        //ashish problem reciverType
        Call<Login> addSignature = retrofitClass.addSignatureAll(LoginUser.getUserId()+"",
                signatureModel.getUserId(),
                LoginUser.getUserTypeKey()+"",
                signatureModel.getUserType(),
                signatureModel.getGreeting(),
                signatureModel.getMessage(),
                signatureModel.getFontId());

        addSignature.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Log.d(TAG, "onResponse: ");
                Login resLogin = response.body();
                if (resLogin.getResponseCode().equals("2")) {
                    Toast.makeText(SignaturePreviewActivity.this, resLogin.getResponseMsg(), Toast.LENGTH_LONG).show();
                }
                progressDialog.dismiss();
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
                progressDialog.dismiss();
            }
        });
    }
}

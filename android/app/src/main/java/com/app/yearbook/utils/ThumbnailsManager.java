package com.app.yearbook.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.app.yearbook.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Varun on 30/06/15.
 *         <p/>
 *         Singleton Class Used to Manage filters and process them all at once
 */
public final class ThumbnailsManager {
    private static List<ThumbnailItem> filterThumbs = new ArrayList<ThumbnailItem>(10);
    private static List<ThumbnailItem> processedThumbs = new ArrayList<ThumbnailItem>(10);

    private ThumbnailsManager() {
    }

    public static void addThumb(ThumbnailItem thumbnailItem) {
        filterThumbs.add(thumbnailItem);
    }

    public static List<ThumbnailItem> processThumbs(Context context) {
        for (ThumbnailItem thumb : filterThumbs) {
            // scaling down the image
            float size = context.getResources().getDimension(R.dimen.thumbnail_size);

            thumb.image = Bitmap.createScaledBitmap(thumb.image, (int)(thumb.image.getWidth()*0.5),(int)(thumb.image.getHeight()*0.5), true);

            Log.d("TTT", "processThumbs: "+thumb.image.getWidth()+"/"+thumb.image.getHeight());
            thumb.image = thumb.filter.processFilter(thumb.image);
            // cropping circle
            // thumb.image = GeneralUtils.generateCircularBitmap(thumb.image);
            processedThumbs.add(thumb);
        }
        return processedThumbs;
    }

    public static void clearThumbs() {
        filterThumbs = new ArrayList<>();
        processedThumbs = new ArrayList<>();
    }
}

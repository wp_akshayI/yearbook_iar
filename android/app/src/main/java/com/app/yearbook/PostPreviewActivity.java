package com.app.yearbook;

import android.app.ProgressDialog;

import androidx.databinding.DataBindingUtil;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.yearbook.adapter.AdapterOptions;
import com.app.yearbook.databinding.ActivityPostPreviewBinding;
import com.app.yearbook.databinding.LayoutImagePostPreviewBinding;
import com.app.yearbook.databinding.LayoutPollPreviewBinding;
import com.app.yearbook.databinding.LayoutTextPostPreviewBinding;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.allpost.ImageVideoPost;
import com.app.yearbook.model.allpost.OptionsModel;
import com.app.yearbook.model.allpost.PollPost;
import com.app.yearbook.model.allpost.TextPost;
import com.app.yearbook.model.postmodels.PollOption;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.util.AppUtil;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.BitmapUtils;
import com.app.yearbook.utils.Constants;
import com.bumptech.glide.Glide;
import com.github.ybq.android.spinkit.style.Circle;

import org.json.JSONArray;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.VideoInfo;

public class PostPreviewActivity extends AppCompatActivity {

    private static final String TAG = "PostPreviewActivity";
    ActivityPostPreviewBinding binding;

    TextPost textPost;
    PollPost pollPost;
    ImageVideoPost imageVideoPost;
    String postType;
    PostListItem postListItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_preview);
        setToolbar();

        if (getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false)) {
            postListItem = getIntent().getParcelableExtra(Constants.INTENT_DATA);
            binding.appbar.toolbarTitle.setText(getString(R.string.edit_post));
        }

        if (getIntent().hasExtra(Constants.POST_TYPE)) {
            postType = getIntent().getStringExtra(Constants.POST_TYPE);
            if (postType.equalsIgnoreCase(Constants.POST_TEXT)) {
                if (getIntent().hasExtra(Constants.POST_TEXT)) {
                    textPost = getIntent().getParcelableExtra(Constants.POST_TEXT);
                } else {
                    textPost = new TextPost("", "", "", "");
                }
                LayoutTextPostPreviewBinding textFeedBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.layout_text_post_preview, binding.mainLayout, false);
                textFeedBinding.setModel(textPost);

                Date c = Calendar.getInstance().getTime();
                String formattedDate = Constants.simpleDateFormat.format(c);
                textPost.setDate(Constants.getDisplayDate(formattedDate, Constants.simpleDateFormat));

                binding.conPost.addView(textFeedBinding.getRoot());
            } else if (postType.equalsIgnoreCase(Constants.POST_POll)) {
                if (getIntent().hasExtra(Constants.POST_POll)) {
                    pollPost = getIntent().getParcelableExtra(Constants.POST_POll);
                    LayoutPollPreviewBinding pollFeedBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.layout_poll_preview, binding.mainLayout, false);
                    pollFeedBinding.setModel(pollPost);
                    pollFeedBinding.txtDayLeft.setText(Constants.getDaysLeft(pollPost.getLastDate()));
                    pollFeedBinding.rcOptions.setAdapter(new AdapterOptions(pollPost.getOptionsModels(), new OnRecyclerClick() {

                        @Override
                        public void onVote(int pos, String pollOptionId) {

                        }

                        @Override
                        public void onClick(int pos, int type) {
                            pollPost.getOptionsModels().get(pos).setSelected(false);
                        }
                    }));
                    binding.conPost.addView(pollFeedBinding.getRoot());
                    if (getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false)) {
                        binding.appbar.toolbarTitle.setText(getString(R.string.edit_poll));
                    } else {
                        binding.appbar.toolbarTitle.setText(getString(R.string.new_poll));
                    }
                }
            } else if (postType.equalsIgnoreCase(Constants.POST_IMAGE)) {
                if (getIntent().hasExtra(Constants.POST_IMAGE)) {
                    imageVideoPost = getIntent().getParcelableExtra(Constants.POST_IMAGE);
                    LayoutImagePostPreviewBinding imagePostPreviewBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.layout_image_post_preview, binding.mainLayout, false);
                    imagePostPreviewBinding.setModel(imageVideoPost);

                    imagePostPreviewBinding.imgPost.setVisibility(View.VISIBLE);

                    Glide.with(this)
                            .load(imageVideoPost.getUrl())
                            .into(imagePostPreviewBinding.imgPost);


                    Date c = Calendar.getInstance().getTime();
                    String formattedDate = Constants.simpleDateFormat.format(c);
                    imageVideoPost.setDate(Constants.getDisplayDate(formattedDate, Constants.simpleDateFormat));
                    binding.conPost.addView(imagePostPreviewBinding.getRoot());

                }
            } else if (postType.equalsIgnoreCase(Constants.POST_VIDEO)) {
                if (getIntent().hasExtra(Constants.POST_VIDEO)) {
                    imageVideoPost = getIntent().getParcelableExtra(Constants.POST_VIDEO);
                    LayoutImagePostPreviewBinding imagePostPreviewBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.layout_image_post_preview, binding.mainLayout, false);
                    imagePostPreviewBinding.setModel(imageVideoPost);
                    imagePostPreviewBinding.videoView.setVisibility(View.VISIBLE);
                    imagePostPreviewBinding.videoView.getVideoInfo().setPortraitWhenFullScreen(false);
                    imagePostPreviewBinding.videoView.setVideoPath(imageVideoPost.getUrl());
                    imagePostPreviewBinding.videoView.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player

                    Date c = Calendar.getInstance().getTime();
                    String formattedDate = Constants.simpleDateFormat.format(c);
                    imageVideoPost.setDate(Constants.getDisplayDate(formattedDate, Constants.simpleDateFormat));
                    binding.conPost.addView(imagePostPreviewBinding.getRoot());

                }
            }
        }


    }

    private void setToolbar() {
        setSupportActionBar(binding.appbar.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        binding.appbar.toolbarTitle.setText(getString(R.string.new_post));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_post, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_post:
                if (getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false)) {
                    editPostData();
                } else if (postType.equalsIgnoreCase(Constants.POST_TEXT)) {
                    addTextPost();
                } else if (postType.equalsIgnoreCase(Constants.POST_POll)) {
                    addPollPost();
                } else if (postType.equalsIgnoreCase(Constants.POST_IMAGE)) {
                    addImagePost();
                } else if (postType.equalsIgnoreCase(Constants.POST_VIDEO)) {
                    addVideoPost();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addVideoPost() {
        progressDialog = ProgressDialog.show(this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(1));
        RequestBody staffId = RequestBody.create(MediaType.parse("text/plain"), LoginUser.getUserData().getStaffId());
        RequestBody schoolId = RequestBody.create(MediaType.parse("text/plain"), LoginUser.getUserData().getSchoolId());
        RequestBody title = RequestBody.create(MediaType.parse("text/plain"), AppUtil.getEncodeUTF8String(imageVideoPost.getSubject()));
        RequestBody subTitle = RequestBody.create(MediaType.parse("text/plain"), AppUtil.getEncodeUTF8String(imageVideoPost.getDescription()));
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody fileType = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(2));


        File fileVideo = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), Uri.parse(imageVideoPost.getUrl())));
        RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileVideo);
        MultipartBody.Part part = MultipartBody.Part.createFormData("post_file", fileVideo.getName(), filePost);

        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(fileVideo.getAbsolutePath(), MediaStore.Video.Thumbnails.MINI_KIND);
        fileVideo = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), AllMethods.getImageUri(getApplicationContext(), bMap)));
        RequestBody filePostThumb = RequestBody.create((MediaType.parse("*/*")), fileVideo);
        MultipartBody.Part thumbPart = MultipartBody.Part.createFormData("post_thumbnail", fileVideo.getName(), filePostThumb);
//        rbFileThumb = RequestBody.create(MediaType.parse("text/plain"), fileVideo.getName());

        RequestBody timestamp = RequestBody.create(MediaType.parse("text/plain"), Calendar.getInstance().getTimeInMillis() + "");

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<Login> addVideoPost = retrofitClass.addMediaPost(timestamp, type, staffId, schoolId, title, subTitle, description, fileType, part, thumbPart);
        addVideoPost.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Login resLogin = response.body();
                if (resLogin.getResponseCode().equals("1")) {
                    setResult(RESULT_OK);
                    finish();
                    Toast.makeText(PostPreviewActivity.this, "Post added successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(PostPreviewActivity.this, resLogin.getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(PostPreviewActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addImagePost() {
        progressDialog = ProgressDialog.show(this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(1));
        RequestBody staffId = RequestBody.create(MediaType.parse("text/plain"), LoginUser.getUserData().getStaffId());
        RequestBody schoolId = RequestBody.create(MediaType.parse("text/plain"), LoginUser.getUserData().getSchoolId());
        RequestBody title = RequestBody.create(MediaType.parse("text/plain"), AppUtil.getEncodeUTF8String(imageVideoPost.getSubject()));
        RequestBody subTitle = RequestBody.create(MediaType.parse("text/plain"), AppUtil.getEncodeUTF8String(imageVideoPost.getDescription()));
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody fileType = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(1));

        File fileImage = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), Uri.parse(imageVideoPost.getUrl())));
        RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
        MultipartBody.Part part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);

        RequestBody timestamp = RequestBody.create(MediaType.parse("text/plain"), Calendar.getInstance().getTimeInMillis() + "");

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<Login> addImagePost = retrofitClass.addMediaPost(timestamp, type, staffId, schoolId, title, subTitle, description, fileType, part, null);
        addImagePost.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                try {
                    Login resLogin = response.body();
                    if (resLogin.getResponseCode().equals("1")) {
                        setResult(RESULT_OK);
                        finish();
                        Toast.makeText(PostPreviewActivity.this, "Post added successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(PostPreviewActivity.this, resLogin.getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(PostPreviewActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(PostPreviewActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addPollPost() {
        //Progress bar
        progressDialog = ProgressDialog.show(this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        JSONArray options = new JSONArray();
        for (OptionsModel optionsModel : pollPost.getOptionsModels()) {
            options.put(AppUtil.getEncodeUTF8String(optionsModel.getOption()));
        }

        Log.d(TAG, "addPollPost: " + options);

        String grade = pollPost.getGradeListItem().getGrade();
        Log.e("---grade========", grade);
        grade = grade.toLowerCase();
        /*if (!pollPost.getGradeListItem().getStudentGradeId().equals("-1")) {
            grade = grade.replaceAll("[^\\d.]", "");
        } else {
            grade = grade.toLowerCase();
        }*/

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<Login> addTextPost = retrofitClass.addPollPost(Calendar.getInstance().getTimeInMillis() + "", 4,
                Integer.parseInt(LoginUser.getUserData().getStaffId()),
                Integer.parseInt(LoginUser.getUserData().getSchoolId()),
                AppUtil.getEncodeUTF8String(pollPost.getSubject()),
                AppUtil.getEncodeUTF8String(pollPost.getDescription()),
//                pollPost.getSubject(),
//                pollPost.getDescription(),
                options.toString(),
                grade,
                pollPost.getLastDate());
        addTextPost.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Login resLogin = response.body();
                if (resLogin.getResponseCode().equals("1")) {
                    setResult(RESULT_OK);
                    finish();
                    Toast.makeText(PostPreviewActivity.this, "Post added successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(PostPreviewActivity.this, resLogin.getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(PostPreviewActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ProgressDialog progressDialog;

    /*public void addTextPost() {
        Log.e("Timestamp", Calendar.getInstance().getTimeInMillis()+"");
    }*/
    public void addTextPost() {
        //Progress bar
        progressDialog = ProgressDialog.show(this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);
        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<Login> addTextPost = retrofitClass.addTextPost(Calendar.getInstance().getTimeInMillis() + "", 3,
                Integer.parseInt(LoginUser.getUserData().getStaffId()),
                Integer.parseInt(LoginUser.getUserData().getSchoolId()),
                AppUtil.getEncodeUTF8String(textPost.getSubject()),
                AppUtil.getEncodeUTF8String(textPost.getSubTitle()),
                AppUtil.getEncodeUTF8String(textPost.getComposedPost()));
                /*textPost.getSubject(),
                textPost.getSubTitle(),
                textPost.getComposedPost());*/
        addTextPost.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Login resLogin = response.body();
                if (resLogin.getResponseCode().equals("1")) {
                    setResult(RESULT_OK);
                    finish();
                    Toast.makeText(PostPreviewActivity.this, "Post added successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(PostPreviewActivity.this, resLogin.getResponseMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(PostPreviewActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void editPostData() {
        progressDialog = ProgressDialog.show(this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);
        MultipartBody.Part part = null;
        MultipartBody.Part thumbPart = null;
        RequestBody options = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody expiry_date = RequestBody.create(MediaType.parse("text/plain"), "");
        //set post data
        if (postType.equalsIgnoreCase(Constants.POST_TEXT)) {
            postListItem.setPostTitle(textPost.getSubject());
            postListItem.setPostSubTitle(textPost.getSubTitle());
            postListItem.setPostDescription(textPost.getComposedPost());
        } else if (postType.equalsIgnoreCase(Constants.POST_IMAGE)) {
            postListItem.setPostTitle(imageVideoPost.getSubject());
            postListItem.setPostSubTitle(imageVideoPost.getDescription());
            postListItem.setPostDescription("");

            postListItem.setPostFile(imageVideoPost.getUrl());

            File fileVideo = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), Uri.parse(postListItem.getPostFile())));
            RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileVideo);
            part = MultipartBody.Part.createFormData("post_file", fileVideo.getName(), filePost);

        } else if (postType.equalsIgnoreCase(Constants.POST_VIDEO)) {
            postListItem.setPostTitle(imageVideoPost.getSubject());
            postListItem.setPostSubTitle(imageVideoPost.getDescription());
            postListItem.setPostDescription("");

            postListItem.setPostFile(imageVideoPost.getUrl());

            File fileVideo = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), Uri.parse(postListItem.getPostFile())));
            RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileVideo);
            part = MultipartBody.Part.createFormData("post_file", fileVideo.getName(), filePost);

            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(fileVideo.getAbsolutePath(), MediaStore.Video.Thumbnails.MINI_KIND);
            fileVideo = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), AllMethods.getImageUri(getApplicationContext(), bMap)));
            RequestBody filePostThumb = RequestBody.create((MediaType.parse("*/*")), fileVideo);
            thumbPart = MultipartBody.Part.createFormData("post_thumbnail", fileVideo.getName(), filePostThumb);

        } else if (postType.equalsIgnoreCase(Constants.POST_POll)) {
            postListItem.setPostTitle(pollPost.getSubject());
            postListItem.setPostSubTitle(pollPost.getDescription());
            postListItem.setPostDescription("");

            JSONArray optJson = new JSONArray();
            /*for (OptionsModel optionsModel : pollPost.getOptionsModels()) {
                optJson.put(optionsModel.getOption());
            }*/
            /*for (PollOption pollOption : postListItem.getPollOptions()) {
                if (pollPost.getOptionsModels().contains(pollOption.getOptionDescription())){
                    optJson.put(pollOption.getOptionDescription());
                }
            }*/
            for (int i = 0; i < pollPost.getOptionsModels().size(); i++) {
                boolean isfound = false;
                for (int j = 0; j < postListItem.getPollOptions().size(); j++) {
                    if (pollPost.getOptionsModels().get(i).getOption().equalsIgnoreCase(postListItem.getPollOptions().get(j).getOptionDescription())) {
                        isfound = true;
                        break;
                    }
                }
                if (!isfound)
                    optJson.put(AppUtil.getEncodeUTF8String(pollPost.getOptionsModels().get(i).getOption()));
            }
            options = RequestBody.create(MediaType.parse("text/plain"), optJson.toString());
            expiry_date = RequestBody.create(MediaType.parse("text/plain"), pollPost.getLastDate());
            //GRADE not pass

        }
        ///set post data

        RequestBody postId = RequestBody.create(MediaType.parse("text/plain"), postListItem.getPostId());
        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), postListItem.getPostType());
        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), LoginUser.getUserData().getStaffId());
        RequestBody fileType = RequestBody.create(MediaType.parse("text/plain"), postListItem.getPostFileType());

        /*RequestBody title = RequestBody.create(MediaType.parse("text/plain"), postListItem.getPostTitle());
        RequestBody subTitle = RequestBody.create(MediaType.parse("text/plain"), postListItem.getPostSubTitle());
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), postListItem.getPostDescription());*/

        RequestBody title = RequestBody.create(MediaType.parse("text/plain"), AppUtil.getEncodeUTF8String(postListItem.getPostTitle()));
        RequestBody subTitle = RequestBody.create(MediaType.parse("text/plain"), AppUtil.getEncodeUTF8String(postListItem.getPostSubTitle()));
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), AppUtil.getEncodeUTF8String(postListItem.getPostDescription()));
        RequestBody timestamp = RequestBody.create(MediaType.parse("text/plain"), Calendar.getInstance().getTimeInMillis() + "");

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<Login> addVideoPost = retrofitClass.editPostData(timestamp, postId, type, userId, title, subTitle, description, options, expiry_date, fileType, part, thumbPart);
        addVideoPost.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Login resLogin = response.body();
                if (resLogin.getResponseCode().equals("1")) {
                    setResult(RESULT_OK);
                    finish();
                    Toast.makeText(PostPreviewActivity.this, "Post updated successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(PostPreviewActivity.this, resLogin.getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(PostPreviewActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}

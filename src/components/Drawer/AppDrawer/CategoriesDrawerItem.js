import React from "react"
import { useSelector } from "react-redux";
import DrawerList from "../DrawerList";
import List from '@material-ui/core/List';
import { withRouter } from "react-router-dom";

function CategoriesDrawerItem(props) {
    const { categories } = useSelector(({ category }) => category);

    const _navigateEditor = (data) => {
        if (!data) return;
        const { screen_id, name, category_id } = data;

        if (props.location.pathname === "/app/dashboard/sub-categories")
            return props.history.push({ pathname: "/app/dashboard/sub-categories", search: "_id="+ category_id+"&name="+name })

        return props.history.push({ pathname: "/app/page-editor", search: "_id=" + screen_id + "&_name=" + name + "&_category=" + category_id })
    }

    return (
        <div>
            <List style={{ padding: 0 }}>
                {
                    categories && categories.map((item, index) => {
                        return (
                            <DrawerList
                                title={(index + 1) + ". " + item.category_name}
                                key={item.category_id}
                                action={() => _navigateEditor(item)}
                            />
                        )
                    })
                }
            </List>

        </div>
    )
}


export default withRouter(CategoriesDrawerItem)
import { action } from "../utils";

let initialState = {
    drawerType: "components",   // categories, components, selectedComponents,
    drawerBackType: "selectedComponents",
    snackMessage: undefined,
    snackVisible: false,
    snackVariant: "default",  // success, warning, error, info, default, loader
}

export default function NavigationReducer(state = initialState, data) {
    switch (data.type) {

        case action.SET_DRAWER_TITLE:
            return {
                ...state,
                drawerTitle: data.title
            }

        case action.OPEN_DRAWER:
            return {
                ...state,
                drawerType: data.drawer,
                drawerBackType: data.backType
            }

        case action.close_notification:
            return {
                ...state,
                snackMessage: undefined,
                snackVisible: false,
                snackVariant: "default"
            }

        case action.open_notification:
            let snackVariant = "default"
            if (data.variant) snackVariant = data.variant
            return {
                ...state,
                snackMessage: data.message,
                snackVisible: true,
                snackVariant
            }


        case action.INITIAL_STATE:
            return {
                ...initialState
            }

        default:
            break;
    }
    return state;
}
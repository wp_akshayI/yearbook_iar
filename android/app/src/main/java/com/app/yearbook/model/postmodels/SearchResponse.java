package com.app.yearbook.model.postmodels;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchResponse implements Parcelable {
    @SerializedName("ResponseCode")
    private String responseCode;

    @SerializedName("ServerTimeZone")
    private String serverTimeZone;

    @SerializedName("ResponseMsg")
    private String responseMsg;

    @SerializedName("serverTime")
    private String serverTime;

    @SerializedName("tags_list")
    private List<PostListItem> postList;

    protected SearchResponse(Parcel in) {
        responseCode = in.readString();
        serverTimeZone = in.readString();
        responseMsg = in.readString();
        serverTime = in.readString();
        postList = in.createTypedArrayList(PostListItem.CREATOR);
        result = in.readString();
    }

    public static final Creator<SearchResponse> CREATOR = new Creator<SearchResponse>() {
        @Override
        public SearchResponse createFromParcel(Parcel in) {
            return new SearchResponse(in);
        }

        @Override
        public SearchResponse[] newArray(int size) {
            return new SearchResponse[size];
        }
    };

    public List<PostListItem> getPostList() {
        return postList;
    }

    public void setPostList(List<PostListItem> postList) {
        this.postList = postList;
    }

    @SerializedName("Result")
    private String result;


    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setServerTimeZone(String serverTimeZone) {
        this.serverTimeZone = serverTimeZone;
    }

    public String getServerTimeZone() {
        return serverTimeZone;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public String getServerTime() {
        return serverTime;
    }


    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(responseCode);
        dest.writeString(serverTimeZone);
        dest.writeString(responseMsg);
        dest.writeString(serverTime);
        dest.writeTypedList(postList);
        dest.writeString(result);
    }
}

package com.app.yearbook;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.yearbook.adapter.StudentPhotosAdapter;
import com.app.yearbook.databinding.ActivitySchoolAllStudentBinding;
import com.app.yearbook.model.employee.GetUserSchoolWise;
import com.app.yearbook.model.employee.UserListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.OnRecyclerClick;
import com.github.ybq.android.spinkit.style.Circle;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolAllStudentActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ProgressDialog progressDialog;
    private AllMethods utils;
    private TextView toolbar_title;
    private static final String TAG = "SchoolAllStudentActivit";
    public ArrayList<UserListItem> userPostItems;
    ArrayList<UserListItem> studentPhotos;
    private LoginUser loginUser;
    String schoolId = "", CategoryId = "",post="";
    private androidx.appcompat.widget.SearchView searchView;
    ActivitySchoolAllStudentBinding binding;
    StudentPhotosAdapter photoAdapter;
    int IntentStudentData = 888;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_school_all_student);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_school_all_student);
        setView();
    }

    public void setView() {
        //set toolbar
        setToolbar();

        utils = new AllMethods();
        userPostItems = new ArrayList<>();
        studentPhotos = new ArrayList<>();

        if (getIntent().getExtras() != null) {
            // schoolId = getIntent().getStringExtra("SchoolId");
            CategoryId = getIntent().getStringExtra("CategoryId");

            //SP object
            loginUser = new LoginUser(SchoolAllStudentActivity.this);
            if (loginUser.getUserData() != null) {
                schoolId = loginUser.getUserData().getSchoolId();
            }
            getSchoolUser(schoolId);

            if(getIntent().getStringExtra("post")!=null)
            {
                post=getIntent().getStringExtra("post");
            }
        }
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Student's portrait");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void getSchoolUser(String schoolId) {
        progressDialog = ProgressDialog.show(SchoolAllStudentActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetUserSchoolWise> schoolList = retrofitClass.getUserSchool(schoolId);
        schoolList.enqueue(new Callback<GetUserSchoolWise>() {
            @Override
            public void onResponse(Call<GetUserSchoolWise> call, Response<GetUserSchoolWise> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Log.d(TAG, "onResponse: " + response.toString());
                if (response.body().getResponseCode().equals("1")) {

                    if (response.body().getUserList() != null && response.body().getUserList().size() != 0) {
                        binding.lvNoData.setVisibility(View.GONE);
                        binding.recyclerViewStudent.setVisibility(View.VISIBLE);

                        studentPhotos.clear();
                        userPostItems.clear();
                        userPostItems.addAll(response.body().getUserList());

                        Log.d(TAG, "onResponse: user size: " + userPostItems.size());
                        if (userPostItems.size() > 0) {
                            for (UserListItem userPostItem : userPostItems) {
                                Log.d(TAG, "onResponse: user's images: " + userPostItem.getUserImages().size());

                                for (String s : userPostItem.getUserImages()) {
                                    //studentPhotos.add(new StudentPhotos(userPostItem.getUserId(), userPostItem.getUserFirstname(), userPostItem.getUserLastname(), s));
                                    Log.d(TAG, "onResponse: user pic: "+s);

                                    UserListItem newUser=new UserListItem();
                                    newUser.setUserSinglePhoto(s);
                                    newUser.setUserGrade(userPostItem.getUserGrade());
                                    newUser.setUserFirstname(userPostItem.getUserFirstname());
                                    newUser.setUserLastname(userPostItem.getUserLastname());
                                    newUser.setUserImage(userPostItem.getUserImage());
                                    newUser.setUserId(userPostItem.getUserId());
                                    newUser.setSchoolId(userPostItem.getSchoolId());

                                    Log.d(TAG, "onResponse: get user img: "+newUser.getUserSinglePhoto());
                                    studentPhotos.add(newUser);
                                }
                            }
                        }

                        Log.d(TAG, "onResponse: studentPhotos: " + studentPhotos.size());
                        if (studentPhotos.size() > 0) {

                            GridLayoutManager gridLayoutManager = new GridLayoutManager(SchoolAllStudentActivity.this, 3);
                            binding.recyclerViewStudent.setLayoutManager(gridLayoutManager);
                            photoAdapter = new StudentPhotosAdapter(SchoolAllStudentActivity.this, studentPhotos, new OnRecyclerClick() {
                                @Override
                                public void onClick(int pos, View v, int type) {

                                    if(post.equalsIgnoreCase("studentPortrait"))
                                    {
                                        Intent i=new Intent();
                                        i.putExtra("Type","Image");
                                        i.putExtra("CropType",3);
                                        i.putExtra("AddPostImage",studentPhotos.get(pos).getUserSinglePhoto());
                                        setResult(RESULT_OK,i);
                                        finish();
                                    }
                                    else
                                    {
                                        Log.d(TAG, "onClick: rv: "+studentPhotos.get(pos).getUserSinglePhoto());
                                        Intent i=new Intent(SchoolAllStudentActivity.this,AddYearbookPostActivity.class);
                                        i.putExtra("CategoryId",CategoryId);
                                        i.putExtra("Type","Image");
                                        i.putExtra("CropType",2);
                                        i.putExtra("UserData",studentPhotos.get(pos));
                                        i.putExtra("AddPostImage",studentPhotos.get(pos).getUserSinglePhoto());
                                        startActivityForResult(i,IntentStudentData);
                                    }
                                }
                            });
                            binding.recyclerViewStudent.setAdapter(photoAdapter);
                        } else {
                            binding.lvNoData.setVisibility(View.VISIBLE);
                            binding.recyclerViewStudent.setVisibility(View.GONE);
                        }

                    } else {
                        utils.setAlert(SchoolAllStudentActivity.this, "", response.body().getResponseMsg());
                        binding.lvNoData.setVisibility(View.VISIBLE);
                        binding.recyclerViewStudent.setVisibility(View.GONE);
                    }
                } else {
                    utils.setAlert(SchoolAllStudentActivity.this, "", response.body().getResponseMsg());
                    binding.lvNoData.setVisibility(View.VISIBLE);
                    binding.recyclerViewStudent.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<GetUserSchoolWise> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                utils.setAlert(SchoolAllStudentActivity.this, "", t.getMessage());
                binding.lvNoData.setVisibility(View.VISIBLE);
                binding.recyclerViewStudent.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchview, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (androidx.appcompat.widget.SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Search by studentname");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "onQueryTextSubmit: "+query);
                if(studentPhotos.size()!=0)
                    photoAdapter.getFilter().filter(query);
                return false;

            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(studentPhotos.size()!=0)
                    photoAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            AllMethods.hideKeyboard(SchoolAllStudentActivity.this,getCurrentFocus());
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentStudentData && resultCode == RESULT_OK) {
            //getYearbook(schoolId, staffId);
            Intent i = getIntent();
            setResult(RESULT_OK, i);
            finish();
        }
    }
}

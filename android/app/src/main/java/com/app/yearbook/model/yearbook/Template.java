
package com.app.yearbook.model.yearbook;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Template implements Serializable {

    @SerializedName("template_id")
    @Expose
    private String templateId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("template_no_of_photo")
    @Expose
    private String templateNoOfPhoto;
    @SerializedName("template_no_of_small_photo")
    @Expose
    private String templateNoOfSmallPhoto;
    @SerializedName("template_no_of_midium_photo")
    @Expose
    private String templateNoOfMidiumPhoto;
    @SerializedName("template_no_of_large_photo")
    @Expose
    private String templateNoOfLargePhoto;
    @SerializedName("template_created")
    @Expose
    private String templateCreated;

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getTemplateNoOfPhoto() {
        return templateNoOfPhoto;
    }

    public void setTemplateNoOfPhoto(String templateNoOfPhoto) {
        this.templateNoOfPhoto = templateNoOfPhoto;
    }

    public String getTemplateNoOfSmallPhoto() {
        return templateNoOfSmallPhoto;
    }

    public void setTemplateNoOfSmallPhoto(String templateNoOfSmallPhoto) {
        this.templateNoOfSmallPhoto = templateNoOfSmallPhoto;
    }

    public String getTemplateNoOfMidiumPhoto() {
        return templateNoOfMidiumPhoto;
    }

    public void setTemplateNoOfMidiumPhoto(String templateNoOfMidiumPhoto) {
        this.templateNoOfMidiumPhoto = templateNoOfMidiumPhoto;
    }

    public String getTemplateNoOfLargePhoto() {
        return templateNoOfLargePhoto;
    }

    public void setTemplateNoOfLargePhoto(String templateNoOfLargePhoto) {
        this.templateNoOfLargePhoto = templateNoOfLargePhoto;
    }

    public String getTemplateCreated() {
        return templateCreated;
    }

    public void setTemplateCreated(String templateCreated) {
        this.templateCreated = templateCreated;
    }

}

package com.app.yearbook.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.ImageActivity;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.ReportContentActivity;
import com.app.yearbook.StudentCommentActivity;
import com.app.yearbook.databinding.ItemStaffpostuserListBinding;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.staff.trash.DeleteListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.VideoInfo;

public class StaffDeletedPostUserListAdapter extends RecyclerView.Adapter<StaffDeletedPostUserListAdapter.MyViewHolder> {

    private ArrayList<DeleteListItem> postLists;
    private Activity ctx;
    public ProgressDialog progressDialog;
    public ItemStaffpostuserListBinding binding;
    private ListAdapterListener mListener;

    public interface ListAdapterListener { // create an interface
        void onClickAtOKButton(int position); // create callback function
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                , R.layout.item_staffpostuser_list, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.binding.setPostuser(postLists.get(position));
        if (holder.binding.getPostuser().getFlag().equalsIgnoreCase("user")) {
            holder.binding.lvUser.setVisibility(View.VISIBLE);
            holder.binding.lvPost.setVisibility(View.GONE);

            //user img
            if (holder.binding.getPostuser().getStaffImage() != null) {
                Glide.with(ctx)
                        .load(holder.binding.getPostuser().getStaffImage())
                        .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.mipmap.post_place_holder)
                        .error(R.mipmap.post_place_holder)
                        .into(holder.binding.imgUser);
            }

            holder.binding.imgUserRestore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ctx);
                    LayoutInflater inflater = ctx.getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
                    dialogBuilder.setView(dialogView);
                    final AlertDialog alert = dialogBuilder.create();
                    alert.setCancelable(false);
                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                    TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
                    tvMessage.setText("Are you sure you want to restore the user?");

                    TextView tvYes = dialogView.findViewById(R.id.tvYes);
                    TextView tvNo = dialogView.findViewById(R.id.tvNo);

                    tvYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            alert.dismiss();
                            final String id = holder.binding.getPostuser().getStaffId();

                            progressDialog = ProgressDialog.show(ctx, "", "", true);
                            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            progressDialog.setContentView(R.layout.progress_view);
                            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                            Circle bounce = new Circle();
                            bounce.setColor(Color.BLACK);
                            progressBar.setIndeterminateDrawable(bounce);

                            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                            final Call<GiveReport> userPost = retrofitClass.undoUser(id);
                            userPost.enqueue(new Callback<GiveReport>() {
                                @Override
                                public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }

                                    if (response.body().getResponseCode().equals("1")) {
                                        for (int i = 0; i < postLists.size(); i++) {
                                            if (postLists.get(i).getStaffId().equals(String.valueOf(id))) {
                                                Log.d("TTT", "position: " + i);
                                                postLists.remove(i);
                                                notifyItemRemoved(i);
                                            }
                                        }

                                        mListener.onClickAtOKButton(postLists.size());

                                        Toast.makeText(ctx, "Successfully restore the user.", Toast.LENGTH_SHORT).show();
                                    } else if (response.body().getResponseCode().equals("10")) {
                                        Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                        LoginUser loginSP = new LoginUser(ctx);
                                        loginSP.clearData();

                                        Intent i = new Intent(ctx, LoginActivity.class);
                                        ctx.startActivity(i);
                                        ctx.finish();
                                    } else {
                                        Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onFailure(Call<GiveReport> call, Throwable t) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                    Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

                    tvNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });
                    alert.show();


                }
            });

        } else {
            //post
            holder.binding.lvUser.setVisibility(View.GONE);
            holder.binding.lvPost.setVisibility(View.VISIBLE);

            //user img
            if (holder.binding.getPostuser().getStaffImage() != null) {
                Glide.with(ctx)
                        .load(holder.binding.getPostuser().getStaffImage())
                        .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.mipmap.post_place_holder)
                        .error(R.mipmap.post_place_holder)
                        .into(holder.binding.imgUserImage);
                binding.imgUserImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ctx, ImageActivity.class);
                        i.putExtra("FileUrl", holder.binding.getPostuser().getStaffImage());
                        ctx.startActivity(i);
                    }
                });
            }

            // post img
            if (holder.binding.getPostuser().getPostFile() != null || !holder.binding.getPostuser().getPostFile().equals("")) {
                final String uri = holder.binding.getPostuser().getPostFile();
                String extension = uri.substring(uri.lastIndexOf("."));
                Log.d("TTT", "Url type: " + extension);

                if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {
                    holder.binding.imgUserPost.setVisibility(View.VISIBLE);
                    holder.binding.videoUserPost.setVisibility(View.GONE);
                    if (holder.binding.getPostuser().getPostFile() != null) {
                        Glide.with(ctx)
                                .load(holder.binding.getPostuser().getPostFile())
                                .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                        holder.binding.imgUserPost.setImageBitmap(resource);
                                    }
                                });
                    }
                } else {
                    holder.binding.imgUserPost.setVisibility(View.GONE);
                    holder.binding.videoUserPost.setVisibility(View.VISIBLE);

//                    holder.binding.videoUserPost.reset();
//                    holder.binding.videoUserPost.setSource(Uri.parse(holder.binding.getPostuser().getPostFile()));


                    if (binding.videoUserPost.getCoverView() != null) {
                        Glide.with(ctx)
                                .load(binding.getPostuser().getPostThumbnail())
                                .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(binding.videoUserPost.getCoverView());
                    }

                    binding.videoUserPost.getVideoInfo().setPortraitWhenFullScreen(false);
                    binding.videoUserPost.setVideoPath(binding.getPostuser().getPostFile());
                    binding.videoUserPost.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player


                    Log.d("TTT", "Video urllllll: " + holder.binding.getPostuser().getPostFile());
                }
            }

            holder.binding.imgRestore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ctx);
                    LayoutInflater inflater = ctx.getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
                    dialogBuilder.setView(dialogView);
                    final AlertDialog alert = dialogBuilder.create();
                    alert.setCancelable(false);
                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                    TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
                    tvMessage.setText("Are you sure you want to restore the post?");

                    TextView tvYes = dialogView.findViewById(R.id.tvYes);
                    TextView tvNo = dialogView.findViewById(R.id.tvNo);

                    tvYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            alert.dismiss();
                            final String id = holder.binding.getPostuser().getPostId();

                            progressDialog = ProgressDialog.show(ctx, "", "", true);
                            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            progressDialog.setContentView(R.layout.progress_view);
                            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                            Circle bounce = new Circle();
                            bounce.setColor(Color.BLACK);
                            progressBar.setIndeterminateDrawable(bounce);

                            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                            final Call<GiveReport> userPost = retrofitClass.undoPost(id);
                            userPost.enqueue(new Callback<GiveReport>() {
                                @Override
                                public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }

                                    if(response.body().getResponseCode().equals("1")) {
                                        for (int i = 0; i < postLists.size(); i++) {
                                            if (postLists.get(i).getPostId() != null) {
                                                if (postLists.get(i).getPostId().equals(id)) {
                                                    Log.d("TTT", "position: " + i);
                                                    postLists.remove(i);
                                                    notifyItemRemoved(i);
                                                }
                                            }
                                        }
                                        mListener.onClickAtOKButton(postLists.size());
                                        Toast.makeText(ctx, "Successfully restore the post.", Toast.LENGTH_SHORT).show();
                                    }
                                    else if (response.body().getResponseCode().equals("10")) {
                                        Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                        LoginUser loginSP = new LoginUser(ctx);
                                        loginSP.clearData();

                                        Intent i = new Intent(ctx, LoginActivity.class);
                                        ctx.startActivity(i);
                                        ctx.finish();
                                    } else {
                                        Toast.makeText(ctx, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onFailure(Call<GiveReport> call, Throwable t) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                    Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

                    tvNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });
                    alert.show();


                }
            });

            //comment
            holder.binding.tvComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx.getApplicationContext(), StudentCommentActivity.class);
                    i.putExtra("PostId", holder.binding.getPostuser().getPostId());
                    i.putExtra("comment", "staff");
                    ctx.startActivity(i);
                }
            });

            //view content
            holder.binding.tvReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("TTT", "Post id: " + holder.binding.getPostuser().getPostId());
                    if (!holder.binding.tvReport.getText().toString().trim().equalsIgnoreCase("Not reported yet")) {
                        Intent i = new Intent(ctx, ReportContentActivity.class);
                        i.putExtra("postId", holder.binding.getPostuser().getPostId());
                        ctx.startActivity(i);
                    }
                }
            });

        }
    }

    public StaffDeletedPostUserListAdapter(ArrayList<DeleteListItem> postLists, Activity context, ListAdapterListener mListener) {
        //this.staffID = userID;
        this.postLists = postLists;
        this.ctx = context;
        this.mListener = mListener;
    }

    @Override
    public int getItemCount() {
        return postLists.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ItemStaffpostuserListBinding binding;

        public MyViewHolder(ItemStaffpostuserListBinding view) {
            super(view.getRoot());
            binding = view;
        }
    }

}

package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.yearbook.fragment.AccessCodeDisable;
import com.app.yearbook.fragment.FirstVersionProfileFragment;
import com.app.yearbook.fragment.HomeFragment;
import com.app.yearbook.fragment.LibraryFragment;
import com.app.yearbook.fragment.NotificationFirstVersionFragment;
import com.app.yearbook.fragment.ProfileFragment;
import com.app.yearbook.fragment.SearchFragment;
import com.app.yearbook.fragment.YearBookFG;
import com.app.yearbook.fragment.YearbookFragment;
import com.app.yearbook.model.StaffStudent;
import com.app.yearbook.model.studenthome.GetSchoolDuration;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.GiraffePlayer;
import tcking.github.com.giraffeplayer2.PlayerManager;

public class StudentHomeActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private LoginUser loginUser;
    public String studentName = "", schoolId = "", start_date, end_date, userId = "", schoolName = "";
    //private BottomNavigationView bottomNavigationView;
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private FragmentTransaction transaction;
    public ProgressDialog progressDialog;
    public AllMethods utils;
    private Fragment fragment;
    // private BottomNavigationViewEx bnve;
    private TextView toolbar_title, toolbar_title_left;
    private HomeFragment homeFragment;
    private AccessCodeDisable accessCodeFragment;
//    private YearBookFG yearbookFragment;
    private YearbookFragment yearbookFragment;
    private LibraryFragment libraryFragment;
    private SearchFragment searchFragment;
    private ProfileFragment profileFragment;
    private NotificationFirstVersionFragment notificationFirstVersionFragment;
    private FirstVersionProfileFragment firstVersionProfileFragment;

    private boolean isFgYearBookOpen = false;

    //---------------------------------------drawer----------------------------------
    private DrawerLayout drawerLayout;
    NavigationView navigationView;
    public static int navItemIndex = 0;
    ActionBarDrawerToggle actionBarDrawerToggle;
    AppBarLayout mainToolbar;
    public static StudentHomeActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GiraffePlayer.debug = true;//show java logs
        GiraffePlayer.nativeDebug = false;//not show native logs
        mainToolbar = findViewById(R.id.main_toolbar);
        instance = this;
        //all control
        setView();

        if (getIntent().getBooleanExtra("isRedirect", false)) {
            if (getIntent().getStringExtra("redirectOn").contentEquals("notification")) {

                Bundle bundle = new Bundle();
                bundle.putString("redirectOn", getIntent().getStringExtra("redirectOn"));

                navItemIndex = 3;
                toolbarVisibility(2, "" + schoolName);
                getSupportActionBar().setElevation(0f);
                profileFragment = new ProfileFragment();
                fragment = profileFragment;
                profileFragment.setArguments(bundle);
                transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.frame_layout, profileFragment).commit();
            }
        }
    }

    public void setView() {
        //utils
        utils = new AllMethods();

        //SP object
        notifyData();

        //set toolbar
        setToolbar();


        //1:transaprent toolbar, 2:white toolbar
        toolbarVisibility(1, "");
        getSupportActionBar().setElevation(5f);

        //-------------------------------drawer---------------------------------
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        setUpNavigationView();

//        if (1==0){
        if (loginUser.getUserData().getIsAccessCodeVerify() != null && loginUser.getUserData().getIsAccessCodeVerify().equalsIgnoreCase("Yes")) {
            homeFragment = new HomeFragment();
            fragment = homeFragment;
        } else {
            accessCodeFragment = new AccessCodeDisable();
            fragment = accessCodeFragment;
        }
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_layout, homeFragment == null ? accessCodeFragment : homeFragment).commit();

        try {
            if (getIntent().getExtras() != null && getIntent().getAction().equals("accept")) {
                Log.d("TTT", "setView: accepttt");
            } else if (getIntent().getExtras() != null && getIntent().getAction().equals("deny")) {
                Log.d("TTT", "setView: deny");
            }
        } catch (Exception e) {
        }
        try {
//            getUserProfileData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void notifyData() {
        loginUser = new LoginUser(StudentHomeActivity.this);
        if (loginUser.getUserData() != null) {
            studentName = loginUser.getUserData().getUserFirstname() + " " + loginUser.getUserData().getUserLastname();
            schoolId = loginUser.getUserData().getSchoolId();
            userId = loginUser.getUserData().getUserId();
            schoolName = loginUser.getUserData().getSchoolName();
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int pxToDp(int px) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private void toolbarVisibility(int i, String title) {
        if (i == 1) {
            toolbar_title_left.setVisibility(View.GONE);
//            toolbar_title.setVisibility(View.GONE);
            toolbar_title.setText(loginUser.getUserData().getSchoolName());

        } else {
            toolbar.setBackgroundColor(Color.WHITE);
            toolbar_title_left.setVisibility(View.GONE);
            toolbar_title.setVisibility(View.VISIBLE);
            toolbar_title.setText(title);
        }
    }

    public void notifyToolbarTitle(String title) {
        if (toolbar_title != null && !TextUtils.isEmpty(title))
            toolbar_title.setText(title);
    }

    public void checkCameraOption() {
        //Progress bar
        progressDialog = ProgressDialog.show(StudentHomeActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetSchoolDuration> userPost = retrofitClass.getSchoolDuration(schoolId);
        userPost.enqueue(new Callback<GetSchoolDuration>() {
            @Override
            public void onResponse(Call<GetSchoolDuration> call, Response<GetSchoolDuration> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    start_date = response.body().getSchoolDetails().getSchoolStartDate();
                    end_date = response.body().getSchoolDetails().getSchoolEndDate();

                    Date startDate = null, endDate = null, cDateObject = null;   // assume these are set to something
                    Date currentDate = new Date();          // the date in question
                    String cDate = null;

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        startDate = format.parse(start_date);
                        endDate = format.parse(end_date);
                        cDate = format.format(currentDate);
                        cDateObject = format.parse(cDate);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Boolean b = (cDateObject.after(startDate) || cDateObject.equals(startDate)) && (cDateObject.before(endDate) || cDateObject.equals(endDate));
                    Log.d("TTT", "Camera option::: " + b + " // " + start_date + " / " + end_date + " / " + cDate);

                    //default call 1st fragmnet
                    if (!isFinishing()) {
                        if (fragment instanceof HomeFragment) {

                        } else {
                            // setDrawerState(false);
                            homeFragment = new HomeFragment();
                            fragment = homeFragment;
                            transaction = fragmentManager.beginTransaction();
                            transaction.replace(R.id.frame_layout, homeFragment).commit();
                        }
                    }
                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(StudentHomeActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StudentHomeActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StudentHomeActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    utils.setAlert(StudentHomeActivity.this, "", response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<GetSchoolDuration> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                utils.setAlert(StudentHomeActivity.this, "", t.getMessage());
            }
        });
    }

    private void setUpNavigationView() {

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setItemIconTintList(null);
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                showToolbar();
                isFgYearBookOpen = false;
                //Check to see which item was being clicked and perform appropriate action
                drawerLayout.closeDrawers();
                toolbar.findViewById(R.id.view_icon).setVisibility(View.GONE);
                switch (menuItem.getItemId()) {
                    case R.id.action_home:
                        navItemIndex = 0;

                        toolbarVisibility(1, "");
                        getSupportActionBar().setElevation(5f);
//                        if (1==0){
                        if (loginUser.getUserData().getIsAccessCodeVerify() != null && loginUser.getUserData().getIsAccessCodeVerify().equalsIgnoreCase("Yes")) {
                            homeFragment = new HomeFragment();
                            fragment = homeFragment;
                        } else {
                            accessCodeFragment = new AccessCodeDisable();
                            fragment = accessCodeFragment;
                        }
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.frame_layout, fragment).commit();
                        break;

                    case R.id.action_yearbook:
                        navItemIndex = 1;
//                        hideToolbar();
                        toolbar.findViewById(R.id.view_icon).setVisibility(View.VISIBLE);
                        isFgYearBookOpen = true;
                        toolbarVisibility(2, "Yearbooks");
                        getSupportActionBar().setElevation(5f);
                        yearbookFragment = new YearbookFragment();
//                        yearbookFragment = new YearBookFG();
                        fragment = yearbookFragment;
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.frame_layout, yearbookFragment).commit();
                        break;

                    case R.id.action_search:
                        navItemIndex = 2;
                        toolbarVisibility(2, "Search");
                        getSupportActionBar().setElevation(5f);
                        searchFragment = new SearchFragment();
                        fragment = searchFragment;
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.frame_layout, searchFragment).commit();
                        break;

                    case R.id.action_profile:
                        navItemIndex = 3;

                        toolbarVisibility(2, "" + schoolName);
                        getSupportActionBar().setElevation(0f);
                        profileFragment = new ProfileFragment();
                        fragment = profileFragment;
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.frame_layout, profileFragment).commit();
                        break;

                    default:
                        navItemIndex = 0;

                        toolbarVisibility(1, "");
                        getSupportActionBar().setElevation(5f);
                        homeFragment = new HomeFragment();
                        fragment = homeFragment;
                        //HomeFragmentTest homeFragment = new HomeFragmentTest();
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.frame_layout, homeFragment).commit();
                        break;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                return true;
            }
        });


        actionBarDrawerToggle = new ActionBarDrawerToggle(StudentHomeActivity.this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                AllMethods.hideKeyboard(StudentHomeActivity.this, getCurrentFocus());
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                actionBarDrawerToggle.setHomeAsUpIndicator(R.drawable.menu_ic);
                actionBarDrawerToggle.syncState();
            }
        });

        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });


        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        //Closing drawer on item click
        drawerLayout.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);  // OPEN DRAWER
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("TTT", "Resulttt code: " + requestCode);
        if (requestCode == 111 && resultCode == RESULT_OK) {
            Log.d("TTTT", "Home activity onActivityResult: " + requestCode);
            //bnve.setSelectedItemId(R.id.action_home);`
            homeFragment = new HomeFragment();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.frame_layout, homeFragment).commit();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("TTT", "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TTT", "onResume");
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        /*if (isFgYearBookOpen) {
            yearbookFragment.onBackPressed();
        }*/
        if (PlayerManager.getInstance().onBackPressed()) {
            return;
        } else {
            finishDialog();
        }
    }

    public void finishDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(StudentHomeActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alert = dialogBuilder.create();
        alert.setCancelable(true);
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
        tvMessage.setText("Are you sure you want to exit?");

        TextView tvYes = dialogView.findViewById(R.id.tvYes);
        TextView tvNo = dialogView.findViewById(R.id.tvNo);

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        alert.show();
    }

    public void setToolbar() {
        Log.d("TTT", "Toolbar: " + studentName);
        toolbar = findViewById(R.id.toolbar);
        toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title_left = toolbar.findViewById(R.id.toolbar_title_left);
        // toolbar_title.setText("divya");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void hideToolbar() {
        getSupportActionBar().hide();
        mainToolbar.setVisibility(View.GONE);
        CoordinatorLayout.LayoutParams param = (CoordinatorLayout.LayoutParams) findViewById(R.id.frame_layout).getLayoutParams();
        param.setBehavior(null);
        int paddingDp = 25;
        float density = getResources().getDisplayMetrics().density;
        int paddingPixel = (int) (paddingDp * density);
        findViewById(R.id.frame_layout).setPadding(0, paddingPixel, 0, 0);
    }

    public void showToolbar() {
        getSupportActionBar().show();
        mainToolbar.setVisibility(View.VISIBLE);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) findViewById(R.id.frame_layout).getLayoutParams();
        params.setBehavior(new AppBarLayout.ScrollingViewBehavior());
        findViewById(R.id.frame_layout).requestLayout();
        findViewById(R.id.frame_layout).setPadding(0, 0, 0, 0);
    }

    public void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void closeDrawer() {
        drawerLayout.closeDrawers();
    }


    private void getUserProfileData() {
        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<JsonObject> userPost = retrofitClass.get_user_profile(userId, schoolId);
        userPost.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    JsonObject res = response.body();
                    JSONObject jsonObject = new JSONObject(res.toString());
                    if (jsonObject.getString("ResponseCode").equals(String.valueOf(1))) {
                        String userDetail = jsonObject.getJSONObject("user_profile").getString("user_detail");
                        StaffStudent dd = new Gson().fromJson(userDetail, StaffStudent.class);
                        if (dd != null) {
                            loginUser.setUserData(dd);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
            }
        });
    }
}

package com.app.yearbook.fragment;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.yearbook.ImageActivity;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.NewImageVideoActivity;
import com.app.yearbook.NewPollActivity;
import com.app.yearbook.NewTextPostActivity;
import com.app.yearbook.R;
import com.app.yearbook.SearchActivity;
import com.app.yearbook.adapter.AdapterPostList;
import com.app.yearbook.adapter.StaffUserListAdapter;
import com.app.yearbook.databinding.LayoutAddPostBinding;
import com.app.yearbook.interfaces.OnMenuOptionClick;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.model.postmodels.StaffPostResponse;
import com.app.yearbook.model.staff.livefeed.StaffListItem;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.studenthome.LikeUnlike;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.Constants;
import com.app.yearbook.utils.OptionMenuController;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffHomeFragment1 extends Fragment implements
        SwipyRefreshLayout.OnRefreshListener,
        View.OnClickListener {

    private static final String TAG = "StaffHomeFragment";
    public View view;
    public ProgressDialog progressDialog;
    private ArrayList<StaffListItem> userListArray;
    public static ArrayList<PostListItem> PostListArray;
    private RecyclerView rvPostList, rvUserList;
    private LoginUser loginUser;
    public int schoolId, userId, page = 1;
    public static AdapterPostList postListAdapter;
    public StaffUserListAdapter userListAdapter;
    public AllMethods utils;
    private LinearLayout lvNoData;
    private SwipyRefreshLayout swipyrefreshlayout;
    FloatingActionButton btnAddPost;
    BottomSheetDialog addPostDialog;
    ConstraintLayout cvDim;
    public int NEW_POST=178;

    RetrofitClass retrofitClass;
    public static Handler handler;
    Context ctx;

    public StaffHomeFragment1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//         ((StaffHomeActivity) getActivity()).getSupportActionBar().setTitle("ewrf");
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.staff_fragment_home, container, false);
        addPostDialog = new BottomSheetDialog(getContext());
        ctx = getActivity();

        initHandler();
        View();
        return view;
    }

    private void initHandler() {
        handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                if (msg.what == Constants.SHOW_PICKER) {
                    showAddPostDialog();
                }
                return false;
            }
        });
    }

    public void View() {
        //utils
        utils = new AllMethods();
        retrofitClass = APIClient.getClient().create(RetrofitClass.class);


        //swipyrefreshlayout
        swipyrefreshlayout = view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setOnRefreshListener(this);

        //SP object
        loginUser = new LoginUser(getActivity());
        if (loginUser.getUserData() != null) {
            schoolId = Integer.parseInt(loginUser.getUserData().getSchoolId());
            userId = Integer.parseInt(loginUser.getUserData().getStaffId());
        }

        //swipe
//        swipeContainer=view.findViewById(R.id.swipeRefreshLayout);
//        swipeContainer.setOnRefreshListener(this);

        //lv
        lvNoData = view.findViewById(R.id.lvNoData);

        //rv
        rvPostList = view.findViewById(R.id.rvPostList);
        rvUserList = view.findViewById(R.id.rvUserList);

        userListArray = new ArrayList<>();
        PostListArray = new ArrayList<>();

        cvDim=view.findViewById(R.id.cvDim);
        /*btnAddPost = view.findViewById(R.id.btn_add_post);
        btnAddPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddPostDialog();
            }
        });*/

        getStudentPost(page);

    }

    LayoutAddPostBinding addPostBinding;

    private void showAddPostDialog() {
        if (addPostBinding == null) {
            addPostBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.layout_add_post, null, false);
            addPostDialog.setContentView(addPostBinding.getRoot());
            addPostBinding.btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (addPostDialog.isShowing()) {
                        addPostDialog.dismiss();
                    }
                }
            });
            addPostBinding.lnrText.setOnClickListener(this);
            addPostBinding.lnrImage.setOnClickListener(this);
            addPostBinding.lnrVideo.setOnClickListener(this);
            addPostBinding.lnrPoll.setOnClickListener(this);
        }
        if (!addPostDialog.isShowing()) {
            addPostDialog.show();
        }
    }

    public void getStudentPost(int page) {
        Log.d("TTT", "Page: " + page);
        if (page == 1) {
            Log.d("TTT", "GetPost: " + userId + " / " + schoolId);
            //Progress bar
            progressDialog = ProgressDialog.show(getActivity(), "", "", true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.progress_view);
            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
            Circle bounce = new Circle();
            bounce.setColor(Color.BLACK);
            progressBar.setIndeterminateDrawable(bounce);

            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);

            final Call<StaffPostResponse> userPost = retrofitClass.getStaffPostNew(userId, schoolId, page);
            userPost.enqueue(new Callback<StaffPostResponse>() {
                @Override
                public void onResponse(Call<StaffPostResponse> call, Response<StaffPostResponse> response) {
                    swipyrefreshlayout.setRefreshing(false);
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.body().getResponseCode().equals("1")) {
                        lvNoData.setVisibility(View.GONE);
                        if (response.body().getUserPost().getStaffList().size() > 0) {
                            /*Log.d("TTT", "User post: " + response.body().getUserPost().getStaffList().size());
                            userListArray.clear();
                            userListArray.addAll(response.body().getUserPost().getStaffList());
                            Log.d("TTT", "User size: " + userListArray.size());
                            userListAdapter = new StaffUserListAdapter(userListArray, getActivity(), "staff");
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                            rvUserList.setLayoutManager(mLayoutManager);
                            rvUserList.setAdapter(userListAdapter);*/
                        }
                        if (response.body().getUserPost().getPostList().size() > 0) {

                            PostListArray.clear();
                            PostListArray.addAll(response.body().getUserPost().getPostList());
                            Log.d("TTT", "PostListArray: " + PostListArray.get(0).getTotalRepoted());
                            // Toast.makeText(getActivity(),"Size: "+PostListArray.size(),Toast.LENGTH_SHORT).show();
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            rvPostList.setLayoutManager(mLayoutManager);
                            postListAdapter = new AdapterPostList(PostListArray, new OnRecyclerClick() {
                                @Override
                                public void onVote(int pos, String pollOptionId) {
                                    addPollVote(pos, pollOptionId);
                                }

                                @Override
                                public void onClick(int pos, int type) {
                                    switch (type) {
                                        case Constants.TYPE_BOOKMARK:
                                            if (!PostListArray.get(pos).getPostBookmark()) {
                                                PostListArray.get(pos).setPostBookmark("True");
                                            } else {
                                                PostListArray.get(pos).setPostBookmark("False");
                                            }
                                            addPostBookmark(PostListArray.get(pos));
                                            break;
                                        case Constants.TYPE_LIKE:
                                            if (!PostListArray.get(pos).getPostLike()) {
                                                PostListArray.get(pos).setPostLike("True");
                                                String likes = String.valueOf(Integer.parseInt(PostListArray.get(pos).getTotalLike()) + 1);
                                                PostListArray.get(pos).setTotalLike(likes);
                                            } else {
                                                PostListArray.get(pos).setPostLike("False");
                                                String likes = String.valueOf(Integer.parseInt(PostListArray.get(pos).getTotalLike()) - 1);
                                                PostListArray.get(pos).setTotalLike(likes);
                                            }
                                            addPostLikeDislike(PostListArray.get(pos));
                                            break;
                                        case Constants.TYPE_VIEW_IMAGE:
                                            Intent i = new Intent(getActivity(), ImageActivity.class);
                                            i.putExtra("caption", PostListArray.get(pos).getPostDescription());
                                            i.putExtra("FileUrl", PostListArray.get(pos).getPostFile());
                                            startActivity(i);
                                            break;
                                    }
                                }
                            },false,true);

                            OptionMenuController controller = new OptionMenuController(getActivity(), postListAdapter, (ArrayList<PostListItem>) PostListArray);
                            controller.setOptionMenu(new OnMenuOptionClick() {
                                @Override
                                public void onClick(int pos, int type, View view) {
                                    if (PostListArray.get(pos).getPostType().equalsIgnoreCase(Constants.POST_TEXT_INT)) {
                                        startActivityForResult(new Intent(getActivity(), NewTextPostActivity.class)
                                                .putExtra(Constants.INTENT_ISEDIT, true)
                                                .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), Constants.NEW_POST);
                                    } else if (PostListArray.get(pos).getPostType().equalsIgnoreCase(Constants.POST_IMAGE_VIDEO_INT)) {
                                        startActivityForResult(new Intent(getActivity(), NewImageVideoActivity.class)
                                                .putExtra(Constants.POST_TYPE, (PostListArray.get(pos).getPostFileType().equalsIgnoreCase("1")) ? Constants.POST_IMAGE : Constants.POST_VIDEO)
                                                .putExtra(Constants.INTENT_ISEDIT, true)
                                                .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), Constants.NEW_POST);
                                    } else {
                                        startActivityForResult(new Intent(getActivity(), NewPollActivity.class)
                                                .putExtra(Constants.INTENT_ISEDIT, true)
                                                .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), Constants.NEW_POST);
                                    }
                                }
                            });

                            /*postListAdapter.setPopupOpenListener(new OnMenuOptionClick() {
                                @Override
                                public void onClick(final int pos, int type, View view) {
                                    PopupMenu popup = new PopupMenu(getActivity(), view);
                                    //Inflating the Popup using xml file
                                    popup.getMenuInflater().inflate(R.menu.feeds_post_option, popup.getMenu());

                                    //registering popup with OnMenuItemClickListener
                                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                        public boolean onMenuItemClick(MenuItem item) {
                                            Log.e("You Clicked : ","" + item.getTitle());
                                            if (item.getTitle().toString().equalsIgnoreCase("edit")) {
                                                if (PostListArray.get(pos).getPostType().equalsIgnoreCase(Constants.POST_TEXT_INT)) {
                                                    startActivityForResult(new Intent(getActivity(), NewTextPostActivity.class)
                                                            .putExtra(Constants.INTENT_ISEDIT, true)
                                                            .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), NEW_POST);
                                                }else if (PostListArray.get(pos).getPostType().equalsIgnoreCase(Constants.POST_IMAGE_VIDEO_INT)) {
                                                    startActivityForResult(new Intent(getActivity(), NewImageVideoActivity.class)
                                                            .putExtra(Constants.POST_TYPE,  (PostListArray.get(pos).getPostFileType().equalsIgnoreCase("1"))?Constants.POST_IMAGE:Constants.POST_VIDEO)
                                                            .putExtra(Constants.INTENT_ISEDIT, true)
                                                            .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), NEW_POST);
                                                }else {
                                                    startActivityForResult(new Intent(getActivity(), NewPollActivity.class)
                                                            .putExtra(Constants.INTENT_ISEDIT, true)
                                                            .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), NEW_POST);
                                                }
                                            }else{
                                                openDeletePopUp(pos);
                                            }
                                            return true;
                                        }
                                    });

                                    popup.show();
                                }
                            });*/

                            rvPostList.setAdapter(postListAdapter);

                            ViewCompat.setNestedScrollingEnabled(view.findViewById(R.id.nestedScrollView), false);
                            if (PostListArray.size()>5){
                                swipyrefreshlayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                            }else {
                                swipyrefreshlayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                            }
                        }
                    } else if (response.body().getResponseCode().equals("10")) {
                        Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        LoginUser loginSP = new LoginUser(getActivity());
                        loginSP.clearData();

                        Intent i = new Intent(getActivity(), LoginActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        //utils.setAlert(getActivity(), "", response.body().getResponseMsg());
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<StaffPostResponse> call, Throwable t) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    utils.setAlert(getActivity(), "", t.getMessage());
                }
            });
        } else {
            Log.d("TTT", "GetPost: " + userId + " / " + schoolId);

            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);

            final Call<StaffPostResponse> userPost = retrofitClass.getStaffPostNew(userId, schoolId, page);
            userPost.enqueue(new Callback<StaffPostResponse>() {
                @Override
                public void onResponse(Call<StaffPostResponse> call, Response<StaffPostResponse> response) {
                    swipyrefreshlayout.setRefreshing(false);
                    if (response.body().getResponseCode().equals("1")) {
//                        if (response.body().getUserPost().getUserList().size() > 0) {
//                            Log.d("TTT", "User post: " + response.body().getUserPost().getUserList().size());
//                            userListArray.addAll(response.body().getUserPost().getUserList());
//                            rvUserList.getAdapter().notifyDataSetChanged();
//                        }
                        if (response.body().getUserPost().getPostList().size() > 0) {
                            PostListArray.addAll(response.body().getUserPost().getPostList());
                            Log.d("TTT", "PostListArray: " + PostListArray.get(0).getTotalRepoted());
                            rvPostList.getAdapter().notifyItemInserted(PostListArray.size() - response.body().getUserPost().getPostList().size());
                        }
                    } else if (response.body().getResponseCode().equals("10")) {
                        Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        LoginUser loginSP = new LoginUser(getActivity());
                        loginSP.clearData();

                        Intent i = new Intent(getActivity(), LoginActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<StaffPostResponse> call, Throwable t) {
                    swipyrefreshlayout.setRefreshing(false);
                    utils.setAlert(getActivity(), "", t.getMessage());
                }
            });
        }
    }

    private void openDeletePopUp(final int pos) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        callDeletePostApi(pos);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(getString(R.string.delete_msg)).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void callDeletePostApi(final int pos) {
        progressDialog = ProgressDialog.show(ctx, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> deletePost = retrofitClass.deletePost(Integer.parseInt(PostListArray.get(pos).getPostId()));
        deletePost.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                    PostListArray.remove(pos);
                    postListAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(ctx, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(ctx, t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addPollVote(final int pos, String pollOptionId) {
        Call<Login> addBookMark = retrofitClass.getUserVotePoll(pollOptionId, LoginUser.getUserData().getStaffId(), LoginUser.getUserTypeKey());
        addBookMark.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Log.d(TAG, "onResponse: ");
                postListAdapter.notifyItemChanged(pos);
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }

    private void addPostLikeDislike(final PostListItem postListItem) {
        Call<LikeUnlike> addBookMark = retrofitClass.postLikeDislike(Integer.parseInt(loginUser.getUserData().getStaffId()),
                Integer.parseInt(postListItem.getPostId()),
                loginUser.getUserTypeKey());
        addBookMark.enqueue(new Callback<LikeUnlike>() {
            @Override
            public void onResponse(Call<LikeUnlike> call, Response<LikeUnlike> response) {
                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<LikeUnlike> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }

    private void addPostBookmark(PostListItem postListItem) {
        Call<Login> addBookMark = retrofitClass.addBookmarkUnbookmark(LoginUser.getUserData().getStaffId(), postListItem.getPostId());
        addBookMark.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d("TTT", "onRefresh: " + direction);
        if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            Log.d("TTT", "onRefresh bottom swipeeee");
            swipyrefreshlayout.setRefreshing(true);
            page = page + 1;
            getStudentPost(page);
        } else if (direction == SwipyRefreshLayoutDirection.TOP) {
            Log.d("TTT", "onRefresh top swipeeee");
            swipyrefreshlayout.setRefreshing(true);
            page = 1;
            getStudentPost(page);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    SearchView searchView;
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.searchview, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setQueryHint("Search users, tags, post and polls");
        ImageView v = searchView.findViewById(R.id.search_button);
        v.setImageResource(R.drawable.ic_search);

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvDim.setVisibility(View.VISIBLE);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!query.equals("")) {
                    searchView.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(searchView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                    cvDim.setVisibility(View.GONE);
                    Intent intent = new Intent(getActivity(), SearchActivity.class);
                    intent.putExtra("query", query);
                    intent.putExtra("type", "staff");
                    intent.putExtra("PostListArray", PostListArray);
                    startActivity(intent);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //cvDim.setVisibility(View.VISIBLE);
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                Log.d(TAG, "onClose: ");
                cvDim.setVisibility(View.GONE);
                return false;
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            /*if (StaffHomeActivity.handler != null) {
                StaffHomeActivity.handler.sendEmptyMessage(Constants.SEARCH);
            }*/
            Log.d(TAG, "onOptionsItemSelected: ");
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            intent.putExtra("type", "staff");
            intent.putParcelableArrayListExtra("PostListArray", PostListArray);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v == addPostBinding.lnrText) {
            hideDialog();
            startActivityForResult(new Intent(getActivity(), NewTextPostActivity.class),NEW_POST);
        } else if (v == addPostBinding.lnrImage) {
            hideDialog();
            Intent intent = new Intent(getActivity(), NewImageVideoActivity.class);
            intent.putExtra(Constants.POST_TYPE, Constants.POST_IMAGE);
            startActivityForResult(intent,NEW_POST);
        } else if (v == addPostBinding.lnrVideo) {
            hideDialog();
            Intent intent = new Intent(getActivity(), NewImageVideoActivity.class);
            intent.putExtra(Constants.POST_TYPE, Constants.POST_VIDEO);
            startActivityForResult(intent,NEW_POST);
        } else if (v == addPostBinding.lnrPoll) {
            hideDialog();
            startActivityForResult(new Intent(getActivity(), NewPollActivity.class),NEW_POST);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: Staff home fragmnet: "+resultCode);
        if (requestCode == NEW_POST) {
            if (resultCode == getActivity().RESULT_OK) {
                getStudentPost(1);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void hideDialog() {
        if (addPostDialog.isShowing()) {
            addPostDialog.dismiss();
        }
    }
}

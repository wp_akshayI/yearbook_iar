
import { action, global } from '../utils'
import { postResponse, failerResponse } from '../utils/funtions'

export function uploadVideo (video_file, thumb_image) {
  return async (dispatch, getState) => {
    dispatch({ type: action.open_notification, message: 'Video uploading...', variant: 'loader' })
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      video_file,
      thumb_image
    }
    const res = await postResponse('add_temp_video.php', request)
    if (res.Result === false) {
      dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: 'error' })
      failerResponse('add_temp_video', res)
    } else {
      dispatch({ type: action.close_notification })
    }
    return res
  }
}

export function uploadImage (image) {
  return async (dispatch, getState) => {
    dispatch({ type: action.open_notification, message: 'Image uploading...', variant: 'loader' })
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      image
    }
    const res = await postResponse('add_temp_image.php', request)
    if (res.Result === false) {
      dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: 'error' })
      failerResponse('add_temp_image', res)
    } else {
      dispatch({ type: action.close_notification })
    }
    return res
  }
}

export function removeTampfile (flag, url) {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    const request = {
      Token: authUser,
      flag,
      // thumb_url: flag === 1 && url,
      file_url: url
    }
    await postResponse('remove_temp_file.php', request)
  }
}

export function uploadGalleryImage (images) {
  return async (dispatch, getState) => {
    dispatch({ type: action.open_notification, message: 'Image uploading...', variant: 'loader' })
    const { authUser } = getState().auth

    let req = {}
    for (let i = 0; i < images.length; i++) {
      const reqName = `images[${i}]`
      req = { ...req, [reqName]: images[i] }
    }
    const request = {
      Token: authUser,
      ...req
    }
    const res = await postResponse('add_images_of_gallery.php', request)
    if (res.Result === false) {
      dispatch({ type: action.open_notification, message: res.ResponseMsg, variant: 'error' })
      failerResponse('add_images_of_gallery', res)
    } else {
      dispatch({ type: action.close_notification })
    }
    return res.Data
  }
}

// Student Grade list
export function get_students_grade () {
  return async (dispatch, getState) => {
    const { authUser } = getState().auth
    const request = {
      Token: authUser
    }
    const res = await postResponse('get_grade_list.php', request)
    if (res.Result === false) failerResponse('get_grade_list', res)
    else {
      dispatch({
        type: action.get_grades,
        payload: res.Data
      })
      return res.Data
    }
  }
}

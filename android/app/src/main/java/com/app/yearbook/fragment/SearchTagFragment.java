package com.app.yearbook.fragment;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.adapter.SearchTagAdapter;
import com.app.yearbook.model.staff.search.GetTegResponse;
import com.app.yearbook.model.staff.search.TagsListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchTagFragment extends Fragment {

    public View view;
    private ArrayList<TagsListItem> getTag;
    public ProgressDialog progressDialog;
    private LoginUser loginUser;
    public AllMethods utils;
    private SearchTagAdapter searchTagAdapter;
    private androidx.appcompat.widget.SearchView searchView;
    private RecyclerView rvSearchTag;
    private int schoolId;
    private LinearLayout lvNoData;
    private String type;

    public SearchTagFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_search_tag, container, false);
        setHasOptionsMenu(true);
        setView();
        return view;
    }

    public void setView()
    {
        type = getArguments().getString("type", "");
        Log.d("TTT","Type: "+type);

        //SP
        loginUser = new LoginUser(getActivity());
        if (loginUser.getUserData() != null) {
            schoolId = Integer.parseInt(loginUser.getUserData().getSchoolId());

            Log.d("TTT","School id: "+schoolId);
        }

        //utils
        utils = new AllMethods();

        //rv
        rvSearchTag = view.findViewById(R.id.rvSearchTag);

        //lv
        lvNoData = view.findViewById(R.id.lvNoData);
        
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.searchview, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (androidx.appcompat.widget.SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setQueryHint("Search hashtags");
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getActivity().getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!query.equals("")) {
                    searchView.clearFocus();
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    searchTag(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void searchTag(String tag) {
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetTegResponse> userPost = retrofitClass.searchTag(tag, "tags","", String.valueOf(schoolId));

        userPost.enqueue(new Callback<GetTegResponse>() {
            @Override
            public void onResponse(Call<GetTegResponse> call, Response<GetTegResponse> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (response.body().getResponseCode().equals("1")) {
                    getTag = new ArrayList<>();
                    if (response.body().getTagsList().size() > 0) {
                        rvSearchTag.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        getTag.addAll(response.body().getTagsList());
                        searchTagAdapter = new SearchTagAdapter(getTag, getActivity(),type);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        rvSearchTag.setLayoutManager(mLayoutManager);
                        rvSearchTag.setAdapter(searchTagAdapter);
                    } else {
                        rvSearchTag.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(getActivity(),response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(getActivity());
                    loginSP.clearData();

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                }
                else {
                    rvSearchTag.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                    // utils.setAlert(getActivity(),"",response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<GetTegResponse> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Log.d("TTT", "onFailure: "+t.getMessage()+" / "+t.getLocalizedMessage());
                utils.setAlert(getActivity(), "", t.getMessage());
            }
        });
    }
}

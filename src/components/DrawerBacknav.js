import React from "react";
import { ListSubheader, IconButton } from "@material-ui/core";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import { colors } from "../utils";

function DrawerBacknav({ title, backClick }) {

    return (
        <ListSubheader
            style={{
                color: "#868686",
                fontSize: 13,
                borderBottomWidth: 1.5,
                borderTopWidth: 0,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderStyle: "solid",
                borderColor: "#b2b2b2",
                display: "flex",
                alignItems: "center",
                zIndex: 9999
            }}>
            {
                backClick && (
                    <IconButton
                        onClick={() => backClick()}
                        style={{ marginLeft: -10 }}
                    >
                        <KeyboardArrowLeft style={{ color: colors.LightGray, fontSize: 26 }} />
                    </IconButton>
                )
            }
            {title}
        </ListSubheader>
    )
}

export default DrawerBacknav;
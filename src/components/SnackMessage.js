import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { action } from '../utils'
import CircularProgress from '@material-ui/core/CircularProgress'
import clsx from 'clsx'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import ErrorIcon from '@material-ui/icons/Error'
import InfoIcon from '@material-ui/icons/Info'
import CloseIcon from '@material-ui/icons/Close'
import { amber, green } from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import WarningIcon from '@material-ui/icons/Warning'
import { makeStyles } from '@material-ui/core/styles'

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
  default: undefined,
  loader: undefined
}

const useStyles1 = makeStyles(theme => ({
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: theme.palette.error.dark
  },
  info: {
    backgroundColor: theme.palette.primary.main
  },
  warning: {
    backgroundColor: amber[700]
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1)
  },
  message: {
    display: 'flex',
    alignItems: 'center'
  },
  progress: {
    color: '#fff',
    marginRight: 8
  }
}))

function SnackMessage () {
  const dispatch = useDispatch()
  const { snackVisible, snackMessage, snackVariant } = useSelector(({ navigation }) => navigation)
  const classes = useStyles1()
  const Icon = variantIcon[snackVariant]

  const onClose = () => dispatch({ type: action.close_notification })

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right'
      }}
      open={snackVisible}
      autoHideDuration={snackVariant === 'loader' ? null : 6e3}
      ClickAwayListenerProps={{ onClickAway: () => null }}
      onClose={onClose}
    >
      <SnackbarContent
        className={Icon ? clsx(classes[snackVariant]) : ''}
        aria-describedby='client-snackbar'
        message={
          <span id='client-snackbar' className={classes.message}>
            {Icon && <Icon className={clsx(classes.icon, classes.iconVariant)} />}
            {snackVariant === 'loader' && <CircularProgress className={classes.progress} size={24} thickness={5} />}
            {snackMessage}
          </span>
        }
        action={[
          snackVariant !== 'loader' && (
            <IconButton key='close' aria-label='close' color='inherit' onClick={onClose}>
              <CloseIcon className={classes.icon} />
            </IconButton>
          )
        ]}
      />
    </Snackbar>
  )
}

export default SnackMessage

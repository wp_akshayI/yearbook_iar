package com.app.yearbook;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.yearbook.videoTrimmer.HgLVideoTrimmer;
import com.app.yearbook.videoTrimmer.interfaces.OnHgLVideoListener;
import com.app.yearbook.videoTrimmer.interfaces.OnTrimVideoListener;
import com.app.yearbook.videocompress.CompressListener;
import com.app.yearbook.videocompress.Compressor;
import com.app.yearbook.videocompress.InitListener;

import java.io.File;
import java.util.Scanner;
import java.util.regex.Pattern;

import tcking.github.com.giraffeplayer2.GiraffePlayer;

public class TrimmerActivity extends AppCompatActivity implements OnTrimVideoListener, OnHgLVideoListener {

    private HgLVideoTrimmer mVideoTrimmer;
    // private ProgressDialog mProgressDialog;
    private String path = "", from = "", CategoryId = "", YearbookPost = "";
    private int maxDuration = 60000;
    private Toolbar toolbar;
    private long fileSizeInMB, fileSizeInKB;

    //new lib
    private String currentOutputVideoPath = "/mnt/sdcard/videokit/out.mp4";
    private String cmd;
    double totalSecs = 0;
    private Dialog alert;
    ProgressBar mProgress;
    TextView tvMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trimmer);
        GiraffePlayer.nativeDebug = false;

        setView();
    }

    public void setView() {
        //toolbar
        setToolbar();

        Intent extraIntent = getIntent();
        if (extraIntent != null) {
            path = extraIntent.getStringExtra("selectUri");
            from = extraIntent.getStringExtra("from");

            if (from.equalsIgnoreCase("staff")) {
                CategoryId = getIntent().getStringExtra("CategoryId");

                if (extraIntent.getStringExtra("EditYearPost") != null) {
                    YearbookPost = extraIntent.getStringExtra("EditYearPost");
                }
            }
        }

        //setting progressbar
//        mProgressDialog = new ProgressDialog(this);
//        mProgressDialog.setCancelable(false);
//        mProgressDialog.setMessage("");

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrimmerActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_progress, null);
        dialogBuilder.setView(dialogView);
        alert = dialogBuilder.create();
        alert.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        alert.setCancelable(false);
        Resources res = getResources();
        Drawable drawable = res.getDrawable(R.drawable.circular);
        mProgress = dialogView.findViewById(R.id.circularProgressbar);
        mProgress.setProgress(0);   // Main Progress
        mProgress.setSecondaryProgress(100); // Secondary Progress
        mProgress.setMax(100); // Maximum Progress
        mProgress.setProgressDrawable(drawable);
        tvMessage = dialogView.findViewById(R.id.tv);

        mVideoTrimmer = findViewById(R.id.timeLine);
        if (mVideoTrimmer != null) {
            /**
             * get total duration of video file
             */
            Log.e("tg", "maxDuration = " + maxDuration);
            mVideoTrimmer.setMaxDuration(maxDuration);
            mVideoTrimmer.setOnTrimVideoListener(this);
            mVideoTrimmer.setOnHgLVideoListener(this);
            mVideoTrimmer.setVideoURI(Uri.parse(path));
            mVideoTrimmer.setVideoInformationVisibility(true);
        }

    }

    @Override
    public void onTrimStarted() {
        tvMessage.setText("0%");
        if (!alert.isShowing() && !isFinishing()) {
            alert.show();
        }
        //mProgressDialog.show();
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Video");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void getResult(final Uri contentUri) {
        //mProgressDialog.cancel();
        //without compress
//        mProgressDialog.cancel();
//        String path = contentUri.getPath();
//        File convertFile = new File(path);
//        if (from.equalsIgnoreCase("staff")) {
//            if (YearbookPost.equalsIgnoreCase("StaffPost")) {
//                Intent i = new Intent();
//                i.putExtra("Type", "Video");
//                i.putExtra("selectUri", Uri.fromFile(convertFile).toString());
//                setResult(RESULT_OK, i);
//                finish();
//            } else {
//                Intent i = new Intent(getApplicationContext(), AddYearbookPostActivity.class);
//                i.putExtra("CategoryId", CategoryId);
//                i.putExtra("Type", "Video");
//                i.putExtra("selectUri", Uri.fromFile(convertFile).toString());
//                startActivityForResult(i, 222);
//            }
//        } else if (from.equalsIgnoreCase("library")) {
//            Intent i = new Intent(getApplicationContext(), AddPostActivity.class);
//            i.putExtra("Type", "Video");
//            i.putExtra("selectUri", Uri.fromFile(convertFile).toString());
//            startActivityForResult(i, 222);
//        }


        //with compress
        //check size of file if size is >6 than need to compress.
        Log.d("TTT", "Grt uri: " + path + " / " + from);
        String path = contentUri.getPath();
        File convertFile = new File(path);
        fileSizeInKB = convertFile.length() / 1024;
        Log.d("TTT", "New file size in KBBBB: " + fileSizeInKB);

        if (fileSizeInKB > 1000) {
            fileSizeInMB = fileSizeInKB / 1024;
            Log.d("TTT", "New file size in MB: " + fileSizeInMB);
        } else {
            Log.d("TTT", "New file size in KB: " + fileSizeInKB);
        }

        if (fileSizeInMB > 10) {
//            if (!mProgressDialog.isShowing()) {
//                mProgressDialog.show();
//            }

            if (!alert.isShowing()) {
                alert.show();
            }
            // mCircleDrawable.stop();
            Log.d("TTT", "Need to compress");

//            AsyncTask<String, String, String> task = null;
//
//            try {
//                //Uri path=Uri.parse(holder.binding.getData().getPostFile());
//                //File fileThumbImage = new File(holder.binding.getData().getPostFile());
//                task =new abv(path).execute();
//
//            } catch (Throwable throwable) {
//                throwable.printStackTrace();
//            }

            newCompress(path);
        } else {
            Log.d("TTT", "No need to compress");
            //mProgressDialog.cancel();
            if (alert.isShowing())
                alert.dismiss();
            //file size in KB or MB < 6
            if (from.equalsIgnoreCase("staff")) {
                if (YearbookPost.equalsIgnoreCase("StaffPost")) {
                    Intent i = new Intent();
                    i.putExtra("Type", "Video");
                    i.putExtra("selectUri", Uri.fromFile(convertFile).toString());
                    setResult(RESULT_OK, i);
                    finish();
                } else {
                    Intent i = new Intent(getApplicationContext(), AddYearbookPostActivity.class);
                    i.putExtra("CategoryId", CategoryId);
                    i.putExtra("Type", "Video");
                    i.putExtra("selectUri", Uri.fromFile(convertFile).toString());
                    startActivityForResult(i, 222);
                }
            } else if (from.equalsIgnoreCase("library")) {
                //compress(path);
                //newCompress(path);
//                Intent i = new Intent(getApplicationContext(), AddPostActivity.class); // oldFlow
                Intent i = new Intent();
                i.putExtra("Type", "Video");
                i.putExtra("selectUri", Uri.fromFile(convertFile).toString());
                i.putExtra("path", path);
                setResult(RESULT_OK, i); // newFlow
                finish(); // newFlow
//                startActivityForResult(i, 222); // oldFlow
            }

        }

    }

//    public class abv extends AsyncTask<String, String, String> {
//        private String data;
//
//        public abv(String data) {
//            this.data = data;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            String s = null;
//            FFmpegMediaMetadataRetriever retriever = new FFmpegMediaMetadataRetriever();
//
//            try {
//                retriever.setDataSource(data);
//                s = retriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
//                Log.d("TTT","Orientation Trimmer: "+s);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            retriever.release();
//
//            return s;
//        }
//
//        @Override
//        protected void onPostExecute(String orientation) {
//            super.onPostExecute(orientation);
//            Log.d("TTT", "Task Trimmer: " + orientation);
//        }
//    }

//    private String getRealPathFromURI(Uri contentUri) {
//        String[] proj = {MediaStore.Images.Media.DATA};
//        CursorLoader loader = new CursorLoader(TrimmerActivity.this, contentUri, proj, null, null, null);
//        Cursor cursor = loader.loadInBackground();
//        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//        cursor.moveToFirst();
//        String result = cursor.getString(column_index);
//        cursor.close();
//        return result;
//    }

//    private void playUriOnVLC(Uri uri) {
//
//        int vlcRequestCode = 42;
//        Intent vlcIntent = new Intent(Intent.ACTION_VIEW);
//        vlcIntent.setPackage("org.videolan.vlc");
//        vlcIntent.setDataAndTypeAndNormalize(uri, "video/*");
//        vlcIntent.putExtra("title", "Kung Fury");
//        vlcIntent.putExtra("from_start", false);
//        vlcIntent.putExtra("position", 90000l);
//        startActivityForResult(vlcIntent, vlcRequestCode);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 222 && resultCode == RESULT_OK) {
            Log.d("TTTT", "ImageFilterActivity onActivityResult");
            Intent i = getIntent();
            setResult(RESULT_OK, i);
            finish();
        }
    }

    @Override
    public void cancelAction() {
        //mProgressDialog.cancel();
        if (alert.isShowing()) {
            alert.dismiss();
        }
        mVideoTrimmer.destroy();
        finish();
    }

    @Override
    public void onError(final String message) {
//        mProgressDialog.cancel();
        if (alert.isShowing()) {
            alert.dismiss();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(TrimmerActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onVideoPrepared() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Toast.makeText(TrimmerActivity.this, "onVideoPrepared", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    public void compress(String path)
//    {
//        Log.d("TTT","Compress: "+path);
//        VideoCompressor.compress(this, path, new VideoCompressListener() {
//            @Override
//            public void onSuccess(final String outputFile, String filename, long duration) {
//
//                mProgressDialog.cancel();
//
//                Log.d("TTT","Onsucess: "+outputFile+" / "+filename+" / "+duration);
//
//                Worker.postMain(new Runnable() {
//                    @Override
//                    public void run() {
//                       // Toast.makeText(TrimmerActivity.this,"video compress success:"+outputFile,Toast.LENGTH_SHORT).show();
//                        SGLog.e("video compress success:"+outputFile);
//
//                    }
//                });
//
//                File f=new File(outputFile);
//                Log.d("TTT","New file: "+ Uri.fromFile(f));
//
//
//                long fileSizeInKB = f.length() / 1024;
//                Log.d("TTT","New file size in KBBBB: "+ fileSizeInKB);
//
//                if (fileSizeInKB > 1000) {
//                    long fileSizeInMB = fileSizeInKB / 1024;
//                    Log.d("TTT","New file size in MB: "+ fileSizeInMB);
//                } else {
//                    Log.d("TTT","New file size in KB: "+ fileSizeInKB);
//                }
//
//
//                Intent i = new Intent(TrimmerActivity.this, AddPostActivity.class);
//                i.putExtra("Type", "Video");
//                i.putExtra("selectUri", Uri.fromFile(f).toString());
//                startActivityForResult(i, 222);
//
//            }
//
//            @Override
//            public void onFail(final String reason) {
//                Worker.postMain(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(TrimmerActivity.this,"video compress failed:"+reason,Toast.LENGTH_SHORT).show();
//                        SGLog.e("video compress failed:"+reason);
//                    }
//                });
//            }
//
//            @Override
//            public void onProgress(final int progress) {
//                Worker.postMain(new Runnable() {
//                    @Override
//                    public void run() {
//                        SGLog.e("video compress progress:"+progress);
//
//                    }
//                });
//            }
//        });
//    }

//    public void compress(String path)
//    {
//        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getPackageName() + "/media/videos");
//        if (f.mkdirs() || f.isDirectory())
//            //compress and output new video specs
//            new VideoCompressAsyncTask(this).execute(path, f.getPath());
//    }
//
//
//    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {
//        ProgressBar p=new ProgressBar(TrimmerActivity.this);
//        Context mContext;
//
//        public VideoCompressAsyncTask(Context context) {
//            mContext = context;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            p.setVisibility(View.VISIBLE);
//            Log.d("TTT","Start video compression");
//        }
//
//        @Override
//        protected String doInBackground(String... paths) {
//            String filePath = null;
//            try {
//
//                filePath = SiliCompressor.with(mContext).compressVideo(Uri.parse(paths[0]), paths[1]);
//
//            } catch (URISyntaxException e) {
//                e.printStackTrace();
//            }
//            return filePath;
//        }
//
//        @Override
//        protected void onPostExecute(String compressedFilePath) {
//            super.onPostExecute(compressedFilePath);
//            p.setVisibility(View.GONE);
//            File imageFile = new File(compressedFilePath);
//            float length = imageFile.length() / 1024f; // Size in KB
//            String value;
//            if (length >= 1024)
//                value = length / 1024f + " MB";
//            else
//                value = length + " KB";
//            String text = String.format(Locale.US, "%s\nName: %s\nSize: %s", "video_compression_complete", imageFile.getName(), value);
//            Log.i("Silicompressor", "Path: " + compressedFilePath + " / " + text);
//        }
//    }

    public void newCompress(String path) {
        //mProgressDialog.show();
        File file = new File("/mnt/sdcard/videokit");
        if (!file.exists()) {
            file.mkdirs();
        }

//        cmd = "-y -i " + path + " -strict -2 -vcodec libx264 -preset ultrafast " +
//                "-crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k -s 640x480 -aspect 16:9 " + currentOutputVideoPath;

//        cmd = "-y -i " + path + " -strict -2 -vcodec libx264 -preset ultrafast " +
//                "-crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k " + currentOutputVideoPath;

        //final
//        cmd = "-y -i " + path + " -strict experimental -vcodec libx264 -preset veryfast " +
//                "-crf 24 -acodec aac -ar 44100 -ac 2 -r 10 -b:a 128k -aspect 16:9 " + currentOutputVideoPath;

        // -s 640x480
        // cmd="ffmpeg -i "+path+" -lavfi '[0:v]scale=ih*16/9:-1,boxblur=luma_radius=min(h\\,w)/20:luma_power=1:chroma_radius=min(cw\\,ch)/20:chroma_power=1[bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=h=iw*9/16' -vb 800K "+currentOutputVideoPath;

        // cmd="-i "+path+" -vcodec libx264 -crf 24 "+currentOutputVideoPath;
//        cmd = "-y -i " + path + " -strict experimental -vcodec libx264 -preset medium " +
//                "-crf 24 -acodec aac -b:a 128k -vf scale=-2:720,format=yuv420p " + currentOutputVideoPath;

        // cmd="-i "+path+" -vcodec libx264 -filter:v scale=-1:-720 -c:a copy "+currentOutputVideoPath;
        // cmd= "-y -i " + path + " -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 128k -movflags +faststart -vf scale=-2:720,format=yuv420p " + currentOutputVideoPath;

        //final code // scale=720:-1 // -s 640x480
        // cmd="-y -i "+path+" -s 640x480 -r 30000/1001 -b 200k -bt 240k -vcodec libx264 -preset ultrafast -acodec aac -ac 2 -ar 48000 -ab 192k "+currentOutputVideoPath;


        //final done command 30 -aspect 4:3 2097k -ar 22050 -ab 48000;
        // cmd="-y -i "+path+" -strict experimental -r 30 -ab 48000 -b 1024k -bt 240k -vcodec libx264 -preset ultrafast -acodec aac -ac 2 -ar 48000 "+currentOutputVideoPath;

        cmd = "-y -i " + path + " -strict -2 -vcodec libx264 -preset ultrafast -crf 26 -acodec aac -ar 44100 -ac 2 -b:a 96k " + currentOutputVideoPath;

        Log.d("TTT", "Pathhh: " + cmd);

        if (TextUtils.isEmpty(cmd)) {
            Toast.makeText(TrimmerActivity.this, "please input command"
                    , Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(path)) {
            Toast.makeText(TrimmerActivity.this, "video file not found, please record first", Toast.LENGTH_SHORT).show();
        } else {
            file = new File(currentOutputVideoPath);
            if (file.exists()) {
                file.delete();
            }
            execCommand(cmd);
        }
    }

    private void execCommand(String cmd) {
        File mFile = new File(currentOutputVideoPath);
        if (mFile.exists()) {
            mFile.delete();
        }
        Compressor mCompressor = new Compressor(this);
        mCompressor.loadBinary(new InitListener() {
            @Override
            public void onLoadSuccess() {
                Log.v("TTT", "load library succeed");
            }

            @Override
            public void onLoadFail(String reason) {
                Log.i("TTT", "load library fail:" + reason);
            }
        });

        mCompressor.execCommand(cmd, new CompressListener() {
            @Override
            public void onExecSuccess(String message) {
//                mProgressDialog.cancel();
                if (alert.isShowing()) {
                    alert.dismiss();
                }
                Log.i("TTT", "success " + message);

                File f = new File(currentOutputVideoPath);
                Log.d("TTT", "New file: " + Uri.fromFile(f));

                long fileSizeInKB = f.length() / 1024;
                Log.d("TTT", "New file size in KBBBB: " + fileSizeInKB);

                if (fileSizeInKB > 1000) {
                    long fileSizeInMB = fileSizeInKB / 1024;
                    Log.d("TTT", "New file size in MB: " + fileSizeInMB);
                } else {
                    Log.d("TTT", "New file size in KB: " + fileSizeInKB);
                }

                if (from.equalsIgnoreCase("staff")) {
                    if (YearbookPost.equalsIgnoreCase("StaffPost")) {
                        Intent i = new Intent();
                        i.putExtra("Type", "Video");
                        i.putExtra("selectUri", Uri.fromFile(f).toString());
                        setResult(RESULT_OK, i);
                        finish();
                    } else {
                        Intent i = new Intent(getApplicationContext(), AddYearbookPostActivity.class);
                        i.putExtra("CategoryId", CategoryId);
                        i.putExtra("Type", "Video");
                        i.putExtra("selectUri", Uri.fromFile(f).toString());
                        startActivityForResult(i, 222);
                    }
                } else if (from.equalsIgnoreCase("library")) {
//                    Intent i = new Intent(getApplicationContext(), AddPostActivity.class);//oldFLow
                    Intent i = new Intent();
                    i.putExtra("Type", "Video");
                    i.putExtra("selectUri", Uri.fromFile(f).toString());
                    i.putExtra("path", path);
//                    startActivityForResult(i, 222);//oldFlow
                    setResult(RESULT_OK, i);//newFlow
                    finish();
                }
            }

            @Override
            public void onExecFail(String reason) {
//                mProgressDialog.cancel();
                if (alert.isShowing()) {
                    alert.dismiss();
                }
                Log.i("TTT", "fail: " + reason);
            }

            @Override
            public void onExecProgress(String message) {

                Log.i("TTT", "===progress " + message);

                Scanner sc = new Scanner(message);
                Pattern durPattern = Pattern.compile("(?<=Duration:)[^,]*");
                Pattern timePattern = Pattern.compile("(?<=time=)[\\d:.]*");

                String dur = sc.findWithinHorizon(durPattern, 0);
                if (dur != null) {
                    Log.d("TTT", "=====Total duration: " + dur + " seconds.");

                    String[] hms = dur.split(":");
                    totalSecs = Integer.parseInt(hms[0].trim()) * 3600
                            + Integer.parseInt(hms[1].trim()) * 60
                            + Double.parseDouble(hms[2].trim());

                    Log.d("TTT", "=====Total totalSecs: " + totalSecs + " seconds.");
                }

                //calculation
                String match = sc.findWithinHorizon(timePattern, 0);
                if (match != null && !match.equals("")) {
                    Log.d("TTT", "======Match not null====:" + match);
                    String[] matchSplit = match.split(":");
                    if (totalSecs != 0) {
                        Log.d("TTT", "======Match Sec====:" + totalSecs);
                        double progress = (Integer.parseInt(matchSplit[0]) * 3600 +
                                Integer.parseInt(matchSplit[1]) * 60 +
                                Float.parseFloat(matchSplit[2])) / totalSecs;
                        double showProgress = (progress * 100);
//                        mProgressDialog.setMessage((int)showProgress+"%");
                        mProgress.setProgress((int) showProgress);
                        tvMessage.setText((int) showProgress + "%");
                        Log.d("TTT", "=======PROGRESS========" + (int) showProgress);
                    }
                }

            }
        });
    }
}

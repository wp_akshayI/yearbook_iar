package com.app.yearbook.adapter;

import android.app.Activity;

import androidx.appcompat.widget.PopupMenu;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ablanco.zoomy.DoubleTapListener;
import com.ablanco.zoomy.Zoomy;
import com.app.yearbook.R;
import com.app.yearbook.databinding.AdapterMediaPostBinding;
import com.app.yearbook.databinding.AdapterPollPostBinding;
import com.app.yearbook.databinding.AdapterSponserPostBinding;
import com.app.yearbook.databinding.AdapterStaffItemBinding;
import com.app.yearbook.databinding.AdapterTextPostBinding;
import com.app.yearbook.databinding.AdapterUserItemBinding;
import com.app.yearbook.interfaces.OnMenuOptionClick;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.postmodels.PollOption;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.Constants;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.List;

public class AdapterPostList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "AdapterPostList";
    List<PostListItem> postListItems;
    OnRecyclerClick onRecyclerClick;
    OnMenuOptionClick menuOptionClick;
    boolean isBookmarkPost = false;
    boolean isStaff = false;

    public AdapterPostList(List<PostListItem> postLists, OnRecyclerClick onRecyclerClick) {
        this.postListItems = postLists;
        this.onRecyclerClick = onRecyclerClick;
    }

    public AdapterPostList(List<PostListItem> postLists, OnRecyclerClick onRecyclerClick, boolean isBookmarkPost, boolean isStaff) {
        this.postListItems = postLists;
        this.onRecyclerClick = onRecyclerClick;
        this.isBookmarkPost = isBookmarkPost;
        this.isStaff = isStaff;

    }

    public void setPopupOpenListener(OnMenuOptionClick menuOptionClick) {
        this.menuOptionClick = menuOptionClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                AdapterMediaPostBinding mediaPostBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_media_post, parent, false);
                return new MediaPostHolder(mediaPostBinding.getRoot());
            case 2:
                AdapterSponserPostBinding sponsorPostBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_sponser_post, parent, false);
                return new SponorPostHolder(sponsorPostBinding.getRoot());
            case 3:
                AdapterTextPostBinding textPostBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_text_post, parent, false);
                return new TextPostHolder(textPostBinding.getRoot());
            case 4:
                AdapterPollPostBinding pollPostBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_poll_post, parent, false);
                return new PollPostHolder(pollPostBinding.getRoot());
            case 5:
                AdapterUserItemBinding userItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_user_item, parent, false);
                return new UserHolder(userItemBinding.getRoot());
            case 6:
                AdapterStaffItemBinding staffItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_staff_item, parent, false);
                return new StaffHolder(staffItemBinding.getRoot());
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case 1:
                ((MediaPostHolder) holder).mediaPostBinding.setModel(postListItems.get(position));
                ((MediaPostHolder) holder).mediaPostBinding.executePendingBindings();
                /*try {
                    ((MediaPostHolder) holder).mediaPostBinding.imgOption.setVisibility(
                            postListItems.get(position).getStaffId().contentEquals(LoginUser.getUserData().getStaffId())? View.VISIBLE: View.GONE
                    );
                }catch (Exception e){}*/
                break;
            case 2:
                ((SponorPostHolder) holder).mediaPostBinding.setModel(postListItems.get(position));
                ((SponorPostHolder) holder).mediaPostBinding.executePendingBindings();
                break;
            case 3:
                ((TextPostHolder) holder).textPostBinding.setModel(postListItems.get(position));
                ((TextPostHolder) holder).textPostBinding.executePendingBindings();
                break;
            case 5:
                if (postListItems.get(position).getUserType().equalsIgnoreCase("student"))
                    postListItems.get(position).setUserType("Student");
                else if (postListItems.get(position).getUserType().equalsIgnoreCase("staff"))
                    postListItems.get(position).setUserType("Staff");
                else
                    postListItems.get(position).setUserType("Yearbook Student");
                ((UserHolder) holder).binding.setModel(postListItems.get(position));
                ((UserHolder) holder).binding.executePendingBindings();
                break;
            case 6:
                postListItems.get(position).setUserType("Staff");
                ((StaffHolder) holder).binding.setModel(postListItems.get(position));
                ((StaffHolder) holder).binding.executePendingBindings();
                break;
            case 4:
                ((PollPostHolder) holder).pollPostBinding.setModel(postListItems.get(position));
                if (postListItems.get(position).getPollOptions() != null) {
                    final List<PollOption> pollOptions = postListItems.get(position).getPollOptions();

                    boolean isResult = false;
                    try {
                        isResult = postListItems.get(position).getStaffId().contentEquals(LoginUser.getUserData().getStaffId());
                    } catch (Exception e) {
                    }
                    if (isBookmarkPost || isResult) {
                        if (pollOptions.size() > 0) {
                            ((PollPostHolder) holder).pollPostBinding.rcOptions.setAdapter(new AdapterPollResult(pollOptions));
                        }
                    } else {
                        if (pollOptions.size() > 0) {
                            boolean isAnswered = false;
                            //Dev Ashish // block for staff
                            /*if (isStaff) {
                                isAnswered = true;
                            } else {

                                for (PollOption pollOption : pollOptions) {
                                    if (pollOption.getOptionSelect()) {
                                        isAnswered = true;
                                        break;
                                    }
                                }
                            }*/
                            for (PollOption pollOption : pollOptions) {
                                if (pollOption.getOptionSelect()) {
                                    isAnswered = true;
                                    break;
                                }
                            }
                            ///Dev Ashish


                            ((PollPostHolder) holder).pollPostBinding.rcOptions.setAdapter(new AdapterPollOptions(pollOptions, isAnswered, new OnRecyclerClick() {

                                @Override
                                public void onVote(int pos, String pollOptionId) {

                                }

                                @Override
                                public void onClick(int pos, int type) {

                                    for (PollOption pollOption : pollOptions) {
                                        pollOption.setOptionSelect("False");
                                    }
                                    pollOptions.get(pos).setOptionSelect("True");
                                    onRecyclerClick.onVote(position, postListItems.get(position).getPollOptions().get(pos).getOptionId());
                                }
                            }));
                        }
                    }
                }
                ((PollPostHolder) holder).pollPostBinding.executePendingBindings();
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        Log.d(TAG, "getItemViewType: " + postListItems.get(position).getPostType());
        return Integer.parseInt(postListItems.get(position).getPostType());
    }

    @Override
    public int getItemCount() {
        return postListItems.size();
    }

    public class TextPostHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        AdapterTextPostBinding textPostBinding;

        TextPostHolder(View itemView) {
            super(itemView);
            textPostBinding = DataBindingUtil.bind(itemView);
            if (textPostBinding != null) {
                textPostBinding.imgBookmark.setOnClickListener(this);
                textPostBinding.imgLike.setOnClickListener(this);
                textPostBinding.imgOption.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v) {
            if (v == textPostBinding.imgBookmark) {
                YoYo.with(Techniques.RubberBand).duration(500).playOn(textPostBinding.imgBookmark);
                onRecyclerClick.onClick(getAdapterPosition(), Constants.TYPE_BOOKMARK);
            } else if (v == textPostBinding.imgLike) {
                YoYo.with(Techniques.RubberBand).duration(500).playOn(textPostBinding.imgLike);
                onRecyclerClick.onClick(getAdapterPosition(), Constants.TYPE_LIKE);
            } else if (v == textPostBinding.imgOption) {
                if (menuOptionClick != null)
                    menuOptionClick.onClick(getAdapterPosition(), Constants.VIEW_OPTION_MENU, v);
            }
        }
    }

    public class PollPostHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        AdapterPollPostBinding pollPostBinding;

        PollPostHolder(View itemView) {
            super(itemView);
            pollPostBinding = DataBindingUtil.bind(itemView);
            if (pollPostBinding != null) {
                pollPostBinding.imgBookmark.setOnClickListener(this);
                pollPostBinding.imgLike.setOnClickListener(this);
                pollPostBinding.imgOption.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v) {
            if (v == pollPostBinding.imgBookmark) {
                YoYo.with(Techniques.RubberBand).duration(500).playOn(pollPostBinding.imgBookmark);
                onRecyclerClick.onClick(getAdapterPosition(), Constants.TYPE_BOOKMARK);
            } else if (v == pollPostBinding.imgLike) {
                YoYo.with(Techniques.RubberBand).duration(500).playOn(pollPostBinding.imgLike);
                onRecyclerClick.onClick(getAdapterPosition(), Constants.TYPE_LIKE);
            } else if (v == pollPostBinding.imgOption) {
                if (menuOptionClick != null)
                    menuOptionClick.onClick(getAdapterPosition(), Constants.VIEW_OPTION_MENU, v);
            }
        }
    }

    public class MediaPostHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        AdapterMediaPostBinding mediaPostBinding;

        MediaPostHolder(View itemView) {
            super(itemView);
            mediaPostBinding = DataBindingUtil.bind(itemView);
            if (mediaPostBinding != null) {
                mediaPostBinding.imgBookmark.setOnClickListener(this);
                mediaPostBinding.imgPost.setOnClickListener(this);
                Zoomy.Builder builder = new Zoomy.Builder(((Activity) mediaPostBinding.imgPost.getContext()))
                        .enableImmersiveMode(false).target(mediaPostBinding.imgPost)
                        .doubleTapListener(new DoubleTapListener() {
                            @Override
                            public void onDoubleTap(View v) {
                                YoYo.with(Techniques.RubberBand).duration(500).playOn(mediaPostBinding.imgLike);
                                onRecyclerClick.onClick(getAdapterPosition(), Constants.TYPE_LIKE);
                            }
                        });
                builder.register();
                mediaPostBinding.imgLike.setOnClickListener(this);
                mediaPostBinding.imgOption.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v) {
            if (v == mediaPostBinding.imgBookmark) {
                YoYo.with(Techniques.RubberBand).duration(500).playOn(mediaPostBinding.imgBookmark);
                onRecyclerClick.onClick(getAdapterPosition(), Constants.TYPE_BOOKMARK);
            } else if (v == mediaPostBinding.imgLike) {
                YoYo.with(Techniques.RubberBand).duration(500).playOn(mediaPostBinding.imgLike);
                onRecyclerClick.onClick(getAdapterPosition(), Constants.TYPE_LIKE);
            } else if (v == mediaPostBinding.imgPost) {
                if (postListItems.get(getAdapterPosition()).getPostFileType().equalsIgnoreCase("1")) {
                    onRecyclerClick.onClick(getAdapterPosition(), Constants.TYPE_VIEW_IMAGE);
                }
            } else if (v == mediaPostBinding.imgOption) {
                if (menuOptionClick != null)
                    menuOptionClick.onClick(getAdapterPosition(), Constants.VIEW_OPTION_MENU, v);
            }
        }
    }

    public class SponorPostHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        AdapterSponserPostBinding mediaPostBinding;

        SponorPostHolder(View itemView) {
            super(itemView);
            mediaPostBinding = DataBindingUtil.bind(itemView);
            if (mediaPostBinding != null) {
                mediaPostBinding.imgPost.setOnClickListener(this);
                Zoomy.Builder builder = new Zoomy.Builder(((Activity) mediaPostBinding.imgPost.getContext())).enableImmersiveMode(false).target(mediaPostBinding.imgPost);
                builder.register();
                mediaPostBinding.imgPost.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v) {

        }
    }


    public class UserHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        AdapterUserItemBinding binding;

        public UserHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerClick.onClick(getAdapterPosition(), Constants.VIEW_PROFILE);
        }
    }


    public class StaffHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        AdapterStaffItemBinding binding;

        public StaffHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerClick.onClick(getAdapterPosition(), Constants.VIEW_PROFILE);
        }
    }
}

import React, { useEffect, useState } from 'react'
import { Text, TouchableOpacity, Image } from 'react-native'
import { up_arrow_ic, down_arrow_ic } from '../assets'
import { useDispatch } from 'react-redux'
import Actions from '../actions'
import { global, action } from '../utils'

export default function InsideList ({ navigation, screenId }) {
  const dispatch = useDispatch()
  //   const list = useSelector(({ components }) => components.pageInside)

  const [state, setState] = useState({
    expand: false,
    data: null
  })

  useEffect(() => {
    _loadList()
    return () => {
      dispatch({ type: action.page_inside_list })
    }
  }, [])

  const _loadList = async () => {
    const data = await dispatch(Actions.get_pageinside_list(screenId))
    setState({ ...state, data })
  }

  const _toggleExpand = () => {
    setState({ ...state, expand: !state.expand })
  }

  const _navigate = (screenId, title) => {
    if (global.platform === 'web') return
    navigation.push('Category', { screenId, title })
  }

  const renderItem = (item, index) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={{
          backgroundColor: '#363636',
          padding: 15
        }}
        onPress={() => _navigate(item.screen_id, item.name)}
        key={index}
      >
        <Text
          numberOfLines={1}
          ellipsizeMode='tail'
          style={{
            color: '#fff',
            fontSize: 15
          }}
        >
          {item.name}
        </Text>
      </TouchableOpacity>
    )
  }

  return (
    <>
      <TouchableOpacity
        style={{
          backgroundColor: '#252525',
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
          padding: 15
        }}
        onPress={_toggleExpand}
        activeOpacity={0.9}
      >
        <Text
          numberOfLines={1}
          ellipsizeMode='tail'
          style={{
            color: '#fff',
            fontSize: 15
          }}
        >
                    Page Inside
        </Text>
        <Image
          source={state.expand ? up_arrow_ic : down_arrow_ic}
          resizeMode='contain'
          style={{ width: 12, height: 12 }}
        />
      </TouchableOpacity>
      {
        state.expand && state.data && state.data.map(renderItem)
      }
    </>
  )
}

package com.app.yearbook;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.UserImageAdapter;
import com.app.yearbook.databinding.ActivitySchoolUserBinding;
import com.app.yearbook.model.employee.GetImageResponse;
import com.app.yearbook.model.employee.UserListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.OnRecyclerClick;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolUserImageActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "SchoolUserImageActivity";
    private ActivitySchoolUserBinding binding;
    private Toolbar toolbar;
    private ProgressDialog progressDialog;
    private TextView toolbar_title;
    private AllMethods utils;
    //private ArrayList<UserListItem> userPostItems;
    private static final int CAMERA_PERMISSION = 200;
    private static final int CAMERA_RESULT = 201;
    private String imageFilePath, userImageEdit = "";
    private File image;
    private static final int CAMERA_REQUEST = 202;
    private final int EDIT_IMAGE_REQUEST = 203;
    private static final int EDIT_IMAGE_RESULT = 204;
    private static final int EDIT_IMAGE_PERMISSION = 205;
    ArrayList<String> userImagePath;
    ArrayList<String> newUserImagePath;
    String userId, empId;
    private LoginUser loginUser;
    private int currentPosition = 0, imagePosition = 0, position;
    private UserListItem UserData;
    MenuItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_school_user);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_school_user);
        setView();
    }

    public void setView() {
        //set toolbar
        setToolbar();

        binding.btnUploadImage.setOnClickListener(this);
        //binding.cvCamera.setOnClickListener(this);
        utils = new AllMethods();

        //SP object
        loginUser = new LoginUser(SchoolUserImageActivity.this);
        if (loginUser.getUserData() != null) {
            empId = loginUser.getUserData().getEmployeeId();
            Log.d("TTT", "setView: emp id: " + empId);
        }

        newUserImagePath = new ArrayList<>();
        userImagePath = new ArrayList<>();
        if (getIntent().getExtras() != null) {
            UserData = (UserListItem) getIntent().getSerializableExtra("UserData");
            position = getIntent().getIntExtra("position", 0);
            //userImagePath = getIntent().getStringArrayListExtra("userImage");
            userImagePath.clear();
            userImagePath.addAll(UserData.getUserImages());
            userId = UserData.getUserId();
            setPhotoAdapter(position);

            //get all students
            //getSchoolUser(schoolId);
        }
    }


//    private void getSchoolUser(String schoolId) {
//        //Progress bar
//        progressDialog.show();
//
//        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
//        final Call<GetUserSchoolWise> schoolList = retrofitClass.getUserSchool(schoolId);
//        schoolList.enqueue(new Callback<GetUserSchoolWise>() {
//            @Override
//            public void onResponse(Call<GetUserSchoolWise> call, Response<GetUserSchoolWise> response) {
//                userPostItems = new ArrayList<>();
//                userPostItems.add(0, new UserListItem("Select", "User"));
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//                Log.d(TAG, "onResponse: " + response.toString());
//                if (response.body().getResponseCode().equals("1")) {
//
//                    if (response.body().getUserList() != null && response.body().getUserList().size() != 0) {
//                        userPostItems.addAll(response.body().getUserList());
//                        SchoolUsersAdapter saveLoginUsersAdapter = new SchoolUsersAdapter(SchoolUserImageActivity.this, userPostItems);
//                        binding.spinner.setAdapter(saveLoginUsersAdapter);
//                        binding.spinner.setOnItemSelectedListener(listener);
//
//
//                    } else {
//                        utils.setAlert(SchoolUserImageActivity.this, "", response.body().getResponseMsg());
//                    }
//                } else {
//                    utils.setAlert(SchoolUserImageActivity.this, "", response.body().getResponseMsg());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GetUserSchoolWise> call, Throwable t) {
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//                utils.setAlert(SchoolUserImageActivity.this, "", t.getMessage());
//
//            }
//        });
//    }
//
//    private AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
//        @Override
//        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
////            ((TextView) parent.getChildAt(1)).setTextColor(Color.WHITE);
////            LinearLayout ll= (LinearLayout) parent.getChildAt(0);
////            TextView tt=ll.findViewById(R.id.tvUsers);
////            tt.setTextColor(Color.WHITE);
//
//            if (position != 0) {
//
//                currentPosition = position;
//                binding.recyclerViewImage.setVisibility(View.VISIBLE);
//                binding.btnUploadImage.setVisibility(View.VISIBLE);
//
//                userId = userPostItems.get(position).getUserId();
//                newUserImagePath = new ArrayList<>();
//                userImagePath = new ArrayList<>();
//
//                if (userPostItems.get(position).getUserImages() != null && userPostItems.get(position).getUserImages().size() > 0) {
//
//                    userImagePath.addAll(userPostItems.get(position).getUserImages());
//
//                    binding.cvCamera.setVisibility(View.VISIBLE);
////                    if (userPostItems.get(position).getUserImages().size() == 3) {
////                        binding.cvCamera.setVisibility(View.GONE);
////                    } else {
////                        binding.cvCamera.setVisibility(View.VISIBLE);
////                    }
//
//                } else {
//                    binding.cvCamera.setVisibility(View.VISIBLE);
//                }
//
//                setPhotoAdapter(position);
//            } else {
//                binding.recyclerViewImage.setVisibility(View.GONE);
//                binding.cvCamera.setVisibility(View.GONE);
//                binding.btnUploadImage.setVisibility(View.GONE);
//            }
//        }
//
//        @Override
//        public void onNothingSelected(AdapterView<?> parent) {
//
//        }
//    };

    private void setPhotoAdapter(final int position) {

        if (userImagePath.size() == 0) {
            binding.lvNoData.setVisibility(View.VISIBLE);
        } else {
            binding.lvNoData.setVisibility(View.GONE);
        }


        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.recyclerViewImage.setLayoutManager(gridLayoutManager);
        binding.recyclerViewImage.setItemAnimator(new DefaultItemAnimator());

        UserImageAdapter userImageAdapter = new UserImageAdapter(SchoolUserImageActivity.this, userImagePath, new OnRecyclerClick() {
            @Override
            public void onClick(final int pos, View v, int type) {

                if (type == 0) {
                    //0 for delete
                    progressDialog = ProgressDialog.show(SchoolUserImageActivity.this, "", "", true);
                    progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    progressDialog.setContentView(R.layout.progress_view);
                    ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                    Circle bounce = new Circle();
                    bounce.setColor(Color.BLACK);
                    progressBar.setIndeterminateDrawable(bounce);

//                    String patter = "^(http|https)://.*$";
//                    String imagePath = userImagePath.get(pos);

                  //  if (imagePath.matches(patter)) {
                        //server image
                        //Log.d(TAG, "onClick: server image");

                        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                        String path = userImagePath.get(pos);
                        Log.d(TAG, "onClick: file name: " + new File(path).getName()+" / "+userId);
                        final Call<GetImageResponse> deletePhoto = retrofitClass.deleteUserImage(userId, new File(path).getName());
                        deletePhoto.enqueue(new Callback<GetImageResponse>() {
                            @Override
                            public void onResponse(Call<GetImageResponse> call, Response<GetImageResponse> response) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }

                                if (response.body().getResult().equalsIgnoreCase("true")) {
                                    utils.setAlert(SchoolUserImageActivity.this, "", response.body().getResponseMsg());
                                    userImagePath.clear();
                                    userImagePath.addAll(response.body().getData());

                                    if (userImagePath.size() > 0) {
                                        binding.lvNoData.setVisibility(View.GONE);
                                    } else {
                                        binding.lvNoData.setVisibility(View.VISIBLE);
                                    }

                                    if (StudentListActivity.userPostItems.get(position).getUserImages() != null) {
                                        StudentListActivity.userPostItems.get(position).getUserImages().clear();
                                        StudentListActivity.userPostItems.get(position).getUserImages().addAll(userImagePath);

                                        if (StudentListActivity.recyclerViewStudent.getAdapter() != null) {
                                            StudentListActivity.recyclerViewStudent.getAdapter().notifyDataSetChanged();
                                        }
                                    }

//                                    userPostItems.get(position).getUserImages().clear();
//                                    userPostItems.get(position).getUserImages().addAll(response.body().getData());

                                    if (binding.recyclerViewImage.getAdapter() != null) {
                                        binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
                                    }

                                } else {
                                    utils.setAlert(SchoolUserImageActivity.this, "", response.body().getResponseMsg());
                                }
                            }

                            @Override
                            public void onFailure(Call<GetImageResponse> call, Throwable t) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                                utils.setAlert(SchoolUserImageActivity.this, "", t.getMessage());
                            }
                        });
                   // }
//                    else {
//                        //local image
//                        Log.d(TAG, "onClick: local image");
//                        if (progressDialog.isShowing()) {
//                            progressDialog.dismiss();
//                        }
//
//                        Log.d(TAG, "onClick: remove: " + newUserImagePath.size());
//                        for (int i = 0; i < newUserImagePath.size(); i++) {
//                            if (newUserImagePath.get(i).equals(userImagePath.get(pos))) {
//                                newUserImagePath.remove(i);
//                            }
//                        }
//                        userImagePath.remove(pos);
//                        if (userImagePath.size() > 0) {
//                            binding.lvNoData.setVisibility(View.GONE);
//                        } else {
//                            binding.lvNoData.setVisibility(View.VISIBLE);
//                        }
//
//                        if (binding.recyclerViewImage.getAdapter() != null) {
//                            binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
//                        }
//                    }
                } else if (type == 1) {
                    //1 for update single picture
                   //userImageEdit = UserData.getUserImages().get(pos);
                    userImageEdit=userImagePath.get(pos);
                    imagePosition = pos;

                    if (ActivityCompat.checkSelfPermission(SchoolUserImageActivity.this, android.Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(SchoolUserImageActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SchoolUserImageActivity.this, new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                EDIT_IMAGE_PERMISSION);
                    } else {
                        openCameraForEdit();
                    }
                }
            }
        });
        binding.recyclerViewImage.setAdapter(userImageAdapter);

    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Profile Image");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.setting) {
            if (ActivityCompat.checkSelfPermission(SchoolUserImageActivity.this, android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(SchoolUserImageActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(SchoolUserImageActivity.this, new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        CAMERA_PERMISSION);
            } else {
                if (userImagePath.size() < 3) {
                    openCamera();
                } else {
                    utils.setAlert(SchoolUserImageActivity.this, "", "You can upload only 3 photos");
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION) {
            if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                if (userImagePath.size() < 3) {
                    openCamera();
                } else {
                    utils.setAlert(SchoolUserImageActivity.this, "", "You can upload only 3 photos");
                }
            } else {
                Toast.makeText(this, "Camera permission denied", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == EDIT_IMAGE_PERMISSION) {
            if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                openCameraForEdit();
            } else {
                Toast.makeText(this, "Camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
//        if (v.getId() == R.id.cvCamera) {
//            if (ActivityCompat.checkSelfPermission(SchoolUserImageActivity.this, android.Manifest.permission.CAMERA)
//                    != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(SchoolUserImageActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions(SchoolUserImageActivity.this, new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                        CAMERA_PERMISSION);
//            } else {
//                if (userImagePath.size() < 3) {
//                    openCamera();
//                } else {
//                    utils.setAlert(SchoolUserImageActivity.this, "", "You can upload only 3 photos");
//                }
//            }
//        } else

        if (v.getId() == R.id.btnUploadImage) {

//            if (newUserImagePath.size() > 0) {
//
//                progressDialog = ProgressDialog.show(SchoolUserImageActivity.this, "", "", true);
//                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                progressDialog.setContentView(R.layout.progress_view);
//                ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
//                Circle bounce = new Circle();
//                bounce.setColor(Color.BLACK);
//                progressBar.setIndeterminateDrawable(bounce);
//
//                uploadImage();
//            } else {
//                utils.setAlert(SchoolUserImageActivity.this, "", "Please capture photo");
//            }

//            if(userImagePath.size()>=3)
//            {
//                utils.setAlert(SchoolUserImageActivity.this, "", "You can upload only 3 photos");
//            }
//            else
//            {
//                if (newUserImagePath.size() > 0) {
//                    uploadImage();
//                } else {
//                    utils.setAlert(SchoolUserImageActivity.this, "", "Please capture photo");
//                }
//            }
        }
    }

    private void uploadImage() {

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[newUserImagePath.size()];
        for (int i = 0; i < newUserImagePath.size(); i++) {
            File file = new File(newUserImagePath.get(i));
            try {
                File compressedImageFile = new Compressor(SchoolUserImageActivity.this).compressToFile(file);
                Log.d(TAG, "uploadImage: " + compressedImageFile.getPath());
                RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), compressedImageFile);
                surveyImagesParts[i] = MultipartBody.Part.createFormData("images[]", compressedImageFile.getName(), surveyBody);
                Log.d(TAG, "showUriList: part: " + surveyImagesParts[i].toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        RequestBody requestBodyUID = RequestBody.create(MediaType.parse("text/plain*"), userId);
        RequestBody requestBodyEID = RequestBody.create(MediaType.parse("text/plain*"), empId);

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("user_id", requestBodyUID);
        map.put("employee_id", requestBodyEID);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetImageResponse> schoolList = retrofitClass.uploadImages(map, surveyImagesParts);
        schoolList.enqueue(new Callback<GetImageResponse>() {
            @Override
            public void onResponse(Call<GetImageResponse> call, Response<GetImageResponse> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Log.d(TAG, "onResponse: " + response.toString());
                if (response.body().getResult().equalsIgnoreCase("true")) {
                    // userImagePath.clear();
                    //userImagePath.addAll(response.body().getData());
                    Log.d(TAG, "onResponse: currentPosition: " + currentPosition);

                    userImagePath.clear();
                    userImagePath.addAll(response.body().getData());

                    if (StudentListActivity.userPostItems.get(position).getUserImages() != null) {
                        StudentListActivity.userPostItems.get(position).getUserImages().clear();
                        StudentListActivity.userPostItems.get(position).getUserImages().addAll(userImagePath);

                        if (StudentListActivity.recyclerViewStudent.getAdapter() != null) {
                            StudentListActivity.recyclerViewStudent.getAdapter().notifyDataSetChanged();
                        }
                    }

                    if (binding.recyclerViewImage.getAdapter() != null) {
                        binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
                    }

                    utils.setAlert(SchoolUserImageActivity.this, "", response.body().getResponseMsg());
                    newUserImagePath.clear();
                } else {
                    utils.setAlert(SchoolUserImageActivity.this, "", response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<GetImageResponse> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                utils.setAlert(SchoolUserImageActivity.this, "", t.getMessage());
            }
        });
    }

    private void editImage(String path) {

        progressDialog = ProgressDialog.show(SchoolUserImageActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[1];
        File file = new File(path);
        try {
            File compressedImageFile = new Compressor(SchoolUserImageActivity.this).compressToFile(file);
            Log.d(TAG, "uploadImage: " + compressedImageFile.getPath());
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), compressedImageFile);
            surveyImagesParts[0] = MultipartBody.Part.createFormData("images", compressedImageFile.getName(), surveyBody);
            Log.d(TAG, "showUriList: part: " + surveyImagesParts[0].toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        final File userImage = new File(userImageEdit);
        RequestBody requestBodyUID = RequestBody.create(MediaType.parse("text/plain*"), userId);
        final RequestBody requestBodyEID = RequestBody.create(MediaType.parse("text/plain*"), empId);
        RequestBody requestBodyImageName = RequestBody.create(MediaType.parse("text/plain*"), userImage.getName());

        Log.d(TAG, "editImage: " + userImage.getName());
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("user_id", requestBodyUID);
        map.put("employee_id", requestBodyEID);
        map.put("old_image_name", requestBodyImageName);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetImageResponse> schoolList = retrofitClass.editImages(map, surveyImagesParts);
        schoolList.enqueue(new Callback<GetImageResponse>() {
            @Override
            public void onResponse(Call<GetImageResponse> call, Response<GetImageResponse> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Log.d(TAG, "onResponse: " + response.body().getResult());
                if (response.body().getResult().equalsIgnoreCase("true")) {
                    utils.setAlert(SchoolUserImageActivity.this, "", response.body().getResponseMsg());
                    Log.d(TAG, "onResponse: currentPosition: " + currentPosition);

                    userImagePath.clear();
                    userImagePath.addAll(response.body().getData());

                    if (StudentListActivity.userPostItems.get(position).getUserImages() != null) {
                        StudentListActivity.userPostItems.get(position).getUserImages().clear();
                        StudentListActivity.userPostItems.get(position).getUserImages().addAll(userImagePath);

                        if (StudentListActivity.recyclerViewStudent.getAdapter() != null) {
                            StudentListActivity.recyclerViewStudent.getAdapter().notifyDataSetChanged();
                        }
                    }

//                    userPostItems.get(currentPosition).getUserImages().clear();
//                    userPostItems.get(currentPosition).getUserImages().addAll(userImagePath);

                    if (binding.recyclerViewImage.getAdapter() != null) {
                        binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
                    }
                } else {
                    utils.setAlert(SchoolUserImageActivity.this, "", response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<GetImageResponse> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Log.d(TAG, "onFailure: " + t.getMessage());
                utils.setAlert(SchoolUserImageActivity.this, "", t.getMessage());
            }
        });
    }

    public void openCamera() {
        Intent pictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile1();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            if (photoFile != null) {

                //Uri photoURI = Uri.fromFile(photoFile);
                // Uri photoURI = FileProvider.getUriForFile(this, getString(R.string.file_provider_authority), photoFile);
                Uri photoURI = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID+".provider", photoFile);
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());

                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    public void openCameraForEdit() {
        Intent pictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile1();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                // Uri photoURI = FileProvider.getUriForFile(this, getString(R.string.file_provider_authority), photoFile);

                Uri photoURI = Uri.fromFile(photoFile);
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());

                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        EDIT_IMAGE_REQUEST);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            if (image != null) {
                Log.d(TAG, "onActivityResult: RESULT_OK" + image.getPath());

                Intent i = new Intent(SchoolUserImageActivity.this, StudentPhotoCropActivity.class);
                i.putExtra("Path", image.getPath());
                i.putExtra("UserID", userId);
                i.putExtra("EmpId", empId);
                i.putExtra("flag","add");
                startActivityForResult(i, CAMERA_RESULT);

            } else {
                Toast.makeText(this, "Photo capture failed", Toast.LENGTH_SHORT).show();
                finish();
            }
        } else if (requestCode == CAMERA_RESULT && resultCode == RESULT_OK) {
//            String Path = data.getStringExtra("Path");
//            userImagePath.add(Path);
//
//            //capture image
//            newUserImagePath.add(Path);
//
//            if(userImagePath.size()>0)
//            {
//                binding.lvNoData.setVisibility(View.GONE);
//            }
//
//            if (binding.recyclerViewImage.getAdapter() != null) {
//                binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
//            }

            ArrayList<String> images = data.getStringArrayListExtra("Images");
            userImagePath.clear();
            userImagePath.addAll(images);

            if (userImagePath.size() > 0) {
                binding.lvNoData.setVisibility(View.GONE);
            }

            if (StudentListActivity.userPostItems.get(position).getUserImages() != null) {
                StudentListActivity.userPostItems.get(position).getUserImages().clear();
                StudentListActivity.userPostItems.get(position).getUserImages().addAll(userImagePath);

                if (StudentListActivity.recyclerViewStudent.getAdapter() != null) {
                    StudentListActivity.recyclerViewStudent.getAdapter().notifyDataSetChanged();
                }
            }

            if (binding.recyclerViewImage.getAdapter() != null) {
                binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
            }

        } else if (requestCode == EDIT_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if (image != null) {
                Log.d(TAG, "onActivityResult: RESULT_OK" + image.getPath());

                Intent i = new Intent(SchoolUserImageActivity.this, StudentPhotoCropActivity.class);
                i.putExtra("Path", image.getPath());
                i.putExtra("UserID", userId);
                i.putExtra("EmpId", empId);
                i.putExtra("OldImageName",userImageEdit);
                i.putExtra("flag","edit");
                startActivityForResult(i, EDIT_IMAGE_RESULT);

            } else {
                Toast.makeText(this, "Photo capture failed", Toast.LENGTH_SHORT).show();
                finish();
            }
        } else if (requestCode == EDIT_IMAGE_RESULT && resultCode == RESULT_OK) {
//            String Path = data.getStringExtra("Path");
//            userImagePath.set(imagePosition, Path);
//            if (binding.recyclerViewImage.getAdapter() != null) {
//                binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
//            }
//            editImage(Path);

            ArrayList<String> images = data.getStringArrayListExtra("Images");
            userImagePath.clear();
            userImagePath.addAll(images);

            if (StudentListActivity.userPostItems.get(position).getUserImages() != null) {
                StudentListActivity.userPostItems.get(position).getUserImages().clear();
                StudentListActivity.userPostItems.get(position).getUserImages().addAll(userImagePath);

                if (StudentListActivity.recyclerViewStudent.getAdapter() != null) {
                    StudentListActivity.recyclerViewStudent.getAdapter().notifyDataSetChanged();
                }
            }

            if (binding.recyclerViewImage.getAdapter() != null) {
                binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
            }

        }
    }

    private File createImageFile1() throws IOException {

        File file = new File(Environment.getExternalStorageDirectory(), "CQR");
        if (!file.exists()) {
            file.mkdir();
        }

        image = new File(file.getPath(), "IMG_" + (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime()) + ".jpg");

        imageFilePath = image.getPath();
        Log.d(TAG, "createImageFile: " + imageFilePath);
        return image;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.camera, menu);
        item = menu.findItem(R.id.setting);
        return super.onCreateOptionsMenu(menu);
    }
}

import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import Actions from '../../actions'
import GridList from '@material-ui/core/GridList'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import MobilePaper from '../../components/MobilePaper'
import AddCategoryDialog from '../../components/AddCategoryDialog'
import Loader from '../../components/Loader'
import DeleteIcon from '@material-ui/icons/Delete'
import IconButton from '@material-ui/core/IconButton'
import { confirmDialog } from '../../utils/funtions'

function Subcategories (props) {
  const dispatch = useDispatch()
  const classes = useStyles()

  const [state, setState] = useState({
    _id: undefined,
    name: '',
    subCategories: undefined
  })

  useEffect(() => {
    const searchParams = new URLSearchParams(props.location.search)
    const _id = searchParams.get('_id') || ''
    const name = searchParams.get('name') || ''
    _getSubcategories(_id, name)
  }, [props.location.search])

  const _getSubcategories = async (_id, name) => {
    setState({ ...state, _id, name })
    const data = await dispatch(Actions.get_sub_category_list(_id))
    return setState({ ...state, subCategories: data, _id, name })
  }

  const _addSubcategory = async (name) => {
    await dispatch(Actions.add_sub_category(state._id, name))
    const data = await dispatch(Actions.get_sub_category_list(state._id))
    return setState({ ...state, subCategories: data })
  }

  const listCallback = async () => {
    const data = await dispatch(Actions.get_sub_category_list(state._id))
    return setState({ ...state, subCategories: data })
  }

  const renderItem = (item, index) => {
    return (
      <ScreenList
        data={item}
        key={index}
        callback={listCallback}
      />
    )
  }

  return (
    <>
      <div style={{ marginBottom: 10 }}>
        <Typography variant='h4'>
          {state.name}
        </Typography>
      </div>
      {
        state.subCategories ? (
          <GridList className={classes.gridList}>
            {state.subCategories.map(renderItem)}
            <AddCategoryDialog
              title='New Sub-category Page'
              onSubmit={_addSubcategory}
            />
          </GridList>
        )
          : <Loader isVisible />
      }

    </>
  )
}

export default Subcategories

function ScreenList ({ data, callback }) {
  const dispatch = useDispatch()
  const classes = useStyles()
  let delete_id = null

  const _delete = async () => {
    const res = await dispatch(Actions.delete_screen(delete_id))
    if (res) {
      callback()
    }
  }

  const _alert = (id) => {
    delete_id = id
    confirmDialog(_delete)
  }

  return (
    <div style={{ height: '100%', width: 220, margin: 15 }}>
      <div
        style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', height: 45 }}
      >
        <Typography variant='subtitle1' className={classes.listTitle}>
          {data.name}
        </Typography>
        <IconButton
          aria-label='delete'
          onClick={() => _alert(data.screen_id)}
        >
          <DeleteIcon fontSize='small' />
        </IconButton>
      </div>

      <MobilePaper
        navigate
        data={data}
        source={{ uri: data.snap_shot }}
      />
    </div>

  )
}

const useStyles = makeStyles(theme => ({
  gridList: {
    width: '100%',
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1)
  },
  listTitle: {
    color: '#808080',
    paddingBottom: theme.spacing(1)
  }
}))

package com.app.yearbook.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.app.yearbook.R;
import com.app.yearbook.StudentListActivity;
import com.app.yearbook.model.employee.SchoolListItem;

import java.util.ArrayList;

public class EmpSchoolAdapter extends RecyclerView.Adapter<EmpSchoolAdapter.MyViewHolder> implements Filterable {

    private ArrayList<SchoolListItem> schoolList;
    private ArrayList<SchoolListItem> schoolListFiltered;
    private Activity ctx;

    @NonNull
    @Override
    public EmpSchoolAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_school_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EmpSchoolAdapter.MyViewHolder holder, final int position) {

        final SchoolListItem schoolData=schoolListFiltered.get(position);
        holder.tvSchoolName.setText(schoolData.getSchoolName());

        holder.tvSchoolAddress.setText(schoolData.getSchoolAddress());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TTT","Adapter: "+schoolData.getSchoolId());
                Intent i=new Intent(ctx,StudentListActivity.class);
                i.putExtra("schoolName", schoolData.getSchoolName());
                i.putExtra("schoolId", schoolData.getSchoolId()+"");
                ctx.startActivity(i);
            }
        });

    }

    public EmpSchoolAdapter(ArrayList<SchoolListItem> schoolList, Activity context) {
        this.schoolList = schoolList;
        this.ctx=context;
        this.schoolListFiltered = schoolList;
    }

    @Override
    public int getItemCount() {
        return schoolListFiltered.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSchoolName, tvSchoolAddress;

        public MyViewHolder(View view) {
            super(view);
            tvSchoolName = view.findViewById(R.id.tvSchoolName);
            tvSchoolAddress = view.findViewById(R.id.tvSchoolAddress);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    schoolListFiltered = schoolList;
                } else {
                    ArrayList<SchoolListItem> filteredList = new ArrayList<>();
                    for (SchoolListItem row : schoolList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getSchoolName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                            Log.d("TTT","Filter size: "+filteredList.size());
                        }
                    }
                    schoolListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = schoolListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                schoolListFiltered = (ArrayList<SchoolListItem>) filterResults.values;
                Log.d("TTT","publishResults: "+schoolListFiltered.size());
                notifyDataSetChanged();
            }
        };
    }

}

package com.app.yearbook.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.app.yearbook.AccessCode;
import com.app.yearbook.R;
import com.app.yearbook.databinding.FgAccessCodeDisableBinding;

public class AccessCodeDisable extends Fragment {

    FgAccessCodeDisableBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fg_access_code_disable, container, false);

        binding.txtAccessCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AccessCode.class);
                startActivity(i);
            }
        });

        return binding.getRoot();
    }
}

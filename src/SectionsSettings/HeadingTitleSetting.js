import React from "react"
import InputText from "../components/InputText";

export default function HeadingTitleSetting({ data, updateComponents }) {

    const _heading = (e) => {
        let value = { component_heading: e.target.value }
        updateComponents(value, data.component_order_by)
    }

    return (
        <div style={{ padding: 10, marginTop: 5 }}>
            <InputText
                title="Heading Text"
                defaultValue={data.component_heading}
                onChange={_heading}
                labelWidth={100}
            />
        </div>
    )
}
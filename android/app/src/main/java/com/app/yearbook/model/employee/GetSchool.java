package com.app.yearbook.model.employee;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetSchool{

	@SerializedName("ResponseCode")
	private String responseCode;

	@SerializedName("ServerTimeZone")
	private String serverTimeZone;

	@SerializedName("school_list")
	private List<SchoolListItem> schoolList;

	@SerializedName("ResponseMsg")
	private String responseMsg;

	@SerializedName("serverTime")
	private String serverTime;

	@SerializedName("Result")
	private String result;

	public void setResponseCode(String responseCode){
		this.responseCode = responseCode;
	}

	public String getResponseCode(){
		return responseCode;
	}

	public void setServerTimeZone(String serverTimeZone){
		this.serverTimeZone = serverTimeZone;
	}

	public String getServerTimeZone(){
		return serverTimeZone;
	}

	public void setSchoolList(List<SchoolListItem> schoolList){
		this.schoolList = schoolList;
	}

	public List<SchoolListItem> getSchoolList(){
		return schoolList;
	}

	public void setResponseMsg(String responseMsg){
		this.responseMsg = responseMsg;
	}

	public String getResponseMsg(){
		return responseMsg;
	}

	public void setServerTime(String serverTime){
		this.serverTime = serverTime;
	}

	public String getServerTime(){
		return serverTime;
	}

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	@Override
 	public String toString(){
		return 
			"GetSchool{" + 
			"responseCode = '" + responseCode + '\'' + 
			",serverTimeZone = '" + serverTimeZone + '\'' + 
			",school_list = '" + schoolList + '\'' + 
			",responseMsg = '" + responseMsg + '\'' + 
			",serverTime = '" + serverTime + '\'' + 
			",result = '" + result + '\'' + 
			"}";
		}
}
package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.yearbook.model.studenthome.home.PostListItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.ybq.android.spinkit.style.Circle;
import com.volokh.danylo.hashtaghelper.HashTagHelper;
import com.app.yearbook.adapter.TaggStudentUserListAdapter;
import com.app.yearbook.databinding.ActivityStudentPostEditBinding;
import com.app.yearbook.fragment.FirstVersionProfileFragment;
import com.app.yearbook.model.studenthome.AddBookMark;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.studenthome.LikeUnlike;
import com.app.yearbook.model.studenthome.OnOffNotification;
import com.app.yearbook.model.studenthome.PostList;
import com.app.yearbook.model.staff.yearbook.UserTagg;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.PlayerManager;
import tcking.github.com.giraffeplayer2.VideoInfo;

public class StudentPostEditActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private PostList postData;
    private HashTagHelper mEditTextHashTagHelper;
    private AllMethods allMethods;
    private ActivityStudentPostEditBinding binding;
    public ProgressDialog progressDialog;
    private LoginUser loginUser;
    private int userId;
    public ArrayList<UserTagg> tagUserListArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_student_post_edit);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_student_post_edit);
        setView();
    }

    public void setView() {
        //toolbar
        setToolbar();

        //all methods object
        allMethods = new AllMethods();
        loginUser = new LoginUser(getApplicationContext());
        if (loginUser.getUserData() != null) {
            userId = Integer.parseInt(loginUser.getUserData().getUserId());
        }

        if (getIntent().getExtras() != null) {
            postData = (PostList) getIntent().getSerializableExtra("PostObject");
            binding.setPostlist(postData);
            //set img
            if (postData.getUserImage() != null) {
                Glide.with(getApplicationContext())
                        .load(postData.getUserImage())
                        .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.mipmap.post_place_holder)
                        .error(R.mipmap.post_place_holder)
                        .into(binding.imgUserImage);

                binding.imgUserImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(StudentPostEditActivity.this, ImageActivity.class);
                        i.putExtra("FileUrl", postData.getUserImage());
                        startActivity(i);
                    }
                });
            }

            //set post img or video
            final String uri = postData.getPostFile();
            String extension = uri.substring(uri.lastIndexOf("."));
            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {
                binding.imgUserPost.setVisibility(View.VISIBLE);
                binding.videoUserPost.setVisibility(View.GONE);
                if (postData.getPostFile() != null) {
                    Glide.with(getApplicationContext())
                            .load(postData.getPostFile())
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    binding.imgUserPost.setImageBitmap(resource);
                                }
                            });

                }

                binding.imgUserPost.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(StudentPostEditActivity.this, ImageActivity.class);
                        i.putExtra("caption", binding.getPostlist().getPostDescription());
                        i.putExtra("FileUrl", uri);
                        startActivity(i);
                    }
                });
            } else {
                binding.imgUserPost.setVisibility(View.GONE);
                binding.videoUserPost.setVisibility(View.VISIBLE);

                //binding.videoUserPost.setSource(Uri.parse(postData.getPostFile()));

                if (binding.videoUserPost.getCoverView() != null) {
                    Glide.with(StudentPostEditActivity.this)
                            .load(binding.getPostlist().getPostThumbnail())
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(binding.videoUserPost.getCoverView());
                }

                binding.videoUserPost.getVideoInfo().setPortraitWhenFullScreen(false);
                binding.videoUserPost.setVideoPath(binding.getPostlist().getPostFile());
                binding.videoUserPost.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player

            }

            //tag user
            if (binding.getPostlist().getUserTagg() != null) {
                if (binding.getPostlist().getUserTagg().size() > 0) {
                    Log.d("TTT", "Taggg User: " + binding.getPostlist().getUserTagg().size());
                    tagUserListArray = new ArrayList<>();
                    binding.lvTaggPpl.setVisibility(View.VISIBLE);
                    tagUserListArray.addAll(binding.getPostlist().getUserTagg());
                    TaggStudentUserListAdapter userListAdapter = new TaggStudentUserListAdapter(tagUserListArray, StudentPostEditActivity.this, "student");
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(StudentPostEditActivity.this, LinearLayoutManager.HORIZONTAL, false);
                    binding.rvTaggUserList.setLayoutManager(mLayoutManager);
                    binding.rvTaggUserList.setAdapter(userListAdapter);
                } else {
                    binding.lvTaggPpl.setVisibility(View.GONE);
                }
            }

            //description
            mEditTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.hashtag), null);
            mEditTextHashTagHelper.handle(binding.tvPostDescription);

            //tv total like
            binding.tvTotalLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(StudentPostEditActivity.this, GetAllStudentLikeActivity.class);
                    i.putExtra("postId", binding.getPostlist().getPostId());
                    i.putExtra("Type", "student");
                    startActivity(i);
                }
            });
            //like-unlike
            Log.d("TTT", "Like dislike val: " + postData.getPostLike());
            if (postData.getPostLike().equalsIgnoreCase("true")) {
                binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
            } else {
                binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
            }

            binding.imgLikeDislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("TTT", "LikeDisLike: " + postData.getUserId() + " / " + postData.getPostId());

                    if (binding.getPostlist().getPostLike().equalsIgnoreCase("true")) {
                        YoYo.with(Techniques.RubberBand)
                                .duration(500)
                                .playOn(binding.imgLikeDislike);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
                            }
                        }, 200);

                    } else if (binding.getPostlist().getPostLike().equalsIgnoreCase("false")) {

                        YoYo.with(Techniques.RubberBand)
                                .duration(500)
                                .playOn(binding.imgLikeDislike);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
                            }
                        }, 200);
                    }

                    RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                    Call<LikeUnlike> userPost = retrofitClass.postLikeDislike(Integer.parseInt(postData.getUserId()), Integer.parseInt(postData.getPostId()), LoginUser.getUserTypeKey());
                    userPost.enqueue(new Callback<LikeUnlike>() {
                        @Override
                        public void onResponse(Call<LikeUnlike> call, Response<LikeUnlike> response) {
                            if (response.body().getResponseCode().equals("1")) {
                                if (response.body().getPostDetail().get(0).getPostLike().equalsIgnoreCase("true")) {
                                    binding.imgLikeDislike.setImageResource(R.mipmap.diamond_selected_ic);
                                } else if (response.body().getPostDetail().get(0).getPostLike().equalsIgnoreCase("false")) {
                                    binding.imgLikeDislike.setImageResource(R.mipmap.diamond_ic);
                                }
                                postData.setTotalLike(response.body().getPostDetail().get(0).getTotalLike());
                                postData.setPostLike(response.body().getPostDetail().get(0).getPostLike());

//                                if (ProfileFragment.getUserList != null) {
//                                    for (int i = 0; i < ProfileFragment.getUserList.size(); i++) {
//                                        if (ProfileFragment.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
//                                            PostListItem newList = response.body().getPostDetail().get(0);
//                                            ProfileFragment.getUserList.get(i).setPostLike(newList.getPostLike());
//                                            ProfileFragment.getUserList.get(i).setTotalLike(newList.getTotalLike());
//                                        }
//                                    }
//                                }
                                if (FirstVersionProfileFragment.getUserList != null) {
                                    for (int i = 0; i < FirstVersionProfileFragment.getUserList.size(); i++) {
                                        if (FirstVersionProfileFragment.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                            PostListItem newList = response.body().getPostDetail().get(0);
                                            FirstVersionProfileFragment.getUserList.get(i).setPostLike(newList.getPostLike());
                                            FirstVersionProfileFragment.getUserList.get(i).setTotalLike(newList.getTotalLike());
                                        }
                                    }
                                }

                                if (StudentBookmarkActivity.bookmarkList != null) {
                                    for (int i = 0; i < StudentBookmarkActivity.bookmarkList.size(); i++) {
                                        if (StudentBookmarkActivity.bookmarkList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                            PostListItem newList = response.body().getPostDetail().get(0);
                                            StudentBookmarkActivity.bookmarkList.get(i).setPostLike(newList.getPostLike());
                                            StudentBookmarkActivity.bookmarkList.get(i).setTotalLike(newList.getTotalLike());
                                        }
                                    }
                                }

//                                if (UserDetailActivity.getUserList != null) {
//                                    for (int i = 0; i < UserDetailActivity.getUserList.size(); i++) {
//                                        if (UserDetailActivity.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
//                                            PostListItem newList = response.body().getPostDetail().get(0);
//                                            UserDetailActivity.getUserList.get(i).setPostLike(newList.getPostLike());
//                                            UserDetailActivity.getUserList.get(i).setTotalLike(newList.getTotalLike());
//                                        }
//                                    }
//                                }

                                if (StudentPostTagImageActivity.getUserList != null) {
                                    for (int i = 0; i < StudentPostTagImageActivity.getUserList.size(); i++) {
                                        if (StudentPostTagImageActivity.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                            PostListItem newList = response.body().getPostDetail().get(0);
//                                            StudentPostTagImageActivity.getUserList.get(i).setPostLike(newList.getPostLike());
//                                            StudentPostTagImageActivity.getUserList.get(i).setTotalLike(newList.getTotalLike());
                                        }
                                    }
                                }


                            } else if (response.body().getResponseCode().equals("10")) {
                                Toast.makeText(StudentPostEditActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                LoginUser loginSP = new LoginUser(StudentPostEditActivity.this);
                                loginSP.clearData();

                                Intent i = new Intent(StudentPostEditActivity.this, LoginActivity.class);
                                startActivity(i);
                                finish();
                            } else {
                                allMethods.setAlert(StudentPostEditActivity.this, "", response.body().getResponseMsg() + "");
                            }
                        }

                        @Override
                        public void onFailure(Call<LikeUnlike> call, Throwable t) {
                            Log.d("TTT", "Failure: " + t.getMessage());
                            allMethods.setAlert(StudentPostEditActivity.this, "", t.getMessage() + "");
                        }
                    });
                }
            });

            //bookmark
            Log.d("TTT", "Bookmark: " + postData.getPostBookmark());
            if (postData.getPostBookmark().equalsIgnoreCase("true")) {
                binding.imgBookmark.setImageResource(R.mipmap.bookmark_selected_ic);
            } else {
                binding.imgBookmark.setImageResource(R.mipmap.bookmark_ic);
            }

            binding.imgBookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                    Call<AddBookMark> bookmark = retrofitClass.addBookmark(Integer.parseInt(postData.getUserId()), Integer.parseInt(postData.getPostId()));
                    bookmark.enqueue(new Callback<AddBookMark>() {
                        @Override
                        public void onResponse(Call<AddBookMark> call, Response<AddBookMark> response) {
                            if (response.body().getResponseCode().equals("1")) {
                                if (response.body().getPostDetail().get(0).getPostBookmark().equalsIgnoreCase("true")) {
                                    binding.imgBookmark.setImageResource(R.mipmap.bookmark_selected_ic);
                                } else if (response.body().getPostDetail().get(0).getPostBookmark().equalsIgnoreCase("false")) {
                                    binding.imgBookmark.setImageResource(R.mipmap.bookmark_ic);
                                }
                                postData.setPostBookmark(response.body().getPostDetail().get(0).getPostBookmark());

//                                if (ProfileFragment.getUserList != null) {
//                                    for (int i = 0; i < ProfileFragment.getUserList.size(); i++) {
//                                        if (ProfileFragment.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
//                                            PostListItem newList = response.body().getPostDetail().get(0);
//                                            ProfileFragment.getUserList.get(i).setPostBookmark(newList.getPostBookmark());
//                                        }
//                                    }
//                                }

                                if (FirstVersionProfileFragment.getUserList != null) {
                                    for (int i = 0; i < FirstVersionProfileFragment.getUserList.size(); i++) {
                                        if (FirstVersionProfileFragment.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                            PostListItem newList = response.body().getPostDetail().get(0);
                                            FirstVersionProfileFragment.getUserList.get(i).setPostBookmark(newList.getPostBookmark());

                                        }
                                    }
                                }

                                if (StudentBookmarkActivity.bookmarkList != null) {
                                    for (int i = 0; i < StudentBookmarkActivity.bookmarkList.size(); i++) {
                                        if (StudentBookmarkActivity.bookmarkList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                            PostListItem newList = response.body().getPostDetail().get(0);
                                            StudentBookmarkActivity.bookmarkList.get(i).setPostBookmark(newList.getPostBookmark());
                                            Log.d("TTT", "Removed flag: " + newList.getPostBookmark());
                                            Log.d("TTT", "Removeddd: " + i);

//                                            if(newList.getPostBookmark().equalsIgnoreCase("false")) {
//                                                Log.d("TTT","Removeddd item size: "+StudentBookmarkActivity.bookmarkList.size());
//                                                StudentBookmarkActivity.bookmarkList.remove(i);
//                                                StudentBookmarkActivity.rvUserBookmark.getAdapter().notifyItemRemoved(i);
//                                            }
//                                            else
//                                            {
//                                                StudentBookmarkActivity.bookmarkList.add(response.body().getPostDetail()))
//                                                StudentBookmarkActivity.rvUserBookmark.getAdapter().notifyDataSetChanged();
//                                            }

                                        }
                                    }
                                }

//                                if (UserDetailActivity.getUserList != null) {
//                                    for (int i = 0; i < UserDetailActivity.getUserList.size(); i++) {
//                                        if (UserDetailActivity.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
//                                            PostListItem newList = response.body().getPostDetail().get(0);
//                                            UserDetailActivity.getUserList.get(i).setPostBookmark(newList.getPostBookmark());
//                                        }
//                                    }
//                                }

                                if (StudentPostTagImageActivity.getUserList != null) {
                                    for (int i = 0; i < StudentPostTagImageActivity.getUserList.size(); i++) {
                                        if (StudentPostTagImageActivity.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                            PostListItem newList = response.body().getPostDetail().get(0);
                                          //  StudentPostTagImageActivity.getUserList.get(i).setPostBookmark(newList.getPostBookmark());
                                        }
                                    }
                                }
                            } else if (response.body().getResponseCode().equals("10")) {
                                Toast.makeText(StudentPostEditActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                LoginUser loginSP = new LoginUser(StudentPostEditActivity.this);
                                loginSP.clearData();

                                Intent i = new Intent(StudentPostEditActivity.this, LoginActivity.class);
                                startActivity(i);
                                finish();
                            } else {
                                allMethods.setAlert(StudentPostEditActivity.this, "", response.body().getResponseMsg() + "");
                            }
                        }

                        @Override
                        public void onFailure(Call<AddBookMark> call, Throwable t) {
                            allMethods.setAlert(StudentPostEditActivity.this, "", t.getMessage() + "");
                        }
                    });
                }
            });
            //comment
            Log.d("TTT", "Total comment : " + postData.getTotalComment());

            binding.tvComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), StudentCommentActivity.class);
                    i.putExtra("comment", "user");
                    i.putExtra("PostId", postData.getPostId());
                    startActivityForResult(i, 123);
                }
            });

            binding.imgComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), StudentCommentActivity.class);
                    i.putExtra("comment", "user");
                    i.putExtra("PostId", postData.getPostId());
                    startActivityForResult(i, 123);
                }
            });

            //option
            Log.d("TTT", "Option: " + userId + " / " + Integer.parseInt(binding.getPostlist().getUserId()));
            if (userId == Integer.parseInt(binding.getPostlist().getUserId())) {
                binding.imgOption.setVisibility(View.VISIBLE);
            } else {
                binding.imgOption.setVisibility(View.GONE);
            }
            binding.imgOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialogForSameStudent(postData, binding.imgOption);
                }
            });
        }
    }

    public void showDialogForSameStudent(final PostList postList, ImageView imgOption) {
        PopupMenu popup = new PopupMenu(StudentPostEditActivity.this, imgOption);
        popup.getMenuInflater().inflate(R.menu.same_student_opt, popup.getMenu());

        MenuItem itemNotification = popup.getMenu().findItem(R.id.notification);
        itemNotification.setVisible(true);

        if (postList.getPostNotification() != null) {
            if (postList.getPostNotification().equalsIgnoreCase("true")) {
                popup.getMenu().findItem(R.id.notification).setTitle("Turn off notification");
            } else {
                popup.getMenu().findItem(R.id.notification).setTitle("Turn on notification");
            }
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Edit")) {
                    Log.d("TTT", "Postttt dialog: " + postList.getPostId() + " / " + postList.getUserTagg().size());
                    Intent i = new Intent(getApplicationContext(), AddPostActivity.class);
                    i.putExtra("Edit", "Edit");
                    i.putExtra("Thumb", postList.getPostThumbnail());
                    i.putExtra("PostId", postList.getPostId());
                    i.putExtra("Description", postList.getPostDescription());
                    i.putExtra("postUrl", postList.getPostFile());
                    i.putExtra("tagUserId", (ArrayList<UserTagg>) postList.getUserTagg());
                    startActivityForResult(i, 124);
                } else if (item.getTitle().equals("Delete")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(StudentPostEditActivity.this);
                    builder.setMessage("Are you sure you want to delete the post?")
                            .setCancelable(false)
                            .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    progressDialog = ProgressDialog.show(StudentPostEditActivity.this, "", "", true);
                                    progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    progressDialog.setContentView(R.layout.progress_view);
                                    ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                                    Circle bounce = new Circle();
                                    bounce.setColor(Color.BLACK);
                                    progressBar.setIndeterminateDrawable(bounce);

                                    RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                                    final Call<GiveReport> deletePost = retrofitClass.deletePost(Integer.parseInt(postList.getPostId()));
                                    deletePost.enqueue(new Callback<GiveReport>() {
                                        @Override
                                        public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                            if (progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }
                                            if (response.body().getResponseCode().equals("1")) {
                                                Toast.makeText(StudentPostEditActivity.this, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();

//                                                if (ProfileFragment.getUserList != null) {
//                                                    for (int i = 0; i < ProfileFragment.getUserList.size(); i++) {
//                                                        if (ProfileFragment.getUserList.get(i).getPostId().equals(postList.getPostId())) {
//                                                            Log.d("TTT", "Removeeee " + postList.getPostId() + " / " + i);
//                                                            ProfileFragment.getUserList.remove(i);
//                                                            ProfileFragment.rvUserPost.getAdapter().notifyItemRemoved(i);
//                                                        }
//                                                    }
//                                                }

                                                if (FirstVersionProfileFragment.getUserList != null) {
                                                    for (int i = 0; i < FirstVersionProfileFragment.getUserList.size(); i++) {
                                                        if (FirstVersionProfileFragment.getUserList.get(i).getPostId().equals(postList.getPostId())) {
                                                            FirstVersionProfileFragment.getUserList.remove(i);
                                                            FirstVersionProfileFragment.rvUserPost.getAdapter().notifyItemRemoved(i);
                                                        }
                                                    }
                                                }

                                                if (StudentBookmarkActivity.bookmarkList != null) {
                                                    for (int i = 0; i < StudentBookmarkActivity.bookmarkList.size(); i++) {
                                                        if (StudentBookmarkActivity.bookmarkList.get(i).getPostId().equals(postList.getPostId())) {
                                                            Log.d("TTT", "Removeeee " + postList.getPostId() + " / " + i);
                                                            StudentBookmarkActivity.bookmarkList.remove(i);
                                                            StudentBookmarkActivity.rvUserBookmark.getAdapter().notifyItemRemoved(i);
                                                        }
                                                    }
                                                }

//                                                if (UserDetailActivity.getUserList != null) {
//                                                    for (int i = 0; i < UserDetailActivity.getUserList.size(); i++) {
//                                                        if (UserDetailActivity.getUserList.get(i).getPostId().equals(postList.getPostId())) {
//                                                            Log.d("TTT", "Removeeee " + postList.getPostId() + " / " + i);
//                                                            UserDetailActivity.getUserList.remove(i);
//                                                            UserDetailActivity.rvUserPost.getAdapter().notifyItemRemoved(i);
//                                                        }
//                                                    }
//                                                }

                                                if (StudentPostTagImageActivity.getUserList != null) {
                                                    for (int i = 0; i < StudentPostTagImageActivity.getUserList.size(); i++) {
                                                        if (StudentPostTagImageActivity.getUserList.get(i).getPostId().equals(postList.getPostId())) {
                                                            Log.d("TTT", "Removeeee " + postList.getPostId() + " / " + i);
                                                            StudentPostTagImageActivity.getUserList.remove(i);
                                                            StudentPostTagImageActivity.rvUserTagPost.getAdapter().notifyItemRemoved(i);
                                                        }
                                                    }
                                                }
                                                finish();

                                            } else if (response.body().getResponseCode().equals("10")) {
                                                Toast.makeText(StudentPostEditActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                                LoginUser loginSP = new LoginUser(StudentPostEditActivity.this);
                                                loginSP.clearData();

                                                Intent i = new Intent(StudentPostEditActivity.this, LoginActivity.class);
                                                startActivity(i);
                                                finish();
                                            } else {
                                                Toast.makeText(StudentPostEditActivity.this, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<GiveReport> call, Throwable t) {
                                            if (progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }
                                            Toast.makeText(StudentPostEditActivity.this, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            })
                            .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    }

                            );

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    alert.setTitle("");
                    alert.show();

                } else {
                    progressDialog = ProgressDialog.show(StudentPostEditActivity.this, "", "", true);
                    progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    progressDialog.setContentView(R.layout.progress_view);
                    ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                    Circle bounce = new Circle();
                    bounce.setColor(Color.BLACK);
                    progressBar.setIndeterminateDrawable(bounce);

                    RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                    Call<OnOffNotification> notification = retrofitClass.studentOwnPostNotification(userId, Integer.parseInt(postList.getPostId()));
                    notification.enqueue(new Callback<OnOffNotification>() {
                        @Override
                        public void onResponse(Call<OnOffNotification> call, Response<OnOffNotification> response) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            if (response.body().getResponseCode().equals("1")) {
                                postData.setPostNotification(response.body().getPostDetail().get(0).getPostNotification());

//                                if (ProfileFragment.getUserList != null) {
//                                    for (int i = 0; i < ProfileFragment.getUserList.size(); i++) {
//                                        if (ProfileFragment.getUserList.get(i).getPostId().equals(postList.getPostId())) {
//                                            ProfileFragment.getUserList.get(i).setPostNotification(response.body().getPostDetail().get(0).getPostNotification());
//                                        }
//                                    }
//                                }

                                if (FirstVersionProfileFragment.getUserList != null) {
                                    for (int i = 0; i < FirstVersionProfileFragment.getUserList.size(); i++) {
                                        if (FirstVersionProfileFragment.getUserList.get(i).getPostId().equals(postList.getPostId())) {
                                            FirstVersionProfileFragment.getUserList.get(i).setPostNotification(response.body().getPostDetail().get(0).getPostNotification());
                                        }
                                    }
                                }

                                if (StudentBookmarkActivity.bookmarkList != null) {
                                    for (int i = 0; i < StudentBookmarkActivity.bookmarkList.size(); i++) {
                                        if (StudentBookmarkActivity.bookmarkList.get(i).getPostId().equals(postList.getPostId())) {
                                            StudentBookmarkActivity.bookmarkList.get(i).setPostNotification(response.body().getPostDetail().get(0).getPostNotification());
                                        }
                                    }
                                }

//                                if (UserDetailActivity.getUserList != null) {
//                                    for (int i = 0; i < UserDetailActivity.getUserList.size(); i++) {
//                                        if (UserDetailActivity.getUserList.get(i).getPostId().equals(postList.getPostId())) {
//                                            UserDetailActivity.getUserList.get(i).setPostNotification(response.body().getPostDetail().get(0).getPostNotification());
//                                        }
//                                    }
//                                }

                                if (StudentPostTagImageActivity.getUserList != null) {
                                    for (int i = 0; i < StudentPostTagImageActivity.getUserList.size(); i++) {
                                        if (StudentPostTagImageActivity.getUserList.get(i).getPostId().equals(postList.getPostId())) {
                                          //  StudentPostTagImageActivity.getUserList.get(i).setPostNotification(response.body().getPostDetail().get(0).getPostNotification());
                                        }
                                    }
                                }

                                Toast.makeText(StudentPostEditActivity.this, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();

                            } else if (response.body().getResponseCode().equals("10")) {
                                Toast.makeText(StudentPostEditActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                LoginUser loginSP = new LoginUser(StudentPostEditActivity.this);
                                loginSP.clearData();

                                Intent i = new Intent(StudentPostEditActivity.this, LoginActivity.class);
                                startActivity(i);
                                finish();
                            } else {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                                Toast.makeText(StudentPostEditActivity.this, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<OnOffNotification> call, Throwable t) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            Toast.makeText(StudentPostEditActivity.this, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                return true;
            }
        });
        popup.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TTT", "Resulttt: " + requestCode + " / " + resultCode);
        if (resultCode == RESULT_OK) {
            if (requestCode == 123) {
                String comment = data.getStringExtra("Comment");
                Log.d("TTT", "cmntttttttt: " + comment);
                postData.setTotalComment(comment);
            } else if (requestCode == 124) {
                String desc = data.getStringExtra("Description");
                ArrayList<UserTagg> userId = (ArrayList<UserTagg>) data.getSerializableExtra("taggedUser");
                postData.setPostDescription(desc);
                postData.setUserTagg(userId);
            }
        }
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Post");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        if (PlayerManager.getInstance().onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}

package com.app.yearbook;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.takusemba.cropme.CropView;
import com.takusemba.cropme.OnCropListener;
import com.app.yearbook.utils.AllMethods;

import java.io.IOException;

import gun0912.tedbottompicker.TedBottomPicker;
import tcking.github.com.giraffeplayer2.PlayerManager;
import tcking.github.com.giraffeplayer2.VideoInfo;
import tcking.github.com.giraffeplayer2.VideoView;

public class StaffPhotoVideoSelectActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private static final int REQUEST_PERMISSION = 101;
    private TedBottomPicker tedBottomPicker;
    private ImageView imgDummy, imgSnap, imgLarge, imgMedium, imgSmall,imgUpArrow;
    private MenuItem next;
    private VideoView videoSelected;
   // private TextView tvPhoto, tvVideo;
    private String postTag, CategoryId = "", actions = "photo", YearbookPost = "";
    private Uri videoUri,selectedPhoto;
    private FrameLayout frameLayoutMain;
    private LinearLayout lvCropOption;
    private CropView cropView;
    private int CropType = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_photo_video_select);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Log.d("TTT", "Permission...");
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
        } else {
            Log.d("TTT", "granted Permission...");
            setView();
        }
    }

    public void setView() {
        //toolbar
        setToolbar();

        if (getIntent().getExtras() != null) {
            actions = getIntent().getStringExtra("actions");
            CategoryId = getIntent().getStringExtra("CategoryId");

            if(getIntent().getStringExtra("EditYearPost")!=null) {
                YearbookPost = getIntent().getStringExtra("EditYearPost");
            }
        }

        //frameLayoutMain
        frameLayoutMain = findViewById(R.id.frameLayoutMain);
        lvCropOption = findViewById(R.id.lvCropOption);

        //video
        videoSelected = findViewById(R.id.videoSelected);

//        tvPhoto = findViewById(R.id.tvPhoto);
//        tvPhoto.setOnClickListener(this);

        imgUpArrow=findViewById(R.id.imgUpArrow);
        imgUpArrow.setOnClickListener(this);

//        tvVideo = findViewById(R.id.tvVideo);
//        tvVideo.setOnClickListener(this);

        //img
        imgDummy = findViewById(R.id.imgDummy);
        //imgSelect=findViewById(R.id.imgSelected);

        imgLarge = findViewById(R.id.imgLarge);
        imgLarge.setOnClickListener(this);

        imgMedium = findViewById(R.id.imgMedium);
        imgMedium.setOnClickListener(this);

        imgSmall = findViewById(R.id.imgSmall);
        imgSmall.setOnClickListener(this);

        cropView = findViewById(R.id.cropViewLarge);
        float height = getResources().getFraction(R.fraction.fraction60, 1, 1);
        float width = getResources().getFraction(R.fraction.fraction100, 1, 1);
        cropView.setHeightWidth(height, width);

        //resize
        imgSnap = findViewById(R.id.imgSnap);
        imgSnap.setOnClickListener(this);
        //1 small , 2 large
        if (actions.equalsIgnoreCase("photo")) {
            //tvVideo.setVisibility(View.GONE);
            //tvPhoto.setVisibility(View.VISIBLE);
            lvCropOption.setVisibility(View.VISIBLE);
            frameLayoutMain.setVisibility(View.VISIBLE);

            tedBottomPicker = new TedBottomPicker.Builder(getApplicationContext())
                    .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                        @Override
                        public void onImageSelected(Uri uri) {
                            // here is selected uri

                            selectedPhoto=uri;
                            next.setVisible(true);
                            postTag = "Image";
                            //imgDummy.setVisibility(View.GONE);
                            CropType = 1;
                            try {
                                Bitmap mBitmap = AllMethods.GetBitmap(uri, StaffPhotoVideoSelectActivity.this);
                                setImageInCropView(mBitmap, uri);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    })
                    .setTitle("Select image")
                    .create();
            tedBottomPicker.show(getSupportFragmentManager());
        } else if (actions.equalsIgnoreCase("video")) {
            //tvPhoto.setVisibility(View.GONE);
            //tvVideo.setVisibility(View.VISIBLE);
            lvCropOption.setVisibility(View.GONE);
            imgDummy.setVisibility(View.VISIBLE);
            frameLayoutMain.setVisibility(View.GONE);

            tedBottomPicker = new TedBottomPicker.Builder(getApplicationContext())
                    .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                        @Override
                        public void onImageSelected(Uri uri) {
                            // here is selected uri
                            //view view
                            videoUri = uri;
                            postTag = "Video";
                            next.setVisible(true);
                            Log.d("TTT", "Uri: " + uri);
                            imgDummy.setVisibility(View.GONE);

                            videoSelected.setVisibility(View.VISIBLE);

//                            videoSelected.reset();
//                            videoSelected.setSource(uri);
//                            Log.d("TTT", "Size: " + videoSelected.getDuration());

                            videoSelected.getVideoInfo().setPortraitWhenFullScreen(false);
                            videoSelected.setVideoPath(uri.toString());
                            videoSelected.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player
                            videoSelected.getPlayer().start();
                        }
                    })
                    .setTitle("Select video")
                    .showVideoMedia()
                    .create();
            tedBottomPicker.show(getSupportFragmentManager());
        }
    }

    public void setImageInCropView(Bitmap mBitmap, Uri selectedImage)
    {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(selectedImage.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        Log.d("TTT", "Orientation: " + orientation);

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 90);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 180);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            mBitmap = AllMethods.rotateBitmap(mBitmap, 270);
        }
        cropView.setBitmap(mBitmap);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selectimage, menu);
        next = menu.findItem(R.id.next);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    Log.d("TTT", "Permission... onRequestPermissionsResult");
                    setView();
                } else {
                    Log.d("TTT", "denyyyy... onRequestPermissionsResult");
                    Toast.makeText(this, "Deny", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Post");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        if (PlayerManager.getInstance().onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.next) {
            if (postTag.equalsIgnoreCase("Image")) {
                //cropViewLarge
                if(cropView!=null) {
                    cropView.crop(new OnCropListener() {
                        @Override
                        public void onSuccess(Bitmap bitmap) {
                            // do something
                            Log.d("TTT", "onOptionsItemSelected: " + CropType);
                            Uri getUri = AllMethods.getImageUri(getApplicationContext(), bitmap);
                            Intent i = new Intent(getApplicationContext(), ImageFilterForStaffPostActivity.class);
                            i.putExtra("CategoryId", CategoryId);
                            i.putExtra("from", "staff");
                            i.putExtra("selectUri", getUri.toString());
                            i.putExtra("CropType", CropType);
                            i.putExtra("EditYearPost", YearbookPost);
                            startActivityForResult(i, 111);
                        }

                        @Override
                        public void onFailure() {
                            Toast.makeText(getApplicationContext(), "crop failed", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } else {
                // Intent i = new Intent(getActivity(), AddPostActivity.class);
                Intent i = new Intent(getApplicationContext(), TrimmerActivity.class);
                i.putExtra("CategoryId", CategoryId);
                i.putExtra("from", "staff");
                i.putExtra("Type", "Video");
                i.putExtra("selectUri", videoUri.toString());
                i.putExtra("EditYearPost", YearbookPost);
                startActivityForResult(i, 111);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 111 && resultCode == RESULT_OK) {
            if (data.getExtras() != null) {

                String type="";

                if(data.getStringExtra("Type")!=null)
                {
                    type=data.getStringExtra("Type");

                    if(type.equalsIgnoreCase("Image"))
                    {
                        int CropType=data.getIntExtra("CropType",1);
                        String uri=data.getStringExtra("AddPostImage");
                        Log.d("TTT","Crop type:::: "+CropType);
                        Intent i = new Intent();
                        i.putExtra("Type", "Image");
                        i.putExtra("CropType", CropType);
                        i.putExtra("AddPostImage", uri);
                        setResult(RESULT_OK, i);
                        finish();
                    }
                    else if(type.equalsIgnoreCase("Video"))
                    {
                        String uri=data.getStringExtra("selectUri");
                        Intent i=new Intent();
                        i.putExtra("Type", "Video");
                        i.putExtra("selectUri", uri);
                        setResult(RESULT_OK,i);
                        finish();
                    }
                }
                else
                {
                    Intent i = getIntent();
                    setResult(RESULT_OK, i);
                    finish();
                }

            }

        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.imgUpArrow)
        {
            if(actions.equalsIgnoreCase("photo")) {
                tedBottomPicker = new TedBottomPicker.Builder(getApplicationContext())
                        .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                            @Override
                            public void onImageSelected(Uri uri) {
                                // here is selected uri
                                selectedPhoto=uri;
                                postTag = "Image";
                                next.setVisible(true);
                                imgDummy.setVisibility(View.GONE);
                                frameLayoutMain.setVisibility(View.VISIBLE);

                                try {

                                    Bitmap mBitmap = AllMethods.GetBitmap(uri, StaffPhotoVideoSelectActivity.this);
                                    setImageInCropView(mBitmap, uri);

                                    //cropView.setUri(uri);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }).setTitle("Select image").create();

                tedBottomPicker.show(getSupportFragmentManager());
            }
            else if(actions.equalsIgnoreCase("video"))
            {
                tedBottomPicker = new TedBottomPicker.Builder(getApplicationContext())
                        .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                            @Override
                            public void onImageSelected(Uri uri) {
                                // here is selected uri
                                videoUri = uri;
                                postTag = "Video";
                                next.setVisible(true);
                                Log.d("TTT", "Uri: " + uri);
                                imgDummy.setVisibility(View.GONE);
                                frameLayoutMain.setVisibility(View.GONE);
                                videoSelected.setVisibility(View.VISIBLE);

//                            videoSelected.reset();
//                            videoSelected.setSource(uri);
                                //Log.d("TTT", "Size: " + videoSelected.getDuration());

                                videoSelected.getVideoInfo().setPortraitWhenFullScreen(false);
                                videoSelected.setVideoPath(uri.toString());
                                videoSelected.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player
                                videoSelected.getPlayer().start();
                            }
                        })
                        .setTitle("Select video")
                        .showVideoMedia()
                        .create();

                tedBottomPicker.show(getSupportFragmentManager());
            }
        }
        else if (v.getId() == R.id.imgLarge) {

            imgLarge.setImageDrawable(getResources().getDrawable(R.mipmap.large_selected_box));
            imgMedium.setImageDrawable(getResources().getDrawable(R.mipmap.medium_box));
            imgSmall.setImageDrawable(getResources().getDrawable(R.mipmap.small_box));

            float height = getResources().getFraction(R.fraction.fraction60, 1, 1);
            float width = getResources().getFraction(R.fraction.fraction100, 1, 1);
            cropView.setHeightWidth(height, width);
            cropView.reCallParam();

//            if (selectedPhoto != null) {
//                Log.d("TTT"," selectedPhoto: "+selectedPhoto);
//                cropView.setUri(selectedPhoto);
//            }

//            cropView.invalidate();
//            cropView.requestLayout();

            CropType = 1;

//            if (selectedPhoto != null) {
//                Log.d("TTT"," selectedPhoto: "+selectedPhoto);
//                cropView.setUri(selectedPhoto);
//                cropView.requestLayout();
//            }
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    cropViewLarge.requestLayout();
//                }
//            },1000);

        } else if (v.getId() == R.id.imgMedium) {

            imgLarge.setImageDrawable(getResources().getDrawable(R.mipmap.large_box));
            imgMedium.setImageDrawable(getResources().getDrawable(R.mipmap.medium_selected_box));
            imgSmall.setImageDrawable(getResources().getDrawable(R.mipmap.small_box));

            float height = getResources().getFraction(R.fraction.fraction60, 1, 1);
            float width = getResources().getFraction(R.fraction.fraction70, 1, 1);
            cropView.setHeightWidth(height, width);
            cropView.reCallParam();

//            if (selectedPhoto != null) {
//                Log.d("TTT"," selectedPhoto: "+selectedPhoto);
//
//                cropView.setUri(selectedPhoto);
//            }

//            cropView.invalidate();
//            cropView.requestLayout();

            CropType = 2;

//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    cropViewLarge.requestLayout();
//                }
//            },1000);


        } else if (v.getId() == R.id.imgSmall) {

            imgLarge.setImageDrawable(getResources().getDrawable(R.mipmap.large_box));
            imgMedium.setImageDrawable(getResources().getDrawable(R.mipmap.medium_box));
            imgSmall.setImageDrawable(getResources().getDrawable(R.mipmap.small_selected_box));
//
//            float height = getResources().getFraction(R.fraction.fraction60, 1, 1);
//            float width = getResources().getFraction(R.fraction.fraction55, 1, 1);

            float height = getResources().getFraction(R.fraction.fraction100, 1, 1);
            float width = getResources().getFraction(R.fraction.fraction100, 1, 1);

            cropView.setHeightWidth(height, width);
            cropView.reCallParam();

//            if(selectedPhoto!=null)
//            {
//                Log.d("TTT"," selectedPhoto: "+selectedPhoto);
//                cropView.setUri(selectedPhoto);
//            }

//            cropView.invalidate();
//            cropView.requestLayout();

            CropType = 3;

        }
    }

}

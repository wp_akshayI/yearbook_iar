import { Platform, Dimensions } from 'react-native'

export const debuge = false

export const api = debuge ? 'http://localhost/Yearbook/api/web_editor/' : 'https://classof.app/api/web_editor/'
export const adminpanel = debuge ? 'http://localhost/Yearbook/' : 'https://classof.app'

export const auto_save_time = 300000
export const platform = Platform.OS === 'web' ? 'web' : 'app'
export const width = platform === 'web' ? 320 : Dimensions.get('screen').width
export const height = platform === 'web' ? 570 : Dimensions.get('window').height

export const components = [
  { component_id: '', component_type: 0, name: 'Text' },
  { component_id: '', component_type: 1, name: 'Heading Text' },
  { component_id: '', component_type: 2, name: 'Image' },
  { component_id: '', component_type: 3, name: 'Image + Text Overlay', component_url: '', component_title: '', x: 0, y: 0, font_name: 'Arimo', font_size: 14, font_color: '#FFF', font_border: false, rotation: 1 },
  { component_id: '', component_type: 4, name: 'Video' },
  { component_id: '', component_type: 5, name: 'Portrait Columns', component_title: '', grid_row: 2, student_list: [], staff_list: [] },
  { component_id: '', component_type: 6, name: 'Pages Inside(Grid)' },
  { component_id: '', component_type: 7, name: 'Pages Inside(List)' },
  { component_id: '', component_type: 8, name: 'Image Gallery', grid_row: 2, images: [] },
  { component_id: '', component_type: 9, name: 'Image Title Section', component_url: '', component_title: '', component_description: '' }
]

package com.app.yearbook;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.yearbook.utils.BaseUrl;
import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.model.StaffStudent;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsConditionStaffActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private TextView tvAgree, tvDisagree, tvAgreePolicy, tvDisagreePolicy;
    private WebView mWebview, mWebviewPolicy;
    public ProgressDialog progressDialog;
    private AllMethods allMethods;
    private LoginUser loginUserSP;
    private LinearLayout lvPrivacyPolicy, lvTermsCondition;
    private TextView toolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);
        setView();
    }

    public void setView() {
        //common methods
        allMethods = new AllMethods();

        //toolbar
        setToolbar();

        //SP
        loginUserSP = new LoginUser(TermsConditionStaffActivity.this);

        //webvview
        mWebview = findViewById(R.id.webview);
        mWebviewPolicy = findViewById(R.id.webviewPolicy);

        //lv
        lvTermsCondition = findViewById(R.id.lvTermsCondition);
        lvPrivacyPolicy = findViewById(R.id.lvPrivacyPolicy);

        //text view for terms and condition
        tvAgree = findViewById(R.id.tvAgree);
        tvAgree.setOnClickListener(this);
        tvDisagree = findViewById(R.id.tvDisagree);
        tvDisagree.setOnClickListener(this);

        //text view for policy
        tvAgreePolicy = findViewById(R.id.tvAgreePolicy);
        tvAgreePolicy.setOnClickListener(this);
        tvDisagreePolicy = findViewById(R.id.tvDisagreePolicy);
        tvDisagreePolicy.setOnClickListener(this);

        progressDialog = ProgressDialog.show(TermsConditionStaffActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        mWebview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(TermsConditionStaffActivity.this, description, Toast.LENGTH_SHORT).show();
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });

//        mWebview.loadUrl("http://77.104.138.70/~scaveng2/yearbook_dev/terms.html");
        mWebview.loadUrl(new BaseUrl().getHost() + "/termsofuse");
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Terms and condition");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvAgree) {
            OpenPrivacyPolicy();
        } else if (v.getId() == R.id.tvDisagree) {
            DisAgree();
        } else if (v.getId() == R.id.tvAgreePolicy) {
            Agree();
        } else if (v.getId() == R.id.tvDisagreePolicy) {
            DisAgree();
        }
    }

    public void OpenPrivacyPolicy() {
        lvTermsCondition.setVisibility(View.GONE);
        lvPrivacyPolicy.setVisibility(View.VISIBLE);
        toolbar_title.setText("Privacy policy");

        progressDialog = ProgressDialog.show(TermsConditionStaffActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        mWebviewPolicy.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(TermsConditionStaffActivity.this, description, Toast.LENGTH_SHORT).show();
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });

//        mWebviewPolicy.loadUrl("http://77.104.138.70/~scaveng2/yearbook_dev/privacy.html");
        mWebviewPolicy.loadUrl(new BaseUrl().getHost()+"/privacypolicy");
    }

    public void Agree() {
        int staffId = Integer.parseInt(loginUserSP.getUserData().getStaffId());
        progressDialog.show();
        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<GiveReport> termsCondition = retrofitClass.termsConditionStaff(staffId, "agree");
        termsCondition.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (response.body().getResponseCode().equals("1")) {
                    StaffStudent staffUser = loginUserSP.getUserData();
                    staffUser.setStaffIsAgree("1");
                    loginUserSP.setUserData(staffUser);

                    Log.d("TTT", "TermsCondition: " + loginUserSP.getUserData().getStaffIsAgree());

                    Intent i = new Intent(getApplicationContext(), StaffHomeActivity.class);
                    startActivity(i);
                    finish();
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(TermsConditionStaffActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(TermsConditionStaffActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(TermsConditionStaffActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    allMethods.setAlert(TermsConditionStaffActivity.this, "", response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(TermsConditionStaffActivity.this, getResources().getString(R.string.app_name), t.getMessage());
            }
        });

    }

    public void DisAgree() {
        String token = PreferenceManager.getDefaultSharedPreferences(TermsConditionStaffActivity.this).getString("Token", "");
        Log.d("TTT", "token: " + token);

        if (!token.equals("")) {
            progressDialog = ProgressDialog.show(TermsConditionStaffActivity.this, "", "", true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.progress_view);
            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
            Circle bounce = new Circle();
            bounce.setColor(Color.BLACK);
            progressBar.setIndeterminateDrawable(bounce);

            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
            final Call<GiveReport> userPost = retrofitClass.logout(token, "staff");
            userPost.enqueue(new Callback<GiveReport>() {
                @Override
                public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    LoginUser loginSP = new LoginUser(TermsConditionStaffActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(TermsConditionStaffActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

                @Override
                public void onFailure(Call<GiveReport> call, Throwable t) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    allMethods.setAlert(TermsConditionStaffActivity.this, "", t.getMessage() + "");
                }
            });
        } else {
            LoginUser loginSP = new LoginUser(TermsConditionStaffActivity.this);
            loginSP.clearData();

            Intent i = new Intent(TermsConditionStaffActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
    }
}

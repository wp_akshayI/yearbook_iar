package com.app.yearbook;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.model.staff.yearbook.GetStaffYearbook;
import com.app.yearbook.model.staff.yearbook.StaffYBPostList;
import com.app.yearbook.model.yearbook.CategoryList;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisplayYearbookActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private AllMethods allMethods;
    private CategoryList category;
    public ProgressDialog progressDialog;
    private ArrayList<StaffYBPostList> postLists;
    private LinearLayout lvMain, lvNoData;
    private ImageView img;//img  imgSetImage
    private LoginUser loginUser;
    private int deviceHeight,finalHeight,finalWidth;
    private int schoolId, staffId, IntentData = 999,IntentDeleteEdit=222;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_yearbook);
        setView();
    }

    public void setView() {
        //all methods object
        allMethods = new AllMethods();

        deviceHeight=getApplicationContext().getResources().getDisplayMetrics().heightPixels;
        Log.d("TTT","Device height: "+deviceHeight);

        finalHeight=(deviceHeight*350)/1280;
        Log.d("TTT","Final height: "+finalHeight);

        finalWidth=getApplicationContext().getResources().getDisplayMetrics().widthPixels/3;

        //SP object
        loginUser = new LoginUser(DisplayYearbookActivity.this);
        if (loginUser.getUserData() != null) {
            schoolId = Integer.parseInt(loginUser.getUserData().getSchoolId());
            staffId = Integer.parseInt(loginUser.getUserData().getStaffId());
        }

        if (getIntent().getExtras() != null) {
            category = (CategoryList) getIntent().getSerializableExtra("category");
            Log.d("TTT","CID: "+category.getCategoryId());
        }

        //lv
        lvMain = findViewById(R.id.lvMain);

        lvNoData = findViewById(R.id.lvNoData);

        //set toolbar
        setToolbar();

        getYearbook(schoolId, staffId);
        //getYearbook(16, 54);
    }

    //set toolbar
    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(category.getCategoryName());
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.advertisement) {

            AlertDialog.Builder builder = new AlertDialog.Builder(DisplayYearbookActivity.this);
            builder.setTitle("Available actions");
            builder.setCancelable(false);
            builder.setItems(new CharSequence[]{"Student's portrait","Photo", "Video","Cancel"},
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 1:
                                    Intent i = new Intent(DisplayYearbookActivity.this, StaffPhotoVideoSelectActivity.class);
                                    i.putExtra("actions", "photo");
                                    i.putExtra("CategoryId", category.getCategoryId());
                                    startActivityForResult(i, IntentData);
                                    break;
                                case 2:
                                    Intent ii = new Intent(DisplayYearbookActivity.this, StaffPhotoVideoSelectActivity.class);
                                    ii.putExtra("actions", "video");
                                    ii.putExtra("CategoryId", category.getCategoryId());
                                    startActivityForResult(ii, IntentData);
                                    break;
                                case 0:
                                    Intent iii = new Intent(DisplayYearbookActivity.this, SchoolAllStudentActivity.class);
                                    iii.putExtra("SchoolId", schoolId);
                                    iii.putExtra("CategoryId", category.getCategoryId());
                                    startActivityForResult(iii, IntentData);
                                    //dialog.dismiss();
                                    break;
                                case 3:
                                    dialog.dismiss();
                                    break;
                            }
                        }
                    });
            builder.create().show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_advertisement, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //function for get yearbook
    public void getYearbook(int schoolId, int staffId) {

        Log.d("TTT", "Data: " + schoolId + " / " + staffId + " / " + category.getCategoryId());
        progressDialog = ProgressDialog.show(DisplayYearbookActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetStaffYearbook> yearbook = retrofitClass.getStaffYearbookPost(Integer.parseInt(category.getCategoryId()), schoolId, staffId, "staff");
        yearbook.enqueue(new Callback<GetStaffYearbook>() {
            @Override
            public void onResponse(Call<GetStaffYearbook> call, Response<GetStaffYearbook> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {

                    lvNoData.setVisibility(View.GONE                                          );
                    lvMain.setVisibility(View.VISIBLE);

                    postLists = new ArrayList<>();

                    if (response.body().getPostList().size() > 0) {
                        postLists.clear();
                        postLists.addAll(response.body().getPostList());

                        setImage();
                    }
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(DisplayYearbookActivity.this,response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(DisplayYearbookActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(DisplayYearbookActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    lvNoData.setVisibility(View.VISIBLE);
                    lvMain.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<GetStaffYearbook> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                lvNoData.setVisibility(View.VISIBLE);
                lvMain.setVisibility(View.GONE);
                allMethods.setAlert(DisplayYearbookActivity.this, "", t.getMessage() + "");
            }
        });
    }

    //function for set image in staff side
    public void setImage() {
        lvMain.removeAllViews();
        int counter = 0, mainCounter = 0;

        for (int i = 0; i < postLists.size(); i++) {
            if (mainCounter > postLists.size() - 1) {
                break;
            }
            Log.d("TTT", "counter main row wise: " + i);

            LinearLayout l = new LinearLayout(this);
            l.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            l.setLayoutParams(lparams);
            l.setGravity(Gravity.CENTER);
            l.setWeightSum(3);
            for (int j = 0; j < 3; j++) {
                Log.d("TTT", "Type::: " + postLists.get(i).getFileSizeType());
                if (mainCounter > postLists.size() - 1) {

                } else {
                    if (postLists.get(mainCounter).getFileType().equalsIgnoreCase("2")) {
                        //If we set video file display type 2 (medium)
                        if (counter >= 2) {
                            counter = 0;
                            break;
                        }
                        img = new ImageView(this);
                        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        Glide.with(DisplayYearbookActivity.this)
                                .load(postLists.get(mainCounter).getPostThumbnail())  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(img);

                        ImageView imgVideoIcon = new ImageView(this);
                        imgVideoIcon.setImageDrawable(getResources().getDrawable(R.mipmap.video_camera_ic));
                        LinearLayout.LayoutParams lvVideo = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        lvVideo.gravity = Gravity.BOTTOM | Gravity.RIGHT;
                        lvVideo.setMargins(20, 20, 20, 20);
                        imgVideoIcon.setLayoutParams(lvVideo);

                        LinearLayout lvImgVideo = new LinearLayout(this);
                        LinearLayout.LayoutParams lparamsVideo = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                        lparamsVideo.gravity = Gravity.BOTTOM;
                        lvImgVideo.setLayoutParams(lparamsVideo);
                        lvImgVideo.setGravity(Gravity.END);
                        //lvImgVideo.setBackground(getResources().getDrawable(R.mipmap.video_camera_ic));
                        lvImgVideo.addView(imgVideoIcon);

                        //Adding views to FrameLayout
                        FrameLayout framelayout = new FrameLayout(this);
                        LinearLayout.LayoutParams lll;
                        if (counter == 0) {
                            Log.d("TTT","counter  0");
                            if ((mainCounter + 1) < postLists.size()) {
                                Log.d("TTT","counter  0 if");
                                if (postLists.get(mainCounter + 1).getFileSizeType().equals("3")) {
                                    Log.d("TTT","counter  0 if if");
                                    lll = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                    lll.setMargins(3, 3, 3, 3);
                                    counter=counter+2;
                                }
                                else if (postLists.get(mainCounter + 1).getFileSizeType().equals("2")) {
                                    Log.d("TTT","counter  0 if else if");
                                    l.setWeightSum(2);
                                    lll = new LinearLayout.LayoutParams(0, finalHeight, 1);
                                    lll.setMargins(3, 3, 3, 3);
                                    counter = counter + 1;
                                }
                                else {
                                    Log.d("TTT","counter  0 if else");
                                    lll = new LinearLayout.LayoutParams(0, finalHeight, 2);
                                    lll.setMargins(3, 3, 3, 3);
                                    counter=counter+2;
                                }
                            } else {
                                Log.d("TTT","counter  0 else ");
                                lll = new LinearLayout.LayoutParams(0, finalHeight, 2);
                                lll.setMargins(3, 3, 3, 3);
                                counter=counter+2;
                            }

                        } else {
                            Log.d("TTT","counter not  0");
                            if (postLists.get(mainCounter - 1).getFileSizeType().equals("2")) {
                                Log.d("TTT","counter not  0 if");
                                lll = new LinearLayout.LayoutParams(0, finalHeight, 1);
                                lll.setMargins(3, 3, 3, 3);
                                counter = counter + 1;
                            }
                            else {
                                Log.d("TTT","counter not  0 else");
                                lll = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                lll.setMargins(3, 3, 3, 3);
                                counter=counter+2;
                            }
                        }

                        framelayout.setLayoutParams(lll);
                        framelayout.addView(img);
                        framelayout.addView(lvImgVideo);

                        Log.d("TTT", "counter: " + mainCounter);
                        img.setTag(postLists.get(mainCounter));

                        Log.d("TTT","frame layout height: "+framelayout.getHeight());
                        l.addView(framelayout);
                        img.setOnClickListener(submitListener);

                        //counter = counter + 2;
                        mainCounter = mainCounter + 1;

                        Log.d("TTT","Linear View 22: "+img.getHeight());

                    } else {
                        if (postLists.get(mainCounter).getFileSizeType().equals("3")) {
                            //small size
                            img = new ImageView(this);
                            LinearLayout.LayoutParams lll = null;
                            if (counter == 0) {
                                Log.d("TTT","counter 0");
                                if (mainCounter == postLists.size() - 1) {
                                    Log.d("TTT","mainCounter if "+mainCounter);
                                    l.setWeightSum(0);
                                  //  lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(finalHeight, finalHeight));
                                    lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(finalWidth,finalWidth));

                                } else {
                                    Log.d("TTT","mainCounter else"+mainCounter);
                                    if (getType(mainCounter).equals("1")) {
                                        Log.d("TTT","mainCounter else if"+mainCounter);
                                        l.setWeightSum(0);
                                       // lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(finalHeight, finalHeight));
                                        lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(finalWidth,finalWidth));

                                    } else {
                                        Log.d("TTT","mainCounter else else"+mainCounter);
                                        lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(0,finalWidth, 1));
                                    }
                                }
                            } else {
                                Log.d("TTT","counter not 0");
                                lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(0, finalWidth, 1));
                            }

                            lll.setMargins(3, 3, 3, 3);
                            img.setLayoutParams(lll);

                            img.setScaleType(ImageView.ScaleType.FIT_XY);

                            Log.d("TTT", "url 0: " + postLists.get(mainCounter).getPostFile());
                            Glide.with(DisplayYearbookActivity.this)
                                    .load(postLists.get(mainCounter).getPostFile())  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                    .placeholder(R.mipmap.yearbook_placeholder)
                                    .error(R.mipmap.yearbook_placeholder)
                                    .into(img);

                            Log.d("TTT", "counter: " + mainCounter);
                            img.setTag(postLists.get(mainCounter));
                            l.addView(img);
                            img.setOnClickListener(submitListener);
                            mainCounter = mainCounter + 1;
                            counter = counter + 1;

                            Log.d("TTT","Linear View 3: "+l.getHeight());
                        } else if (postLists.get(mainCounter).getFileSizeType().equals("2")) {

                            //medium size
                            //before changes
//                            if (counter >= 2) {
//                                counter = 0;
//                                break;
//                            }
//                            img = new ImageView(this);
//                            img.setScaleType(ImageView.ScaleType.CENTER_CROP);
//                            LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
//                            lll.setMargins(3, 3, 3, 3);
//                            img.setLayoutParams(lll);
//
//                            Log.d("TTT", "url: " + postLists.get(mainCounter).getPostFile());
//                            Glide.with(DisplayYearbookActivity.this)
//                                    .load(postLists.get(mainCounter).getPostFile())
//                                    .placeholder(R.mipmap.home_post_img_placeholder)
//                                    .error(R.mipmap.home_post_img_placeholder)
//                                    .into(img);
//
//                            Log.d("TTT", "counter: " + mainCounter);
//                            img.setTag(postLists.get(mainCounter));
//                            l.addView(img);
//                            img.setOnClickListener(submitListener);
//                            counter = counter + 2;
//                            mainCounter = mainCounter + 1;
//
//                            Log.d("TTT","Linear View 2: "+l.getHeight());

                            //after changes
                            Log.d("TTT","----: "+mainCounter+" / "+counter);
                            if (counter >= 2) {
                                counter = 0;
                                break;
                            }
                            else {
                                img = new ImageView(this);
                                img.setScaleType(ImageView.ScaleType.CENTER_CROP);

                                if (counter == 0) {
                                    Log.d("TTT","counter  0");
                                    if ((mainCounter + 1) < postLists.size()) {
                                        Log.d("TTT","mainCounter if if: "+mainCounter);
                                        if (postLists.get(mainCounter + 1).getFileSizeType().equals("3")) {
                                            Log.d("TTT","mainCounter if----if: "+mainCounter);
                                            LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                            lll.setMargins(3, 3, 3, 3);
                                            img.setLayoutParams(lll);
                                            counter = counter + 2;
                                        }
                                        else if (postLists.get(mainCounter + 1).getFileSizeType().equals("2")) {
                                            l.setWeightSum(2);
                                            LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, finalHeight, 1);
                                            lll.setMargins(3, 3, 3, 3);
                                            img.setLayoutParams(lll);
                                            counter = counter + 1;
                                        }
                                        else {
                                            Log.d("TTT","mainCounter if else: "+mainCounter);
                                            LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, finalHeight, 2);
                                            lll.setMargins(3, 3, 3, 3);
                                            img.setLayoutParams(lll);
                                            counter = counter + 2;
                                        }
                                    }
                                    else {

                                        Log.d("TTT","mainCounter else: ");
                                        LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, finalHeight, 2);
                                        lll.setMargins(3, 3, 3, 3);
                                        img.setLayoutParams(lll);
                                        counter = counter + 2;
                                    }

                                } else {
                                    Log.d("TTT", "counter not 0");
                                    if (postLists.get(mainCounter - 1).getFileSizeType().equals("2")) {
                                        //l.setWeightSum(2);
                                        LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, finalHeight, 1);
                                        lll.setMargins(3, 3, 3, 3);
                                        img.setLayoutParams(lll);
                                        counter = counter + 1;
                                    }
                                    else {
                                        LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                        lll.setMargins(3, 3, 3, 3);
                                        img.setLayoutParams(lll);
                                        counter = counter + 2;
                                    }
                                }
                                // lparams.weight=1;
                                Glide.with(DisplayYearbookActivity.this)
                                        .load(postLists.get(mainCounter).getPostFile())  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                        .placeholder(R.mipmap.home_post_img_placeholder)
                                        .error(R.mipmap.home_post_img_placeholder)
                                        .into(img);

                                img.setTag(postLists.get(mainCounter));
                                l.addView(img);
                                img.setOnClickListener(submitListener);
                               // counter = counter + 2;
                                mainCounter = mainCounter + 1;
                            }

                        } else if (postLists.get(mainCounter).getFileSizeType().equals("1")) {

                            //large size
                            if (counter != 0) {
                                counter = 0;
                                break;
                            } else {
                                img = new ImageView(DisplayYearbookActivity.this);
                                img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0,finalHeight, 3);
                                lll.setMargins(3, 3, 3, 3);
                                img.setLayoutParams(lll);


                                Log.d("TTT", "url: " + postLists.get(mainCounter).getPostFile());
                                Glide.with(this)
                                        .load(postLists.get(mainCounter).getPostFile())  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                        .placeholder(R.mipmap.home_post_img_placeholder)
                                        .error(R.mipmap.home_post_img_placeholder)
                                        .into(img);


                                Log.d("TTT", "counter: " + mainCounter);
                                img.setTag(postLists.get(mainCounter));
                                l.addView(img);
                                img.setOnClickListener(submitListener);
                                counter = counter + 3;
                                mainCounter = mainCounter + 1;
                            }

                            Log.d("TTT","Linear View 1 : "+l.getHeight());

                        }
                    }
                }

                if (counter == 3) {
                    counter = 0;
                    break;
                }
            }
            lvMain.addView(l);
        }
    }

    private View.OnClickListener submitListener = new View.OnClickListener() {
        public void onClick(View view) {

            StaffYBPostList staff=(StaffYBPostList)view.getTag();
            Log.d("TTT","Post Id: "+staff.getPostId());
            Log.d("TTT","Post url: "+staff.getPostFile());

           // if(staff.getPostFile()!=null && !staff.getPostFile().equals("")) {
                Intent i = new Intent(DisplayYearbookActivity.this, ManageStaffPostActivity.class);
                i.putExtra("StaffYBPostList", (StaffYBPostList) view.getTag());
                startActivityForResult(i, IntentDeleteEdit);
//            }
//            else
//            {
//                Toast.makeText(getApplicationContext(),"Student's portrait is deleted",Toast.LENGTH_SHORT).show();
//            }
        }
    };

    public String getType(int counter) {
        String type = null;
        type = postLists.get(counter + 1).getFileSizeType();
        return type;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentData && resultCode == RESULT_OK) {
            getYearbook(schoolId, staffId);
        }
        else if(requestCode==IntentDeleteEdit && resultCode==RESULT_OK)
        {
            if(data.getExtras()!=null)
            {
                String optionDelete=data.getStringExtra("Option");
                Log.d("TTT","Option::::: "+optionDelete);
                if(optionDelete.equalsIgnoreCase("Delete"))
                {
                    getYearbook(schoolId, staffId);
                }
                else if(optionDelete.equalsIgnoreCase("Edit"))
                {
                    getYearbook(schoolId, staffId);
                }
            }
        }
    }
}

package com.app.yearbook;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.yearbook.adapter.AdapterPostList;
import com.app.yearbook.adapter.UserPostAdapter;
import com.app.yearbook.databinding.ActivitySearchBinding;
import com.app.yearbook.interfaces.OnMenuOptionClick;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.studenthome.LikeUnlike;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.model.postmodels.SearchResponse;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.Constants;
import com.app.yearbook.utils.OptionMenuController;
import com.github.ybq.android.spinkit.style.Circle;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = "SearchActivity";

    ActivitySearchBinding binding;
    SearchView searchView;
    AdapterPostList postListAdapter;
    UserPostAdapter userPostAdapter;
    private LoginUser loginUser;
    public int schoolId, userId, page = 1;
    ArrayList<PostListItem> PostListArray;
    RetrofitClass retrofitClass;
    String type;
    ProgressDialog progressDialog;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        setSupportActionBar(binding.appbar.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        binding.appbar.toolbarTitle.setText(getString(R.string.lbl_search));
        setData();
    }

    private void addPollVote(final int pos, String pollOptionId) {
        Call<Login> addBookMark = retrofitClass.getUserVotePoll(pollOptionId, LoginUser.getUserId()+"", LoginUser.getUserTypeKey());
        addBookMark.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Log.d(TAG, "onResponse: ");
                postListAdapter.notifyItemChanged(pos);
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }

    private void setData() {
        if (loginUser.getUserData() != null) {
            schoolId = Integer.parseInt(loginUser.getUserData().getSchoolId());
            if (getIntent().getStringExtra("type").equals("student")) {
                type = "1";
                userId = Integer.parseInt(LoginUser.getUserData().getUserId());
//                PostListArray = getIntent().getParcelableArrayListExtra("PostListArray");
                PostListArray = new ArrayList<>();
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                binding.rvPostList.setLayoutManager(mLayoutManager);
//                userPostAdapter = new UserPostAdapter(PostListArray, this, "");
//                binding.rvPostList.setAdapter(postListAdapter);
                postListAdapter = new AdapterPostList(PostListArray, new OnRecyclerClick() {
                    @Override
                    public void onVote(int pos, String pollOptionId) {
                        addPollVote(pos, pollOptionId);
                    }

                    @Override
                    public void onClick(int pos, int type) {
                        switch (type) {
                            case Constants.TYPE_BOOKMARK:
                                if (!PostListArray.get(pos).getPostBookmark()) {
                                    PostListArray.get(pos).setPostBookmark("True");
                                } else {
                                    PostListArray.get(pos).setPostBookmark("False");
                                }
                                addPostBookmark(PostListArray.get(pos));
                                break;
                            case Constants.TYPE_VIEW_IMAGE:
                                Intent i = new Intent(SearchActivity.this, ImageActivity.class);
                                i.putExtra("caption", PostListArray.get(pos).getPostDescription());
                                i.putExtra("FileUrl", PostListArray.get(pos).getPostFile());
                                startActivity(i);
                                break;
                            case Constants.TYPE_LIKE:
                                if (!PostListArray.get(pos).getPostLike()) {
                                    PostListArray.get(pos).setPostLike("True");
                                    String likes = String.valueOf(Integer.parseInt(PostListArray.get(pos).getTotalLike()) + 1);
                                    PostListArray.get(pos).setTotalLike(likes);
                                } else {
                                    PostListArray.get(pos).setPostLike("False");
                                    String likes = String.valueOf(Integer.parseInt(PostListArray.get(pos).getTotalLike()) - 1);
                                    PostListArray.get(pos).setTotalLike(likes);
                                }
                                addPostLikeDislike(PostListArray.get(pos));
                                break;
                            case Constants.VIEW_PROFILE:
                                openProfile(PostListArray.get(pos));
                                break;
                        }
                    }
                });
                binding.rvPostList.setAdapter(postListAdapter);
            } else if (getIntent().getStringExtra("type").equals("staff")) {
                type = "2";
                userId = Integer.parseInt(loginUser.getUserData().getStaffId());
//                PostListArray = getIntent().getParcelableArrayListExtra("PostListArray");
                PostListArray = new ArrayList<>();
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                binding.rvPostList.setLayoutManager(mLayoutManager);
                postListAdapter = new AdapterPostList(PostListArray, new OnRecyclerClick() {
                    @Override
                    public void onVote(int pos, String pollOptionId) {
                        addPollVote(pos, pollOptionId);
                    }

                    @Override
                    public void onClick(int pos, int type) {
                        switch (type) {
                            case Constants.TYPE_BOOKMARK:
                                if (!PostListArray.get(pos).getPostBookmark()) {
                                    PostListArray.get(pos).setPostBookmark("True");
                                } else {
                                    PostListArray.get(pos).setPostBookmark("False");
                                }
                                addPostBookmark(PostListArray.get(pos));
                                break;
                            case Constants.TYPE_VIEW_IMAGE:
                                Intent i = new Intent(SearchActivity.this, ImageActivity.class);
                                i.putExtra("caption", PostListArray.get(pos).getPostDescription());
                                i.putExtra("FileUrl", PostListArray.get(pos).getPostFile());
                                startActivity(i);
                                break;
                            case Constants.TYPE_LIKE:
                                if (!PostListArray.get(pos).getPostLike()) {
                                    PostListArray.get(pos).setPostLike("True");
                                    String likes = String.valueOf(Integer.parseInt(PostListArray.get(pos).getTotalLike()) + 1);
                                    PostListArray.get(pos).setTotalLike(likes);
                                } else {
                                    PostListArray.get(pos).setPostLike("False");
                                    String likes = String.valueOf(Integer.parseInt(PostListArray.get(pos).getTotalLike()) - 1);
                                    PostListArray.get(pos).setTotalLike(likes);
                                }
                                addPostLikeDislike(PostListArray.get(pos));
                                break;
                            case Constants.VIEW_PROFILE:
                                openProfile(PostListArray.get(pos));
                                break;
                        }
                    }
                });
                binding.rvPostList.setAdapter(postListAdapter);
            }

            OptionMenuController controller = new OptionMenuController(this, postListAdapter, (ArrayList<PostListItem>) PostListArray);
            controller.setOptionMenu(new OnMenuOptionClick() {
                @Override
                public void onClick(int pos, int type, View view) {
                    if (PostListArray.get(pos).getPostType().equalsIgnoreCase(Constants.POST_TEXT_INT)) {
                        startActivityForResult(new Intent(SearchActivity.this, NewTextPostActivity.class)
                                .putExtra(Constants.INTENT_ISEDIT, true)
                                .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), Constants.NEW_POST);
                    } else if (PostListArray.get(pos).getPostType().equalsIgnoreCase(Constants.POST_IMAGE_VIDEO_INT)) {
                        startActivityForResult(new Intent(SearchActivity.this, NewImageVideoActivity.class)
                                .putExtra(Constants.POST_TYPE, (PostListArray.get(pos).getPostFileType().equalsIgnoreCase("1")) ? Constants.POST_IMAGE : Constants.POST_VIDEO)
                                .putExtra(Constants.INTENT_ISEDIT, true)
                                .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), Constants.NEW_POST);
                    } else {
                        startActivityForResult(new Intent(SearchActivity.this, NewPollActivity.class)
                                .putExtra(Constants.INTENT_ISEDIT, true)
                                .putExtra(Constants.INTENT_DATA, PostListArray.get(pos)), Constants.NEW_POST);
                    }
                }
            });
        }

    }

    private void openDeletePopUp(final int pos) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        callDeletePostApi(pos);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(getString(R.string.delete_msg)).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void callDeletePostApi(final int pos) {
        progressDialog = ProgressDialog.show(context, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> deletePost = retrofitClass.deletePost(Integer.parseInt(PostListArray.get(pos).getPostId()));
        deletePost.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    Toast.makeText(context, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                    PostListArray.remove(pos);
                    postListAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(context, t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: Staff home fragmnet: "+resultCode);
        if (requestCode == Constants.NEW_POST) {
            if (resultCode == SearchActivity.RESULT_OK) {
//                setData();
                searchPeople(searchView.getQuery().toString());
                hideKeyboard();
            }
        }
    }

    private void openProfile(PostListItem postListItem) {
        Intent i1;
        if(postListItem.getFlag().equalsIgnoreCase("staff")){
            i1 = new Intent(SearchActivity.this, OtherStaffProfileActivity.class);
        }else{
            i1 = new Intent(SearchActivity.this, OtherUserProfileActivity.class);
        }

        i1.putExtra(Constants.USER_DATA, postListItem);

        startActivity(i1);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        searchView.setIconifiedByDefault(true);
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.clearFocus();
        if (getIntent().hasExtra("query")) {
            String query = getIntent().getStringExtra("query");
            searchView.setQuery(query, false);
            searchPeople(query);
            hideKeyboard();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private void dimEnable(boolean b) {
        if (b) {
            binding.rvPostList.setAlpha(0.5f);
        } else {
            binding.rvPostList.setAlpha(1.0f);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchview, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (androidx.appcompat.widget.SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Search users, tags, post and polls");
        ImageView v = searchView.findViewById(R.id.search_button);
        v.setImageResource(R.drawable.ic_search);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!query.equals("")) {
                    searchView.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(searchView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    }

                    searchPeople(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                Log.d(TAG, "onClose: ");
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void searchPeople(String query) {
        progressDialog = ProgressDialog.show(this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);
        binding.txtNoDta.setVisibility(View.GONE);
        final Call<SearchResponse> searchPost = retrofitClass.searchPost(userId, schoolId, query, type);
        searchPost.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                SearchResponse searchResponse = response.body();
                if (searchResponse != null) {
                    if (PostListArray != null) {
                        PostListArray.clear();
                    }

                    if (searchResponse.getPostList() != null) {
                        PostListArray.addAll(searchResponse.getPostList());
                        if (PostListArray.size() > 0) {
                            postListAdapter.notifyDataSetChanged();
                        }else {
                            binding.txtNoDta.setVisibility(View.VISIBLE);
                        }
                    } else {
                        binding.txtNoDta.setVisibility(View.VISIBLE);
                    }
                }else {
                    binding.txtNoDta.setVisibility(View.VISIBLE);
                }
                progressDialog.dismiss();

                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.d(TAG, "onFailure: ");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            hideKeyboard();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(searchView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void addPostBookmark(PostListItem postListItem) {
        Call<Login> addBookMark = retrofitClass.addBookmarkUnbookmark(LoginUser.getUserData().getStaffId(), postListItem.getPostId());
        addBookMark.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }

    private void addPostLikeDislike(PostListItem postListItem) {
        Call<LikeUnlike> addBookMark = retrofitClass.postLikeDislike(Integer.parseInt(LoginUser.getUserTypeKey()==2?loginUser.getUserData().getStaffId():loginUser.getUserData().getUserId()),
                Integer.parseInt(postListItem.getPostId()), LoginUser.getUserTypeKey());
        addBookMark.enqueue(new Callback<LikeUnlike>() {
            @Override
            public void onResponse(Call<LikeUnlike> call, Response<LikeUnlike> response) {
                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<LikeUnlike> call, Throwable t) {
                Log.d(TAG, "onResponse: ");
            }
        });
    }
}

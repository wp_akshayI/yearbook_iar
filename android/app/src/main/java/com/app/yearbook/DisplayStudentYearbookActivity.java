package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.yearbook.model.studenthome.profile.GetUserProfile;
import com.app.yearbook.model.studenthome.profile.UserProfile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.model.staff.yearbook.GetStaffYearbook;
import com.app.yearbook.model.staff.yearbook.StaffYBPostList;
import com.app.yearbook.model.yearbook.CategoryList;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisplayStudentYearbookActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private AllMethods allMethods;
    private CategoryList category;
    public ProgressDialog progressDialog;
    public static ArrayList<StaffYBPostList> postLists;
    private LinearLayout lvMain, lvNoData;
    private ImageView img;//img  imgSetImage
    private LoginUser loginUser;
    private int schoolId, userId, IntentData = 999, IntentDeleteEdit = 222;
    private int deviceHeight, finalHeight, finalWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_yearbook);
        setView();
    }

    public void setView() {
        //all methods object
        allMethods = new AllMethods();

        deviceHeight = getApplicationContext().getResources().getDisplayMetrics().heightPixels;
        Log.d("TTT", "Device height: " + deviceHeight);

        finalHeight = (deviceHeight * 350) / 1280;
        Log.d("TTT", "Final height: " + finalHeight);

        finalWidth = getApplicationContext().getResources().getDisplayMetrics().widthPixels / 3;

        //SP object
        loginUser = new LoginUser(DisplayStudentYearbookActivity.this);
        if (loginUser.getUserData() != null) {
            schoolId = Integer.parseInt(loginUser.getUserData().getSchoolId());
            userId = Integer.parseInt(loginUser.getUserData().getUserId());
        }

        if (getIntent().getExtras() != null) {
            category = (CategoryList) getIntent().getSerializableExtra("category");
            Log.d("TTT", "CID: " + category.getCategoryId());
        }

        //lv
        lvMain = findViewById(R.id.lvMain);

        lvNoData = findViewById(R.id.lvNoData);

        //set toolbar
        setToolbar();

        //get yearbook
        getYearbook(schoolId, userId);
        //getYearbook(16, 54);
    }

    //set toolbar
    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(category.getCategoryName());
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.add_advertisement, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //function for get yearbook
    public void getYearbook(int schoolId, int userId) {

        Log.d("TTT", "Data: " + schoolId + " / " + userId + " / " + category.getCategoryId());
        progressDialog = ProgressDialog.show(DisplayStudentYearbookActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetStaffYearbook> yearbook = retrofitClass.getStaffYearbookPost(Integer.parseInt(category.getCategoryId()), schoolId, userId, "user");
        yearbook.enqueue(new Callback<GetStaffYearbook>() {
            @Override
            public void onResponse(Call<GetStaffYearbook> call, Response<GetStaffYearbook> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    postLists = new ArrayList<>();

                    if (response.body().getPostList().size() > 0) {
                        postLists.clear();
                        postLists.addAll(response.body().getPostList());

                        setImage();
                    }
                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(DisplayStudentYearbookActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(DisplayStudentYearbookActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(DisplayStudentYearbookActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    lvNoData.setVisibility(View.VISIBLE);
                    lvMain.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<GetStaffYearbook> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                lvNoData.setVisibility(View.VISIBLE);
                lvMain.setVisibility(View.GONE);
                allMethods.setAlert(DisplayStudentYearbookActivity.this, "", t.getMessage() + "");
            }
        });
    }

    //function for display image
    public void setImage() {
        lvMain.removeAllViews();
        int counter = 0, mainCounter = 0;

        for (int i = 0; i < postLists.size(); i++) {
            if (mainCounter > postLists.size() - 1) {
                break;
            }
            Log.d("TTT", "counter main row wise: " + i);

            LinearLayout l = new LinearLayout(this);
            l.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            l.setLayoutParams(lparams);
            l.setGravity(Gravity.CENTER);
            l.setWeightSum(3);
            for (int j = 0; j < 3; j++) {
                Log.d("TTT", "Type::: " + postLists.get(i).getFileSizeType());
                if (mainCounter > postLists.size() - 1) {

                } else {
                    if (postLists.get(mainCounter).getFileType().equalsIgnoreCase("2")) {
//                        img = new ImageView(this);
//                        FrameLayout framelayout = new FrameLayout(this);
//                        LinearLayout.LayoutParams lll = null;
//                        if (counter == 0) {
//                            if (mainCounter == postLists.size() - 1) {
//                                l.setWeightSum(0);
//                                lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(finalHeight, finalHeight));
//                                // lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//                            } else {
//                                if (getType(mainCounter).equals("1")) {
//                                    l.setWeightSum(0);
//                                    lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(finalHeight, finalHeight));
//
//                                } else {
//                                    lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(0,finalWidth, 1));
//                                }
//                            }
//                        } else {
//                            lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(0, finalWidth, 1));
//                        }
//
//                        lll.setMargins(3, 3, 3, 3);
//
//                        framelayout.setLayoutParams(lll);
//                        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
//
//                        Glide.with(DisplayStudentYearbookActivity.this)
//                                .load(postLists.get(mainCounter).getPostThumbnail())
//                                .placeholder(R.mipmap.home_post_img_placeholder)
//                                .error(R.mipmap.home_post_img_placeholder)
//                                .into(img);
//
//                        ImageView imgVideoIcon = new ImageView(this);
//
//                        imgVideoIcon.setImageDrawable(getResources().getDrawable(R.mipmap.video_camera_ic));
//                        LinearLayout.LayoutParams lvVideo = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//                        lvVideo.gravity = Gravity.BOTTOM | Gravity.RIGHT;
//                        lvVideo.setMargins(20, 20, 20, 20);
//                        imgVideoIcon.setLayoutParams(lvVideo);
//
//                        LinearLayout lvImgVideo = new LinearLayout(this);
//                        LinearLayout.LayoutParams lparamsVideo = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//                        lparamsVideo.gravity = Gravity.BOTTOM;
//                        lvImgVideo.setLayoutParams(lparamsVideo);
//                        lvImgVideo.setGravity(Gravity.END);
//                        lvImgVideo.addView(imgVideoIcon);
//
//                        //Adding views to FrameLayout
//                        framelayout.addView(img);
//                        framelayout.addView(lvImgVideo);
//
//                        Log.d("TTT", "counter: " + mainCounter);
//                        img.setTag(postLists.get(mainCounter));
//                        l.addView(framelayout);
//                        img.setOnClickListener(submitListener);
//
//                        mainCounter = mainCounter + 1;
//                        counter = counter + 1;

                        //-------------------
                        //If we set video file display type 2 (medium)
                        if (counter >= 2) {
                            counter = 0;
                            break;
                        }
                        img = new ImageView(this);
                        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        Glide.with(DisplayStudentYearbookActivity.this)
                                .load(postLists.get(mainCounter).getPostThumbnail()).diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(img);

                        ImageView imgVideoIcon = new ImageView(this);
                        imgVideoIcon.setImageDrawable(getResources().getDrawable(R.mipmap.video_camera_ic));
                        LinearLayout.LayoutParams lvVideo = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        lvVideo.gravity = Gravity.BOTTOM | Gravity.RIGHT;
                        lvVideo.setMargins(20, 20, 20, 20);
                        imgVideoIcon.setLayoutParams(lvVideo);

                        LinearLayout lvImgVideo = new LinearLayout(this);
                        LinearLayout.LayoutParams lparamsVideo = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                        lparamsVideo.gravity = Gravity.BOTTOM;
                        lvImgVideo.setLayoutParams(lparamsVideo);
                        lvImgVideo.setGravity(Gravity.END);
                        //lvImgVideo.setBackground(getResources().getDrawable(R.mipmap.video_camera_ic));
                        lvImgVideo.addView(imgVideoIcon);

                        //Adding views to FrameLayout
                        FrameLayout framelayout = new FrameLayout(this);
                        LinearLayout.LayoutParams lll;
                        if (counter == 0) {
                            if ((mainCounter + 1) < postLists.size()) {
                                if (postLists.get(mainCounter + 1).getFileSizeType().equals("3")) {
                                    lll = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                    lll.setMargins(3, 3, 3, 3);
                                    counter = counter + 2;
                                } else if (postLists.get(mainCounter + 1).getFileSizeType().equals("2")) {
                                    l.setWeightSum(2);
                                    lll = new LinearLayout.LayoutParams(0, finalHeight, 1);
                                    lll.setMargins(3, 3, 3, 3);
                                    counter = counter + 1;
                                } else {
                                    lll = new LinearLayout.LayoutParams(0, finalHeight, 2);
                                    lll.setMargins(3, 3, 3, 3);
                                    counter = counter + 2;
                                }
                            } else {
                                lll = new LinearLayout.LayoutParams(0, finalHeight, 2);
                                lll.setMargins(3, 3, 3, 3);
                                counter = counter + 2;
                            }

                        } else {
                            if (postLists.get(mainCounter - 1).getFileSizeType().equals("2")) {
                                lll = new LinearLayout.LayoutParams(0, finalHeight, 1);
                                lll.setMargins(3, 3, 3, 3);
                                counter = counter + 1;
                            } else {
                                lll = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                lll.setMargins(3, 3, 3, 3);
                                counter = counter + 2;
                            }
                        }

                        framelayout.setLayoutParams(lll);
                        framelayout.addView(img);
                        framelayout.addView(lvImgVideo);

                        Log.d("TTT", "counter: " + mainCounter);
                        img.setTag(postLists.get(mainCounter));

                        Log.d("TTT", "frame layout height: " + framelayout.getHeight());
                        l.addView(framelayout);
                        img.setOnClickListener(submitListener);

                        //counter = counter + 2;
                        mainCounter = mainCounter + 1;

                        Log.d("TTT", "Linear View 22: " + img.getHeight());


                    } else {

                        if (postLists.get(mainCounter).getFileSizeType().equals("3")) {
                            //small
                            img = new ImageView(this);
                            LinearLayout.LayoutParams lll = null;
                            if (counter == 0) {
                                if (mainCounter == postLists.size() - 1) {
                                    l.setWeightSum(0);
                                    // lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(finalHeight, finalHeight));
                                    lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(finalWidth, finalWidth));
                                } else {
                                    if (getType(mainCounter).equals("1")) {
                                        l.setWeightSum(0);
                                        // lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(finalHeight, finalHeight));
                                        lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(finalWidth, finalWidth));
                                    } else {
                                        lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(0, finalWidth, 1));
                                    }
                                }
                            } else {
                                lll = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(0, finalWidth, 1));
                            }

                            lll.setMargins(3, 3, 3, 3);
                            img.setLayoutParams(lll);

                            img.setScaleType(ImageView.ScaleType.FIT_XY);

                            Log.d("TTT", "url 0: " + postLists.get(mainCounter).getPostFile());
                            Glide.with(DisplayStudentYearbookActivity.this)
                                    .load(postLists.get(mainCounter).getPostFile()).diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                    .placeholder(R.mipmap.yearbook_placeholder)
                                    .error(R.mipmap.yearbook_placeholder)
                                    .into(img);

                            Log.d("TTT", "counter: " + mainCounter);
                            img.setTag(postLists.get(mainCounter));
                            l.addView(img);
                            img.setOnClickListener(submitListener);
                            mainCounter = mainCounter + 1;
                            counter = counter + 1;
                        } else if (postLists.get(mainCounter).getFileSizeType().equals("2")) {
                            //medium
                            Log.d("TTT", "----: " + mainCounter + " / " + counter);
                            if (counter >= 2) {
                                counter = 0;
                                break;
                            } else {
                                img = new ImageView(this);
                                img.setScaleType(ImageView.ScaleType.CENTER_CROP);

                                if (counter == 0) {
                                    Log.d("TTT", "counter  0");
                                    if ((mainCounter + 1) < postLists.size()) {
                                        Log.d("TTT", "mainCounter if if: " + mainCounter);
                                        if (postLists.get(mainCounter + 1).getFileSizeType().equals("3")) {
                                            Log.d("TTT", "mainCounter if----if: " + mainCounter);
                                            LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                            lll.setMargins(3, 3, 3, 3);
                                            img.setLayoutParams(lll);
                                            counter = counter + 2;
                                        } else if (postLists.get(mainCounter + 1).getFileSizeType().equals("2")) {
                                            l.setWeightSum(2);
                                            LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, finalHeight, 1);
                                            lll.setMargins(3, 3, 3, 3);
                                            img.setLayoutParams(lll);
                                            counter = counter + 1;
                                        } else {
                                            Log.d("TTT", "mainCounter if else: " + mainCounter);
                                            LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, finalHeight, 2);
                                            lll.setMargins(3, 3, 3, 3);
                                            img.setLayoutParams(lll);
                                            counter = counter + 2;
                                        }
                                    } else {

                                        Log.d("TTT", "mainCounter else: ");
                                        LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, finalHeight, 2);
                                        lll.setMargins(3, 3, 3, 3);
                                        img.setLayoutParams(lll);
                                        counter = counter + 2;
                                    }

                                } else {
                                    Log.d("TTT", "counter not 0");
                                    if (postLists.get(mainCounter - 1).getFileSizeType().equals("2")) {
                                        //l.setWeightSum(2);
                                        LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, finalHeight, 1);
                                        lll.setMargins(3, 3, 3, 3);
                                        img.setLayoutParams(lll);
                                        counter = counter + 1;
                                    } else {
                                        LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                        lll.setMargins(3, 3, 3, 3);
                                        img.setLayoutParams(lll);
                                        counter = counter + 2;
                                    }
                                }
//                    lparams.weight=1;
                                Glide.with(DisplayStudentYearbookActivity.this)
                                        .load(postLists.get(mainCounter).getPostFile()).diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                        .placeholder(R.mipmap.home_post_img_placeholder)
                                        .error(R.mipmap.home_post_img_placeholder)
                                        .into(img);
                                img.setTag(postLists.get(mainCounter));
                                l.addView(img);
                                img.setOnClickListener(submitListener);
                                // counter = counter + 2;
                                mainCounter = mainCounter + 1;
                            }
                        } else if (postLists.get(mainCounter).getFileSizeType().equals("1")) {
                            //large
                            if (counter != 0) {
                                counter = 0;
                                break;
                            } else {
                                img = new ImageView(DisplayStudentYearbookActivity.this);
                                img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                LinearLayout.LayoutParams lll = new LinearLayout.LayoutParams(0, finalHeight, 3);
                                lll.setMargins(3, 3, 3, 3);
                                img.setLayoutParams(lll);


                                Log.d("TTT", "url: " + postLists.get(mainCounter).getPostFile());
                                Glide.with(this)
                                        .load(postLists.get(mainCounter).getPostFile()).diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                        .placeholder(R.mipmap.home_post_img_placeholder)
                                        .error(R.mipmap.home_post_img_placeholder)
                                        .into(img);


                                Log.d("TTT", "counter: " + mainCounter);
                                img.setTag(postLists.get(mainCounter));
                                l.addView(img);
                                img.setOnClickListener(submitListener);
                                counter = counter + 3;
                                mainCounter = mainCounter + 1;
                            }
                        }
                    }
                }

                if (counter == 3) {
                    counter = 0;
                    break;
                }
            }
            lvMain.addView(l);
        }
    }

    private View.OnClickListener submitListener = new View.OnClickListener() {
        public void onClick(View view) {

            StaffYBPostList staff = (StaffYBPostList) view.getTag();
            Log.d("TTT", "Post Id: " + staff.getPostId());
            Log.d("TTT", "Post url: " + staff.getPostFile());

            if (staff.getPostType().equals("1")) {
                Intent i = new Intent(DisplayStudentYearbookActivity.this, StudentYearbookViewPostActivity.class);
                i.putExtra("PostId", staff.getPostId());
                //i.putExtra("StudentYBPostList",(StaffYBPostList)view.getTag());
                startActivityForResult(i, IntentDeleteEdit);
            } else if (staff.getPostType().equals("2")) {
                displayStudentInfo(staff.getPostUserId());
            }

        }
    };

    private void displayStudentInfo(String postUserId) {

        if (postUserId != null && !postUserId.equals("")) {
            //Progress bar
            progressDialog = ProgressDialog.show(DisplayStudentYearbookActivity.this, "", "", true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.progress_view);
            ProgressBar progressBar1 = progressDialog.findViewById(R.id.progress);
            Circle bounce = new Circle();
            bounce.setColor(Color.BLACK);
            progressBar1.setIndeterminateDrawable(bounce);

            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
            final Call<GetUserProfile> userPost = retrofitClass.getUserProfile(Integer.parseInt(postUserId), schoolId);
            userPost.enqueue(new Callback<GetUserProfile>() {
                @Override
                public void onResponse(Call<GetUserProfile> call, Response<GetUserProfile> response) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if (response.body().getResponseCode().equals("1")) {
                        showData(response.body().getUserProfile());

                    } else if (response.body().getResponseCode().equals("10")) {
                        Toast.makeText(DisplayStudentYearbookActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        LoginUser loginSP = new LoginUser(DisplayStudentYearbookActivity.this);
                        loginSP.clearData();

                        Intent i = new Intent(DisplayStudentYearbookActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<GetUserProfile> call, Throwable t) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    allMethods.setAlert(DisplayStudentYearbookActivity.this, "", t.getMessage() + "");
                }
            });
        }
    }

    public void showData(final UserProfile usertagg) {
        androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(DisplayStudentYearbookActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_alertbox_yearbookuserprofile, null);
        dialogBuilder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog alert = dialogBuilder.create();
        alert.setCancelable(false);
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        TextView tvName = dialogView.findViewById(R.id.tvName);
        tvName.setText(usertagg.getUserDetail().getUserFirstname() + " " + usertagg.getUserDetail().getUserLastname());

        TextView tvGrade=dialogView.findViewById(R.id.tvGrade);
        tvGrade.setText("Grade: "+usertagg.getUserDetail().getUserGrade());

        tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                Intent i = new Intent(DisplayStudentYearbookActivity.this, UserDetailActivity.class);
                i.putExtra("Photo", usertagg.getUserDetail().getUserImage());
               // i.putExtra("type", type);
                i.putExtra("Id", usertagg.getUserDetail().getUserId());
                i.putExtra("schoolId", usertagg.getUserDetail().getSchoolId());
                i.putExtra("Name", usertagg.getUserDetail().getUserFirstname() + " " + usertagg.getUserDetail().getUserLastname());
                startActivity(i);
            }
        });

        LinearLayout lvSign = dialogView.findViewById(R.id.lvSign);

        TextView tvSign = dialogView.findViewById(R.id.tvSign);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Brasileirinha Personal Use.ttf");
        tvSign.setTypeface(typeface);

        lvSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DisplayStudentYearbookActivity.this, AddSignatureActivity.class);
                i.putExtra("searchUserId", usertagg.getUserDetail().getUserId());
                startActivity(i);
            }
        });

        ImageView imgUserPhoto = dialogView.findViewById(R.id.imgUserPhoto);
        Glide.with(DisplayStudentYearbookActivity.this)
                .load(usertagg.getUserDetail().getUserImage())
                .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.home_post_img_placeholder)
                .error(R.mipmap.home_post_img_placeholder)
                .into(imgUserPhoto);

        ImageView imgBack = dialogView.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

//        imgUserPhoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alert.dismiss();
//                Intent i = new Intent(ctx, UserDetailActivity.class);
//                i.putExtra("Photo", usertagg.getUserImage());
//                i.putExtra("type", type);
//                i.putExtra("Id", usertagg.getUserId());
//                i.putExtra("schoolId", usertagg.getSchoolId());
//                i.putExtra("Name", usertagg.getUserFirstname() + " " + usertagg.getUserLastname());
//                ctx.startActivity(i);
//            }
//        });

        alert.show();


    }

    public String getType(int counter) {
        String type = null;
        type = postLists.get(counter + 1).getFileSizeType();
        return type;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == IntentData && resultCode == RESULT_OK) {
//            getYearbook(schoolId, userId);
//        }
//        else if(requestCode==IntentDeleteEdit && resultCode==RESULT_OK)
//        {
//            if(data.getExtras()!=null)
//            {
//                String optionDelete=data.getStringExtra("Option");
//                Log.d("TTT","Option::::: "+optionDelete);
//                if(optionDelete.equalsIgnoreCase("Delete"))
//                {
//                    getYearbook(schoolId, userId);
//                }
//                else if(optionDelete.equalsIgnoreCase("Edit"))
//                {
//                    getYearbook(schoolId, userId);
//                }
//            }
//        }
    }
}

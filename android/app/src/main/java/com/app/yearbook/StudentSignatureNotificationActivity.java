package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.model.NotificationAcceptDeny;
import com.app.yearbook.model.profile.NotificationList;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentSignatureNotificationActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    public ProgressDialog progressDialog;
    Button btnAccpet, btnDeny;
    NotificationList notificationList;
    LinearLayout lvSignature;
    TextView tvMsg;
    int position;
    private static final String TAG = "StudentSignatureNotific";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_signature_notification);

        setView();
    }

    public void setView() {
        //set toolbar
        setToolbar();

        btnAccpet = findViewById(R.id.btnAccept);
        btnDeny = findViewById(R.id.btnDeny);

        btnDeny.setOnClickListener(this);
        btnAccpet.setOnClickListener(this);

        lvSignature=findViewById(R.id.lvSignature);

        tvMsg=findViewById(R.id.tvMsg);


        if (getIntent().getExtras() != null) {
            notificationList = (NotificationList) getIntent().getSerializableExtra("NotificationData");
            position=getIntent().getIntExtra("position",0);
            if(notificationList.getIsAccept().equalsIgnoreCase("0"))
            {
                lvSignature.setVisibility(View.VISIBLE);
            }
            else
            {
                lvSignature.setVisibility(View.GONE);
            }

            Log.d(TAG, "setView: "+notificationList.getSignatureMessage());
            tvMsg.setText(notificationList.getSignatureMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Signature");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void callAPI(final Context context, String id, final String flag) {
        //Progress bar
        progressDialog = ProgressDialog.show(StudentSignatureNotificationActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<NotificationAcceptDeny> userPost = retrofitClass.notificationStatus(Integer.parseInt(id), Integer.parseInt(flag));
        userPost.enqueue(new Callback<NotificationAcceptDeny>() {
            @Override
            public void onResponse(Call<NotificationAcceptDeny> call, Response<NotificationAcceptDeny> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equalsIgnoreCase("1")) {

                     if(NotificationActivity.rvNotification.getAdapter()!=null) {
                         NotificationActivity.notificationLists.get(position).setIsAccept(flag);
                         //notificationList.setIsAccept(flag);
                         NotificationActivity.rvNotification.getAdapter().notifyDataSetChanged();
                     }
                }
                Toast.makeText(context, "" + response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<NotificationAcceptDeny> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(context, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnAccept) {
            callAPI(StudentSignatureNotificationActivity.this, notificationList.getNotificationId(), "1");
        } else if (v.getId() == R.id.btnDeny) {
            //deny
            callAPI(StudentSignatureNotificationActivity.this, notificationList.getNotificationId(), "2");
        }
    }
}

package com.app.yearbook.model.postmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserPost implements Parcelable {

    @SerializedName("post_list")
    private List<PostListItem> postList;

    @SerializedName("staff_list")
    private List<Object> staffList;

    protected UserPost(Parcel in) {
        postList = in.createTypedArrayList(PostListItem.CREATOR);
    }

    public static final Creator<UserPost> CREATOR = new Creator<UserPost>() {
        @Override
        public UserPost createFromParcel(Parcel in) {
            return new UserPost(in);
        }

        @Override
        public UserPost[] newArray(int size) {
            return new UserPost[size];
        }
    };

    public void setPostList(List<PostListItem> postList) {
        this.postList = postList;
    }

    public List<PostListItem> getPostList() {
        return postList;
    }

    public void setStaffList(List<Object> staffList) {
        this.staffList = staffList;
    }

    public List<Object> getStaffList() {
        return staffList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(postList);
    }
}
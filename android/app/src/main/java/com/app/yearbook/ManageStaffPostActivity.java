package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.android.flexbox.FlexboxLayout;
import com.volokh.danylo.hashtaghelper.HashTagHelper;
import com.app.yearbook.databinding.ActivityManageStaffPostBinding;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.staff.yearbook.StaffYBPostList;
import com.app.yearbook.model.staff.yearbook.UserTagg;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.PlayerManager;
import tcking.github.com.giraffeplayer2.VideoInfo;

public class ManageStaffPostActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public ProgressDialog progressDialog;
    private AllMethods allMethods;
    public static StaffYBPostList postList;
    private ActivityManageStaffPostBinding binding;
    private HashTagHelper mEditTextHashTagHelper;
    private int FileType, IntentData = 101, EditIntent = 102;
    private Bitmap thumbImage = null;
    private String getUri, flag = "", option = "", CropType;
    private LinearLayout llayout, lvBubble;
    private View inflatedLayout;
    private TextView tvId, tvName;
    private FlexboxLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_manage_staff_post);
        //setContentView(R.layout.activity_manage_staff_post);
        setView();
    }

    public void setView() {

        //toolbar
        setToolbar();

        //all methods object
        allMethods = new AllMethods();

        //intent
        if (getIntent().getExtras() != null) {
            postList = (StaffYBPostList) getIntent().getSerializableExtra("StaffYBPostList");
            binding.setStaffpost(postList);
            setpostData();
        }
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Edit post");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.postoption, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void setpostData() {
        //post img

        if(binding.getStaffpost().getPostType().equals("1"))
        {
            binding.likeCmnt.setVisibility(View.VISIBLE);
        }
        else
        {
            binding.likeCmnt.setVisibility(View.GONE);
        }

        if (binding.getStaffpost().getPostFile() != null && !binding.getStaffpost().getPostFile().equals("")) {

            final String uri = binding.getStaffpost().getPostFile();
            Log.d("TTT", " " + uri);
            if (uri != null && !uri.equals("")) {
                String extension = uri.substring(uri.lastIndexOf("."));
                Log.d("TTT", "Url type: " + extension);
                getUri = binding.getStaffpost().getPostFile();

                if (!extension.equals("") && (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg"))) {
                    FileType = 1;
                    binding.imgUserPost.setVisibility(View.VISIBLE);
                    binding.videoUserPost.setVisibility(View.GONE);
                    if (binding.getStaffpost().getPostFile() != null) {

                        Glide.with(ManageStaffPostActivity.this)
                                .load(Uri.parse(binding.getStaffpost().getPostFile()))
                                .asBitmap()
                                .fitCenter()
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(final Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                        binding.imgUserPost.setImageBitmap(resource);
                                    }
                                });
                    }

            } else {
                FileType = 2;
                binding.imgUserPost.setVisibility(View.GONE);
                binding.videoUserPost.setVisibility(View.VISIBLE);

//                binding.videoUserPost.reset();
//                binding.videoUserPost.setSource(Uri.parse(binding.getStaffpost().getPostFile()));

                if (binding.videoUserPost.getCoverView() != null) {
                    Glide.with(ManageStaffPostActivity.this)
                            .load(binding.getStaffpost().getPostThumbnail())
                            .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(binding.videoUserPost.getCoverView());
                }

                binding.videoUserPost.getVideoInfo().setPortraitWhenFullScreen(false);
                binding.videoUserPost.setVideoPath(binding.getStaffpost().getPostFile());
                binding.videoUserPost.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player

                Log.d("TTT", "Video urllllll: " + binding.getStaffpost().getPostFile());
            }
        }

            //image touch
            binding.imgUserPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ManageStaffPostActivity.this, ImageActivity.class);
                    i.putExtra("caption", binding.getStaffpost().getPostDescription());
                    i.putExtra("FileUrl", uri);
                    startActivity(i);
                }
            });
        }
        else
        {
            FileType = 1;
            binding.imgUserPost.setVisibility(View.VISIBLE);
            binding.videoUserPost.setVisibility(View.GONE);
        }

        //set file type
        CropType = binding.getStaffpost().getFileSizeType();

        //like
        binding.tvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TTT", "post id adapter: " + binding.getStaffpost().getPostId());
                Intent i = new Intent(ManageStaffPostActivity.this, GetAllStudentLikeActivity.class);
                i.putExtra("postId", binding.getStaffpost().getPostId());
                i.putExtra("Type", "yearbook");
                startActivity(i);
            }
        });

        //comment
        binding.tvComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ManageStaffPostActivity.this, StudentCommentActivity.class);
                i.putExtra("PostId", binding.getStaffpost().getPostId());
                i.putExtra("displayComment", "yearbook");
                i.putExtra("comment", "staff");
                startActivity(i);
            }
        });

        //description
        mEditTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.hashtag), null);
        mEditTextHashTagHelper.handle(binding.tvDesc);

        //set video img
        if (FileType == 2) {
            binding.imgTagUser.setPadding(20, 0, 0, 70);
        }

        //set tag user name in bubble
        llayout = findViewById(R.id.rootlayout);
        llayout.setVisibility(View.GONE);
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        inflatedLayout = inflater.inflate(R.layout.tagg_bubble, null, false);
        lvBubble = inflatedLayout.findViewById(R.id.lvBubble);

        container = inflatedLayout.findViewById(R.id.v_container);

        //set tag user
        if (binding.getStaffpost().getUserTagg().size() > 0) {
            setTagLayout();
            binding.imgTagUser.setVisibility(View.VISIBLE);
        } else {
            binding.imgTagUser.setVisibility(View.GONE);
        }

        Log.d("TTT", "size: " + binding.getStaffpost().getUserTagg().size());
        binding.imgTagUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TTT", "size click: ");
                //displayTagUserRandomPosition();
                displayUserInNinePatch();
            }
        });

//        binding.imgEditPhoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //1 img , 2 video
//                if (FileType == 1) {
//                    Intent i = new Intent(ManageStaffPostActivity.this, StaffPhotoVideoSelectActivity.class);
//                    i.putExtra("actions", "photo");
//                    i.putExtra("EditYearPost", "StaffPost");
//                    startActivityForResult(i, IntentData);
//                } else if (FileType == 2) {
//                    Intent ii = new Intent(ManageStaffPostActivity.this, StaffPhotoVideoSelectActivity.class);
//                    ii.putExtra("actions", "video");
//                    ii.putExtra("EditYearPost", "StaffPost");
//                    startActivityForResult(ii, IntentData);
//                }
//            }
//        });
    }

    public View.OnClickListener lvClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            UserTagg tag = (UserTagg) v.getTag();
            Log.d("TTT", "user/img: " + tag.getUserImage());

            Intent i = new Intent(ManageStaffPostActivity.this, UserDetailActivity.class);
            i.putExtra("Id", tag.getUserId() + "");
            i.putExtra("type", "staff");
            i.putExtra("Name", tag.getUserFirstname() + " " + tag.getUserLastname());
            i.putExtra("schoolId", tag.getSchoolId());
            i.putExtra("Photo", tag.getUserImage());
            startActivity(i);
        }
    };

    public void setTagLayout() {
        Log.d("TTT", "Size:" + binding.getStaffpost().getUserTagg().size());
        if (binding.getStaffpost().getUserTagg().size() > 0) {

            binding.imgTagUser.setVisibility(View.VISIBLE);
            for (int i = 0; i < binding.getStaffpost().getUserTagg().size(); i++) {

                //name
                tvName = new TextView(this);
                tvName.setBackground(getResources().getDrawable(R.drawable.rounded_chip));
                tvName.setPadding(10, 5, 10, 5);
                tvName.setTextColor(getResources().getColor(R.color.deselectTab));
                tvName.setText(binding.getStaffpost().getUserTagg().get(i).getUserFirstname() + " " + binding.getStaffpost().getUserTagg().get(i).getUserLastname());

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(5, 5, 5, 5);
                tvName.setLayoutParams(params);

                Log.d("TTT", "data: " + binding.getStaffpost().getUserTagg().get(i).getUserFirstname() + " " + binding.getStaffpost().getUserTagg().get(i).getUserId());
                tvName.setTag(binding.getStaffpost().getUserTagg().get(i));
                //lvMain.addView(tvName);
                container.addView(tvName);
                tvName.setOnClickListener(lvClick);
            }
        } else {
            binding.imgTagUser.setVisibility(View.GONE);
        }
        if (container.getParent() != null) {
            ((ViewGroup) container.getParent()).removeView(container);
        }

        lvBubble.addView(container);

        if (lvBubble.getParent() != null) {
            ((ViewGroup) lvBubble.getParent()).removeView(lvBubble);
        }
        llayout.addView(lvBubble);
    }

    public void displayUserInNinePatch() {
        if (llayout.getVisibility() == View.VISIBLE) {
            llayout.setVisibility(View.GONE);
        } else {
            llayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentData && resultCode == RESULT_OK) {
            Log.d("TTT", "OnActivity");
            if (data.getExtras() != null) {
                flag = "mediachange";
                String type = "";
                if (data.getStringExtra("Type") != null) {
                    type = data.getStringExtra("Type");
                    Log.d("TTT", "OnActivity type: " + type);

                    if (type.equalsIgnoreCase("Image")) {
                        CropType = data.getStringExtra("CropType");
                        getUri = data.getStringExtra("AddPostImage");

                        try {
                            thumbImage = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(getUri));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        binding.imgUserPost.setImageBitmap(thumbImage);

                    } else if (type.equalsIgnoreCase("Video")) {
                        getUri = data.getStringExtra("selectUri");

                        binding.imgUserPost.setVisibility(View.GONE);
                        binding.videoUserPost.setVisibility(View.VISIBLE);

//                        binding.videoUserPost.reset();
//                        binding.videoUserPost.setSource(Uri.parse(getUri));

                        if (binding.videoUserPost.getCoverView() != null) {
                            Glide.with(ManageStaffPostActivity.this)
                                    .load(R.mipmap.home_post_img_placeholder)
                                    .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                    .placeholder(R.mipmap.home_post_img_placeholder)
                                    .error(R.mipmap.home_post_img_placeholder)
                                    .into(binding.videoUserPost.getCoverView());
                        }

                        binding.videoUserPost.getVideoInfo().setPortraitWhenFullScreen(false);
                        binding.videoUserPost.setVideoPath(getUri);
                        binding.videoUserPost.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player
                        binding.videoUserPost.getPlayer().start();

                    }
                }
            }
        } else if (requestCode == EditIntent && resultCode == RESULT_OK) {
            if (data.getExtras() != null) {

                option = "edit";
                String desc = data.getStringExtra("Desc");
                ArrayList<UserTagg> userTaggs = (ArrayList<UserTagg>) data.getSerializableExtra("userTaggs");

                Log.d("TTT", "userTaggg: " + userTaggs.size());
                String url = data.getStringExtra("url");
                String postId = data.getStringExtra("postId");
                String fileType = data.getStringExtra("fileType");

                Log.d("TTT", "ManagePost: " + url);
                binding.getStaffpost().setPostDescription(desc);
                binding.getStaffpost().setPostFile(url);
                binding.getStaffpost().setUserTagg(userTaggs);

                if (fileType.equalsIgnoreCase("1")) {
//                    Glide.with(getApplicationContext())
//                            .load(url)
//                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                            .placeholder(R.mipmap.home_post_img_placeholder)
//                            .error(R.mipmap.home_post_img_placeholder)
//                            .into(binding.imgUserPost);


                    Glide.with(getApplicationContext())
                            .load(url)
                            .asBitmap()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    binding.imgUserPost.setImageBitmap(resource);
                                }
                            });


                } else {
//                    binding.videoUserPost.reset();
//                    binding.videoUserPost.setSource(Uri.parse(url));

                    if (binding.videoUserPost.getCoverView() != null) {
                        Glide.with(ManageStaffPostActivity.this)
                                .load(R.mipmap.home_post_img_placeholder)
                                .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(binding.videoUserPost.getCoverView());
                    }

                    binding.videoUserPost.getVideoInfo().setPortraitWhenFullScreen(false);
                    binding.videoUserPost.setVideoPath(url);
                    binding.videoUserPost.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player
                    binding.videoUserPost.getPlayer().start();


                }

                Log.d("TTT", "OnActivity Size: " + binding.getStaffpost().getUserTagg().size());


                Log.d("TTT", "tag user size bind: " + binding.getStaffpost().getUserTagg().size());
                if (binding.getStaffpost().getUserTagg().size() > 0) {

                    if (llayout.getParent() != null) {
                        llayout.removeAllViews();
                        container.removeAllViews();
                    }

                    if (container.getParent() != null) {
                        Log.d("TTT", "Remove view: ");
                        ((ViewGroup) container.getParent()).removeView(container);
                    }

                    binding.imgTagUser.setVisibility(View.VISIBLE);
                    for (int i = 0; i < binding.getStaffpost().getUserTagg().size(); i++) {

                        //id
                        tvId = new TextView(this);
                        tvId.setVisibility(View.GONE);
                        tvId.setText(binding.getStaffpost().getUserTagg().get(i).getUserId());

                        //name
                        tvName = new TextView(this);
                        tvName.setBackground(getResources().getDrawable(R.drawable.rounded_chip));
                        tvName.setPadding(10, 5, 10, 5);
                        tvName.setTextColor(getResources().getColor(R.color.deselectTab));
                        tvName.setText(binding.getStaffpost().getUserTagg().get(i).getUserFirstname() + " " + binding.getStaffpost().getUserTagg().get(i).getUserLastname());

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        params.setMargins(5, 5, 5, 5);
                        tvName.setLayoutParams(params);

                        Log.d("TTT", "data: " + binding.getStaffpost().getUserTagg().get(i).getUserFirstname() + " " + binding.getStaffpost().getUserTagg().get(i).getUserId());
                        tvName.setTag(tvId.getText().toString());
                        container.addView(tvName);
                        tvName.setOnClickListener(lvClick);
                    }

                    lvBubble.addView(container);

                    if (lvBubble.getParent() != null) {
                        ((ViewGroup) lvBubble.getParent()).removeView(lvBubble);
                    }

                    llayout.addView(lvBubble);

                } else {
                    binding.imgTagUser.setVisibility(View.GONE);
                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        Log.d("TTT", "Option: " + option);
        if (option.equalsIgnoreCase("edit")) {
            Intent i = getIntent();
            i.putExtra("Option", "Edit");
            setResult(RESULT_OK, i);
            finish();
        }
        if (PlayerManager.getInstance().onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();

            return true;
        } else if (item.getItemId() == R.id.option) {
            View menuItemView = findViewById(R.id.option);
            showDialog(menuItemView);
        }
        return super.onOptionsItemSelected(item);
    }

    public void showDialog(View v) {
        final PopupMenu popup = new PopupMenu(ManageStaffPostActivity.this, v);
        popup.getMenuInflater().inflate(R.menu.same_student_opt, popup.getMenu());

        popup.getMenu().findItem(R.id.edit).setVisible(true);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Edit")) {
                    //set bundle val
                    Bundle bundle = new Bundle();

                    bundle.putString("postId", binding.getStaffpost().getPostId());
                    bundle.putString("fileType", FileType + "");
                    bundle.putSerializable("tagUserId", (ArrayList<UserTagg>) binding.getStaffpost().getUserTagg());
                    bundle.putString("desc", binding.getStaffpost().getPostDescription());
                    bundle.putString("imagevideofile", getUri);
                    bundle.putString("imgThumbnail", binding.getStaffpost().getPostThumbnail());
                    bundle.putInt("filesizetype", Integer.parseInt(CropType));

                    bundle.putString("post_type",binding.getStaffpost().getPostType());
                    bundle.putString("post_user_id",binding.getStaffpost().getPostUserId());
                    // bundle.putString("flag", flag);

                    Intent i = new Intent(ManageStaffPostActivity.this, EditYearBookPostActivity.class);
                    i.putExtras(bundle);
                    startActivityForResult(i, EditIntent);

                } else if (item.getTitle().equals("Delete")) {
                    androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(ManageStaffPostActivity.this);
                    LayoutInflater inflater = getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
                    dialogBuilder.setView(dialogView);
                    final androidx.appcompat.app.AlertDialog alert = dialogBuilder.create();
                    alert.setCancelable(false);
                    alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                    TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
                    tvMessage.setText("Are you sure you want to remove the post?");

                    TextView tvYes = dialogView.findViewById(R.id.tvYes);
                    TextView tvNo = dialogView.findViewById(R.id.tvNo);

                    tvYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                            progressDialog = ProgressDialog.show(ManageStaffPostActivity.this, "", "", true);
                            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            progressDialog.setContentView(R.layout.progress_view);
                            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                            Circle bounce = new Circle();
                            bounce.setColor(Color.BLACK);
                            progressBar.setIndeterminateDrawable(bounce);

                            RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                            final Call<GiveReport> deletePost = retrofitClass.deleteYearbookPost(Integer.parseInt(postList.getPostId()));
                            deletePost.enqueue(new Callback<GiveReport>() {
                                @Override
                                public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }

                                    if (response.body().getResponseCode().equals("1")) {
                                        Toast.makeText(ManageStaffPostActivity.this, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                        Intent i = getIntent();
                                        i.putExtra("Option", "Delete");
                                        setResult(RESULT_OK, i);
                                        finish();
                                    } else if (response.body().getResponseCode().equals("10")) {
                                        Toast.makeText(ManageStaffPostActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                        LoginUser loginSP = new LoginUser(ManageStaffPostActivity.this);
                                        loginSP.clearData();

                                        Intent i = new Intent(ManageStaffPostActivity.this, LoginActivity.class);
                                        startActivity(i);
                                        finish();
                                    } else {
                                        Toast.makeText(ManageStaffPostActivity.this, response.body().getResponseMsg() + "", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<GiveReport> call, Throwable t) {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                    Toast.makeText(ManageStaffPostActivity.this, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

                    tvNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });
                    alert.show();
                }
                return true;
            }
        });
        popup.show();
    }
}

package com.app.yearbook.model;


import android.os.Parcel;
import android.os.Parcelable;

public class SignFontModel implements Parcelable {
    int resourceId;
    String fontTitle;
    String id;

    protected SignFontModel(Parcel in) {
        resourceId = in.readInt();
        fontTitle = in.readString();
        id = in.readString();
    }

    public static final Creator<SignFontModel> CREATOR = new Creator<SignFontModel>() {
        @Override
        public SignFontModel createFromParcel(Parcel in) {
            return new SignFontModel(in);
        }

        @Override
        public SignFontModel[] newArray(int size) {
            return new SignFontModel[size];
        }
    };

    public String getFontTitle() {
        return fontTitle;
    }

    public void setFontTitle(String fontTitle) {
        this.fontTitle = fontTitle;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SignFontModel(String fontTitle) {
        this.fontTitle = fontTitle;
    }

    public SignFontModel(int resourceId, String fontTitle, String id) {
        this.resourceId = resourceId;
        this.fontTitle = fontTitle;
        this.id = id;
    }


    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(resourceId);
        dest.writeString(fontTitle);
        dest.writeString(id);
    }
}

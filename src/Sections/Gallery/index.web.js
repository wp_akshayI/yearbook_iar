import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native'
import { global } from '../../utils';
import { image_placeholder } from '../../assets';
import ImageScal from '../../utils/ImageScal';
import ImageLightBox from '../../components/ImageLightBox';

export default function Gallery ({ data }) {
  const [state, setState] = useState({
    isLightbox: false,
    lightbox_uri: ''

  })

    const renderItem = ({ item, index }) => {
    const column = Number(data.grid_row)
        const uri = item || image_placeholder
        return (
      <TouchableOpacity
        style={[styles.list]}
        onPress={() => setState({ ...state, isLightbox: true, lightbox_uri: uri })}
      >
        <Image
          resizeMode='cover'
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: global.width / column - 4,
            height: global.width / column - 4
          }}
          source={{ uri }}
                />
      </TouchableOpacity>
    )
  }

  const renderSingleItem = ({ item }) => {
    const uri = item || image_placeholder
        return (
      <TouchableOpacity
        style={styles.list}
        onPress={() => setState({ ...state, isLightbox: true, lightbox_uri: uri })}
        activeOpacity={1}
            >
        <View style={styles.listHeader}>
          <Text
            numberOfLines={2}
            ellipsizeMode='tail'
            style={styles.listTitle}
          >
            {data.component_title}
          </Text>
        </View>
        <ImageWithFullHeight
          uri={uri}
        />
        <View style={styles.listHeader}>
          <Text
            numberOfLines={2}
            ellipsizeMode='tail'
            style={styles.listTitle}
          >
            {item.firstname} {item.lastname}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }

  return (
    <View style={styles.container}>
      {
        Number(data.grid_row) === 1 && (
          <FlatList
            data={data.images || []}
            keyExtractor={(item, index) => String(index)}
            renderItem={renderSingleItem}
            key={String(data.grid_row)}
          />
        )
      }
      {
        Number(data.grid_row) === 2 && (
          <FlatList
            data={data.images || []}
            numColumns={2}
            keyExtractor={(item, index) => String(index)}
            renderItem={renderItem}
            key={String(data.grid_row)}
          />
        )
      }
      {
        Number(data.grid_row) === 3 && (
          <FlatList
            data={data.images || []}
            numColumns={3}
            keyExtractor={(item, index) => String(index)}
            renderItem={renderItem}
            key={String(data.grid_row)}
          />
        )
      }
      <ImageLightBox
        isVisible={state.isLightbox}
        handleClose={() => setState({ ...state, isLightbox: false })}
        uri={state.lightbox_uri}
      />
    </View>
  )
}

function ImageWithFullHeight ({ uri, ...props }) {
  const [state, setState] = useState({
    isLoading: true,
    height: global.height * 0.4
  })

    useEffect(() => {
    _getScal()
    }, [uri])

    const _getScal = async () => {
    if (!uri) return
        const { height } = await ImageScal(uri)
    if (height)
      {setState({ ...state, height })}
  }

  return (
    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
      <Image
        resizeMode='contain'
        style={{
          width: '100%',
          height: state.height
        }}
        source={{ uri }}
        onLoad={() => setState({ ...state, isLoading: false })}
        {...props}
      />
      {
        state.isLoading && <ActivityIndicator style={{ position: 'absolute' }} />
      }
    </View>
  )
}

const styles = StyleSheet.create({
  columnTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: 'Calibri',
    padding: 10,
    color: '#000'
  },
  container: {
    backgroundColor: '#fff'
  },
  list: {
    margin: 2
  },
  listHeader: {
    backgroundColor: '#fff',
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center'
  },
  listTitle: {
    color: '#000',
    fontSize: 14
  }
})

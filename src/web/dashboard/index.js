import React from 'react'
import { Route, Switch } from 'react-router-dom'
import NavigationDrawer from '../../components/NavigationDrawer'
import Home from './Home'
import Subcategories from './Subcategories'

export default function Router () {
  return (
    <Switch>
      <NavigationDrawer>
        <Route exact path='/app/dashboard' component={Home} />
        <Route exact path='/app/dashboard/sub-categories' component={Subcategories} />
      </NavigationDrawer>

    </Switch>
  )
}

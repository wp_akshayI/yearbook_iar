package com.app.yearbook.model.staff.livefeed;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.app.yearbook.model.staff.yearbook.UserTagg;

public class PostListItem implements Serializable {

	@SerializedName("staff_image")
	private String staffImage;

	@SerializedName("post_title")
	private String postTitle;

	@SerializedName("total_repoted")
	private String totalRepoted;

	@SerializedName("staff_lastname")
	private String staffLastname;

	@SerializedName("post_file")
	private String postFile;

	@SerializedName("total_comment")
	private String totalComment;

	@SerializedName("staff_firstname")
	private String staffFirstname;

	@SerializedName("post_description")
	private String postDescription;

	@SerializedName("post_file_type")
	private String postFileType;

	@SerializedName("post_id")
	private String postId;

	@SerializedName("school_id")
	private String schoolId;

	@SerializedName("post_date")
	private String postDate;

	@SerializedName("staff_id")
	private String staffId;

	@SerializedName("post_thumbnail")
	private String postThumbnail;

	@SerializedName("post_type")
	private String postType;

	@SerializedName("user_tagg")
	private List<UserTagg> userTagg;

	public void setStaffImage(String staffImage){
		this.staffImage = staffImage;
	}

	public String getStaffImage(){
		return staffImage;
	}

	public void setPostTitle(String postTitle){
		this.postTitle = postTitle;
	}

	public String getPostTitle(){
		return postTitle;
	}

	public void setTotalRepoted(String totalRepoted){
		this.totalRepoted = totalRepoted;
	}

	public String getTotalRepoted(){
		return totalRepoted;
	}

	public void setStaffLastname(String staffLastname){
		this.staffLastname = staffLastname;
	}

	public String getStaffLastname(){
		return staffFirstname +" "+staffLastname;
	}

	public void setPostFile(String postFile){
		this.postFile = postFile;
	}

	public String getPostFile(){
		return postFile;
	}

	public void setTotalComment(String totalComment){
		this.totalComment = totalComment;
	}

	public String getTotalComment(){
		return totalComment;
	}

	public void setStaffFirstname(String staffFirstname){
		this.staffFirstname = staffFirstname;
	}

	public String getStaffFirstname(){
		return staffFirstname;
	}

	public void setPostDescription(String postDescription){
		this.postDescription = postDescription;
	}

	public String getPostDescription(){
		return postDescription;
	}

	public void setPostFileType(String postFileType){
		this.postFileType = postFileType;
	}

	public String getPostFileType(){
		return postFileType;
	}

	public void setPostId(String postId){
		this.postId = postId;
	}

	public String getPostId(){
		return postId;
	}

	public void setSchoolId(String schoolId){
		this.schoolId = schoolId;
	}

	public String getSchoolId(){
		return schoolId;
	}

	public void setPostDate(String postDate){
		this.postDate = postDate;
	}

	public String getPostDate(){
		return postDate;
	}

	public void setStaffId(String staffId){
		this.staffId = staffId;
	}

	public String getStaffId(){
		return staffId;
	}

	public void setPostThumbnail(String postThumbnail){
		this.postThumbnail = postThumbnail;
	}

	public String getPostThumbnail(){
		return postThumbnail;
	}

	public void setPostType(String postType){
		this.postType = postType;
	}

	public String getPostType(){
		return postType;
	}

	public void setUserTagg(List<UserTagg> userTagg){
		this.userTagg = userTagg;
	}

	public List<UserTagg> getUserTagg(){
		return userTagg;
	}
}
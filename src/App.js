import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import axios from 'axios'
import Category from './App/Category'
import YearbookDetails from './App/YearbookDetails'
import Yearbooks from './App/Yearbooks'
import Splashscreen from './App/Splashscreen'
import { Provider } from 'react-redux'
import configureStore from './store'
import { global } from './utils'
const store = configureStore()

axios.defaults.baseURL = global.api
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

const AppNavigator = createStackNavigator({
  Splashscreen,
  Yearbooks,
  YearbookDetails,
  Category
}, {
  initialRouteName: 'Splashscreen',
  headerLayoutPreset: 'center',
  defaultNavigationOptions: {
    headerTintColor: '#000',
    headerBackTitle: null,
    headerTitleStyle: {
      fontWeight: 'bold'
    }
  }
})

const App = createAppContainer(AppNavigator)

export default function Approuter (props) {
  return (
    <Provider store={store}>
      <App
        screenProps={{ token: props.Token, screen_id: props.screen_id, yearbook_name: props.yearbook_name }}
      />
    </Provider>
  )
}

package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.model.StaffStudent;
import com.app.yearbook.model.staff.StaffSignUP;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffSignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private TextView tvRequestSchool;
    private LinearLayout lvAccessCode, lvEmail, lvPassword, lvDone;
    private EditText etAccessCode, etEmail, etPassword;
    private Button btnPassword, btnOk, btnEmail, btnAccessCode;
    private AllMethods allMethods;
    private ProgressDialog progressDialog;
    private LoginUser loginUserSP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_sign_up);

        setView();
    }

    public void setView() {
        //toolbar
        setToolbar();

        //SP
        loginUserSP=new LoginUser(StaffSignUpActivity.this);

        //all methods object
        allMethods = new AllMethods();

        //tv request
        tvRequestSchool = findViewById(R.id.tvRequestSchool);
        tvRequestSchool.setOnClickListener(this);

        //lv
        lvAccessCode = findViewById(R.id.lvAccessCode);
        lvEmail = findViewById(R.id.lvEmail);
        lvPassword = findViewById(R.id.lvPassword);
        lvDone = findViewById(R.id.lvDone);

        //edit text
        etAccessCode = findViewById(R.id.etAccessCode);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);

        //btn
        btnAccessCode = findViewById(R.id.btnAccessCode);
        btnAccessCode.setOnClickListener(this);
        btnOk = findViewById(R.id.btnOk);
        btnOk.setOnClickListener(this);
        btnEmail = findViewById(R.id.btnEmail);
        btnEmail.setOnClickListener(this);
        btnPassword = findViewById(R.id.btnPassword);
        btnPassword.setOnClickListener(this);
    }


    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Access code login");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(6f);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Log.d("TTT", "OnBack");
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(lvEmail.getVisibility()==View.VISIBLE)
        {
            setVisibility(lvEmail,lvAccessCode);
        }
        else if(lvPassword.getVisibility()==View.VISIBLE)
        {
            setVisibility(lvPassword,lvEmail);
        }
         else if(lvDone.getVisibility()==View.VISIBLE)
        {
            Intent i=new Intent(StaffSignUpActivity.this,StaffHomeActivity.class);
            startActivity(i);
            finish();
        }
        else if(lvAccessCode.getVisibility()==View.VISIBLE) {

            finish();
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvRequestSchool) {
            Intent i = new Intent(StaffSignUpActivity.this, StaffRequestActivity.class);
            startActivity(i);
        } else if (v.getId() == R.id.btnAccessCode) {
            if (etAccessCode.getText().toString().trim().equals("")) {
                allMethods.setAlert(StaffSignUpActivity.this, getResources().getString(R.string.app_name), "Please enter access code");
            } else {
                setVisibility(lvAccessCode, lvEmail);
            }
        } else if (v.getId() == R.id.btnEmail) {
            if (etEmail.getText().toString().trim().equals("")) {
                allMethods.setAlert(StaffSignUpActivity.this, getResources().getString(R.string.app_name), "Please enter email");
            } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                allMethods.setAlert(StaffSignUpActivity.this, getResources().getString(R.string.app_name), "Please enter valid email address");
            } else {
                setVisibility(lvEmail, lvPassword);
            }
        } else if (v.getId() == R.id.btnPassword) {
            if (etPassword.getText().toString().trim().equals("")) {
                allMethods.setAlert(StaffSignUpActivity.this, getResources().getString(R.string.app_name), "Please enter password");
            } else {
                SignUp();
            }
        }  else if (v.getId() == R.id.btnOk) {
            Intent i=new Intent(StaffSignUpActivity.this,StaffHomeActivity.class);
            startActivity(i);
            finish();
        }
    }

    public void SignUp() {
        //Progress bar
        progressDialog = ProgressDialog.show(StaffSignUpActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        String token = "";
        token = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("Token", "");
        Log.d("TTT", "token: " + token);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<StaffSignUP> staffSignup = retrofitClass.staffSignUp(etAccessCode.getText().toString(), etEmail.getText().toString(), etPassword.getText().toString(), "android", token);
        staffSignup.enqueue(new Callback<StaffSignUP>() {
            @Override
            public void onResponse(Call<StaffSignUP> call, Response<StaffSignUP> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (response.body().getResponseCode().equals("1")) {
                    toolbar.setVisibility(View.GONE);
                    setVisibility(lvPassword, lvDone);

                    StaffStudent staffUSer=new StaffStudent();
                    staffUSer.setStaffId(response.body().getData().getStaffId());
                    staffUSer.setSchoolId(response.body().getData().getSchoolId());
                    staffUSer.setSchoolName(response.body().getData().getSchoolName());
                    staffUSer.setStaffPhone(response.body().getData().getStaffPhone());
                    staffUSer.setStaffEmail(response.body().getData().getStaffEmail());
                    staffUSer.setStaffPassword(response.body().getData().getStaffPassword());
                    staffUSer.setStaffDeviceType(response.body().getData().getStaffDeviceType());
                    staffUSer.setStaffDeviceToken(response.body().getData().getStaffDeviceToken());
                    staffUSer.setStaffAccessCode(response.body().getData().getStaffAccessCode());
                    staffUSer.setUserType(response.body().getData().getUserType());

                    loginUserSP.setUserData(staffUSer);
                    Log.d("TTT","SP: "+loginUserSP.getUserData().getUserType());

                } else {
                    allMethods.setAlert(StaffSignUpActivity.this, "", response.body().getResponseMsg() + "");
                }
            }

            @Override
            public void onFailure(Call<StaffSignUP> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StaffSignUpActivity.this, "", t.getMessage() + "");
            }
        });

    }

    public void setVisibility(LinearLayout hideView, LinearLayout showView) {
        hideView.setVisibility(View.GONE);
        showView.setVisibility(View.VISIBLE);
    }

}

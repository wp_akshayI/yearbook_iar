package com.app.yearbook;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.app.yearbook.adapter.AdapterAddOptions;
import com.app.yearbook.databinding.ActivityNewPollPostBinding;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.GradeListItem;
import com.app.yearbook.model.GradesResponse;
import com.app.yearbook.model.allpost.OptionsModel;
import com.app.yearbook.model.allpost.PollPost;
import com.app.yearbook.model.postmodels.PostListItem;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewPollActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "NewPollActivity";
    public static final int MAX_OPTIONS = 5;
    ActivityNewPollPostBinding binding;
    List<OptionsModel> optionsModels;
    AdapterAddOptions adapterAddOptions;
    PollPost model;
    List<GradeListItem> gradeListItems;
    PostListItem postListItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_poll_post);
        binding.txtAddOption.setOnClickListener(this);
        optionsModels = new ArrayList<>();
        optionsModels.add(new OptionsModel(""));
       // optionsModels.add(new OptionsModel(""));
        adapterAddOptions = new AdapterAddOptions(optionsModels, new OnRecyclerClick() {

            @Override
            public void onVote(int pos, String pollOptionId) {

            }

            @Override
            public void onClick(int pos, int type) {
                if (type == 0) {
                    optionsModels.remove(pos);
                    adapterAddOptions.notifyItemRemoved(pos);
                    adapterAddOptions.notifyDataSetChanged();
                }
            }
        });
        binding.rcOptions.setAdapter(adapterAddOptions);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GradesResponse> addTextPost = retrofitClass.getSchoolGrade();
        addTextPost.enqueue(new Callback<GradesResponse>() {
            @Override
            public void onResponse(Call<GradesResponse> call, Response<GradesResponse> response) {
                Log.d(TAG, "onResponse: ");
                GradesResponse gradesResponse = response.body();
                if (gradesResponse != null) {
                    if (gradesResponse.getResponseCode().equals("1")) {
                        gradeListItems = new ArrayList<>();
                        gradeListItems.addAll(gradesResponse.getGradeList());
                    }
                }
            }

            @Override
            public void onFailure(Call<GradesResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });

        setToolbar();
        if (getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false)){
            postListItem = getIntent().getParcelableExtra(Constants.INTENT_DATA);
            binding.appbar.toolbarTitle.setText(getString(R.string.edit_poll));
            optionsModels.clear();
            for (int i = 0; i <postListItem.getPollOptions().size() ; i++) {
                optionsModels.add(new OptionsModel(postListItem.getPollOptions().get(i).getOptionDescription(), true));
            }
            model = new PollPost(postListItem.getPostTitle(), postListItem.getPostSubTitle(), optionsModels);
            binding.setModel(model);
            adapterAddOptions.notifyDataSetChanged();
        }else{
            model = new PollPost("", "", optionsModels);
            binding.setModel(model);
        }

    }

    private void setToolbar() {
        setSupportActionBar(binding.appbar.toolbar);
        binding.appbar.toolbarTitle.setText(getString(R.string.new_poll));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }
    }

    MenuItem action_next;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_next, menu);
        action_next = menu.findItem(R.id.action_next);
        if (getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false)) {
            action_next.setVisible(true);
        }else{
            action_next.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_next:
                validate();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void validate() {

        Log.d(TAG, "validate: " + optionsModels.size());
        if (TextUtils.isEmpty(model.getSubject())) {
            binding.edtSubject.setError("Please enter subject");
        } else if (TextUtils.isEmpty(model.getDescription())) {
            binding.edtDescription.setError("Please enter description");
        } else {
            for (OptionsModel optionsModel : optionsModels) {
                if (TextUtils.isEmpty(optionsModel.getOption())) {
                    Toast.makeText(this, "Empty options not allowed", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            if (gradeListItems !=null && gradeListItems.size() > 0) {
                model.setOptionsModels(optionsModels);
                Intent intent = new Intent(this, NewPollActivity2.class);
                intent.putExtra(Constants.INTENT_ISEDIT, getIntent().getBooleanExtra(Constants.INTENT_ISEDIT, false));
                intent.putExtra(Constants.INTENT_DATA, postListItem);
                intent.putExtra(Constants.POST_POll, model);
                intent.putParcelableArrayListExtra(Constants.GRADE_ITEM, (ArrayList<? extends Parcelable>) gradeListItems);
                startActivityForResult(intent, 111);
            }else{
                Toast.makeText(this, "please, come again", Toast.LENGTH_SHORT).show();
            }
        }

    }


    @Override
    public void onClick(View v) {
        if (v == binding.txtAddOption) {
            if (optionsModels.size() < MAX_OPTIONS) {
                optionsModels.add(new OptionsModel(""));
                adapterAddOptions.notifyItemInserted(optionsModels.size());
//                adapterAddOptions.notifyDataSetChanged();
                /*binding.scrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        binding.scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });*/

                if(optionsModels.size()>1){
                    action_next.setVisible(true);
                }
            } else {
                Toast.makeText(this, "Max limit is " + MAX_OPTIONS, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111 && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (requestCode == 111 && resultCode == RESULT_CANCELED) {
            model = data.getExtras().getParcelable(Constants.POST_POll);
            binding.setModel(model);
        }

    }
}

package com.app.yearbook;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.BitmapUtils;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdvertismentFinalPostActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private AllMethods allMethods;
    private Uri getUri;
    private ImageView imgPostPhoto;
    private Bitmap thumbImage;
    private MultipartBody.Part part;
    private File fileImage;
    private RequestBody rbFile,rbSchoolID,rbFileType;
    private LoginUser loginUser;
    private String schoolId;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisment_final_post);

        setView();
    }

    public void setView() {

        //toolbar
        setToolbar();

        //SP object
        loginUser = new LoginUser(AdvertismentFinalPostActivity.this);
        if (loginUser.getUserData() != null) {
            schoolId=loginUser.getUserData().getSchoolId();
            rbSchoolID=RequestBody.create(MediaType.parse("text/plain"), schoolId);
        }

        rbFileType=RequestBody.create(MediaType.parse("text/plain"),"1");
        //common methods
        allMethods = new AllMethods();

        imgPostPhoto=findViewById(R.id.imgPost);

        if (getIntent().getExtras() != null) {
            getUri = Uri.parse(getIntent().getStringExtra("AddPostImage"));

            try {
                thumbImage = MediaStore.Images.Media.getBitmap(getContentResolver(), getUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //imgPostPhoto.setImageBitmap(Bitmap.createScaledBitmap(thumbImage, 640, 640, false));

            imgPostPhoto.setImageURI(getUri);

            fileImage = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), getUri));
            RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
            part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);
            rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.next) {
          AddAdvertisement();
        }

        return super.onOptionsItemSelected(item);
    }

    //add advertisment
    public void AddAdvertisement()
    {
        //Progress bar
        progressDialog = ProgressDialog.show(AdvertismentFinalPostActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<GiveReport> addAdvertise = retrofitClass.addAdvertise(rbSchoolID,rbFileType,part,rbFile);
        addAdvertise.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if(response.body().getResponseCode().equalsIgnoreCase("1"))
                {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(AdvertismentFinalPostActivity.this);
                    dialog.setCancelable(false);
                    dialog.setTitle("");
                    dialog.setCancelable(false);
                    dialog.setMessage(response.body().getResponseMsg());
                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //Action for "Delete".
                            dialog.dismiss();
                            Intent i = getIntent();
                            setResult(RESULT_OK, i);
                            finish();
                        }
                    });

                    final AlertDialog alert = dialog.create();
                    alert.show();
                }
                else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(AdvertismentFinalPostActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(AdvertismentFinalPostActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(AdvertismentFinalPostActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    allMethods.setAlert(AdvertismentFinalPostActivity.this, "", response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(AdvertismentFinalPostActivity.this, getResources().getString(R.string.app_name), t.getMessage());
            }
        });
    }

    //set toolbar
    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Add Advertisement");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selectimage, menu);
        MenuItem next = menu.findItem(R.id.next);
        next.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }

}

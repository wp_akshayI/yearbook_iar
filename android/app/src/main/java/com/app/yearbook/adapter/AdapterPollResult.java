package com.app.yearbook.adapter;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.yearbook.R;
import com.app.yearbook.databinding.AdapterPollResultBinding;
import com.app.yearbook.model.postmodels.PollOption;

import java.util.List;

public class AdapterPollResult extends RecyclerView.Adapter<AdapterPollResult.PollResultHolder> {
    List<PollOption> pollOptions;

    public AdapterPollResult(List<PollOption> pollOptions) {
        this.pollOptions = pollOptions;
    }

    @NonNull
    @Override
    public PollResultHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterPollResultBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_poll_result, parent, false);
        return new PollResultHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull PollResultHolder holder, int position) {
        holder.binding.setModel(pollOptions.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return pollOptions.size();
    }

    public class PollResultHolder extends RecyclerView.ViewHolder {

        AdapterPollResultBinding binding;

        public PollResultHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}

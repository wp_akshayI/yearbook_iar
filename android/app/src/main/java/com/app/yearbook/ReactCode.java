package com.app.yearbook;

import android.app.Activity;
import android.os.Bundle;

import com.azendoo.reactnativesnackbar.SnackbarPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.common.LifecycleState;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.reactnativecommunity.slider.ReactSliderPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;

//import com.facebook.react.PackageList;

public class ReactCode extends Activity implements DefaultHardwareBackBtnHandler {

    public static ReactCode instance;
    private ReactRootView mReactRootView;
    private ReactInstanceManager mReactInstanceManager;

//    @Override
//    protected ReactActivityDelegate createReactActivityDelegate() {
//        return new ReactActivityDelegate(this, getMainComponentName()) {
//            @Override
//            protected Bundle getLaunchOptions() {
//
//                Bundle initialProperties = new Bundle();
//                initialProperties.putString("var_1","Im the first var");
//                initialProperties.putString("var_2","paramSecond");
//                return initialProperties;
//            }
//        };
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SoLoader.init(this, false);

        instance = this;

        mReactRootView = new ReactRootView(this);
     /*   List<ReactPackage> packages = new PackageList(getApplication()).getPackages();
        // Packages that cannot be autolinked yet can be added manually here, for example:
         packages.add(new MainReactPackage());
        packages.add(new ReactModules());
        packages.add(new RNGestureHandlerPackage());
        packages.add(new ReactVideoPackage());
        packages.add(new ReactSliderPackage());
        packages.add(new SnackbarPackage());*/

        // Remember to include them in `settings.gradle` and `app/build.gradle` too.

        mReactInstanceManager = ReactInstanceManager.builder()
                .setApplication(getApplication())
                .setCurrentActivity(this)
                .setBundleAssetName("index.android.bundle")
                .setJSMainModulePath("index")
//                .addPackages(packages)
                .addPackage(new MainReactPackage())
                .addPackage(new ReactModules())
                .addPackage(new RNGestureHandlerPackage())
                .addPackage(new ReactVideoPackage())
                .addPackage(new ReactSliderPackage())
                .addPackage(new SnackbarPackage())
                .setUseDeveloperSupport(BuildConfig.DEBUG)
                .setInitialLifecycleState(LifecycleState.RESUMED)
                .build();
        // The string here (e.g. "MyReactNativeApp") has to match
        // the string in AppRegistry.registerComponent() in index.js
        mReactRootView.startReactApplication(mReactInstanceManager, "yearbook", null);

        setContentView(mReactRootView);
    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        instance = null;
    }
}

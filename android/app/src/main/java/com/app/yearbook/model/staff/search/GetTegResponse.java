package com.app.yearbook.model.staff.search;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetTegResponse{

	@SerializedName("ResponseCode")
	private String responseCode;

	@SerializedName("ServerTimeZone")
	private String serverTimeZone;

	@SerializedName("tags_list")
	private List<TagsListItem> tagsList;

	@SerializedName("ResponseMsg")
	private String responseMsg;

	@SerializedName("serverTime")
	private String serverTime;

	@SerializedName("Result")
	private String result;

	public void setResponseCode(String responseCode){
		this.responseCode = responseCode;
	}

	public String getResponseCode(){
		return responseCode;
	}

	public void setServerTimeZone(String serverTimeZone){
		this.serverTimeZone = serverTimeZone;
	}

	public String getServerTimeZone(){
		return serverTimeZone;
	}

	public void setTagsList(List<TagsListItem> tagsList){
		this.tagsList = tagsList;
	}

	public List<TagsListItem> getTagsList(){
		return tagsList;
	}

	public void setResponseMsg(String responseMsg){
		this.responseMsg = responseMsg;
	}

	public String getResponseMsg(){
		return responseMsg;
	}

	public void setServerTime(String serverTime){
		this.serverTime = serverTime;
	}

	public String getServerTime(){
		return serverTime;
	}

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}
}
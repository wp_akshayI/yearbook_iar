
package com.app.yearbook.model.yearbook;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YearbookList {

    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("yearbook_id")
    @Expose
    private String yearbookId;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("yearbook_name")
    @Expose
    private String yearbookName;
    @SerializedName("yearbook_status")
    @Expose
    private String yearbookStatus;
    @SerializedName("yearbook_created")
    @Expose
    private String yearbookCreated;
    @SerializedName("yearbook_amount")
    @Expose
    private String yearbookAmount;
    @SerializedName("yearbook_cover")
    @Expose
    private String yearbookCover;

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getYearbookId() {
        return yearbookId;
    }

    public void setYearbookId(String yearbookId) {
        this.yearbookId = yearbookId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getYearbookName() {
        return yearbookName;
    }

    public void setYearbookName(String yearbookName) {
        this.yearbookName = yearbookName;
    }

    public String getYearbookStatus() {
        return yearbookStatus;
    }

    public void setYearbookStatus(String yearbookStatus) {
        this.yearbookStatus = yearbookStatus;
    }

    public String getYearbookCreated() {
        return yearbookCreated;
    }

    public void setYearbookCreated(String yearbookCreated) {
        this.yearbookCreated = yearbookCreated;
    }

    public String getYearbookAmount() {
        return yearbookAmount;
    }

    public void setYearbookAmount(String yearbookAmount) {
        this.yearbookAmount = yearbookAmount;
    }

    public String getYearbookCover() {
        return yearbookCover;
    }

    public void setYearbookCover(String yearbookCover) {
        this.yearbookCover = yearbookCover;
    }
}

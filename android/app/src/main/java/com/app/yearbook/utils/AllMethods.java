package com.app.yearbook.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.app.yearbook.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class AllMethods {
    private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR
            = new DecelerateInterpolator();
    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR
            = new AccelerateInterpolator();

    /**
     *
     * @param ctx context
     * @param title title
     * @param message message
     */
    public void setAlert(Context ctx,String title,String message)
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setCancelable(false);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
                dialog.dismiss();
            }
        });

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    /**
     *
     * @param uri generate bitmap from uri
     * @param ctx Context
     * @return
     */
    public static Bitmap GetBitmap(Uri uri,Context ctx)
    {
        Log.d("TTT","Method: "+uri);
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(ctx.getContentResolver(), uri);
            Log.d("TTT","Method bitmap: "+bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;

    }

    /**
     *
     * @param inContext  context
     * @param inImage bitmap of image
     * @return
     */
    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    /**
     *
     * @param ctx context
     * @param view view
     */
    public static void hideKeyboard(Context ctx,View view)
    {
        if(view != null) {
            InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     *
     * @param heartImageView display image when user double tap on view
     * @param likeDislikeImg animate other image
     */
    public static void heart(final ImageView heartImageView, final ImageView likeDislikeImg) {
        heartImageView.setVisibility(View.VISIBLE);
//        viewBG.setVisibility(View.VISIBLE);

//        viewBG.setScaleY(0.1f);
//        viewBG.setScaleX(0.1f);
//        viewBG.setAlpha(1f);

        heartImageView.setScaleY(0.1f);
        heartImageView.setScaleX(0.1f);

        AnimatorSet animatorSet = new AnimatorSet();

//        ObjectAnimator bgScaleYAnim = ObjectAnimator.ofFloat(viewBG, "scaleY", 0.1f, 1f);
//        bgScaleYAnim.setDuration(200);
//        bgScaleYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
//        ObjectAnimator bgScaleXAnim = ObjectAnimator.ofFloat(viewBG, "scaleX", 0.1f, 1f);
//        bgScaleXAnim.setDuration(200);
//        bgScaleXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
//        ObjectAnimator bgAlphaAnim = ObjectAnimator.ofFloat(viewBG, "alpha", 1f, 0f);
//        bgAlphaAnim.setDuration(200);
//        bgAlphaAnim.setStartDelay(150);
//        bgAlphaAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

        ObjectAnimator imgScaleUpYAnim = ObjectAnimator.ofFloat(heartImageView, "scaleY", 0.1f, 1f);
        imgScaleUpYAnim.setDuration(300);
        imgScaleUpYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
        ObjectAnimator imgScaleUpXAnim = ObjectAnimator.ofFloat(heartImageView, "scaleX", 0.1f, 1f);
        imgScaleUpXAnim.setDuration(300);
        imgScaleUpXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

        ObjectAnimator imgScaleDownYAnim = ObjectAnimator.ofFloat(heartImageView, "scaleY", 1f, 0f);
        imgScaleDownYAnim.setDuration(300);
        imgScaleDownYAnim.setInterpolator(ACCELERATE_INTERPOLATOR);
        ObjectAnimator imgScaleDownXAnim = ObjectAnimator.ofFloat(heartImageView, "scaleX", 1f, 0f);
        imgScaleDownXAnim.setDuration(300);
        imgScaleDownXAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

        animatorSet.playTogether(imgScaleUpYAnim, imgScaleUpXAnim);
        animatorSet.play(imgScaleDownYAnim).with(imgScaleDownXAnim).after(imgScaleUpYAnim);

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                //viewBG.setVisibility(View.GONE);
                heartImageView.setVisibility(View.GONE);
            }
        });
        animatorSet.start();

        YoYo.with(Techniques.RubberBand)
                .duration(500)
                .playOn(likeDislikeImg);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                likeDislikeImg.setImageResource(R.mipmap.diamond_selected_ic);
            }
        }, 200);

    }

    /**
     *
     * @param source bitmap
     * @param angle angle
     * @return
     */
    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
    public static int getDisplayWidth(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int orientation = activity.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            return metrics.heightPixels;
        }
        return metrics.widthPixels;
    }

    public static int getDisplayHeight(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int orientation = activity.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            return metrics.widthPixels;
        }
        return metrics.heightPixels;
    }

//    public static String getCapsSentences(String tagName) {
//
//        String[] splits = tagName.toLowerCase().split(" ");
//        Log.d("TTT", "getCapsSentences: "+splits.length);
//        StringBuilder sb = new StringBuilder();
//
//        for (int i = 0; i < splits.length; i++) {
//            if(splits[i]!=null && splits[i].length()>0) {
//                String eachWord = splits[i];
//                if (i > 0 && eachWord.length() > 0) {
//                    sb.append(" ");
//                }
//                String cap = eachWord.substring(0, 1).toUpperCase()
//                        + eachWord.substring(1);
//                sb.append(cap);
//            }
//        }
//        return sb.toString();
//    }

}

package com.app.yearbook.fragment;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.adapter.SearchUserAdapter;
import com.app.yearbook.model.search.User;
import com.app.yearbook.model.search.getPeople;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchPeopleFragment extends Fragment {

    public View view;
    private androidx.appcompat.widget.SearchView searchView;
    private ArrayList<User> getUser;
    public ProgressDialog progressDialog;
    private LoginUser loginUser;
    public AllMethods utils;
    public int schoolId;
    private RecyclerView rvSearchUser;
    private LinearLayout lvNoData;
    private SearchUserAdapter searchUserAdapter;
    private String type="",userId="";
    public SearchPeopleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search_people, container, false);
        setHasOptionsMenu(true);
        setView();
        return view;
    }

    public void setView() {
        type = getArguments().getString("type", "");
        Log.d("TTT","Type: "+type);

        //SP object
        loginUser = new LoginUser(getActivity());
        if (loginUser.getUserData() != null) {
            schoolId = Integer.parseInt(loginUser.getUserData().getSchoolId());
            userId=loginUser.getUserData().getUserId();
        }

        //utils
        utils = new AllMethods();

        //rv
        rvSearchUser = view.findViewById(R.id.rvSearchPeople);

        //lv
        lvNoData = view.findViewById(R.id.lvNoData);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.searchview, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (androidx.appcompat.widget.SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setQueryHint("Search by username");
        ImageView v = searchView.findViewById(R.id.search_button);
        v.setImageResource(R.drawable.ic_search);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!query.equals("")) {
                    searchView.clearFocus();
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    searchPeople(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void searchPeople(String user) {
        progressDialog = ProgressDialog.show(getActivity(), "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<getPeople> userPost = retrofitClass.searchPeople(user, "people",userId, String.valueOf(schoolId));
        userPost.enqueue(new Callback<getPeople>() {
            @Override
            public void onResponse(Call<getPeople> call, Response<getPeople> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    getUser = new ArrayList<>();
                    if (response.body().getUserList().size() > 0) {
                        rvSearchUser.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        getUser.addAll(response.body().getUserList());
                        searchUserAdapter = new SearchUserAdapter(getUser, getActivity(),type);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        rvSearchUser.setLayoutManager(mLayoutManager);
                        rvSearchUser.setAdapter(searchUserAdapter);
                    } else {
                        rvSearchUser.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10"))
                {
                    Toast.makeText(getActivity(),response.body().getResponseMsg(),Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(getActivity());
                    loginSP.clearData();

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    getActivity().startActivity(i);
                    getActivity().finish();
                }
                else {
                    rvSearchUser.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                    // utils.setAlert(getActivity(),"",response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<getPeople> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                utils.setAlert(getActivity(), "", t.getMessage());
            }
        });
    }
}

import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux"
import Actions from "../../actions";
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Fab from '@material-ui/core/Fab';
import banner_bg from "../../assets/images/bg.png"
import finding_ic from "../../assets/images/finding_ic.png"
import { global } from "../../utils";
import Paper from '@material-ui/core/Paper';
import { CircularProgress } from "@material-ui/core";

function Loading({ history }) {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { query } = useSelector(({ router }) => router.location);

    const [state, setState] = useState({
        isloading: true,
        message: ""
    });

    useEffect(() => {
        _check_auth_token();
        document.getElementById("pre-loader").style.display = "none";
    }, []);

    const _check_auth_token = async () => {
        try {
            let { sign, _id } = query;
            let { redirect } = history.location.state || [];
            if (!sign || !_id) {
                let local_auth = await dispatch(Actions.check_local_token());
                if (!local_auth) return;
                sign = local_auth.token;
                _id = local_auth.yearbook_id;
            }
            let status = await dispatch(Actions.check_auth_token(sign, _id));
            if (!status.Result) return setState({ ...state, isloading: false, message: status.ResponseMsg });
            history.push(redirect || "/");
        } catch (error) {
            console.log(error);
            return setState({ ...state, isloading: false, message: "The page or resource you were trying to reach is absolutely forbidden for some reason. please login again" });
        }

    }

    return (
        <div className={classes.root}>
            <Paper className={classes.main} elevation={3}>

                <div style={{ textAlign: "center", padding: 20 }}>
                    <img src={finding_ic} />
                    {
                        state.isloading ? (
                            <div style={{ padding: 20 }}>
                                <CircularProgress />
                            </div>
                        ) : (
                                <div>
                                    <Typography component="h2" gutterBottom>
                                        {state.message}
                                    </Typography>
                                    <div style={{ paddingTop: 40 }} />
                                    <Link
                                        href={global.adminpanel}
                                    >
                                        <Fab
                                            variant="extended"
                                            size="medium"
                                            color="primary"
                                            classes={{ label: classes.buttonLable, sizeMedium: classes.buttonSize }}
                                            onClick={() => window.location = global.adminpanel}
                                        >
                                            Login
                                        </Fab>
                                    </Link>
                                </div>
                            )
                    }
                </div>
            </Paper>
        </div >
    );
}

export default Loading

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        backgroundImage: `url(${banner_bg})`,
        backgroundSize: 'cover'
    },
    main: {
        margin: theme.spacing(6),
        backgroundColor: '#FFFFFF',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: 320
    },
    buttonLable: {
        textTransform: "capitalize",
        fontSize: 16
    },
    buttonSize: {
        minWidth: "230px !important"
    },
}));
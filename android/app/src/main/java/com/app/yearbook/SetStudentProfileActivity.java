package com.app.yearbook;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.UserProfileAdapter;
import com.app.yearbook.databinding.ActivitySetStudentProfileBinding;
import com.app.yearbook.model.profile.ChangeProfilePhoto;
import com.app.yearbook.model.studenthome.profile.StudentProfile;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.OnRecyclerClick;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SetStudentProfileActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView toolbar_title;
    ActivitySetStudentProfileBinding binding;
    ArrayList<String> userImagePath;
    ArrayList<StudentProfile> studentProfiles;
    String userId, profile = "";
    private AllMethods allMethods;
    public ProgressDialog progressDialog;
    private LoginUser loginUser;
    private MenuItem itemSave;
    private static final String TAG = "SetStudentProfileActivi";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_set_student_profile);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_set_student_profile);
        setView();
    }

    public void setView() {
        Log.d(TAG, "setView: ");
        //toolbar
        setToolbar();

        //SP
        loginUser = new LoginUser(SetStudentProfileActivity.this);
        allMethods = new AllMethods();
        userImagePath = new ArrayList<>();
        studentProfiles = new ArrayList<>();
        if (getIntent().getExtras() != null) {
            userImagePath.addAll(getIntent().getStringArrayListExtra("UserImage"));
            userId = getIntent().getStringExtra("userId");
            for (int i = 0; i < userImagePath.size(); i++) {
                studentProfiles.add(new StudentProfile(userImagePath.get(i), false));
            }
            setAdapter();
        }
    }

    private void setAdapter() {

        if (studentProfiles.size() > 0) {

            binding.recyclerView.setVisibility(View.VISIBLE);
            binding.lvNoData.setVisibility(View.GONE);

            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
            gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            binding.recyclerView.setLayoutManager(gridLayoutManager);
            binding.recyclerView.setItemAnimator(new DefaultItemAnimator());

            UserProfileAdapter userProfileAdapter = new UserProfileAdapter(SetStudentProfileActivity.this, studentProfiles, new OnRecyclerClick() {
                @Override
                public void onClick(int pos, View v, int type) {
                    for (StudentProfile studentProfile : studentProfiles) {
                        studentProfile.setChecked(false);
                    }
                    studentProfiles.get(pos).setChecked(true);
                    profile = studentProfiles.get(pos).getPath();
                    binding.recyclerView.getAdapter().notifyDataSetChanged();
                }
            });
            binding.recyclerView.setAdapter(userProfileAdapter);
        } else {
            binding.recyclerView.setVisibility(View.GONE);
            binding.lvNoData.setVisibility(View.VISIBLE);
        }
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Select Photo");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5.0f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.save_image) {
            if (!profile.equals("")) {
                androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(SetStudentProfileActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.custom_alertbox_staff, null);
                dialogBuilder.setView(dialogView);
                final androidx.appcompat.app.AlertDialog alert = dialogBuilder.create();
                alert.setCancelable(false);
                alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
                tvMessage.setText("Are you sure you would like to make this change?");

                TextView tvYes = dialogView.findViewById(R.id.tvYes);
                TextView tvNo = dialogView.findViewById(R.id.tvNo);

                tvYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                        setProfile(profile);
                    }
                });

                tvNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });
                alert.show();
            } else {
                allMethods.setAlert(SetStudentProfileActivity.this, "", "Please select photo");
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void setProfile(String profile) {
        //Progress bar
        progressDialog = ProgressDialog.show(SetStudentProfileActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        File fileImage = new File(profile);
        RequestBody Id = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());
        Log.d("TTT", "retrofittt: " + userId + " / " + fileImage.getName());

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<ChangeProfilePhoto> userPost = retrofitClass.addEditUserPhoto(Id, rbFile);
        userPost.enqueue(new Callback<ChangeProfilePhoto>() {
            @Override
            public void onResponse(Call<ChangeProfilePhoto> call, final Response<ChangeProfilePhoto> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    Log.d("TTT", "Tokennnn: " + response.body().getData().getUserToken());

                    loginUser.setUserData(response.body().getData());
                    // profile = loginUser.getUserData().getUserImage();
                    Log.d("TTT", "SP: " + loginUser.getUserData().getUserType());
                    // allMethods.setAlert(SetStudentProfileActivity.this, getResources().getString(R.string.app_name), "Profile edit successfully");

                    AlertDialog.Builder dialog = new AlertDialog.Builder(SetStudentProfileActivity.this);
                    dialog.setCancelable(false);
                    dialog.setTitle("");
                    dialog.setMessage(response.body().getResponseMsg());
                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //Action for "Delete".
                            dialog.dismiss();
                            Intent i = new Intent();
                            i.putExtra("profile", response.body().getData().getUserImage());
                            setResult(RESULT_OK, i);
                            finish();
                        }
                    });

                    final AlertDialog alert = dialog.create();
                    alert.show();

                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(SetStudentProfileActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(SetStudentProfileActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(SetStudentProfileActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    allMethods.setAlert(SetStudentProfileActivity.this, "", response.body().getResponseMsg() + "");
                }
            }

            @Override
            public void onFailure(Call<ChangeProfilePhoto> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(SetStudentProfileActivity.this, "", t.getMessage() + "");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        Log.d(TAG, "onCreateOptionsMenu: ");
        getMenuInflater().inflate(R.menu.save, menu);
        itemSave = menu.findItem(R.id.save_image);
        if (studentProfiles.size() > 0) {
            itemSave.setVisible(true);
        } else {
            itemSave.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }
}

package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.StudentCategoryAdapter;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.yearbook.CategoryList;
import com.app.yearbook.model.yearbook.GetCategory;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffCategoryActivity extends AppCompatActivity implements View.OnClickListener {

    private String yearbookId, year;
    private ArrayList<CategoryList> categoryLists;
    private Toolbar toolbar;
    private AllMethods allMethods;
    public ProgressDialog progressDialog;
    private RecyclerView rvCategory;
    private LinearLayout lvNoData;
    private ImageView imgAddCat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_category);

        setView();
    }

    public void setView() {
        //get intent
        if (getIntent().getExtras() != null) {
            yearbookId = getIntent().getStringExtra("yearBookId");
            year = getIntent().getStringExtra("year");
        }

        //all methods object
        allMethods = new AllMethods();

        //set toolbar
        setToolbar();

        //add cat
        imgAddCat = findViewById(R.id.imgAddCat);
        imgAddCat.setOnClickListener(this);

        //category
        rvCategory = findViewById(R.id.rvCategory);

        //lv
        lvNoData = findViewById(R.id.lvNoData);

        getCategory();
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(year);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void getCategory() {
        Log.d("TTT", "Yeaybook Id: " + yearbookId);
        //Progress bar
        progressDialog = ProgressDialog.show(StaffCategoryActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

       RequestBody id = RequestBody.create(MediaType.parse("text/plain"), yearbookId);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GetCategory> category = retrofitClass.getCategory(id);
        category.enqueue(new Callback<GetCategory>() {
            @Override
            public void onResponse(Call<GetCategory> call, Response<GetCategory> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (response.body().getResponseCode().equals("1")) {
                    if (response.body().getCategoryList().size() > 0) {
                        rvCategory.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        categoryLists = new ArrayList<>();
                        categoryLists.addAll(response.body().getCategoryList());

                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(StaffCategoryActivity.this);
                        rvCategory.setLayoutManager(mLayoutManager);

                        StudentCategoryAdapter mAdapter = new StudentCategoryAdapter(categoryLists, StaffCategoryActivity.this, "staff");
                        rvCategory.setAdapter(mAdapter);

                    } else {
                        rvCategory.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10")) {
                    Toast.makeText(StaffCategoryActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(StaffCategoryActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(StaffCategoryActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    rvCategory.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<GetCategory> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(StaffCategoryActivity.this, "", t.getMessage() + "");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgAddCat) {
            addCategory();
        }
    }

    public void addCategory() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(StaffCategoryActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_alertbox_report, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alert = dialogBuilder.create();
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        final Button btnSend = dialogView.findViewById(R.id.btnSend);
        final EditText etMessage = dialogView.findViewById(R.id.etMessage);
        etMessage.setLines(1);
        etMessage.setHint("Add category name");

        TextView tvTitle = dialogView.findViewById(R.id.tvTitle);
        tvTitle.setText("Create category");
        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    btnSend.setEnabled(true);
                    btnSend.setBackgroundColor(getResources().getColor(R.color.btnGray));
                    btnSend.setTextColor(getResources().getColor(R.color.white));
                } else {
                    btnSend.setEnabled(false);
                    btnSend.setBackgroundColor(getResources().getColor(R.color.edtBG));
                    btnSend.setTextColor(getResources().getColor(R.color.deselectTab));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = ProgressDialog.show(StaffCategoryActivity.this, "", "", true);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDialog.setContentView(R.layout.progress_view);
                ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
                Circle bounce = new Circle();
                bounce.setColor(Color.BLACK);
                progressBar.setIndeterminateDrawable(bounce);

                RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
                    final Call<GiveReport> report = retrofitClass.addCategory(Integer.parseInt(yearbookId), etMessage.getText().toString());
                    report.enqueue(new Callback<GiveReport>() {
                        @Override
                        public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                            alert.dismiss();
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            if(response.body().getResponseCode().equalsIgnoreCase("1"))
                            {
                                getCategory();
//                                CategoryList cl=new CategoryList(yearbookId,etMessage.getText().toString());
//                                categoryLists.add(cl);
//                                rvCategory.getAdapter().notifyDataSetChanged();
                            }
                            else if (response.body().getResponseCode().equals("10")) {
                                Toast.makeText(StaffCategoryActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                                LoginUser loginSP = new LoginUser(StaffCategoryActivity.this);
                                loginSP.clearData();

                                Intent i = new Intent(StaffCategoryActivity.this, LoginActivity.class);
                                startActivity(i);
                                finish();
                            }
                            else
                            {
                                Toast.makeText(StaffCategoryActivity.this, response.body().getResponseMsg()+ "", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<GiveReport> call, Throwable t) {
                            alert.dismiss();
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            Toast.makeText(StaffCategoryActivity.this, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                        }
                    });
            }
        });
        alert.show();

    }
}

package com.app.yearbook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.yearbook.adapter.ThumbnailsAdapter;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.ThumbnailCallback;
import com.app.yearbook.utils.ThumbnailItem;
import com.app.yearbook.utils.ThumbnailsManager;
import com.zomato.photofilters.FilterPack;
import com.zomato.photofilters.imageprocessors.Filter;

import java.io.IOException;
import java.util.List;

public class ImageFilterForStaffPostActivity extends AppCompatActivity implements ThumbnailCallback {

    static {
        System.loadLibrary("NativeImageProcessor");
    }

    private Activity activity;
    private Toolbar toolbar;
    private Uri getUri;
    private RecyclerView thumbListView;
    private ImageView placeHolderImageView;
    private Bitmap thumbImage;
    private String getType, CategoryId = "", YearbookPost = "";
    private int CropType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_filterforstaff);
        setView();
    }

    public void setView() {

        //toolbar
        setToolbar();

        if (getIntent().getExtras() != null) {
            getUri = Uri.parse(getIntent().getStringExtra("selectUri"));
            getType = getIntent().getStringExtra("from");

            if (getType.equalsIgnoreCase("staff")) {
                CategoryId = getIntent().getStringExtra("CategoryId");
                CropType = getIntent().getIntExtra("CropType", 1);

                if(getIntent().getStringExtra("EditYearPost")!=null) {
                    YearbookPost = getIntent().getStringExtra("EditYearPost");
                }
            }

            try {
                thumbImage = MediaStore.Images.Media.getBitmap(getContentResolver(), getUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        activity = this;
        initUIWidgets();
    }

    private void initUIWidgets() {
        thumbListView = findViewById(R.id.thumbnails);
        placeHolderImageView = findViewById(R.id.place_holder_imageview);
        // placeHolderImageView.setImageBitmap(Bitmap.createScaledBitmap(thumbImage, 640, 640, false));
        placeHolderImageView.setImageBitmap(thumbImage);
        initHorizontalList();
    }

    private void initHorizontalList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.scrollToPosition(0);
        thumbListView.setLayoutManager(layoutManager);
        thumbListView.setHasFixedSize(true);
        bindDataToAdapter();
    }

    private void bindDataToAdapter() {
        final Context context = this.getApplication();
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {

                ThumbnailsManager.clearThumbs();
                // add normal bitmap first
                ThumbnailItem thumbnailItem = new ThumbnailItem();
                thumbnailItem.image = thumbImage;
                thumbnailItem.filter.setName(getString(R.string.filter_normal));
                ThumbnailsManager.addThumb(thumbnailItem);

                List<Filter> filters = FilterPack.getFilterPack(getApplicationContext());

                for (Filter filter : filters) {
                    ThumbnailItem tI = new ThumbnailItem();
                    tI.image = thumbImage;
                    tI.filter = filter;
                    ThumbnailsManager.addThumb(tI);
                }

                List<ThumbnailItem> thumbs = ThumbnailsManager.processThumbs(context);

                ThumbnailsAdapter adapter = new ThumbnailsAdapter(getApplicationContext(), thumbs, (ThumbnailCallback) activity);
                thumbListView.setAdapter(adapter);

                adapter.notifyDataSetChanged();
            }
        };
        handler.post(r);
    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Filter");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.next) {
            BitmapDrawable drawable = (BitmapDrawable) placeHolderImageView.getDrawable();
            Bitmap bitmap = drawable.getBitmap();

            Uri uri = AllMethods.getImageUri(getApplicationContext(), bitmap);

            if (getType.equalsIgnoreCase("library")) {
                Intent i = new Intent(getApplicationContext(), AddPostActivity.class);
                i.putExtra("Type", "Image");
                i.putExtra("AddPostImage", uri.toString());
                startActivityForResult(i, 222);
            } else if (getType.equalsIgnoreCase("staff")) {

                Log.d("TTT","ImageFilterrr: "+CropType);
                if (YearbookPost.equalsIgnoreCase("StaffPost") && YearbookPost!=null) {
                    Intent i = new Intent();
                    i.putExtra("Type", "Image");
                    i.putExtra("CropType", CropType);
                    i.putExtra("AddPostImage", uri.toString());
                    setResult(RESULT_OK, i);
                    finish();
                } else {
                    Intent i = new Intent(getApplicationContext(), AddYearbookPostActivity.class);
                    i.putExtra("CategoryId", CategoryId);
                    i.putExtra("Type", "Image");
                    i.putExtra("AddPostImage", uri.toString());
                    i.putExtra("CropType", CropType);
                    startActivityForResult(i, 222);
                }

            } else if (getType.equalsIgnoreCase("advertisement")) {
                Intent i = new Intent(getApplicationContext(), AdvertismentFinalPostActivity.class);
                i.putExtra("AddPostImage", uri.toString());
                startActivityForResult(i, 222);
            }

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onThumbnailClick(Filter filter) {
        Bitmap.Config config = null;
        //int width = thumbImage.getWidth();
        //int height = thumbImage.getHeight();

        if (thumbImage.getConfig() == null) {
            config = Bitmap.Config.ARGB_8888;
        } else {
            config = thumbImage.getConfig();
        }

        int pixel;
        // Here you ll get the immutable Bitmap
        Bitmap copyBitmap = thumbImage.copy(config, true);
        // scan through all pixels
//        for (int x = 0; x < width; ++x) {
//            for (int y = 0; y < height; ++y) {
//                // get pixel color
//                pixel = thumbImage.getPixel(x, y);
//                if (x == 400 && y == 200) {
//                    copyBitmap.setPixel(x, y,
//                            Color.argb(0xFF, Color.red(pixel), 0, 0));
//                } else {
//                    copyBitmap.setPixel(x, y, pixel);
//                }
//            }
//        }
        placeHolderImageView.setImageBitmap(filter.processFilter(Bitmap.createScaledBitmap(copyBitmap, thumbImage.getWidth(), thumbImage.getHeight(), false)));
        //placeHolderImageView.setImageBitmap(filter.processFilter(Bitmap.createScaledBitmap(copyBitmap, 640, 640, false)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selectimage, menu);
        MenuItem next = menu.findItem(R.id.next);
        next.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 222 && resultCode == RESULT_OK) {
            Log.d("TTTT", "ImageFilterActivity onActivityResult");
            Intent i = getIntent();
            setResult(RESULT_OK, i);
            finish();
        }
    }
}

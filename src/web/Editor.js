import React, { Fragment } from "react"
import { View } from "react-native-web"
import { connect } from "react-redux";
import AppHeader from "../SectionsSettings/Header";
import actions from "../actions";
import EditorHeader from "../components/EditorHeader";
import { colors, action, global } from "../utils";
import EditorSections from "../components/Drawer/AppDrawer/EditorSections";
import SectionSettingSidebar from "../components/Drawer/AppDrawer/SectionSettingSidebar";
import moment from "moment";
import { auto_save_time } from "../utils/const";
import html2canvas from "html2canvas";
import {
    TitleSubTitle,
    HeadingTitle,
    ImageSection,
    InsideGrid,
    InsideList,
    PortraitColumns,
    VideoSection,
    ImageTextOverlay
} from "../Sections";
import { Box } from "@material-ui/core";
import Gallery from "Sections/Gallery";
import YBImageSection from "Sections/YBImageSection";
import Swal from "sweetalert2"
import { withRouter } from 'react-router';
import { Prompt, } from 'react-router'

class ConatinerEditor extends React.Component {

    _autoSaveInterval = undefined;
    _reorder = false;

    constructor(props) {
        super(props);
        let id, name, category_id;
        let searchParams = new URLSearchParams(props.location.search);
        id = searchParams.get("_id") || "";
        name = searchParams.get("_name") || "";
        category_id = searchParams.get("_category") || "";

        let ybSplashscreen = null;
        if (props.yearbookDetails && (id === props.yearbookDetails.screen_id)) {
            ybSplashscreen = { component_id: "", component_order_by: 0, component_type: 9, component_url: "", component_title: "", component_description: "" };
        }

        this.state = {
            category_id,
            screen_id: id || "",
            screenName: name,
            selected: [],
            drawer: null,
            settingsComponent: null,
            lastSave: new Date(),
            isUpdate: false,
            ybSplashscreen
        }
    }

    componentDidMount() {
        // console.log(this.props);
        this._sectionList();
        // window.history.pushState(null, document.title, this.props.location.href);
        // window.history.replaceState(null, document.title, this.props.location.href);
        // window.history.pushState(null, document.title, this.props.match.path);
        // window.addEventListener("beforeunload", this._confirm);
        // window.onpopstate = this._backConfirm;
        window.onbeforeunload = this._confirm;
        // window.isEditor = false;
    }

    componentWillUnmount() {
        clearTimeout(this._autoSaveInterval);
        // window.removeEventListener("beforeunload", this._confirm);
        window.onpopstate = () => { }
        window.onbeforeunload = () => { }
    }

    _backConfirm = async () => {
        const { history, location, match } = this.props;
        if (!window.isEditor) {
            // window.history.pushState(null, document.title, match.path);
            Swal.fire({
                title: 'Are you sure?',
                text: 'Confirm that your changes have been saved before leaving the page!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Leave',
                cancelButtonText: 'Cancel'
            }).then((result) => {
                if (result.value) {
                    history.goBack();
                    window.isEditor = true;
                } else {
                    window.history.pushState(null, document.title, location.href);
                }
            });
        }

    }

    _prompt = () => {
        window.isEditor = false;
        return "You have unsaved changes, are you sure you want to leave?";
    }

    _confirm = (e) => {
        var confirmationMessage = "\o/";
        e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
        return confirmationMessage;
    }

    _reorderSection = (data) => {
        // console.log('main', data)
        this._reorder = true;
        this.setState({ drawer: <EditorSections obj={this} />, selected: data })
    }

    _sectionList = async (loader = true) => {
        let data = await this.props.get_screen_detail(this.state.screen_id, loader);
        data = data || [];
        this.setState((preState) => {
            let ybSplashscreen = data.filter(item => item.component_type === 9);
            if (ybSplashscreen[0]) {
                preState.ybSplashscreen = ybSplashscreen[0];
            }
            data = data.filter(item => item.component_type !== 9);
            preState.selected = data.map((item, index) => { return { ...item, component_order_by: index } });
            return {
                ...preState,
                drawer: <EditorSections obj={this} />
            }
        })
        this._autoSave();
    }

    setSelectionDrawer = () => {
        this.setState({ drawer: <EditorSections obj={this} /> });
    }

    _addSection = (cmp) => {
        try {
            let selected = this.state.selected;
            let lastComponent = selected[selected.length - 1];
            let component_order_by = lastComponent ? (lastComponent.component_order_by + 1) : 0;
            cmp = { component_order_by, ...cmp }
            selected.push(cmp);
            this.setState({ selected });
            this.editComponentSettings(component_order_by);
            setTimeout(() => {
                if (this.refs.previewCapture != null) {
                    this.refs.previewCapture.scroll({ top: this.refs.previewCapture.scrollHeight, behavior: "smooth" })
                }
            }, 300);
        } catch (error) {
            console.log("_addSection =>", error)
        }
    }

    _removeSection = (order) => {
        let { selected } = this.state;
        selected = selected.filter(item => item.component_order_by !== order)
        selected = selected.map((item, i) => {
            item.component_order_by = i
            return item
        })
        this.setState({
            drawer: <EditorSections obj={this} />,
            selected
        })
    }

    editYBSection = () => {
        try {
            this.setState((previseState) => {
                return {
                    drawer: <SectionSettingSidebar obj={this} index={0} />,
                    settingsComponent: previseState.ybSplashscreen
                }
            })
        } catch (error) {
            console.log("editYBSection => ", error);
        }
    }

    editComponentSettings = (order) => {
        try {
            // console.log('main ', this.state.selected.filter(item => item.component_order_by === order)[0])
            let index = this.state.selected.findIndex(item => item.component_order_by === order);
            if (index === -1) return;
            this.setState((previseState) => {
                return {
                    drawer: <SectionSettingSidebar obj={this} index={index} />,
                    settingsComponent: previseState.selected[index]
                }
            })
        } catch (error) {
            console.log("editComponentSettings =>", error)
        }

    }

    ypUpdateComponents = (data) => {
        try {
            this.setState((prevState) => {
                prevState.ybSplashscreen = { ...prevState.ybSplashscreen, ...data };
                return {
                    ...prevState,
                    isUpdate: true
                }
            });
            this._autoSave(true);
        } catch (error) {
            console.log("ypUpdateComponents =>", error)
        }
    }

    updateComponents = (data, order) => {
        try {
            let selected = this.state.selected;
            let index = selected.findIndex(item => item.component_order_by === order);
            data = { ...selected[index], ...data };
            selected[index] = data;
            this.setState({ selected, settingsComponent: data, isUpdate: true });
            this._autoSave(true);
        } catch (error) {
            console.log("updateComponents =>", error)
        }
    }

    _captureScreen = () => {
        return new Promise(resolve => {
            html2canvas(document.querySelector("#capture"), {
                allowTaint: true,
                logging: true,
                taintTest: false,
                useCORS: true
            }).then((canvas) => {
                let base64 = canvas.toDataURL("image/png");
                resolve(base64)
            });
        })
    }

    render() {
        let { selected, lastSave, drawer, screenName, screen_id, ybSplashscreen, } = this.state;
        const yearbookDetails = this.props.yearbookDetails || [];
        return (
            <View style={{ height: "100vh" }}>
                <Prompt
                    message={(location, action) => {
                        if (location.pathname === "/app/page-editor" || window.isnavigate) {
                            window.isnavigate = false;
                            return true;
                        } else {
                            Swal.fire({
                                title: 'Are you sure?',
                                text: 'Confirm that your changes have been saved before leaving the page!',
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonText: 'Leave',
                                cancelButtonText: 'Cancel'
                            }).then((result) => {
                                if (result.value) {
                                    window.isnavigate = true;
                                    if (action === "") {
                                        this.props.history.goBack();
                                    } else {
                                        this.props.history.push(location.pathname);
                                    }
                                } else {
                                    window.isnavigate = false;
                                }
                            });
                            return false
                        }
                    }}
                />
                <EditorHeader
                    title={screenName}
                    onTitleChange={(screenName) => this.setState({ screenName })}
                    onSave={this.onSave}
                    lastUpdated={moment(lastSave).format("hh:mm A  D MMM, YYYY")}
                />
                <View style={{ flex: 1, flexDirection: "row" }}>

                    <View style={{ width: 270, backgroundColor: colors.Gray, overflow: "auto" }}>
                        {drawer}
                    </View>

                    <View style={styles.root}>
                        <Box boxShadow={8} borderRadius={4}>
                            <div
                                elevation={3}
                                style={styles.device}
                                id="capture"
                            >
                                {screen_id !== yearbookDetails.screen_id && (
                                    <AppHeader title={screenName} />
                                )}
                                <div
                                    style={{ overflow: "auto", height: screen_id !== yearbookDetails.screen_id ? global.height - 78 : global.height }}
                                    ref="previewCapture"
                                >

                                    {
                                        ybSplashscreen && (
                                            <YBImageSection data={ybSplashscreen} />
                                        )
                                    }

                                    {selected && selected.map(this._renderComponent)}
                                </div>
                            </div>
                        </Box>
                    </View>

                </View>


            </View>
        )
    }

    onSave = async (isAuto = false) => {
        let { selected, screen_id, screenName, ybSplashscreen } = this.state;
        if (!selected) return;
        if (ybSplashscreen) {
            const { component_url, component_title, component_description } = ybSplashscreen;
            if (component_title === "" && component_description === "" && component_url === "") {

            } else {
                selected = [ybSplashscreen, ...selected];
            }
        }
        selected = selected.map((item, index) => {
            item.component_order_by = index
            if (item.component_type === 5) {
                let students = "", staff = "";
                item.staff_list.map((s) => staff += s.id + ",")
                item.student_list.map((s) => students += s.id + ",")
                item.staff_list = staff
                item.student_list = students
            }
            return item
        })

        console.log(selected)
        // return;

        if (!isAuto) this.props.snackMessage("loader", "Please wait...");

        if (this.refs.previewCapture != null) {
            this.refs.previewCapture.scroll({ top: 0 });
        }
        setTimeout(async () => {
            let snapshot = await this._captureScreen();
            let res = await this.props.add_screen_component(screen_id, screenName, selected, snapshot);
            if (res.Result) this._sectionList(false);
            
            // this.setState({ lastSave: new Date(), isUpdate: false })
            window.onbeforeunload = () => { }
            window.location.reload(true)
        }, 300);
    }

    _autoSave = (isUpdate = this.state.isUpdate) => {
        if (isUpdate) {
            clearTimeout(this._autoSaveInterval);
            this._autoSaveInterval = setTimeout(() => {
                console.log("Auto Saving...!");
                clearTimeout(this._autoSaveInterval);
                this.onSave(true);
            }, auto_save_time);
            return;
        }
    }

    _renderComponent = (item, index) => {
        return (
            <Fragment key={index}>
                {item.component_type === 0 && (<TitleSubTitle data={item} />)}
                {item.component_type === 1 && (<HeadingTitle data={item} />)}
                {item.component_type === 2 && (<ImageSection data={item} />)}
                {item.component_type === 3 && (<ImageTextOverlay data={item} />)}
                {item.component_type === 4 && (<VideoSection data={item} />)}
                {item.component_type === 5 && (<PortraitColumns data={item} />)}
                {item.component_type === 6 && (
                    <InsideGrid
                        data={item}
                        screenId={this.state.screen_id}
                    />
                )}
                {item.component_type === 7 && (
                    <InsideList
                        data={item}
                        screenId={this.state.screen_id}
                    />
                )}
                {item.component_type === 8 && (<Gallery data={item} />)}
                {/* {item.component_type === 9 && (<YBImageSection data={item} />)} */}
            </Fragment>
        )
    }

}

function mapStateToProps({ yearbook }) {
    return {
        yearbookDetails: yearbook.yearbookDetails
    }
}

function mapDispatchToProps(dispatch) {
    return {
        get_screen_detail: (id, loader) => dispatch(actions.get_screen_detail(id, loader)),
        add_screen_component: (id, name, data, snapshot) => dispatch(actions.add_screen_component(id, name, data, snapshot)),
        snackMessage: (variant, message) => dispatch({ type: action.open_notification, message, variant }),
        snackMessageClose: () => dispatch({ type: action.close_notification })
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ConatinerEditor));


const styles = {
    root: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    device: {
        maxHeight: "85vh",
        maxWidth: "25vw",
        minWidth: 320,
        width: 320,
        minHeight: 520,
        overflow: "hidden",
        overflowY: "scroll"
    },
    textComponent: {
        paddingLeft: 10,
        paddingRight: 10
    }
};
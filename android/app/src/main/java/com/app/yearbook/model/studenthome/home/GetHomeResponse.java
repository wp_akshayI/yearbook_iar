package com.app.yearbook.model.studenthome.home;

import com.google.gson.annotations.SerializedName;
public class GetHomeResponse{

	@SerializedName("ResponseCode")
	private String responseCode;

	@SerializedName("ServerTimeZone")
	private String serverTimeZone;

	@SerializedName("ResponseMsg")
	private String responseMsg;

	@SerializedName("serverTime")
	private String serverTime;

	@SerializedName("user_post")
	private UserPost userPost;

	@SerializedName("Result")
	private String result;

	public void setResponseCode(String responseCode){
		this.responseCode = responseCode;
	}

	public String getResponseCode(){
		return responseCode;
	}

	public void setServerTimeZone(String serverTimeZone){
		this.serverTimeZone = serverTimeZone;
	}

	public String getServerTimeZone(){
		return serverTimeZone;
	}

	public void setResponseMsg(String responseMsg){
		this.responseMsg = responseMsg;
	}

	public String getResponseMsg(){
		return responseMsg;
	}

	public void setServerTime(String serverTime){
		this.serverTime = serverTime;
	}

	public String getServerTime(){
		return serverTime;
	}

	public void setUserPost(UserPost userPost){
		this.userPost = userPost;
	}

	public UserPost getUserPost(){
		return userPost;
	}

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}
}
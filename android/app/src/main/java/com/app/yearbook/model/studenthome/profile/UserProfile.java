package com.app.yearbook.model.studenthome.profile;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.app.yearbook.model.studenthome.home.PostListItem;

public class UserProfile{

	@SerializedName("post_list")
	private List<PostListItem> postList;

	@SerializedName("user_detail")
	private UserDetail userDetail;

	public void setPostList(List<PostListItem> postList){
		this.postList = postList;
	}

	public List<PostListItem> getPostList(){
		return postList;
	}

	public void setUserDetail(UserDetail userDetail){
		this.userDetail = userDetail;
	}

	public UserDetail getUserDetail(){
		return userDetail;
	}
}
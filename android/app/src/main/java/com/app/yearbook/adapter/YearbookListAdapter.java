package com.app.yearbook.adapter;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.yearbook.LoginActivity;
import com.app.yearbook.R;
import com.app.yearbook.ReactCode;
import com.app.yearbook.ReactModules;
import com.app.yearbook.SchoolUserImageActivity;
import com.app.yearbook.UserProcessWithWeb;
import com.app.yearbook.databinding.ItemAllstudentListBinding;
import com.app.yearbook.databinding.ItemYearbookBinding;
import com.app.yearbook.model.YBResponse;
import com.app.yearbook.model.employee.UserListItem;
import com.app.yearbook.utils.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

public class YearbookListAdapter extends RecyclerView.Adapter<YearbookListAdapter.MyViewHolder> implements Filterable {
    private ArrayList<YBResponse.YearbookList> yearbookList;
    private ArrayList<YBResponse.YearbookList> yearbookListFiltered;
    private Activity ctx;
    ItemYearbookBinding binding;
    boolean isStudent;

    @NonNull
    @Override
    public YearbookListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_yearbook, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final YearbookListAdapter.MyViewHolder holder, final int position) {
        final YBResponse.YearbookList userList = yearbookListFiltered.get(position);

        holder.binding.tvYearbook.setText(userList.getYearbookName());
        holder.binding.tvIsPurchased.setText("Purchased");

        if (isStudent) {
//            holder.binding.imgNext.setVisibility(userList.isPurchased() ? View.VISIBLE : View.GONE);
            holder.binding.txtBuy.setVisibility(!userList.isPurchased() ? View.VISIBLE : View.GONE);
            if (!userList.isPurchased()) {
                holder.binding.tvIsPurchased.setText("Yearbook & Signatures");
                holder.binding.imgNext.setVisibility(View.GONE);
            } else if (!userList.getProgress().equalsIgnoreCase("Complete")) {
                holder.binding.tvIsPurchased.setText("In Progress");
                holder.binding.imgNext.setVisibility(View.GONE);
            } else if (userList.isPurchased()) {
                holder.binding.tvIsPurchased.setText("Purchased");
                holder.binding.imgNext.setVisibility(View.VISIBLE);
            } else {
                holder.binding.tvIsPurchased.setText(!TextUtils.isEmpty(userList.getProgress()) ? userList.getProgress() : "");
                holder.binding.imgNext.setVisibility(View.GONE);
            }
            holder.binding.txtBuy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ctx, UserProcessWithWeb.class);
                    intent.putExtra("action", Constants.USER_WEB_PAYMENT);
                    intent.putExtra("fromYBList", true);
                    ctx.startActivity(intent);
                }
            });
        }

        Glide.with(ctx)
                .load(userList.getYearbookCover())
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                .placeholder(R.mipmap.profile_placeholder)
//                .error(R.mipmap.profile_placeholder)
                .into(holder.binding.imgUserImage);

        holder.binding.tvYearbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performClickAct(position);
            }
        });
        holder.binding.tvIsPurchased.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performClickAct(position);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performClickAct(position);
            }
        });
    }

    private void performClickAct(int position) {
        if (isStudent) {
            if (yearbookListFiltered.get(position).isPurchased()) {
                if (yearbookListFiltered.get(position).getYearbookStatus().equalsIgnoreCase("1") &&
                        yearbookListFiltered.get(position).getProgress().equalsIgnoreCase("Complete")) {
                    Intent i = new Intent(ctx, ReactCode.class);
                    ReactModules.yearbookObjLastClicked = yearbookListFiltered.get(position);
                    ctx.startActivity(i);
                } else {
                    Toast.makeText(ctx, "Yearbook is in-progress or not published yet", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            Intent i = new Intent(ctx, ReactCode.class);
            ReactModules.yearbookObjLastClicked = yearbookListFiltered.get(position);
            ctx.startActivity(i);
        }
    }

    public YearbookListAdapter(ArrayList<YBResponse.YearbookList> yearbookList, boolean isStudent, Activity context) {
        this.yearbookListFiltered = yearbookList;
        this.yearbookList = yearbookList;
        this.ctx = context;
        this.isStudent = isStudent;
    }

    @Override
    public int getItemCount() {
        return yearbookListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    yearbookListFiltered = yearbookList;
                } else {
                    ArrayList<YBResponse.YearbookList> filteredList = new ArrayList<>();
                    for (YBResponse.YearbookList row : yearbookListFiltered) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name
                        if (row.getYearbookName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                            Log.d("TTT", "Filter size: " + filteredList.size());
                        }
                    }
                    yearbookListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = yearbookListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                yearbookListFiltered = (ArrayList<YBResponse.YearbookList>) filterResults.values;
                Log.d("TTT", "publishResults: " + yearbookListFiltered.size());
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ItemYearbookBinding binding;

        public MyViewHolder(ItemYearbookBinding view) {
            super(view.getRoot());
            binding = view;
        }
    }
}

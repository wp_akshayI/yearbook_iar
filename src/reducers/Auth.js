import { action } from "../utils";

let initialState = {
    authUser: null,
    schoolID: null,
    school_name: null,
    isAuth: false,
    initURL: '',
}

export default function AuthReducer(state = initialState, data) {
    const { payload } = data;
    switch (data.type) {

        case action.INITIAL_STATE:
            return {
                ...initialState
            }

        case action.AUTH_SUCCESS:
            if(!payload) return state;
            return {
                ...state,
                schoolID: payload.school_id,
                authUser: payload.token,
                school_name: payload.school_name,
                isAuth: true
            }

        case action.SET_AUTH_TOKEN: {
            return {
                ...state,
                authUser: payload
            }
        }

        case action.INIT_URL: {
            return {
                ...state,
                initURL: payload
            }
        }

        default:
            break;
    }
    return state;
}
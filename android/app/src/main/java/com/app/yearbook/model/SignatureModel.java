package com.app.yearbook.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SignatureModel implements Parcelable {

    String greeting;
    String message;
    int fontResourceId;
    String fontId;

    public String getFontId() {
        return fontId;
    }

    public void setFontId(String fontId) {
        this.fontId = fontId;
    }

    public int getFontResourceId() {
        return fontResourceId;
    }

    public void setFontResourceId(int fontResourceId) {
        this.fontResourceId = fontResourceId;
    }

    String userId;
    String userImage;
    String userName;
    String userType;

    public SignatureModel(String greeting, String message,String fontId, int fontResourceId, String userId, String userImage, String userName, String userType) {
        this.greeting = greeting;
        this.message = message;
        this.fontId = fontId;
        this.fontResourceId = fontResourceId;
        this.userId = userId;
        this.userImage = userImage;
        this.userName = userName;
        this.userType = userType;
    }

    public SignatureModel() {
    }

    protected SignatureModel(Parcel in) {
        greeting = in.readString();
        message = in.readString();
        fontId = in.readString();
        fontResourceId = in.readInt();
        userId = in.readString();
        userImage = in.readString();
        userName = in.readString();
        userType = in.readString();
    }

    public static final Creator<SignatureModel> CREATOR = new Creator<SignatureModel>() {
        @Override
        public SignatureModel createFromParcel(Parcel in) {
            return new SignatureModel(in);
        }

        @Override
        public SignatureModel[] newArray(int size) {
            return new SignatureModel[size];
        }
    };

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(greeting);
        dest.writeString(message);
        dest.writeString(fontId);
        dest.writeInt(fontResourceId);
        dest.writeString(userId);
        dest.writeString(userImage);
        dest.writeString(userName);
        dest.writeString(userType);
    }
}

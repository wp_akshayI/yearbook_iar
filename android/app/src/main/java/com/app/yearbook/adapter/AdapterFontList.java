package com.app.yearbook.adapter;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.yearbook.R;
import com.app.yearbook.databinding.AdapterSignatureFontBinding;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.SignFontModel;

import java.util.List;

public class AdapterFontList extends RecyclerView.Adapter<AdapterFontList.FontHolder> {

    List<SignFontModel> signFontModelList;
    OnRecyclerClick onRecyclerClick;

    public AdapterFontList(List<SignFontModel> signFontModelList, OnRecyclerClick onRecyclerClick) {
        this.signFontModelList = signFontModelList;
        this.onRecyclerClick = onRecyclerClick;
    }

    @NonNull
    @Override
    public AdapterFontList.FontHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterSignatureFontBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_signature_font, parent, false);
        return new FontHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterFontList.FontHolder holder, int position) {
        holder.binding.setModel(signFontModelList.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return signFontModelList.size();
    }

    public class FontHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        AdapterSignatureFontBinding binding;

        public FontHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerClick.onClick(getAdapterPosition(), 0);
        }
    }
}

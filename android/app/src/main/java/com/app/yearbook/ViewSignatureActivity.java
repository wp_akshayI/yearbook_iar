package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.yearbook.utils.OnRecyclerClick;
import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.adapter.StudentSignatureListAdapter;
import com.app.yearbook.model.profile.SignatureList;
import com.app.yearbook.model.profile.getSignature;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.RecyclerItemTouchHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewSignatureActivity extends AppCompatActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener{

    private Toolbar toolbar;
    private String userId;
    public ProgressDialog progressDialog;
    private RecyclerView rvSignature;
    private LinearLayout lvNoData;
    private StudentSignatureListAdapter studentSignatureListAdapter;
    private AllMethods allMethods;
    private ArrayList<SignatureList> signatureList;
    private static final String TAG = "ViewSignatureActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_signature);

        setView();
    }

    public void setView()
    {
        //set toolbar
        setToolbar();

        if(getIntent().getExtras()!=null)
        {
            userId=getIntent().getStringExtra("Id");

            Log.d(TAG, "setView: ViewSignatureActivity: "+userId);
        }

        //all methods object
        allMethods = new AllMethods();

        //rv
        rvSignature=findViewById(R.id.rvSignature);

        //lv
        lvNoData=findViewById(R.id.lvNoData);

        getSignature();

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rvSignature);

    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Signatures");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public  void getSignature()
    {
        //Progress bar
        progressDialog = ProgressDialog.show(ViewSignatureActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        //problem Ashish usertype
        final Call<getSignature> signature = retrofitClass.getSignature(Integer.parseInt(userId), LoginUser.getUserTypeKey(), "2020");
        signature.enqueue(new Callback<getSignature>() {
            @Override
            public void onResponse(Call<getSignature> call, Response<getSignature> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if(response.body().getResponseCode().equals("1"))
                {
                    signatureList=new ArrayList<>();
                    if(response.body().getSignatureList().size()>0)
                    {
                        rvSignature.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        signatureList.addAll(response.body().getSignatureList());
                        studentSignatureListAdapter = new StudentSignatureListAdapter(signatureList, ViewSignatureActivity.this, new OnRecyclerClick() {
                            @Override
                            public void onClick(int pos, View v, int type) {

                            }
                        });

                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        rvSignature.setLayoutManager(mLayoutManager);
                        rvSignature.setItemAnimator(new DefaultItemAnimator());
                        rvSignature.addItemDecoration(new DividerItemDecoration(ViewSignatureActivity.this, DividerItemDecoration.VERTICAL));
                        rvSignature.setAdapter(studentSignatureListAdapter);
                    }
                    else{
                        rvSignature.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10")) {
                    Toast.makeText(ViewSignatureActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(ViewSignatureActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(ViewSignatureActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    rvSignature.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<getSignature> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(ViewSignatureActivity.this, "", t.getMessage() + "");
            }
        });

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof StudentSignatureListAdapter.MyViewHolder) {
            // get the removed item name to display it in snack bar
            String Id = signatureList.get(viewHolder.getAdapterPosition()).getSignatureId();
            Log.d("TTT","Id: "+Id);

            // remove the item from recycler view
            studentSignatureListAdapter.removeItem(viewHolder.getAdapterPosition());

            deleteSignature(Id);

            if(signatureList.size()==0)
            {
                rvSignature.setVisibility(View.GONE);
                lvNoData.setVisibility(View.VISIBLE);
            }
        }
    }

    public void deleteSignature(String id)
    {
        //Progress bar
        progressDialog = ProgressDialog.show(ViewSignatureActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<GiveReport> signature = retrofitClass.deleteSignature(Integer.parseInt(id));
        signature.enqueue(new Callback<GiveReport>() {
            @Override
            public void onResponse(Call<GiveReport> call, Response<GiveReport> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if(response.body().getResponseCode().equals("1"))
                {
                    Toast.makeText(getApplicationContext(),"Signature delete successfully",Toast.LENGTH_SHORT).show();
                }
                else if(response.body().getResponseCode().equals("10")) {
                    Toast.makeText(ViewSignatureActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(ViewSignatureActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(ViewSignatureActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    allMethods.setAlert(ViewSignatureActivity.this, "", response.body().getResponseMsg() + "");
                }
            }

            @Override
            public void onFailure(Call<GiveReport> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(ViewSignatureActivity.this, "", t.getMessage() + "");
            }
        });

    }
}

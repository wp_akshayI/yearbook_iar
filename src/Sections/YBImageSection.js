import React, { useState, useEffect } from "react";
import { View, Text, ImageBackground } from "react-native";
import ImageScal from "../utils/ImageScal"
import {global} from "../utils"

export default function YBImageSection({ data }) {
    const { component_url, component_title, component_description } = data;

    const [state, setState] = useState({
        height: undefined
    })

    useEffect(() => {
        _getScal();
    }, [component_url]);

    const _getScal = async () => {
        if (!component_url) return;
        const { height } = await ImageScal(data.component_url)
        if (height)
            setState({ ...state, height })
    }
    if(component_title === "" && component_description === "" && component_url === ""){
        return null;
    }

    return (
        <View>
            <ImageBackground
                source={{ uri: component_url }}
                style={{ height: global.height, width: "100%" }}
                resizeMode="cover"
            >
                <View style={{ justifyContent: "center", alignItems: "center", flex: 1 }}>
                    <Text
                        style={{ fontSize: 22, textAlign: "center" }}
                    >
                        {component_title}
                    </Text>
                    <Text style={{ fontSize: 16, textAlign: "center", }}>
                        {component_description}
                    </Text>
                </View>
            </ImageBackground>
        </View>
    )
}
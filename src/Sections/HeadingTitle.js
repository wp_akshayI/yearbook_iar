import React from "react";
import { View, Text } from "react-native";

export default function HeadingTitle({ data }) {
    const { component_heading } = data;
    return (
        <View style={{ flex: 1, paddingHorizontal: 10, paddingVertical: 10 }}>
            <Text
                ellipsizeMode="tail"
                style={{
                    color: "#000",
                    fontSize: 18,
                    fontWeight: "bold",
                    flexWrap: "wrap"
                }}>
                {component_heading}
            </Text>
        </View>
    )
}
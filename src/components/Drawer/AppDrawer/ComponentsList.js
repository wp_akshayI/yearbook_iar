import React from 'react'
import DrawerList from '../DrawerList'
import List from '@material-ui/core/List'
import { global, colors } from '../../../utils'
import DrawerBacknav from '../../DrawerBacknav'
import AddIcon from '@material-ui/icons/Add'
import IconButton from '@material-ui/core/IconButton'

function ComponentsList ({ obj }) {
  let sections = global.components
  sections = sections.filter(item => item.component_type !== 9)

  const getComponent = (type) => {
    const component = global.components.filter(i => i.component_type === type)
    if (!component && component.length === 0) return
    obj._addSection(component[0])
  }

  return (
    <div>
      <List style={{ padding: 0 }}>
        <DrawerBacknav
          title='Add new Section'
          backClick={() => obj.setSelectionDrawer()}
        />
        {
          sections && sections.map((item, index) => {
            return (
              <DrawerList
                title={(index + 1) + '. ' + item.name}
                key={item.component_type}
                right={
                  <IconButton onClick={() => getComponent(item.component_type)}>
                    <AddIcon style={{ color: colors.white, fontSize: 18 }} />
                  </IconButton>
                }
              />
            )
          })
        }
      </List>

    </div>
  )
}

export default ComponentsList

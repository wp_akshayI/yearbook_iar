package com.app.yearbook.model.employee;

import com.google.gson.annotations.SerializedName;

public class SchoolListItem{

	@SerializedName("school_status")
	private String schoolStatus;

	@SerializedName("school_name")
	private String schoolName;

	@SerializedName("admin_type")
	private String adminType;

	@SerializedName("school_created")
	private String schoolCreated;

	@SerializedName("school_city")
	private String schoolCity;

	@SerializedName("school_state")
	private String schoolState;

	@SerializedName("school_end_date")
	private String schoolEndDate;

	@SerializedName("school_start_date")
	private String schoolStartDate;

	@SerializedName("password")
	private String password;

	@SerializedName("school_is_deleted")
	private String schoolIsDeleted;

	@SerializedName("school_safety")
	private String schoolSafety;

	@SerializedName("school_id")
	private String schoolId;

	@SerializedName("school_address")
	private String schoolAddress;

	@SerializedName("school_phone")
	private String schoolPhone;

	@SerializedName("school_is_suspend")
	private String schoolIsSuspend;

	@SerializedName("email")
	private String email;

	@SerializedName("yearbook_price")
	private String yearbookPrice;

	public void setSchoolStatus(String schoolStatus){
		this.schoolStatus = schoolStatus;
	}

	public String getSchoolStatus(){
		return schoolStatus;
	}

	public void setSchoolName(String schoolName){
		this.schoolName = schoolName;
	}

	public String getSchoolName(){
		return schoolName;
	}

	public void setAdminType(String adminType){
		this.adminType = adminType;
	}

	public String getAdminType(){
		return adminType;
	}

	public void setSchoolCreated(String schoolCreated){
		this.schoolCreated = schoolCreated;
	}

	public String getSchoolCreated(){
		return schoolCreated;
	}

	public void setSchoolCity(String schoolCity){
		this.schoolCity = schoolCity;
	}

	public String getSchoolCity(){
		return schoolCity;
	}

	public void setSchoolState(String schoolState){
		this.schoolState = schoolState;
	}

	public String getSchoolState(){
		return schoolState;
	}

	public void setSchoolEndDate(String schoolEndDate){
		this.schoolEndDate = schoolEndDate;
	}

	public String getSchoolEndDate(){
		return schoolEndDate;
	}

	public void setSchoolStartDate(String schoolStartDate){
		this.schoolStartDate = schoolStartDate;
	}

	public String getSchoolStartDate(){
		return schoolStartDate;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setSchoolIsDeleted(String schoolIsDeleted){
		this.schoolIsDeleted = schoolIsDeleted;
	}

	public String getSchoolIsDeleted(){
		return schoolIsDeleted;
	}

	public void setSchoolSafety(String schoolSafety){
		this.schoolSafety = schoolSafety;
	}

	public String getSchoolSafety(){
		return schoolSafety;
	}

	public void setSchoolId(String schoolId){
		this.schoolId = schoolId;
	}

	public String getSchoolId(){
		return schoolId;
	}

	public void setSchoolAddress(String schoolAddress){
		this.schoolAddress = schoolAddress;
	}

	public String getSchoolAddress(){
		return schoolAddress;
	}

	public void setSchoolPhone(String schoolPhone){
		this.schoolPhone = schoolPhone;
	}

	public String getSchoolPhone(){
		return schoolPhone;
	}

	public void setSchoolIsSuspend(String schoolIsSuspend){
		this.schoolIsSuspend = schoolIsSuspend;
	}

	public String getSchoolIsSuspend(){
		return schoolIsSuspend;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setYearbookPrice(String yearbookPrice){
		this.yearbookPrice = yearbookPrice;
	}

	public String getYearbookPrice(){
		return yearbookPrice;
	}

	@Override
 	public String toString(){
		return 
			"SchoolListItem{" + 
			"school_status = '" + schoolStatus + '\'' + 
			",school_name = '" + schoolName + '\'' + 
			",admin_type = '" + adminType + '\'' + 
			",school_created = '" + schoolCreated + '\'' + 
			",school_city = '" + schoolCity + '\'' + 
			",school_state = '" + schoolState + '\'' + 
			",school_end_date = '" + schoolEndDate + '\'' + 
			",school_start_date = '" + schoolStartDate + '\'' + 
			",password = '" + password + '\'' + 
			",school_is_deleted = '" + schoolIsDeleted + '\'' + 
			",school_safety = '" + schoolSafety + '\'' + 
			",school_id = '" + schoolId + '\'' + 
			",school_address = '" + schoolAddress + '\'' + 
			",school_phone = '" + schoolPhone + '\'' + 
			",school_is_suspend = '" + schoolIsSuspend + '\'' + 
			",email = '" + email + '\'' + 
			",yearbook_price = '" + yearbookPrice + '\'' + 
			"}";
		}
}

package com.app.yearbook.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StaffUser {

    @SerializedName("staff_id")
    @Expose
    private String staffId;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("staff_phone")
    @Expose
    private String staffPhone;
    @SerializedName("staff_email")
    @Expose
    private String staffEmail;
    @SerializedName("staff_password")
    @Expose
    private String staffPassword;
    @SerializedName("staff_device_type")
    @Expose
    private String staffDeviceType;
    @SerializedName("staff_device_token")
    @Expose
    private String staffDeviceToken;
    @SerializedName("staff_access_code")
    @Expose
    private String staffAccessCode;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("staff_request_status")
    @Expose
    private String staffRequestStatus;

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getStaffPhone() {
        return staffPhone;
    }

    public void setStaffPhone(String staffPhone) {
        this.staffPhone = staffPhone;
    }

    public String getStaffEmail() {
        return staffEmail;
    }

    public void setStaffEmail(String staffEmail) {
        this.staffEmail = staffEmail;
    }

    public String getStaffPassword() {
        return staffPassword;
    }

    public void setStaffPassword(String staffPassword) {
        this.staffPassword = staffPassword;
    }

    public String getStaffDeviceType() {
        return staffDeviceType;
    }

    public void setStaffDeviceType(String staffDeviceType) {
        this.staffDeviceType = staffDeviceType;
    }

    public String getStaffDeviceToken() {
        return staffDeviceToken;
    }

    public void setStaffDeviceToken(String staffDeviceToken) {
        this.staffDeviceToken = staffDeviceToken;
    }

    public String getStaffAccessCode() {
        return staffAccessCode;
    }

    public void setStaffAccessCode(String staffAccessCode) {
        this.staffAccessCode = staffAccessCode;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getStaffRequestStatus() {
        return staffRequestStatus;
    }

    public void setStaffRequestStatus(String staffRequestStatus) {
        this.staffRequestStatus = staffRequestStatus;
    }

}

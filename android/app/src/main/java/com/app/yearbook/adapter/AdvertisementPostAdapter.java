package com.app.yearbook.adapter;

import android.app.Activity;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.yearbook.R;
import com.app.yearbook.databinding.ItemAdvertisementBinding;
import com.app.yearbook.model.staff.livefeed.PostListItem;

import java.util.ArrayList;

public class AdvertisementPostAdapter extends RecyclerView.Adapter<AdvertisementPostAdapter.MyViewHolder> {
    private ArrayList<PostListItem> userLists;
    private Activity ctx;
    ItemAdvertisementBinding binding;

    private ListAdapterListener mListener;

    public interface ListAdapterListener { // create an interface
        void onClickAtOKButton(int position); // create callback function
    }


    @NonNull
    @Override
    public AdvertisementPostAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                , R.layout.item_advertisement, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdvertisementPostAdapter.MyViewHolder holder, final int position) {
        holder.binding.setPost(userLists.get(position));

        //post img
        if (holder.binding.getPost().getPostFile() != null || !holder.binding.getPost().getPostFile().equals("")) {
                if (holder.binding.getPost().getPostFile() != null) {
                    Glide.with(ctx)
                            .load(holder.binding.getPost().getPostFile())
                            .asBitmap()  .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.mipmap.home_post_img_placeholder)
                            .error(R.mipmap.home_post_img_placeholder)
                            .into(holder.binding.imgPost);
                }
        }

        holder.binding.imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClickAtOKButton(Integer.parseInt(holder.binding.getPost().getPostId()));
            }
        });
    }

    public AdvertisementPostAdapter(ArrayList<PostListItem> userLists, Activity context, ListAdapterListener mListener) {
        this.userLists = userLists;
        this.ctx = context;
        this.mListener=mListener;
    }

    @Override
    public int getItemCount() {
        return userLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ItemAdvertisementBinding binding;

        public MyViewHolder(ItemAdvertisementBinding view) {
            super(view.getRoot());
            binding=view;

        }
    }
}

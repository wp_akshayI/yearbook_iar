package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.app.yearbook.model.profile.getUserProfile;
import com.app.yearbook.model.studenthome.PostList;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUserTagPhotoActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private String schoolId, userId;
    public ProgressDialog progressDialog;
    private AllMethods allMethods;
    private RecyclerView rvUserPost;
    private LinearLayout lvNoData;
    private ArrayList<PostList> getUserList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user_tag_photo);

        setView();
    }

    public void setView() {
        //toolbar
        setToolbar();

        //intent
        if (getIntent().getExtras() != null) {
            userId = getIntent().getStringExtra("userId");
            schoolId = getIntent().getStringExtra("schoolId");
        }

        //all methods object
        allMethods = new AllMethods();

        //lv no
        lvNoData = findViewById(R.id.lvNoData);

        //rv
        rvUserPost = findViewById(R.id.rvUserPost);

        getUserPost();

    }
    public void getUserPost() {
        //Progress bar
        progressDialog = ProgressDialog.show(SearchUserTagPhotoActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        Log.d("TTT","Search user tag photo: "+userId+" / "+schoolId);
        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        final Call<getUserProfile> userPost = retrofitClass.getUserTag(Integer.parseInt(userId),Integer.parseInt(schoolId));
        userPost.enqueue(new Callback<getUserProfile>() {
            @Override
            public void onResponse(Call<getUserProfile> call, Response<getUserProfile> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                getUserList = new ArrayList<>();
                if (response.body().getResponseCode().equals("1")) {
                    if (response.body().getPostList().size() > 0) {
                        rvUserPost.setVisibility(View.VISIBLE);
                        lvNoData.setVisibility(View.GONE);
                        getUserList.addAll(response.body().getPostList());
                        GridLayoutManager mLayoutManager = new GridLayoutManager(SearchUserTagPhotoActivity.this, 3);
                        rvUserPost.setLayoutManager(mLayoutManager);
//                        UserTagPostAdapter mAdapter = new UserTagPostAdapter(getUserList, SearchUserTagPhotoActivity.this);
//                        rvUserPost.setAdapter(mAdapter);

                    } else {
                        rvUserPost.setVisibility(View.GONE);
                        lvNoData.setVisibility(View.VISIBLE);
                    }
                }
                else if(response.body().getResponseCode().equals("10")) {
                    Toast.makeText(SearchUserTagPhotoActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(SearchUserTagPhotoActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(SearchUserTagPhotoActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    rvUserPost.setVisibility(View.GONE);
                    lvNoData.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(Call<getUserProfile> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(SearchUserTagPhotoActivity.this, "", t.getMessage() + "");
            }
        });

    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title=toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Tagged Photos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

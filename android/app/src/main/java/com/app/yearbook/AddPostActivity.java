package com.app.yearbook;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.doodle.android.chips.ChipsView;
import com.doodle.android.chips.model.Contact;
import com.github.ybq.android.spinkit.style.Circle;
import com.volokh.danylo.hashtaghelper.HashTagHelper;
import com.app.yearbook.fragment.StaffHomeFragment;
import com.app.yearbook.model.library.AddPost;
import com.app.yearbook.model.search.User;
import com.app.yearbook.model.staff.home.EditStaffPost;
import com.app.yearbook.model.staff.livefeed.PostListItem;
import com.app.yearbook.model.staff.yearbook.UserTagg;
import com.app.yearbook.restclient.APIClient;
import com.app.yearbook.restclient.RetrofitClass;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.app.yearbook.utils.AllMethods;
import com.app.yearbook.utils.BitmapUtils;
import com.app.yearbook.videocompress.CompressListener;
import com.app.yearbook.videocompress.Compressor;
import com.app.yearbook.videocompress.InitListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tcking.github.com.giraffeplayer2.PlayerManager;
import tcking.github.com.giraffeplayer2.VideoInfo;


public class AddPostActivity extends AppCompatActivity implements HashTagHelper.OnHashTagClickListener {

    private Toolbar toolbar;
    private ImageView imgPostPhoto;
    private tcking.github.com.giraffeplayer2.VideoView videoSelected;
    private EditText edtPostMsg, edtPostTitle;
    private Uri getUri;
    private Bitmap thumbImage;
    private LoginUser loginUser;
    private String getUserId, getSchoolId, getTitle = "", getMsg = "", getType = "", getPostId = "", getDesc = "", tagUserList = "", getUrl = "", opration = "", getVideoUrl;
    private String cmd, currentOutputVideoPath = "/mnt/sdcard/videokit/out.mp4", getThumb = "";
    private MultipartBody.Part part, thumbPart = null;
    private ProgressDialog progressDialog;
    private File fileImage = null, fileVideo = null;
    private AllMethods allMethods;
    private RequestBody rbFile = null, rbFileThumb = null, type;
    private HashTagHelper mTextHashTagHelper;
    private HashTagHelper mEditTextHashTagHelper;
    private ChipsView mChipsView;
    private LinearLayout lvTag, lvTagPeople;
    private int IntentCode = 654;
    private ArrayList<UserTagg> userTaggs ;
    private List<String> tagUser = new ArrayList<>();
    //Bitmap bMap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);

        setView();
    }

    public void setView() {

        //common methods
        allMethods = new AllMethods();

        //user tag
        userTaggs = new ArrayList<>();

        //toolbar
        setToolbar();

        //img
        imgPostPhoto = findViewById(R.id.imgPost);

        //video
        videoSelected = findViewById(R.id.videoSelected);

        //et msg
        edtPostMsg = findViewById(R.id.edtPostMsg);
        edtPostTitle = findViewById(R.id.edtPostTitle);

        //hash tag
        mEditTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.hashtag), null);
        mEditTextHashTagHelper.handle(edtPostMsg);

        //chip view
        mChipsView = findViewById(R.id.chipsView);

        //lv for tag ppl name
        lvTag = findViewById(R.id.lvTag);
        lvTagPeople = findViewById(R.id.lvTagPeople);

        lvTagPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddPostActivity.this, SearchUserActivity.class);
                i.putExtra("Type", "student");
                startActivityForResult(i, IntentCode);
            }
        });

        //get Post (Add)
        if (getIntent().getExtras() != null) {
            if (getIntent().getStringExtra("Edit") != null) {
                if (getIntent().getStringExtra("Edit").equalsIgnoreCase("Edit")) {
                    opration = "Edit";

                    //edit post
                    getPostId = getIntent().getStringExtra("PostId");
                    getDesc = getIntent().getStringExtra("Description");
                    getUrl = getIntent().getStringExtra("postUrl");
                    getThumb = getIntent().getStringExtra("Thumb");
                    getTitle = getIntent().getStringExtra("Title");
                    userTaggs = (ArrayList<UserTagg>) getIntent().getSerializableExtra("tagUserId");

                    //set description
                    Log.d("TTT", "Postttttt: " + getPostId + " / " + userTaggs.size());
                    edtPostMsg.setText(getDesc);
                    edtPostTitle.setText(getTitle);

                    //set image or video
                    String extension = getUrl.substring(getUrl.lastIndexOf("."));
                    if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {
                        imgPostPhoto.setVisibility(View.VISIBLE);
                        videoSelected.setVisibility(View.GONE);

                        Glide.with(getApplicationContext())
                                .load(getUrl)
                                .asBitmap()
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .placeholder(R.mipmap.home_post_img_placeholder)
                                .error(R.mipmap.home_post_img_placeholder)
                                .into(imgPostPhoto);
                    } else {
                        imgPostPhoto.setVisibility(View.GONE);
                        videoSelected.setVisibility(View.VISIBLE);

//                        videoSelected.reset();
//                        videoSelected.setSource(Uri.parse(getUrl));
//                        videoSelected.start();

                        if (videoSelected.getCoverView() != null) {
                            Glide.with(AddPostActivity.this)
                                    .load(getThumb)
                                    .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                    .placeholder(R.mipmap.home_post_img_placeholder)
                                    .error(R.mipmap.home_post_img_placeholder)
                                    .into(videoSelected.getCoverView());
                        }

                        videoSelected.getVideoInfo().setPortraitWhenFullScreen(false);
                        videoSelected.setVideoPath(getUrl);
                        videoSelected.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player

                    }

                    //setTag user
                    Log.d("TTT", "Sizeee: " + userTaggs.size());
                    if (userTaggs != null && userTaggs.size() > 0) {
                        lvTag.setVisibility(View.GONE);
                        mChipsView.setVisibility(View.VISIBLE);

                        for (int i = 0; i < userTaggs.size(); i++) {
                            Contact c = new Contact(userTaggs.get(i).getUserFirstname(), userTaggs.get(i).getUserLastname(), userTaggs.get(i).getUserId(), "", null);
                            mChipsView.addChip(userTaggs.get(i).getUserFirstname() + " " + userTaggs.get(i).getUserLastname(), "", c);
                        }
                    }

                }

            } else {
                lvTagPeople.setVisibility(View.VISIBLE);
                opration = "Add";
                //add post
                getType = getIntent().getStringExtra("Type");
                if (getType.equalsIgnoreCase("Image")) {
                    imgPostPhoto.setVisibility(View.VISIBLE);
                    videoSelected.setVisibility(View.GONE);

                    getUri = Uri.parse(getIntent().getStringExtra("AddPostImage"));
                    try {
                        thumbImage = MediaStore.Images.Media.getBitmap(getContentResolver(), getUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    imgPostPhoto.setImageURI(getUri);
                    //imgPostPhoto.setImageBitmap(Bitmap.createScaledBitmap(thumbImage, 640, 640, false));

                    //image upload
                    //get file from uri
                    fileImage = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), getUri));

                    RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
                    part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);
                    rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());

                    //set type
                    type = RequestBody.create(MediaType.parse("text/plain"), "1");
                } else if (getType.equalsIgnoreCase("Video")) {
                    imgPostPhoto.setVisibility(View.GONE);
                    videoSelected.setVisibility(View.VISIBLE);

                    // file:///mnt/sdcard/videokit/out.mp4
                    getVideoUrl = getIntent().getStringExtra("selectUri");
                    Log.d("TTT", "New URL: " + getVideoUrl);
                    getUri = Uri.parse(getVideoUrl);

//                    videoSelected.reset();
//                    videoSelected.setSource(getUri);

                    //for video
                    fileImage = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), getUri));
                    RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
                    part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);
                    rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());

                    //for thumbnail
                    Bitmap bMap = ThumbnailUtils.createVideoThumbnail(fileImage.getAbsolutePath(), MediaStore.Video.Thumbnails.MINI_KIND);
                    fileVideo = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), AllMethods.getImageUri(getApplicationContext(), bMap)));
                    RequestBody filePostThumb = RequestBody.create((MediaType.parse("*/*")), fileVideo);
                    thumbPart = MultipartBody.Part.createFormData("post_thumbnail", fileVideo.getName(), filePostThumb);
                    rbFileThumb = RequestBody.create(MediaType.parse("text/plain"), fileVideo.getName());

                    //clear thumbnail
//                    AsyncTask<String, String, Bitmap> task = null;
//
//                    try {
//                        Uri path=Uri.parse(getIntent().getStringExtra("path"));
//                        File fileThumbImage = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), path));
//                        task = new abv(fileThumbImage.getAbsolutePath());
//                        task.execute();
//                    } catch (Throwable throwable) {
//                        throwable.printStackTrace();
//                    }

//                    if (bMap != null) {
//                        bMap = Bitmap.createScaledBitmap(bMap, 240, 240, false);
//                    }

                    //for thump nail
                    //Bitmap bMap=(Bitmap) getIntent().getExtras().getParcelable("Bitmapthumb");

//                    Bitmap bMap = getIntent().getParcelableExtra("Bitmapthumb");
//
//                    Log.d("TTT","BMAP: "+bMap);
                    videoSelected.getVideoInfo().setPortraitWhenFullScreen(false);
                    videoSelected.setVideoPath(getVideoUrl);
                    videoSelected.getVideoInfo().setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT);//config player
                    videoSelected.getPlayer().start();

                    //set type
                    type = RequestBody.create(MediaType.parse("text/plain"), "2");
                } else {
                    allMethods.setAlert(getApplicationContext(), getResources().getString(R.string.app_name), "Something wrong, please try again");
                }
            }

        }

        //SP object
        loginUser = new LoginUser(AddPostActivity.this);
        if (loginUser.getUserData() != null) {
            getUserId = loginUser.getUserData().getStaffId();
            getSchoolId = loginUser.getUserData().getSchoolId();
            Log.d("TTT", "setView: " + getUserId);
        }

        //chip view listner
        mChipsView.setChipsListener(new ChipsView.ChipsListener() {
            @Override
            public void onChipAdded(ChipsView.Chip chip) {
                // chip added
            }

            @Override
            public void onChipDeleted(ChipsView.Chip chip) {
                // chip deleted
                Log.d("TTT", "Chip delete: " + chip.getContact().getDisplayName());
                String id = chip.getContact().getDisplayName();

                for (int i = 0; i < userTaggs.size(); i++) {
                    if (userTaggs.get(i).getUserId().equalsIgnoreCase(id)) {
                        userTaggs.remove(i);
                    }
                }

                if (userTaggs.size() == 0) {

                    mChipsView.setVisibility(View.GONE);
                    lvTag.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence text) {
                text = null;
                // text was changed
            }

            @Override
            public boolean onInputNotRecognized(String text) {
                // return true to delete the input
                return false; // keep the typed text
            }
        });

    }

    //toolbar
    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Post to Yearbook");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(5f);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    public class abv extends AsyncTask<String, String, Bitmap> {
        private String data;

        public abv(String data) {
            this.data = data;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bMap = null;
            try {
                bMap = retriveVideoFrameFromVideo(data);
                if (bMap != null) {
                    //int width = View.MeasureSpec.getSize(widthMeasureSpec);
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    int width = displayMetrics.widthPixels;
                    //387
                    bMap = Bitmap.createScaledBitmap(bMap, 500, 500, false);
                    Log.d("TTT", "doInBackground: " + bMap);
                }

            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

            return bMap;
        }

        @Override
        protected void onPostExecute(Bitmap aLong) {
            super.onPostExecute(aLong);

            Log.d("TTT", "Task: " + aLong);
            fileVideo = new File(BitmapUtils.getFilePathFromUri(getApplicationContext(), AllMethods.getImageUri(getApplicationContext(), aLong)));
            RequestBody filePostThumb = RequestBody.create((MediaType.parse("*/*")), fileVideo);
            thumbPart = MultipartBody.Part.createFormData("post_thumbnail", fileVideo.getName(), filePostThumb);
            rbFileThumb = RequestBody.create(MediaType.parse("text/plain"), fileVideo.getName());

        }
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();

            if (Build.VERSION.SDK_INT >= 14) {
                Log.d("TTT", "method: " + videoPath);
                FileInputStream inputStream = new FileInputStream(videoPath);
                mediaMetadataRetriever.setDataSource(inputStream.getFD());
            } else
                mediaMetadataRetriever.setDataSource(videoPath);


            String s = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
            Log.e("Rotation", s);
            bitmap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST);


            Log.d("TTT", "Pathhh: " + mediaMetadataRetriever.getFrameAtTime());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("TTT", "exception: " + e.getMessage());
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        Log.d("TTT", "Method bitmap: " + bitmap);
        return bitmap;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentCode && resultCode == RESULT_OK) {
            if (data.getExtras() != null) {
                User user = (User) data.getSerializableExtra("SearchUser");

                mChipsView.setVisibility(View.VISIBLE);
                lvTag.setVisibility(View.GONE);
                if (userTaggs.size() > 0) {
                    String flag = "add";
                    for (int i = 0; i < userTaggs.size(); i++) {
                        if (userTaggs.get(i).getUserId().equals(user.getUserId())) {
                            flag = "notadd";
                        }
                    }
                    if (flag.equals("add")) {
                        UserTagg userTagg = new UserTagg();
                        userTagg.setUserId(user.getUserId());
                        userTagg.setUserFirstname(user.getUserFirstname());
                        userTagg.setUserLastname(user.getUserLastname());
                        userTaggs.add(userTagg);
                        Contact c = new Contact(user.getUserFirstname(), user.getUserLastname(), user.getUserId(), "", null);
                        mChipsView.addChip(user.getUserFirstname() + " " + user.getUserLastname(), "", c);
                    }
                } else {
                    UserTagg userTagg = new UserTagg();
                    userTagg.setUserId(user.getUserId());
                    userTagg.setUserFirstname(user.getUserFirstname());
                    userTagg.setUserLastname(user.getUserLastname());

                    userTaggs.add(userTagg);
                    Contact c = new Contact(user.getUserFirstname(), user.getUserLastname(), user.getUserId(), "", Uri.parse(user.getUserImage()));
                    mChipsView.addChip(user.getUserFirstname() + " " + user.getUserLastname(), Uri.parse(user.getUserImage()), c);
                }
                Log.d("TTT", "Sizeee: " + userTaggs.size());
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (PlayerManager.getInstance().onBackPressed()) {
            return;
        }
        super.onBackPressed();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.next) {
            if (opration.equalsIgnoreCase("edit")) {
                if (edtPostTitle.getText().toString().equals("")) {
                    allMethods.setAlert(AddPostActivity.this, "", "Please enter title");
                } else {
                    EditPost();
                }
            } else {
                // newCompress(getVideoUrl);

                if (getType.equalsIgnoreCase("Video")) {
                    if (PlayerManager.getInstance().getCurrentPlayer() != null) {
                        PlayerManager.getInstance().getCurrentPlayer().stop();
                    }
                }

                if (edtPostTitle.getText().toString().equals("")) {
                    allMethods.setAlert(AddPostActivity.this, "", "Please enter title");
                } else {
                    AddPost();
                }
            }
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selectimage, menu);
        MenuItem next = menu.findItem(R.id.next);
        next.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }

    //function for add post
    public void AddPost() {

        Log.d("TTT", "Total size: " + userTaggs.size());
        tagUser.clear();
        for (int i = 0; i < userTaggs.size(); i++) {
            tagUser.add(userTaggs.get(i).getUserId());
        }

        tagUserList = "";
        if (tagUser.size() > 0) {
            tagUserList = TextUtils.join(",", tagUser);
        }
        Log.d("TTT", "tagUserList: " + tagUserList);

        //Progress bar
        progressDialog = ProgressDialog.show(this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(getResources().getColor(R.color.gray));
        progressBar.setIndeterminateDrawable(bounce);

        getMsg = edtPostMsg.getText().toString();
        getTitle = edtPostTitle.getText().toString();

        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), getUserId);
        RequestBody tagguserId = RequestBody.create(MediaType.parse("text/plain"), tagUserList);
        RequestBody schoolId = RequestBody.create(MediaType.parse("text/plain"), getSchoolId);
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), getMsg);
        RequestBody title = RequestBody.create(MediaType.parse("text/plain"), getTitle);
        RequestBody timestamp = RequestBody.create(MediaType.parse("text/plain"), Calendar.getInstance().getTimeInMillis()+"");

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<AddPost> login = retrofitClass.addStaffPost(timestamp, userId, schoolId, title, desc, part, rbFile, type, thumbPart, tagguserId, rbFileThumb);
        login.enqueue(new Callback<AddPost>() {
            @Override
            public void onResponse(Call<AddPost> call, Response<AddPost> response) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddPostActivity.this);
                    builder.setMessage(response.body().getResponseMsg())
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    Intent i = getIntent();
                                    setResult(RESULT_OK, i);
                                    finish();
                                }
                            });

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    alert.setTitle("");
                    alert.show();
                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(AddPostActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(AddPostActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(AddPostActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Log.d("TTT", "Msgggg: " + response.body().getResponseMsg());
                    allMethods.setAlert(AddPostActivity.this, "", response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<AddPost> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(AddPostActivity.this, getResources().getString(R.string.app_name), t.getMessage());
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if (progressDialog != null && progressDialog.isShowing()) {
//            progressDialog.dismiss();
//        }
    }

    //function for edit post
    public void EditPost() {

        Log.d("TTT", "Total size: " + userTaggs.size());
        tagUser.clear();
        for (int i = 0; i < userTaggs.size(); i++) {
            tagUser.add(userTaggs.get(i).getUserId());
        }

        tagUserList = "";
        if (tagUser.size() > 0) {
            tagUserList = TextUtils.join(",", tagUser);
        }
        Log.d("TTT", "tagUserList: " + tagUserList);

        //Progress bar
        progressDialog = ProgressDialog.show(AddPostActivity.this, "", "", true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_view);
        ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
        Circle bounce = new Circle();
        bounce.setColor(Color.BLACK);
        progressBar.setIndeterminateDrawable(bounce);

        String msg = edtPostMsg.getText().toString();
        Log.d("TTT", "postttt id: " + getPostId + " / " + msg);
        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<EditStaffPost> editPost = retrofitClass.editStaffPost(Calendar.getInstance().getTimeInMillis()+"", Integer.parseInt(getPostId), msg, tagUserList);
        editPost.enqueue(new Callback<EditStaffPost>() {
            @Override
            public void onResponse(Call<EditStaffPost> call, final Response<EditStaffPost> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.body().getResponseCode().equals("1")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddPostActivity.this);
                    builder.setMessage(response.body().getResponseMsg())
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    //home
                                    if (StaffHomeFragment.PostListArray != null) {
                                        for (int i = 0; i < StaffHomeFragment.PostListArray.size(); i++) {
                                            if (StaffHomeFragment.PostListArray.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
                                                PostListItem newList = response.body().getPostDetail().get(0);
                                                StaffHomeFragment.PostListArray.get(i).setPostDescription(newList.getPostDescription());
//                                                StaffHomeFragment.PostListArray.get(i).setUserTagg(newList.getUserTagg());
                                                StaffHomeFragment.postListAdapter.notifyItemChanged(i);
                                                Log.d("TTT", "New desc val: " + StaffHomeFragment.PostListArray.get(i).getPostDescription() + " / " + StaffHomeFragment.PostListArray.get(i).getUserTagg());
                                            }
                                        }
                                    }

//                                    //profile fragment
//                                    if (ProfileFragment.getUserList != null) {
//                                        for (int i = 0; i < ProfileFragment.getUserList.size(); i++) {
//                                            if (ProfileFragment.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
//                                                PostList newList = response.body().getPostDetail().get(0);
//                                                ProfileFragment.getUserList.get(i).setPostDescription(newList.getPostDescription());
//                                                ProfileFragment.getUserList.get(i).setUserTagg(newList.getUserTagg());
//                                            }
//                                        }
//                                    }

//                                    //bookmark
//                                    if (StudentBookmarkActivity.bookmarkList != null) {
//                                        for (int i = 0; i < StudentBookmarkActivity.bookmarkList.size(); i++) {
//                                            if (StudentBookmarkActivity.bookmarkList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
//                                                PostList newList = response.body().getPostDetail().get(0);
//                                                StudentBookmarkActivity.bookmarkList.get(i).setPostDescription(newList.getPostDescription());
//                                                StudentBookmarkActivity.bookmarkList.get(i).setUserTagg(newList.getUserTagg());
//                                            }
//                                        }
//                                    }
//
//                                    //user detail
//                                    if (UserDetailActivity.getUserList != null) {
//                                        for (int i = 0; i < UserDetailActivity.getUserList.size(); i++) {
//                                            if (UserDetailActivity.getUserList.get(i).getPostId().equals(response.body().getPostDetail().get(0).getPostId())) {
//                                                PostList newList = response.body().getPostDetail().get(0);
//                                                UserDetailActivity.getUserList.get(i).setPostDescription(newList.getPostDescription());
//                                                UserDetailActivity.getUserList.get(i).setUserTagg(newList.getUserTagg());
//                                            }
//                                        }
//                                    }

                                    Intent i = new Intent();
                                    i.putExtra("Description", response.body().getPostDetail().get(0).getPostDescription());
                                    i.putExtra("taggedUser", (ArrayList<UserTagg>) response.body().getPostDetail().get(0).getUserTagg());
                                    setResult(RESULT_OK, i);
                                    finish();
                                }
                            });

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    alert.setTitle("");
                    alert.show();
                } else if (response.body().getResponseCode().equals("10")) {
                    Toast.makeText(AddPostActivity.this, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    LoginUser loginSP = new LoginUser(AddPostActivity.this);
                    loginSP.clearData();

                    Intent i = new Intent(AddPostActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    allMethods.setAlert(AddPostActivity.this, "", response.body().getResponseMsg());
                }
            }

            @Override
            public void onFailure(Call<EditStaffPost> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                allMethods.setAlert(AddPostActivity.this, getResources().getString(R.string.app_name), t.getMessage());
            }
        });
    }


    @Override
    public void onHashTagClicked(String hashTag) {

    }

    public void newCompress(String path) {
        //mProgressDialog.show();
        File file = new File("/mnt/sdcard/videokit");
        if (!file.exists()) {
            file.mkdirs();
        }

//        cmd = "-y -i " + path + " -strict -2 -vcodec libx264 -preset ultrafast " +
//                "-crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k -s 640x480 -aspect 16:9 " + currentOutputVideoPath;

//        cmd = "-y -i " + path + " -strict -2 -vcodec libx264 -preset ultrafast " +
//                "-crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k " + currentOutputVideoPath;

        cmd = "-y -i " + path + " -strict experimental -vcodec libx264 -preset ultrafast " +
                "-crf 24 -acodec aac -ar 44100 -ac 2 -r 10 -b:a 96k " + currentOutputVideoPath;

        Log.d("TTT", "Pathhh: " + cmd);

        if (TextUtils.isEmpty(cmd)) {
            Toast.makeText(AddPostActivity.this, "please input command"
                    , Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(path)) {
            Toast.makeText(AddPostActivity.this, "video file not found, please record first", Toast.LENGTH_SHORT).show();
        } else {
            progressDialog = ProgressDialog.show(this, "", "", true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.progress_view);
            ProgressBar progressBar = progressDialog.findViewById(R.id.progress);
            Circle bounce = new Circle();
            bounce.setColor(getResources().getColor(R.color.gray));
            progressBar.setIndeterminateDrawable(bounce);

            file = new File(currentOutputVideoPath);
            if (file.exists()) {
                file.delete();
            }
            execCommand(cmd);
        }
    }

    private void execCommand(String cmd) {
        File mFile = new File(currentOutputVideoPath);
        if (mFile.exists()) {
            mFile.delete();
        }
        Compressor mCompressor = new Compressor(this);
        mCompressor.loadBinary(new InitListener() {
            @Override
            public void onLoadSuccess() {
                Log.v("TTT", "load library succeed");
            }

            @Override
            public void onLoadFail(String reason) {
                Log.i("TTT", "load library fail:" + reason);
            }
        });

        mCompressor.execCommand(cmd, new CompressListener() {
            @Override
            public void onExecSuccess(String message) {
                //  mProgressDialog.cancel();
                Log.i("TTT", "success " + message);

//                File f=new File(currentOutputVideoPath);
//                Log.d("TTT","New file: "+ Uri.fromFile(f));


                //for video
                fileImage = new File(currentOutputVideoPath);
                RequestBody filePost = RequestBody.create((MediaType.parse("*/*")), fileImage);
                part = MultipartBody.Part.createFormData("post_file", fileImage.getName(), filePost);
                rbFile = RequestBody.create(MediaType.parse("text/plain"), fileImage.getName());

                long fileSizeInKB = fileImage.length() / 1024;
                Log.d("TTT", "New file size in KBBBB: " + fileSizeInKB);

                if (fileSizeInKB > 1000) {
                    long fileSizeInMB = fileSizeInKB / 1024;
                    Log.d("TTT", "New file size in MB: " + fileSizeInMB);
                } else {
                    Log.d("TTT", "New file size in KB: " + fileSizeInKB);
                }

                AddPost();

//                if (from.equalsIgnoreCase("staff")) {
//                    if (YearbookPost.equalsIgnoreCase("StaffPost")) {
//                        Intent i = new Intent();
//                        i.putExtra("Type", "Video");
//                        i.putExtra("selectUri", Uri.fromFile(f).toString());
//                        setResult(RESULT_OK, i);
//                        finish();
//                    } else {
//                        Intent i = new Intent(getApplicationContext(), AddYearbookPostActivity.class);
//                        i.putExtra("CategoryId", CategoryId);
//                        i.putExtra("Type", "Video");
//                        i.putExtra("selectUri", Uri.fromFile(f).toString());
//                        startActivityForResult(i, 222);
//                    }
//                } else if (from.equalsIgnoreCase("library")) {
//                    Intent i = new Intent(getApplicationContext(), AddPostActivity.class);
//                    i.putExtra("Type", "Video");
//                    i.putExtra("selectUri", Uri.fromFile(f).toString());
//                    startActivityForResult(i, 222);
//                }

            }

            @Override
            public void onExecFail(String reason) {
                //  mProgressDialog.cancel();
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Log.i("TTT", "fail: " + reason);

            }

            @Override
            public void onExecProgress(String message) {

                Log.i("TTT", "progress " + message);
                //Log.v("TTT",getString(R.string.compress_progress,getProgress(message)));
            }
        });
    }


}


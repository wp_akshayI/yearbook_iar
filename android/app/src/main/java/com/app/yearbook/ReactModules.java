package com.app.yearbook;

import com.app.yearbook.model.YBResponse;
import com.app.yearbook.sharedpreferance.LoginUser;
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.UiThreadUtil;
import com.facebook.react.uimanager.ViewManager;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//Defination of Native Module
public class ReactModules implements ReactPackage {

    public static YBResponse.YearbookList yearbookObjLastClicked = null;

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();
        modules.add(new AppModules(reactContext));
        return modules;
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }
}

// send login user information
class AppModules extends ReactContextBaseJavaModule {

    public AppModules(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "AppModules";
    }

    @ReactMethod
    public void getToken(Callback callback) {
        String Token = "", type = "";
        new LoginUser(getReactApplicationContext());
        if (LoginUser.getUserData() != null) {
            type = LoginUser.getUserData().getUserType();

            if (type.equalsIgnoreCase("student") || type.equalsIgnoreCase("yb_student")) {
//                    Id=LoginUser.getUserData().getUserId();
                Token = LoginUser.getUserData().getUserToken();
            } else if (type.equalsIgnoreCase("employee")) {
//                    Id=LoginUser.getUserData().getEmployeeId();
                Token = LoginUser.getUserData().getEmployeeToken();
            } else {
//                    Id=LoginUser.getUserData().getStaffId();
                Token = LoginUser.getUserData().getStaffToken();
            }

//                Log.d("TTT","Client Id: "+Token+" / "+Id+" / "+UserType);
        }
        String token = Token;
        //then take the token
//            Toast.makeText(getReactApplicationContext(), token, Toast.LENGTH_LONG).show();
        callback.invoke(token, type);
    }

    @ReactMethod
    public void getLastYearbookClicked(Callback callback) {
        String data = "";
        if (ReactModules.yearbookObjLastClicked != null){
            try {
                data = new Gson().toJson(ReactModules.yearbookObjLastClicked);
            }catch (Exception e){}
        }
        callback.invoke(data);
    }

    @ReactMethod
    public void backToYearbookList() {
        UiThreadUtil.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (ReactCode.instance != null)
                    ReactCode.instance.finish();

                /*if (StaffHomeActivity.instance != null) {
                    StaffHomeActivity.instance.openDrawer();
                } else {
                    StudentHomeActivity.instance.openDrawer();
                }*/

            }
        });
    }

    @ReactMethod
    public void openDrawer() {
        UiThreadUtil.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (StaffHomeActivity.instance != null) {
                    StaffHomeActivity.instance.openDrawer();
                } else {
                    StudentHomeActivity.instance.openDrawer();
                }
            }
        });
    }

    @ReactMethod
    public void backDialog() {
        UiThreadUtil.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (StaffHomeActivity.instance != null) {
                    StaffHomeActivity.instance.finishDialog();
                } else {
                    StudentHomeActivity.instance.finishDialog();
                }
            }
        });
    }
}
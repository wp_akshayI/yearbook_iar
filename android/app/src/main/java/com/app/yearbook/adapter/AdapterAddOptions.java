package com.app.yearbook.adapter;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.yearbook.R;
import com.app.yearbook.databinding.AdapterAddOptionBinding;
import com.app.yearbook.interfaces.OnRecyclerClick;
import com.app.yearbook.model.allpost.OptionsModel;

import java.util.List;

public class AdapterAddOptions extends RecyclerView.Adapter<AdapterAddOptions.OptionHolder> {

    List<OptionsModel> optionsModels;
    OnRecyclerClick onRecyclerClick;

    public AdapterAddOptions(List<OptionsModel> optionsModels, OnRecyclerClick onRecyclerClick) {
        this.optionsModels = optionsModels;
        this.onRecyclerClick = onRecyclerClick;
    }

    @NonNull
    @Override

    public OptionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterAddOptionBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_add_option, parent, false);
        return new OptionHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull OptionHolder holder, int position) {
        holder.binding.setModel(optionsModels.get(position));
        String hint = "Option";
        switch (position) {
            case 0:
                hint = (position + 1) + ". First option";
                break;
            case 1:
                hint = (position + 1) + ". Second option";
                break;
            case 2:
                hint = (position + 1) + ". Third option";
                break;
            case 3:
                hint = (position + 1) + ". Fourth option";
                break;
            case 4:
                hint = (position + 1) + ". Fifth option";
                break;
        }
        holder.binding.edtOption.setHint(hint);
        if (optionsModels.size() > 2 && !optionsModels.get(position).isEdited()) {
            holder.binding.btnRemove.setVisibility(View.VISIBLE);
            holder.binding.btnRemove.setClickable(true);
        } else {
            holder.binding.btnRemove.setVisibility(View.INVISIBLE);
            holder.binding.btnRemove.setClickable(false);
        }

        if (optionsModels.get(position).isEdited())
            holder.binding.edtOption.setEnabled(false);
        else
            holder.binding.edtOption.setEnabled(true);

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return optionsModels.size();
    }

    public class OptionHolder extends RecyclerView.ViewHolder implements View.OnClickListener,TextWatcher {

        AdapterAddOptionBinding binding;

        public OptionHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            binding.btnRemove.setOnClickListener(this);

           // binding.edtOption.addTextChangedListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == binding.btnRemove) {
                onRecyclerClick.onClick(getAdapterPosition(), 0);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //onRecyclerClick.onClick(getAdapterPosition(), 1);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}

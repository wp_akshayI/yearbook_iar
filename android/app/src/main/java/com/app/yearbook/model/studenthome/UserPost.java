
package com.app.yearbook.model.studenthome;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserPost {

    @SerializedName("user_list")
    @Expose
    private List<UserList> userList = null;
    @SerializedName("post_list")
    @Expose
    private List<PostList> postList = null;

    public List<UserList> getUserList() {
        return userList;
    }

    public void setUserList(List<UserList> userList) {
        this.userList = userList;
    }

    public List<PostList> getPostList() {
        return postList;
    }

    public void setPostList(List<PostList> postList) {
        this.postList = postList;
    }

}

import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import {
  TitleSubTitleSetting,
  HeadingTitleSetting,
  ImageSectionSetting,
  ImageTextOverlaySetting,
  PortraitSettings,
  VideoSectionSetting,
  InsideListSettings,
  InsideGridSettings
} from '../../../SectionsSettings'
import { global } from '../../../utils'
import DrawerBacknav from '../../DrawerBacknav'
import ImageGallerySettings from 'SectionsSettings/ImageGallerySettings'
import YBImageTitleSettings from 'SectionsSettings/YBImageTitleSettings'

function SectionSettingSidebar ({ obj, index }) {
  const classes = useStyles()
  const data = obj.state.settingsComponent
  const component = global.components.filter(i => i.component_type === data.component_type)

  return (
    <>
      <List style={{ padding: 0 }}>
        <DrawerBacknav
          title='Edit Section'
          backClick={() => obj.setSelectionDrawer()}
        />
        <ListItem
          button
          className={classes.ListItemBorder}
          classes={{ root: classes.ListItem }}
        >
          <ListItemText
            classes={{ primary: classes.ListItemText }}
            primary={(index + 1) + '. ' + component[0].name}
          />
        </ListItem>
      </List>
      {data.component_type === 9 && (
        <YBImageTitleSettings
          data={data}
          updateComponents={obj.ypUpdateComponents}
        />
      )}
      {data.component_type === 0 && (
        <TitleSubTitleSetting
          data={data}
          updateComponents={obj.updateComponents}
        />
      )}
      {data.component_type === 1 && (
        <HeadingTitleSetting
          data={data}
          updateComponents={obj.updateComponents}
        />
      )}
      {data.component_type === 2 && (
        <ImageSectionSetting
          data={data}
          updateComponents={obj.updateComponents}
        />
      )}
      {data.component_type === 3 && (
        <ImageTextOverlaySetting
          data={data}
          updateComponents={obj.updateComponents}
          obj={obj}
        />
      )}
      {data.component_type === 4 && (
        <VideoSectionSetting
          data={data}
          updateComponents={obj.updateComponents}
        />
      )}
      {data.component_type === 5 && (
        <PortraitSettings
          data={data}
          updateComponents={obj.updateComponents}
          obj={obj}
        />
      )}
      {data.component_type === 6 && (
        <InsideGridSettings
          screenId={obj.state.screen_id}
          name={obj.state.screenName}
          data={data}
          updateComponents={obj.updateComponents}
        />
      )}
      {data.component_type === 7 && (
        <InsideListSettings
          screenId={obj.state.screen_id}
          name={obj.state.screenName}
        />
      )}
      {data.component_type === 8 && (
        <ImageGallerySettings
          data={data}
          updateComponents={obj.updateComponents}
          obj={obj}
        />
      )}
    </>
  )
}

export default SectionSettingSidebar

const useStyles = makeStyles(theme => ({

  row: {
    display: 'flex',
    alignItems: 'center'
  },
  ListItemText: {
    color: '#c9c9c9',
    fontSize: 14
  },
  ListItemBorder: {
    borderBottomWidth: 1,
    borderStyle: 'solid',
    borderColor: '#b2b2b2'
  },
  ListItem: {
    paddingTop: 10,
    paddingBottom: 12
  }
}))

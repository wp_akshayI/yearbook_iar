package com.app.yearbook.restclient;

import com.app.yearbook.model.CheckUserStatus;
import com.app.yearbook.model.GradesResponse;
import com.app.yearbook.model.Login;
import com.app.yearbook.model.NotificationAcceptDeny;
import com.app.yearbook.model.SearchSchool;
import com.app.yearbook.model.SignatureYearsListModel;
import com.app.yearbook.model.StaffProfileResponse;
import com.app.yearbook.model.StudentSignUP;
import com.app.yearbook.model.UserProfileResponse;
import com.app.yearbook.model.UserSignup;
import com.app.yearbook.model.YBResponse;
import com.app.yearbook.model.employee.GetImageResponse;
import com.app.yearbook.model.employee.GetSchool;
import com.app.yearbook.model.employee.GetUserSchoolWise;
import com.app.yearbook.model.library.AddPost;
import com.app.yearbook.model.postmodels.BookmarkPostResponse;
import com.app.yearbook.model.postmodels.SearchResponse;
import com.app.yearbook.model.postmodels.StaffPostResponse;
import com.app.yearbook.model.profile.ChangeProfilePhoto;
import com.app.yearbook.model.profile.getNotification;
import com.app.yearbook.model.profile.getSignature;
import com.app.yearbook.model.profile.getUserProfile;
import com.app.yearbook.model.search.getPeople;
import com.app.yearbook.model.staff.EditProfileResponse;
import com.app.yearbook.model.staff.GetStaffPostDEtail;
import com.app.yearbook.model.staff.StaffRequest;
import com.app.yearbook.model.staff.StaffSignUP;
import com.app.yearbook.model.staff.home.EditStaffPost;
import com.app.yearbook.model.staff.home.GetContent;
import com.app.yearbook.model.staff.search.GetTegResponse;
import com.app.yearbook.model.staff.trash.GetDeleteList;
import com.app.yearbook.model.staff.trash.GetReportedPost;
import com.app.yearbook.model.staff.yearbook.GetStaffYearbook;
import com.app.yearbook.model.staff.yearbook.GetStudentYearbook;
import com.app.yearbook.model.studenthome.AddBookMark;
import com.app.yearbook.model.studenthome.AddComment;
import com.app.yearbook.model.studenthome.EditPost;
import com.app.yearbook.model.studenthome.GetComment;
import com.app.yearbook.model.studenthome.GetLike;
import com.app.yearbook.model.studenthome.GetSchoolDuration;
import com.app.yearbook.model.studenthome.GetStudentPost;
import com.app.yearbook.model.studenthome.GetUserPost;
import com.app.yearbook.model.studenthome.GiveReport;
import com.app.yearbook.model.studenthome.LikeUnlike;
import com.app.yearbook.model.studenthome.OnOffNotification;
import com.app.yearbook.model.studenthome.profile.GetUserProfile;
import com.app.yearbook.model.yearbook.GetCategory;
import com.app.yearbook.model.yearbook.GetYearBookComment;
import com.app.yearbook.model.yearbook.getYearBook;
import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface RetrofitClass {

    //login
    @FormUrlEncoded
    @POST("login.php")
    Call<Login> login(@Field("emailid") String email,
                      @Field("password") String password,
                      @Field("devicetype") String devicetype,
                      @Field("devicetoken") String token);

    //student registration
    @FormUrlEncoded
    @POST("user_signup.php")
    Call<StudentSignUP> studentSignUp(@Field("firstname") String firstname,
                                      @Field("lastname") String lastname,
                                      @Field("emailid") String email,
                                      @Field("password") String password,
                                      @Field("school_id") int schoolid,
                                      @Field("student_id") int studentid,
                                      @Field("access_code") String accesscode,
                                      @Field("devicetype") String devicetype,
                                      @Field("devicetoken") String token);

    //search school
    @POST("search_school.php")
    Call<SearchSchool> getAllSchool();

    /*//Student post
    @FormUrlEncoded
    @POST("get_post.php")
    Call<GetHomeResponse> getStudentPost(@Field("user_id") int userId,
                                         @Field("school_id") int schoolId,
                                         @Field("page") int page);*/

    //Student post
    @FormUrlEncoded
    @POST("get_post.php")
    Call<StaffPostResponse> getStudentPost(@Field("user_id") int userId,
                                           @Field("school_id") int schoolId,
                                           @Field("page") int page);

    //forgot password
    @FormUrlEncoded
    @POST("forgot_password.php")
    Call<GiveReport> forgotPwd(@Field("email") String emailid);

    //Student post like unlike
    @FormUrlEncoded
    @POST("add_like_unlike.php")
    Call<LikeUnlike> postLikeDislike(@Field("user_id") int userId,
                                     @Field("post_id") int schoolId,
                                     @Field("user_type") int user_type);   //1-user & 2-staff

    //double tap like
    @FormUrlEncoded
    @POST("add_like.php")
    Call<LikeUnlike> addDoubleTapLike(@Field("user_id") int userId,
                                      @Field("post_id") int postId);

    //get Comment
    @FormUrlEncoded
    @POST("get_comment_list.php")
    Call<GetComment> getComments(@Field("post_id") int postId);

    //add Comment
    @FormUrlEncoded
    @POST("add_comment.php")
    Call<AddComment> addComments(@Field("user_id") int userId,
                                 @Field("post_id") int postId,
                                 @Field("comment_message") String msg);

    //book mark
    @FormUrlEncoded
    @POST("add_bookmark.php")
    Call<AddBookMark> addBookmark(@Field("user_id") int userId,
                                  @Field("post_id") int postId);

    //notification
    @FormUrlEncoded
    @POST("add_post_notification_status.php")
    Call<OnOffNotification> notification(@Field("user_id") int userId,
                                         @Field("post_id") int postId);


    //reported
    @FormUrlEncoded
    @POST("add_post_repoted.php")
    Call<GiveReport> reported(@Field("user_id") int userId,
                              @Field("post_id") int postId,
                              @Field("message") String msg);

    //Add post
//    @Multipart
//    @POST("add_user_post.php")
//    Call<AddPost> addPost(@Part("user_id") RequestBody userId,
//                          @Part("school_id") RequestBody schoolId,
//                          @Part("description") RequestBody des,
//                          @Part MultipartBody.Part imageVideo,
//                          @Part("post_file") RequestBody name,
//                          @Part("file_type") RequestBody type,
//                          @Part MultipartBody.Part thumb,
//                          @Part("post_thumbnail") RequestBody thumlPost);

    //Add post (new API)
    @Multipart
    @POST("new_add_user_post.php")
    Call<AddPost> addPost(@Part("user_id") RequestBody userId,
                          @Part("school_id") RequestBody schoolId,
                          @Part("description") RequestBody des,
                          @Part MultipartBody.Part imageVideo,
                          @Part("post_file") RequestBody name,
                          @Part("file_type") RequestBody type,
                          @Part MultipartBody.Part thumb,
                          @Part("tagg_user_id") RequestBody taguid,
                          @Part("post_thumbnail") RequestBody thumlPost);

    //edit post
    @FormUrlEncoded
    @POST("edit_user_post.php")
    Call<EditPost> editPost(@Field("post_id") int postId,
                            @Field("description") String des,
                            @Field("user_id") String userId);

    //delete post
    @FormUrlEncoded
    @POST("delete_post.php")
    Call<GiveReport> deletePost(@Field("post_id") int postId);

    //search people
    @FormUrlEncoded
    @POST("search_user_tags.php")
    Call<getPeople> searchPeople(@Field("search") String search,
                                 @Field("flag") String flag,
                                 @Field("user_id") String uid,
                                 @Field("school_id") String id);

    //search tag
    @FormUrlEncoded
    @POST("search_user_tags.php")
    Call<GetTegResponse> searchTag(@Field("search") String search,
                                   @Field("flag") String flag,
                                   @Field("user_id") String uid,
                                   @Field("school_id") String id);

    //get user profile
    @FormUrlEncoded
    @POST("get_user_profile.php")
    Call<GetUserProfile> getUserProfile(@Field("user_id") int id, @Field("school_id") int sid);

    //get search user profile
    @FormUrlEncoded
    @POST("get_search_user_profile.php")
    Call<getUserProfile> getSearchUserProfile(@Field("user_id") int id);


    //get search user tag post
    @FormUrlEncoded
    @POST("get_tagg_post.php")
    Call<getUserProfile> getUserTag(@Field("user_id") int id,
                                    @Field("school_id") int sid);

    //add signature
    @FormUrlEncoded
    @POST("add_signature.php")
    Call<GiveReport> addSignature(@Field("user_id") int id,
                                  @Field("receiver_id") int rid,
                                  @Field("signature_message") String msg,
                                  @Field("signature_font_type") int type);

    //get signature
    @FormUrlEncoded
    @POST("get_signature_list.php")
    Call<getSignature> getSignature(@Field("receiver_id") int rid, @Field("receiver_type") int receiverType, @Field("year") String year);

    //get notification
    @FormUrlEncoded
    @POST("get_notification_list.php")
    Call<getNotification> getNotification(@Field("user_id") int id, @Field("user_type") int userType);

    //get bookmark new
    @FormUrlEncoded
    @POST("get_bookmark_list.php")
    Call<BookmarkPostResponse> getBookmark(@Field("user_id") int id);

//    //Add /Edit user photo
//    @Multipart
//    @POST("edit_user_photo.php")
//    Call<ChangeProfilePhoto> addEditUserPhoto(@Part("user_id") RequestBody userId,
//                                              @Part MultipartBody.Part image,
//                                              @Part("user_image") RequestBody name);

    //Add /Edit user photo
    @Multipart
    @POST("edit_user_photo.php")
    Call<ChangeProfilePhoto> addEditUserPhoto(@Part("user_id") RequestBody userId,
                                              @Part("user_image") RequestBody name);

    //get yearbook
    @FormUrlEncoded
    @POST("get_year_book_list.php")
    Call<getYearBook> getYearbook(@Field("user_id") int id,
                                  @Field("school_id") int sid,
                                  @Field("user_flag") String flag);

    //change password
    @FormUrlEncoded
    @POST("change_password.php")
    Call<GiveReport> changePassword(@Field("user_id") int id,
                                    @Field("flag") String flag,
                                    @Field("user_password") String pwd);

    //logout
    @FormUrlEncoded
    @POST("logout.php")
    Call<GiveReport> logout(@Field("devicetoken") String token,
                            @Field("flag") String flag);

    //get category
    @Multipart
    @POST("get_category_list.php")
    Call<GetCategory> getCategory(@Part("yearbook_id") RequestBody id);

    //get user post
    @FormUrlEncoded
    @POST("get_post_details.php")
    Call<GetUserPost> getUserPost(@Field("user_id") int id,
                                  @Field("post_id") int pid);

    //get student yearbook post
    @FormUrlEncoded
    @POST("get_yearbook_post_details.php")
    Call<GetStudentYearbook> getStudentYBDetail(@Field("user_id") int id,
                                                @Field("post_id") int pid);

    //delete signature
    @FormUrlEncoded
    @POST("delete_signature.php")
    Call<GiveReport> deleteSignature(@Field("signature_id") int sId);

    //add book mark
    @FormUrlEncoded
    @POST("add_yearbook_bookmark.php")
    Call<AddBookMark> addYearbookBookmark(@Field("user_id") int userId,
                                          @Field("post_id") int postId);

    // year book notification
    @FormUrlEncoded
    @POST("notification_on_off_request.php")
    Call<OnOffNotification> taggUserNotification(@Field("user_id") int userId,
                                                 @Field("yearbook_post_id") int yId);

    //get search tag detail
    @FormUrlEncoded
    @POST("get_search_tag_post.php")
    Call<GetReportedPost> getSearchTagPost(@Field("user_id") int userId,
                                           @Field("school_id") int schoolId,
                                           @Field("tags") String tag,
                                           @Field("flag") String flag);

    //get search tag detail
    @FormUrlEncoded
    @POST("search_user_tags.php")
    Call<SearchResponse> searchPost(@Field("user_id") int userId,
                                    @Field("school_id") int schoolId,
                                    @Field("search") String search,
                                    @Field("type") String type);


    //get like
    @FormUrlEncoded
    @POST("get_like_user_list.php")
    Call<GetLike> getAllLike(@Field("post_id") int postId);

    //remove profile photo
    @FormUrlEncoded
    @POST("delete_user_image.php")
    Call<Login> removeProfilePhoto(@Field("user_id") int userId);

    //remove staff profile photo
    @FormUrlEncoded
    @POST("delete_staff_image.php")
    Call<Login> removeStaffImage(@Field("staff_id") int staffId);

    //student own notification
    @FormUrlEncoded
    @POST("notification_on_off_request_for_post.php")
    Call<OnOffNotification> studentOwnPostNotification(@Field("user_id") int userId, @Field("post_id") int postId);

    //check user status
    @FormUrlEncoded
    @POST("check_user_status.php")
    Call<CheckUserStatus> checkStatus(@Field("user_id") int userId);

    //terms and condition
    @FormUrlEncoded
    @POST("user_agree.php")
    Call<GiveReport> termsCondition(@Field("user_id") int userId, @Field("agree") String agree);


    //check user status
    @FormUrlEncoded
    @POST("signature_request.php")
    Call<NotificationAcceptDeny> notificationStatus(@Field("notification_id") int userId, @Field("request_status") int status);

    //check sign yearbook
    //Out dated
    @FormUrlEncoded
    @POST("check_yearbook_publish.php")
    Call<GiveReport> checkYearbookPublish(@Field("school_id") int sid);

    @FormUrlEncoded
    @POST("web_editor/check_yearbook_publish.php")
    Call<YBResponse> checkYearbookPublish(@Field("Token") String Token, @Field("platform") String platform);


    //---------------------------------------------------staff------------------------------------------------

    //staff request code
    @FormUrlEncoded
    @POST("staff_student_access_request.php")
    Call<StaffRequest> staffRequestCode(@Field("school_id") int schoolid,
                                        @Field("firstname") String firstname,
                                        @Field("lastname") String lastname,
                                        @Field("emailid") String email,
                                        @Field("phoneno") String phoneno,
                                        @Field("user_type") String usertype,
                                        @Field("devicetype") String devicetype,
                                        @Field("devicetoken") String token);

    //staff signup
    @FormUrlEncoded
    @POST("staff_student_signup.php")
    Call<StaffSignUP> staffSignUp(@Field("access_code") String accesscode,
                                  @Field("emailid") String email,
                                  @Field("password") String password,
                                  @Field("devicetype") String devicetype,
                                  @Field("devicetoken") String token);


    //get staff post
    @FormUrlEncoded
    @POST("get_staff_post.php")
    Call<GetStudentPost> getStaffPost(@Field("user_id") int userId,
                                      @Field("school_id") int schoolId,
                                      @Field("page") int page);

    //delete post
    @FormUrlEncoded
    @POST("staff_delete_post.php")
    Call<GiveReport> staffDeletePost(@Field("post_id") int id, @Field("staff_id") int staffId);

    //delete user
    @FormUrlEncoded
    @POST("delete_user.php")
    Call<GiveReport> staffDeleteUser(@Field("user_id") int id);

    //delete tag
    @FormUrlEncoded
    @POST("delete_tags.php")
    Call<GiveReport> staffDeleteTag(@Field("tag") String tag);

    //get content
    @FormUrlEncoded
    @POST("get_repoted_content.php")
    Call<GetContent> getContent(@Field("post_id") int id);

    //get delete post and user
    @FormUrlEncoded
    @POST("get_delete_post_and_user.php")
    Call<GetDeleteList> deletePostUser(@Field("school_id") String sid);

    //undo user
    @FormUrlEncoded
    @POST("undo_delete_user.php")
    Call<GiveReport> undoUser(@Field("user_id") String user_id);

    //undo post
    @FormUrlEncoded
    @POST("undo_deleted_post.php")
    Call<GiveReport> undoPost(@Field("post_id") String post_id);


    //get staff side user post
    @FormUrlEncoded
    @POST("get_staff_post_details.php")
    Call<GetStaffPostDEtail> getStaffPost(@Field("post_id") int pid);

    //get advertise
    @Multipart
    @POST("add_advertise.php")
    Call<GiveReport> addAdvertise(@Part("school_id") RequestBody schoolId,
                                  @Part("file_type") RequestBody file_type,
                                  @Part MultipartBody.Part image,
                                  @Part("post_file") RequestBody name);

    //Edit Staff Image
    @Multipart
    @POST("edit_staff_profile.php")
    Call<EditProfileResponse> editStaffProfile(@Part("staff_id") RequestBody staffID,
                                               @Part MultipartBody.Part image/*,
                                 @Part("staff_image") RequestBody name*/);

    //get advertise
    @FormUrlEncoded
    @POST("get_advertise_list.php")
    Call<GetStaffPostDEtail> getAdvertise(@Field("school_id") int schoolId);

    //delete advertise
    @FormUrlEncoded
    @POST("delete_advertise.php")
    Call<GiveReport> deleteAdvertise(@Field("post_id") int pid);

    //delete comment
    @FormUrlEncoded
    @POST("delete_comment.php")
    Call<GiveReport> deleteComment(@Field("comment_id") int pid,
                                   @Field("flag") String flag);

    //add category
    @FormUrlEncoded
    @POST("new_add_category.php")
    Call<GiveReport> addCategory(@Field("yearbook_id") int yid,
                                 @Field("category_name") String cname);

    //delete category
    @FormUrlEncoded
    @POST("delete_category.php")
    Call<GiveReport> deleteCategory(@Field("category_id") int cid);

    //edit category
    @FormUrlEncoded
    @POST("new_edit_category.php")
    Call<GiveReport> editCategory(@Field("yearbook_id") int yid,
                                  @Field("category_id") int cid,
                                  @Field("category_name") String cname);

    //get yearbook
    @FormUrlEncoded
    @POST("new_get_yearbook_post.php")
    Call<GetStaffYearbook> getStaffYearbookPost(@Field("category_id") int cid,
                                                @Field("school_id") int sid,
                                                @Field("user_id") int uid,
                                                @Field("flag") String flag);

    //Add post
    @Multipart
    @POST("new_add_yearbook_post.php")
    Call<AddPost> addStaffPost(@Part("user_id") RequestBody userId,
                               @Part("school_id") RequestBody schoolId,
                               @Part("staff_id") RequestBody sId,
                               @Part("category_id") RequestBody cId,
                               @Part("description") RequestBody des,
                               @Part MultipartBody.Part imageVideo,
                               @Part("post_file") RequestBody name,
                               @Part("file_type") RequestBody type,
                               @Part MultipartBody.Part thumb,
                               @Part("post_thumbnail") RequestBody thumlPost,
                               @Part("file_size_type") RequestBody filesizetype,
                               @Part("post_user_id") RequestBody postUserId,
                               @Part("post_type") RequestBody post_type
    );

    //delete yearbook post
    @FormUrlEncoded
    @POST("delete_yearbook_post.php")
    Call<GiveReport> deleteYearbookPost(@Field("post_id") int pid);

    //Edit staff post
    @Multipart
    @POST("edit_yearbook_post.php")
    Call<GiveReport> editStaffPost(@Part("post_id") RequestBody userId,
                                   @Part("file_type") RequestBody type,
                                   @Part("user_id") RequestBody user_id,
                                   @Part("description") RequestBody des,
                                   @Part MultipartBody.Part imageVideo,
                                   @Part("post_file") RequestBody name,
                                   @Part MultipartBody.Part thumb,
                                   @Part("post_thumbnail") RequestBody thumlPost,
                                   @Part("file_size_type") RequestBody filesizetype,
                                   @Part("post_user_id") RequestBody postUserId,
                                   @Part("post_type") RequestBody post_type
    );

    //get yearbookComment
    @FormUrlEncoded
    @POST("get_yearbook_comment_list.php")
    Call<GetYearBookComment> getYearBookComments(@Field("post_id") int postId);

    //add yearbook comment
    @FormUrlEncoded
    @POST("add_yearbook_comment.php")
    Call<AddComment> addYearbookComments(@Field("user_id") int userId,
                                         @Field("post_id") int postId,
                                         @Field("comment_message") String msg);

    //add yearbook like
    @FormUrlEncoded
    @POST("add_yearbook_like_unlike.php")
    Call<LikeUnlike> postYearbookLikeDislike(@Field("user_id") int userId,
                                             @Field("post_id") int schoolId);

    //get like
    @FormUrlEncoded
    @POST("get_yearbook_like_user_list.php")
    Call<GetLike> getAllYearbookLike(@Field("yearbook_post_id") int postId);


    //delete yearbook cmnt
    @FormUrlEncoded
    @POST("delete_yearbook_comment.php")
    Call<EditPost> DeleteYaerbookComment(@Field("comment_id") int cId);

    //check staff status
    @FormUrlEncoded
    @POST("check_staff_status.php")
    Call<CheckUserStatus> checkStaffStatus(@Field("staff_id") int staffId);

    //terms and condition
    @FormUrlEncoded
    @POST("staff_agree.php")
    Call<GiveReport> termsConditionStaff(@Field("staff_id") int staffId, @Field("agree") String agree);

    //   ------------------------------------------school add ------------------------------------------
    //add school
    @FormUrlEncoded
    @POST("add_school.php")
    Call<GiveReport> addSchool(@Field("school_name") String name);

    //school access code
    @FormUrlEncoded
    @POST("school_access_request.php")
    Call<StaffRequest> schoolAccessCode(@Field("school_id") String school,
                                        @Field("emailid") String email,
                                        @Field("flag") String flag,
                                        @Field("phoneno") String phone);

    //get school duration
    @FormUrlEncoded
    @POST("get_school_duration.php")
    Call<GetSchoolDuration> getSchoolDuration(@Field("school_id") String school_id);

    //------------------------------------------employee------------------------------------------
    //get school
    @POST("get_school_list.php")
    Call<GetSchool> getEmpSchool();

    //get school's user
    @FormUrlEncoded
    @POST("get_school_user_list.php")
    Call<GetUserSchoolWise> getUserSchool(@Field("school_id") String school_id);

    //Upload image
//    @Multipart
//    @POST("add_user_images.php")
//    Call<GiveReport> uploadImages(@Part("user_id") String uid, @Part("employee_id") String eid, @Part MultipartBody.Part[] images);

    //Upload image
    @Multipart
    @POST("add_user_images.php")
    Call<GetImageResponse> uploadImages(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part[] images);

    //edit image
    @Multipart
    @POST("edit_user_images.php")
    Call<GetImageResponse> editImages(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part[] images);

    //delete user image
    @FormUrlEncoded
    @POST("delete_user_images.php")
    Call<GetImageResponse> deleteUserImage(@Field("user_id") String user_id, @Field("image_name") String imagename);

    //----------------------------------------------------new staff------------------------------------------------------
    //Add post (new API)
    @Multipart
    @POST("new_add_staff_post.php")
    Call<AddPost> addStaffPost(@Part("post_time_stamp") RequestBody timestamp,
                               @Part("staff_id") RequestBody userId,
                               @Part("school_id") RequestBody schoolId,
                               @Part("post_title") RequestBody title,
                               @Part("description") RequestBody des,
                               @Part MultipartBody.Part imageVideo,
                               @Part("post_file") RequestBody name,
                               @Part("file_type") RequestBody type,
                               @Part MultipartBody.Part thumb,
                               @Part("tagg_user_id") RequestBody taguid,
                               @Part("post_thumbnail") RequestBody thumlPost);

    //edit post
    @FormUrlEncoded
    @POST("edit_staff_post.php")
    Call<EditStaffPost> editStaffPost(@Field("post_time_stamp") String timestamp,
                                      @Field("post_id") int postId,
                                      @Field("description") String des,
                                      @Field("user_id") String userId);


    /*//get staff post
    @FormUrlEncoded
    @POST("get_staff_post.php")
    Call<GetStaffPost> getStaffPostNew(@Field("staff_id") int userId,
                                       @Field("school_id") int schoolId,
                                       @Field("page") int page);*/

    //get staff post
    @FormUrlEncoded
    @POST("get_staff_post.php")
    Call<StaffPostResponse> getStaffPostNew(@Field("staff_id") int userId,
                                            @Field("school_id") int schoolId,
                                            @Field("page") int page);


    //get reported post
    @FormUrlEncoded
    @POST("get_repoted_post_list.php")
    Call<GetReportedPost> getReportedPost(@Field("school_id") int schoolId,
                                          @Field("page") int page);

    //search student
    @FormUrlEncoded
    @POST("search_user_tags.php")
    Call<getPeople> searchStudent(@Field("search") String search,
                                  @Field("flag") String flag,
                                  @Field("staff_id") String uid,
                                  @Field("school_id") String id);


    //edit post
    @Multipart
    @POST("edit_staff_post.php")
    Call<Login> editPostData(@Part("post_time_stamp") RequestBody timestamp,
                             @Part("post_id") RequestBody postId,
                             @Part("post_type") RequestBody type,
                             @Part("user_id") RequestBody userId,
                             @Part("post_title") RequestBody postTitle,
                             @Part("post_sub_title") RequestBody postSubTitle,
                             @Part("post_description") RequestBody postDescription,
                             @Part("options") RequestBody options,
                             @Part("expiry_date") RequestBody expiry_date,
                             @Part("file_type") RequestBody fileType,
                             @Part MultipartBody.Part imageVideo, @Part MultipartBody.Part thumbPart);

    //add text post
    @FormUrlEncoded
    @POST("new_add_staff_post.php")
    Call<Login> addTextPost(@Field("post_time_stamp") String timestamp,
                            @Field("post_type") int type,
                            @Field("staff_id") int staffId,
                            @Field("school_id") int schoolId,
                            @Field("post_title") String postTitle,
                            @Field("post_sub_title") String postSubTitle,
                            @Field("description") String postDescription);

    //add text post
    @FormUrlEncoded
    @POST("new_add_staff_post.php")
    Call<Login> addPollPost(@Field("post_time_stamp") String timestamp,
                            @Field("post_type") int type,
                            @Field("staff_id") int staffId,
                            @Field("school_id") int schoolId,
                            @Field("post_title") String postTitle,
                            @Field("post_sub_title") String postSubTitle,
                            @Field("options") String options,
                            @Field("grade") String grade,
                            @Field("expiry_date") String expiryDate);

    // add Image Post
    @Multipart
    @POST("new_add_staff_post.php")
    Call<Login> addMediaPost(@Part("post_time_stamp") RequestBody timestamp,
                             @Part("post_type") RequestBody type,
                             @Part("staff_id") RequestBody staffId,
                             @Part("school_id") RequestBody schoolId,
                             @Part("post_title") RequestBody postTitle,
                             @Part("post_sub_title") RequestBody postSubTitle,
                             @Part("post_description") RequestBody postDescription,
                             @Part("file_type") RequestBody fileType,
                             @Part MultipartBody.Part imageVideo, @Part MultipartBody.Part thumbPart);

    //get reported post
    @Headers({"Cache-Control: no-cache"})
    @POST("get_school_grade.php")
    Call<GradesResponse> getSchoolGrade();


    //bookmark and unbookmark staff
    @FormUrlEncoded
    @POST("add_staff_unbookmark.php")
    Call<Login> addBookmarkUnbookmark(@Field("staff_id") String staffId,
                                      @Field("post_id") String postId);

    //bookmark and unbookmark student
    @FormUrlEncoded
    @POST("add_bookmark.php")
    Call<Login> addBookmarkUnbookmarkStudent(@Field("user_id") String staffId,
                                             @Field("post_id") String postId);

    // get Notification
    @POST("get_notification.php")
    Call<Login> getNotification();

    // add User Poll
    @FormUrlEncoded
    @POST("add_user_vote_poll.php")
    Call<Login> getUserVotePoll(@Field("poll_option_id") String pollOptionId,
                                @Field("user_id") String userId,
                                @Field("user_type") int user_type);

    // get Staff Bookmark List
    @FormUrlEncoded
    @POST("get_staff_bookmark_list.php")
    Call<BookmarkPostResponse> getStaffBookmarkList(@Field("staff_id") String staffId);

    // get User Profile
    @FormUrlEncoded
    @POST("get_user_profile.php")
    Call<UserProfileResponse> getUserProfile(@Field("user_id") String userId,
                                             @Field("school_id") String schoolId,
                                             @Field("current_user_id") String currentUserId);

    @FormUrlEncoded
    @POST("get_staff_profile.php")
    Call<StaffProfileResponse> getStaffProfile(@Field("staff_id") String userId);

    // add signature request
    /*@FormUrlEncoded
    @POST("add_signature_request.php")
    Call<Login> addSignatureRequest(@Field("user_id") String userId,
                                    @Field("receiver_id") String receiverId);*/

    @FormUrlEncoded
    @POST("add_signature_request.php")
    Call<Login> addSignatureRequest(@Field("user_id") String userId,
                                    @Field("receiver_id") String receiverId,
                                    @Field("sender_type") String senderType,
                                    @Field("receiver_type") String receiverType);

    // add signature request
    @FormUrlEncoded
    @POST("add_signature.php")
    Call<Login> addSignatureAll(@Field("user_id") String userId,
                                @Field("receiver_id") String receiverId,
                                @Field("sender_type") String senderType,
                                @Field("receiver_type") String receiverType,
                                @Field("signature_greeting") String signatureGreeting,
                                @Field("signature_message") String signatureMessage,
                                @Field("signature_font_type") String signatureFontType);

   /* Call<Login> addSignatureNew(@Field("user_id") String userId,
                             @Field("receiver_id") String receiverId,
                             @Field("signature_greeting") String signatureGreeting,
                             @Field("signature_message") String signatureMessage,
                             @Field("signature_font_type") String signatureFontType);*/

    @FormUrlEncoded
    @POST("verify_access_code.php")
    Call<Login> verifyAccessCode(@Field("type") String type,
                                 @Field("user_id") String user_id,
                                 @Field("access_code") String access_code);
    @FormUrlEncoded
    @POST("signup.php")
    Call<UserSignup> newUserSignup(@Field("emailid") String email,
                                   @Field("password") String password,
                                   @Field("devicetype") String devicetype,
                                   @Field("devicetoken") String token);

    @FormUrlEncoded
    @POST("get_user_profile.php")
    Call<JsonObject> get_user_profile(@Field("user_id") String user_id,
                                      @Field("school_id") String school_id);
    @FormUrlEncoded
    @POST("get_staff_profile.php")
    Call<JsonObject> get_staff_profile(@Field("staff_id") String userId);

    @FormUrlEncoded
    @POST("get_signature_years_list.php")
    Call<SignatureYearsListModel> getSignatureYear(@Field("receiver_id") int rid, @Field("receiver_type") int receiverType);
}

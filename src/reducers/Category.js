import { action } from "../utils";

let initialState = {
    categories: undefined,
    subCategories: undefined,
    categoryPageTitle: ""
}

export default function CategoryReducer(state = initialState, data) {

    switch (data.type) {

        case action.get_categories:
            return {
                ...state,
                categories: data.categories
            }

        case action.get_gride_sub_category:
            return {
                ...state,
                subCategories: data.subCategories
            }

        case action.set_category_page_title:
            return {
                ...state,
                categoryPageTitle: data.title
            }

        default:
            break;
    }
    return state;
}
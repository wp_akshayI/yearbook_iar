package com.app.yearbook.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.yearbook.R;
import com.app.yearbook.utils.OnRecyclerClick;

import java.util.ArrayList;

public class StudentProfileAdapter extends RecyclerView.Adapter<StudentProfileAdapter.MyViewHolderGallery> {

    private Context context;
    private ArrayList<String> imgList;
    private LayoutInflater inflater;
    private OnRecyclerClick onRecyclerClick;
    private static final String TAG = "UserImageAdapter";

    public StudentProfileAdapter(Context context, ArrayList<String> imgList, OnRecyclerClick onRecyclerClick) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.imgList = imgList;
        this.onRecyclerClick = onRecyclerClick;
    }

    @Override
    public StudentProfileAdapter.MyViewHolderGallery onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = inflater.inflate(R.layout.item_userpost, parent, false);
        MyViewHolderGallery holder = new MyViewHolderGallery(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(MyViewHolderGallery holder, int position) {
        // holder.imgIcon.setImageURI(imgList.get(position).toString());

        Log.d(TAG, "onBindViewHolder: " + imgList.get(position).toString());
        Glide.with(context)
                .load(imgList.get(position))
                .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.home_post_img_placeholder)
                .error(R.mipmap.home_post_img_placeholder)
                .into(holder.imgPost);
    }

    @Override
    public int getItemCount() {
        return imgList.size();
    }

    class MyViewHolderGallery extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgPostVideo, imgPost;

        public MyViewHolderGallery(View itemView) {
            super(itemView);
            imgPost = itemView.findViewById(R.id.imgPost);
            imgPostVideo = itemView.findViewById(R.id.imgPostVideo);
            imgPostVideo.setVisibility(View.GONE);
            imgPost.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.imgPost) {
                 onRecyclerClick.onClick(getAdapterPosition(), v, 0);
            }
        }
    }
}



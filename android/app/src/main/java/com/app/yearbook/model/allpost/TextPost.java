package com.app.yearbook.model.allpost;

import android.os.Parcel;
import android.os.Parcelable;

public class TextPost implements Parcelable {
    String subject, subTitle, composedPost, date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public TextPost(String subject, String subTitle, String composedPost, String date) {
        this.subject = subject;
        this.subTitle = subTitle;
        this.composedPost = composedPost;
        this.date = date;
    }

    public TextPost() {
    }

    protected TextPost(Parcel in) {
        subject = in.readString();
        subTitle = in.readString();
        composedPost = in.readString();
        date = in.readString();
    }

    public static final Creator<TextPost> CREATOR = new Creator<TextPost>() {
        @Override
        public TextPost createFromParcel(Parcel in) {
            return new TextPost(in);
        }

        @Override
        public TextPost[] newArray(int size) {
            return new TextPost[size];
        }
    };

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getComposedPost() {
        return composedPost;
    }

    public void setComposedPost(String composedPost) {
        this.composedPost = composedPost;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(subject);
        dest.writeString(subTitle);
        dest.writeString(composedPost);
        dest.writeString(date);
    }
}

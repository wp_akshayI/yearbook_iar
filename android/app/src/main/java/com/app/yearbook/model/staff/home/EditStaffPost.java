package com.app.yearbook.model.staff.home;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.app.yearbook.model.staff.livefeed.PostListItem;

public class EditStaffPost{

	@SerializedName("ResponseCode")
	private String responseCode;

	@SerializedName("ServerTimeZone")
	private String serverTimeZone;

	@SerializedName("ResponseMsg")
	private String responseMsg;

	@SerializedName("serverTime")
	private String serverTime;

	@SerializedName("post_Detail")
	private List<PostListItem> postDetail;

	@SerializedName("Result")
	private String result;

	public void setResponseCode(String responseCode){
		this.responseCode = responseCode;
	}

	public String getResponseCode(){
		return responseCode;
	}

	public void setServerTimeZone(String serverTimeZone){
		this.serverTimeZone = serverTimeZone;
	}

	public String getServerTimeZone(){
		return serverTimeZone;
	}

	public void setResponseMsg(String responseMsg){
		this.responseMsg = responseMsg;
	}

	public String getResponseMsg(){
		return responseMsg;
	}

	public void setServerTime(String serverTime){
		this.serverTime = serverTime;
	}

	public String getServerTime(){
		return serverTime;
	}

	public void setPostDetail(List<PostListItem> postDetail){
		this.postDetail = postDetail;
	}

	public List<PostListItem> getPostDetail(){
		return postDetail;
	}

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}
}
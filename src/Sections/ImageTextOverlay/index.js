import React, { useState, useEffect } from 'react'
import { View, Text, ImageBackground, Dimensions } from 'react-native'
import ImageScal from '../../utils/ImageScal'

export default function ImageTextOverlay ({ data }) {
  let { component_url, component_title, x, y, font_name, font_size, font_color, font_border, rotation, border_color } = data

  const [state, setState] = useState({
    height: 200
  })

  useEffect(() => {
    _getScal()
  }, [component_url])

  const _getScal = async () => {
    if (!component_url) return
    const { height } = await ImageScal(data.component_url)
    if (height) { setState({ ...state, height }) }
  }
  font_size = Dimensions.get('screen').width * font_size / 320
  return (
    <View style={{ flex: 1 }}>
      <ImageBackground
        source={{ uri: component_url }}
        style={{ height: state.height, width: '100%' }}
      >
        <Text
          style={[{
            fontSize: font_size,
            color: font_color,
            top: y + '%',
            left: x + '%',
            fontFamily: font_name,
            transform: [{ rotate: rotation + 'deg' }],
            alignSelf: 'baseline'
          },
          font_border && { textShadowColor: border_color || '#000', textShadowRadius: 5 }
          ]}
        >
          {component_title}
        </Text>
      </ImageBackground>
    </View>
  )
}

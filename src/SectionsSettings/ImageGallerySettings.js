import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { colors } from '../utils';
import Fab from "@material-ui/core/Fab";
import { Button } from '@material-ui/core';
import MediaUpload from 'components/MediaUpload';


export default function ImageGallerySettings({ data, updateComponents }) {
    const classes = useStyles();
    const [state, setState] = useState({
        grid_row: data.grid_row,
        isVisible: false,
    })

    const _selectionDone = (items) => {
        updateComponents({ images: items }, data.component_order_by);
        setState({ ...state, isVisible: false })
    }

    const _column = (grid_row) => {
        updateComponents({ grid_row }, data.component_order_by);
        setState({ ...state, grid_row })
    }

    return (
        <div style={{ padding: 10, flex: 1 }}>
            <div style={{ padding: 8 }} />
            <span style={{ color: colors.white }}>
                Grid Row
            </span>
            <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center", paddingTop: 10 }}>
                <Button
                    variant="contained"
                    color={state.grid_row === 1 || state.grid_row === "1" ? "primary" : "inherit"}
                    onClick={() => _column(1)}
                >
                    One
                </Button>
                <Button
                    variant="contained"
                    color={state.grid_row === 2 || state.grid_row === "2" ? "primary" : "inherit"}
                    onClick={() => _column(2)}
                >
                    Two
                </Button>
                <Button
                    variant="contained"
                    color={state.grid_row === 3 || state.grid_row === "3" ? "primary" : "inherit"}
                    onClick={() => _column(3)}
                >
                    Three
                </Button>
            </div>
            <div style={{ padding: 15 }} />
            <Fab
                variant="extended"
                size="medium"
                color="primary"
                classes={{ label: classes.buttonLable, sizeMedium: classes.buttonSize }}
                onClick={() => setState({ ...state, isVisible: !state.isVisible })}
            >
                Manage Column Images
            </Fab>
            <MediaUpload
                isVisible={state.isVisible}
                handleClose={() => setState({ ...state, isVisible: false })}
                onSubmit={_selectionDone}
                items={data.images}
            />
        </div>
    )
}

const useStyles = makeStyles(() => ({
    buttonLable: {
        textTransform: "capitalize",
    },
    buttonSize: {
        width: "100% !important"
    },
}));
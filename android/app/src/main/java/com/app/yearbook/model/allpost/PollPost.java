package com.app.yearbook.model.allpost;


import android.os.Parcel;
import android.os.Parcelable;

import com.app.yearbook.model.GradeListItem;

import java.util.List;

public class PollPost implements Parcelable {
    String subject;
    String description;

    GradeListItem gradeListItem;

    protected PollPost(Parcel in) {
        subject = in.readString();
        description = in.readString();
        gradeListItem = in.readParcelable(GradeListItem.class.getClassLoader());
        lastDate = in.readString();
        optionsModels = in.createTypedArrayList(OptionsModel.CREATOR);
    }

    public static final Creator<PollPost> CREATOR = new Creator<PollPost>() {
        @Override
        public PollPost createFromParcel(Parcel in) {
            return new PollPost(in);
        }

        @Override
        public PollPost[] newArray(int size) {
            return new PollPost[size];
        }
    };

    public GradeListItem getGradeListItem() {
        return gradeListItem;
    }

    public void setGradeListItem(GradeListItem gradeListItem) {
        this.gradeListItem = gradeListItem;
    }


    public String getLastDate() {
        return lastDate;
    }

    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }

    String lastDate;
    List<OptionsModel> optionsModels;


    public PollPost(String subject, String description, List<OptionsModel> optionsModels) {
        this.subject = subject;
        this.description = description;
        this.optionsModels = optionsModels;
    }

    public PollPost() {
    }


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<OptionsModel> getOptionsModels() {
        return optionsModels;
    }

    public void setOptionsModels(List<OptionsModel> optionsModels) {
        this.optionsModels = optionsModels;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(subject);
        dest.writeString(description);
        dest.writeParcelable(gradeListItem, flags);
        dest.writeString(lastDate);
        dest.writeTypedList(optionsModels);
    }
}

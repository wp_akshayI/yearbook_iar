import React from "react"
import { View, Image, Text } from "react-native";
import { Header } from "react-navigation-stack";
import { UnAthorized_ic } from "../../assets"


export default function UnAthorized() {
    return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingHorizontal: 30, marginTop: -Header.HEIGHT }}>

            <Image
                source={UnAthorized_ic}
                resizeMode="contain"
                style={{ height: 75, width: 75 }}
            />

            <Text style={{ textAlign: "center", fontWeight: "bold", fontSize: 15, marginTop: 30, marginBottom: 5 }}>
                Unathorized!
            </Text>

            <Text style={{ textAlign: "center", fontSize: 14 }}>
                You don't have right to access this yearbook!
            </Text>

        </View>
    )
}